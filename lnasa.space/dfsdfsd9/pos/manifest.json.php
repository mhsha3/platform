<?php 
$config_data = json_decode(base64_decode($_GET['config_data']));
if (!(json_last_error() === JSON_ERROR_NONE)) {
    $config_data = [];
}?>{
  "name": "<?php echo ((isset($config_data->config_name) && $config_data->config_name != "") ? $config_data->config_name . " - " : ""); ?>LanaPOS Web App",
  "short_name": "<?php echo ((isset($config_data->config_name) && $config_data->config_name != "") ? $config_data->config_name . " - " : ""); ?>LanaPOS PWA",
  "display": "standalone",
  "theme_color": "#f6c522",
  "background_color": "#1e1e2d",
  "scope": "./",
  "start_url": "./",
  "icons": [
     {
        "src": "<?php echo ((isset($config_data->config_icon32) && $config_data->config_icon32 != "") ? $config_data->config_icon32 : "assets/icons/pos-icon-32x32.png"); ?>",
        "sizes": "32x32",
        "type": "image/png"
      },
      {
        "src": "<?php echo ((isset($config_data->config_icon64) && $config_data->config_icon64 != "") ? $config_data->config_icon64 : "assets/icons/pos-icon-64x64.png"); ?>",
        "sizes": "64x64",
        "type": "image/png"
      },
      {
        "src": "<?php echo ((isset($config_data->config_icon128) && $config_data->config_icon128 != "") ? $config_data->config_icon128 : "assets/icons/pos-icon-128x128.png"); ?>",
        "sizes": "128x128",
        "type": "image/png"
      },
      {
        "src": "<?php echo ((isset($config_data->config_icon256) && $config_data->config_icon256 != "") ? $config_data->config_icon256 : "assets/icons/pos-icon-256x256.png"); ?>",
        "sizes": "256x256",
        "type": "image/png"
      },
      {
        "src": "<?php echo ((isset($config_data->config_icon512) && $config_data->config_icon512 != "") ? $config_data->config_icon512 : "assets/icons/pos-icon-512x512.png"); ?>",
        "sizes": "512x512",
        "type": "image/png"
      }
  ]
}