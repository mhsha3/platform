<?php
$url = isset($_REQUEST['url']) ? $_REQUEST['url'] : '';

?>
<!DOCTYPE html>
<html>
<body>

<h2>Redirect to a POS</h2>
<p>Please wait..........</p>

<script>
    <?php if($url): ?>
     location.replace("<?php echo $url; ?>");
    <?php else: ?>
     var url = window.location.href;
     var pos_url =  url.replace("redirect.php", "");
     location.replace(pos_url);
    <?php endif; ?>
</script>

</body>
</html>

