<?php
// Version
define('VERSION', '3.0.2.0');

// Configuration
if (is_file('../../config.php')) {
    require_once('../../config.php');
}
// Install
if (!defined('DIR_APPLICATION')) {
    header('Location: ../../install/index.php');
    exit;
}
// Startup
require_once(DIR_SYSTEM . 'startup.php');
//start('catalog');
$application_config = 'catalog';
// Registry
$registry = new Registry();

// Config
$config = new Config();
$config->load('default');
$config->load($application_config);
$registry->set('config', $config);

$url = new Url($config->get('site_url'), $config->get('site_ssl'));

$loader = new Loader($registry);
$registry->set('load', $loader);

// Library Autoload
if ($config->has('library_autoload')) {
    foreach ($config->get('library_autoload') as $value) {
        $loader->library($value);
    }
}
// Database
if ($config->get('db_autostart')) {
    $registry->set('db', new DB($config->get('db_engine'), $config->get('db_hostname'), $config->get('db_username'), $config->get('db_password'), $config->get('db_database'), $config->get('db_port')));
}

$loader->library('openpos');
$openpos = new Openpos($registry);
$custom_css = $openpos->getSetting('custom_css','pos',0);


$site_url = $config->get('site_ssl') ? HTTPS_SERVER : HTTP_SERVER;

$site_url = trim($site_url,'/');
$pos_verion = time();// $openpos->version();

$id = (int)$_GET['id'];
//echo DIR_CACHE;die;
?>
<?php if($id):  ?>
    <html lang="en" style="height: calc(100% - 0px);">
    <head>
        <meta charset="utf-8">
        <title>Bill Screen</title>
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/x-icon" href="favicon.ico">
        <script src="jquery-3.3.1.min.js"></script>
        <script src="ejs.js"></script>
        <script src="accounting.min.js"></script>
        <script src="NoSleep.min.js"></script>

        <script>
            var data_template= <?php echo json_encode(array(
                    'template' => '<% items.forEach(function(item){ %>
                        <tr>
                            <td><%- item.name %></td>
                            <td><%= item.qty %></td>
                            <td><%= item.total %></td>
                        </tr>
                       <% }); %>',
                    'total_cashier_template' => '<p><span><%= sale_person_name %></span></p>',
                    'total_cashier_grand_total' => '<span><%= grand_total %></span>',
            ));?>;
            var data_url = "<?php echo $url->link('extension/module/openpos/bill','',true); ?>";
            var register_id = <?php echo $id; ?>
        </script>
        <style type="text/css">
            .bill-content{
                width: 100%;
                height: 100%;
                display: block;
                background: #fff;
                position: relative;
                margin:0;

            }
            .order-total{
                position: absolute;
                height: 60px;
                width: 100%;
                border: solid 1px #999;
                bottom: 0;
                background: #4CAF50;
                color: #fff;
                border-radius: 10px;
            }
            .order-total .cashier-name
            {
                float: left;
                display: inline-block;
                padding: 5px;
            }
            .order-total .cashier-name span{
                border: solid 1px #0b96b9;
                border-radius: 10px;
                padding: 10px;
            }

            .order-total .grand-total{
                float: right;
                display: inline-block;
                text-align: right;
                padding-right:10px
            }

            .grand-total span{
                font-weight: bold;
                font-size: 50px;

            }
            .item-lists{
                display: block;
                height: calc(100% - 100px);
                padding: 10px;
                overflow:  auto;
                background: #fff;
            }
            .item-lists table{
                width: 100%;
            }
            .item-lists table tr.item-list-header{
                background: #FF5722;
                color: #fff;
            }
            .item-lists table tr.item-list-header th{
                padding: 15px 0;
            }
            .item-lists table td{
                text-align: center;
            }
            .item-lists table tbody td{
                padding: 5px 0;
            }
            .item-lists table tbody tr:nth-child(odd)
            {
                background: #cccccc;
            }
        </style>
    </head>
    <body>
    <div  class="bill-content">
        <div class="item-lists">
            <table>
                <thead>
                    <tr class="item-list-header">
                        <th>Product</th>
                        <th>Qty</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>

            </table>
        </div>
        <div class="order-total">
            <div class="cashier-name"></div>
            <div class="grand-total"><span>0.00</span></div>
        </div>
    </div>
    <script src="bill.js"></script>
    </body>
    </html>
<?php else: ?>
    <h1>Opppos !!!!</h1>
<?php endif; ?>

