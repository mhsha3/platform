<?php
    global $op_in_pos_screen;
    $op_in_pos_screen = true;
    // Version
    define('VERSION', '3.0.2.0');

    // Configuration
    if (is_file('../config.php')) {
        require_once('../config.php');
    }
    // Install
    if (!defined('DIR_APPLICATION')) {
        header('Location: ../install/index.php');
        exit;
    }
    // Startup
    require_once(DIR_SYSTEM . 'startup.php');
    //start('catalog');
    $application_config = 'catalog';
    // Registry
    $registry = new Registry();

    // Config
    $config = new Config();
    $config->load('default');
    $config->load($application_config);
    $registry->set('config', $config);

    $site_url_tmp = $config->get('site_ssl') ? $config->get('site_ssl') : $config->get('site_url');
    $url = new Url($site_url_tmp, $site_url_tmp);

    $loader = new Loader($registry);
    $registry->set('load', $loader);

    // Library Autoload
    if ($config->has('library_autoload')) {
        foreach ($config->get('library_autoload') as $value) {
            $loader->library($value);
        }
    }
    // Database
    if ($config->get('db_autostart')) {
        $registry->set('db', new DB($config->get('db_engine'), $config->get('db_hostname'), $config->get('db_username'), $config->get('db_password'), $config->get('db_database'), $config->get('db_port')));
    }

    $loader->library('openpos');
    $openpos = new Openpos($registry);
    $custom_css = $openpos->getSetting('custom_css','pos',0);
    $custom_language = $openpos->getSetting('pos_language','pos',0);


    $site_url = $config->get('site_ssl') ? HTTPS_SERVER : HTTP_SERVER;

    $site_url = str_replace('/pos','',trim($site_url,'/'));
    $pos_verion =  $openpos->version();

    $config_data = $openpos->getLanaConfigData();
        
?>
<!doctype html>
<html lang="en" style="height: calc(100% - 0px);">
<head>
    <meta charset="utf-8">
    <title><?php echo $config_data['config_name'] ?> - POS</title>
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <link rel="manifest" href="<?php echo $site_url; ?>/pos/manifest.json.php?config_data=<?php echo base64_encode(json_encode($config_data)); ?>" />
    <base href="<?php echo $site_url.'/pos/'; ?>">

    <link rel='stylesheet' id='openpos.styles-css'  href='<?php echo $site_url; ?>/pos/styles.css?ver=<?php echo $pos_verion; ?>' type='text/css' media='all' />

    <link rel='stylesheet' id='openpos.front-css'  href='<?php echo $site_url; ?>/pos/pos.css?ver=<?php echo $pos_verion; ?>' type='text/css' media='all' />

    <script type="text/javascript">
        <?php if($custom_language && $custom_language != '_auto'): ?>
        var pos_lang = '<?php echo $custom_language; ?>';
        <?php endif; ?>
        var action_url = '<?php echo str_replace('/pos','',$url->link('extension/module/openpos','',true)); ?>';
        var global = global || window;
        global.action_url = action_url;
        var Buffer = Buffer || [];
        var process = process || {
            env: { DEBUG: undefined },
            version: []
        };
    </script>
    <style type="text/css">
        <?php echo $custom_css; ?>
    </style>
    <?php echo $openpos->getCustomJs(); ?>
</head>

<body style="width: 100%; height: 100%; overflow: hidden;">

<app-root>
    <style>

        body {
            background: #27223c;
            margin: 0;
            padding: 0;
        }

        .sk-circle {
            margin: 25% auto;
            width: 40px;
            height: 40px;
            position: relative;
        }
        .sk-circle .sk-child {
            width: 100%;
            height: 100%;
            position: absolute;
            left: 0;
            top: 0;
        }
        .sk-circle .sk-child:before {
            content: '';
            display: block;
            margin: 0 auto;
            width: 15%;
            height: 15%;
            background-color: #fff;
            border-radius: 100%;
            -webkit-animation: sk-circleBounceDelay 1.2s infinite ease-in-out both;
            animation: sk-circleBounceDelay 1.2s infinite ease-in-out both;
        }
        .sk-circle .sk-circle2 {
            -webkit-transform: rotate(30deg);
            -ms-transform: rotate(30deg);
            transform: rotate(30deg); }
        .sk-circle .sk-circle3 {
            -webkit-transform: rotate(60deg);
            -ms-transform: rotate(60deg);
            transform: rotate(60deg); }
        .sk-circle .sk-circle4 {
            -webkit-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            transform: rotate(90deg); }
        .sk-circle .sk-circle5 {
            -webkit-transform: rotate(120deg);
            -ms-transform: rotate(120deg);
            transform: rotate(120deg); }
        .sk-circle .sk-circle6 {
            -webkit-transform: rotate(150deg);
            -ms-transform: rotate(150deg);
            transform: rotate(150deg); }
        .sk-circle .sk-circle7 {
            -webkit-transform: rotate(180deg);
            -ms-transform: rotate(180deg);
            transform: rotate(180deg); }
        .sk-circle .sk-circle8 {
            -webkit-transform: rotate(210deg);
            -ms-transform: rotate(210deg);
            transform: rotate(210deg); }
        .sk-circle .sk-circle9 {
            -webkit-transform: rotate(240deg);
            -ms-transform: rotate(240deg);
            transform: rotate(240deg); }
        .sk-circle .sk-circle10 {
            -webkit-transform: rotate(270deg);
            -ms-transform: rotate(270deg);
            transform: rotate(270deg); }
        .sk-circle .sk-circle11 {
            -webkit-transform: rotate(300deg);
            -ms-transform: rotate(300deg);
            transform: rotate(300deg); }
        .sk-circle .sk-circle12 {
            -webkit-transform: rotate(330deg);
            -ms-transform: rotate(330deg);
            transform: rotate(330deg); }
        .sk-circle .sk-circle2:before {
            -webkit-animation-delay: -1.1s;
            animation-delay: -1.1s; }
        .sk-circle .sk-circle3:before {
            -webkit-animation-delay: -1s;
            animation-delay: -1s; }
        .sk-circle .sk-circle4:before {
            -webkit-animation-delay: -0.9s;
            animation-delay: -0.9s; }
        .sk-circle .sk-circle5:before {
            -webkit-animation-delay: -0.8s;
            animation-delay: -0.8s; }
        .sk-circle .sk-circle6:before {
            -webkit-animation-delay: -0.7s;
            animation-delay: -0.7s; }
        .sk-circle .sk-circle7:before {
            -webkit-animation-delay: -0.6s;
            animation-delay: -0.6s; }
        .sk-circle .sk-circle8:before {
            -webkit-animation-delay: -0.5s;
            animation-delay: -0.5s; }
        .sk-circle .sk-circle9:before {
            -webkit-animation-delay: -0.4s;
            animation-delay: -0.4s; }
        .sk-circle .sk-circle10:before {
            -webkit-animation-delay: -0.3s;
            animation-delay: -0.3s; }
        .sk-circle .sk-circle11:before {
            -webkit-animation-delay: -0.2s;
            animation-delay: -0.2s; }
        .sk-circle .sk-circle12:before {
            -webkit-animation-delay: -0.1s;
            animation-delay: -0.1s; }

        @-webkit-keyframes sk-circleBounceDelay {
            0%, 80%, 100% {
                -webkit-transform: scale(0);
                transform: scale(0);
            } 40% {
                  -webkit-transform: scale(1);
                  transform: scale(1);
              }
        }

        @keyframes sk-circleBounceDelay {
            0%, 80%, 100% {
                -webkit-transform: scale(0);
                transform: scale(0);
            } 40% {
                  -webkit-transform: scale(1);
                  transform: scale(1);
              }
        }
    </style>

    <div class="sk-circle">
        <div class="sk-circle1 sk-child"></div>
        <div class="sk-circle2 sk-child"></div>
        <div class="sk-circle3 sk-child"></div>
        <div class="sk-circle4 sk-child"></div>
        <div class="sk-circle5 sk-child"></div>
        <div class="sk-circle6 sk-child"></div>
        <div class="sk-circle7 sk-child"></div>
        <div class="sk-circle8 sk-child"></div>
        <div class="sk-circle9 sk-child"></div>
        <div class="sk-circle10 sk-child"></div>
        <div class="sk-circle11 sk-child"></div>
        <div class="sk-circle12 sk-child"></div>
    </div>
</app-root>



<script type='text/javascript' src='<?php echo $site_url; ?>/pos/runtime.js?ver=<?php echo $pos_verion; ?>'></script>
<script type='text/javascript' src='<?php echo $site_url; ?>/pos/polyfills.js?ver=<?php echo $pos_verion; ?>'></script>
<script type='text/javascript' src='<?php echo $site_url; ?>/pos/main.js?ver=<?php echo $pos_verion; ?>'></script>
<script type='text/javascript'>
                if ('serviceWorker' in navigator) {
                    console.log('register service worker');
                    navigator.serviceWorker.register('./service-worker.js?v=<?php echo $pos_verion; ?>').then(function(registration) {
                        console.log('ServiceWorker registration successful with scope:',  registration.scope);
                    }).catch(function(error) {
                        console.log('ServiceWorker registration failed:', error);
                    });
                }
            
</script>
</body>
</html>
