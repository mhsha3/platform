<?php
// Configuration
if (is_file('./../../global.php')) {
	require_once('./../../global.php');
}

define('HTTP_SERVER', 'https://' . str_replace('www.', '', $_SERVER['HTTP_HOST']) . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/');
define('HTTPS_SERVER', 'https://' . str_replace('www.', '', $_SERVER['HTTP_HOST']) . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/');
// DIR
define('DIR_APPLICATION', HOME_DIR . '/FRONTCORE/catalog/');
define('DIR_SYSTEM', HOME_DIR . '/FRONTCORE/system/');
define('DIR_SYSTEM_STORAGE', HOME_DIR . '/lnasa.space/dfsdfsd9/system/');
define('DIR_IMAGE', HOME_DIR . '/lnasa.space/dfsdfsd9/image/');
define('DIR_STORAGE', DIR_SYSTEM_STORAGE . 'storage/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/theme/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

define('EXPIRE_PACKAGE_PAGE',HOME_DIR . '/lnasa.space/expire.php');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', PLATFORM_DB_HOST);
define('DB_USERNAME', PLATFORM_DB_USER);
define('DB_PASSWORD', PLATFORM_DB_PASS);
define('DB_DATABASE', 'dbstore_dfsdfsd9_9BmY');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');