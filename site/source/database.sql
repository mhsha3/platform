-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 13, 2020 at 12:30 PM
-- Server version: 5.7.32
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbstore_dfsdfsd9_9BmY`
--

-- --------------------------------------------------------

--
-- Table structure for table `oc_address`
--

CREATE TABLE `oc_address` (
  `address_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `company` varchar(40) NOT NULL,
  `address_1` varchar(128) NOT NULL,
  `address_2` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `custom_field` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_address`
--

INSERT INTO `oc_address` (`address_id`, `customer_id`, `firstname`, `lastname`, `company`, `address_1`, `address_2`, `city`, `postcode`, `country_id`, `zone_id`, `custom_field`) VALUES
(1, 1, 'demo', 'demo', '', 'surat', '', 'surat', '', 99, 1485, ''),
(2, 2, 'TSFG', 'HJGHG', 'JGSJGH', 'JHCGH', 'ajdghj', 'surat', '4524252', 99, 1485, '[]'),
(3, 3, 'fggfg', 'fdf', '', 'surat', '', 'surat', '356006', 99, 1485, ''),
(4, 4, 'Webiarch', 'Infotech', '', 'Varachha', '', 'surat', '395006', 99, 1485, ''),
(5, 5, 'fsdf', 'dsfsdf', '', 'dsfsfd', '', 'CAIRO', '55555', 184, 2879, ''),
(6, 6, 'Imran', 'Haider', 'SheenSol Technologies (Pvt) Limited', '57, Nargis Block, Allama Iqbal Town', 'Near Nargis Block Market', 'Lahore', '54570', 162, 2461, '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_api`
--

CREATE TABLE `oc_api` (
  `api_id` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `key` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_api`
--

INSERT INTO `oc_api` (`api_id`, `username`, `key`, `status`, `date_added`, `date_modified`) VALUES
(5, 'Default', '9VMOKL7mHh9yFjBydtBj11xpKM6Ve7cxGTOcfrGUtLfIn149INr3PCjRYn1p3SejUh8rlIxzkdZqNCrSFVzUOdOGOj9b3Zr8MFKHQlLv6toKUKgLsjLbEYzkyz3iRtw9NWcG7IbPx1bni8YaITWresboRsqYRshLRxotyEY91cTLBfNxJow6UEgvOUhiosX7NMgkmX5he4Wih0FdH1sGFgUjVwbU0amPb5ejBHuaxOBgdBQd1JbiSfjPa7lJmcdf', 1, '2020-11-22 01:08:38', '2020-11-21 16:16:42'),
(6, 'mohab', 'IC8RQ7KbWmSQfxzJspOeCbQ1eEWyuRYI0JhvglEEE54aLTNriQvRinQeV1NBJnNxpzofIOoUi52gHj0ILUT4OuebBC0SmOR30a0GPnLNhW2oCsC5T3pwyzgPVDqdzX4EbXQjU27qeGB1TFbvx1riKrPn6Mlz6ieKYa5WUYPQu6nv4dBX6O8USs0tDIIak7ZrLDhqfj7yHde2ZgAgl46zjRwdYAmqsNJKIPD3fMKIZj2pF6NAJlvFPLGiMzO2Rm30', 1, '2020-11-29 09:18:05', '2020-11-29 09:18:05');

-- --------------------------------------------------------

--
-- Table structure for table `oc_api_ip`
--

CREATE TABLE `oc_api_ip` (
  `api_ip_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_api_ip`
--

INSERT INTO `oc_api_ip` (`api_ip_id`, `api_id`, `ip`) VALUES
(1, 1, '::1'),
(2, 4, '41.236.165.233'),
(3, 5, '41.236.165.233'),
(4, 5, '72.255.57.244'),
(5, 6, '72.255.57.244'),
(6, 6, '%.%.%.%'),
(7, 5, '119.152.146.139'),
(8, 5, '41.236.130.109'),
(9, 5, '102.44.11.123'),
(10, 5, '41.234.143.234'),
(11, 5, '176.224.255.237'),
(12, 5, '41.234.145.236'),
(13, 5, '41.237.205.191'),
(14, 5, '176.224.86.229'),
(15, 5, '41.236.153.25'),
(16, 5, '41.234.216.142'),
(17, 5, '41.234.226.212'),
(18, 5, '41.234.133.32'),
(19, 5, '41.237.220.100');

-- --------------------------------------------------------

--
-- Table structure for table `oc_api_session`
--

CREATE TABLE `oc_api_session` (
  `api_session_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_api_session`
--

INSERT INTO `oc_api_session` (`api_session_id`, `api_id`, `session_id`, `ip`, `date_added`, `date_modified`) VALUES
(165, 5, '0cfc0dc2b084f578a1da8675fc', '41.234.133.32', '2020-12-12 15:23:46', '2020-12-12 15:23:52'),
(164, 5, '0f03c1483f04f2414b1fcc4298', '41.234.133.32', '2020-12-12 15:23:34', '2020-12-12 15:23:34'),
(169, 5, 'a7ab38b0d79bec204584d3cd81', '41.237.220.100', '2020-12-13 12:26:28', '2020-12-13 12:26:28'),
(168, 5, 'd68c688d86b988409777dcdbf4', '41.237.220.100', '2020-12-13 12:20:16', '2020-12-13 12:20:16'),
(167, 5, '2713eabb1579a31d478f68c9f6', '41.234.133.32', '2020-12-12 15:24:02', '2020-12-12 15:24:02'),
(166, 5, '6894def385845a885010a5741c', '41.234.133.32', '2020-12-12 15:23:54', '2020-12-12 15:24:00');

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute`
--

CREATE TABLE `oc_attribute` (
  `attribute_id` int(11) NOT NULL,
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_attribute`
--

INSERT INTO `oc_attribute` (`attribute_id`, `attribute_group_id`, `sort_order`) VALUES
(1, 6, 1),
(2, 6, 5),
(3, 6, 3),
(4, 3, 1),
(5, 3, 2),
(6, 3, 3),
(7, 3, 4),
(8, 3, 5),
(9, 3, 6),
(10, 3, 7),
(11, 3, 8);

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute_description`
--

CREATE TABLE `oc_attribute_description` (
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_attribute_description`
--

INSERT INTO `oc_attribute_description` (`attribute_id`, `language_id`, `name`) VALUES
(1, 1, 'Description'),
(2, 1, 'No. of Cores'),
(4, 1, 'test 1'),
(5, 1, 'test 2'),
(6, 1, 'test 3'),
(7, 1, 'test 4'),
(8, 1, 'test 5'),
(9, 1, 'test 6'),
(10, 1, 'test 7'),
(11, 1, 'test 8'),
(3, 1, 'Clockspeed'),
(1, 2, 'Description'),
(2, 2, 'No. of Cores'),
(4, 2, 'test 1'),
(5, 2, 'test 2'),
(6, 2, 'test 3'),
(7, 2, 'test 4'),
(8, 2, 'test 5'),
(9, 2, 'test 6'),
(10, 2, 'test 7'),
(11, 2, 'test 8'),
(3, 2, 'Clockspeed');

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute_group`
--

CREATE TABLE `oc_attribute_group` (
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_attribute_group`
--

INSERT INTO `oc_attribute_group` (`attribute_group_id`, `sort_order`) VALUES
(3, 2),
(4, 1),
(5, 3),
(6, 4);

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute_group_description`
--

CREATE TABLE `oc_attribute_group_description` (
  `attribute_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_attribute_group_description`
--

INSERT INTO `oc_attribute_group_description` (`attribute_group_id`, `language_id`, `name`) VALUES
(3, 1, 'Memory'),
(4, 1, 'Technical'),
(5, 1, 'Motherboard'),
(6, 1, 'Processor'),
(3, 2, 'Memory'),
(4, 2, 'Technical'),
(5, 2, 'Motherboard'),
(6, 2, 'Processor');

-- --------------------------------------------------------

--
-- Table structure for table `oc_banner`
--

CREATE TABLE `oc_banner` (
  `banner_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_banner`
--

INSERT INTO `oc_banner` (`banner_id`, `name`, `status`) VALUES
(6, 'HP Products', 1),
(7, 'السلايدر الرئيسي', 1),
(8, 'الشركات', 1),
(9, 'Home page Sellbanner', 1),
(10, 'بنر العروض', 1),
(11, 'البنر الايسر الاول', 1),
(12, 'البنر الايسر الثاني', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_banner_image`
--

CREATE TABLE `oc_banner_image` (
  `banner_image_id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_banner_image`
--

INSERT INTO `oc_banner_image` (`banner_image_id`, `banner_id`, `language_id`, `title`, `link`, `image`, `sort_order`) VALUES
(87, 6, 1, 'HP Banner', 'index.php?route=product/manufacturer/info&amp;manufacturer_id=7', 'catalog/demo/compaq_presario.jpg', 0),
(262, 8, 2, 'Harley Davidson', '', 'catalog/daaea6afd9d0196014e6f1e5693d5dd20.21185200 1606862222.png', 0),
(261, 8, 2, 'Dell', '', 'catalog/daaea6afd9d0196014e6f1e5693d5dd20.21185200 1606862222.png', 0),
(260, 8, 1, 'Canon', '', 'catalog/daaea6afd9d0196014e6f1e5693d5dd20.21185200 1606862222.png', 0),
(100, 6, 2, 'HP Banner', 'index.php?route=product/manufacturer/info&amp;manufacturer_id=7', 'catalog/demo/compaq_presario.jpg', 0),
(259, 8, 1, 'Harley Davidson', '', 'catalog/daaea6afd9d0196014e6f1e5693d5dd20.21185200 1606862222.png', 0),
(258, 8, 1, 'Dell', '', 'catalog/daaea6afd9d0196014e6f1e5693d5dd20.21185200 1606862222.png', 0),
(257, 8, 1, 'Disney', '', 'catalog/daaea6afd9d0196014e6f1e5693d5dd20.21185200 1606862222.png', 0),
(256, 8, 1, 'Starbucks', '', 'catalog/daaea6afd9d0196014e6f1e5693d5dd20.21185200 1606862222.png', 0),
(276, 7, 2, 'السلايد الاول', '#', 'catalog/0b0f2b148a1f05f40f1d110015df3bb10.23765200 1606959618.png', 2),
(199, 9, 1, 'sellbanner3', '#', 'catalog/sellbanner/sellbanner.jpg', 0),
(198, 9, 1, 'sellbanner2', '#', 'catalog/sellbanner/sellbanner.jpg', 0),
(197, 9, 1, 'sellbanner1', '#', 'catalog/sellbanner/sellbanner.jpg', 0),
(267, 10, 1, 'offerbanner', '#', 'catalog/offerbanner/ec676cc9bc50505180c17d80221beaf20.30053000 1606862543.png', 0),
(200, 9, 2, 'sellbanner3', '#', 'catalog/sellbanner/sellbanner.jpg', 0),
(201, 9, 2, 'sellbanner2', '#', 'catalog/sellbanner/sellbanner.jpg', 0),
(202, 9, 2, 'sellbanner1', '#', 'catalog/sellbanner/sellbanner.jpg', 0),
(275, 7, 1, 'السلايد الثالث', '', 'catalog/65d5f5c5e73c25400cf610e32de882f20.78954800 1606959583.png', 1),
(251, 11, 1, 'البنر الايسر ', '#', 'catalog/leftbanner1/8add3a145092eaf2cffacd96322624a00.55116500 1606861354.png', 0),
(253, 12, 1, 'البنر الايسر الثاني', '#', 'catalog/leftbanner1/0183f6981b515c61241d13bc0a49e87e0.96414300 1606861564.png', 0),
(255, 8, 1, 'Nintendo', '', 'catalog/daaea6afd9d0196014e6f1e5693d5dd20.21185200 1606862222.png', 0),
(273, 7, 1, 'السلايد الاول', '#', 'catalog/d5885e7f0710fb55aebf1b47606777290.17285500 1606959652.png', 3),
(274, 7, 1, 'السلايد الثاني', '#', 'catalog/0b0f2b148a1f05f40f1d110015df3bb10.23765200 1606959618.png', 2),
(252, 11, 2, 'البنر الايسر', '#', 'catalog/leftbanner1/8add3a145092eaf2cffacd96322624a00.55116500 1606861354.png', 0),
(254, 12, 2, 'البنر الايسر الثاني', '#', 'catalog/leftbanner1/0183f6981b515c61241d13bc0a49e87e0.96414300 1606861564.png', 0),
(263, 8, 2, 'Canon', '', 'catalog/daaea6afd9d0196014e6f1e5693d5dd20.21185200 1606862222.png', 0),
(264, 8, 2, 'Burger King', '', 'catalog/daaea6afd9d0196014e6f1e5693d5dd20.21185200 1606862222.png', 0),
(265, 8, 2, 'Sony', '', 'catalog/daaea6afd9d0196014e6f1e5693d5dd20.21185200 1606862222.png', 0),
(266, 8, 2, 'Coca Cola', '', 'catalog/daaea6afd9d0196014e6f1e5693d5dd20.21185200 1606862222.png', 0),
(268, 10, 2, 'offerbanner', '#', 'catalog/offerbanner/ec676cc9bc50505180c17d80221beaf20.30053000 1606862543.png', 0),
(277, 7, 2, 'السلايد الثاني', '#', 'catalog/65d5f5c5e73c25400cf610e32de882f20.78954800 1606959583.png', 1),
(278, 7, 2, 'السلايد الثالث', '', 'catalog/d5885e7f0710fb55aebf1b47606777290.17285500 1606959652.png', 3);

-- --------------------------------------------------------

--
-- Table structure for table `oc_blogger`
--

CREATE TABLE `oc_blogger` (
  `blogger_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `image` varchar(255) DEFAULT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_blogger`
--

INSERT INTO `oc_blogger` (`blogger_id`, `module_id`, `status`, `image`, `date_added`, `date_modified`) VALUES
(5, 38, 1, 'catalog/blog/1.jpg', '2017-09-18 15:30:51', '2017-09-19 13:12:44'),
(4, 38, 1, 'catalog/blog/1.jpg', '2017-09-18 15:30:13', '2017-09-19 13:12:34'),
(6, 38, 1, 'catalog/blog/1.jpg', '2017-09-18 15:31:47', '2017-09-19 13:12:55');

-- --------------------------------------------------------

--
-- Table structure for table `oc_blogger_comment`
--

CREATE TABLE `oc_blogger_comment` (
  `blogger_comment_id` int(11) NOT NULL,
  `blogger_id` int(11) NOT NULL,
  `approved` int(1) NOT NULL DEFAULT '0',
  `author` varchar(64) NOT NULL DEFAULT '',
  `email` varchar(96) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_blogger_comment`
--

INSERT INTO `oc_blogger_comment` (`blogger_comment_id`, `blogger_id`, `approved`, `author`, `email`, `date_added`) VALUES
(1, 6, 1, 'webiarch', 'webiarchinfotech@gmail.com', '2017-09-18 15:40:55'),
(2, 6, 1, 'webiarch', 'webiarchinfotech@gmail.com', '2017-09-18 15:41:11');

-- --------------------------------------------------------

--
-- Table structure for table `oc_blogger_comment_description`
--

CREATE TABLE `oc_blogger_comment_description` (
  `blogger_comment_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `comment` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_blogger_comment_description`
--

INSERT INTO `oc_blogger_comment_description` (`blogger_comment_id`, `language_id`, `comment`) VALUES
(1, 1, '                                        e diam dignissim feugiat. Curabitur vitae metus augue. Mauris eget massa nec sapien tempor aliquet. Nam fermentum sed urna et varius. Fusce ultricies sagittis ultrices. Phasellus at dignissim turpis, non scelerisque tellus. Donec auctor condimentum por'),
(2, 1, '                                        e diam dignissim feugiat. Curabitur vitae metus augue. Mauris eget massa nec sapien tempor aliquet. Nam fermentum sed urna et varius. Fusce ultricies sagittis ultrices. Phasellus at dignissim turpis, non scelerisque tellus. Donec auctor condimentum por');

-- --------------------------------------------------------

--
-- Table structure for table `oc_blogger_description`
--

CREATE TABLE `oc_blogger_description` (
  `blogger_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(64) NOT NULL DEFAULT '',
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_blogger_description`
--

INSERT INTO `oc_blogger_description` (`blogger_id`, `language_id`, `title`, `description`) VALUES
(5, 1, 'Etiam lacinia venenatis dolor', 'Sed facilisis tristique eros, ultrices convallis tellus efficitur at. Ut egestas augue vulputate dui imperdiet, venenatis rutrum odio ornare. Nunc vestibulum iaculis urna non maximus. Praesent eget velit mollis, porttitor sem nec, ornare orci. Nulla tempor ac leo vel faucibus. Aenean dui felis, feugiat ut consequat ac, fringilla nec quam. Aenean consequat lacus rhoncus interdum scelerisque. Cras blandit pharetra diam, viverra tristique ligula molestie at. Nulla fermentum faucibus ante, eget euismod augue fermentum sed. Suspendisse pharetra velit id elementum maximus. Ut efficitur id mi eu laoreet. Etiam vel lectus tincidunt, molestie ante non, ultricies turpis. Curabitur arcu quam, ornare eget pulvinar ut, tincidunt quis mi. '),
(4, 1, 'Lorem ipsum dolor sit amet', 'Nullam a eros lectus. Cras non enim velit. Nunc fringilla nulla non dui pellentesque, quis volutpat tellus faucibus. Aenean pulvinar elit eu euismod tincidunt. Sed rhoncus egestas augue, ac consectetur purus maximus vitae. Nunc vestibulum semper lectus, sed vulputate mauris aliquam vitae. Sed convallis condimentum leo, ut sodales est blandit sed. Aliquam facilisis iaculis ligula, eu auctor velit rhoncus non. Donec tincidunt ultrices quam sed elementum. Donec at pretium ante.'),
(5, 2, 'Etiam lacinia venenatis dolor', 'Sed facilisis tristique eros, ultrices convallis tellus efficitur at. Ut egestas augue vulputate dui imperdiet, venenatis rutrum odio ornare. Nunc vestibulum iaculis urna non maximus. Praesent eget velit mollis, porttitor sem nec, ornare orci. Nulla tempor ac leo vel faucibus. Aenean dui felis, feugiat ut consequat ac, fringilla nec quam. Aenean consequat lacus rhoncus interdum scelerisque. Cras blandit pharetra diam, viverra tristique ligula molestie at. Nulla fermentum faucibus ante, eget euismod augue fermentum sed. Suspendisse pharetra velit id elementum maximus. Ut efficitur id mi eu laoreet. Etiam vel lectus tincidunt, molestie ante non, ultricies turpis. Curabitur arcu quam, ornare eget pulvinar ut, tincidunt quis mi. '),
(4, 2, 'Lorem ipsum dolor sit amet', 'Nullam a eros lectus. Cras non enim velit. Nunc fringilla nulla non dui pellentesque, quis volutpat tellus faucibus. Aenean pulvinar elit eu euismod tincidunt. Sed rhoncus egestas augue, ac consectetur purus maximus vitae. Nunc vestibulum semper lectus, sed vulputate mauris aliquam vitae. Sed convallis condimentum leo, ut sodales est blandit sed. Aliquam facilisis iaculis ligula, eu auctor velit rhoncus non. Donec tincidunt ultrices quam sed elementum. Donec at pretium ante.'),
(6, 2, 'Fusce maximus justo est', 'Sed consectetur nisl quis felis egestas, non fringilla lectus eleifend. Ut ac erat in enim interdum facilisis. Aliquam a libero sem. Sed et lorem pellentesque, consectetur tortor vel, laoreet neque. Nulla aliquam posuere lectus fringilla semper. Integer finibus diam vitae diam dignissim feugiat. Curabitur vitae metus augue. Mauris eget massa nec sapien tempor aliquet. Nam fermentum sed urna et varius. Fusce ultricies sagittis ultrices. Phasellus at dignissim turpis, non scelerisque tellus. Donec auctor condimentum porta. '),
(6, 1, 'Fusce maximus justo est', 'Sed consectetur nisl quis felis egestas, non fringilla lectus eleifend. Ut ac erat in enim interdum facilisis. Aliquam a libero sem. Sed et lorem pellentesque, consectetur tortor vel, laoreet neque. Nulla aliquam posuere lectus fringilla semper. Integer finibus diam vitae diam dignissim feugiat. Curabitur vitae metus augue. Mauris eget massa nec sapien tempor aliquet. Nam fermentum sed urna et varius. Fusce ultricies sagittis ultrices. Phasellus at dignissim turpis, non scelerisque tellus. Donec auctor condimentum porta. ');

-- --------------------------------------------------------

--
-- Table structure for table `oc_cart`
--

CREATE TABLE `oc_cart` (
  `cart_id` int(11) UNSIGNED NOT NULL,
  `api_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `option` text NOT NULL,
  `quantity` int(5) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_cart`
--

INSERT INTO `oc_cart` (`cart_id`, `api_id`, `customer_id`, `session_id`, `product_id`, `recurring_id`, `option`, `quantity`, `date_added`) VALUES
(3, 0, 2, '4aab623e382ad1e4275d76d471', 30, 0, '{\"226\":\"15\"}', 1, '2017-09-14 10:51:32'),
(4, 0, 2, '4aab623e382ad1e4275d76d471', 43, 0, '[]', 1, '2017-09-14 11:46:32'),
(9, 0, 3, '376144983739f7259d3d9dd692', 43, 0, '[]', 1, '2017-09-17 11:37:11'),
(10, 0, 3, '376144983739f7259d3d9dd692', 49, 0, '[]', 2, '2017-09-17 13:53:37'),
(22, 0, 5, '640e811678d259763d1d752fae', 50, 0, '[]', 1, '2020-11-22 19:44:42');

-- --------------------------------------------------------

--
-- Table structure for table `oc_category`
--

CREATE TABLE `oc_category` (
  `category_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category`
--

INSERT INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(25, '', 0, 1, 2, 3, 1, '2009-01-31 01:04:25', '2020-12-01 15:04:43'),
(20, 'catalog/demo/gift-voucher-birthday.jpg', 0, 1, 3, 1, 1, '2009-01-05 21:49:43', '2020-12-01 15:04:59'),
(24, '', 0, 1, 1, 5, 1, '2009-01-20 02:36:26', '2020-12-01 15:07:16'),
(17, '', 0, 1, 1, 4, 1, '2009-01-03 21:08:57', '2020-12-01 15:07:26'),
(33, '', 0, 1, 1, 6, 1, '2009-02-03 14:17:55', '2020-12-04 18:17:12'),
(34, 'catalog/demo/ipod_touch_4.jpg', 0, 1, 3, 7, 1, '2009-02-03 14:18:11', '2020-12-01 15:07:03'),
(57, '', 0, 1, 1, 3, 1, '2011-04-26 08:53:16', '2020-12-01 15:07:36'),
(60, '', 0, 1, 1, 0, 1, '2017-09-13 19:03:56', '2020-12-01 15:07:44'),
(61, '', 0, 1, 1, 0, 1, '2017-09-13 19:04:52', '2020-12-01 15:05:28');

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_description`
--

CREATE TABLE `oc_category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category_description`
--

INSERT INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(33, 1, 'Cameras', '', 'Cameras', '', ''),
(33, 2, 'كاميرا', '', 'Cameras', '', ''),
(17, 2, 'مثال لقسم	', '', 'Software', '', ''),
(25, 1, 'Components', '', 'Components', '', ''),
(24, 2, 'مثال لقسم	', '', 'Phones &amp; PDAs', '', ''),
(20, 1, 'Desktops', '&lt;p&gt;\r\n	Example of category description text&lt;/p&gt;\r\n', 'Desktops', 'Example of category description', ''),
(34, 1, 'MP3 Players', '&lt;p&gt;\r\n	Shop Laptop feature only the best laptop deals on the market. By comparing laptop deals from the likes of PC World, Comet, Dixons, The Link and Carphone Warehouse, Shop Laptop has the most comprehensive selection of laptops on the internet. At Shop Laptop, we pride ourselves on offering customers the very best laptop deals. From refurbished laptops to netbooks, Shop Laptop ensures that every laptop - in every colour, style, size and technical spec - is featured on the site at the lowest possible price.&lt;/p&gt;\r\n', 'MP3 Players', '', ''),
(34, 2, 'مثال لقسم	', '&lt;p&gt;\r\n	Shop Laptop feature only the best laptop deals on the market. By comparing laptop deals from the likes of PC World, Comet, Dixons, The Link and Carphone Warehouse, Shop Laptop has the most comprehensive selection of laptops on the internet. At Shop Laptop, we pride ourselves on offering customers the very best laptop deals. From refurbished laptops to netbooks, Shop Laptop ensures that every laptop - in every colour, style, size and technical spec - is featured on the site at the lowest possible price.&lt;/p&gt;\r\n', 'MP3 Players', '', ''),
(57, 2, 'مثال لقسم	', '', 'Tablets', '', ''),
(17, 1, 'Software', '', 'Software', '', ''),
(25, 2, 'مثال لقسم', '', 'Components', '', ''),
(24, 1, 'Phones &amp; PDAs', '', 'Phones &amp; PDAs', '', ''),
(20, 2, 'مثال لقسم', '&lt;p&gt;\r\n	Example of category description text&lt;/p&gt;\r\n', 'Desktops', 'Example of category description', ''),
(61, 2, 'مثال لقسم', '', 'man', '', ''),
(57, 1, 'Tablets', '', 'Tablets', '', ''),
(60, 2, 'مثال لقسم	', '', 'watch', '', ''),
(60, 1, 'watch', '', 'watch', '', ''),
(61, 1, 'man', '', 'man', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_filter`
--

CREATE TABLE `oc_category_filter` (
  `category_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category_filter`
--

INSERT INTO `oc_category_filter` (`category_id`, `filter_id`) VALUES
(20, 1),
(20, 2),
(20, 3);

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_path`
--

CREATE TABLE `oc_category_path` (
  `category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category_path`
--

INSERT INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(25, 25, 0),
(20, 20, 0),
(24, 24, 0),
(17, 17, 0),
(33, 33, 0),
(34, 34, 0),
(57, 57, 0),
(60, 60, 0),
(61, 61, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_to_layout`
--

CREATE TABLE `oc_category_to_layout` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category_to_layout`
--

INSERT INTO `oc_category_to_layout` (`category_id`, `store_id`, `layout_id`) VALUES
(57, 0, 0),
(60, 0, 0),
(61, 0, 0),
(20, 0, 0),
(34, 0, 0),
(24, 0, 0),
(17, 0, 0),
(25, 0, 0),
(33, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_to_store`
--

CREATE TABLE `oc_category_to_store` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category_to_store`
--

INSERT INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(17, 0),
(20, 0),
(24, 0),
(25, 0),
(33, 0),
(34, 0),
(57, 0),
(60, 0),
(61, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_country`
--

CREATE TABLE `oc_country` (
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `iso_code_2` varchar(2) NOT NULL,
  `iso_code_3` varchar(3) NOT NULL,
  `address_format` text NOT NULL,
  `postcode_required` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_country`
--

INSERT INTO `oc_country` (`country_id`, `name`, `iso_code_2`, `iso_code_3`, `address_format`, `postcode_required`, `status`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', '', 0, 1),
(2, 'Albania', 'AL', 'ALB', '', 0, 1),
(3, 'Algeria', 'DZ', 'DZA', '', 0, 1),
(4, 'American Samoa', 'AS', 'ASM', '', 0, 1),
(5, 'Andorra', 'AD', 'AND', '', 0, 1),
(6, 'Angola', 'AO', 'AGO', '', 0, 1),
(7, 'Anguilla', 'AI', 'AIA', '', 0, 1),
(8, 'Antarctica', 'AQ', 'ATA', '', 0, 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', '', 0, 1),
(10, 'Argentina', 'AR', 'ARG', '', 0, 1),
(11, 'Armenia', 'AM', 'ARM', '', 0, 1),
(12, 'Aruba', 'AW', 'ABW', '', 0, 1),
(13, 'Australia', 'AU', 'AUS', '', 0, 1),
(14, 'Austria', 'AT', 'AUT', '', 0, 1),
(15, 'Azerbaijan', 'AZ', 'AZE', '', 0, 1),
(16, 'Bahamas', 'BS', 'BHS', '', 0, 1),
(17, 'Bahrain', 'BH', 'BHR', '', 0, 1),
(18, 'Bangladesh', 'BD', 'BGD', '', 0, 1),
(19, 'Barbados', 'BB', 'BRB', '', 0, 1),
(20, 'Belarus', 'BY', 'BLR', '', 0, 1),
(21, 'Belgium', 'BE', 'BEL', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 0, 1),
(22, 'Belize', 'BZ', 'BLZ', '', 0, 1),
(23, 'Benin', 'BJ', 'BEN', '', 0, 1),
(24, 'Bermuda', 'BM', 'BMU', '', 0, 1),
(25, 'Bhutan', 'BT', 'BTN', '', 0, 1),
(26, 'Bolivia', 'BO', 'BOL', '', 0, 1),
(27, 'Bosnia and Herzegovina', 'BA', 'BIH', '', 0, 1),
(28, 'Botswana', 'BW', 'BWA', '', 0, 1),
(29, 'Bouvet Island', 'BV', 'BVT', '', 0, 1),
(30, 'Brazil', 'BR', 'BRA', '', 0, 1),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', '', 0, 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', '', 0, 1),
(33, 'Bulgaria', 'BG', 'BGR', '', 0, 1),
(34, 'Burkina Faso', 'BF', 'BFA', '', 0, 1),
(35, 'Burundi', 'BI', 'BDI', '', 0, 1),
(36, 'Cambodia', 'KH', 'KHM', '', 0, 1),
(37, 'Cameroon', 'CM', 'CMR', '', 0, 1),
(38, 'Canada', 'CA', 'CAN', '', 0, 1),
(39, 'Cape Verde', 'CV', 'CPV', '', 0, 1),
(40, 'Cayman Islands', 'KY', 'CYM', '', 0, 1),
(41, 'Central African Republic', 'CF', 'CAF', '', 0, 1),
(42, 'Chad', 'TD', 'TCD', '', 0, 1),
(43, 'Chile', 'CL', 'CHL', '', 0, 1),
(44, 'China', 'CN', 'CHN', '', 0, 1),
(45, 'Christmas Island', 'CX', 'CXR', '', 0, 1),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', '', 0, 1),
(47, 'Colombia', 'CO', 'COL', '', 0, 1),
(48, 'Comoros', 'KM', 'COM', '', 0, 1),
(49, 'Congo', 'CG', 'COG', '', 0, 1),
(50, 'Cook Islands', 'CK', 'COK', '', 0, 1),
(51, 'Costa Rica', 'CR', 'CRI', '', 0, 1),
(52, 'Cote D\'Ivoire', 'CI', 'CIV', '', 0, 1),
(53, 'Croatia', 'HR', 'HRV', '', 0, 1),
(54, 'Cuba', 'CU', 'CUB', '', 0, 1),
(55, 'Cyprus', 'CY', 'CYP', '', 0, 1),
(56, 'Czech Republic', 'CZ', 'CZE', '', 0, 1),
(57, 'Denmark', 'DK', 'DNK', '', 0, 1),
(58, 'Djibouti', 'DJ', 'DJI', '', 0, 1),
(59, 'Dominica', 'DM', 'DMA', '', 0, 1),
(60, 'Dominican Republic', 'DO', 'DOM', '', 0, 1),
(61, 'East Timor', 'TL', 'TLS', '', 0, 1),
(62, 'Ecuador', 'EC', 'ECU', '', 0, 1),
(63, 'Egypt', 'EG', 'EGY', '', 0, 1),
(64, 'El Salvador', 'SV', 'SLV', '', 0, 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', '', 0, 1),
(66, 'Eritrea', 'ER', 'ERI', '', 0, 1),
(67, 'Estonia', 'EE', 'EST', '', 0, 1),
(68, 'Ethiopia', 'ET', 'ETH', '', 0, 1),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', '', 0, 1),
(70, 'Faroe Islands', 'FO', 'FRO', '', 0, 1),
(71, 'Fiji', 'FJ', 'FJI', '', 0, 1),
(72, 'Finland', 'FI', 'FIN', '', 0, 1),
(74, 'France, Metropolitan', 'FR', 'FRA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(75, 'French Guiana', 'GF', 'GUF', '', 0, 1),
(76, 'French Polynesia', 'PF', 'PYF', '', 0, 1),
(77, 'French Southern Territories', 'TF', 'ATF', '', 0, 1),
(78, 'Gabon', 'GA', 'GAB', '', 0, 1),
(79, 'Gambia', 'GM', 'GMB', '', 0, 1),
(80, 'Georgia', 'GE', 'GEO', '', 0, 1),
(81, 'Germany', 'DE', 'DEU', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(82, 'Ghana', 'GH', 'GHA', '', 0, 1),
(83, 'Gibraltar', 'GI', 'GIB', '', 0, 1),
(84, 'Greece', 'GR', 'GRC', '', 0, 1),
(85, 'Greenland', 'GL', 'GRL', '', 0, 1),
(86, 'Grenada', 'GD', 'GRD', '', 0, 1),
(87, 'Guadeloupe', 'GP', 'GLP', '', 0, 1),
(88, 'Guam', 'GU', 'GUM', '', 0, 1),
(89, 'Guatemala', 'GT', 'GTM', '', 0, 1),
(90, 'Guinea', 'GN', 'GIN', '', 0, 1),
(91, 'Guinea-Bissau', 'GW', 'GNB', '', 0, 1),
(92, 'Guyana', 'GY', 'GUY', '', 0, 1),
(93, 'Haiti', 'HT', 'HTI', '', 0, 1),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', '', 0, 1),
(95, 'Honduras', 'HN', 'HND', '', 0, 1),
(96, 'Hong Kong', 'HK', 'HKG', '', 0, 1),
(97, 'Hungary', 'HU', 'HUN', '', 0, 1),
(98, 'Iceland', 'IS', 'ISL', '', 0, 1),
(99, 'India', 'IN', 'IND', '', 0, 1),
(100, 'Indonesia', 'ID', 'IDN', '', 0, 1),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', '', 0, 1),
(102, 'Iraq', 'IQ', 'IRQ', '', 0, 1),
(103, 'Ireland', 'IE', 'IRL', '', 0, 1),
(104, 'Israel', 'IL', 'ISR', '', 0, 1),
(105, 'Italy', 'IT', 'ITA', '', 0, 1),
(106, 'Jamaica', 'JM', 'JAM', '', 0, 1),
(107, 'Japan', 'JP', 'JPN', '', 0, 1),
(108, 'Jordan', 'JO', 'JOR', '', 0, 1),
(109, 'Kazakhstan', 'KZ', 'KAZ', '', 0, 1),
(110, 'Kenya', 'KE', 'KEN', '', 0, 1),
(111, 'Kiribati', 'KI', 'KIR', '', 0, 1),
(112, 'North Korea', 'KP', 'PRK', '', 0, 1),
(113, 'South Korea', 'KR', 'KOR', '', 0, 1),
(114, 'Kuwait', 'KW', 'KWT', '', 0, 1),
(115, 'Kyrgyzstan', 'KG', 'KGZ', '', 0, 1),
(116, 'Lao People\'s Democratic Republic', 'LA', 'LAO', '', 0, 1),
(117, 'Latvia', 'LV', 'LVA', '', 0, 1),
(118, 'Lebanon', 'LB', 'LBN', '', 0, 1),
(119, 'Lesotho', 'LS', 'LSO', '', 0, 1),
(120, 'Liberia', 'LR', 'LBR', '', 0, 1),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', '', 0, 1),
(122, 'Liechtenstein', 'LI', 'LIE', '', 0, 1),
(123, 'Lithuania', 'LT', 'LTU', '', 0, 1),
(124, 'Luxembourg', 'LU', 'LUX', '', 0, 1),
(125, 'Macau', 'MO', 'MAC', '', 0, 1),
(126, 'FYROM', 'MK', 'MKD', '', 0, 1),
(127, 'Madagascar', 'MG', 'MDG', '', 0, 1),
(128, 'Malawi', 'MW', 'MWI', '', 0, 1),
(129, 'Malaysia', 'MY', 'MYS', '', 0, 1),
(130, 'Maldives', 'MV', 'MDV', '', 0, 1),
(131, 'Mali', 'ML', 'MLI', '', 0, 1),
(132, 'Malta', 'MT', 'MLT', '', 0, 1),
(133, 'Marshall Islands', 'MH', 'MHL', '', 0, 1),
(134, 'Martinique', 'MQ', 'MTQ', '', 0, 1),
(135, 'Mauritania', 'MR', 'MRT', '', 0, 1),
(136, 'Mauritius', 'MU', 'MUS', '', 0, 1),
(137, 'Mayotte', 'YT', 'MYT', '', 0, 1),
(138, 'Mexico', 'MX', 'MEX', '', 0, 1),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', '', 0, 1),
(140, 'Moldova, Republic of', 'MD', 'MDA', '', 0, 1),
(141, 'Monaco', 'MC', 'MCO', '', 0, 1),
(142, 'Mongolia', 'MN', 'MNG', '', 0, 1),
(143, 'Montserrat', 'MS', 'MSR', '', 0, 1),
(144, 'Morocco', 'MA', 'MAR', '', 0, 1),
(145, 'Mozambique', 'MZ', 'MOZ', '', 0, 1),
(146, 'Myanmar', 'MM', 'MMR', '', 0, 1),
(147, 'Namibia', 'NA', 'NAM', '', 0, 1),
(148, 'Nauru', 'NR', 'NRU', '', 0, 1),
(149, 'Nepal', 'NP', 'NPL', '', 0, 1),
(150, 'Netherlands', 'NL', 'NLD', '', 0, 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', '', 0, 1),
(152, 'New Caledonia', 'NC', 'NCL', '', 0, 1),
(153, 'New Zealand', 'NZ', 'NZL', '', 0, 1),
(154, 'Nicaragua', 'NI', 'NIC', '', 0, 1),
(155, 'Niger', 'NE', 'NER', '', 0, 1),
(156, 'Nigeria', 'NG', 'NGA', '', 0, 1),
(157, 'Niue', 'NU', 'NIU', '', 0, 1),
(158, 'Norfolk Island', 'NF', 'NFK', '', 0, 1),
(159, 'Northern Mariana Islands', 'MP', 'MNP', '', 0, 1),
(160, 'Norway', 'NO', 'NOR', '', 0, 1),
(161, 'Oman', 'OM', 'OMN', '', 0, 1),
(162, 'Pakistan', 'PK', 'PAK', '', 0, 1),
(163, 'Palau', 'PW', 'PLW', '', 0, 1),
(164, 'Panama', 'PA', 'PAN', '', 0, 1),
(165, 'Papua New Guinea', 'PG', 'PNG', '', 0, 1),
(166, 'Paraguay', 'PY', 'PRY', '', 0, 1),
(167, 'Peru', 'PE', 'PER', '', 0, 1),
(168, 'Philippines', 'PH', 'PHL', '', 0, 1),
(169, 'Pitcairn', 'PN', 'PCN', '', 0, 1),
(170, 'Poland', 'PL', 'POL', '', 0, 1),
(171, 'Portugal', 'PT', 'PRT', '', 0, 1),
(172, 'Puerto Rico', 'PR', 'PRI', '', 0, 1),
(173, 'Qatar', 'QA', 'QAT', '', 0, 1),
(174, 'Reunion', 'RE', 'REU', '', 0, 1),
(175, 'Romania', 'RO', 'ROM', '', 0, 1),
(176, 'Russian Federation', 'RU', 'RUS', '', 0, 1),
(177, 'Rwanda', 'RW', 'RWA', '', 0, 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', '', 0, 1),
(179, 'Saint Lucia', 'LC', 'LCA', '', 0, 1),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '', 0, 1),
(181, 'Samoa', 'WS', 'WSM', '', 0, 1),
(182, 'San Marino', 'SM', 'SMR', '', 0, 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', '', 0, 1),
(184, 'Saudi Arabia', 'SA', 'SAU', '', 0, 1),
(185, 'Senegal', 'SN', 'SEN', '', 0, 1),
(186, 'Seychelles', 'SC', 'SYC', '', 0, 1),
(187, 'Sierra Leone', 'SL', 'SLE', '', 0, 1),
(188, 'Singapore', 'SG', 'SGP', '', 0, 1),
(189, 'Slovak Republic', 'SK', 'SVK', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city} {postcode}\r\n{zone}\r\n{country}', 0, 1),
(190, 'Slovenia', 'SI', 'SVN', '', 0, 1),
(191, 'Solomon Islands', 'SB', 'SLB', '', 0, 1),
(192, 'Somalia', 'SO', 'SOM', '', 0, 1),
(193, 'South Africa', 'ZA', 'ZAF', '', 0, 1),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS', 'SGS', '', 0, 1),
(195, 'Spain', 'ES', 'ESP', '', 0, 1),
(196, 'Sri Lanka', 'LK', 'LKA', '', 0, 1),
(197, 'St. Helena', 'SH', 'SHN', '', 0, 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', '', 0, 1),
(199, 'Sudan', 'SD', 'SDN', '', 0, 1),
(200, 'Suriname', 'SR', 'SUR', '', 0, 1),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', '', 0, 1),
(202, 'Swaziland', 'SZ', 'SWZ', '', 0, 1),
(203, 'Sweden', 'SE', 'SWE', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(204, 'Switzerland', 'CH', 'CHE', '', 0, 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', '', 0, 1),
(206, 'Taiwan', 'TW', 'TWN', '', 0, 1),
(207, 'Tajikistan', 'TJ', 'TJK', '', 0, 1),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', '', 0, 1),
(209, 'Thailand', 'TH', 'THA', '', 0, 1),
(210, 'Togo', 'TG', 'TGO', '', 0, 1),
(211, 'Tokelau', 'TK', 'TKL', '', 0, 1),
(212, 'Tonga', 'TO', 'TON', '', 0, 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', '', 0, 1),
(214, 'Tunisia', 'TN', 'TUN', '', 0, 1),
(215, 'Turkey', 'TR', 'TUR', '', 0, 1),
(216, 'Turkmenistan', 'TM', 'TKM', '', 0, 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', '', 0, 1),
(218, 'Tuvalu', 'TV', 'TUV', '', 0, 1),
(219, 'Uganda', 'UG', 'UGA', '', 0, 1),
(220, 'Ukraine', 'UA', 'UKR', '', 0, 1),
(221, 'United Arab Emirates', 'AE', 'ARE', '', 0, 1),
(222, 'United Kingdom', 'GB', 'GBR', '', 1, 1),
(223, 'United States', 'US', 'USA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}', 0, 1),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', '', 0, 1),
(225, 'Uruguay', 'UY', 'URY', '', 0, 1),
(226, 'Uzbekistan', 'UZ', 'UZB', '', 0, 1),
(227, 'Vanuatu', 'VU', 'VUT', '', 0, 1),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', '', 0, 1),
(229, 'Venezuela', 'VE', 'VEN', '', 0, 1),
(230, 'Viet Nam', 'VN', 'VNM', '', 0, 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', '', 0, 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', '', 0, 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', '', 0, 1),
(234, 'Western Sahara', 'EH', 'ESH', '', 0, 1),
(235, 'Yemen', 'YE', 'YEM', '', 0, 1),
(237, 'Democratic Republic of Congo', 'CD', 'COD', '', 0, 1),
(238, 'Zambia', 'ZM', 'ZMB', '', 0, 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', '', 0, 1),
(242, 'Montenegro', 'ME', 'MNE', '', 0, 1),
(243, 'Serbia', 'RS', 'SRB', '', 0, 1),
(244, 'Aaland Islands', 'AX', 'ALA', '', 0, 1),
(245, 'Bonaire, Sint Eustatius and Saba', 'BQ', 'BES', '', 0, 1),
(246, 'Curacao', 'CW', 'CUW', '', 0, 1),
(247, 'Palestinian Territory, Occupied', 'PS', 'PSE', '', 0, 1),
(248, 'South Sudan', 'SS', 'SSD', '', 0, 1),
(249, 'St. Barthelemy', 'BL', 'BLM', '', 0, 1),
(250, 'St. Martin (French part)', 'MF', 'MAF', '', 0, 1),
(251, 'Canary Islands', 'IC', 'ICA', '', 0, 1),
(252, 'Ascension Island (British)', 'AC', 'ASC', '', 0, 1),
(253, 'Kosovo, Republic of', 'XK', 'UNK', '', 0, 1),
(254, 'Isle of Man', 'IM', 'IMN', '', 0, 1),
(255, 'Tristan da Cunha', 'TA', 'SHN', '', 0, 1),
(256, 'Guernsey', 'GG', 'GGY', '', 0, 1),
(257, 'Jersey', 'JE', 'JEY', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon`
--

CREATE TABLE `oc_coupon` (
  `coupon_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(20) NOT NULL,
  `type` char(1) NOT NULL,
  `discount` decimal(15,4) NOT NULL,
  `logged` tinyint(1) NOT NULL,
  `shipping` tinyint(1) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  `uses_total` int(11) NOT NULL,
  `uses_customer` varchar(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_coupon`
--

INSERT INTO `oc_coupon` (`coupon_id`, `name`, `code`, `type`, `discount`, `logged`, `shipping`, `total`, `date_start`, `date_end`, `uses_total`, `uses_customer`, `status`, `date_added`) VALUES
(4, ' خصم -10%', '2222', 'P', 10.0000, 0, 0, 0.0000, '2014-01-01', '2020-01-01', 10, '10', 0, '2009-01-27 13:55:03'),
(5, 'شحن مجانى', '3333', 'P', 0.0000, 0, 1, 100.0000, '2014-01-01', '2014-02-01', 10, '10', 0, '2009-03-14 21:13:53'),
(6, 'خصم -10.00', '1111', 'F', 10.0000, 0, 0, 10.0000, '2014-01-01', '2020-01-01', 100000, '10000', 0, '2009-03-14 21:15:18');

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon_category`
--

CREATE TABLE `oc_coupon_category` (
  `coupon_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon_history`
--

CREATE TABLE `oc_coupon_history` (
  `coupon_history_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon_product`
--

CREATE TABLE `oc_coupon_product` (
  `coupon_product_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_currency`
--

CREATE TABLE `oc_currency` (
  `currency_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` double(15,8) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_currency`
--

INSERT INTO `oc_currency` (`currency_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `status`, `date_modified`) VALUES
(1, 'ريال سعودي', 'SAR', '', 'ريال', '2', 1.00000000, 1, '2020-12-13 19:24:00'),
(2, 'US Dollar', 'USD', '$', '', '2', 1.00000000, 1, '2020-11-20 20:19:31');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer`
--

CREATE TABLE `oc_customer` (
  `customer_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `cart` text,
  `wishlist` text,
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `address_id` int(11) NOT NULL DEFAULT '0',
  `custom_field` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `safe` tinyint(1) NOT NULL,
  `token` text NOT NULL,
  `code` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_customer`
--

INSERT INTO `oc_customer` (`customer_id`, `customer_group_id`, `store_id`, `language_id`, `firstname`, `lastname`, `email`, `telephone`, `fax`, `password`, `salt`, `cart`, `wishlist`, `newsletter`, `address_id`, `custom_field`, `ip`, `status`, `safe`, `token`, `code`, `date_added`) VALUES
(1, 1, 0, 2, 'demo', 'demo', 'demo@demo.com', '123456789', '', '119e3d6ca800c67fd33b27233209a8098de30443', 'neEBCHY2G', NULL, NULL, 1, 1, '', '176.225.220.169', 1, 0, '', '', '2017-09-14 10:45:47'),
(2, 1, 0, 1, 'l;kl.l', 'kl', 'h@gmail.com', '445454452', '', 'ba6efcb9b4f551bd6763641454f3943f9c944e57', 'qWDnDpHOO', NULL, NULL, 0, 2, '[]', '::1', 1, 0, '', '', '2017-09-14 10:51:13'),
(3, 1, 0, 2, 'fggfg', 'fdf', 'de@de.com', '123456789', '', '578510879657b144f48cec83d362cb7b539a1f65', 'ycCBjugdE', NULL, NULL, 1, 3, '', '176.225.220.169', 1, 0, '', '', '2017-09-17 11:35:57'),
(4, 1, 0, 1, 'Webiarch', 'Infotech', 'webiarchinfotech@gmail.com', '1231231321', '', 'ccf4704ddf9ae5ab1a54010b5e233c9fd071ccc4', 'QbxmwSlEd', NULL, NULL, 0, 4, '', '::1', 1, 0, '', '', '2017-09-18 15:56:14'),
(5, 1, 0, 2, 'fsdf', 'dsfsdf', 'dd@dd.com', '4234234234', '', 'cedd481d577a1a8d6ab94405fab4e9552afae59d', 'HHMtxGLaY', NULL, NULL, 1, 5, '', '102.44.4.10', 1, 0, '', '', '2020-11-21 16:20:35'),
(6, 1, 0, 1, 'Imran', 'Haider', 'sheensoltech@gmail.com', '+923214117183', '', 'a6df6e1457dcd39edcc622a12a13b7d50931c574', 'qnmTzXuwl', NULL, NULL, 0, 6, '', '72.255.57.244', 1, 0, '', '', '2020-11-29 10:16:27');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_activity`
--

CREATE TABLE `oc_customer_activity` (
  `customer_activity_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `data` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_affiliate`
--

CREATE TABLE `oc_customer_affiliate` (
  `customer_id` int(11) NOT NULL,
  `company` varchar(40) NOT NULL,
  `website` varchar(255) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `commission` decimal(4,2) NOT NULL DEFAULT '0.00',
  `tax` varchar(64) NOT NULL,
  `payment` varchar(6) NOT NULL,
  `cheque` varchar(100) NOT NULL,
  `paypal` varchar(64) NOT NULL,
  `bank_name` varchar(64) NOT NULL,
  `bank_branch_number` varchar(64) NOT NULL,
  `bank_swift_code` varchar(64) NOT NULL,
  `bank_account_name` varchar(64) NOT NULL,
  `bank_account_number` varchar(64) NOT NULL,
  `custom_field` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_approval`
--

CREATE TABLE `oc_customer_approval` (
  `customer_approval_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `type` varchar(9) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_group`
--

CREATE TABLE `oc_customer_group` (
  `customer_group_id` int(11) NOT NULL,
  `approval` int(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_customer_group`
--

INSERT INTO `oc_customer_group` (`customer_group_id`, `approval`, `sort_order`) VALUES
(1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_group_description`
--

CREATE TABLE `oc_customer_group_description` (
  `customer_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_customer_group_description`
--

INSERT INTO `oc_customer_group_description` (`customer_group_id`, `language_id`, `name`, `description`) VALUES
(1, 1, 'Default', 'test'),
(1, 2, 'Default', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_history`
--

CREATE TABLE `oc_customer_history` (
  `customer_history_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_ip`
--

CREATE TABLE `oc_customer_ip` (
  `customer_ip_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_customer_ip`
--

INSERT INTO `oc_customer_ip` (`customer_ip_id`, `customer_id`, `ip`, `date_added`) VALUES
(1, 1, '::1', '2017-09-14 10:45:48'),
(2, 2, '::1', '2017-09-14 10:51:15'),
(3, 3, '::1', '2017-09-17 11:36:03'),
(4, 4, '::1', '2017-09-18 15:56:16'),
(5, 5, '41.236.165.233', '2020-11-21 16:20:36'),
(6, 5, '41.234.240.89', '2020-11-22 03:03:34'),
(7, 5, '41.234.219.19', '2020-11-22 19:23:59'),
(8, 5, '41.234.187.214', '2020-11-23 03:32:34'),
(9, 5, '41.237.193.64', '2020-11-23 11:01:07'),
(10, 5, '41.236.153.254', '2020-11-23 15:10:44'),
(11, 5, '41.234.186.248', '2020-11-24 05:54:39'),
(12, 5, '41.237.206.83', '2020-11-24 08:26:42'),
(13, 5, '41.236.148.9', '2020-11-24 16:06:44'),
(14, 5, '41.236.183.195', '2020-11-25 05:38:14'),
(15, 5, '41.234.195.140', '2020-11-26 08:25:13'),
(16, 5, '41.236.153.48', '2020-11-27 08:24:01'),
(17, 5, '102.44.4.10', '2020-11-28 06:28:26'),
(18, 1, '72.255.57.244', '2020-11-29 09:19:35'),
(19, 6, '72.255.57.244', '2020-11-29 10:16:28'),
(20, 1, '182.185.231.191', '2020-12-03 08:46:38'),
(21, 1, '176.225.220.169', '2020-12-05 11:58:33'),
(22, 3, '176.225.220.169', '2020-12-05 11:59:23');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_login`
--

CREATE TABLE `oc_customer_login` (
  `customer_login_id` int(11) NOT NULL,
  `email` varchar(96) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `total` int(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_customer_login`
--

INSERT INTO `oc_customer_login` (`customer_login_id`, `email`, `ip`, `total`, `date_added`, `date_modified`) VALUES
(3, 'daxi@gmail.com', '::1', 2, '2017-09-18 10:25:29', '2017-09-18 10:25:32'),
(4, 'admin', '72.255.57.244', 1, '2020-11-29 12:44:46', '2020-11-29 12:44:46');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_online`
--

CREATE TABLE `oc_customer_online` (
  `ip` varchar(40) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `referer` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_reward`
--

CREATE TABLE `oc_customer_reward` (
  `customer_reward_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `points` int(8) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_customer_reward`
--

INSERT INTO `oc_customer_reward` (`customer_reward_id`, `customer_id`, `order_id`, `description`, `points`, `date_added`) VALUES
(1, 1, 0, 'New', 1000, '2017-09-19 10:01:58'),
(2, 1, 0, 'New', 1000, '2017-09-19 10:02:01');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_search`
--

CREATE TABLE `oc_customer_search` (
  `customer_search_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_category` tinyint(1) NOT NULL,
  `description` tinyint(1) NOT NULL,
  `products` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_transaction`
--

CREATE TABLE `oc_customer_transaction` (
  `customer_transaction_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_customer_transaction`
--

INSERT INTO `oc_customer_transaction` (`customer_transaction_id`, `customer_id`, `order_id`, `description`, `amount`, `date_added`) VALUES
(1, 4, 0, 'Hello', 1000.0000, '2017-09-19 10:02:45'),
(2, 4, 0, 'Hello', 1000.0000, '2017-09-19 10:02:49');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_wishlist`
--

CREATE TABLE `oc_customer_wishlist` (
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_customer_wishlist`
--

INSERT INTO `oc_customer_wishlist` (`customer_id`, `product_id`, `date_added`) VALUES
(2, 43, '2017-09-14 11:46:39'),
(1, 40, '2017-09-17 11:05:46'),
(4, 29, '2017-09-18 16:00:24'),
(5, 50, '2020-11-22 19:44:34');

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field`
--

CREATE TABLE `oc_custom_field` (
  `custom_field_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `value` text NOT NULL,
  `validation` varchar(255) NOT NULL,
  `location` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_customer_group`
--

CREATE TABLE `oc_custom_field_customer_group` (
  `custom_field_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_description`
--

CREATE TABLE `oc_custom_field_description` (
  `custom_field_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_value`
--

CREATE TABLE `oc_custom_field_value` (
  `custom_field_value_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_value_description`
--

CREATE TABLE `oc_custom_field_value_description` (
  `custom_field_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_download`
--

CREATE TABLE `oc_download` (
  `download_id` int(11) NOT NULL,
  `mpseller_id` int(11) NOT NULL,
  `filename` varchar(160) NOT NULL,
  `mask` varchar(128) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_download_description`
--

CREATE TABLE `oc_download_description` (
  `download_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_event`
--

CREATE TABLE `oc_event` (
  `event_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `trigger` text NOT NULL,
  `action` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_event`
--

INSERT INTO `oc_event` (`event_id`, `code`, `trigger`, `action`, `status`, `sort_order`) VALUES
(1, 'activity_customer_add', 'catalog/model/account/customer/addCustomer/after', 'event/activity/addCustomer', 1, 0),
(2, 'activity_customer_edit', 'catalog/model/account/customer/editCustomer/after', 'event/activity/editCustomer', 1, 0),
(3, 'activity_customer_password', 'catalog/model/account/customer/editPassword/after', 'event/activity/editPassword', 1, 0),
(4, 'activity_customer_forgotten', 'catalog/model/account/customer/editCode/after', 'event/activity/forgotten', 1, 0),
(5, 'activity_transaction', 'catalog/model/account/customer/addTransaction/after', 'event/activity/addTransaction', 1, 0),
(6, 'activity_customer_login', 'catalog/model/account/customer/deleteLoginAttempts/after', 'event/activity/login', 1, 0),
(7, 'activity_address_add', 'catalog/model/account/address/addAddress/after', 'event/activity/addAddress', 1, 0),
(8, 'activity_address_edit', 'catalog/model/account/address/editAddress/after', 'event/activity/editAddress', 1, 0),
(9, 'activity_address_delete', 'catalog/model/account/address/deleteAddress/after', 'event/activity/deleteAddress', 1, 0),
(10, 'activity_affiliate_add', 'catalog/model/account/customer/addAffiliate/after', 'event/activity/addAffiliate', 1, 0),
(11, 'activity_affiliate_edit', 'catalog/model/account/customer/editAffiliate/after', 'event/activity/editAffiliate', 1, 0),
(12, 'activity_order_add', 'catalog/model/checkout/order/addOrderHistory/before', 'event/activity/addOrderHistory', 1, 0),
(13, 'activity_return_add', 'catalog/model/account/return/addReturn/after', 'event/activity/addReturn', 1, 0),
(14, 'mail_transaction', 'catalog/model/account/customer/addTransaction/after', 'mail/transaction', 1, 0),
(15, 'mail_forgotten', 'catalog/model/account/customer/editCode/after', 'mail/forgotten', 1, 0),
(16, 'mail_customer_add', 'catalog/model/account/customer/addCustomer/after', 'mail/register', 1, 0),
(17, 'mail_customer_alert', 'catalog/model/account/customer/addCustomer/after', 'mail/register/alert', 1, 0),
(18, 'mail_affiliate_add', 'catalog/model/account/customer/addAffiliate/after', 'mail/affiliate', 1, 0),
(19, 'mail_affiliate_alert', 'catalog/model/account/customer/addAffiliate/after', 'mail/affiliate/alert', 1, 0),
(20, 'mail_voucher', 'catalog/model/checkout/order/addOrderHistory/after', 'extension/total/voucher/send', 1, 0),
(21, 'mail_order_add', 'catalog/model/checkout/order/addOrderHistory/before', 'mail/order', 1, 0),
(22, 'mail_order_alert', 'catalog/model/checkout/order/addOrderHistory/before', 'mail/order/alert', 1, 0),
(23, 'statistics_review_add', 'catalog/model/catalog/review/addReview/after', 'event/statistics/addReview', 1, 0),
(24, 'statistics_return_add', 'catalog/model/account/return/addReturn/after', 'event/statistics/addReturn', 1, 0),
(25, 'statistics_order_history', 'catalog/model/checkout/order/addOrderHistory/after', 'event/statistics/addOrderHistory', 1, 0),
(26, 'admin_mail_affiliate_approve', 'admin/model/customer/customer_approval/approveAffiliate/after', 'mail/affiliate/approve', 1, 0),
(27, 'admin_mail_affiliate_deny', 'admin/model/customer/customer_approval/denyAffiliate/after', 'mail/affiliate/deny', 1, 0),
(28, 'admin_mail_customer_approve', 'admin/model/customer/customer_approval/approveCustomer/after', 'mail/customer/approve', 1, 0),
(29, 'admin_mail_customer_deny', 'admin/model/customer/customer_approval/denyCustomer/after', 'mail/customer/deny', 1, 0),
(30, 'admin_mail_reward', 'admin/model/customer/customer/addReward/after', 'mail/reward', 1, 0),
(31, 'admin_mail_transaction', 'admin/model/customer/customer/addTransaction/after', 'mail/transaction', 1, 0),
(32, 'admin_mail_return', 'admin/model/sale/return/addReturn/after', 'mail/return', 1, 0),
(33, 'admin_mail_forgotten', 'admin/model/user/user/editCode/after', 'mail/forgotten', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_extension`
--

CREATE TABLE `oc_extension` (
  `extension_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_extension`
--

INSERT INTO `oc_extension` (`extension_id`, `type`, `code`) VALUES
(1, 'payment', 'cod'),
(2, 'total', 'shipping'),
(3, 'total', 'sub_total'),
(4, 'total', 'tax'),
(5, 'total', 'total'),
(6, 'module', 'banner'),
(7, 'module', 'carousel'),
(8, 'total', 'credit'),
(9, 'shipping', 'flat'),
(10, 'total', 'handling'),
(11, 'total', 'low_order_fee'),
(12, 'total', 'coupon'),
(13, 'module', 'category'),
(14, 'module', 'account'),
(15, 'total', 'reward'),
(16, 'total', 'voucher'),
(17, 'payment', 'free_checkout'),
(18, 'module', 'featured'),
(19, 'module', 'slideshow'),
(20, 'theme', 'default'),
(21, 'dashboard', 'activity'),
(22, 'dashboard', 'sale'),
(23, 'dashboard', 'recent'),
(24, 'dashboard', 'order'),
(25, 'dashboard', 'online'),
(26, 'dashboard', 'map'),
(27, 'dashboard', 'customer'),
(28, 'dashboard', 'chart'),
(29, 'report', 'sale_coupon'),
(31, 'report', 'customer_search'),
(32, 'report', 'customer_transaction'),
(33, 'report', 'product_purchased'),
(34, 'report', 'product_viewed'),
(35, 'report', 'sale_return'),
(36, 'report', 'sale_order'),
(37, 'report', 'sale_shipping'),
(38, 'report', 'sale_tax'),
(39, 'report', 'customer_activity'),
(40, 'report', 'customer_order'),
(41, 'report', 'customer_reward'),
(42, 'module', 'html'),
(43, 'module', 'latest'),
(44, 'module', 'bestseller'),
(45, 'module', 'special'),
(59, 'payment', 'paylink'),
(47, 'shipping', 'free'),
(48, 'module', 'sellbanner'),
(49, 'module', 'offerbanner'),
(50, 'module', 'blogger'),
(51, 'module', 'leftbanner'),
(54, 'module', 'bestproduct'),
(53, 'module', 'leftsellbanner'),
(55, 'module', 'onsaleproduct'),
(56, 'module', 'filter'),
(60, 'analytics', 'google'),
(64, 'module', 'divido_calculator'),
(66, 'module', 'store'),
(67, 'shipping', 'fedex'),
(68, 'shipping', 'item'),
(69, 'shipping', 'pickup'),
(70, 'shipping', 'weight'),
(71, 'payment', 'bank_transfer'),
(73, 'module', 'smsa'),
(74, 'module', 'openpos'),
(75, 'module', 'mpseller_account'),
(76, 'shipping', 'mpseller'),
(77, 'shipping', 'hitdhlexpress');

-- --------------------------------------------------------

--
-- Table structure for table `oc_extension_install`
--

CREATE TABLE `oc_extension_install` (
  `extension_install_id` int(11) NOT NULL,
  `extension_download_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_extension_install`
--

INSERT INTO `oc_extension_install` (`extension_install_id`, `extension_download_id`, `filename`, `date_added`) VALUES
(1, 0, 'Paylink_new_system_edition BY Eng.Mohab.ocmod.zip', '2020-11-29 15:32:22'),
(2, 0, 'Lazy-Load-Images-OC3.0.x.ocmod.zip', '2020-12-02 22:44:54');

-- --------------------------------------------------------

--
-- Table structure for table `oc_extension_path`
--

CREATE TABLE `oc_extension_path` (
  `extension_path_id` int(11) NOT NULL,
  `extension_install_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_extension_path`
--

INSERT INTO `oc_extension_path` (`extension_path_id`, `extension_install_id`, `path`, `date_added`) VALUES
(1, 1, 'admin/controller/extension/payment/paylink.php', '2020-11-29 15:32:23'),
(2, 1, 'admin/view/image/payment/paylink.png', '2020-11-29 15:32:23'),
(3, 1, 'catalog/controller/extension/payment/paylink.php', '2020-11-29 15:32:23'),
(4, 1, 'catalog/model/extension/payment/paylink.php', '2020-11-29 15:32:23'),
(5, 1, 'admin/language/ar/extension/payment/paylink.php', '2020-11-29 15:32:23'),
(6, 1, 'admin/language/en-gb/extension/payment/paylink.php', '2020-11-29 15:32:23'),
(7, 1, 'admin/view/template/extension/payment/paylink.twig', '2020-11-29 15:32:23'),
(8, 1, 'catalog/language/ar/extension/payment/paylink.php', '2020-11-29 15:32:23'),
(9, 1, 'catalog/language/en-gb/extension/payment/paylink.php', '2020-11-29 15:32:23'),
(10, 1, 'catalog/view/theme/default/image/paylink.png', '2020-11-29 15:32:23'),
(11, 1, 'catalog/view/theme/default/template/extension/payment/paylink.twig', '2020-11-29 15:32:23'),
(12, 2, 'catalog/view/javascript/lazyload', '2020-12-02 22:44:55'),
(13, 2, 'catalog/view/javascript/lazyload/jquery.lazyload.min.js', '2020-12-02 22:44:55'),
(14, 2, 'catalog/view/javascript/lazyload/loading.gif', '2020-12-02 22:44:55');

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter`
--

CREATE TABLE `oc_filter` (
  `filter_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_filter`
--

INSERT INTO `oc_filter` (`filter_id`, `filter_group_id`, `sort_order`) VALUES
(1, 1, 0),
(2, 1, 1),
(3, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter_description`
--

CREATE TABLE `oc_filter_description` (
  `filter_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_filter_description`
--

INSERT INTO `oc_filter_description` (`filter_id`, `language_id`, `filter_group_id`, `name`) VALUES
(1, 2, 1, 'Red'),
(1, 1, 1, 'Red'),
(2, 1, 1, 'Green'),
(2, 2, 1, 'Green'),
(3, 1, 1, 'Blue'),
(3, 2, 1, 'Blue');

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter_group`
--

CREATE TABLE `oc_filter_group` (
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_filter_group`
--

INSERT INTO `oc_filter_group` (`filter_group_id`, `sort_order`) VALUES
(1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter_group_description`
--

CREATE TABLE `oc_filter_group_description` (
  `filter_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_filter_group_description`
--

INSERT INTO `oc_filter_group_description` (`filter_group_id`, `language_id`, `name`) VALUES
(1, 2, 'Color'),
(1, 1, 'Color');

-- --------------------------------------------------------

--
-- Table structure for table `oc_geo_zone`
--

CREATE TABLE `oc_geo_zone` (
  `geo_zone_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_geo_zone`
--

INSERT INTO `oc_geo_zone` (`geo_zone_id`, `name`, `description`, `date_added`, `date_modified`) VALUES
(3, 'UK VAT Zone', 'UK VAT', '2009-01-06 23:26:25', '2010-02-26 22:33:24'),
(4, 'السعودية', 'السعودية', '2009-06-23 01:14:53', '2020-11-21 01:47:27');

-- --------------------------------------------------------

--
-- Table structure for table `oc_hittech_dhl_details_new`
--

CREATE TABLE `oc_hittech_dhl_details_new` (
  `id` int(11) NOT NULL,
  `order_id` text NOT NULL,
  `tracking_num` text NOT NULL,
  `shipping_label` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `invoice` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `return_label` text CHARACTER SET utf8 COLLATE utf8_bin,
  `return_invoice` text CHARACTER SET utf8 COLLATE utf8_bin,
  `one` text CHARACTER SET utf8 COLLATE utf8_bin,
  `two` text CHARACTER SET utf8 COLLATE utf8_bin,
  `three` text CHARACTER SET utf8 COLLATE utf8_bin
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_information`
--

CREATE TABLE `oc_information` (
  `information_id` int(11) NOT NULL,
  `bottom` int(1) NOT NULL DEFAULT '0',
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_information`
--

INSERT INTO `oc_information` (`information_id`, `bottom`, `sort_order`, `status`) VALUES
(3, 1, 3, 1),
(4, 1, 1, 1),
(5, 1, 4, 1),
(6, 1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_information_description`
--

CREATE TABLE `oc_information_description` (
  `information_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` mediumtext NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_information_description`
--

INSERT INTO `oc_information_description` (`information_id`, `language_id`, `title`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(4, 1, 'About Us', '&lt;div class=&quot;row&quot;&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;The standard Lorem Ipsum passage&lt;/h3&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;row&quot;&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/thumb-up-button.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;An natum nulla eruditi mei, te qui idque adipisci, nonumy graeci has ad. Cu diam dolor noluisse nec. Nam ex dicta graeco, audire nominati persequeris eu eos. Pri nibh dolor soleat cu, sanctus inermis instructior eum ei. Scripta ceteros sit ex. Et eum alia laudem delectus.&lt;/p&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;Aliquam vulputate, neque consectetur gravida egestas\r\n&lt;/h3&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/front-bus.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;Et quis dictas meliore nec, habeo lorem blandit et vis, alia veniam assueverit an duo. Facilis offendit eleifend id has. Deserunt expetenda est an, tacimates iudicabit sed eu. Cum eu periculis similique, ut meis expetenda quo.&amp;nbsp; Eam ea omnis fierent disputando.\r\n    &lt;/p&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;Suspendisse vel lorem massa. Morbi\r\n&lt;/h3&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/clipboard-paste-button.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;Eam eu illud menandri praesent, ei regione oporteat tractatos nam. Ad mea postea voluptua constituam, aeterno aperiri facilis et nec, utroque veritus no mea. Mel quando tantas accusamus et, quo quot augue senserit ex, possim efficiendi has no. Nam at bonorum prodesset, per ne primis platonem, mel in mundi salutatus constituto. Ei vis vidit oportere, no prima eripuit ius, vim at justo legendos.&lt;br&gt;\r\n    &lt;/p&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;Vivamus ut diam ut risus sodales vestibulum eget varius dolor&lt;/h3&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/circles-extend-button.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;Has ut stet discere nonumes, te dicam nullam vim. Eam iriure tacimates persecuti ut. No labore reprimique consequuntur pri, iusto nihil ex usu. Ne illud definitionem pri, prima fuisset phaedrum sea cu. His ea partem tempor elaboraret, nam iudico atomorum accusamus ad, quo ut quidam aliquam voluptua. Cu cotidieque dissentiunt vix, at putent tamquam scribentur usu.&lt;/p&gt;\r\n    &lt;/div&gt;\r\n&lt;/div&gt;', 'About Us', '', ''),
(5, 1, 'Terms &amp; Conditions', '&lt;div class=&quot;row&quot;&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;The standard Lorem Ipsum passage&lt;/h3&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;row&quot;&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/thumb-up-button.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;An natum nulla eruditi mei, te qui idque adipisci, nonumy graeci has ad. Cu diam dolor noluisse nec. Nam ex dicta graeco, audire nominati persequeris eu eos. Pri nibh dolor soleat cu, sanctus inermis instructior eum ei. Scripta ceteros sit ex. Et eum alia laudem delectus.&lt;/p&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;Aliquam vulputate, neque consectetur gravida egestas\r\n&lt;/h3&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/front-bus.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;Et quis dictas meliore nec, habeo lorem blandit et vis, alia veniam assueverit an duo. Facilis offendit eleifend id has. Deserunt expetenda est an, tacimates iudicabit sed eu. Cum eu periculis similique, ut meis expetenda quo.&amp;nbsp; Eam ea omnis fierent disputando.\r\n    &lt;/p&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;Suspendisse vel lorem massa. Morbi\r\n&lt;/h3&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/clipboard-paste-button.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;Eam eu illud menandri praesent, ei regione oporteat tractatos nam. Ad mea postea voluptua constituam, aeterno aperiri facilis et nec, utroque veritus no mea. Mel quando tantas accusamus et, quo quot augue senserit ex, possim efficiendi has no. Nam at bonorum prodesset, per ne primis platonem, mel in mundi salutatus constituto. Ei vis vidit oportere, no prima eripuit ius, vim at justo legendos.&lt;br&gt;\r\n    &lt;/p&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;Vivamus ut diam ut risus sodales vestibulum eget varius dolor&lt;/h3&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/circles-extend-button.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;Has ut stet discere nonumes, te dicam nullam vim. Eam iriure tacimates persecuti ut. No labore reprimique consequuntur pri, iusto nihil ex usu. Ne illud definitionem pri, prima fuisset phaedrum sea cu. His ea partem tempor elaboraret, nam iudico atomorum accusamus ad, quo ut quidam aliquam voluptua. Cu cotidieque dissentiunt vix, at putent tamquam scribentur usu.&lt;/p&gt;\r\n    &lt;/div&gt;\r\n&lt;/div&gt;', 'Terms &amp; Conditions', '', ''),
(3, 1, 'Privacy Policy', '&lt;div class=&quot;row&quot;&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;The standard Lorem Ipsum passage&lt;/h3&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;row&quot;&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/thumb-up-button.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;An natum nulla eruditi mei, te qui idque adipisci, nonumy graeci has ad. Cu diam dolor noluisse nec. Nam ex dicta graeco, audire nominati persequeris eu eos. Pri nibh dolor soleat cu, sanctus inermis instructior eum ei. Scripta ceteros sit ex. Et eum alia laudem delectus.&lt;/p&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;Aliquam vulputate, neque consectetur gravida egestas\r\n&lt;/h3&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/front-bus.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;Et quis dictas meliore nec, habeo lorem blandit et vis, alia veniam assueverit an duo. Facilis offendit eleifend id has. Deserunt expetenda est an, tacimates iudicabit sed eu. Cum eu periculis similique, ut meis expetenda quo.&amp;nbsp; Eam ea omnis fierent disputando.\r\n    &lt;/p&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;Suspendisse vel lorem massa. Morbi\r\n&lt;/h3&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/clipboard-paste-button.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;Eam eu illud menandri praesent, ei regione oporteat tractatos nam. Ad mea postea voluptua constituam, aeterno aperiri facilis et nec, utroque veritus no mea. Mel quando tantas accusamus et, quo quot augue senserit ex, possim efficiendi has no. Nam at bonorum prodesset, per ne primis platonem, mel in mundi salutatus constituto. Ei vis vidit oportere, no prima eripuit ius, vim at justo legendos.&lt;br&gt;\r\n    &lt;/p&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;Vivamus ut diam ut risus sodales vestibulum eget varius dolor&lt;/h3&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/circles-extend-button.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;Has ut stet discere nonumes, te dicam nullam vim. Eam iriure tacimates persecuti ut. No labore reprimique consequuntur pri, iusto nihil ex usu. Ne illud definitionem pri, prima fuisset phaedrum sea cu. His ea partem tempor elaboraret, nam iudico atomorum accusamus ad, quo ut quidam aliquam voluptua. Cu cotidieque dissentiunt vix, at putent tamquam scribentur usu.&lt;/p&gt;\r\n    &lt;/div&gt;\r\n&lt;/div&gt;', 'Privacy Policy', '', ''),
(6, 1, 'Delivery Information', '&lt;div class=&quot;row&quot;&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;The standard Lorem Ipsum passage&lt;/h3&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;row&quot;&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/thumb-up-button.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;An natum nulla eruditi mei, te qui idque adipisci, nonumy graeci has ad. Cu diam dolor noluisse nec. Nam ex dicta graeco, audire nominati persequeris eu eos. Pri nibh dolor soleat cu, sanctus inermis instructior eum ei. Scripta ceteros sit ex. Et eum alia laudem delectus.&lt;/p&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;Aliquam vulputate, neque consectetur gravida egestas\r\n&lt;/h3&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/front-bus.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;Et quis dictas meliore nec, habeo lorem blandit et vis, alia veniam assueverit an duo. Facilis offendit eleifend id has. Deserunt expetenda est an, tacimates iudicabit sed eu. Cum eu periculis similique, ut meis expetenda quo.&amp;nbsp; Eam ea omnis fierent disputando.\r\n    &lt;/p&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;Suspendisse vel lorem massa. Morbi\r\n&lt;/h3&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/clipboard-paste-button.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;Eam eu illud menandri praesent, ei regione oporteat tractatos nam. Ad mea postea voluptua constituam, aeterno aperiri facilis et nec, utroque veritus no mea. Mel quando tantas accusamus et, quo quot augue senserit ex, possim efficiendi has no. Nam at bonorum prodesset, per ne primis platonem, mel in mundi salutatus constituto. Ei vis vidit oportere, no prima eripuit ius, vim at justo legendos.&lt;br&gt;\r\n    &lt;/p&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;Vivamus ut diam ut risus sodales vestibulum eget varius dolor&lt;/h3&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/circles-extend-button.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;Has ut stet discere nonumes, te dicam nullam vim. Eam iriure tacimates persecuti ut. No labore reprimique consequuntur pri, iusto nihil ex usu. Ne illud definitionem pri, prima fuisset phaedrum sea cu. His ea partem tempor elaboraret, nam iudico atomorum accusamus ad, quo ut quidam aliquam voluptua. Cu cotidieque dissentiunt vix, at putent tamquam scribentur usu.&lt;/p&gt;\r\n    &lt;/div&gt;\r\n&lt;/div&gt;', 'Delivery Information', '', ''),
(4, 2, 'About Us', '&lt;div class=&quot;row&quot;&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;The standard Lorem Ipsum passage&lt;/h3&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;row&quot;&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/thumb-up-button.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;An natum nulla eruditi mei, te qui idque adipisci, nonumy graeci has ad. Cu diam dolor noluisse nec. Nam ex dicta graeco, audire nominati persequeris eu eos. Pri nibh dolor soleat cu, sanctus inermis instructior eum ei. Scripta ceteros sit ex. Et eum alia laudem delectus.&lt;/p&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;Aliquam vulputate, neque consectetur gravida egestas\r\n&lt;/h3&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/front-bus.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;Et quis dictas meliore nec, habeo lorem blandit et vis, alia veniam assueverit an duo. Facilis offendit eleifend id has. Deserunt expetenda est an, tacimates iudicabit sed eu. Cum eu periculis similique, ut meis expetenda quo.&amp;nbsp; Eam ea omnis fierent disputando.\r\n    &lt;/p&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;Suspendisse vel lorem massa. Morbi\r\n&lt;/h3&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/clipboard-paste-button.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;Eam eu illud menandri praesent, ei regione oporteat tractatos nam. Ad mea postea voluptua constituam, aeterno aperiri facilis et nec, utroque veritus no mea. Mel quando tantas accusamus et, quo quot augue senserit ex, possim efficiendi has no. Nam at bonorum prodesset, per ne primis platonem, mel in mundi salutatus constituto. Ei vis vidit oportere, no prima eripuit ius, vim at justo legendos.&lt;br&gt;\r\n    &lt;/p&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;Vivamus ut diam ut risus sodales vestibulum eget varius dolor&lt;/h3&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/circles-extend-button.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;Has ut stet discere nonumes, te dicam nullam vim. Eam iriure tacimates persecuti ut. No labore reprimique consequuntur pri, iusto nihil ex usu. Ne illud definitionem pri, prima fuisset phaedrum sea cu. His ea partem tempor elaboraret, nam iudico atomorum accusamus ad, quo ut quidam aliquam voluptua. Cu cotidieque dissentiunt vix, at putent tamquam scribentur usu.&lt;/p&gt;\r\n    &lt;/div&gt;\r\n&lt;/div&gt;', 'About Us', '', ''),
(6, 2, 'Delivery Information', '&lt;div class=&quot;row&quot;&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;The standard Lorem Ipsum passage&lt;/h3&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;row&quot;&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/thumb-up-button.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;An natum nulla eruditi mei, te qui idque adipisci, nonumy graeci has ad. Cu diam dolor noluisse nec. Nam ex dicta graeco, audire nominati persequeris eu eos. Pri nibh dolor soleat cu, sanctus inermis instructior eum ei. Scripta ceteros sit ex. Et eum alia laudem delectus.&lt;/p&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;Aliquam vulputate, neque consectetur gravida egestas\r\n&lt;/h3&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/front-bus.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;Et quis dictas meliore nec, habeo lorem blandit et vis, alia veniam assueverit an duo. Facilis offendit eleifend id has. Deserunt expetenda est an, tacimates iudicabit sed eu. Cum eu periculis similique, ut meis expetenda quo.&amp;nbsp; Eam ea omnis fierent disputando.\r\n    &lt;/p&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;Suspendisse vel lorem massa. Morbi\r\n&lt;/h3&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/clipboard-paste-button.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;Eam eu illud menandri praesent, ei regione oporteat tractatos nam. Ad mea postea voluptua constituam, aeterno aperiri facilis et nec, utroque veritus no mea. Mel quando tantas accusamus et, quo quot augue senserit ex, possim efficiendi has no. Nam at bonorum prodesset, per ne primis platonem, mel in mundi salutatus constituto. Ei vis vidit oportere, no prima eripuit ius, vim at justo legendos.&lt;br&gt;\r\n    &lt;/p&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;Vivamus ut diam ut risus sodales vestibulum eget varius dolor&lt;/h3&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/circles-extend-button.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;Has ut stet discere nonumes, te dicam nullam vim. Eam iriure tacimates persecuti ut. No labore reprimique consequuntur pri, iusto nihil ex usu. Ne illud definitionem pri, prima fuisset phaedrum sea cu. His ea partem tempor elaboraret, nam iudico atomorum accusamus ad, quo ut quidam aliquam voluptua. Cu cotidieque dissentiunt vix, at putent tamquam scribentur usu.&lt;/p&gt;\r\n    &lt;/div&gt;\r\n&lt;/div&gt;', 'Delivery Information', '', ''),
(3, 2, 'Privacy Policy', '&lt;div class=&quot;row&quot;&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;The standard Lorem Ipsum passage&lt;/h3&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;row&quot;&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/thumb-up-button.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;An natum nulla eruditi mei, te qui idque adipisci, nonumy graeci has ad. Cu diam dolor noluisse nec. Nam ex dicta graeco, audire nominati persequeris eu eos. Pri nibh dolor soleat cu, sanctus inermis instructior eum ei. Scripta ceteros sit ex. Et eum alia laudem delectus.&lt;/p&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;Aliquam vulputate, neque consectetur gravida egestas\r\n&lt;/h3&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/front-bus.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;Et quis dictas meliore nec, habeo lorem blandit et vis, alia veniam assueverit an duo. Facilis offendit eleifend id has. Deserunt expetenda est an, tacimates iudicabit sed eu. Cum eu periculis similique, ut meis expetenda quo.&amp;nbsp; Eam ea omnis fierent disputando.\r\n    &lt;/p&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;Suspendisse vel lorem massa. Morbi\r\n&lt;/h3&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/clipboard-paste-button.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;Eam eu illud menandri praesent, ei regione oporteat tractatos nam. Ad mea postea voluptua constituam, aeterno aperiri facilis et nec, utroque veritus no mea. Mel quando tantas accusamus et, quo quot augue senserit ex, possim efficiendi has no. Nam at bonorum prodesset, per ne primis platonem, mel in mundi salutatus constituto. Ei vis vidit oportere, no prima eripuit ius, vim at justo legendos.&lt;br&gt;\r\n    &lt;/p&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;Vivamus ut diam ut risus sodales vestibulum eget varius dolor&lt;/h3&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/circles-extend-button.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;Has ut stet discere nonumes, te dicam nullam vim. Eam iriure tacimates persecuti ut. No labore reprimique consequuntur pri, iusto nihil ex usu. Ne illud definitionem pri, prima fuisset phaedrum sea cu. His ea partem tempor elaboraret, nam iudico atomorum accusamus ad, quo ut quidam aliquam voluptua. Cu cotidieque dissentiunt vix, at putent tamquam scribentur usu.&lt;/p&gt;\r\n    &lt;/div&gt;\r\n&lt;/div&gt;', 'Privacy Policy', '', ''),
(5, 2, 'Terms &amp; Conditions', '&lt;div class=&quot;row&quot;&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;The standard Lorem Ipsum passage&lt;/h3&gt;&lt;/div&gt;&lt;/div&gt;&lt;div class=&quot;row&quot;&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/thumb-up-button.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;An natum nulla eruditi mei, te qui idque adipisci, nonumy graeci has ad. Cu diam dolor noluisse nec. Nam ex dicta graeco, audire nominati persequeris eu eos. Pri nibh dolor soleat cu, sanctus inermis instructior eum ei. Scripta ceteros sit ex. Et eum alia laudem delectus.&lt;/p&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;Aliquam vulputate, neque consectetur gravida egestas\r\n&lt;/h3&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/front-bus.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;Et quis dictas meliore nec, habeo lorem blandit et vis, alia veniam assueverit an duo. Facilis offendit eleifend id has. Deserunt expetenda est an, tacimates iudicabit sed eu. Cum eu periculis similique, ut meis expetenda quo.&amp;nbsp; Eam ea omnis fierent disputando.\r\n    &lt;/p&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;Suspendisse vel lorem massa. Morbi\r\n&lt;/h3&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/clipboard-paste-button.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;Eam eu illud menandri praesent, ei regione oporteat tractatos nam. Ad mea postea voluptua constituam, aeterno aperiri facilis et nec, utroque veritus no mea. Mel quando tantas accusamus et, quo quot augue senserit ex, possim efficiendi has no. Nam at bonorum prodesset, per ne primis platonem, mel in mundi salutatus constituto. Ei vis vidit oportere, no prima eripuit ius, vim at justo legendos.&lt;br&gt;\r\n    &lt;/p&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-12 col-xs-12&quot;&gt;\r\n        &lt;h3 style=&quot;text-transform: none;letter-spacing: 1px;&quot;&gt;Vivamus ut diam ut risus sodales vestibulum eget varius dolor&lt;/h3&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-1 col-xs-1&quot; style=&quot;width: 5.333%;&quot;&gt;\r\n    &lt;img src=&quot;image/catalog/icon/circles-extend-button.png&quot; style=&quot;width: 35px; height: 35px; float: left;&quot;&gt;\r\n    &lt;/div&gt;\r\n    &lt;div class=&quot;col-sm-11 col-xs-11&quot;&gt;\r\n    &lt;p style=&quot;font-size:14px;&quot;&gt;Has ut stet discere nonumes, te dicam nullam vim. Eam iriure tacimates persecuti ut. No labore reprimique consequuntur pri, iusto nihil ex usu. Ne illud definitionem pri, prima fuisset phaedrum sea cu. His ea partem tempor elaboraret, nam iudico atomorum accusamus ad, quo ut quidam aliquam voluptua. Cu cotidieque dissentiunt vix, at putent tamquam scribentur usu.&lt;/p&gt;\r\n    &lt;/div&gt;\r\n&lt;/div&gt;', 'Terms &amp; Conditions', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_information_to_layout`
--

CREATE TABLE `oc_information_to_layout` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_information_to_layout`
--

INSERT INTO `oc_information_to_layout` (`information_id`, `store_id`, `layout_id`) VALUES
(4, 0, 0),
(6, 0, 0),
(3, 0, 0),
(5, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_information_to_store`
--

CREATE TABLE `oc_information_to_store` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_information_to_store`
--

INSERT INTO `oc_information_to_store` (`information_id`, `store_id`) VALUES
(3, 0),
(4, 0),
(5, 0),
(6, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_language`
--

CREATE TABLE `oc_language` (
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(64) NOT NULL,
  `directory` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_language`
--

INSERT INTO `oc_language` (`language_id`, `name`, `code`, `locale`, `image`, `directory`, `sort_order`, `status`) VALUES
(1, 'English', 'en-gb', 'en-US,en_US.UTF-8,en_US,en-gb,english', 'gb.png', 'english', 1, 1),
(2, 'عربي', 'ar', 'ar_ua-arabic', 'ar.png', 'عربي', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_layout`
--

CREATE TABLE `oc_layout` (
  `layout_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_layout`
--

INSERT INTO `oc_layout` (`layout_id`, `name`) VALUES
(1, 'Home'),
(2, 'Product'),
(3, 'Category'),
(4, 'Default'),
(5, 'Manufacturer'),
(6, 'Account'),
(7, 'Checkout'),
(8, 'Contact'),
(9, 'Sitemap'),
(10, 'Affiliate'),
(11, 'Information'),
(12, 'Compare'),
(13, 'Search'),
(14, 'Blog');

-- --------------------------------------------------------

--
-- Table structure for table `oc_layout_module`
--

CREATE TABLE `oc_layout_module` (
  `layout_module_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `position` varchar(14) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_layout_module`
--

INSERT INTO `oc_layout_module` (`layout_module_id`, `layout_id`, `code`, `position`, `sort_order`) VALUES
(1006, 1, 'carousel.29', 'content_bottom', 0),
(1005, 1, 'offerbanner.37', 'content_top', 6),
(1004, 1, 'latest.33', 'content_top', 5),
(1003, 1, 'sellbanner.36', 'content_top', 4),
(1002, 1, 'bestseller.34', 'content_top', 3),
(1001, 1, 'special.35', 'content_top', 2),
(999, 1, 'slideshow.27', 'content_top', 0),
(1000, 1, 'featured.28', 'content_top', 1),
(874, 2, 'bestproduct.42', 'column_left', 4),
(868, 11, 'bestproduct.42', 'column_left', 4),
(865, 8, 'bestproduct.42', 'column_left', 4),
(871, 5, 'bestproduct.42', 'column_left', 4),
(701, 7, 'leftsellbanner.41', 'column_left', 5),
(700, 7, 'bestproduct.42', 'column_left', 4),
(698, 7, 'onsaleproduct.43', 'column_left', 2),
(697, 7, 'leftbanner.39', 'column_left', 1),
(696, 7, 'category', 'column_left', 0),
(862, 12, 'bestproduct.42', 'column_left', 4),
(861, 12, 'onsaleproduct.43', 'column_left', 2),
(741, 4, 'leftsellbanner.41', 'column_left', 5),
(740, 4, 'bestproduct.42', 'column_left', 4),
(738, 4, 'onsaleproduct.43', 'column_left', 2),
(737, 4, 'leftbanner.39', 'column_left', 1),
(736, 4, 'category', 'column_left', 0),
(877, 13, 'bestproduct.42', 'column_left', 4),
(1009, 6, 'account', 'column_right', 1),
(1008, 6, 'onsaleproduct.43', 'column_left', 2),
(1007, 6, 'category', 'column_left', 0),
(1011, 10, 'account', 'column_right', 1),
(1010, 10, 'category', 'column_left', 0),
(858, 3, 'filter', 'column_left', 1),
(857, 3, 'category', 'column_left', 0),
(1012, 14, 'category', 'column_left', 0),
(702, 7, 'html.46', 'footer_left', 0),
(703, 7, 'html.45', 'ftop_full', 0),
(704, 7, 'html.47', 'footer_right', 0),
(860, 12, 'leftbanner.39', 'column_left', 1),
(859, 12, 'category', 'column_left', 0),
(864, 8, 'onsaleproduct.43', 'column_left', 2),
(863, 8, 'category', 'column_left', 0),
(867, 11, 'onsaleproduct.43', 'column_left', 2),
(866, 11, 'category', 'column_left', 0),
(742, 4, 'account', 'content_top', 0),
(743, 4, 'html.46', 'footer_left', 0),
(744, 4, 'html.45', 'ftop_full', 0),
(745, 4, 'html.47', 'footer_right', 0),
(870, 5, 'onsaleproduct.43', 'column_left', 2),
(869, 5, 'category', 'column_left', 0),
(873, 2, 'onsaleproduct.43', 'column_left', 2),
(872, 2, 'category', 'column_left', 0),
(876, 13, 'onsaleproduct.43', 'column_left', 2),
(879, 9, 'onsaleproduct.43', 'column_left', 2),
(878, 9, 'category', 'column_left', 0),
(996, 1, 'leftbanner.39', 'column_left', 1),
(875, 13, 'category', 'column_left', 0),
(998, 1, 'leftsellbanner.41', 'column_left', 3),
(997, 1, 'onsaleproduct.43', 'column_left', 2),
(995, 1, 'category', 'column_left', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_layout_route`
--

CREATE TABLE `oc_layout_route` (
  `layout_route_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_layout_route`
--

INSERT INTO `oc_layout_route` (`layout_route_id`, `layout_id`, `store_id`, `route`) VALUES
(143, 6, 0, 'account/%'),
(144, 10, 0, 'affiliate/%'),
(125, 3, 0, 'product/category'),
(142, 1, 0, 'common/home'),
(130, 2, 0, 'product/product'),
(128, 11, 0, 'information/information'),
(111, 7, 0, 'checkout/%'),
(127, 8, 0, 'information/contact'),
(132, 9, 0, 'information/sitemap'),
(115, 4, 0, ''),
(129, 5, 0, 'product/manufacturer'),
(126, 12, 0, 'product/compare'),
(131, 13, 0, 'product/search'),
(145, 14, 0, 'information/blogger_blogs'),
(146, 14, 0, 'information/blogger');

-- --------------------------------------------------------

--
-- Table structure for table `oc_length_class`
--

CREATE TABLE `oc_length_class` (
  `length_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_length_class`
--

INSERT INTO `oc_length_class` (`length_class_id`, `value`) VALUES
(1, 1.00000000),
(2, 10.00000000),
(3, 0.39370000);

-- --------------------------------------------------------

--
-- Table structure for table `oc_length_class_description`
--

CREATE TABLE `oc_length_class_description` (
  `length_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_length_class_description`
--

INSERT INTO `oc_length_class_description` (`length_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Centimeter', 'cm'),
(2, 1, 'Millimeter', 'mm'),
(3, 1, 'Inch', 'in'),
(1, 2, 'Centimeter', 'cm'),
(2, 2, 'Millimeter', 'mm'),
(3, 2, 'Inch', 'in');

-- --------------------------------------------------------

--
-- Table structure for table `oc_location`
--

CREATE TABLE `oc_location` (
  `location_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `address` text NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `geocode` varchar(32) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `open` text NOT NULL,
  `comment` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_manufacturer`
--

CREATE TABLE `oc_manufacturer` (
  `manufacturer_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_manufacturer`
--

INSERT INTO `oc_manufacturer` (`manufacturer_id`, `name`, `image`, `sort_order`) VALUES
(5, 'HTC', 'catalog/demo/htc_logo.jpg', 0),
(6, 'Palm', 'catalog/demo/palm_logo.jpg', 0),
(7, 'Hewlett-Packard', 'catalog/demo/hp_logo.jpg', 0),
(8, 'Apple', 'catalog/demo/apple_logo.jpg', 0),
(9, 'Canon', 'catalog/demo/canon_logo.jpg', 0),
(10, 'Sony', 'catalog/demo/sony_logo.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_manufacturer_to_store`
--

CREATE TABLE `oc_manufacturer_to_store` (
  `manufacturer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_manufacturer_to_store`
--

INSERT INTO `oc_manufacturer_to_store` (`manufacturer_id`, `store_id`) VALUES
(5, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_marketing`
--

CREATE TABLE `oc_marketing` (
  `marketing_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `code` varchar(64) NOT NULL,
  `clicks` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_modification`
--

CREATE TABLE `oc_modification` (
  `modification_id` int(11) NOT NULL,
  `extension_install_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(64) NOT NULL,
  `author` varchar(64) NOT NULL,
  `version` varchar(32) NOT NULL,
  `link` varchar(255) NOT NULL,
  `xml` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_modification`
--

INSERT INTO `oc_modification` (`modification_id`, `extension_install_id`, `name`, `code`, `author`, `version`, `link`, `xml`, `status`, `date_added`) VALUES
(1, 1, 'Lana - PaymentOnline by Paylink', 'fetching_partners_products', 'LANAPlatform Developers', '2.1.0', 'https://lna.sa', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<modification>\n	<name>Lana - PaymentOnline by Paylink</name>\n	<code>fetching_partners_products</code>\n	<version>2.1.0</version>\n	<author>LANAPlatform Developers</author>\n	<link>https://lna.sa</link>\n</modification>\n', 1, '2020-11-29 15:32:24'),
(2, 2, 'Lazy Load', 'lazyload', 'Equotix', '2.0.0', 'https://www.equotix.com', '<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<modification>\r\n	<name>Lazy Load</name>\r\n	<code>lazyload</code>\r\n	<version>2.0.0</version>\r\n	<author>Equotix</author>\r\n	<link>https://www.equotix.com</link>\r\n	<file path=\"catalog/view/theme/*/template/common/header.twig\">\r\n		<operation>\r\n			<search><![CDATA[\r\n				</head>\r\n			]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n				<script type=\"text/javascript\" src=\"catalog/view/javascript/lazyload/jquery.lazyload.min.js\"></script>\r\n				<script type=\"text/javascript\">\r\n				$(document).ready(function() {\r\n					$(\'img.lazy-load\').lazyload({\r\n						effect: \"fadeIn\",\r\n						effectspeed: 3000,  \r\n						threshold: 100,\r\n						placeholder : \"catalog/view/javascript/lazyload/loading.gif\"\r\n					});\r\n				});\r\n				</script>\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n	<file path=\"catalog/controller/extension/module/featured.php\">\r\n		<operation>\r\n			<search><![CDATA[\r\n				foreach ($products as $product_id) {\r\n			]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n				$data[\'lazy_load_width_height\'] = \'width=\"\' . $setting[\'width\'] . \'\" height=\"\' . $setting[\'height\'] . \'\"\';\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n	<file path=\"catalog/controller/extension/module/{bestseller,special,latest}.php\">\r\n		<operation>\r\n			<search><![CDATA[\r\n				foreach ($results as $result) {\r\n			]]></search>\r\n			<add position=\"before\"><![CDATA[\r\n				$data[\'lazy_load_width_height\'] = \'width=\"\' . $setting[\'width\'] . \'\" height=\"\' . $setting[\'height\'] . \'\"\';\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n	<file path=\"catalog/controller/product/{special,search,category}.php\">\r\n		<operation>\r\n			<search><![CDATA[\r\n				foreach ($results as $result) {\r\n			]]></search>\r\n			<add position=\"before\" index=\"0\"><![CDATA[\r\n				$data[\'lazy_load_width_height\'] = \'width=\"\' . $this->config->get(\'theme_\' . $this->config->get(\'config_theme\') . \'_image_product_width\') . \'\" height=\"\' . $this->config->get(\'theme_\' . $this->config->get(\'config_theme\') . \'_image_product_height\') . \'\"\';\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n	<file path=\"catalog/view/theme/*/template/extension/module/{featured,latest,special,bestseller}.twig\">\r\n		<operation error=\"skip\">\r\n			<search><![CDATA[\r\n				src=\"{{ product.thumb }}\"\r\n			]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n				class=\"lazy-load\" data-original=\"{{ product.thumb }}\" {{ lazy_load_width_height }}\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n	<file path=\"catalog/view/theme/*/template/product/{special,category,search}.twig\">\r\n		<operation error=\"skip\">\r\n			<search><![CDATA[\r\n				src=\"{{ product.thumb }}\"\r\n			]]></search>\r\n			<add position=\"replace\"><![CDATA[\r\n				class=\"lazy-load\" data-original=\"{{ product.thumb }}\" {{ lazy_load_width_height }}\r\n			]]></add>\r\n		</operation>\r\n	</file>\r\n</modification>', 1, '2020-12-02 22:44:56');

-- --------------------------------------------------------

--
-- Table structure for table `oc_module`
--

CREATE TABLE `oc_module` (
  `module_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(32) NOT NULL,
  `setting` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_module`
--

INSERT INTO `oc_module` (`module_id`, `name`, `code`, `setting`) VALUES
(30, 'Category', 'banner', '{\"name\":\"Category\",\"banner_id\":\"6\",\"width\":\"182\",\"height\":\"182\",\"status\":\"1\"}'),
(29, 'Home page Brand', 'carousel', '{\"name\":\"Home page Brand\",\"banner_id\":\"8\",\"width\":\"225\",\"height\":\"150\",\"status\":\"1\"}'),
(28, 'Featured Product', 'featured', '{\"name\":\"Featured Product\",\"product_name\":\"\",\"product\":[\"43\",\"40\",\"42\",\"28\",\"41\",\"47\",\"30\",\"49\",\"33\",\"46\",\"29\"],\"limit\":\"20\",\"width\":\"270\",\"height\":\"354\",\"status\":\"1\"}'),
(27, 'Home Page Slideshow', 'slideshow', '{\"name\":\"Home Page Slideshow\",\"banner_id\":\"7\",\"width\":\"1470\",\"height\":\"500\",\"status\":\"1\"}'),
(31, 'Banner 1', 'banner', '{\"name\":\"Banner 1\",\"banner_id\":\"6\",\"width\":\"182\",\"height\":\"182\",\"status\":\"1\"}'),
(33, 'Latest Product', 'latest', '{\"name\":\"Latest Product\",\"limit\":\"20\",\"width\":\"270\",\"height\":\"354\",\"status\":\"1\"}'),
(34, 'Bestsell Products', 'bestseller', '{\"name\":\"Bestsell Products\",\"limit\":\"20\",\"width\":\"270\",\"height\":\"354\",\"status\":\"1\"}'),
(35, 'Special Product', 'special', '{\"name\":\"Special Product\",\"limit\":\"20\",\"width\":\"270\",\"height\":\"354\",\"status\":\"1\"}'),
(36, 'Home page Sellbanner', 'sellbanner', '{\"name\":\"Home page Sellbanner\",\"banner_id\":\"9\",\"width\":\"470\",\"height\":\"250\",\"status\":\"1\"}'),
(37, 'Home page Offerbanner', 'offerbanner', '{\"name\":\"Home page Offerbanner\",\"banner_id\":\"10\",\"width\":\"1470\",\"height\":\"150\",\"status\":\"1\"}'),
(38, 'Latest Blog', 'blogger', '{\"module_id\":\"38\",\"name\":\"Latest Blog\",\"status\":\"1\",\"width\":\"550\",\"height\":\"600\",\"limit\":\"5\",\"char_limit\":\"250\",\"comments\":\"1\",\"login\":\"0\",\"auto_approve\":\"1\"}'),
(39, 'Leftbanner1', 'leftbanner', '{\"name\":\"Leftbanner1\",\"banner_id\":\"11\",\"width\":\"270\",\"height\":\"450\",\"status\":\"1\"}'),
(42, 'left Best Product', 'bestproduct', '{\"name\":\"left Best Product\",\"product_name\":\"\",\"product\":[\"42\",\"47\",\"28\",\"41\",\"46\",\"49\",\"33\",\"30\",\"44\"],\"limit\":\"20\",\"width\":\"80\",\"height\":\"100\",\"status\":\"1\"}'),
(41, 'Leftbanner2', 'leftsellbanner', '{\"name\":\"Leftbanner2\",\"banner_id\":\"12\",\"width\":\"270\",\"height\":\"370\",\"status\":\"1\"}'),
(43, 'left Sale Product', 'onsaleproduct', '{\"name\":\"left Sale Product\",\"product_name\":\"\",\"product\":[\"48\",\"36\",\"34\",\"40\",\"41\"],\"limit\":\"20\",\"width\":\"80\",\"height\":\"100\",\"status\":\"1\"}'),
(45, 'Footer Newsletter', 'html', '{\"name\":\"Footer Newsletter\",\"module_description\":{\"1\":{\"title\":\"\",\"description\":\"&lt;div class=&quot;news-bg&quot;&gt;\\r\\n&lt;div class=&quot;container&quot;&gt;\\r\\n  &lt;div class=&quot;row&quot;&gt;\\r\\n&lt;div class=&quot;block_newsletter&quot;&gt;\\r\\n    &lt;h3 id=&quot;newstitle&quot; class=&quot;col-md-5 col-sm-5 col-xs-12 hidden-xs&quot;&gt;&lt;img src=&quot;image\\/catalog\\/envelop.png&quot; alt=&quot;envelop&quot;&gt; &lt;span&gt;Sign up for newsletter&lt;\\/span&gt;&lt;\\/h3&gt;\\r\\n    &lt;div class=&quot;col-md-5 col-sm-7 col-xs-12&quot;&gt;\\r\\n      \\r\\n        &lt;div class=&quot;row&quot;&gt;\\r\\n          &lt;div class=&quot;col-xs-12&quot;&gt;\\r\\n            &lt;button class=&quot;btn btn-primary&quot; name=&quot;submitNewsletter&quot; value=&quot;Subscribe&quot; type=&quot;submit&quot;&gt;Subscribe&lt;\\/button&gt;\\r\\n            &lt;div class=&quot;input-wrapper&quot;&gt;\\r\\n              &lt;input name=&quot;email&quot; value=&quot;&quot; placeholder=&quot;Your email address&quot; type=&quot;text&quot;&gt;\\r\\n            &lt;\\/div&gt;\\r\\n          &lt;\\/div&gt;\\r\\n          &lt;div class=&quot;col-md-2&quot;&gt;&lt;\\/div&gt;\\r\\n        &lt;\\/div&gt;\\r\\n      \\r\\n    &lt;\\/div&gt;\\r\\n&lt;\\/div&gt;\\r\\n  &lt;\\/div&gt;\\r\\n&lt;\\/div&gt;\\r\\n&lt;\\/div&gt;\"},\"2\":{\"title\":\"\",\"description\":\"&lt;div class=&quot;news-bg&quot;&gt;\\r\\n&lt;div class=&quot;container&quot;&gt;\\r\\n  &lt;div class=&quot;row&quot;&gt;\\r\\n&lt;div class=&quot;block_newsletter&quot;&gt;\\r\\n    &lt;h3 id=&quot;newstitle&quot; class=&quot;col-md-5 col-sm-5 col-xs-12 hidden-xs&quot;&gt;&lt;img src=&quot;image\\/catalog\\/envelop.png&quot; alt=&quot;envelop&quot;&gt; &lt;span&gt;Sign up for newsletter&lt;\\/span&gt;&lt;\\/h3&gt;\\r\\n    &lt;div class=&quot;col-md-5 col-sm-7 col-xs-12&quot;&gt;\\r\\n      \\r\\n        &lt;div class=&quot;row&quot;&gt;\\r\\n          &lt;div class=&quot;col-xs-12&quot;&gt;\\r\\n            &lt;button class=&quot;btn btn-primary&quot; name=&quot;submitNewsletter&quot; value=&quot;Subscribe&quot; type=&quot;submit&quot;&gt;Subscribe&lt;\\/button&gt;\\r\\n            &lt;div class=&quot;input-wrapper&quot;&gt;\\r\\n              &lt;input name=&quot;email&quot; value=&quot;&quot; placeholder=&quot;Your email address&quot; type=&quot;text&quot;&gt;\\r\\n            &lt;\\/div&gt;\\r\\n          &lt;\\/div&gt;\\r\\n          &lt;div class=&quot;col-md-2&quot;&gt;&lt;\\/div&gt;\\r\\n        &lt;\\/div&gt;\\r\\n      \\r\\n    &lt;\\/div&gt;\\r\\n&lt;\\/div&gt;\\r\\n  &lt;\\/div&gt;\\r\\n&lt;\\/div&gt;\\r\\n&lt;\\/div&gt;\"}},\"email\":\"\",\"status\":\"1\"}'),
(46, 'Html Footer Left', 'html', '{\"name\":\"Html Footer Left\",\"module_description\":{\"1\":{\"title\":\"\",\"description\":\"&lt;h5&gt;&lt;img src=&quot;image\\/catalog\\/footerlogo.png&quot; class=&quot;img-responsive hidden-xs&quot; alt=&quot;footerlogo&quot;&gt;&lt;span class=&quot;hidden-sm hidden-md hidden-lg&quot;&gt;Contact us&lt;\\/span&gt;\\r\\n   &lt;button type=&quot;button&quot; class=&quot;btn btn-primary toggle collapsed&quot; data-toggle=&quot;collapse&quot; data-target=&quot;#contact&quot;&gt;&lt;\\/button&gt;\\r\\n&lt;\\/h5&gt;\\r\\n&lt;div id=&quot;contact&quot; class=&quot;collapse footer-collapse footcontact&quot;&gt;\\r\\n&lt;ul class=&quot;list-unstyled f-left&quot;&gt;\\r\\n  &lt;li&gt;&lt;i class=&quot;fa fa-map-marker&quot;&gt;&lt;\\/i&gt;Fashion Stor&lt;\\/li&gt;\\r\\n  &lt;li&gt;&lt;i class=&quot;fa fa-envelope&quot;&gt;&lt;\\/i&gt;demo@demo.com&lt;\\/li&gt;\\r\\n  &lt;li&gt;&lt;i class=&quot;fa fa-phone&quot;&gt;&lt;\\/i&gt;123456789&lt;\\/li&gt;\\r\\n  &lt;li&gt;&lt;i class=&quot;fa fa-paper-plane&quot;&gt;&lt;\\/i&gt;Surat&lt;\\/li&gt;\\r\\n&lt;\\/ul&gt;\\r\\n&lt;\\/div&gt;\"},\"2\":{\"title\":\"\",\"description\":\"&lt;h5&gt;&lt;img src=&quot;image\\/catalog\\/footerlogo.png&quot; class=&quot;img-responsive hidden-xs&quot; alt=&quot;footerlogo&quot;&gt;&lt;span class=&quot;hidden-sm hidden-md hidden-lg&quot;&gt;Contact us&lt;\\/span&gt;\\r\\n   &lt;button type=&quot;button&quot; class=&quot;btn btn-primary toggle collapsed&quot; data-toggle=&quot;collapse&quot; data-target=&quot;#contact&quot;&gt;&lt;\\/button&gt;\\r\\n&lt;\\/h5&gt;\\r\\n&lt;div id=&quot;contact&quot; class=&quot;collapse footer-collapse footcontact&quot;&gt;\\r\\n&lt;ul class=&quot;list-unstyled f-left&quot;&gt;\\r\\n  &lt;li&gt;&lt;i class=&quot;fa fa-map-marker&quot;&gt;&lt;\\/i&gt;Fashion Stor&lt;\\/li&gt;\\r\\n  &lt;li&gt;&lt;i class=&quot;fa fa-envelope&quot;&gt;&lt;\\/i&gt;demo@demo.com&lt;\\/li&gt;\\r\\n  &lt;li&gt;&lt;i class=&quot;fa fa-phone&quot;&gt;&lt;\\/i&gt;123456789&lt;\\/li&gt;\\r\\n  &lt;li&gt;&lt;i class=&quot;fa fa-paper-plane&quot;&gt;&lt;\\/i&gt;Surat&lt;\\/li&gt;\\r\\n&lt;\\/ul&gt;\\r\\n&lt;\\/div&gt;\"}},\"status\":\"1\"}'),
(47, 'Html Footer Right', 'html', '{\"name\":\"Html Footer Right\",\"module_description\":{\"1\":{\"title\":\"\",\"description\":\"&lt;ul class=&quot;list-inline list-unstyled foot-payment&quot;&gt;\\r\\n&lt;li&gt;&lt;a href=&quot;#&quot;&gt;&lt;i class=&quot;fa fa-cc-mastercard&quot;&gt;&lt;\\/i&gt;&lt;\\/a&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;&lt;a href=&quot;#&quot;&gt;&lt;i class=&quot;fa fa-cc-visa&quot;&gt;&lt;\\/i&gt;&lt;\\/a&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;&lt;a href=&quot;#&quot;&gt;&lt;i class=&quot;fa fa-credit-card&quot;&gt;&lt;\\/i&gt;&lt;\\/a&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;&lt;a href=&quot;#&quot;&gt;&lt;i class=&quot;fa fa-cc-paypal&quot;&gt;&lt;\\/i&gt;&lt;\\/a&gt;&lt;\\/li&gt;\\r\\n&lt;\\/ul&gt;\"},\"2\":{\"title\":\"\",\"description\":\"&lt;ul class=&quot;list-inline list-unstyled foot-payment&quot;&gt;\\r\\n&lt;li&gt;&lt;a href=&quot;#&quot;&gt;&lt;i class=&quot;fa fa-cc-mastercard&quot;&gt;&lt;\\/i&gt;&lt;\\/a&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;&lt;a href=&quot;#&quot;&gt;&lt;i class=&quot;fa fa-cc-visa&quot;&gt;&lt;\\/i&gt;&lt;\\/a&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;&lt;a href=&quot;#&quot;&gt;&lt;i class=&quot;fa fa-credit-card&quot;&gt;&lt;\\/i&gt;&lt;\\/a&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;&lt;a href=&quot;#&quot;&gt;&lt;i class=&quot;fa fa-cc-paypal&quot;&gt;&lt;\\/i&gt;&lt;\\/a&gt;&lt;\\/li&gt;\\r\\n&lt;\\/ul&gt;\"}},\"status\":\"1\"}');

-- --------------------------------------------------------

--
-- Table structure for table `oc_mpseller`
--

CREATE TABLE `oc_mpseller` (
  `mpseller_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `store_owner` varchar(255) NOT NULL,
  `store_name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `seo_keyword` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `banner` varchar(255) NOT NULL,
  `shipping_type` varchar(255) NOT NULL COMMENT '1 = product wise, 1 = product wise, 0 = order wise',
  `shipping_amount` decimal(10,2) NOT NULL,
  `payment_type` varchar(100) NOT NULL,
  `paypal_email` varchar(96) NOT NULL,
  `bank_details` text NOT NULL,
  `cheque_payee_name` varchar(255) NOT NULL,
  `facebook_url` text NOT NULL,
  `google_plus_url` text NOT NULL,
  `twitter_url` text NOT NULL,
  `pinterest_url` text NOT NULL,
  `linkedin_url` text NOT NULL,
  `youtube_url` text NOT NULL,
  `instagram_url` text NOT NULL,
  `flickr_url` text NOT NULL,
  `approved` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `commission_rate` int(10) NOT NULL COMMENT 'Percent (%)',
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_mpseller_commission`
--

CREATE TABLE `oc_mpseller_commission` (
  `mpseller_commission_id` int(11) NOT NULL,
  `mpseller_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `type` varchar(30) NOT NULL COMMENT 'sale, withdraw',
  `status` tinyint(4) NOT NULL COMMENT '1 = Complete, 0 = Pending',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_mpseller_enquiry`
--

CREATE TABLE `oc_mpseller_enquiry` (
  `mpseller_enquiry_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `mpseller_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `message` text NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_mpseller_enquiry_chat`
--

CREATE TABLE `oc_mpseller_enquiry_chat` (
  `mpseller_enquiry_chat_id` int(11) NOT NULL,
  `mpseller_enquiry_id` int(11) NOT NULL,
  `from` varchar(255) NOT NULL,
  `mpseller_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_mpseller_information_section`
--

CREATE TABLE `oc_mpseller_information_section` (
  `mpseller_information_section_id` int(11) NOT NULL,
  `mpseller_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_mpseller_message`
--

CREATE TABLE `oc_mpseller_message` (
  `mpseller_message_id` int(11) NOT NULL,
  `mpseller_id` int(11) NOT NULL,
  `from` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `read_status` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_mpseller_order_history`
--

CREATE TABLE `oc_mpseller_order_history` (
  `mpseller_order_history_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `mpseller_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_mpseller_order_product`
--

CREATE TABLE `oc_mpseller_order_product` (
  `mpseller_order_product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `mpseller_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `shipping_amount` decimal(10,4) NOT NULL,
  `tax` decimal(15,4) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  `total` decimal(10,4) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_mpseller_order_total`
--

CREATE TABLE `oc_mpseller_order_total` (
  `mpseller_order_total_id` int(10) NOT NULL,
  `order_id` int(11) NOT NULL,
  `mpseller_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_mpseller_review`
--

CREATE TABLE `oc_mpseller_review` (
  `mpseller_review_id` int(11) NOT NULL,
  `mpseller_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `author` varchar(255) NOT NULL,
  `rating` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_openpos_order`
--

CREATE TABLE `oc_openpos_order` (
  `pos_order_id` int(11) NOT NULL,
  `cashier_user_id` int(11) NOT NULL DEFAULT '0',
  `seller_user_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `register_id` int(11) NOT NULL DEFAULT '0',
  `order_content` text NOT NULL,
  `comment` text,
  `local_id` varchar(255) DEFAULT NULL,
  `session` varchar(255) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_openpos_order_payment`
--

CREATE TABLE `oc_openpos_order_payment` (
  `pos_payment_id` int(11) NOT NULL,
  `payment_code` varchar(255) NOT NULL DEFAULT '',
  `payment_title` varchar(255) NOT NULL DEFAULT '',
  `amount` decimal(16,2) NOT NULL DEFAULT '0.00',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `comment` text,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_openpos_product_log`
--

CREATE TABLE `oc_openpos_product_log` (
  `product_log_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_openpos_receipt_template`
--

CREATE TABLE `oc_openpos_receipt_template` (
  `template_id` int(11) NOT NULL,
  `template_content` text,
  `template_css` text,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_openpos_register`
--

CREATE TABLE `oc_openpos_register` (
  `register_id` int(11) NOT NULL,
  `register_name` varchar(255) NOT NULL,
  `store_id` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_openpos_register_user`
--

CREATE TABLE `oc_openpos_register_user` (
  `register_user_id` int(11) NOT NULL,
  `register_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_openpos_transaction`
--

CREATE TABLE `oc_openpos_transaction` (
  `transaction_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `in_amount` decimal(16,2) NOT NULL DEFAULT '0.00',
  `out_amount` decimal(16,2) NOT NULL DEFAULT '0.00',
  `register_id` int(11) NOT NULL DEFAULT '0',
  `store_id` int(11) NOT NULL DEFAULT '0',
  `comment` text,
  `local_id` varchar(255) DEFAULT NULL,
  `transaction_content` text NOT NULL,
  `session` varchar(255) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_openpos_z_report`
--

CREATE TABLE `oc_openpos_z_report` (
  `z_id` int(11) NOT NULL,
  `session` varchar(255) NOT NULL,
  `report_content` text,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_option`
--

CREATE TABLE `oc_option` (
  `option_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option`
--

INSERT INTO `oc_option` (`option_id`, `type`, `sort_order`) VALUES
(1, 'radio', 1),
(2, 'checkbox', 2),
(4, 'text', 3),
(5, 'select', 4),
(6, 'textarea', 5),
(7, 'file', 6),
(8, 'date', 7),
(9, 'time', 8),
(10, 'datetime', 9),
(11, 'select', 10),
(12, 'date', 11);

-- --------------------------------------------------------

--
-- Table structure for table `oc_option_description`
--

CREATE TABLE `oc_option_description` (
  `option_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option_description`
--

INSERT INTO `oc_option_description` (`option_id`, `language_id`, `name`) VALUES
(1, 1, 'Radio'),
(2, 2, 'Color'),
(4, 1, 'Text'),
(6, 1, 'Textarea'),
(8, 1, 'Date'),
(7, 1, 'File'),
(5, 1, 'Select'),
(9, 1, 'Time'),
(10, 1, 'Date &amp; Time'),
(12, 1, 'Delivery Date'),
(11, 2, 'Size'),
(1, 2, 'Radio'),
(2, 1, 'Color'),
(4, 2, 'Text'),
(6, 2, 'Textarea'),
(8, 2, 'Date'),
(7, 2, 'File'),
(5, 2, 'Select'),
(9, 2, 'Time'),
(10, 2, 'Date &amp; Time'),
(12, 2, 'Delivery Date'),
(11, 1, 'Size');

-- --------------------------------------------------------

--
-- Table structure for table `oc_option_value`
--

CREATE TABLE `oc_option_value` (
  `option_value_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option_value`
--

INSERT INTO `oc_option_value` (`option_value_id`, `option_id`, `image`, `sort_order`) VALUES
(43, 1, '', 3),
(32, 1, '', 1),
(44, 2, '', 3),
(42, 5, '', 4),
(41, 5, '', 3),
(39, 5, '', 1),
(40, 5, '', 2),
(31, 1, '', 2),
(24, 2, '', 2),
(23, 2, '', 1),
(48, 11, '', 3),
(47, 11, '', 2),
(46, 11, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_option_value_description`
--

CREATE TABLE `oc_option_value_description` (
  `option_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option_value_description`
--

INSERT INTO `oc_option_value_description` (`option_value_id`, `language_id`, `option_id`, `name`) VALUES
(43, 1, 1, 'Large'),
(32, 1, 1, 'Small'),
(31, 1, 1, 'Medium'),
(42, 1, 5, 'Yellow'),
(41, 1, 5, 'Green'),
(39, 1, 5, 'Red'),
(40, 1, 5, 'Blue'),
(44, 2, 2, 'Green'),
(44, 1, 2, 'Green'),
(48, 2, 11, 'Large'),
(48, 1, 11, 'Large'),
(47, 2, 11, 'Medium'),
(43, 2, 1, 'Large'),
(32, 2, 1, 'Small'),
(24, 2, 2, 'Blue'),
(24, 1, 2, 'Blue'),
(31, 2, 1, 'Medium'),
(42, 2, 5, 'Yellow'),
(41, 2, 5, 'Green'),
(39, 2, 5, 'Red'),
(40, 2, 5, 'Blue'),
(23, 2, 2, 'Red'),
(23, 1, 2, 'Red'),
(47, 1, 11, 'Medium'),
(46, 2, 11, 'Small'),
(46, 1, 11, 'Small');

-- --------------------------------------------------------

--
-- Table structure for table `oc_order`
--

CREATE TABLE `oc_order` (
  `order_id` int(11) NOT NULL,
  `invoice_no` int(11) NOT NULL DEFAULT '0',
  `invoice_prefix` varchar(26) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `store_name` varchar(64) NOT NULL,
  `store_url` varchar(255) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `custom_field` text NOT NULL,
  `payment_firstname` varchar(32) NOT NULL,
  `payment_lastname` varchar(32) NOT NULL,
  `payment_company` varchar(60) NOT NULL,
  `payment_address_1` varchar(128) NOT NULL,
  `payment_address_2` varchar(128) NOT NULL,
  `payment_city` varchar(128) NOT NULL,
  `payment_postcode` varchar(10) NOT NULL,
  `payment_country` varchar(128) NOT NULL,
  `payment_country_id` int(11) NOT NULL,
  `payment_zone` varchar(128) NOT NULL,
  `payment_zone_id` int(11) NOT NULL,
  `payment_address_format` text NOT NULL,
  `payment_custom_field` text NOT NULL,
  `payment_method` varchar(128) NOT NULL,
  `payment_code` varchar(128) NOT NULL,
  `shipping_firstname` varchar(32) NOT NULL,
  `shipping_lastname` varchar(32) NOT NULL,
  `shipping_company` varchar(40) NOT NULL,
  `shipping_address_1` varchar(128) NOT NULL,
  `shipping_address_2` varchar(128) NOT NULL,
  `shipping_city` varchar(128) NOT NULL,
  `shipping_postcode` varchar(10) NOT NULL,
  `shipping_country` varchar(128) NOT NULL,
  `shipping_country_id` int(11) NOT NULL,
  `shipping_zone` varchar(128) NOT NULL,
  `shipping_zone_id` int(11) NOT NULL,
  `shipping_address_format` text NOT NULL,
  `shipping_custom_field` text NOT NULL,
  `shipping_method` varchar(128) NOT NULL,
  `shipping_code` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `order_status_id` int(11) NOT NULL DEFAULT '0',
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(15,4) NOT NULL,
  `marketing_id` int(11) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `language_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `currency_value` decimal(15,8) NOT NULL DEFAULT '1.00000000',
  `ip` varchar(40) NOT NULL,
  `forwarded_ip` varchar(40) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `accept_language` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_order`
--

INSERT INTO `oc_order` (`order_id`, `invoice_no`, `invoice_prefix`, `store_id`, `store_name`, `store_url`, `customer_id`, `customer_group_id`, `firstname`, `lastname`, `email`, `telephone`, `fax`, `custom_field`, `payment_firstname`, `payment_lastname`, `payment_company`, `payment_address_1`, `payment_address_2`, `payment_city`, `payment_postcode`, `payment_country`, `payment_country_id`, `payment_zone`, `payment_zone_id`, `payment_address_format`, `payment_custom_field`, `payment_method`, `payment_code`, `shipping_firstname`, `shipping_lastname`, `shipping_company`, `shipping_address_1`, `shipping_address_2`, `shipping_city`, `shipping_postcode`, `shipping_country`, `shipping_country_id`, `shipping_zone`, `shipping_zone_id`, `shipping_address_format`, `shipping_custom_field`, `shipping_method`, `shipping_code`, `comment`, `total`, `order_status_id`, `affiliate_id`, `commission`, `marketing_id`, `tracking`, `language_id`, `currency_id`, `currency_code`, `currency_value`, `ip`, `forwarded_ip`, `user_agent`, `accept_language`, `date_added`, `date_modified`) VALUES
(1, 0, 'INV-2017-00', 0, 'Your Store', 'http://localhost/ocfashion-store/', 0, 1, 'demo', 'demo', 'demo@demo.com', '123456789', '', '[]', 'demo', 'demo', '', 'surat', '', 'surat', '395006', 'India', 99, 'Gujarat', 1485, '', '[]', 'Credit Card / Debit Card (BluePay)', 'bluepay_hosted', '', '', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', 602.0000, 0, 0, 0.0000, 0, '', 1, 2, 'USD', 1.00000000, '::1', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:55.0) Gecko/20100101 Firefox/55.0', 'en-US,en;q=0.5', '2017-09-14 10:43:48', '2017-09-14 10:43:48'),
(2, 0, 'INV-2017-00', 0, 'Your Store', 'http://localhost/ocfashion-store/', 1, 1, 'demo', 'demo', 'demo@demo.com', '123456789', '', '', 'demo', 'demo', '', 'surat', '', 'surat', '', 'India', 99, 'Gujarat', 1485, '', '[]', 'Credit Card / Debit Card (BluePay)', 'bluepay_hosted', '', '', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', 602.0000, 0, 0, 0.0000, 0, '', 1, 2, 'USD', 1.00000000, '::1', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:55.0) Gecko/20100101 Firefox/55.0', 'en-US,en;q=0.5', '2017-09-14 10:46:19', '2017-09-14 10:46:19'),
(3, 0, 'INV-2017-00', 0, 'Your Store', 'http://localhost/ocfashion-store/', 1, 1, 'demo', 'demo', 'demo@demo.com', '123456789', '', '', 'demo', 'demo', '', 'surat', '', 'surat', '', 'India', 99, 'Gujarat', 1485, '', '[]', 'Credit Card / Debit Card (BluePay)', 'bluepay_hosted', '', '', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', 602.0000, 0, 0, 0.0000, 0, '', 1, 2, 'USD', 1.00000000, '::1', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:55.0) Gecko/20100101 Firefox/55.0', 'en-US,en;q=0.5', '2017-09-14 10:46:46', '2017-09-14 10:46:46'),
(5, 0, 'INV-2017-00', 0, 'Your Store', 'http://localhost/ocfashion-store/', 1, 1, 'demo', 'demo', 'demo@demo.com', '123456789', '', '', 'demo', 'demo', '', 'surat', '', 'surat', '', 'India', 99, 'Gujarat', 1485, '', '[]', 'Credit Card / Debit Card (BluePay)', 'bluepay_hosted', '', '', '', '', '', '', '', '', 0, '', 0, '', '[]', '', '', '', 1000.0000, 0, 0, 0.0000, 0, '', 1, 2, 'USD', 1.00000000, '::1', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:55.0) Gecko/20100101 Firefox/55.0', 'en-US,en;q=0.5', '2017-09-17 11:13:24', '2017-09-17 11:13:24'),
(12, 0, 'INV-2020-00', 0, 'Mohab', 'https://lanaapp.space/isa_mohab/', 0, 1, 'mohab', 'mamdouh', 'gtgalaxyfit@gmail.com', '201155661474', '', '[]', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', '<img src=\"catalog/view/theme/default/image/paylink.png\" alt=\"onlinepay\" title=\"onlinepay\" width=\"35%\"/>', 'paylink', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', 1005.0000, 0, 0, 0.0000, 0, '', 2, 1, 'SAR', 1.00000000, '41.236.130.109', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'en-US,en;q=0.9,ar;q=0.8', '2020-11-29 16:00:33', '2020-11-29 16:00:33'),
(14, 0, 'INV-2020-00', 0, 'Mohab', 'https://lanaapp.space/isa_mohab/', 0, 1, 'mohab', 'mamdouh', 'gtgalaxyfit@gmail.com', '312312312', '', '[]', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', '<img src=\"catalog/view/theme/default/image/paylink.png\" alt=\"onlinepay\" title=\"onlinepay\" width=\"35%\"/>', 'paylink', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', 205.0000, 0, 0, 0.0000, 0, '', 2, 1, 'SAR', 1.00000000, '41.236.130.109', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'en-US,en;q=0.9,ar;q=0.8', '2020-11-29 16:30:20', '2020-11-29 16:30:20'),
(15, 0, 'INV-2020-00', 0, 'Mohab', 'https://lanaapp.space/isa_mohab/', 0, 1, 'mohab', 'mamdouh', 'gtgalaxyfit@gmail.com', '312312312', '', '[]', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', 'الدفع عند التسليم', 'cod', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', 205.0000, 0, 0, 0.0000, 0, '', 2, 1, 'SAR', 1.00000000, '41.236.130.109', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'en-US,en;q=0.9,ar;q=0.8', '2020-11-29 16:30:42', '2020-11-29 16:30:42'),
(17, 0, 'INV-2020-00', 0, 'Mohab', 'https://lanaapp.space/isa_mohab/', 0, 1, 'mohab', 'mamdouh', 'gtgalaxyfit@gmail.com', '234242', '', '[]', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', 'الدفع عند التسليم', 'cod', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', 265.0000, 0, 0, 0.0000, 0, '', 2, 1, 'SAR', 1.00000000, '41.236.130.109', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'en-US,en;q=0.9,ar;q=0.8', '2020-11-29 19:04:26', '2020-11-29 19:04:26'),
(19, 0, 'INV-2020-00', 0, 'Mohab', 'https://lanaapp.space/isa_mohab/', 0, 1, 'mohab', 'mamdouh', 'gtgalaxyfit@gmail.com', '42342342', '', '[]', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', 'الدفع عند التسليم', 'cod', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', 257.0000, 0, 0, 0.0000, 0, '', 2, 1, 'SAR', 1.00000000, '41.236.130.109', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'en-US,en;q=0.9,ar;q=0.8', '2020-11-29 19:24:05', '2020-11-29 19:24:05'),
(52, 0, 'INV-2020-00', 0, 'Mohab', 'https://lanaapp.space/isa_mohab/', 0, 1, 'klfk', 'kdkl', 'kdkl@ccc.com', '345676543', '', '[]', 'klfk', 'kdkl', '', 'regge', '', 'fdbg', '12321', 'Saudi Arabia', 184, 'Al Madinah', 2877, '', '[]', '<img src=\"catalog/view/theme/default/image/paylink.png\" alt=\"onlinepay\" title=\"onlinepay\" width=\"35%\"/>', 'paylink', 'klfk', 'kdkl', '', 'regge', '', 'fdbg', '12321', 'Saudi Arabia', 184, 'Al Madinah', 2877, '', '[]', 'شحن مجاني', 'free.free', '', 80.0000, 0, 0, 0.0000, 0, '', 2, 1, 'SAR', 1.00000000, '176.224.86.229', '', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36', 'en-GB,en-US;q=0.9,en;q=0.8,ar;q=0.7', '2020-12-05 06:50:47', '2020-12-05 06:50:47'),
(24, 0, 'INV-2020-00', 0, 'Mohab', 'https://lanaapp.space/isa_mohab/', 0, 1, 'mohab', 'mamdouh', 'gtgalaxyfit@gmail.com', '23123', '', '[]', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', '<img src=\"catalog/view/theme/default/image/paylink.png\" alt=\"onlinepay\" title=\"onlinepay\" width=\"35%\"/>', 'paylink', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', 397.0000, 0, 0, 0.0000, 0, '', 2, 1, 'SAR', 1.00000000, '102.44.11.123', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'en-US,en;q=0.9,ar;q=0.8', '2020-11-30 08:39:44', '2020-11-30 08:39:44'),
(26, 0, 'INV-2020-00', 0, 'Mohab', 'https://lanaapp.space/isa_mohab/', 0, 1, 'mohab', 'mamdouh', 'gtgalaxyfit@gmail.com', '5454535345', '', '[]', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', '<img src=\"catalog/view/theme/default/image/paylink.png\" alt=\"onlinepay\" title=\"onlinepay\" width=\"35%\"/>', 'paylink', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', 1005.0000, 0, 0, 0.0000, 0, '', 2, 1, 'SAR', 1.00000000, '102.44.11.123', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'en-US,en;q=0.9,ar;q=0.8', '2020-11-30 09:53:53', '2020-11-30 09:53:53'),
(27, 0, 'INV-2020-00', 0, 'Mohab', 'https://lanaapp.space/isa_mohab/', 0, 1, 'mohab', 'mamdouh', 'gtgalaxyfit@gmail.com', '5454535345', '', '[]', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', '<img src=\"catalog/view/theme/default/image/paylink.png\" alt=\"onlinepay\" title=\"onlinepay\" width=\"35%\"/>', 'paylink', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', 1165.0000, 0, 0, 0.0000, 0, '', 2, 1, 'SAR', 1.00000000, '102.44.11.123', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'en-US,en;q=0.9,ar;q=0.8', '2020-11-30 10:45:18', '2020-11-30 10:45:18'),
(28, 0, 'INV-2020-00', 0, 'Mohab', 'https://lanaapp.space/isa_mohab/', 0, 1, 'mohab', 'mamdouh', 'gtgalaxyfit@gmail.com', '5454535345', '', '[]', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', '<img src=\"catalog/view/theme/default/image/paylink.png\" alt=\"onlinepay\" title=\"onlinepay\" width=\"35%\"/>', 'paylink', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', 41.0000, 0, 0, 0.0000, 0, '', 2, 1, 'SAR', 1.00000000, '102.44.11.123', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'en-US,en;q=0.9,ar;q=0.8', '2020-11-30 17:47:03', '2020-11-30 17:47:03'),
(29, 0, 'INV-2020-00', 0, 'Mohab', 'https://lanaapp.space/isa_mohab/', 0, 1, 'mohab', 'mamdouh', 'gtgalaxyfit@gmail.com', '5454535345', '', '[]', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', '<img src=\"catalog/view/theme/default/image/paylink.png\" alt=\"onlinepay\" title=\"onlinepay\" width=\"35%\"/>', 'paylink', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', 1041.0000, 0, 0, 0.0000, 0, '', 2, 1, 'SAR', 1.00000000, '102.44.11.123', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'en-US,en;q=0.9,ar;q=0.8', '2020-11-30 17:51:46', '2020-11-30 17:51:46'),
(30, 0, 'INV-2020-00', 0, 'Mohab', 'https://lanaapp.space/isa_mohab/', 0, 1, 'mohab', 'mamdouh', 'gtgalaxyfit@gmail.com', '5454535345', '', '[]', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', '<img src=\"catalog/view/theme/default/image/paylink.png\" alt=\"onlinepay\" title=\"onlinepay\" width=\"35%\"/>', 'paylink', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', 1041.0000, 0, 0, 0.0000, 0, '', 2, 1, 'SAR', 1.00000000, '102.44.11.123', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'en-US,en;q=0.9,ar;q=0.8', '2020-11-30 17:55:31', '2020-11-30 17:55:31'),
(31, 0, 'INV-2020-00', 0, 'Mohab', 'https://lanaapp.space/isa_mohab/', 0, 1, 'mohab', 'mamdouh', 'gtgalaxyfit@gmail.com', '5454535345', '', '[]', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', '<img src=\"catalog/view/theme/default/image/paylink.png\" alt=\"onlinepay\" title=\"onlinepay\" width=\"35%\"/>', 'paylink', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', 1041.0000, 0, 0, 0.0000, 0, '', 2, 1, 'SAR', 1.00000000, '102.44.11.123', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'en-US,en;q=0.9,ar;q=0.8', '2020-11-30 17:56:08', '2020-11-30 17:56:08'),
(32, 0, 'INV-2020-00', 0, 'Mohab', 'https://lanaapp.space/isa_mohab/', 0, 1, 'mohab', 'mamdouh', 'gtgalaxyfit@gmail.com', '5454535345', '', '[]', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', '<img src=\"catalog/view/theme/default/image/paylink.png\" alt=\"onlinepay\" title=\"onlinepay\" width=\"35%\"/>', 'paylink', 'mohab', 'mamdouh', '', 'tsadasd', '', 'cairo', '23232', 'Egypt', 63, 'Al Jizah', 1008, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', 1041.0000, 0, 0, 0.0000, 0, '', 2, 1, 'SAR', 1.00000000, '102.44.11.123', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'en-US,en;q=0.9,ar;q=0.8', '2020-11-30 18:04:35', '2020-11-30 18:04:35'),
(55, 0, 'INV-2020-00', 0, 'LnaSpace', 'https://lnasa.space/dfsdfsd9/', 0, 1, 'gfdg', 'fgdfg', 'fsdfsd@fsd.com', '345345', '', '[]', 'gfdg', 'fgdfg', '', '4534534', '', '45345', '435345', 'Saint Lucia', 179, 'Dauphin', 2838, '', '[]', '<img src=\"catalog/view/theme/default/image/paylink.png\" alt=\"onlinepay\" title=\"onlinepay\" width=\"35%\"/>', 'paylink', 'gfdg', 'fgdfg', '', '4534534', '', '45345', '435345', 'Saint Lucia', 179, 'Dauphin', 2838, '', '[]', 'Flat Shipping Rate', 'flat.flat', '', 1005.0000, 0, 0, 0.0000, 0, '', 1, 1, 'SAR', 1.00000000, '41.234.133.32', '', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'en-US,en;q=0.9', '2020-12-12 15:18:59', '2020-12-12 15:18:59');

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_history`
--

CREATE TABLE `oc_order_history` (
  `order_history_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_option`
--

CREATE TABLE `oc_order_option` (
  `order_option_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_option_value_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_order_option`
--

INSERT INTO `oc_order_option` (`order_option_id`, `order_id`, `order_product_id`, `product_option_id`, `product_option_value_id`, `name`, `value`, `type`) VALUES
(12, 19, 26, 218, 5, 'Radio', 'Small', 'radio'),
(13, 19, 26, 223, 9, 'Color', 'Blue', 'checkbox'),
(14, 19, 26, 208, 0, 'Text', 'test', 'text'),
(15, 19, 26, 217, 1, 'Select', 'Green', 'select'),
(16, 19, 26, 209, 0, 'Textarea', 'sdasd', 'textarea'),
(17, 19, 26, 222, 0, 'File', 'a64221f47f7aa36ba67fb576c24799704245eec9', 'file'),
(18, 19, 26, 219, 0, 'Date', '2011-02-20', 'date'),
(19, 19, 26, 221, 0, 'Time', '22:25', 'time'),
(20, 19, 26, 220, 0, 'Date &amp; Time', '2011-02-20 22:25', 'datetime'),
(79, 52, 80, 226, 16, 'Select', 'Blue', 'select'),
(39, 24, 35, 218, 6, 'Radio', 'Medium', 'radio'),
(40, 24, 35, 223, 8, 'Color', 'Red', 'checkbox'),
(41, 24, 35, 208, 0, 'Text', 'test', 'text'),
(42, 24, 35, 217, 4, 'Select', 'Red', 'select'),
(43, 24, 35, 209, 0, 'Textarea', 'SAss', 'textarea'),
(44, 24, 35, 222, 0, 'File', 'ed97f8f358b467dc2bd98b1ad6a6d126f902c128', 'file'),
(45, 24, 35, 219, 0, 'Date', '2011-02-20', 'date'),
(46, 24, 35, 221, 0, 'Time', '22:25', 'time'),
(47, 24, 35, 220, 0, 'Date &amp; Time', '2011-02-20 22:25', 'datetime'),
(57, 27, 40, 226, 16, 'Select', 'Blue', 'select');

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_product`
--

CREATE TABLE `oc_order_product` (
  `order_product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `reward` int(8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_order_product`
--

INSERT INTO `oc_order_product` (`order_product_id`, `order_id`, `product_id`, `name`, `model`, `quantity`, `price`, `total`, `tax`, `reward`) VALUES
(1, 1, 43, 'MacBook', 'Product 16', 1, 500.0000, 500.0000, 102.0000, 600),
(2, 2, 43, 'MacBook', 'Product 16', 1, 500.0000, 500.0000, 102.0000, 600),
(3, 3, 43, 'MacBook', 'Product 16', 1, 500.0000, 500.0000, 102.0000, 600),
(5, 5, 43, 'MacBook', 'Product 16', 2, 500.0000, 1000.0000, 0.0000, 1200),
(15, 12, 50, 'mohab', 'mohab', 1, 1000.0000, 1000.0000, 0.0000, 0),
(17, 14, 41, 'iMac', 'Product 14', 2, 100.0000, 200.0000, 0.0000, 0),
(18, 15, 41, 'iMac', 'Product 14', 2, 100.0000, 200.0000, 0.0000, 0),
(21, 17, 41, 'iMac', 'Product 14', 1, 100.0000, 100.0000, 0.0000, 0),
(22, 17, 48, 'iPod Classic', 'product 20', 4, 40.0000, 160.0000, 0.0000, 0),
(25, 19, 29, 'Palm Treo Pro', 'Product 2', 2, 40.0000, 80.0000, 0.0000, 0),
(26, 19, 42, 'Apple Cinema 30&quot;', 'Product 15', 2, 81.0000, 162.0000, 0.0000, 200),
(27, 19, 43, 'MacBook', 'Product 16', 1, 10.0000, 10.0000, 0.0000, 600),
(80, 52, 30, 'Canon EOS 5D', 'Product 3', 1, 80.0000, 80.0000, 0.0000, 200),
(35, 24, 42, 'Apple Cinema 30&quot;', 'Product 15', 3, 124.0000, 372.0000, 0.0000, 300),
(36, 24, 46, 'Sony VAIO', 'Product 19', 2, 10.0000, 20.0000, 0.0000, 0),
(39, 26, 46, 'Sony VAIO', 'Product 19', 1, 1000.0000, 1000.0000, 0.0000, 0),
(40, 27, 30, 'Canon EOS 5D', 'Product 3', 2, 80.0000, 160.0000, 0.0000, 400),
(41, 27, 46, 'Sony VAIO', 'Product 19', 1, 1000.0000, 1000.0000, 0.0000, 0),
(42, 28, 33, 'Samsung SyncMaster 941BW', 'Product 6', 4, 9.0000, 36.0000, 0.0000, 0),
(43, 29, 33, 'Samsung SyncMaster 941BW', 'Product 6', 4, 9.0000, 36.0000, 0.0000, 0),
(44, 29, 46, 'Sony VAIO', 'Product 19', 1, 1000.0000, 1000.0000, 0.0000, 0),
(45, 30, 33, 'Samsung SyncMaster 941BW', 'Product 6', 4, 9.0000, 36.0000, 0.0000, 0),
(46, 30, 46, 'Sony VAIO', 'Product 19', 1, 1000.0000, 1000.0000, 0.0000, 0),
(47, 31, 33, 'Samsung SyncMaster 941BW', 'Product 6', 4, 9.0000, 36.0000, 0.0000, 0),
(48, 31, 46, 'Sony VAIO', 'Product 19', 1, 1000.0000, 1000.0000, 0.0000, 0),
(49, 32, 33, 'Samsung SyncMaster 941BW', 'Product 6', 4, 9.0000, 36.0000, 0.0000, 0),
(50, 32, 46, 'Sony VAIO', 'Product 19', 1, 1000.0000, 1000.0000, 0.0000, 0),
(83, 55, 46, 'Sony VAIO', 'Product 19', 1, 1000.0000, 1000.0000, 0.0000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_recurring`
--

CREATE TABLE `oc_order_recurring` (
  `order_recurring_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `recurring_name` varchar(255) NOT NULL,
  `recurring_description` varchar(255) NOT NULL,
  `recurring_frequency` varchar(25) NOT NULL,
  `recurring_cycle` smallint(6) NOT NULL,
  `recurring_duration` smallint(6) NOT NULL,
  `recurring_price` decimal(10,4) NOT NULL,
  `trial` tinyint(1) NOT NULL,
  `trial_frequency` varchar(25) NOT NULL,
  `trial_cycle` smallint(6) NOT NULL,
  `trial_duration` smallint(6) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_recurring_transaction`
--

CREATE TABLE `oc_order_recurring_transaction` (
  `order_recurring_transaction_id` int(11) NOT NULL,
  `order_recurring_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `amount` decimal(10,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_shipment`
--

CREATE TABLE `oc_order_shipment` (
  `order_shipment_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `shipping_courier_id` varchar(255) NOT NULL DEFAULT '',
  `tracking_number` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_status`
--

CREATE TABLE `oc_order_status` (
  `order_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_order_status`
--

INSERT INTO `oc_order_status` (`order_status_id`, `language_id`, `name`) VALUES
(2, 2, 'جاري التجهيز'),
(3, 2, 'تم الشحن'),
(7, 2, 'الطلب ملغي'),
(5, 2, 'طلب مكتمل '),
(10, 2, 'توصيل فاشل للعميل'),
(11, 2, 'طلب معاد للمتجر'),
(12, 2, 'مستلم من العميل'),
(13, 2, 'إعادة المبلغ للعميل'),
(1, 2, 'طلب معلق'),
(15, 2, 'تم التجهيز'),
(2, 1, 'Processing'),
(3, 1, 'Shipped'),
(7, 1, 'Canceled'),
(5, 1, 'Complete'),
(10, 1, 'Failed'),
(11, 1, 'Refunded'),
(12, 1, 'Reversed'),
(13, 1, 'Chargeback'),
(1, 1, 'Pending'),
(15, 1, 'Processed');

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_total`
--

CREATE TABLE `oc_order_total` (
  `order_total_id` int(10) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_order_total`
--

INSERT INTO `oc_order_total` (`order_total_id`, `order_id`, `code`, `title`, `value`, `sort_order`) VALUES
(1, 1, 'sub_total', 'Sub-Total', 500.0000, 1),
(2, 1, 'tax', 'Eco Tax (-2.00)', 2.0000, 5),
(3, 1, 'tax', 'VAT (20%)', 100.0000, 5),
(4, 1, 'total', 'Total', 602.0000, 9),
(5, 2, 'sub_total', 'Sub-Total', 500.0000, 1),
(6, 2, 'tax', 'Eco Tax (-2.00)', 2.0000, 5),
(7, 2, 'tax', 'VAT (20%)', 100.0000, 5),
(8, 2, 'total', 'Total', 602.0000, 9),
(9, 3, 'sub_total', 'Sub-Total', 500.0000, 1),
(10, 3, 'tax', 'Eco Tax (-2.00)', 2.0000, 5),
(11, 3, 'tax', 'VAT (20%)', 100.0000, 5),
(12, 3, 'total', 'Total', 602.0000, 9),
(16, 5, 'sub_total', 'Sub-Total', 1000.0000, 1),
(17, 5, 'total', 'Total', 1000.0000, 9),
(36, 12, 'sub_total', 'الاجمالي', 1000.0000, 1),
(37, 12, 'shipping', 'Flat Shipping Rate', 5.0000, 3),
(38, 12, 'total', 'الاجمالي النهائي', 1005.0000, 9),
(42, 14, 'sub_total', 'الاجمالي', 200.0000, 1),
(43, 14, 'shipping', 'Flat Shipping Rate', 5.0000, 3),
(44, 14, 'total', 'الاجمالي النهائي', 205.0000, 9),
(45, 15, 'sub_total', 'الاجمالي', 200.0000, 1),
(46, 15, 'shipping', 'Flat Shipping Rate', 5.0000, 3),
(47, 15, 'total', 'الاجمالي النهائي', 205.0000, 9),
(158, 52, 'total', 'الاجمالي النهائي', 80.0000, 9),
(157, 52, 'shipping', 'شحن مجاني', 0.0000, 3),
(51, 17, 'sub_total', 'الاجمالي', 260.0000, 1),
(52, 17, 'shipping', 'Flat Shipping Rate', 5.0000, 3),
(53, 17, 'total', 'الاجمالي النهائي', 265.0000, 9),
(57, 19, 'sub_total', 'الاجمالي', 252.0000, 1),
(58, 19, 'shipping', 'Flat Shipping Rate', 5.0000, 3),
(59, 19, 'total', 'الاجمالي النهائي', 257.0000, 9),
(156, 52, 'sub_total', 'الاجمالي', 80.0000, 1),
(72, 24, 'sub_total', 'الاجمالي', 392.0000, 1),
(73, 24, 'shipping', 'Flat Shipping Rate', 5.0000, 3),
(74, 24, 'total', 'الاجمالي النهائي', 397.0000, 9),
(78, 26, 'sub_total', 'الاجمالي', 1000.0000, 1),
(79, 26, 'shipping', 'Flat Shipping Rate', 5.0000, 3),
(80, 26, 'total', 'الاجمالي النهائي', 1005.0000, 9),
(81, 27, 'sub_total', 'الاجمالي', 1160.0000, 1),
(82, 27, 'shipping', 'Flat Shipping Rate', 5.0000, 3),
(83, 27, 'total', 'الاجمالي النهائي', 1165.0000, 9),
(84, 28, 'sub_total', 'الاجمالي', 36.0000, 1),
(85, 28, 'shipping', 'Flat Shipping Rate', 5.0000, 3),
(86, 28, 'total', 'الاجمالي النهائي', 41.0000, 9),
(87, 29, 'sub_total', 'الاجمالي', 1036.0000, 1),
(88, 29, 'shipping', 'Flat Shipping Rate', 5.0000, 3),
(89, 29, 'total', 'الاجمالي النهائي', 1041.0000, 9),
(90, 30, 'sub_total', 'الاجمالي', 1036.0000, 1),
(91, 30, 'shipping', 'Flat Shipping Rate', 5.0000, 3),
(92, 30, 'total', 'الاجمالي النهائي', 1041.0000, 9),
(93, 31, 'sub_total', 'الاجمالي', 1036.0000, 1),
(94, 31, 'shipping', 'Flat Shipping Rate', 5.0000, 3),
(95, 31, 'total', 'الاجمالي النهائي', 1041.0000, 9),
(96, 32, 'sub_total', 'الاجمالي', 1036.0000, 1),
(97, 32, 'shipping', 'Flat Shipping Rate', 5.0000, 3),
(98, 32, 'total', 'الاجمالي النهائي', 1041.0000, 9),
(165, 55, 'sub_total', 'Sub-Total', 1000.0000, 1),
(166, 55, 'shipping', 'Flat Shipping Rate', 5.0000, 3),
(167, 55, 'total', 'Total', 1005.0000, 9);

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_voucher`
--

CREATE TABLE `oc_order_voucher` (
  `order_voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product`
--

CREATE TABLE `oc_product` (
  `product_id` int(11) NOT NULL,
  `model` varchar(64) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `upc` varchar(12) NOT NULL,
  `ean` varchar(14) NOT NULL,
  `jan` varchar(13) NOT NULL,
  `isbn` varchar(17) NOT NULL,
  `mpn` varchar(64) NOT NULL,
  `location` varchar(128) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `stock_status_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `shipping` tinyint(1) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `product_cost` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `expire_date` date NOT NULL DEFAULT '0000-00-00',
  `dropshipping` int(11) NOT NULL DEFAULT '0',
  `points` int(8) NOT NULL DEFAULT '0',
  `tax_class_id` int(11) NOT NULL,
  `date_available` date NOT NULL DEFAULT '0000-00-00',
  `weight` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `weight_class_id` int(11) NOT NULL DEFAULT '0',
  `length` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `width` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `height` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `length_class_id` int(11) NOT NULL DEFAULT '0',
  `subtract` tinyint(1) NOT NULL DEFAULT '1',
  `minimum` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `viewed` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `mpseller_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product`
--

INSERT INTO `oc_product` (`product_id`, `model`, `sku`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `stock_status_id`, `image`, `manufacturer_id`, `shipping`, `price`, `product_cost`, `expire_date`, `dropshipping`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `viewed`, `date_added`, `date_modified`, `mpseller_id`) VALUES
(28, 'Product 1', '', '', '', '', '', '', '', 939, 7, 'catalog/product/product.jpg', 5, 1, 100.0000, 0.0000, '0000-00-00', 0, 200, 9, '2009-02-03', 146.40000000, 2, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 0, 1, 13, '2009-02-03 16:06:50', '2017-09-19 13:05:21', 0),
(29, 'Product 2', '', '', '', '', '', '', '', 999, 6, 'catalog/product/product.jpg', 6, 1, 279.9900, 0.0000, '0000-00-00', 0, 0, 9, '2009-02-03', 133.00000000, 2, 0.00000000, 0.00000000, 0.00000000, 3, 1, 1, 0, 1, 18, '2009-02-03 16:42:17', '2017-09-19 13:11:04', 0),
(30, 'Product 3', '', '', '', '', '', '', '', 7, 6, 'catalog/product/product.jpg', 9, 1, 100.0000, 0.0000, '0000-00-00', 0, 0, 9, '2009-02-03', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 0, 1, 24, '2009-02-03 16:59:00', '2017-09-19 13:04:43', 0),
(31, 'Product 4', '', '', '', '', '', '', '', 1000, 6, 'catalog/product/product.jpg', 0, 1, 80.0000, 0.0000, '0000-00-00', 0, 0, 9, '2009-02-03', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 3, 1, 1, 0, 1, 16, '2009-02-03 17:00:10', '2017-09-19 13:10:45', 0),
(32, 'Product 5', '', '', '', '', '', '', '', 999, 6, 'catalog/product/product.jpg', 8, 1, 100.0000, 0.0000, '0000-00-00', 0, 0, 9, '2009-02-03', 5.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 0, 1, 8, '2009-02-03 17:07:26', '2017-09-19 13:08:32', 0),
(33, 'Product 6', '', '', '', '', '', '', '', 1000, 6, 'catalog/product/product.jpg', 0, 1, 200.0000, 0.0000, '0000-00-00', 0, 0, 9, '2009-02-03', 5.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 2, 1, 1, 0, 1, 9, '2009-02-03 17:08:31', '2017-09-19 13:09:14', 0),
(34, 'Product 7', '', '', '', '', '', '', '', 1000, 6, 'catalog/product/product.jpg', 8, 1, 100.0000, 0.0000, '0000-00-00', 0, 0, 9, '2009-02-03', 5.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 2, 1, 1, 0, 1, 28, '2009-02-03 18:07:54', '2017-09-19 13:08:00', 0),
(35, 'Product 8', '', '', '', '', '', '', '', 1000, 5, 'catalog/product/product.jpg', 0, 0, 100.0000, 0.0000, '0000-00-00', 0, 0, 9, '2009-02-03', 5.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 0, 1, 15, '2009-02-03 18:08:31', '2017-09-19 13:11:20', 0),
(36, 'Product 9', '', '', '', '', '', '', '', 994, 6, 'catalog/product/product.jpg', 8, 0, 100.0000, 0.0000, '0000-00-00', 0, 100, 9, '2009-02-03', 5.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 2, 1, 1, 0, 1, 3, '2009-02-03 18:09:19', '2017-09-19 13:07:31', 0),
(40, 'product 11', '', '', '', '', '', '', '', 970, 5, 'catalog/product/product.jpg', 8, 1, 101.0000, 0.0000, '0000-00-00', 1, 0, 9, '2009-02-03', 10.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 0, 1, 16, '2009-02-03 21:07:12', '2017-09-19 13:06:37', 0),
(41, 'Product 14', '', '', '', '', '', '', '', 977, 5, 'catalog/product/product.jpg', 8, 1, 100.0000, 50.0000, '0000-00-00', 1, 0, 9, '2009-02-03', 5.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 0, 1, 43, '2009-02-03 21:07:26', '2017-09-19 13:05:48', 0),
(42, 'Product 15', '', '', '', '', '', '', '', 990, 5, 'catalog/product/product.jpg', 8, 1, 100.0000, 0.0000, '0000-00-00', 1, 400, 9, '2009-02-04', 12.50000000, 1, 1.00000000, 2.00000000, 3.00000000, 1, 1, 2, 0, 1, 40, '2009-02-03 21:07:37', '2020-12-13 03:42:44', 0),
(43, 'Product 16', '', '', '', '', '', '', '', 929, 5, 'catalog/product/product.jpg', 8, 0, 500.0000, 400.0000, '0000-00-00', 0, 0, 9, '2009-02-03', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 2, 1, 1, 0, 1, 16, '2009-02-03 21:07:49', '2020-11-28 14:24:34', 0),
(44, 'Product 17', '', '', '', '', '', '', '', 1000, 5, 'catalog/product/product.jpg', 8, 1, 1000.0000, 800.0000, '0000-00-00', 0, 0, 9, '2009-02-03', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 2, 1, 1, 0, 1, 16, '2009-02-03 21:08:00', '2020-11-30 07:41:10', 0),
(45, 'Product 18', '', '', '', '', '', '', '', 998, 5, 'catalog/product/product.jpg', 8, 1, 2000.0000, 0.0000, '0000-00-00', 0, 0, 0, '2009-02-03', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 2, 1, 1, 0, 1, 14, '2009-02-03 21:08:17', '2017-09-19 13:10:21', 0),
(46, 'Product 19', '', '', '', '', '', '', '', 1000, 5, 'catalog/product/product.jpg', 10, 1, 1000.0000, 900.0000, '0000-00-00', 1, 0, 9, '2009-02-03', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 2, 1, 1, 0, 1, 29, '2009-02-03 21:08:29', '2020-11-30 08:44:20', 0),
(47, 'Product 21', '', '', '', '', '', '', '', 1000, 5, 'catalog/product/product.jpg', 7, 1, 100.0000, 0.0000, '0000-00-00', 0, 400, 9, '2009-02-03', 1.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 0, 1, 0, 1, 9, '2009-02-03 21:08:40', '2017-09-19 13:05:02', 0),
(48, 'product 20', 'test 1', '', '', '', '', '', 'test 2', 995, 5, 'catalog/product/product.jpg', 8, 1, 100.0000, 0.0000, '0000-00-00', 0, 0, 9, '2009-02-08', 1.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 2, 1, 1, 0, 1, 24, '2009-02-08 17:21:51', '2017-09-19 13:06:59', 0),
(49, 'SAM1', '', '', '', '', '', '', '', 0, 8, 'catalog/product/product.jpg', 0, 1, 199.9900, 0.0000, '0000-00-00', 0, 0, 9, '2011-04-25', 0.00000000, 1, 0.00000000, 0.00000000, 0.00000000, 1, 1, 1, 1, 1, 26, '2011-04-26 08:57:34', '2017-09-19 13:11:51', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_attribute`
--

CREATE TABLE `oc_product_attribute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_attribute`
--

INSERT INTO `oc_product_attribute` (`product_id`, `attribute_id`, `language_id`, `text`) VALUES
(42, 3, 1, '100mhz'),
(47, 4, 1, '16GB'),
(43, 4, 1, '8gb'),
(47, 2, 1, '4'),
(42, 3, 2, '100mhz'),
(47, 2, 2, '4'),
(43, 2, 2, '1'),
(47, 4, 2, '16GB'),
(43, 4, 2, '8gb'),
(43, 2, 1, '1');

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_description`
--

CREATE TABLE `oc_product_description` (
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `tag` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_description`
--

INSERT INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(35, 1, 'Product 8', '&lt;p&gt;\r\n	Product 8&lt;/p&gt;\r\n', '', 'Product 8', '', ''),
(48, 1, 'iPod Classic', '&lt;div class=&quot;cpt_product_description &quot;&gt;\r\n	&lt;div&gt;\r\n		&lt;p&gt;\r\n			&lt;strong&gt;More room to move.&lt;/strong&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			With 80GB or 160GB of storage and up to 40 hours of battery life, the new iPod classic lets you enjoy up to 40,000 songs or up to 200 hours of video or any combination wherever you go.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;strong&gt;Cover Flow.&lt;/strong&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Browse through your music collection by flipping through album art. Select an album to turn it over and see the track list.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;strong&gt;Enhanced interface.&lt;/strong&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Experience a whole new way to browse and view your music and video.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;strong&gt;Sleeker design.&lt;/strong&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Beautiful, durable, and sleeker than ever, iPod classic now features an anodized aluminum and polished stainless steel enclosure with rounded edges.&lt;/p&gt;\r\n	&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!-- cpt_container_end --&gt;', '', 'iPod Classic', '', ''),
(40, 2, 'iPhone', '&lt;p class=&quot;intro&quot;&gt;\r\n	iPhone is a revolutionary new mobile phone that allows you to make a call by simply tapping a name or number in your address book, a favorites list, or a call log. It also automatically syncs all your contacts from a PC, Mac, or Internet service. And it lets you select and listen to voicemail messages in whatever order you want just like email.&lt;/p&gt;\r\n', '', 'iPhone', '', ''),
(28, 2, 'HTC Touch HD', '&lt;p&gt;\r\n	HTC Touch - in High Definition. Watch music videos and streaming content in awe-inspiring high definition clarity for a mobile experience you never thought possible. Seductively sleek, the HTC Touch HD provides the next generation of mobile functionality, all at a simple touch. Fully integrated with Windows Mobile Professional 6.1, ultrafast 3.5G, GPS, 5MP camera, plus lots more - all delivered on a breathtakingly crisp 3.8&amp;quot; WVGA touchscreen - you can take control of your mobile world with the HTC Touch HD.&lt;/p&gt;\r\n&lt;p&gt;\r\n	&lt;strong&gt;Features&lt;/strong&gt;&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		Processor Qualcomm&amp;reg; MSM 7201A&amp;trade; 528 MHz&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Windows Mobile&amp;reg; 6.1 Professional Operating System&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Memory: 512 MB ROM, 288 MB RAM&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Dimensions: 115 mm x 62.8 mm x 12 mm / 146.4 grams&lt;/li&gt;\r\n	&lt;li&gt;\r\n		3.8-inch TFT-LCD flat touch-sensitive screen with 480 x 800 WVGA resolution&lt;/li&gt;\r\n	&lt;li&gt;\r\n		HSDPA/WCDMA: Europe/Asia: 900/2100 MHz; Up to 2 Mbps up-link and 7.2 Mbps down-link speeds&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Quad-band GSM/GPRS/EDGE: Europe/Asia: 850/900/1800/1900 MHz (Band frequency, HSUPA availability, and data speed are operator dependent.)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Device Control via HTC TouchFLO&amp;trade; 3D &amp;amp; Touch-sensitive front panel buttons&lt;/li&gt;\r\n	&lt;li&gt;\r\n		GPS and A-GPS ready&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Bluetooth&amp;reg; 2.0 with Enhanced Data Rate and A2DP for wireless stereo headsets&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Wi-Fi&amp;reg;: IEEE 802.11 b/g&lt;/li&gt;\r\n	&lt;li&gt;\r\n		HTC ExtUSB&amp;trade; (11-pin mini-USB 2.0)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		5 megapixel color camera with auto focus&lt;/li&gt;\r\n	&lt;li&gt;\r\n		VGA CMOS color camera&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Built-in 3.5 mm audio jack, microphone, speaker, and FM radio&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Ring tone formats: AAC, AAC+, eAAC+, AMR-NB, AMR-WB, QCP, MP3, WMA, WAV&lt;/li&gt;\r\n	&lt;li&gt;\r\n		40 polyphonic and standard MIDI format 0 and 1 (SMF)/SP MIDI&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Rechargeable Lithium-ion or Lithium-ion polymer 1350 mAh battery&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Expansion Slot: microSD&amp;trade; memory card (SD 2.0 compatible)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		AC Adapter Voltage range/frequency: 100 ~ 240V AC, 50/60 Hz DC output: 5V and 1A&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Special Features: FM Radio, G-Sensor&lt;/li&gt;\r\n&lt;/ul&gt;\r\n', '', '	 HTC Touch HD', '', ''),
(44, 1, 'MacBook Air', '&lt;div&gt;\r\n	MacBook Air is ultrathin, ultraportable, and ultra unlike anything else. But you don&amp;rsquo;t lose inches and pounds overnight. It&amp;rsquo;s the result of rethinking conventions. Of multiple wireless innovations. And of breakthrough design. With MacBook Air, mobile computing suddenly has a new standard.&lt;/div&gt;\r\n', '', 'MacBook Air', '', ''),
(45, 2, 'MacBook Pro', '&lt;div class=&quot;cpt_product_description &quot;&gt;\r\n	&lt;div&gt;\r\n		&lt;p&gt;\r\n			&lt;b&gt;Latest Intel mobile architecture&lt;/b&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Powered by the most advanced mobile processors from Intel, the new Core 2 Duo MacBook Pro is over 50% faster than the original Core Duo MacBook Pro and now supports up to 4GB of RAM.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;b&gt;Leading-edge graphics&lt;/b&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			The NVIDIA GeForce 8600M GT delivers exceptional graphics processing power. For the ultimate creative canvas, you can even configure the 17-inch model with a 1920-by-1200 resolution display.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;b&gt;Designed for life on the road&lt;/b&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Innovations such as a magnetic power connection and an illuminated keyboard with ambient light sensor put the MacBook Pro in a class by itself.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;b&gt;Connect. Create. Communicate.&lt;/b&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Quickly set up a video conference with the built-in iSight camera. Control presentations and media from up to 30 feet away with the included Apple Remote. Connect to high-bandwidth peripherals with FireWire 800 and DVI.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;b&gt;Next-generation wireless&lt;/b&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Featuring 802.11n wireless technology, the MacBook Pro delivers up to five times the performance and up to twice the range of previous-generation technologies.&lt;/p&gt;\r\n	&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!-- cpt_container_end --&gt;', '', 'MacBook Pro', '', ''),
(29, 1, 'Palm Treo Pro', '&lt;p&gt;\r\n	Redefine your workday with the Palm Treo Pro smartphone. Perfectly balanced, you can respond to business and personal email, stay on top of appointments and contacts, and use Wi-Fi or GPS when you&amp;rsquo;re out and about. Then watch a video on YouTube, catch up with news and sports on the web, or listen to a few songs. Balance your work and play the way you like it, with the Palm Treo Pro.&lt;/p&gt;\r\n&lt;p&gt;\r\n	&lt;strong&gt;Features&lt;/strong&gt;&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		Windows Mobile&amp;reg; 6.1 Professional Edition&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Qualcomm&amp;reg; MSM7201 400MHz Processor&lt;/li&gt;\r\n	&lt;li&gt;\r\n		320x320 transflective colour TFT touchscreen&lt;/li&gt;\r\n	&lt;li&gt;\r\n		HSDPA/UMTS/EDGE/GPRS/GSM radio&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Tri-band UMTS &amp;mdash; 850MHz, 1900MHz, 2100MHz&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Quad-band GSM &amp;mdash; 850/900/1800/1900&lt;/li&gt;\r\n	&lt;li&gt;\r\n		802.11b/g with WPA, WPA2, and 801.1x authentication&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Built-in GPS&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Bluetooth Version: 2.0 + Enhanced Data Rate&lt;/li&gt;\r\n	&lt;li&gt;\r\n		256MB storage (100MB user available), 128MB RAM&lt;/li&gt;\r\n	&lt;li&gt;\r\n		2.0 megapixel camera, up to 8x digital zoom and video capture&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Removable, rechargeable 1500mAh lithium-ion battery&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Up to 5.0 hours talk time and up to 250 hours standby&lt;/li&gt;\r\n	&lt;li&gt;\r\n		MicroSDHC card expansion (up to 32GB supported)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		MicroUSB 2.0 for synchronization and charging&lt;/li&gt;\r\n	&lt;li&gt;\r\n		3.5mm stereo headset jack&lt;/li&gt;\r\n	&lt;li&gt;\r\n		60mm (W) x 114mm (L) x 13.5mm (D) / 133g&lt;/li&gt;\r\n&lt;/ul&gt;\r\n', '', 'Palm Treo Pro', '', ''),
(36, 1, 'iPod Nano', '&lt;div&gt;\r\n	&lt;p&gt;\r\n		&lt;strong&gt;Video in your pocket.&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		Its the small iPod with one very big idea: video. The worlds most popular music player now lets you enjoy movies, TV shows, and more on a two-inch display thats 65% brighter than before.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;strong&gt;Cover Flow.&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		Browse through your music collection by flipping through album art. Select an album to turn it over and see the track list.&lt;strong&gt;&amp;nbsp;&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;strong&gt;Enhanced interface.&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		Experience a whole new way to browse and view your music and video.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;strong&gt;Sleek and colorful.&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		With an anodized aluminum and polished stainless steel enclosure and a choice of five colors, iPod nano is dressed to impress.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;strong&gt;iTunes.&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		Available as a free download, iTunes makes it easy to browse and buy millions of songs, movies, TV shows, audiobooks, and games and download free podcasts all at the iTunes Store. And you can import your own music, manage your whole media library, and sync your iPod or iPhone with ease.&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '', 'iPod Nano', '', ''),
(46, 1, 'Sony VAIO', '&lt;div&gt;\r\n	Unprecedented power. The next generation of processing technology has arrived. Built into the newest VAIO notebooks lies Intel&amp;#39;s latest, most powerful innovation yet: Intel&amp;reg; Centrino&amp;reg; 2 processor technology. Boasting incredible speed, expanded wireless connectivity, enhanced multimedia support and greater energy efficiency, all the high-performance essentials are seamlessly combined into a single chip.&lt;/div&gt;\r\n', '', 'Sony VAIO', '', ''),
(47, 2, 'HP LP3065', '&lt;p&gt;\r\n	Stop your co-workers in their tracks with the stunning new 30-inch diagonal HP LP3065 Flat Panel Monitor. This flagship monitor features best-in-class performance and presentation features on a huge wide-aspect screen while letting you work as comfortably as possible - you might even forget you&amp;#39;re at the office&lt;/p&gt;\r\n', '', 'HP LP3065', '', ''),
(32, 1, 'iPod Touch', '&lt;p&gt;\r\n	&lt;strong&gt;Revolutionary multi-touch interface.&lt;/strong&gt;&lt;br /&gt;\r\n	iPod touch features the same multi-touch screen technology as iPhone. Pinch to zoom in on a photo. Scroll through your songs and videos with a flick. Flip through your library by album artwork with Cover Flow.&lt;/p&gt;\r\n&lt;p&gt;\r\n	&lt;strong&gt;Gorgeous 3.5-inch widescreen display.&lt;/strong&gt;&lt;br /&gt;\r\n	Watch your movies, TV shows, and photos come alive with bright, vivid color on the 320-by-480-pixel display.&lt;/p&gt;\r\n&lt;p&gt;\r\n	&lt;strong&gt;Music downloads straight from iTunes.&lt;/strong&gt;&lt;br /&gt;\r\n	Shop the iTunes Wi-Fi Music Store from anywhere with Wi-Fi.1 Browse or search to find the music youre looking for, preview it, and buy it with just a tap.&lt;/p&gt;\r\n&lt;p&gt;\r\n	&lt;strong&gt;Surf the web with Wi-Fi.&lt;/strong&gt;&lt;br /&gt;\r\n	Browse the web using Safari and watch YouTube videos on the first iPod with Wi-Fi built in&lt;br /&gt;\r\n	&amp;nbsp;&lt;/p&gt;\r\n', '', '	 iPod Touch', '', ''),
(41, 2, 'iMac', '&lt;div&gt;\r\n	Just when you thought iMac had everything, now there´s even more. More powerful Intel Core 2 Duo processors. And more memory standard. Combine this with Mac OS X Leopard and iLife ´08, and it´s more all-in-one than ever. iMac packs amazing performance into a stunningly slim space.&lt;/div&gt;\r\n', '', 'iMac', '', ''),
(33, 1, 'Samsung SyncMaster 941BW', '&lt;div&gt;\r\n	Imagine the advantages of going big without slowing down. The big 19&amp;quot; 941BW monitor combines wide aspect ratio with fast pixel response time, for bigger images, more room to work and crisp motion. In addition, the exclusive MagicBright 2, MagicColor and MagicTune technologies help deliver the ideal image in every situation, while sleek, narrow bezels and adjustable stands deliver style just the way you want it. With the Samsung 941BW widescreen analog/digital LCD monitor, it&amp;#39;s not hard to imagine.&lt;/div&gt;\r\n', '', 'Samsung SyncMaster 941BW', '', ''),
(34, 1, 'iPod Shuffle', '&lt;div&gt;\r\n	&lt;strong&gt;Born to be worn.&lt;/strong&gt;\r\n	&lt;p&gt;\r\n		Clip on the worlds most wearable music player and take up to 240 songs with you anywhere. Choose from five colors including four new hues to make your musical fashion statement.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;strong&gt;Random meets rhythm.&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		With iTunes autofill, iPod shuffle can deliver a new musical experience every time you sync. For more randomness, you can shuffle songs during playback with the slide of a switch.&lt;/p&gt;\r\n	&lt;strong&gt;Everything is easy.&lt;/strong&gt;\r\n	&lt;p&gt;\r\n		Charge and sync with the included USB dock. Operate the iPod shuffle controls with one hand. Enjoy up to 12 hours straight of skip-free music playback.&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '', 'iPod Shuffle', '', ''),
(43, 1, 'MacBook', '&lt;div&gt;\r\n	&lt;p&gt;\r\n		&lt;b&gt;Intel Core 2 Duo processor&lt;/b&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		Powered by an Intel Core 2 Duo processor at speeds up to 2.16GHz, the new MacBook is the fastest ever.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;b&gt;1GB memory, larger hard drives&lt;/b&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		The new MacBook now comes with 1GB of memory standard and larger hard drives for the entire line perfect for running more of your favorite applications and storing growing media collections.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;b&gt;Sleek, 1.08-inch-thin design&lt;/b&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		MacBook makes it easy to hit the road thanks to its tough polycarbonate case, built-in wireless technologies, and innovative MagSafe Power Adapter that releases automatically if someone accidentally trips on the cord.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;b&gt;Built-in iSight camera&lt;/b&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		Right out of the box, you can have a video chat with friends or family,2 record a video at your desk, or take fun pictures with Photo Booth&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '', 'MacBook', '', ''),
(31, 2, 'Nikon D300', '&lt;div class=&quot;cpt_product_description &quot;&gt;\r\n	&lt;div&gt;\r\n		Engineered with pro-level features and performance, the 12.3-effective-megapixel D300 combines brand new technologies with advanced features inherited from Nikon&amp;#39;s newly announced D3 professional digital SLR camera to offer serious photographers remarkable performance combined with agility.&lt;br /&gt;\r\n		&lt;br /&gt;\r\n		Similar to the D3, the D300 features Nikon&amp;#39;s exclusive EXPEED Image Processing System that is central to driving the speed and processing power needed for many of the camera&amp;#39;s new features. The D300 features a new 51-point autofocus system with Nikon&amp;#39;s 3D Focus Tracking feature and two new LiveView shooting modes that allow users to frame a photograph using the camera&amp;#39;s high-resolution LCD monitor. The D300 shares a similar Scene Recognition System as is found in the D3; it promises to greatly enhance the accuracy of autofocus, autoexposure, and auto white balance by recognizing the subject or scene being photographed and applying this information to the calculations for the three functions.&lt;br /&gt;\r\n		&lt;br /&gt;\r\n		The D300 reacts with lightning speed, powering up in a mere 0.13 seconds and shooting with an imperceptible 45-millisecond shutter release lag time. The D300 is capable of shooting at a rapid six frames per second and can go as fast as eight frames per second when using the optional MB-D10 multi-power battery pack. In continuous bursts, the D300 can shoot up to 100 shots at full 12.3-megapixel resolution. (NORMAL-LARGE image setting, using a SanDisk Extreme IV 1GB CompactFlash card.)&lt;br /&gt;\r\n		&lt;br /&gt;\r\n		The D300 incorporates a range of innovative technologies and features that will significantly improve the accuracy, control, and performance photographers can get from their equipment. Its new Scene Recognition System advances the use of Nikon&amp;#39;s acclaimed 1,005-segment sensor to recognize colors and light patterns that help the camera determine the subject and the type of scene being photographed before a picture is taken. This information is used to improve the accuracy of autofocus, autoexposure, and auto white balance functions in the D300. For example, the camera can track moving subjects better and by identifying them, it can also automatically select focus points faster and with greater accuracy. It can also analyze highlights and more accurately determine exposure, as well as infer light sources to deliver more accurate white balance detection.&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!-- cpt_container_end --&gt;', '', 'Nikon D300', '', ''),
(49, 2, 'Samsung Galaxy Tab 10.1', '&lt;p&gt;\r\n	Samsung Galaxy Tab 10.1, is the world&amp;rsquo;s thinnest tablet, measuring 8.6 mm thickness, running with Android 3.0 Honeycomb OS on a 1GHz dual-core Tegra 2 processor, similar to its younger brother Samsung Galaxy Tab 8.9.&lt;/p&gt;\r\n&lt;p&gt;\r\n	Samsung Galaxy Tab 10.1 gives pure Android 3.0 experience, adding its new TouchWiz UX or TouchWiz 4.0 &amp;ndash; includes a live panel, which lets you to customize with different content, such as your pictures, bookmarks, and social feeds, sporting a 10.1 inches WXGA capacitive touch screen with 1280 x 800 pixels of resolution, equipped with 3 megapixel rear camera with LED flash and a 2 megapixel front camera, HSPA+ connectivity up to 21Mbps, 720p HD video recording capability, 1080p HD playback, DLNA support, Bluetooth 2.1, USB 2.0, gyroscope, Wi-Fi 802.11 a/b/g/n, micro-SD slot, 3.5mm headphone jack, and SIM slot, including the Samsung Stick &amp;ndash; a Bluetooth microphone that can be carried in a pocket like a pen and sound dock with powered subwoofer.&lt;/p&gt;\r\n&lt;p&gt;\r\n	Samsung Galaxy Tab 10.1 will come in 16GB / 32GB / 64GB verities and pre-loaded with Social Hub, Reader&amp;rsquo;s Hub, Music Hub and Samsung Mini Apps Tray &amp;ndash; which gives you access to more commonly used apps to help ease multitasking and it is capable of Adobe Flash Player 10.2, powered by 6860mAh battery that gives you 10hours of video-playback time.&amp;nbsp;&amp;auml;&amp;ouml;&lt;/p&gt;\r\n', '', 'Samsung Galaxy Tab 10.1', '', ''),
(49, 1, 'Samsung Galaxy Tab 10.1', '&lt;p&gt;\r\n	Samsung Galaxy Tab 10.1, is the world&amp;rsquo;s thinnest tablet, measuring 8.6 mm thickness, running with Android 3.0 Honeycomb OS on a 1GHz dual-core Tegra 2 processor, similar to its younger brother Samsung Galaxy Tab 8.9.&lt;/p&gt;\r\n&lt;p&gt;\r\n	Samsung Galaxy Tab 10.1 gives pure Android 3.0 experience, adding its new TouchWiz UX or TouchWiz 4.0 &amp;ndash; includes a live panel, which lets you to customize with different content, such as your pictures, bookmarks, and social feeds, sporting a 10.1 inches WXGA capacitive touch screen with 1280 x 800 pixels of resolution, equipped with 3 megapixel rear camera with LED flash and a 2 megapixel front camera, HSPA+ connectivity up to 21Mbps, 720p HD video recording capability, 1080p HD playback, DLNA support, Bluetooth 2.1, USB 2.0, gyroscope, Wi-Fi 802.11 a/b/g/n, micro-SD slot, 3.5mm headphone jack, and SIM slot, including the Samsung Stick &amp;ndash; a Bluetooth microphone that can be carried in a pocket like a pen and sound dock with powered subwoofer.&lt;/p&gt;\r\n&lt;p&gt;\r\n	Samsung Galaxy Tab 10.1 will come in 16GB / 32GB / 64GB verities and pre-loaded with Social Hub, Reader&amp;rsquo;s Hub, Music Hub and Samsung Mini Apps Tray &amp;ndash; which gives you access to more commonly used apps to help ease multitasking and it is capable of Adobe Flash Player 10.2, powered by 6860mAh battery that gives you 10hours of video-playback time.&amp;nbsp;&amp;auml;&amp;ouml;&lt;/p&gt;\r\n', '', 'Samsung Galaxy Tab 10.1', '', ''),
(30, 2, 'Canon EOS 5D', '&lt;p&gt;\r\n	Canon\'s press material for the EOS 5D states that it \'defines (a) new D-SLR category\', while we\'re not typically too concerned with marketing talk this particular statement is clearly pretty accurate. The EOS 5D is unlike any previous digital SLR in that it combines a full-frame (35 mm sized) high resolution sensor (12.8 megapixels) with a relatively compact body (slightly larger than the EOS 20D, although in your hand it feels noticeably \'chunkier\'). The EOS 5D is aimed to slot in between the EOS 20D and the EOS-1D professional digital SLR\'s, an important difference when compared to the latter is that the EOS 5D doesn\'t have any environmental seals. While Canon don\'t specifically refer to the EOS 5D as a \'professional\' digital SLR it will have obvious appeal to professionals who want a high quality digital SLR in a body lighter than the EOS-1D. It will also no doubt appeal to current EOS 20D owners (although lets hope they\'ve not bought too many EF-S lenses...) äë&lt;/p&gt;\r\n', '', 'sdf', '', ''),
(35, 2, 'Product 8', '&lt;p&gt;\r\n	Product 8&lt;/p&gt;\r\n', '', 'Product 8', '', ''),
(48, 2, 'iPod Classic', '&lt;div class=&quot;cpt_product_description &quot;&gt;\r\n	&lt;div&gt;\r\n		&lt;p&gt;\r\n			&lt;strong&gt;More room to move.&lt;/strong&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			With 80GB or 160GB of storage and up to 40 hours of battery life, the new iPod classic lets you enjoy up to 40,000 songs or up to 200 hours of video or any combination wherever you go.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;strong&gt;Cover Flow.&lt;/strong&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Browse through your music collection by flipping through album art. Select an album to turn it over and see the track list.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;strong&gt;Enhanced interface.&lt;/strong&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Experience a whole new way to browse and view your music and video.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;strong&gt;Sleeker design.&lt;/strong&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Beautiful, durable, and sleeker than ever, iPod classic now features an anodized aluminum and polished stainless steel enclosure with rounded edges.&lt;/p&gt;\r\n	&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!-- cpt_container_end --&gt;', '', 'iPod Classic', '', ''),
(40, 1, 'iPhone', '&lt;p class=&quot;intro&quot;&gt;\r\n	iPhone is a revolutionary new mobile phone that allows you to make a call by simply tapping a name or number in your address book, a favorites list, or a call log. It also automatically syncs all your contacts from a PC, Mac, or Internet service. And it lets you select and listen to voicemail messages in whatever order you want just like email.&lt;/p&gt;\r\n', '', 'iPhone', '', ''),
(28, 1, 'HTC Touch HD', '&lt;p&gt;\r\n	HTC Touch - in High Definition. Watch music videos and streaming content in awe-inspiring high definition clarity for a mobile experience you never thought possible. Seductively sleek, the HTC Touch HD provides the next generation of mobile functionality, all at a simple touch. Fully integrated with Windows Mobile Professional 6.1, ultrafast 3.5G, GPS, 5MP camera, plus lots more - all delivered on a breathtakingly crisp 3.8&amp;quot; WVGA touchscreen - you can take control of your mobile world with the HTC Touch HD.&lt;/p&gt;\r\n&lt;p&gt;\r\n	&lt;strong&gt;Features&lt;/strong&gt;&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		Processor Qualcomm&amp;reg; MSM 7201A&amp;trade; 528 MHz&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Windows Mobile&amp;reg; 6.1 Professional Operating System&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Memory: 512 MB ROM, 288 MB RAM&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Dimensions: 115 mm x 62.8 mm x 12 mm / 146.4 grams&lt;/li&gt;\r\n	&lt;li&gt;\r\n		3.8-inch TFT-LCD flat touch-sensitive screen with 480 x 800 WVGA resolution&lt;/li&gt;\r\n	&lt;li&gt;\r\n		HSDPA/WCDMA: Europe/Asia: 900/2100 MHz; Up to 2 Mbps up-link and 7.2 Mbps down-link speeds&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Quad-band GSM/GPRS/EDGE: Europe/Asia: 850/900/1800/1900 MHz (Band frequency, HSUPA availability, and data speed are operator dependent.)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Device Control via HTC TouchFLO&amp;trade; 3D &amp;amp; Touch-sensitive front panel buttons&lt;/li&gt;\r\n	&lt;li&gt;\r\n		GPS and A-GPS ready&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Bluetooth&amp;reg; 2.0 with Enhanced Data Rate and A2DP for wireless stereo headsets&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Wi-Fi&amp;reg;: IEEE 802.11 b/g&lt;/li&gt;\r\n	&lt;li&gt;\r\n		HTC ExtUSB&amp;trade; (11-pin mini-USB 2.0)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		5 megapixel color camera with auto focus&lt;/li&gt;\r\n	&lt;li&gt;\r\n		VGA CMOS color camera&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Built-in 3.5 mm audio jack, microphone, speaker, and FM radio&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Ring tone formats: AAC, AAC+, eAAC+, AMR-NB, AMR-WB, QCP, MP3, WMA, WAV&lt;/li&gt;\r\n	&lt;li&gt;\r\n		40 polyphonic and standard MIDI format 0 and 1 (SMF)/SP MIDI&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Rechargeable Lithium-ion or Lithium-ion polymer 1350 mAh battery&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Expansion Slot: microSD&amp;trade; memory card (SD 2.0 compatible)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		AC Adapter Voltage range/frequency: 100 ~ 240V AC, 50/60 Hz DC output: 5V and 1A&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Special Features: FM Radio, G-Sensor&lt;/li&gt;\r\n&lt;/ul&gt;\r\n', '', '	 HTC Touch HD', '', ''),
(44, 2, 'MacBook Air', '&lt;div&gt;\r\n	MacBook Air is ultrathin, ultraportable, and ultra unlike anything else. But you don&amp;rsquo;t lose inches and pounds overnight. It&amp;rsquo;s the result of rethinking conventions. Of multiple wireless innovations. And of breakthrough design. With MacBook Air, mobile computing suddenly has a new standard.&lt;/div&gt;\r\n', '', 'MacBook Air', '', ''),
(45, 1, 'MacBook Pro', '&lt;div class=&quot;cpt_product_description &quot;&gt;\r\n	&lt;div&gt;\r\n		&lt;p&gt;\r\n			&lt;b&gt;Latest Intel mobile architecture&lt;/b&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Powered by the most advanced mobile processors from Intel, the new Core 2 Duo MacBook Pro is over 50% faster than the original Core Duo MacBook Pro and now supports up to 4GB of RAM.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;b&gt;Leading-edge graphics&lt;/b&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			The NVIDIA GeForce 8600M GT delivers exceptional graphics processing power. For the ultimate creative canvas, you can even configure the 17-inch model with a 1920-by-1200 resolution display.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;b&gt;Designed for life on the road&lt;/b&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Innovations such as a magnetic power connection and an illuminated keyboard with ambient light sensor put the MacBook Pro in a class by itself.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;b&gt;Connect. Create. Communicate.&lt;/b&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Quickly set up a video conference with the built-in iSight camera. Control presentations and media from up to 30 feet away with the included Apple Remote. Connect to high-bandwidth peripherals with FireWire 800 and DVI.&lt;/p&gt;\r\n		&lt;p&gt;\r\n			&lt;b&gt;Next-generation wireless&lt;/b&gt;&lt;/p&gt;\r\n		&lt;p&gt;\r\n			Featuring 802.11n wireless technology, the MacBook Pro delivers up to five times the performance and up to twice the range of previous-generation technologies.&lt;/p&gt;\r\n	&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!-- cpt_container_end --&gt;', '', 'MacBook Pro', '', ''),
(29, 2, 'Palm Treo Pro', '&lt;p&gt;\r\n	Redefine your workday with the Palm Treo Pro smartphone. Perfectly balanced, you can respond to business and personal email, stay on top of appointments and contacts, and use Wi-Fi or GPS when you&amp;rsquo;re out and about. Then watch a video on YouTube, catch up with news and sports on the web, or listen to a few songs. Balance your work and play the way you like it, with the Palm Treo Pro.&lt;/p&gt;\r\n&lt;p&gt;\r\n	&lt;strong&gt;Features&lt;/strong&gt;&lt;/p&gt;\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n		Windows Mobile&amp;reg; 6.1 Professional Edition&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Qualcomm&amp;reg; MSM7201 400MHz Processor&lt;/li&gt;\r\n	&lt;li&gt;\r\n		320x320 transflective colour TFT touchscreen&lt;/li&gt;\r\n	&lt;li&gt;\r\n		HSDPA/UMTS/EDGE/GPRS/GSM radio&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Tri-band UMTS &amp;mdash; 850MHz, 1900MHz, 2100MHz&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Quad-band GSM &amp;mdash; 850/900/1800/1900&lt;/li&gt;\r\n	&lt;li&gt;\r\n		802.11b/g with WPA, WPA2, and 801.1x authentication&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Built-in GPS&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Bluetooth Version: 2.0 + Enhanced Data Rate&lt;/li&gt;\r\n	&lt;li&gt;\r\n		256MB storage (100MB user available), 128MB RAM&lt;/li&gt;\r\n	&lt;li&gt;\r\n		2.0 megapixel camera, up to 8x digital zoom and video capture&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Removable, rechargeable 1500mAh lithium-ion battery&lt;/li&gt;\r\n	&lt;li&gt;\r\n		Up to 5.0 hours talk time and up to 250 hours standby&lt;/li&gt;\r\n	&lt;li&gt;\r\n		MicroSDHC card expansion (up to 32GB supported)&lt;/li&gt;\r\n	&lt;li&gt;\r\n		MicroUSB 2.0 for synchronization and charging&lt;/li&gt;\r\n	&lt;li&gt;\r\n		3.5mm stereo headset jack&lt;/li&gt;\r\n	&lt;li&gt;\r\n		60mm (W) x 114mm (L) x 13.5mm (D) / 133g&lt;/li&gt;\r\n&lt;/ul&gt;\r\n', '', 'Palm Treo Pro', '', ''),
(36, 2, 'iPod Nano', '&lt;div&gt;\r\n	&lt;p&gt;\r\n		&lt;strong&gt;Video in your pocket.&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		Its the small iPod with one very big idea: video. The worlds most popular music player now lets you enjoy movies, TV shows, and more on a two-inch display thats 65% brighter than before.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;strong&gt;Cover Flow.&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		Browse through your music collection by flipping through album art. Select an album to turn it over and see the track list.&lt;strong&gt;&amp;nbsp;&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;strong&gt;Enhanced interface.&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		Experience a whole new way to browse and view your music and video.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;strong&gt;Sleek and colorful.&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		With an anodized aluminum and polished stainless steel enclosure and a choice of five colors, iPod nano is dressed to impress.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;strong&gt;iTunes.&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		Available as a free download, iTunes makes it easy to browse and buy millions of songs, movies, TV shows, audiobooks, and games and download free podcasts all at the iTunes Store. And you can import your own music, manage your whole media library, and sync your iPod or iPhone with ease.&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '', 'iPod Nano', '', ''),
(46, 2, 'Sony VAIO', '&lt;div&gt;\r\n	Unprecedented power. The next generation of processing technology has arrived. Built into the newest VAIO notebooks lies Intel&amp;#39;s latest, most powerful innovation yet: Intel&amp;reg; Centrino&amp;reg; 2 processor technology. Boasting incredible speed, expanded wireless connectivity, enhanced multimedia support and greater energy efficiency, all the high-performance essentials are seamlessly combined into a single chip.&lt;/div&gt;\r\n', '', 'Sony VAIO', '', ''),
(47, 1, 'HP LP3065', '&lt;p&gt;\r\n	Stop your co-workers in their tracks with the stunning new 30-inch diagonal HP LP3065 Flat Panel Monitor. This flagship monitor features best-in-class performance and presentation features on a huge wide-aspect screen while letting you work as comfortably as possible - you might even forget you&amp;#39;re at the office&lt;/p&gt;\r\n', '', 'HP LP3065', '', ''),
(32, 2, 'iPod Touch', '&lt;p&gt;\r\n	&lt;strong&gt;Revolutionary multi-touch interface.&lt;/strong&gt;&lt;br /&gt;\r\n	iPod touch features the same multi-touch screen technology as iPhone. Pinch to zoom in on a photo. Scroll through your songs and videos with a flick. Flip through your library by album artwork with Cover Flow.&lt;/p&gt;\r\n&lt;p&gt;\r\n	&lt;strong&gt;Gorgeous 3.5-inch widescreen display.&lt;/strong&gt;&lt;br /&gt;\r\n	Watch your movies, TV shows, and photos come alive with bright, vivid color on the 320-by-480-pixel display.&lt;/p&gt;\r\n&lt;p&gt;\r\n	&lt;strong&gt;Music downloads straight from iTunes.&lt;/strong&gt;&lt;br /&gt;\r\n	Shop the iTunes Wi-Fi Music Store from anywhere with Wi-Fi.1 Browse or search to find the music youre looking for, preview it, and buy it with just a tap.&lt;/p&gt;\r\n&lt;p&gt;\r\n	&lt;strong&gt;Surf the web with Wi-Fi.&lt;/strong&gt;&lt;br /&gt;\r\n	Browse the web using Safari and watch YouTube videos on the first iPod with Wi-Fi built in&lt;br /&gt;\r\n	&amp;nbsp;&lt;/p&gt;\r\n', '', '	 iPod Touch', '', ''),
(41, 1, 'iMac', '&lt;div&gt;\r\n	Just when you thought iMac had everything, now there´s even more. More powerful Intel Core 2 Duo processors. And more memory standard. Combine this with Mac OS X Leopard and iLife ´08, and it´s more all-in-one than ever. iMac packs amazing performance into a stunningly slim space.&lt;/div&gt;\r\n', '', 'iMac', '', ''),
(33, 2, 'Samsung SyncMaster 941BW', '&lt;div&gt;\r\n	Imagine the advantages of going big without slowing down. The big 19&amp;quot; 941BW monitor combines wide aspect ratio with fast pixel response time, for bigger images, more room to work and crisp motion. In addition, the exclusive MagicBright 2, MagicColor and MagicTune technologies help deliver the ideal image in every situation, while sleek, narrow bezels and adjustable stands deliver style just the way you want it. With the Samsung 941BW widescreen analog/digital LCD monitor, it&amp;#39;s not hard to imagine.&lt;/div&gt;\r\n', '', 'Samsung SyncMaster 941BW', '', ''),
(34, 2, 'iPod Shuffle', '&lt;div&gt;\r\n	&lt;strong&gt;Born to be worn.&lt;/strong&gt;\r\n	&lt;p&gt;\r\n		Clip on the worlds most wearable music player and take up to 240 songs with you anywhere. Choose from five colors including four new hues to make your musical fashion statement.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;strong&gt;Random meets rhythm.&lt;/strong&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		With iTunes autofill, iPod shuffle can deliver a new musical experience every time you sync. For more randomness, you can shuffle songs during playback with the slide of a switch.&lt;/p&gt;\r\n	&lt;strong&gt;Everything is easy.&lt;/strong&gt;\r\n	&lt;p&gt;\r\n		Charge and sync with the included USB dock. Operate the iPod shuffle controls with one hand. Enjoy up to 12 hours straight of skip-free music playback.&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '', 'iPod Shuffle', '', ''),
(43, 2, 'MacBook', '&lt;div&gt;\r\n	&lt;p&gt;\r\n		&lt;b&gt;Intel Core 2 Duo processor&lt;/b&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		Powered by an Intel Core 2 Duo processor at speeds up to 2.16GHz, the new MacBook is the fastest ever.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;b&gt;1GB memory, larger hard drives&lt;/b&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		The new MacBook now comes with 1GB of memory standard and larger hard drives for the entire line perfect for running more of your favorite applications and storing growing media collections.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;b&gt;Sleek, 1.08-inch-thin design&lt;/b&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		MacBook makes it easy to hit the road thanks to its tough polycarbonate case, built-in wireless technologies, and innovative MagSafe Power Adapter that releases automatically if someone accidentally trips on the cord.&lt;/p&gt;\r\n	&lt;p&gt;\r\n		&lt;b&gt;Built-in iSight camera&lt;/b&gt;&lt;/p&gt;\r\n	&lt;p&gt;\r\n		Right out of the box, you can have a video chat with friends or family,2 record a video at your desk, or take fun pictures with Photo Booth&lt;/p&gt;\r\n&lt;/div&gt;\r\n', '', 'MacBook', '', ''),
(31, 1, 'Nikon D300', '&lt;div class=&quot;cpt_product_description &quot;&gt;\r\n	&lt;div&gt;\r\n		Engineered with pro-level features and performance, the 12.3-effective-megapixel D300 combines brand new technologies with advanced features inherited from Nikon&amp;#39;s newly announced D3 professional digital SLR camera to offer serious photographers remarkable performance combined with agility.&lt;br /&gt;\r\n		&lt;br /&gt;\r\n		Similar to the D3, the D300 features Nikon&amp;#39;s exclusive EXPEED Image Processing System that is central to driving the speed and processing power needed for many of the camera&amp;#39;s new features. The D300 features a new 51-point autofocus system with Nikon&amp;#39;s 3D Focus Tracking feature and two new LiveView shooting modes that allow users to frame a photograph using the camera&amp;#39;s high-resolution LCD monitor. The D300 shares a similar Scene Recognition System as is found in the D3; it promises to greatly enhance the accuracy of autofocus, autoexposure, and auto white balance by recognizing the subject or scene being photographed and applying this information to the calculations for the three functions.&lt;br /&gt;\r\n		&lt;br /&gt;\r\n		The D300 reacts with lightning speed, powering up in a mere 0.13 seconds and shooting with an imperceptible 45-millisecond shutter release lag time. The D300 is capable of shooting at a rapid six frames per second and can go as fast as eight frames per second when using the optional MB-D10 multi-power battery pack. In continuous bursts, the D300 can shoot up to 100 shots at full 12.3-megapixel resolution. (NORMAL-LARGE image setting, using a SanDisk Extreme IV 1GB CompactFlash card.)&lt;br /&gt;\r\n		&lt;br /&gt;\r\n		The D300 incorporates a range of innovative technologies and features that will significantly improve the accuracy, control, and performance photographers can get from their equipment. Its new Scene Recognition System advances the use of Nikon&amp;#39;s acclaimed 1,005-segment sensor to recognize colors and light patterns that help the camera determine the subject and the type of scene being photographed before a picture is taken. This information is used to improve the accuracy of autofocus, autoexposure, and auto white balance functions in the D300. For example, the camera can track moving subjects better and by identifying them, it can also automatically select focus points faster and with greater accuracy. It can also analyze highlights and more accurately determine exposure, as well as infer light sources to deliver more accurate white balance detection.&lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;!-- cpt_container_end --&gt;', '', 'Nikon D300', '', ''),
(42, 2, 'Apple Cinema 30', '&lt;p&gt;\r\n	The 30-inch Apple Cinema HD Display delivers an amazing 2560 x 1600 pixel resolution. Designed specifically for the creative professional, this display provides more space for easier access to all the tools and palettes needed to edit, format and composite your work. Combine this display with a Mac Pro, MacBook Pro, or PowerMac G5 and there\'s no limit to what you can achieve. &lt;br&gt;\r\n	&lt;br&gt;\r\n	The Cinema HD features an active-matrix liquid crystal display that produces flicker-free images that deliver twice the brightness, twice the sharpness and twice the contrast ratio of a typical CRT display. Unlike other flat panels, it\'s designed with a pure digital interface to deliver distortion-free images that never need adjusting. With over 4 million digital pixels, the display is uniquely suited for scientific and technical applications such as visualizing molecular structures or analyzing geological data. &lt;br&gt;\r\n	&lt;br&gt;\r\n	Offering accurate, brilliant color performance, the Cinema HD delivers up to 16.7 million colors across a wide gamut allowing you to see subtle nuances between colors from soft pastels to rich jewel tones. A wide viewing angle ensures uniform color from edge to edge. Apple\'s ColorSync technology allows you to create custom profiles to maintain consistent color onscreen and in print. The result: You can confidently use this display in all your color-critical applications. &lt;br&gt;\r\n	&lt;br&gt;\r\n	Housed in a new aluminum design, the display has a very thin bezel that enhances visual accuracy. Each display features two FireWire 400 ports and two USB 2.0 ports, making attachment of desktop peripherals, such as iSight, iPod, digital and still cameras, hard drives, printers and scanners, even more accessible and convenient. Taking advantage of the much thinner and lighter footprint of an LCD, the new displays support the VESA (Video Electronics Standards Association) mounting interface standard. Customers with the optional Cinema Display VESA Mount Adapter kit gain the flexibility to mount their display in locations most appropriate for their work environment. &lt;br&gt;\r\n	&lt;br&gt;\r\n	The Cinema HD features a single cable design with elegant breakout for the USB 2.0, FireWire 400 and a pure digital connection using the industry standard Digital Video Interface (DVI) interface. The DVI connection allows for a direct pure-digital connection.&lt;br&gt;\r\n	&lt;/p&gt;\r\n\r\n', '', 'Apple Cinema 30', '', ''),
(30, 1, 'Canon EOS 5D', '&lt;p&gt;\r\n	Canon\'s press material for the EOS 5D states that it \'defines (a) new D-SLR category\', while we\'re not typically too concerned with marketing talk this particular statement is clearly pretty accurate. The EOS 5D is unlike any previous digital SLR in that it combines a full-frame (35 mm sized) high resolution sensor (12.8 megapixels) with a relatively compact body (slightly larger than the EOS 20D, although in your hand it feels noticeably \'chunkier\'). The EOS 5D is aimed to slot in between the EOS 20D and the EOS-1D professional digital SLR\'s, an important difference when compared to the latter is that the EOS 5D doesn\'t have any environmental seals. While Canon don\'t specifically refer to the EOS 5D as a \'professional\' digital SLR it will have obvious appeal to professionals who want a high quality digital SLR in a body lighter than the EOS-1D. It will also no doubt appeal to current EOS 20D owners (although lets hope they\'ve not bought too many EF-S lenses...) äë&lt;/p&gt;\r\n', '', 'sdf', '', ''),
(42, 1, 'Apple Cinema 30', '&lt;p&gt;\r\n	The 30-inch Apple Cinema HD Display delivers an amazing 2560 x 1600 pixel resolution. Designed specifically for the creative professional, this display provides more space for easier access to all the tools and palettes needed to edit, format and composite your work. Combine this display with a Mac Pro, MacBook Pro, or PowerMac G5 and there\'s no limit to what you can achieve. &lt;br&gt;\r\n	&lt;br&gt;\r\n	The Cinema HD features an active-matrix liquid crystal display that produces flicker-free images that deliver twice the brightness, twice the sharpness and twice the contrast ratio of a typical CRT display. Unlike other flat panels, it\'s designed with a pure digital interface to deliver distortion-free images that never need adjusting. With over 4 million digital pixels, the display is uniquely suited for scientific and technical applications such as visualizing molecular structures or analyzing geological data. &lt;br&gt;\r\n	&lt;br&gt;\r\n	Offering accurate, brilliant color performance, the Cinema HD delivers up to 16.7 million colors across a wide gamut allowing you to see subtle nuances between colors from soft pastels to rich jewel tones. A wide viewing angle ensures uniform color from edge to edge. Apple\'s ColorSync technology allows you to create custom profiles to maintain consistent color onscreen and in print. The result: You can confidently use this display in all your color-critical applications. &lt;br&gt;\r\n	&lt;br&gt;\r\n	Housed in a new aluminum design, the display has a very thin bezel that enhances visual accuracy. Each display features two FireWire 400 ports and two USB 2.0 ports, making attachment of desktop peripherals, such as iSight, iPod, digital and still cameras, hard drives, printers and scanners, even more accessible and convenient. Taking advantage of the much thinner and lighter footprint of an LCD, the new displays support the VESA (Video Electronics Standards Association) mounting interface standard. Customers with the optional Cinema Display VESA Mount Adapter kit gain the flexibility to mount their display in locations most appropriate for their work environment. &lt;br&gt;\r\n	&lt;br&gt;\r\n	The Cinema HD features a single cable design with elegant breakout for the USB 2.0, FireWire 400 and a pure digital connection using the industry standard Digital Video Interface (DVI) interface. The DVI connection allows for a direct pure-digital connection.&lt;br&gt;\r\n	&lt;/p&gt;\r\n\r\n', '', 'Apple Cinema 30', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_discount`
--

CREATE TABLE `oc_product_discount` (
  `product_discount_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_discount`
--

INSERT INTO `oc_product_discount` (`product_discount_id`, `product_id`, `customer_group_id`, `quantity`, `priority`, `price`, `date_start`, `date_end`) VALUES
(488, 42, 1, 30, 1, 66.0000, '0000-00-00', '0000-00-00'),
(487, 42, 1, 20, 1, 77.0000, '0000-00-00', '0000-00-00'),
(486, 42, 1, 10, 1, 88.0000, '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_filter`
--

CREATE TABLE `oc_product_filter` (
  `product_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_image`
--

CREATE TABLE `oc_product_image` (
  `product_image_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_image`
--

INSERT INTO `oc_product_image` (`product_image_id`, `product_id`, `image`, `sort_order`) VALUES
(2553, 41, 'catalog/product/product.jpg', 0),
(2557, 40, 'catalog/product/product.jpg', 0),
(2564, 36, 'catalog/product/product.jpg', 0),
(2568, 34, 'catalog/product/product.jpg', 0),
(2573, 32, 'catalog/product/product.jpg', 0),
(2628, 43, 'catalog/product/product.jpg', 0),
(2631, 44, 'catalog/product/product.jpg', 0),
(2589, 45, 'catalog/product/product.jpg', 0),
(2592, 31, 'catalog/product/product.jpg', 0),
(2646, 46, 'catalog/product/product.jpg', 0),
(2561, 48, 'catalog/product/product.jpg', 0),
(2556, 40, 'catalog/product/product.jpg', 0),
(2546, 30, 'catalog/product/product.jpg', 0),
(2548, 47, 'catalog/product/product.jpg', 0),
(2550, 28, 'catalog/product/product.jpg', 0),
(2555, 40, 'catalog/product/product.jpg', 0),
(2560, 48, 'catalog/product/product.jpg', 0),
(2563, 36, 'catalog/product/product.jpg', 0),
(2567, 34, 'catalog/product/product.jpg', 0),
(2572, 32, 'catalog/product/product.jpg', 0),
(2571, 32, 'catalog/product/product.jpg', 0),
(2627, 43, 'catalog/product/product.jpg', 0),
(2630, 44, 'catalog/product/product.jpg', 0),
(2588, 45, 'catalog/product/product.jpg', 0),
(2591, 31, 'catalog/product/product.jpg', 0),
(2595, 29, 'catalog/product/product.jpg', 0),
(2645, 46, 'catalog/product/product.jpg', 0),
(2601, 49, 'catalog/product/product.jpg', 0),
(2600, 49, 'catalog/product/product.jpg', 0),
(2599, 49, 'catalog/product/product.jpg', 0),
(2666, 42, 'catalog/product/product.jpg', 3),
(2665, 42, 'catalog/product/product.jpg', 2),
(2552, 41, 'catalog/product/product.jpg', 0),
(2597, 35, 'catalog/product/product.jpg', 0),
(2579, 33, 'catalog/product/product.jpg', 0),
(2667, 42, 'catalog/product/product.jpg', 4),
(2547, 30, 'catalog/product/product.jpg', 0),
(2549, 47, 'catalog/product/product.jpg', 0),
(2551, 28, 'catalog/product/product.jpg', 0),
(2554, 41, 'catalog/product/product.jpg', 0),
(2558, 40, 'catalog/product/product.jpg', 0),
(2559, 40, 'catalog/product/product.jpg', 0),
(2562, 48, 'catalog/product/product.jpg', 0),
(2565, 36, 'catalog/product/product.jpg', 0),
(2566, 36, 'catalog/product/product.jpg', 0),
(2569, 34, 'catalog/product/product.jpg', 0),
(2570, 34, 'catalog/product/product.jpg', 0),
(2574, 32, 'catalog/product/product.jpg', 0),
(2644, 46, 'catalog/product/product.jpg', 0),
(2643, 46, 'catalog/product/product.jpg', 0),
(2580, 33, 'catalog/product/product.jpg', 0),
(2626, 43, 'catalog/product/product.jpg', 0),
(2625, 43, 'catalog/product/product.jpg', 0),
(2629, 44, 'catalog/product/product.jpg', 0),
(2590, 45, 'catalog/product/product.jpg', 0),
(2593, 31, 'catalog/product/product.jpg', 0),
(2594, 31, 'catalog/product/product.jpg', 0),
(2596, 29, 'catalog/product/product.jpg', 0),
(2598, 35, 'catalog/product/product.jpg', 0),
(2602, 49, 'catalog/product/product.jpg', 0),
(2603, 49, 'catalog/product/product.jpg', 0),
(2604, 49, 'catalog/product/product.jpg', 0),
(2664, 42, 'catalog/product/product.jpg', 1),
(2663, 42, 'catalog/product/product.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_option`
--

CREATE TABLE `oc_product_option` (
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_option`
--

INSERT INTO `oc_product_option` (`product_option_id`, `product_id`, `option_id`, `value`, `required`) VALUES
(224, 35, 11, '', 1),
(225, 47, 12, '2011-04-22', 1),
(222, 42, 7, '', 1),
(226, 30, 5, '', 1),
(218, 42, 1, '', 1),
(208, 42, 4, 'test', 1),
(217, 42, 5, '', 1),
(219, 42, 8, '2011-02-20', 1),
(209, 42, 6, '', 1),
(220, 42, 10, '2011-02-20 22:25', 1),
(223, 42, 2, '', 1),
(221, 42, 9, '22:25', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_option_value`
--

CREATE TABLE `oc_product_option_value` (
  `product_option_value_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  `quantity` int(3) NOT NULL,
  `subtract` tinyint(1) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `price_prefix` varchar(1) NOT NULL,
  `points` int(8) NOT NULL,
  `points_prefix` varchar(1) NOT NULL,
  `weight` decimal(15,8) NOT NULL,
  `weight_prefix` varchar(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_option_value`
--

INSERT INTO `oc_product_option_value` (`product_option_value_id`, `product_option_id`, `product_id`, `option_id`, `option_value_id`, `quantity`, `subtract`, `price`, `price_prefix`, `points`, `points_prefix`, `weight`, `weight_prefix`) VALUES
(7, 218, 42, 1, 43, 300, 1, 30.0000, '+', 3, '+', 30.00000000, '+'),
(6, 218, 42, 1, 31, 146, 1, 20.0000, '+', 2, '-', 20.00000000, '+'),
(5, 218, 42, 1, 32, 96, 1, 10.0000, '+', 1, '+', 10.00000000, '+'),
(14, 224, 35, 11, 48, 15, 1, 15.0000, '+', 0, '+', 0.00000000, '+'),
(13, 224, 35, 11, 47, 10, 1, 10.0000, '+', 0, '+', 0.00000000, '+'),
(16, 226, 30, 5, 40, 5, 1, 0.0000, '+', 0, '+', 0.00000000, '+'),
(15, 226, 30, 5, 39, 2, 1, 0.0000, '+', 0, '+', 0.00000000, '+'),
(12, 224, 35, 11, 46, 0, 1, 5.0000, '+', 0, '+', 0.00000000, '+'),
(2, 217, 42, 5, 42, 200, 1, 2.0000, '+', 0, '+', 2.00000000, '+'),
(1, 217, 42, 5, 41, 100, 0, 1.0000, '+', 0, '+', 1.00000000, '+'),
(3, 217, 42, 5, 40, 300, 0, 3.0000, '+', 0, '+', 3.00000000, '+'),
(4, 217, 42, 5, 39, 92, 1, 4.0000, '+', 0, '+', 4.00000000, '+'),
(10, 223, 42, 2, 44, 2696, 1, 30.0000, '+', 0, '+', 30.00000000, '+'),
(9, 223, 42, 2, 24, 194, 1, 20.0000, '+', 0, '+', 20.00000000, '+'),
(8, 223, 42, 2, 23, 48, 1, 10.0000, '+', 0, '+', 10.00000000, '+'),
(11, 223, 42, 2, 23, 3998, 1, 40.0000, '+', 0, '+', 40.00000000, '+');

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_recurring`
--

CREATE TABLE `oc_product_recurring` (
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_related`
--

CREATE TABLE `oc_product_related` (
  `product_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_related`
--

INSERT INTO `oc_product_related` (`product_id`, `related_id`) VALUES
(40, 42),
(41, 42),
(42, 40),
(42, 41),
(42, 42),
(42, 44),
(42, 45),
(42, 47),
(44, 42),
(45, 42),
(47, 42);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_reward`
--

CREATE TABLE `oc_product_reward` (
  `product_reward_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `points` int(8) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_reward`
--

INSERT INTO `oc_product_reward` (`product_reward_id`, `product_id`, `customer_group_id`, `points`) VALUES
(587, 42, 1, 100),
(571, 47, 1, 300),
(572, 28, 1, 400),
(581, 43, 1, 600),
(570, 30, 1, 200),
(582, 44, 1, 700),
(575, 45, 1, 800),
(576, 49, 1, 1000);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_special`
--

CREATE TABLE `oc_product_special` (
  `product_special_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_special`
--

INSERT INTO `oc_product_special` (`product_special_id`, `product_id`, `customer_group_id`, `priority`, `price`, `date_start`, `date_end`) VALUES
(484, 42, 1, 1, 90.0000, '0000-00-00', '0000-00-00'),
(463, 30, 1, 2, 90.0000, '0000-00-00', '0000-00-00'),
(462, 30, 1, 1, 80.0000, '0000-00-00', '0000-00-00'),
(469, 33, 1, 0, 9.0000, '0000-00-00', '0000-00-00'),
(473, 49, 1, 0, 5.0000, '0000-00-00', '0000-00-00'),
(472, 35, 1, 0, 9.0000, '0000-00-00', '0000-00-00'),
(471, 29, 1, 0, 40.0000, '0000-00-00', '0000-00-00'),
(478, 43, 1, 0, 10.0000, '0000-00-00', '0000-00-00'),
(467, 32, 1, 0, 9.0000, '0000-00-00', '0000-00-00'),
(466, 34, 1, 0, 5.0000, '0000-00-00', '0000-00-00'),
(465, 36, 1, 0, 9.0000, '0000-00-00', '0000-00-00'),
(464, 48, 1, 0, 40.0000, '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_category`
--

CREATE TABLE `oc_product_to_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_to_category`
--

INSERT INTO `oc_product_to_category` (`product_id`, `category_id`) VALUES
(28, 20),
(28, 24),
(29, 20),
(29, 24),
(30, 20),
(30, 33),
(31, 33),
(32, 34),
(33, 20),
(34, 34),
(35, 20),
(36, 34),
(40, 20),
(40, 24),
(42, 20),
(43, 20),
(44, 20),
(46, 20),
(47, 20),
(48, 20),
(48, 34),
(49, 57);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_download`
--

CREATE TABLE `oc_product_to_download` (
  `product_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_layout`
--

CREATE TABLE `oc_product_to_layout` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_to_layout`
--

INSERT INTO `oc_product_to_layout` (`product_id`, `store_id`, `layout_id`) VALUES
(42, 0, 0),
(30, 0, 0),
(47, 0, 0),
(28, 0, 0),
(41, 0, 0),
(40, 0, 0),
(48, 0, 0),
(36, 0, 0),
(46, 0, 0),
(33, 0, 0),
(49, 0, 0),
(35, 0, 0),
(29, 0, 0),
(34, 0, 0),
(32, 0, 0),
(43, 0, 0),
(44, 0, 0),
(45, 0, 0),
(31, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_store`
--

CREATE TABLE `oc_product_to_store` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_to_store`
--

INSERT INTO `oc_product_to_store` (`product_id`, `store_id`) VALUES
(28, 0),
(29, 0),
(30, 0),
(31, 0),
(32, 0),
(33, 0),
(34, 0),
(35, 0),
(36, 0),
(40, 0),
(41, 0),
(42, 0),
(43, 0),
(44, 0),
(45, 0),
(46, 0),
(47, 0),
(48, 0),
(49, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_recurring`
--

CREATE TABLE `oc_recurring` (
  `recurring_id` int(11) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  `frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `duration` int(10) UNSIGNED NOT NULL,
  `cycle` int(10) UNSIGNED NOT NULL,
  `trial_status` tinyint(4) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `trial_frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `trial_duration` int(10) UNSIGNED NOT NULL,
  `trial_cycle` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_recurring_description`
--

CREATE TABLE `oc_recurring_description` (
  `recurring_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_return`
--

CREATE TABLE `oc_return` (
  `return_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `product` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `opened` tinyint(1) NOT NULL,
  `return_reason_id` int(11) NOT NULL,
  `return_action_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `comment` text,
  `date_ordered` date NOT NULL DEFAULT '0000-00-00',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_action`
--

CREATE TABLE `oc_return_action` (
  `return_action_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_return_action`
--

INSERT INTO `oc_return_action` (`return_action_id`, `language_id`, `name`) VALUES
(1, 1, 'Refunded'),
(2, 1, 'Credit Issued'),
(3, 1, 'Replacement Sent'),
(1, 2, 'Refunded'),
(2, 2, 'Credit Issued'),
(3, 2, 'Replacement Sent');

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_history`
--

CREATE TABLE `oc_return_history` (
  `return_history_id` int(11) NOT NULL,
  `return_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_reason`
--

CREATE TABLE `oc_return_reason` (
  `return_reason_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_return_reason`
--

INSERT INTO `oc_return_reason` (`return_reason_id`, `language_id`, `name`) VALUES
(1, 1, 'Dead On Arrival'),
(2, 1, 'Received Wrong Item'),
(3, 1, 'Order Error'),
(4, 1, 'Faulty, please supply details'),
(5, 1, 'Other, please supply details'),
(1, 2, 'Dead On Arrival'),
(2, 2, 'Received Wrong Item'),
(3, 2, 'Order Error'),
(4, 2, 'Faulty, please supply details'),
(5, 2, 'Other, please supply details');

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_status`
--

CREATE TABLE `oc_return_status` (
  `return_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_return_status`
--

INSERT INTO `oc_return_status` (`return_status_id`, `language_id`, `name`) VALUES
(1, 1, 'Pending'),
(3, 1, 'Complete'),
(2, 1, 'Awaiting Products'),
(1, 2, 'Pending'),
(3, 2, 'Complete'),
(2, 2, 'Awaiting Products');

-- --------------------------------------------------------

--
-- Table structure for table `oc_review`
--

CREATE TABLE `oc_review` (
  `review_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) NOT NULL,
  `text` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_review`
--

INSERT INTO `oc_review` (`review_id`, `product_id`, `customer_id`, `author`, `text`, `rating`, `status`, `date_added`, `date_modified`) VALUES
(1, 30, 0, 'webairch', 'Sed Consectetur Nisl Quis Felis Egestas, Non Fringilla Lectus Eleifend. Ut Ac Erat In Enim Interdum Facilisis. Aliquam A Libero Sem. Sed Et Lorem Pellentesque, Consectetur Tortor Vel, Laoreet Neque. Nulla Aliquam Posuere Lectus Fringilla Semper. Integer Finibus Diam Vitae Diam Dignissim Feugiat. ', 5, 1, '2017-09-18 15:54:37', '2017-09-19 09:59:21'),
(2, 41, 0, 'gfgdggg', 'sdsddgssssssssssdsddgssssssssssdsddgsssssssss', 3, 1, '2017-09-19 10:56:39', '2017-09-19 10:56:58');

-- --------------------------------------------------------

--
-- Table structure for table `oc_seo_url`
--

CREATE TABLE `oc_seo_url` (
  `seo_url_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `query` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_seo_url`
--

INSERT INTO `oc_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`) VALUES
(914, 0, 1, 'product_id=48', 'ipod-classic'),
(940, 0, 1, 'category_id=20', 'desktops'),
(730, 0, 1, 'manufacturer_id=8', 'apple'),
(871, 0, 1, 'information_id=4', 'about_us'),
(952, 0, 1, 'product_id=42', 'test'),
(944, 0, 1, 'category_id=34', 'mp3-players'),
(939, 0, 1, 'category_id=25', 'component'),
(947, 0, 1, 'category_id=57', 'tablet'),
(946, 0, 1, 'category_id=17', 'software'),
(945, 0, 1, 'category_id=24', 'smartphone'),
(948, 0, 1, 'category_id=33', 'camera'),
(909, 0, 1, 'product_id=30', 'canon-eos-5d'),
(910, 0, 1, 'product_id=47', 'hp-lp3065'),
(911, 0, 1, 'product_id=28', 'htc-touch-hd'),
(933, 0, 1, 'product_id=43', 'macbook'),
(934, 0, 1, 'product_id=44', 'macbook-air'),
(922, 0, 1, 'product_id=45', 'macbook-pro'),
(923, 0, 1, 'product_id=31', 'nikon-d300'),
(924, 0, 1, 'product_id=29', 'palm-treo-pro'),
(925, 0, 1, 'product_id=35', 'product-8'),
(926, 0, 1, 'product_id=49', 'samsung-galaxy-tab-10-1'),
(919, 0, 1, 'product_id=33', 'samsung-syncmaster-941bw'),
(937, 0, 1, 'product_id=46', 'sony-vaio'),
(912, 0, 1, 'product_id=41', 'imac'),
(913, 0, 1, 'product_id=40', 'iphone'),
(915, 0, 1, 'product_id=36', 'ipod-nano'),
(916, 0, 1, 'product_id=34', 'ipod-shuffle'),
(917, 0, 1, 'product_id=32', 'ipod-touch'),
(828, 0, 1, 'manufacturer_id=9', 'canon'),
(829, 0, 1, 'manufacturer_id=5', 'htc'),
(830, 0, 1, 'manufacturer_id=7', 'hewlett-packard'),
(831, 0, 1, 'manufacturer_id=6', 'palm'),
(832, 0, 1, 'manufacturer_id=10', 'sony'),
(872, 0, 1, 'information_id=6', 'delivery'),
(873, 0, 1, 'information_id=3', 'privacy'),
(874, 0, 1, 'information_id=5', 'terms'),
(942, 0, 2, 'category_id=61', 'كاتيجوريمان'),
(941, 0, 1, 'category_id=61', 'category-man');

-- --------------------------------------------------------

--
-- Table structure for table `oc_session`
--

CREATE TABLE `oc_session` (
  `session_id` varchar(32) NOT NULL,
  `data` text NOT NULL,
  `expire` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_session`
--

INSERT INTO `oc_session` (`session_id`, `data`, `expire`) VALUES
('0040949696021c749af6b1a7e6', '{\"api_id\":\"5\"}', '2020-11-29 17:03:35'),
('009777984594ec700b1a883d6d', '{\"api_id\":\"5\",\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-06 00:55:28'),
('00d49bef29ee47556a03bf5e3f', '{\"api_id\":\"5\"}', '2020-11-29 17:18:19'),
('01735d01f203ab2c1769fcab38', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 05:08:07'),
('01debdd1d1eaf83a05f3469492', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 05:08:05'),
('023b4bc05405201eea7ec351e9', '{\"language\":\"ar\",\"currency\":\"SAR\",\"redirect\":\"https:\\/\\/lanaapp.space\\/isa_mohab\\/index.php?route=account\\/wishlist\"}', '2020-11-25 03:57:29'),
('028b201323d0776970133e836f', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:58:53'),
('039c1c182aab42f79e95b143bf', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 03:46:52'),
('047879e1bc3b0a39381ce232eb', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-11 21:00:52'),
('052fdfab6ff0bede2ec2916762', '{\"api_id\":\"5\"}', '2020-11-29 18:36:16'),
('053fca1fd01f3a854acfd8d2bd', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"customer_id\":\"4\"}', '2017-09-18 11:16:28'),
('0570d2af6723abfec7a5baca72', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 18:31:52'),
('05813677c9cc843d1db27dceb5', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 03:58:03'),
('05a807e04f5c64c0a777c749e8', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 14:25:50'),
('05b4cb4ebddb193454f8ee524c', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-25 13:06:19'),
('0661f04fad2393daaa02776e0d', '{\"language\":\"ar\",\"currency\":\"SAR\",\"redirect\":\"https:\\/\\/lanaapp.space\\/isa_mohab\\/index.php?route=account\\/wishlist\"}', '2020-11-24 03:56:47'),
('0687b3fb8a16ea5b5f263705b1', '{\"api_id\":\"5\"}', '2020-11-29 16:36:15'),
('0733a16402e022d2396c6c0e12', '{\"language\":\"ar\",\"currency\":\"SAR\",\"compare\":[]}', '2020-12-09 03:58:51'),
('07e5a5ebc8d28c3971addb39ee', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 05:50:22'),
('0839772a0308f3effeb97a4ea7', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 04:02:16'),
('088cb5291312aad5c1c80b6720', '{\"api_id\":\"5\"}', '2020-11-29 16:36:11'),
('0893b01bc661618d944e19da21', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:58:18'),
('0992caa91c4aeb719bb4a2e1f0', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 03:59:12'),
('099dad489841196e9805880d3b', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:36:08'),
('09cbdeaaf27d920fa8930e2175', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:43:00'),
('09f347f5714c047d17cec0151a', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-12 22:47:35'),
('0ad3a01a8ed2a584bfa7ae04ad', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 21:17:37'),
('0b2605a67508d1a0e83591e250', '{\"language\":\"ar\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"pSSqellhXODyZvLvzhGvV4lFk9hOXSoh\"}', '2020-11-21 01:58:04'),
('0b305f32ad86119e50dd7b6c2c', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 03:56:51'),
('0c7581f5ad4ffd5f11dce0c2e1', '{\"api_id\":\"5\"}', '2020-11-29 17:32:20'),
('0ceec69c66f751feab87e22861', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:41:51'),
('0cfc0dc2b084f578a1da8675fc', '{\"api_id\":\"5\",\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-12 22:47:52'),
('0d6dae8a593f9f7efd9d166534', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 15:07:44'),
('0d937e1725cb9cc4b85e2d19c5', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 02:06:48'),
('0dfd25d1ecff835451d32fc132', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-23 19:17:20'),
('0e1824cd3d9b1f02ee80ef9ea8', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 19:55:11'),
('0e8ab9d48eadf08e9d921fd64d', 'false', '2020-11-29 17:26:26'),
('0e955125efb2e2726fce37a2f3', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:59:05'),
('0e969432acf832f538c25eeaa7', '{\"user_id\":\"1\",\"user_token\":\"lFJzUfMZ5dwQThKKTHUQ7xG6p9jtBLhk\"}', '2020-12-05 12:51:38'),
('0ebc66ebe30c28cc9411d339c8', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-13 18:34:51'),
('0ee46ae5abbc1ab2ef27679be3', '{\"api_id\":\"4\"}', '2020-11-21 23:21:01'),
('0ef9a16bf051163c14c04f7061', '{\"api_id\":\"5\"}', '2020-12-11 21:11:40'),
('0f03c1483f04f2414b1fcc4298', '{\"api_id\":\"5\"}', '2020-12-12 22:47:34'),
('0f2e1b8d75eb53caf6383c1d52', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 06:40:49'),
('0fb6d72303ed8c5345147af9ba', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 07:28:25'),
('0fba55f290ae6e8ac2380a34b9', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 03:58:12'),
('103a990d8d71a97629d45f34d8', '{\"user_id\":\"1\",\"user_token\":\"fp8OjB8vIZ2Zwd8TVAFquOuHiAeNxEVM\",\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-30 02:01:01'),
('107526a4b307a20d13833f2812', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2020-11-20 19:28:34'),
('108d0f99c047c3306cec8ae341', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-07 03:55:47'),
('109237aa5eddcd1258010e2ef4', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 11:00:00'),
('11538b10e4473cde97797abe72', '{\"api_id\":\"5\"}', '2020-11-29 21:51:37'),
('11bd25c60e417be5cb880a5620', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-06 04:02:13'),
('11e3bce551ef36c543bb8d84da', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-05 14:47:52'),
('12251754e4cf728fb405a3a42c', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 03:50:13'),
('1265fc32bc0f8ea88106d8f0fb', '{\"api_id\":\"5\"}', '2020-11-29 17:32:26'),
('12836f01839a53ad5ff4f8d265', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:58:59'),
('12b6ee26add1ead5fc355a769a', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 07:28:28'),
('132734ef69674f229813230ace', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 11:28:38'),
('13b93159f572ff867114a5d519', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 15:11:19'),
('14e994885aaa5d0e4d210be4ff', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 05:51:09'),
('150b5cf748456846e07bad3cd0', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 16:07:04'),
('151bf2d4f7d4fd6267ad77ef2c', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:59:07'),
('151ca6766c0d1e638da2526fa9', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 00:41:58'),
('1526bfbd4ce32a9577de6c197f', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:42:03'),
('1527273324c56996f91d1664c9', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 08:42:31'),
('15396d9105934f16cf424ed483', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 10:54:45'),
('15c3cafad8555e70e21dbdbace', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 06:40:42'),
('15ceee88e43e4e29b1e41f0b45', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-26 04:05:17'),
('1704c0e2f2627b5c7cdbd1de7e', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 03:46:49'),
('17233daec5ce51bf0496640b72', '{\"api_id\":\"5\"}', '2020-11-29 19:28:24'),
('17335f8788fb6aeca93b86830a', '{\"user_id\":\"1\",\"user_token\":\"ISDyBPpGUYbJY2MBJOWPTuY4ffaQX6BK\",\"language\":\"ar\",\"currency\":\"SAR\",\"vouchers\":[]}', '2020-11-28 21:00:34'),
('17b05f71930bbbcdcac2bda286', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:41:27'),
('17ff39f463ea02dd9a3c70e42e', '{\"language\":\"ar\",\"currency\":\"SAR\",\"user_id\":\"1\",\"user_token\":\"nKjHdgm7ezIKN7fqk81dZ1a0BePyX0Uk\"}', '2020-11-29 19:45:52'),
('181ed6ae9107805b8b29347924', '{\"api_id\":\"5\"}', '2020-11-29 19:28:08'),
('18288b3bd7c9c2e233dd3ea53c', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 05:51:05'),
('189687c9a82967dec0160ac504', '{\"user_id\":\"1\",\"user_token\":\"EQXKXMFDIzwa6WJOj0s39eBV9KUbqSHL\",\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-02 12:37:19'),
('18bd935a0c4bd93604ec6b1e3a', '{\"api_id\":\"5\"}', '2020-12-05 00:43:10'),
('1958fc62035c54726233cf2a8b', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 04:02:18'),
('19775092705d13c2ba6dc76d00', '{\"api_id\":\"6\"}', '2020-11-29 18:02:39'),
('1998c0579d08f3e365fb75e0ba', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2020-11-20 19:28:21'),
('19b47b4d9a6c0d120a9e6d2be1', '{\"api_id\":\"5\"}', '2020-12-10 19:21:19'),
('19be505ad736eb15aca5d2e8a1', '{\"api_id\":\"5\"}', '2020-12-01 02:32:00'),
('19fe0d48a7669ffae3a04043dc', 'false', '2020-11-21 03:54:35'),
('1a37011ebe6fb903c32bb7efa2', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-06 04:02:06'),
('1a44a9aa611ffff4c9d799eb91', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 04:03:07'),
('1a4fae67799387fadb2e504c93', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 04:01:10'),
('1a73301f14dc48b55a2945d3da', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 03:57:53'),
('1aafd906ebdb765aa72af452a9', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-05 01:37:02'),
('1b095e97187d8dea4777d87478', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 10:09:05'),
('1c3d936bbb7f9d79ec0a63cdea', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 06:16:15'),
('1c43ca3ed3db1a18d342113157', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 03:57:32'),
('1c699d3ea1b3d4f6d1192809c5', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-26 04:04:52'),
('1cf1630e34f634242f0b6cfc76', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 04:03:47'),
('1d3d5cdd84a71b63a7f2457fda', 'false', '2020-11-21 03:56:29'),
('1e07d0a954dc05134ed089889b', '{\"user_id\":\"1\",\"user_token\":\"KVbdocpjjSDVuYi4nENCZlGoeF0F0gzp\"}', '2020-12-13 19:50:28'),
('1e2cf9a47dd412f5490a3c10b6', '{\"api_id\":\"5\"}', '2020-12-05 00:36:13'),
('1e31cea07cc526a786519298d8', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 03:59:06'),
('1e5cd66c366fe38d284f1f59bc', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-27 03:00:01'),
('1f05c63e589eb2f9b4aae349af', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 16:07:21'),
('1f11d43a9501f1cb2cdb826629', '{\"api_id\":\"5\"}', '2020-11-29 19:37:31'),
('1f7875366624dc4aeaea472f01', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 16:07:21'),
('1fb12a8000c597193677e0c1cb', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 21:50:53'),
('1ff96c558df7c42ac67ebb2ef9', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-06 01:09:20'),
('20bb339b6c017818556a49ebf3', '{\"api_id\":\"5\"}', '2020-12-05 00:42:44'),
('20f56205a87d5d9c5d9e5f251b', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 07:28:27'),
('21721c955664fdcf16216237d6', 'false', '2020-11-29 17:26:25'),
('219df2c42a9c1be03e082ea9c7', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 05:51:08'),
('21a24f312d1c9f34f9f829ad61', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 02:45:42'),
('21d975b2a8c3b89be58cbd3afb', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 14:52:32'),
('220d1bf34c4bb71b520d26dfff', '{\"user_id\":\"1\",\"user_token\":\"KY8NqKlrTrHR0lutR6oruh4xRtsmmklE\"}', '2020-12-12 22:48:19'),
('22634c8bf29ce5702347b9e0fd', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 19:35:59'),
('229286a815009de504722348a1', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 15:23:58'),
('22ec01b40ae470b71c6e240c5a', '{\"language\":\"en-gb\",\"currency\":\"SAR\",\"account\":\"guest\",\"guest\":{\"customer_group_id\":\"1\",\"firstname\":\"asdasd\",\"lastname\":\"dasd\",\"email\":\"sadas@dasdasd.com\",\"telephone\":\"4234234\",\"custom_field\":[],\"shipping_address\":\"1\"},\"payment_address\":{\"firstname\":\"asdasd\",\"lastname\":\"dasd\",\"company\":\"\",\"address_1\":\"fsdfsdfs\",\"address_2\":\"\",\"postcode\":\"434234\",\"city\":\"sdfsdf\",\"country_id\":\"184\",\"zone_id\":\"2876\",\"country\":\"Saudi Arabia\",\"iso_code_2\":\"SA\",\"iso_code_3\":\"SAU\",\"address_format\":\"\",\"custom_field\":[],\"zone\":\"Al Jawf\",\"zone_code\":\"JF\"},\"shipping_address\":{\"firstname\":\"asdasd\",\"lastname\":\"dasd\",\"company\":\"\",\"address_1\":\"fsdfsdfs\",\"address_2\":\"\",\"postcode\":\"434234\",\"city\":\"sdfsdf\",\"country_id\":\"184\",\"zone_id\":\"2876\",\"country\":\"Saudi Arabia\",\"iso_code_2\":\"SA\",\"iso_code_3\":\"SAU\",\"address_format\":\"\",\"zone\":\"Al Jawf\",\"zone_code\":\"JF\",\"custom_field\":[]},\"shipping_methods\":{\"flat\":{\"title\":\"Flat Rate\",\"quote\":{\"flat\":{\"code\":\"flat.flat\",\"title\":\"Flat Shipping Rate\",\"cost\":\"5.00\",\"tax_class_id\":\"9\",\"text\":\"5.00\\u0631\\u064a\\u0627\\u0644\"}},\"sort_order\":\"1\",\"error\":false},\"free\":{\"title\":\"Free Shipping\",\"quote\":{\"free\":{\"code\":\"free.free\",\"title\":\"Free Shipping\",\"cost\":0,\"tax_class_id\":0,\"text\":\"0.00\\u0631\\u064a\\u0627\\u0644\"}},\"sort_order\":\"1\",\"error\":false}},\"shipping_method\":{\"code\":\"flat.flat\",\"title\":\"Flat Shipping Rate\",\"cost\":\"5.00\",\"tax_class_id\":\"9\",\"text\":\"5.00\\u0631\\u064a\\u0627\\u0644\"},\"comment\":\"\",\"payment_methods\":{\"cod\":{\"code\":\"cod\",\"title\":\"Cash On Delivery\",\"terms\":\"\",\"sort_order\":\"1\"},\"paylink\":{\"code\":\"paylink\",\"title\":\"<img src=\\\"catalog\\/view\\/theme\\/default\\/image\\/paylink.png\\\" alt=\\\"onlinepay\\\" title=\\\"onlinepay\\\" width=\\\"35%\\\"\\/>\",\"terms\":\"\",\"sort_order\":\"1\"}}}', '2020-12-05 03:45:43'),
('23782267c7c04552c454543ac0', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 04:06:05'),
('23ff93d27c58bdfbaaa1d10643', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 03:58:39'),
('2414c6ff28a826ce4bdb786992', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 03:59:09'),
('249cebed99ac90b6890c1af8cc', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 13:21:31'),
('249dd843dcf0b589e0afda6b5a', '{\"language\":\"ar\",\"currency\":\"SAR\",\"redirect\":\"https:\\/\\/lanaapp.space\\/isa_mohab\\/index.php?route=account\\/transaction\"}', '2020-12-04 03:58:25'),
('252111c91102b7fdf61bb124c5', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 15:23:57'),
('2522a570f849a286a609a8087b', '{\"api_id\":\"5\"}', '2020-11-29 16:54:46'),
('260adbb7998cf71bcc9238ed03', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 03:59:25'),
('266df70783c5e9c2d0c9ec12c8', 'false', '2020-11-29 17:26:25'),
('2713eabb1579a31d478f68c9f6', '{\"api_id\":\"5\"}', '2020-12-12 22:48:02'),
('274122f850dead9ce33198e5fc', '{\"api_id\":\"5\"}', '2020-11-29 18:36:34'),
('28750014fda51a2bbda6efdae3', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 16:55:39'),
('28938db574c2414c53e449f5da', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 05:08:01'),
('290e35a417e39a7c2da672f979', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-26 04:05:43'),
('293f278ea353fdb3f660d2e0d5', 'false', '2020-11-29 17:26:23'),
('294c5a023598c764433d210139', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 14:19:18'),
('29916ada09c2ad770374decd27', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:59:10'),
('2994ca4ce8d7ef1df158211a04', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 06:40:52'),
('29e7cb07519a1093d8bbaf558c', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-12 15:41:11'),
('29f5ccd3e4691b2b5c9cc4a0dc', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 11:19:55'),
('2a77ca170ca51617f63cfc5ca0', '{\"api_id\":\"5\"}', '2020-11-30 03:02:14'),
('2ace330c7f2e45c008bbd5a323', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 04:01:09'),
('2b0c2ebe41f79030e0f851e16a', '{\"api_id\":\"5\"}', '2020-11-29 17:00:25'),
('2ba73a7c2594c4a072e6643229', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 03:55:49'),
('2c1754666ac52fa25bb1049455', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 13:21:39'),
('2c1896e167c79eccca058af522', '{\"language\":\"ar\",\"currency\":\"SAR\",\"redirect\":\"https:\\/\\/lanaapp.space\\/isa_mohab\\/index.php?route=account\\/wishlist\"}', '2020-11-30 03:57:36'),
('2c6496bf92de089058cddf3ac0', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 17:25:02'),
('2cc5f99226840852f46e0eeafc', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:59:19'),
('2cdb0af73d1eef7a94ad36542b', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 22:05:28'),
('2cef24b6cfb2db00fafd076a0e', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 23:08:32'),
('2d161af178dab99f2cf5f571fe', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 04:16:37'),
('2d1df7ca65e8f70b3e3537d06f', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:58:50'),
('2d52b46984a3e55513808560d6', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 04:04:47'),
('2dd5aeb30995232eb3a2c4ba96', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 15:07:45'),
('2ded2a6d82f9727c12b0bea505', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 09:46:03'),
('2ec8a309523eab27c06572c61f', '{\"language\":\"en-gb\",\"currency\":\"SAR\",\"compare\":[\"41\"]}', '2020-12-12 15:31:40'),
('2eecae594df1b73adeabf19800', '{\"api_id\":\"5\"}', '2020-11-29 17:00:10'),
('2f04a454f07de00ade063f2a5e', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 03:59:05'),
('2f5c1b3ab32b8cab726c8b5d0f', '{\"user_id\":\"1\",\"user_token\":\"CsXo7hyBd0nE6RvkXQiCzeMaEGNL49jN\"}', '2020-12-12 14:28:37'),
('2f9e3e9a9ca5298f8edd9dd83c', '{\"language\":\"en-gb\",\"currency\":\"SAR\",\"account\":\"guest\",\"guest\":{\"customer_group_id\":\"1\",\"firstname\":\"gfdg\",\"lastname\":\"fgdfg\",\"email\":\"fsdfsd@fsd.com\",\"telephone\":\"345345\",\"custom_field\":[],\"shipping_address\":\"1\"},\"payment_address\":{\"firstname\":\"gfdg\",\"lastname\":\"fgdfg\",\"company\":\"\",\"address_1\":\"4534534\",\"address_2\":\"\",\"postcode\":\"435345\",\"city\":\"45345\",\"country_id\":\"179\",\"zone_id\":\"2838\",\"country\":\"Saint Lucia\",\"iso_code_2\":\"LC\",\"iso_code_3\":\"LCA\",\"address_format\":\"\",\"custom_field\":[],\"zone\":\"Dauphin\",\"zone_code\":\"DA\"},\"shipping_address\":{\"firstname\":\"gfdg\",\"lastname\":\"fgdfg\",\"company\":\"\",\"address_1\":\"4534534\",\"address_2\":\"\",\"postcode\":\"435345\",\"city\":\"45345\",\"country_id\":\"179\",\"zone_id\":\"2838\",\"country\":\"Saint Lucia\",\"iso_code_2\":\"LC\",\"iso_code_3\":\"LCA\",\"address_format\":\"\",\"zone\":\"Dauphin\",\"zone_code\":\"DA\",\"custom_field\":[]},\"shipping_methods\":{\"flat\":{\"title\":\"Flat Rate\",\"quote\":{\"flat\":{\"code\":\"flat.flat\",\"title\":\"Flat Shipping Rate\",\"cost\":\"5.00\",\"tax_class_id\":\"9\",\"text\":\"5.00\\u0631\\u064a\\u0627\\u0644\"}},\"sort_order\":\"1\",\"error\":false},\"free\":{\"title\":\"Free Shipping\",\"quote\":{\"free\":{\"code\":\"free.free\",\"title\":\"Free Shipping\",\"cost\":0,\"tax_class_id\":0,\"text\":\"0.00\\u0631\\u064a\\u0627\\u0644\"}},\"sort_order\":\"1\",\"error\":false}},\"shipping_method\":{\"code\":\"flat.flat\",\"title\":\"Flat Shipping Rate\",\"cost\":\"5.00\",\"tax_class_id\":\"9\",\"text\":\"5.00\\u0631\\u064a\\u0627\\u0644\"},\"comment\":\"\",\"payment_methods\":{\"cod\":{\"code\":\"cod\",\"title\":\"Cash On Delivery\",\"terms\":\"\",\"sort_order\":\"1\"},\"paylink\":{\"code\":\"paylink\",\"title\":\"<img src=\\\"catalog\\/view\\/theme\\/default\\/image\\/paylink.png\\\" alt=\\\"onlinepay\\\" title=\\\"onlinepay\\\" width=\\\"35%\\\"\\/>\",\"terms\":\"\",\"sort_order\":\"1\"}},\"payment_method\":{\"code\":\"paylink\",\"title\":\"<img src=\\\"catalog\\/view\\/theme\\/default\\/image\\/paylink.png\\\" alt=\\\"onlinepay\\\" title=\\\"onlinepay\\\" width=\\\"35%\\\"\\/>\",\"terms\":\"\",\"sort_order\":\"1\"},\"order_id\":55,\"PayLink_transactionNo\":\"1607800743630\"}', '2020-12-12 22:43:03'),
('2fea40087e31d5b40ce0dfe64d', '{\"language\":\"ar\",\"currency\":\"USD\"}', '2020-11-26 16:53:48'),
('301282ddb98f7f9ddf3c11465c', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-27 03:00:01'),
('303324f472d0ff069c0fe5c30a', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 11:28:34'),
('30c44f4712f610693ac42c390b', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:42:07'),
('30c771e8505b5eef6f6927f3dd', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 19:53:59'),
('30cf9421e98b5406442b47520a', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:41:39'),
('31b71ef0921164544359c1fe36', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 03:55:47'),
('31de4cf92ff6ca40ea091ea877', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 10:32:37'),
('325ca3b0a068f22a2e9472fd2e', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 15:23:59'),
('32d4564b79c05c9ab2569b63d8', '{\"language\":\"ar\",\"currency\":\"SAR\",\"user_id\":\"1\",\"user_token\":\"4kcGyMUvPWEQLiIby0XGSo2YRLyYq1nz\"}', '2020-11-30 04:12:14'),
('34374046325f2d388a69d0f00b', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-25 15:52:23'),
('347116bd22525e1e4b131b027b', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 03:46:50'),
('34ce26482761f5531fe1aa8870', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 07:36:39'),
('34d4fe273bfbdc5b2e6241beb1', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 08:31:48'),
('3592978451c501fdb0693c7019', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 12:16:24'),
('35c44fe6f0063f0120c57b6497', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:59:09'),
('35eec8a4e2ccfbb62b6e15c9c5', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 13:42:46'),
('36108d69fbe808831982d821aa', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 04:02:15'),
('367ba6257ad66ea4af173a925d', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:58:08'),
('36d1f3d9fb4b690ed838361ab2', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"customer_id\":\"1\",\"shipping_address\":{\"address_id\":\"1\",\"firstname\":\"demo\",\"lastname\":\"demo\",\"company\":\"\",\"address_1\":\"surat\",\"address_2\":\"\",\"postcode\":\"\",\"city\":\"surat\",\"zone_id\":\"1485\",\"zone\":\"Gujarat\",\"zone_code\":\"GU\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":null}}', '2017-09-17 06:05:43'),
('36dc8f387f1b18d1021bcd77be', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-23 19:22:52'),
('36e609b83abe41bb74bf5d7db5', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 03:58:37'),
('3715df868f5560d94f8175ddcd', '{\"api_id\":\"5\"}', '2020-12-06 00:42:49'),
('376144983739f7259d3d9dd692', '{\"user_id\":\"1\",\"user_token\":\"I5cxr5ImBcgbrOcW9hOHuD0KxJ7zq9uC\",\"language\":\"ar\",\"currency\":\"SAR\",\"customer_id\":\"3\",\"shipping_address\":{\"address_id\":\"3\",\"firstname\":\"fggfg\",\"lastname\":\"fdf\",\"company\":\"\",\"address_1\":\"surat\",\"address_2\":\"\",\"postcode\":\"356006\",\"city\":\"surat\",\"zone_id\":\"1485\",\"zone\":\"Gujarat\",\"zone_code\":\"GU\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":null}}', '2020-12-05 19:26:39'),
('38fe74ec0f714fd431246e2d0b', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 08:31:39'),
('39d375e64998d6133c4b18012d', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:43:05'),
('3a309d35733c872b07d5759eef', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 04:02:56'),
('3aaa27d5ce52c43acede08a0c2', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 22:07:21'),
('3af9abb517487306431004be69', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 23:12:26'),
('3b78a4dce535ded1bb986d3a6c', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-04 03:58:18'),
('3be9e10222516f0500ef1c2f34', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:35:50'),
('3c3af55f074637d169c2d78bb9', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 18:31:51'),
('3c421d99e329191de4935e30ff', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 07:28:25'),
('3cecba5482074e6d6b19a56433', 'false', '2020-11-21 03:54:36'),
('3cf2f2cbf79e13bcc03ba84ab9', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 19:36:00'),
('3d438f44d54fd1f991000da19e', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"XDIsxh9ryfH1V69497GNH6QOPReeyD83\"}', '2017-10-07 13:52:29'),
('3da2ec9fde82cfe7d2c533d6bc', '{\"api_id\":\"5\"}', '2020-11-29 17:02:11'),
('3dfa05b76bfda40a05f3438fbf', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 03:58:28'),
('3ea995612ce25e470f7b689157', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-23 20:29:03'),
('3ec4e0ce2eeb87ff519d57d6cc', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 03:55:50'),
('3ecca23b41aa0923a9687e4459', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2020-11-20 18:25:34'),
('3eedc13d7aebddddca7e03c97a', '{\"api_id\":\"5\"}', '2020-12-05 00:36:09'),
('3f18ae59bafce2dd26d6bf90a7', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 13:21:42'),
('3f48373fe78edad4618199734a', '{\"language\":\"en-gb\",\"currency\":\"SAR\",\"user_id\":\"1\",\"user_token\":\"HfN6e362JnpHpcgXYcTBS9L5xhEsgV5q\"}', '2020-11-30 04:23:21'),
('3fcc91a60223d127caee2ad917', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 09:07:37'),
('401ab5872bd6a94f5d8669df6c', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 02:31:49'),
('402dd1eafc4028c89b2c9006a2', '{\"api_id\":\"5\"}', '2020-12-05 01:47:18'),
('40623dcefde9616eafa2430c02', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:59:23'),
('40647fd22743f81d5712f94263', '{\"api_id\":\"4\"}', '2020-11-21 23:23:37'),
('40b1cfa81be67c76f63f8a047a', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 02:44:34'),
('413c608c53f7f02c7947dc42ae', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 15:23:57'),
('4150ee13439822501a42d401b4', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-09-16 07:58:24'),
('41b63720ef800ea6a57f9bd71e', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 16:06:55'),
('41faefaeee03e02aedb2fb0f1e', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 22:52:28'),
('41fb5ecb3dab85bc27d32e3aca', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-26 04:04:16'),
('429de7fc372d581287b88dd593', '{\"api_id\":\"5\"}', '2020-12-12 17:18:59'),
('42a2f96e6c10e88d1d7bf1c8d4', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 13:21:41'),
('42a6114aae7675fe343e674182', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-24 00:07:03'),
('42db9572031ca74cb2ab29c9b5', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 14:43:19'),
('42ff3f14918e898f94c7d42bfc', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 03:59:15'),
('43552616c6ef83361944b09d26', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-10 03:55:07'),
('4399adacda0646366bdbd59c41', 'false', '2020-11-21 03:55:08'),
('44ce181d0e4a25fc27045246c5', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 13:38:56'),
('44cff63ef490837b46802cc788', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-24 18:15:29'),
('44dc380f66f14b76d73eb385fe', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 15:24:01'),
('456fb001fd2052cb22a9d2f9e0', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 18:31:50'),
('4575429057c66552663f23465a', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 11:06:54'),
('4675b89e342a4803a89312dd40', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 09:32:54'),
('46b87dafebbb3bd63626ee19f7', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:58:37'),
('46c1972998664e18e7c5f993b0', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 23:08:27'),
('4728ba2973cbd8f3808bee233e', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 17:59:56'),
('473d88f1d5e19b81bb560efd81', '{\"api_id\":\"6\"}', '2020-11-29 18:11:17'),
('47ba768fb3413d416cadb0e249', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 18:31:50'),
('4809945e6dcff0860cffbaaa0f', '{\"language\":\"ar\",\"currency\":\"SAR\",\"redirect\":\"https:\\/\\/lanaapp.space\\/isa_mohab\\/index.php?route=account\\/wishlist\"}', '2020-11-28 04:03:51'),
('4847ba4ab001ea41d0163739d0', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-07 03:55:55'),
('48c5bdcd90b85b0f8168b41f14', '{\"api_id\":\"4\"}', '2020-11-21 23:25:09'),
('4908acb63f4d6d34a8f367434d', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-08 04:02:28'),
('4911c1e3d4f0f0e8b7ff2180b9', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:42:54'),
('49872ee57d95faff301d6a6e03', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 03:46:51'),
('4999332c62c1fc0ea3c1135418', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 04:02:18'),
('499a6241134c9461309ae47905', '{\"language\":\"en-gb\",\"currency\":\"SAR\",\"account\":\"guest\",\"payment_address\":{\"firstname\":\"mohab\",\"lastname\":\"mamdouh\",\"company\":\"\",\"address_1\":\"mohabma\",\"address_2\":\"\",\"postcode\":\"23232\",\"city\":\"cairo\",\"country_id\":\"63\",\"zone_id\":\"1012\",\"country\":\"Egypt\",\"iso_code_2\":\"EG\",\"iso_code_3\":\"EGY\",\"address_format\":\"\",\"custom_field\":[],\"zone\":\"Al Qalyubiyah\",\"zone_code\":\"QLY\"},\"shipping_address\":{\"firstname\":\"mohab\",\"lastname\":\"mamdouh\",\"company\":\"\",\"address_1\":\"mohabma\",\"address_2\":\"\",\"postcode\":\"23232\",\"city\":\"cairo\",\"country_id\":\"63\",\"zone_id\":\"1012\",\"country\":\"Egypt\",\"iso_code_2\":\"EG\",\"iso_code_3\":\"EGY\",\"address_format\":\"\",\"zone\":\"Al Qalyubiyah\",\"zone_code\":\"QLY\",\"custom_field\":[]},\"compare\":[\"41\"],\"PayLink_transactionNo\":\"1607116936866\",\"notes_cart_data\":[{\"product_id\":\"41\",\"price\":100,\"product_cost\":\"50.0000\",\"quantity\":\"2\",\"balance\":100,\"dropshipping\":\"1\"},{\"product_id\":\"46\",\"price\":1000,\"product_cost\":\"900.0000\",\"quantity\":\"2\",\"balance\":1800,\"dropshipping\":\"1\"}],\"in_cart_dropshipping\":\"1\",\"guest\":{\"customer_group_id\":\"1\",\"firstname\":\"mohab\",\"lastname\":\"mamdouh\",\"email\":\"gtgalaxyfit@gmail.com\",\"telephone\":\"2055\",\"custom_field\":[],\"shipping_address\":\"1\"},\"shipping_methods\":{\"flat\":{\"title\":\"Flat Rate\",\"quote\":{\"flat\":{\"code\":\"flat.flat\",\"title\":\"Flat Shipping Rate\",\"cost\":\"5.00\",\"tax_class_id\":\"9\",\"text\":\"5.00\\u0631\\u064a\\u0627\\u0644\"}},\"sort_order\":\"1\",\"error\":false},\"free\":{\"title\":\"Free Shipping\",\"quote\":{\"free\":{\"code\":\"free.free\",\"title\":\"Free Shipping\",\"cost\":0,\"tax_class_id\":0,\"text\":\"0.00\\u0631\\u064a\\u0627\\u0644\"}},\"sort_order\":\"1\",\"error\":false}},\"shipping_method\":{\"code\":\"flat.flat\",\"title\":\"Flat Shipping Rate\",\"cost\":\"5.00\",\"tax_class_id\":\"9\",\"text\":\"5.00\\u0631\\u064a\\u0627\\u0644\"},\"comment\":\"\",\"payment_methods\":{\"cod\":{\"code\":\"cod\",\"title\":\"Cash On Delivery\",\"terms\":\"\",\"sort_order\":\"1\"},\"paylink\":{\"code\":\"paylink\",\"title\":\"<img src=\\\"catalog\\/view\\/theme\\/default\\/image\\/paylink.png\\\" alt=\\\"onlinepay\\\" title=\\\"onlinepay\\\" width=\\\"35%\\\"\\/>\",\"terms\":\"\",\"sort_order\":\"1\"}}}', '2020-12-05 15:39:33'),
('49a806ac2790512434a3ee8432', '{\"api_id\":\"5\"}', '2020-11-29 19:44:40'),
('49c45d6c347466e140151d3067', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 11:28:40'),
('4a7de354e92c44bfdb8680c008', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 03:57:05'),
('4aab623e382ad1e4275d76d471', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"TnV5rLvzEyzwYJuaapeaudYM8K26xRdu\",\"account\":\"register\",\"compare\":[\"42\",\"48\",\"44\",\"30\"],\"wishlist\":[],\"customer_id\":\"1\",\"shipping_address\":{\"address_id\":\"1\",\"firstname\":\"demo\",\"lastname\":\"demo\",\"company\":\"\",\"address_1\":\"surat\",\"address_2\":\"\",\"postcode\":\"\",\"city\":\"surat\",\"zone_id\":\"1485\",\"zone\":\"Gujarat\",\"zone_code\":\"GU\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":null},\"payment_address\":{\"address_id\":\"1\",\"firstname\":\"demo\",\"lastname\":\"demo\",\"company\":\"\",\"address_1\":\"surat\",\"address_2\":\"\",\"postcode\":\"\",\"city\":\"surat\",\"zone_id\":\"1485\",\"zone\":\"Gujarat\",\"zone_code\":\"GU\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":null}}', '2017-09-19 08:08:48'),
('4aec519f4bc12acb583013a24f', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 01:46:06'),
('4b42a9cac61cd1c6aec3265112', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-21 07:23:53'),
('4b948bdef5593cf0ff26ac86c2', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 05:41:20'),
('4c88499b718e760f6ff25a2f66', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-06 04:02:03'),
('4ccf6961c9669beb46ed7da30e', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 03:56:23'),
('4ce9c36e7d7706974b04251d99', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-26 23:22:08'),
('4d2982502a24414e3e9e701534', '{\"api_id\":\"5\"}', '2020-12-01 03:46:21'),
('4d73ff90be5d66cf56ed0d2736', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 11:47:49'),
('4e6e242e9ad758ec29fe37a4a4', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2020-11-20 18:27:19'),
('4eb9e57c8db74f602e6cd6a79a', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 18:52:34'),
('4f184dbf4abbbb9db0e897493d', 'false', '2020-11-21 03:56:29'),
('4f2c1939ab1157f8a30f25a44b', '{\"api_id\":\"6\"}', '2020-11-29 18:25:23'),
('4fe8acf9b09916de3399e04389', '{\"language\":\"en-gb\",\"currency\":\"SAR\",\"user_id\":\"1\",\"user_token\":\"2WGfiEtOyaIallTEElts9Fxj8bKItP3j\"}', '2020-12-01 17:07:00'),
('4fed5934e0f8776ed0f91439af', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-10 03:55:17'),
('4ff13fe65168a71bda82f9b39d', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 03:56:42'),
('508b225d1b7c42bf743da63afb', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 03:55:53'),
('51a5a6ffaf0f35f753e1848c6f', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 21:51:21'),
('52b91b7c4608e2e5ee7135db95', '{\"api_id\":\"5\"}', '2020-11-30 15:11:58'),
('532ea2c33a554b56c5493a6846', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 18:31:51'),
('536da363d716f36f4c82449860', '{\"language\":\"ar\",\"currency\":\"USD\"}', '2020-11-20 20:51:33'),
('538aba4244fbc4fb8920180bc5', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:59:22'),
('5440150e8c5d3b149b8e60a9d8', '{\"api_id\":\"6\"}', '2020-11-29 18:25:14'),
('544ad5089a5055d65eac46b0af', '{\"language\":\"ar\",\"currency\":\"USD\"}', '2020-11-28 00:26:21'),
('54750a61a49f3129b6ce941e17', '{\"api_id\":\"5\"}', '2020-12-12 17:21:27'),
('55b13a49092a6c881e5773f356', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 04:01:12'),
('56c6b0246affce4f915efa2119', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 18:31:48'),
('56f31479d0113f0791b9d9e3b3', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 15:52:02'),
('57f7b80e3bc919d8d55ed8376e', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-10 03:55:17'),
('58cc5c3a53415063a4395ce085', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-26 04:04:18'),
('58ef58fd88b0c7dab850a8a164', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-04 03:58:09'),
('594e339ec4dbd9d90d2987c067', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 13:21:36'),
('59a5a6331e2695585ce8c5d102', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 03:56:13'),
('59bc4309527ba03fbbfe4691b1', '{\"api_id\":\"5\"}', '2020-11-29 17:20:14'),
('5a9d765695d79f89537c42866e', 'false', '2020-11-21 03:56:21'),
('5b1475717ceffb47efb86883a2', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 15:07:51'),
('5b9baf4c7a405240b188374ebe', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 04:01:39'),
('5bac250cf1aa4f6a2ac7ce7cb1', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 00:07:23'),
('5bcb57d55fc949bed5de7a159b', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 03:59:12'),
('5bd8078a7326d4d4437b1c12b5', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 04:01:15'),
('5c6ee0bb8150e190bcf5219973', '{\"language\":\"ar\",\"currency\":\"SAR\",\"redirect\":\"https:\\/\\/lanaapp.space\\/isa_mohab\\/index.php?route=account\\/wishlist\"}', '2020-11-29 04:02:50'),
('5cb084ca41d3cef54e5f2927d4', '{\"api_id\":\"6\"}', '2020-11-29 18:06:11'),
('5cc35b2e68a1f2177352a2dc10', '{\"language\":\"ar\",\"currency\":\"SAR\",\"user_id\":\"1\",\"user_token\":\"amFcDt1PRSTa4rMKmAfrUXUrCB0rW23B\"}', '2020-12-01 05:10:46'),
('5d5a2ba041ed8b810698d31537', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-26 18:45:24'),
('5d6a3ad2d4a4e2c8cbd3267927', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 03:56:25'),
('5d8b53612eeac6da8478fd5e46', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 14:43:18'),
('5dccf5038b7c04bd867ca13814', '{\"api_id\":\"5\"}', '2020-11-29 16:42:27'),
('5df2ecc660a2df05df952e8f97', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 00:41:55'),
('5e03bfb11ddf547606c0fd6bd4', '{\"language\":\"ar\",\"currency\":\"SAR\",\"redirect\":\"https:\\/\\/lanaapp.space\\/isa_mohab\\/index.php?route=account\\/return\",\"compare\":[]}', '2020-12-04 03:59:29'),
('5e11c101919d49e72af45abe6d', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 11:51:43'),
('5e24d054ba1ce7f855c9fd37a5', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-24 00:47:21'),
('5e5ffd127a8639c5d5556645db', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 22:08:43'),
('5eb5885ff65dc8c571f8633df8', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 03:59:05'),
('5f3f92885019fdc00260c5d0fe', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:41:57'),
('5f7e2e835e1bff07df89ab4b83', '{\"api_id\":\"5\"}', '2020-11-29 19:35:50'),
('5f9fdfaa0cd8b2e87bae288558', '{\"language\":\"ar\",\"currency\":\"SAR\",\"compare\":[]}', '2020-12-02 03:58:40'),
('5fc8d89c402d4292d7e7e53fc7', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 03:46:49'),
('602c95e5b5eaa80ea559085b09', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-07 03:54:45'),
('6064c3fd6f2b5f010a6f67ac78', '{\"api_id\":\"5\"}', '2020-12-05 00:42:52'),
('60fa2381af21e0e39a5c10c686', '{\"api_id\":\"5\"}', '2020-11-30 15:12:12'),
('60fb8357e37c2920d93e40fa14', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 05:08:15'),
('611ab89244f56a88915add9a9f', '{\"api_id\":\"5\",\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-11 22:05:08'),
('61776e8a011cd7a358aa9257ef', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-28 06:13:39'),
('61a724449a6ddb2c74613eab6e', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-24 22:38:14'),
('61d58b8e86d0127cac6ca33904', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 02:06:34'),
('62e6ad304db43651c3c46a209e', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-04 03:59:23'),
('62ff4bade45b374db9c8459105', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:35:40'),
('63c905f2b450696bf308761206', '{\"user_id\":\"1\",\"user_token\":\"CE8Y7DR0EA9NnuppMgGzAfgtYbJJ4GTs\",\"language\":\"ar\",\"currency\":\"SAR\",\"install\":\"rAIAO1ip5y\"}', '2020-12-03 06:32:24'),
('63ed016468d692bd9a3aaaea65', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-26 16:48:25'),
('640847bdd15a483698d2624573', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 13:21:34'),
('640e811678d259763d1d752fae', '{\"language\":\"ar\",\"currency\":\"USD\",\"account\":\"register\",\"customer_id\":\"5\",\"payment_address\":{\"address_id\":\"5\",\"firstname\":\"fsdf\",\"lastname\":\"dsfsdf\",\"company\":\"\",\"address_1\":\"dsfsfd\",\"address_2\":\"\",\"postcode\":\"55555\",\"city\":\"CAIRO\",\"zone_id\":\"2879\",\"zone\":\"Ar Riyad\",\"zone_code\":\"RD\",\"country_id\":\"184\",\"country\":\"Saudi Arabia\",\"iso_code_2\":\"SA\",\"iso_code_3\":\"SAU\",\"address_format\":\"\",\"custom_field\":null},\"shipping_address\":{\"address_id\":\"5\",\"firstname\":\"fsdf\",\"lastname\":\"dsfsdf\",\"company\":\"\",\"address_1\":\"dsfsfd\",\"address_2\":\"\",\"postcode\":\"55555\",\"city\":\"CAIRO\",\"zone_id\":\"2879\",\"zone\":\"Ar Riyad\",\"zone_code\":\"RD\",\"country_id\":\"184\",\"country\":\"Saudi Arabia\",\"iso_code_2\":\"SA\",\"iso_code_3\":\"SAU\",\"address_format\":\"\",\"custom_field\":null},\"vouchers\":[],\"user_id\":\"1\",\"user_token\":\"ifWJKikmiLMj7HaLTaqGiS5d5IumtwJL\"}', '2020-11-28 19:07:27'),
('6455b07dbc3cfc762e98bd29fc', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-26 00:48:25'),
('6472e5d03005a1b9172da79fab', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-23 19:31:10'),
('64d2eaeaf66d45b61455299f97', '{\"api_id\":\"5\"}', '2020-12-10 19:25:05'),
('66283cf026fc2f013fea2e8244', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 04:00:12'),
('66300730cdfe6181ecf10cd8a4', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 10:54:45'),
('66344e0e8845aef1a3a9bc112c', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-26 17:48:12'),
('66effb17bf7a7a1242f7ab6968', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 20:35:07'),
('66f2a388daf6cb2976f9aca149', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 03:56:17'),
('6713a22ffe0929bbe12a4a9e7b', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 03:58:36'),
('67296ac195f708a6a723b7744c', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 15:07:43'),
('67379f870940919896fc148ab9', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-02 20:43:36'),
('6742304adab91140bd3694fbbc', '{\"language\":\"ar\",\"currency\":\"USD\"}', '2020-12-05 12:05:00'),
('6794b2e16ef2bf6bc5759e0ea4', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 03:55:54'),
('679be57f233f9ca80bef73dc00', '{\"language\":\"ar\",\"currency\":\"SAR\",\"account\":\"guest\",\"payment_address\":{\"firstname\":\"xZXZ\",\"lastname\":\"zxzxzXZ\",\"company\":\"\",\"address_1\":\"sfsdfsfs\",\"address_2\":\"\",\"postcode\":\"ffsdfsdfs\",\"city\":\"rwerwe\",\"country_id\":\"184\",\"zone_id\":\"2879\",\"country\":\"Saudi Arabia\",\"iso_code_2\":\"SA\",\"iso_code_3\":\"SAU\",\"address_format\":\"\",\"custom_field\":[],\"zone\":\"Ar Riyad\",\"zone_code\":\"RD\"},\"shipping_address\":{\"firstname\":\"xZXZ\",\"lastname\":\"zxzxzXZ\",\"company\":\"\",\"address_1\":\"sfsdfsfs\",\"address_2\":\"\",\"postcode\":\"ffsdfsdfs\",\"city\":\"rwerwe\",\"country_id\":\"184\",\"zone_id\":\"2879\",\"country\":\"Saudi Arabia\",\"iso_code_2\":\"SA\",\"iso_code_3\":\"SAU\",\"address_format\":\"\",\"zone\":\"Ar Riyad\",\"zone_code\":\"RD\",\"custom_field\":[]},\"in_cart_Notdropshipping\":\"1\"}', '2020-12-12 17:18:48'),
('67b95bed1215c35ad5d23a1235', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 19:35:54'),
('6800aa557eae9292d7f1e2493f', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 13:21:36'),
('6894def385845a885010a5741c', '{\"api_id\":\"5\",\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-12 22:48:00'),
('68b6f8388b2ed4e7d858d4cc03', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 18:31:47'),
('68b80d1d35d62f9693673d9012', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 04:02:15'),
('69284c869f57d704bbdff796a2', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2020-11-20 20:25:46'),
('695c07b945b61f6b2e33aafd71', '{\"api_id\":\"6\"}', '2020-11-29 18:00:34'),
('69b86a1267eb05b4ba97188316', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 19:15:33'),
('6a7e9a574cba7c7cb4c37a8550', '{\"language\":\"ar\",\"currency\":\"SAR\",\"redirect\":\"https:\\/\\/lanaapp.space\\/isa_mohab\\/index.php?route=account\\/transaction\"}', '2020-12-03 04:01:26'),
('6a8ad9fae37cc85b77175e0a4f', '{\"api_id\":\"5\"}', '2020-12-05 00:49:45'),
('6b13e2069a0ea7ad55fb2375fe', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 18:00:29'),
('6b6c441cf3a67f7b852f0f3b87', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 12:42:08'),
('6b9f4ed751a52d1431193a0355', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 14:35:38'),
('6cb699fe950c17c246fdba925b', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 19:35:15'),
('6d779969eb3030cdc6da276724', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:42:14'),
('6dc31ed55499592da38da8bc57', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 00:41:54'),
('6ddc8816941ca4fb3906529a84', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 15:07:42'),
('6ddd485c12a5a790d874840d3f', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 09:07:45'),
('6ed48fb14cf55b9a5b04ab39e5', '{\"user_id\":\"1\",\"user_token\":\"QR9FTgDGbEmBNgqfkIbFE00u78AcYjW0\"}', '2020-12-13 18:36:07'),
('6f0aca046c11a777d901d4f0a4', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 04:02:43'),
('6f1861b69a8345c62c7925cc73', '{\"api_id\":\"5\"}', '2020-11-30 03:01:57'),
('6f7354d645fa9b18aaacb66820', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-02 20:43:33'),
('70d34ede11b3f856191f2c0c64', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 12:15:55'),
('70d9c27dc975dee9592bb8f179', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-12 12:21:44'),
('70e4fc669f7703089260561831', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 04:04:19'),
('70f37182eeb4aa1be99003de85', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 00:30:19'),
('70ff72bb65a45b363bf1b98509', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 07:28:27'),
('712d9584f1817ee8e9f4e1b137', '{\"api_id\":\"5\"}', '2020-12-12 17:21:32'),
('7137c24c6a18d2f60f41993eee', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 04:04:12'),
('72b68f737d564f1e726d7961be', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:35:33'),
('731870f563625eda1f01a46f20', 'false', '2020-11-29 17:26:24'),
('7355ee0674c113a0748b61599e', '{\"language\":\"ar\",\"currency\":\"SAR\",\"user_id\":\"1\",\"user_token\":\"pEtu8GxfZUb3j3w3LdYw9nGMIRySraVm\"}', '2020-11-21 18:08:31'),
('73811dba0bbc3aa0e319313ad1', '{\"language\":\"ar\",\"currency\":\"SAR\",\"compare\":[]}', '2020-11-26 04:05:42'),
('73aa8c6cb3d42bdf332756287d', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 04:02:17'),
('73d180a0c0b750c88acf531795', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 03:55:51'),
('7470cf4d32c328981d405118a5', '{\"api_id\":\"5\"}', '2020-12-10 19:22:11'),
('74a7395dc7e9b6f7ec8d4b64f6', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 19:21:37'),
('751363bd7efeb11ae178cb2b9c', '{\"api_id\":\"5\"}', '2020-11-29 16:55:23'),
('7539be30edbfa3da3014d81df7', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 10:09:06'),
('75e97482e8cc28b68d45c97895', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-26 04:05:02'),
('762baf95beed5f8fcc382f0334', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:58:23'),
('76389d68d4de316287824e7e81', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-06 04:01:21'),
('765fea369814e3f8a2ec0e70cb', '{\"api_id\":\"5\"}', '2020-11-30 16:00:22'),
('7745ac21f70b1f5b40753d7ed1', '{\"user_id\":\"1\",\"user_token\":\"wsQLxIFPVntRHkv9mh6EFH7MkCWnw4bb\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 03:24:01'),
('776389382b262144acc0b6c312', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 13:21:35'),
('776b2c4d83ed971f3953e6b8ee', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 04:59:48'),
('7790f761daf68df9359585c46a', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 08:31:07'),
('77b5094a23f3ebf4552b26a509', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 03:59:17'),
('784eb2fee3e659b14d291920ea', '{\"api_id\":\"5\"}', '2020-12-01 15:29:50'),
('79f4902791daffadba66e7459b', '{\"api_id\":\"5\"}', '2020-11-29 17:49:52'),
('7a22532c54a902d21086256310', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 04:03:36'),
('7aaff6a502176ed98e477b3322', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 16:07:10'),
('7bdd901629c251dde423cc5a25', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:59:01'),
('7c8a019458e018890d17810fed', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-06 04:01:18'),
('7c902d9e319b7b216cc381b36e', '{\"user_id\":\"1\",\"user_token\":\"vCCRZoQ0XWmi7VdqvCIa1Syxhe04uPXS\",\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-02 01:22:35'),
('7d39cc9cf7db5f30d8971c3c66', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 23:08:27'),
('7d48d0de73e9900c364e969e9e', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 05:53:22'),
('7de45e7de7576a53ad846139f9', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-26 04:04:12'),
('7deef0e946c8d18adf1afeee5f', '{\"api_id\":\"5\"}', '2020-12-01 02:28:46'),
('7e2b0fba5bda3472804505f5a4', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 19:57:11'),
('7e45c8b9bbfadb9804b70e1dd7', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 11:28:41'),
('7eab8595505dd34dec80c4bac0', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 03:57:04'),
('7eedca69f56ad226afd145b395', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 06:40:42'),
('7f612ff40f9eddef25ce190d0d', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 03:59:15'),
('7fcf3f1a8b8db7016dfb67f23d', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 11:28:19'),
('80219f74add86672f86322df75', '{\"api_id\":\"5\"}', '2020-12-05 00:41:43'),
('80916acb1c4dd9f39f63e96d5d', 'false', '2020-11-21 03:56:22'),
('813199db00ae56b2a3eacc9728', '{\"api_id\":\"1\",\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-09-19 04:54:50'),
('815f8fbb9a851028258f7f2138', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 04:05:15');
INSERT INTO `oc_session` (`session_id`, `data`, `expire`) VALUES
('8175c46d27f8527ff9da74fc53', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"oVE0jfajncGisX9kA1L8LXLpnEzPgpoP\",\"account\":\"guest\",\"customer_id\":\"2\",\"payment_address\":{\"address_id\":\"2\",\"firstname\":\"TSFG\",\"lastname\":\"HJGHG\",\"company\":\"JGSJGH\",\"address_1\":\"JHCGH\",\"address_2\":\"ajdghj\",\"postcode\":\"4524252\",\"city\":\"surat\",\"zone_id\":\"1485\",\"zone\":\"Gujarat\",\"zone_code\":\"GU\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":null},\"shipping_address\":{\"address_id\":\"2\",\"firstname\":\"TSFG\",\"lastname\":\"HJGHG\",\"company\":\"JGSJGH\",\"address_1\":\"JHCGH\",\"address_2\":\"ajdghj\",\"postcode\":\"4524252\",\"city\":\"surat\",\"zone_id\":\"1485\",\"zone\":\"Gujarat\",\"zone_code\":\"GU\",\"country_id\":\"99\",\"country\":\"India\",\"iso_code_2\":\"IN\",\"iso_code_3\":\"IND\",\"address_format\":\"\",\"custom_field\":null},\"comment\":\"\",\"order_id\":4,\"compare\":[\"43\"]}', '2017-09-14 13:21:55'),
('81776a7355a6be236d13a2c35a', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 22:07:12'),
('81c11cb7eea8019e449dba4e4e', '{\"language\":\"ar\",\"currency\":\"SAR\",\"redirect\":\"https:\\/\\/lanaapp.space\\/isa_mohab2\\/index.php?route=account\\/order\"}', '2020-12-06 04:02:01'),
('81fbb31c2f6a95e4fa898eafd3', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 03:58:09'),
('8259421cbed8331f31db30643b', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-26 18:36:12'),
('825dc3edb8fd366cb5058057b2', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 03:57:05'),
('828fb0f595ffb1aec1f48083dc', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 19:36:12'),
('829a7acde7f84f4732008261d0', '{\"api_id\":\"5\"}', '2020-11-29 18:36:02'),
('831063a1f28fa0295dec4a2634', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 15:07:51'),
('833d42c0d1f888b0bd2c53e077', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:41:33'),
('83c75ccc631c1c9d637e66e3e8', '{\"api_id\":\"5\"}', '2020-11-29 22:55:01'),
('840b5a10f74ce005070ebee7c9', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:58:52'),
('845aed6b13f6d7b568d0ef9040', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 19:35:36'),
('84a91632168b376e9449ec86db', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 04:04:22'),
('8505af0f7a080f1384a77cc875', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 03:57:26'),
('854b1603f5b4c027a523980864', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 12:22:28'),
('85b9610e027a5e11dbec6b0d00', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 04:05:10'),
('85d5866f93f4eefc7028b8ecf1', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 19:53:18'),
('86381a7fea1f25ef99ab56b26d', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 03:57:03'),
('86811c1f2f152a81e04f53626a', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 04:05:15'),
('873c9a9ebd4c412d40cbf06c50', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:42:39'),
('879c69c1cb0b2309701383701d', '{\"user_id\":\"1\",\"user_token\":\"RFJg11DamRLz5JZicCgD2VzMwCzmAmKD\",\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-02 04:36:44'),
('87a346ba371793543ed0474a97', '{\"api_id\":\"5\",\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-06 00:55:24'),
('87c6506e4e9478d60f2e147e77', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 23:19:11'),
('884ef1fe7ef531634280a8af3b', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-26 22:57:19'),
('88696b1348c9e3108fcef9fd09', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-26 04:05:50'),
('8882e91a79665a35674abe7c93', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 03:56:15'),
('88ac10d16594070fdce08f1dc7', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 21:54:18'),
('894becfb558c24076381bc6d9c', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 03:58:37'),
('898f64c1a8dc99f3f6371f0b72', '{\"api_id\":\"5\"}', '2020-11-29 19:19:49'),
('89a9561cec8890d306c42665fa', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 11:28:34'),
('8a3abf40dcd4bd5d2fab721076', 'false', '2020-11-21 03:56:23'),
('8a74c2921479c32791629589c3', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 04:02:22'),
('8a769291b29f70c1f5d0ff6310', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 19:36:07'),
('8a7e6b34439ce223a1da5baede', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 14:56:00'),
('8a8f694491229c60d74339266b', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 04:02:19'),
('8aa41bd7eeaaa94a5476ec627c', '{\"api_id\":\"6\"}', '2020-11-29 18:09:41'),
('8ab668547453f0ced10d70c940', '{\"language\":\"ar\",\"currency\":\"SAR\",\"compare\":[]}', '2020-11-25 03:57:42'),
('8acf0095cf2d385e651b4c37b3', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-26 00:05:07'),
('8ad1b1e6d0b5be149a4c5eecc7', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 14:43:16'),
('8b910fe8730dfd0a35ab0781d6', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 23:08:40'),
('8b98b22af8c9bb45814ddbebbc', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 14:43:16'),
('8bb795d23166a3e23ca13cb72a', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:58:55'),
('8bcfc3638ca2530351c3b5b970', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 04:02:19'),
('8c3bffa8d814b8a80f19aeb820', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-25 12:34:36'),
('8c408a5728e90dcdf4ce1e30ab', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 08:31:09'),
('8cb1aa133bd9f59c89ffa4660d', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-05 04:04:45'),
('8ce45ddb2f1d28f1bed6190899', '{\"api_id\":\"5\"}', '2020-12-10 19:23:10'),
('8ce8ae357701c4383e3558cb63', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-23 18:57:20'),
('8d05faf83632f1a699aa676d71', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 03:57:03'),
('8d153b42cb46e0b38d65c0b150', 'false', '2020-12-10 19:00:10'),
('8d5a33440a59a126c2150f1c7d', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-07 03:55:06'),
('8d8fa3b6ddfbb56c8da76f4641', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:42:33'),
('8dfddd6b84456dd2b749f19aa8', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 04:26:50'),
('8e6177d18ff09017778e6a7bbe', '{\"api_id\":\"5\"}', '2020-12-05 01:48:36'),
('8eaf00cbd8ad13c27a606e3b17', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:36:19'),
('8f6f8177bd1f5afcd9bade237f', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 08:06:23'),
('8f71215eb29f634a7bc315c171', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-05 15:06:56'),
('8fb112938c17da10967df6628f', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 03:58:49'),
('902566a968e7640a210e9aec4c', '{\"api_id\":\"5\"}', '2020-12-11 22:05:16'),
('902e23b0ad301b7f794561c7ed', '{\"user_id\":\"1\",\"user_token\":\"Y5hnA7YaH4m4P9baqmEeFEGTx3052Dfn\"}', '2020-11-29 01:58:52'),
('90a685601319765f7fb41b62fe', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 15:23:58'),
('90badce753db1b5a6a083dc6be', '{\"api_id\":\"5\"}', '2020-12-06 00:55:37'),
('911c2bc00ffb469d9b63cc55a3', '{\"api_id\":\"5\"}', '2020-12-01 02:36:23'),
('916165202ad77f8633502b4ab4', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 04:01:25'),
('9186dd16973fb2e331a6198cbe', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 04:04:16'),
('9196e84513ebeeabbd3cc39cb9', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 05:30:29'),
('91a481e3fafbe00c2e282e6363', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:59:18'),
('91f5f7971d83d84b4b09e194cb', '{\"api_id\":\"5\"}', '2020-12-05 00:43:12'),
('922960cc5b12a1c9f0ec916cea', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-07 03:55:56'),
('931ec9ee26bfa5e344f6daf9a8', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:42:27'),
('9350ce21e9d955855185c4bf30', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-04 03:59:03'),
('93a1a467da6c5903538fc6a09f', '{\"user_id\":\"1\",\"user_token\":\"stWUfRa2d6weKdodzqKmMYa1xCYUkhFv\"}', '2020-12-06 14:24:09'),
('9401efa3434c83832ed0e0d5d2', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 23:08:42'),
('9414b5bfb811bc549bc037a92d', '{\"user_id\":\"1\",\"user_token\":\"SiJiqhaI5c9zuvZWMvfWizoxOEmiO71X\",\"language\":\"ar\",\"currency\":\"SAR\",\"account\":\"guest\",\"payment_address\":{\"firstname\":\"vl;dfl;\",\"lastname\":\";ldsl;v\",\"company\":\"\",\"address_1\":\"kofeldc`\",\"address_2\":\"\",\"postcode\":\"12345\",\"city\":\"cdkf\",\"country_id\":\"184\",\"zone_id\":\"2885\",\"country\":\"Saudi Arabia\",\"iso_code_2\":\"SA\",\"iso_code_3\":\"SAU\",\"address_format\":\"\",\"custom_field\":[],\"zone\":\"Najran\",\"zone_code\":\"NR\"},\"shipping_address\":{\"firstname\":\"cel\",\"lastname\":\"ckfk\",\"company\":\"\",\"address_1\":\"kofeldc`\",\"address_2\":\"\",\"postcode\":\"12345\",\"city\":\"cdkf\",\"country_id\":\"184\",\"zone_id\":\"2885\",\"country\":\"Saudi Arabia\",\"iso_code_2\":\"SA\",\"iso_code_3\":\"SAU\",\"address_format\":\"\",\"zone\":\"Najran\",\"zone_code\":\"NR\",\"custom_field\":[]},\"notes_cart_data\":[{\"product_id\":\"41\",\"price\":100,\"product_cost\":\"50.0000\",\"quantity\":\"1\",\"balance\":50,\"dropshipping\":\"1\"}],\"in_cart_dropshipping\":\"1\",\"guest\":{\"customer_group_id\":\"1\",\"firstname\":\"vl;dfl;\",\"lastname\":\";ldsl;v\",\"email\":\"slfd@dflek.com\",\"telephone\":\"234565456\",\"custom_field\":[],\"shipping_address\":false},\"payment_methods\":[]}', '2020-12-05 03:47:23'),
('941851bc5bb925adb6a0a5c56e', '{\"api_id\":\"5\"}', '2020-12-06 00:55:34'),
('941d6c5d07894184595389964c', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 04:01:18'),
('941fb00e1a868f85c5f66fdae2', '{\"language\":\"ar\",\"currency\":\"SAR\",\"redirect\":\"https:\\/\\/lanaapp.space\\/isa_mohab\\/index.php?route=account\\/wishlist\"}', '2020-11-27 03:59:22'),
('9487e7bc856e04b3ffd3a340ea', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 21:15:01'),
('949371979708f761431a1b8592', '{\"api_id\":\"5\"}', '2020-12-05 00:41:47'),
('94ac26c2ec948fe2015460080b', '{\"api_id\":\"6\"}', '2020-11-29 18:04:22'),
('950901ae6e855e7ad190edfc7b', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-08 04:01:58'),
('961089c512d5f8a30b3c9c8a8f', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 13:21:39'),
('96a0b691d9a152b95edfd82683', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 05:52:00'),
('992e6233ce011ac361c4bc960b', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 04:05:12'),
('993087be84fb3837a4def25a2e', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 04:01:18'),
('994f77be026a9d03a4ccda7fb8', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 13:21:33'),
('99d97dbc9804280eeec260e392', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 04:02:16'),
('9a084bb4523d2e84086506d801', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 04:02:50'),
('9a56bafd1b2c71e3497f98b793', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 15:24:00'),
('9a5a4545d1c8b714b93cef8703', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-13 19:52:40'),
('9a9ece425883f4f85a082a4d29', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-02 07:13:59'),
('9b10c8cd878ac8667b1cb82e47', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:42:43'),
('9b1e45e187f740a89b7bf44021', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 04:01:24'),
('9b4dd88559fa4733c1d53d8f3a', '{\"api_id\":\"6\"}', '2020-11-29 18:02:43'),
('9b6580c7f6b0e3a7a8d5556766', 'false', '2020-11-29 17:26:24'),
('9b6f7bce7e2f6ab80cd3b001bd', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 10:54:43'),
('9bc79539f7ecf0cb93a9ff27ff', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 00:05:50'),
('9c08731c76aab0059454f8a24f', '{\"language\":\"ar\",\"currency\":\"USD\"}', '2020-11-26 18:59:34'),
('9c34956d71cfde1bbf6859bd64', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-10 19:25:12'),
('9da248c6c7ffb76f967a9679ff', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 07:55:11'),
('9db8139e89b131309fb6000aa8', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-02 20:43:35'),
('9de80bbea5feeef21e0d069afd', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 13:21:38'),
('9eaa5f0ef334656497e36b7f4b', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 03:57:06'),
('9ec0514dfe89e165cbb5afeda3', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 13:21:37'),
('9ed097cd58610c0da48f816285', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-02 20:43:34'),
('9f084116ced8167b19c721c566', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-24 13:25:49'),
('9f5f92b8bd44bbb38fb602145a', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:59:21'),
('9f7db0137b92212d6d6e2f8690', '{\"language\":\"ar\",\"currency\":\"SAR\",\"account\":\"guest\",\"guest\":{\"customer_group_id\":\"1\",\"firstname\":\"klfk\",\"lastname\":\"kdkl\",\"email\":\"kdkl@ccc.com\",\"telephone\":\"345676543\",\"custom_field\":[],\"shipping_address\":\"1\"},\"payment_address\":{\"firstname\":\"klfk\",\"lastname\":\"kdkl\",\"company\":\"\",\"address_1\":\"regge\",\"address_2\":\"\",\"postcode\":\"12321\",\"city\":\"fdbg\",\"country_id\":\"184\",\"zone_id\":\"2877\",\"country\":\"Saudi Arabia\",\"iso_code_2\":\"SA\",\"iso_code_3\":\"SAU\",\"address_format\":\"\",\"custom_field\":[],\"zone\":\"Al Madinah\",\"zone_code\":\"MD\"},\"shipping_address\":{\"firstname\":\"klfk\",\"lastname\":\"kdkl\",\"company\":\"\",\"address_1\":\"regge\",\"address_2\":\"\",\"postcode\":\"12321\",\"city\":\"fdbg\",\"country_id\":\"184\",\"zone_id\":\"2877\",\"country\":\"Saudi Arabia\",\"iso_code_2\":\"SA\",\"iso_code_3\":\"SAU\",\"address_format\":\"\",\"zone\":\"Al Madinah\",\"zone_code\":\"MD\",\"custom_field\":[]},\"comment\":\"\",\"order_id\":52,\"PayLink_transactionNo\":\"1607165450922\",\"shipping_methods\":{\"flat\":{\"title\":\"Flat Rate\",\"quote\":{\"flat\":{\"code\":\"flat.flat\",\"title\":\"Flat Shipping Rate\",\"cost\":\"5.00\",\"tax_class_id\":\"9\",\"text\":\"5.00\\u0631\\u064a\\u0627\\u0644\"}},\"sort_order\":\"1\",\"error\":false},\"free\":{\"title\":\"\\u0634\\u062d\\u0646 \\u0645\\u062c\\u0627\\u0646\\u064a\",\"quote\":{\"free\":{\"code\":\"free.free\",\"title\":\"\\u0634\\u062d\\u0646 \\u0645\\u062c\\u0627\\u0646\\u064a\",\"cost\":0,\"tax_class_id\":0,\"text\":\"0.00\\u0631\\u064a\\u0627\\u0644\"}},\"sort_order\":\"1\",\"error\":false}}}', '2020-12-05 14:20:04'),
('9fdb0dba41df62a4ce49abcab0', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 07:28:29'),
('9fdfd07709450abe07e01f36bb', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-06 04:01:34'),
('a00e082428d103dbb55e8d4d9c', '{\"api_id\":\"5\"}', '2020-11-29 19:35:54'),
('a07b9477a07f1ded939a6b985b', '{\"api_id\":\"5\",\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-11 22:05:27'),
('a0b687082727fa92756504a576', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-10 03:55:02'),
('a0dad34d4a36df9ac38f9658a7', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 08:31:08'),
('a113544f712ac7a30c88621468', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 19:36:09'),
('a1351b78bd1ad6ff29acf4bb8e', '{\"language\":\"en-gb\",\"currency\":\"SAR\",\"user_id\":\"1\",\"user_token\":\"PZJYrGIKhF9qXgjkMvtNbd2h0OPkIkAP\"}', '2020-12-05 20:30:04'),
('a13ccf6764e4d770ed36b772eb', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 10:54:42'),
('a15a53ef6ce6a865d07c0472e3', '{\"api_id\":\"5\",\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-06 00:55:33'),
('a15eda482faaf79d1e8babb8b9', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 04:04:24'),
('a1993d1d6710ec8b64eb9ddf31', '{\"language\":\"ar\",\"currency\":\"SAR\",\"compare\":[]}', '2020-11-30 03:57:45'),
('a1af90af6e6bbf9d35ebd1c726', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 15:07:49'),
('a2317d3488ad4aa6ba78e3816a', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 14:43:16'),
('a2ddd666cf4f2b45401633d89c', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 03:46:53'),
('a30c799cf2cb46751cdae017be', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 02:06:40'),
('a336928cdf0d7805575243a871', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:59:16'),
('a3385d9ace343a827940a36729', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-22 17:04:08'),
('a34404d16c9e8153b0ec604ac2', '{\"language\":\"ar\",\"currency\":\"SAR\",\"account\":\"guest\",\"guest\":{\"customer_group_id\":\"1\",\"firstname\":\"lkekl\",\"lastname\":\"lksdkl\",\"email\":\"df@fgg.com\",\"telephone\":\"234565456\",\"custom_field\":[],\"shipping_address\":\"1\"},\"payment_address\":{\"firstname\":\"lkekl\",\"lastname\":\"lksdkl\",\"company\":\"\",\"address_1\":\"sfgdr\",\"address_2\":\"\",\"postcode\":\"12321\",\"city\":\"fg\",\"country_id\":\"184\",\"zone_id\":\"2874\",\"country\":\"Saudi Arabia\",\"iso_code_2\":\"SA\",\"iso_code_3\":\"SAU\",\"address_format\":\"\",\"custom_field\":[],\"zone\":\"Al Bahah\",\"zone_code\":\"BH\"},\"shipping_address\":{\"firstname\":\"lkekl\",\"lastname\":\"lksdkl\",\"company\":\"\",\"address_1\":\"sfgdr\",\"address_2\":\"\",\"postcode\":\"12321\",\"city\":\"fg\",\"country_id\":\"184\",\"zone_id\":\"2874\",\"country\":\"Saudi Arabia\",\"iso_code_2\":\"SA\",\"iso_code_3\":\"SAU\",\"address_format\":\"\",\"zone\":\"Al Bahah\",\"zone_code\":\"BH\",\"custom_field\":[]},\"shipping_methods\":{\"flat\":{\"title\":\"Flat Rate\",\"quote\":{\"flat\":{\"code\":\"flat.flat\",\"title\":\"Flat Shipping Rate\",\"cost\":\"5.00\",\"tax_class_id\":\"9\",\"text\":\"5.00\\u0631\\u064a\\u0627\\u0644\"}},\"sort_order\":\"1\",\"error\":false},\"free\":{\"title\":\"\\u0634\\u062d\\u0646 \\u0645\\u062c\\u0627\\u0646\\u064a\",\"quote\":{\"free\":{\"code\":\"free.free\",\"title\":\"\\u0634\\u062d\\u0646 \\u0645\\u062c\\u0627\\u0646\\u064a\",\"cost\":0,\"tax_class_id\":0,\"text\":\"0.00\\u0631\\u064a\\u0627\\u0644\"}},\"sort_order\":\"1\",\"error\":false}},\"shipping_method\":{\"code\":\"flat.flat\",\"title\":\"Flat Shipping Rate\",\"cost\":\"5.00\",\"tax_class_id\":\"9\",\"text\":\"5.00\\u0631\\u064a\\u0627\\u0644\"},\"comment\":\"\",\"payment_methods\":{\"cod\":{\"code\":\"cod\",\"title\":\"\\u0627\\u0644\\u062f\\u0641\\u0639 \\u0639\\u0646\\u062f \\u0627\\u0644\\u062a\\u0633\\u0644\\u064a\\u0645\",\"terms\":\"\",\"sort_order\":\"1\"},\"paylink\":{\"code\":\"paylink\",\"title\":\"<img src=\\\"catalog\\/view\\/theme\\/default\\/image\\/paylink.png\\\" alt=\\\"onlinepay\\\" title=\\\"onlinepay\\\" width=\\\"35%\\\"\\/>\",\"terms\":\"\",\"sort_order\":\"1\"}},\"user_id\":\"1\",\"user_token\":\"DenfSjiNTPQgZdJj5agLYtB2akstJs5c\"}', '2020-12-05 12:17:30'),
('a3ba53839cdbd202c8a682eb0b', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 04:00:31'),
('a59b9c1dc03d30baed6c017f53', '{\"language\":\"ar\",\"currency\":\"SAR\",\"user_id\":\"1\",\"user_token\":\"esn9J6t1u8GE6JCDdgzzyR21THpoRR8H\"}', '2020-12-01 19:19:18'),
('a5cb7bd8f62ea27e1a49c1138e', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 05:14:39'),
('a5cc643b9fabe79f65ab2d6805', '{\"api_id\":\"5\"}', '2020-12-04 22:31:04'),
('a621c73041b9e35408c4792cad', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-03 00:52:11'),
('a63b9981b506dc35e2438b9638', '{\"language\":\"ar\",\"currency\":\"SAR\",\"vouchers\":[]}', '2020-11-27 21:20:29'),
('a758360746ae6398b9d48ee35f', '{\"language\":\"ar\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"uD96lBKgknnCh9eTLWKCw7ytD5SMdX1H\"}', '2020-11-21 10:13:58'),
('a7854cecd33e3d9e7d178c1096', 'false', '2020-11-29 17:26:23'),
('a7a3d9b313d0948714b0db9a88', 'false', '2020-11-21 03:56:28'),
('a7ab38b0d79bec204584d3cd81', '{\"api_id\":\"5\"}', '2020-12-13 19:50:28'),
('a7af34f2d4fd7e5677befc88fc', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 10:09:07'),
('a852f06692422be5ee7ae4796f', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 18:31:47'),
('a87571a87be9188e8d3c0c7ccb', '{\"language\":\"en-gb\",\"currency\":\"SAR\",\"account\":\"guest\",\"payment_address\":{\"firstname\":\"mmm\",\"lastname\":\"cvbnm,\",\"company\":\"\",\"address_1\":\"ddsdsd\",\"address_2\":\"\",\"postcode\":\"342423\",\"city\":\"sdsad\",\"country_id\":\"184\",\"zone_id\":\"2879\",\"country\":\"Saudi Arabia\",\"iso_code_2\":\"SA\",\"iso_code_3\":\"SAU\",\"address_format\":\"\",\"custom_field\":[],\"zone\":\"Ar Riyad\",\"zone_code\":\"RD\"},\"shipping_address\":{\"firstname\":\"mmm\",\"lastname\":\"cvbnm,\",\"company\":\"\",\"address_1\":\"ddsdsd\",\"address_2\":\"\",\"postcode\":\"342423\",\"city\":\"sdsad\",\"country_id\":\"184\",\"zone_id\":\"2879\",\"country\":\"Saudi Arabia\",\"iso_code_2\":\"SA\",\"iso_code_3\":\"SAU\",\"address_format\":\"\",\"zone\":\"Ar Riyad\",\"zone_code\":\"RD\",\"custom_field\":[]},\"in_cart_Notdropshipping\":\"1\"}', '2020-12-10 19:29:24'),
('a9ca5a82e85d7396c4ee5eca63', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 03:58:38'),
('a9fa9b385e199e5796fdb50137', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 22:07:17'),
('a9fc1b18b2c591eb8352b46dde', '{\"api_id\":\"5\"}', '2020-11-30 16:37:48'),
('aab7c993f863633e0c7df31b23', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 03:55:56'),
('ab0ca9bc339674615de6a5771f', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 20:47:32'),
('ab18969ff6394577b9fc754d4e', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 05:52:03'),
('ab3572af55068aae54c7ce8eb2', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-11 22:04:25'),
('ab3949309605a85d7256d0cb46', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 10:54:46'),
('acaf4c01a36853c3ff43b0f727', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 04:31:18'),
('add62b80ac4ef99e3901a4a60d', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 09:07:45'),
('ae27dce5a1167047c96abe7f6e', '{\"api_id\":\"6\"}', '2020-11-29 18:10:48'),
('ae2f6ab04f38dc52fb38442364', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 03:58:07'),
('ae6d556b5a755206c1bfcbec83', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 03:36:13'),
('ae990f9779fa94e79b3d416707', '{\"language\":\"ar\",\"currency\":\"SAR\",\"redirect\":\"https:\\/\\/lanaapp.space\\/isa_mohab2\\/index.php?route=account\\/order\"}', '2020-12-08 04:02:28'),
('ae9a9566b2a9ee71d336055932', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-13 07:07:28'),
('aeb34fc89fc40200824a1bb44f', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 13:21:34'),
('af1bebe43bfdd6f7c44c7d47a2', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 16:04:44'),
('af6043746daf50f1a9d4a5e444', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 06:13:27'),
('af901cb706d493c33e54a82b68', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-08 04:02:06'),
('afa528002bb94543936c1fa196', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-05 04:09:01'),
('afa918f8340606998249921c4a', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 03:46:52'),
('afb70ae14a95bd520ebd2ddf03', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 13:24:04'),
('b00137bbd0fbd1320a85165c63', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:58:24'),
('b046a1e1901b446d6e557e8952', '{\"api_id\":\"5\"}', '2020-12-05 00:36:20'),
('b0497b26e030a937a496f412f1', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-23 21:06:12'),
('b07d369fb033fe651050c8e7b1', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-24 09:06:07'),
('b0d99cd137915e8b7d665973e1', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 04:05:59'),
('b15d116eb23c55f9c07101a6f4', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 22:52:27'),
('b161156ca8250aafd41dc50c8d', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 14:22:48'),
('b19dad4766aca06196375d52a0', '{\"api_id\":\"5\"}', '2020-12-05 03:48:31'),
('b1d7ecd02d0ad0f92095eb5597', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-09-15 08:57:17'),
('b204cf25ac45576b026b897bc9', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 16:07:04'),
('b282546408591093870f6c7278', '{\"api_id\":\"5\"}', '2020-12-04 22:45:33'),
('b34afcaaea63ac6faad0529ffd', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 07:28:28'),
('b38d986f73fb71b4d56d800fb5', '{\"api_id\":\"4\"}', '2020-11-21 23:24:31'),
('b40a61682a958e334875b7fa78', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 05:52:17'),
('b4124c2f46d815e22d3b9fd82f', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 03:56:15'),
('b4a9e1eff74e9d953ce5651f2a', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-05 15:12:59'),
('b50c60df128e172eee396bea1a', '{\"language\":\"ar\",\"currency\":\"SAR\",\"compare\":[]}', '2020-12-01 04:05:57'),
('b530358f4898bce11a0d28ed5b', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-04 03:58:20'),
('b59968c42bb68a68eb8cf5c8d2', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 14:43:18'),
('b5c75ae794ffb64540926e4331', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 04:03:01'),
('b5f692efe5ffc245199b9af691', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-29 02:02:40'),
('b60c13d18000fe0e3b4ebc231b', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 05:40:30'),
('b6246d79ce485ada1681cbdf46', '{\"language\":\"ar\",\"currency\":\"SAR\",\"redirect\":\"https:\\/\\/lanaapp.space\\/isa_mohab\\/index.php?route=account\\/wishlist\"}', '2020-11-26 04:05:29'),
('b78f4cdb7541bdef0b0d7f8dc5', '{\"language\":\"ar\",\"currency\":\"SAR\",\"compare\":[]}', '2020-11-24 03:57:21'),
('b7d0bcd227e82240af8d819c76', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-07 03:55:58'),
('b7d781e819bab81db35c9ffe47', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 10:54:47'),
('b80beea877866d419c0e490aeb', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 08:31:39'),
('b84286daa6436fcbf6f5d5073a', '{\"api_id\":\"5\"}', '2020-12-10 19:25:09'),
('b85d3571453b11ef2179beea1c', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-06 04:02:02'),
('b874909ba0b6e46f299db33083', '{\"user_id\":\"1\",\"user_token\":\"PEp81moRmHzidtpfPXQ51PxwWiMjPAJL\"}', '2020-12-13 07:08:20'),
('b8853587e530e1e474a90a9935', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 07:28:26'),
('b983c1c34b0956b45acd97a51b', '{\"user_id\":\"1\",\"user_token\":\"dtkRXPz3nkwPEShh472tm8b41d09WOSY\",\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-28 17:21:13'),
('b9967d35def34e837b2fb94bda', '{\"api_id\":\"5\"}', '2020-11-29 16:57:33'),
('b9f9554ef1dbe1275032335e32', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 04:05:13'),
('ba93aecc6c7e7a439c4aa560f8', '{\"language\":\"ar\",\"currency\":\"SAR\",\"account\":\"guest\",\"guest\":{\"customer_group_id\":\"1\",\"firstname\":\"\\u0645\\u0624\\u064a\\u0633\\u0645\",\"lastname\":\"\\u064a\\u0633\\u0643\\u0645\\u0645\\u0643\",\"email\":\"lkdv@f.com\",\"telephone\":\"2345678\",\"custom_field\":[],\"shipping_address\":false},\"payment_address\":{\"firstname\":\"\\u0645\\u0624\\u064a\\u0633\\u0645\",\"lastname\":\"\\u064a\\u0633\\u0643\\u0645\\u0645\\u0643\",\"company\":\"\",\"address_1\":\"3fhdnf\",\"address_2\":\"\",\"postcode\":\"12321\",\"city\":\"fb;df\",\"country_id\":\"184\",\"zone_id\":\"2885\",\"country\":\"Saudi Arabia\",\"iso_code_2\":\"SA\",\"iso_code_3\":\"SAU\",\"address_format\":\"\",\"custom_field\":[],\"zone\":\"Najran\",\"zone_code\":\"NR\"},\"payment_methods\":[]}', '2020-12-05 03:43:49'),
('bab57cd59b4e88f50c9a05a97c', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 14:43:56'),
('bb81904d9783823c8abcaef4ae', 'false', '2020-11-29 17:26:25'),
('bc2250baa1a55b435c29d380c9', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 04:01:15'),
('bc436dc8d8ed1489af05a63b46', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 04:05:14'),
('bd06e1950722b7ec69a69c4cfb', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 13:21:40'),
('bd0837058f19b0fe8030019ed8', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 03:57:02'),
('bd103c7e7ac9b98d275654318b', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:58:38'),
('bdc2eb9db8e9b19c9084fb3e24', '{\"language\":\"ar\",\"currency\":\"SAR\",\"compare\":[]}', '2020-11-27 03:59:38'),
('be83925605b514cb0bb17bb441', '{\"api_id\":\"1\"}', '2017-09-17 06:09:37'),
('be9a592489862cd6bd4d8f228f', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:59:04'),
('beba1c2c3e331d4474e8e79194', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 15:07:46'),
('bed8aaddc9caacb04e7ae4fb9f', 'false', '2020-11-21 03:54:34'),
('bf359765d7e3c2f37d24601af0', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 03:57:45'),
('bf584c204b5909cee166948751', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 05:52:07'),
('bf6448bcd31d7b9d574714ba40', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2020-11-20 18:27:18'),
('bf8d80a6d5e8147080ed1dc94f', '{\"api_id\":\"5\"}', '2020-12-02 15:59:17'),
('c00d673ea3f461dc52d220e8bd', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 04:35:28'),
('c01792acea63b1bd9c364f7e6f', '{\"language\":\"ar\",\"currency\":\"SAR\",\"redirect\":\"https:\\/\\/lanaapp.space\\/isa_mohab\\/index.php?route=account\\/transaction\"}', '2020-12-05 03:59:34'),
('c0861f422747c41485db2a5e0b', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-06 04:01:06'),
('c0a441085da7ca8902c6220bd8', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 08:31:48'),
('c0e2fe9edff99613348b7e1464', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-29 02:00:00'),
('c121bfd3cd256a7fac09e03326', '{\"api_id\":\"5\"}', '2020-12-05 00:43:06'),
('c13ff410dfbbac1eed2097e29b', '{\"api_id\":\"5\"}', '2020-12-12 17:19:02'),
('c1bad072d4174af378411ce3b7', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-12 17:21:39'),
('c20ad3f821ecd0246f43c67a97', '{\"user_id\":\"1\",\"user_token\":\"DNwbASt5oYexKYsZK79slRFim7xF9EPf\"}', '2020-12-10 19:27:58'),
('c24da33ae009c7cff33c0f3140', '{\"api_id\":\"6\"}', '2020-11-29 18:17:00'),
('c27f2202cfec9adac27559d7a1', '{\"api_id\":\"5\"}', '2020-12-10 19:16:41'),
('c27fff79e5f637124d008a3542', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 21:14:35'),
('c2b53de723c9753394227b2302', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 19:35:26'),
('c3108641eaec57fe7070aa6a8e', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 15:07:48'),
('c3328296baf5aa377773469be8', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 04:05:11'),
('c3654c650313274b74f4286f40', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 02:55:52'),
('c37bf4a4d97089e9e8bbd08c29', '{\"api_id\":\"6\"}', '2020-11-29 18:00:26'),
('c38edca3746e260b0234e464a9', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 05:56:40'),
('c41f36c72028a84cac7989fcdc', '{\"api_id\":\"5\"}', '2020-11-29 16:42:33'),
('c44355f2a143faf4c071c6b7f6', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 15:07:47'),
('c5192416eaa2070acccf1b5b72', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 05:52:15'),
('c529562300b0dc4daf78863543', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 03:59:23'),
('c588c38402a4437a9ff163979a', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 15:07:49'),
('c5ad002b0097ddbb189b481728', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 14:43:16'),
('c5f1a741c6abed9f88b2969339', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 16:07:19'),
('c5fce442826d462c278edfb223', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-26 04:04:14'),
('c62424a41526db42b55bb1df5d', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 04:01:17'),
('c6e0482735580740a24f980035', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 01:59:48'),
('c6f8f2f2a79df5431001bb3cba', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 20:49:28'),
('c8091566ebd0373ce405a76c14', '{\"api_id\":\"6\"}', '2020-11-29 18:16:48'),
('c889f231c98af509b283ff9d7e', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 15:49:00'),
('c916dd4d03403287f4d95d9ed9', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-26 04:05:26'),
('c9252919722bac9fa77bb1d2c0', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:59:02'),
('c9727b324b13eeec170844aa5f', '{\"api_id\":\"5\"}', '2020-12-01 03:46:10'),
('c9857b4831b9703b6c2d4190a3', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 03:56:21'),
('c9e996e21883c69975135f066d', '{\"api_id\":\"5\"}', '2020-12-10 19:19:16'),
('ca8019e4b5ee438d50b68bcb1e', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 03:06:52'),
('ca95385673e61b2a73c16fb5ef', '{\"api_id\":\"5\"}', '2020-11-30 16:37:51'),
('cb2cabb2aefedd71a93a138bb1', '{\"language\":\"ar\",\"currency\":\"SAR\",\"redirect\":\"https:\\/\\/lanaapp.space\\/isa_mohab2\\/index.php?route=account\\/order\"}', '2020-12-09 03:58:22'),
('cba939191779f2749e0088e96f', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 05:52:02'),
('cc0f2a77ba397276f6b91e75b0', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 15:07:50'),
('cc77d522b85ec4b7978236ecab', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-24 17:31:57'),
('cc79ef412868caa1cdb08b4f7b', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-24 12:21:17'),
('cd0743f0594ad2419c5c9b9fa9', '{\"language\":\"ar\",\"currency\":\"SAR\",\"compare\":[]}', '2020-11-29 04:02:59'),
('cd099cbf4047a9c09b5b4121f6', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-06 04:01:43'),
('cd3ab8df525ac87c8c1d6a2cc0', '{\"language\":\"ar\",\"currency\":\"SAR\",\"compare\":[]}', '2020-12-03 04:02:48'),
('cd4bfcaedb2afecf979206e119', '{\"api_id\":\"5\"}', '2020-12-11 21:00:50'),
('cd7091b6d737110385f9516206', '{\"api_id\":\"5\"}', '2020-11-29 17:04:07'),
('cdbd306e95ce40c5dd142759fa', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 04:02:09'),
('cde3cd4eaaad99fc18e2cf9392', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 05:29:49'),
('ce34db841b0ed51cd54522dda0', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 10:54:46'),
('ce3a2fc81493c615502baef658', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:58:57'),
('ce7ae41c3c193dd4ac2fa08cbf', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:36:03'),
('cefb64369105f707891c05d0bb', '{\"user_id\":\"1\",\"user_token\":\"VOJNulL3sjgwGf6sCEMoMORlGB4kiBUt\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 18:57:12'),
('cf3603878819e33ffe7ec0df53', '{\"api_id\":\"5\"}', '2020-12-02 12:37:13'),
('cfa3987ef633e644a4c725d3d7', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 03:56:38'),
('cfc519c880507197f187746ff3', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 03:58:03'),
('d0386feeb3c4945113fc9188f6', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 00:05:50'),
('d0512db9cc104b47a471f8f774', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 16:07:10'),
('d0845f4e3ae9bb2630f70b0178', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:59:12'),
('d0c963a353bda800c8609e6838', '{\"language\":\"ar\",\"currency\":\"USD\"}', '2020-11-24 00:41:06'),
('d1b810b47fe435589be16f4535', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2020-11-20 18:25:31'),
('d259b9adbeac922f464ad7bf1d', '{\"api_id\":\"5\"}', '2020-12-01 02:28:50'),
('d2620900bc85818e7a6c45ea03', '{\"api_id\":\"6\"}', '2020-11-29 18:12:37'),
('d279969188532dd3798e27da25', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 03:55:51'),
('d2e5e79541fa9c3d411595cabf', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 03:57:47'),
('d2fbaafbb2e5eab96931f397a0', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 04:02:14'),
('d329639110dce415f4557e1d45', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 00:20:23'),
('d3af92fb1cfa6bdf33619d8161', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-09-19 05:19:45'),
('d3d167750df5073f9bc1cb2ad0', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 10:46:58'),
('d4221903888e6b9e85a9d819d1', '{\"api_id\":\"6\"}', '2020-11-29 18:11:13'),
('d42283a7e17eb7ff4c90026654', '{\"api_id\":\"5\"}', '2020-12-12 17:21:36'),
('d4762363e540a8e16be76bf8ed', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 06:36:21'),
('d4af58ec1f50ffca43c2f2bf45', '{\"language\":\"ar\",\"currency\":\"SAR\",\"compare\":[]}', '2020-11-28 04:04:04'),
('d5b8e17fd2a0413829c9f564ab', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-08 04:02:31'),
('d5f1c5889b4d1e73be55ef8afa', '{\"language\":\"ar\",\"currency\":\"SAR\",\"install\":\"BLQP1y5cSz\",\"account\":\"guest\",\"payment_address\":{\"firstname\":\"mohab\",\"lastname\":\"mamdouh\",\"company\":\"\",\"address_1\":\"tsadasd\",\"address_2\":\"\",\"postcode\":\"23232\",\"city\":\"cairo\",\"country_id\":\"63\",\"zone_id\":\"1008\",\"country\":\"Egypt\",\"iso_code_2\":\"EG\",\"iso_code_3\":\"EGY\",\"address_format\":\"\",\"custom_field\":[],\"zone\":\"Al Jizah\",\"zone_code\":\"JZH\"},\"shipping_address\":{\"firstname\":\"mohab\",\"lastname\":\"mamdouh\",\"company\":\"\",\"address_1\":\"tsadasd\",\"address_2\":\"\",\"postcode\":\"23232\",\"city\":\"cairo\",\"country_id\":\"63\",\"zone_id\":\"1008\",\"country\":\"Egypt\",\"iso_code_2\":\"EG\",\"iso_code_3\":\"EGY\",\"address_format\":\"\",\"zone\":\"Al Jizah\",\"zone_code\":\"JZH\",\"custom_field\":[]},\"PayLink_transactionNo\":\"1606773697181\",\"FAILED_PAYMENT\":{\"payment_method\":\"paylink\",\"payment_transaction\":\"1606773697181\"},\"product_cost\":[\"41\"],\"data\":[{\"prosuct_id\":\"41\",\"price\":100,\"product_cost\":\"0.0000\",\"quantity\":\"1\",\"dropshipping\":\"1\"}],\"notes_cart_data\":[{\"product_id\":\"46\",\"price\":1000,\"product_cost\":\"900.0000\",\"quantity\":\"1\",\"balance\":900,\"dropshipping\":\"1\"}],\"in_cart_dropshipping\":\"1\"}', '2020-12-01 16:00:25'),
('d620eea3e1bc30f58926f2b8cd', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:59:13'),
('d63950d0e9aa38d09249e6808c', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-07 03:55:14'),
('d66f341da61fb4b52e6af4369b', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 03:57:24'),
('d68c688d86b988409777dcdbf4', '{\"api_id\":\"5\"}', '2020-12-13 19:44:16'),
('d6f65317418547f3d58f3d0b3a', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:42:02'),
('d7012686363adc5ad6c18ffced', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:42:19'),
('d81e877f3cef18bdd6bb762281', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:59:15'),
('d85190d81edfd4ebd4ecd38f8d', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 03:39:54'),
('d875f3fd046c7c6af71b74b52d', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-07 03:55:49'),
('d9184a8f6334a721f8af973f50', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 03:59:40'),
('d92f77aa9d3ce49adf5049e562', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-24 16:32:07'),
('da21ad14251595ed3aec001cfd', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-25 11:14:17'),
('dacb9d0f8d7f60d34ef20f1a25', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 10:54:47'),
('db2a948ac2cc1c85f82c80db39', '{\"user_id\":\"1\",\"user_token\":\"9tETQWQvjLWp7SFl1u5Twpo86Q70DrU1\",\"language\":\"en-gb\",\"currency\":\"SAR\",\"account\":\"guest\",\"payment_address\":{\"firstname\":\"mhjhjh\",\"lastname\":\"kjhkhkjhjk\",\"company\":\"\",\"address_1\":\"sasads\",\"address_2\":\"\",\"postcode\":\"4324234\",\"city\":\"sdasdas\",\"country_id\":\"184\",\"zone_id\":\"2881\",\"country\":\"Saudi Arabia\",\"iso_code_2\":\"SA\",\"iso_code_3\":\"SAU\",\"address_format\":\"\",\"custom_field\":[],\"zone\":\"\'Asir\",\"zone_code\":\"AS\"},\"shipping_address\":{\"firstname\":\"mhjhjh\",\"lastname\":\"kjhkhkjhjk\",\"company\":\"\",\"address_1\":\"sasads\",\"address_2\":\"\",\"postcode\":\"4324234\",\"city\":\"sdasdas\",\"country_id\":\"184\",\"zone_id\":\"2881\",\"country\":\"Saudi Arabia\",\"iso_code_2\":\"SA\",\"iso_code_3\":\"SAU\",\"address_format\":\"\",\"zone\":\"\'Asir\",\"zone_code\":\"AS\",\"custom_field\":[]},\"in_cart_Notdropshipping\":\"1\"}', '2020-12-05 03:42:50'),
('db3c970f0ce511b344a8645d86', '{\"language\":\"ar\",\"currency\":\"SAR\",\"redirect\":\"https:\\/\\/lanaapp.space\\/isa_mohab\\/index.php?route=account\\/return\",\"compare\":[]}', '2020-12-05 04:00:39'),
('db5fc91cadd1f1268458f82380', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-12 15:22:32'),
('db9ed941d1fc6240a65740801e', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-29 14:59:25'),
('dbcdc51fd202a4b87f9cb18fa3', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-10 03:55:05'),
('dbf94e18a3a2768596f675e7e1', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-27 14:19:46'),
('dca640bc78f27f51b12b1f4a64', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-23 20:56:30'),
('dcd5dcd2b6ab889b3636a924e1', '{\"api_id\":\"5\"}', '2020-11-29 18:36:06'),
('dd5ba695aacda5b4c28a2d5b68', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 00:12:00'),
('ddcbd792ca6a57207339bad4bb', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:42:51'),
('ddf7a9d8c950aaa59af5ba9fcb', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-02 04:13:27'),
('deeeb303a4fd8b080555c72b7c', '{\"api_id\":\"5\"}', '2020-11-29 17:02:44'),
('df52c3c291f9af514d4a9ecda3', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-27 03:59:46'),
('df6613b9cfd0e1381f742b0774', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 21:45:14'),
('dfc6c5c45457a0f8fdf7e0d8f9', '{\"api_id\":\"5\"}', '2020-12-10 19:19:20'),
('dff154a6385a3ad37c646d4b98', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 05:51:16'),
('e0ca0a55986e78de7592670d6c', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 04:02:18'),
('e103062aa88914a6368b38d60f', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 14:10:05'),
('e17b90a7101e4686f2f0a8d8f2', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 03:56:00'),
('e228ed3010759abb51d5957d7a', '{\"api_id\":\"5\"}', '2020-11-29 19:34:39'),
('e2c279f49c84fd541dbb55bfe0', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 15:07:46'),
('e32e7ed5e181034916ac700c63', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 08:19:16'),
('e32f8d3e4b057f77408d8816f7', 'false', '2020-11-21 03:56:20'),
('e331e340b7af8b9bb8911336b7', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 03:57:06'),
('e3a4b7785e694dc488ed318c55', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 03:46:54'),
('e3fb66fe01e86dfcd23a2fa9b1', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 13:21:32'),
('e42222a7e12a398d8e62b4862e', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 04:39:45'),
('e442c4bb9653656b2a858436f6', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-12 15:16:08'),
('e501c841ba141f04a7c3c9adf9', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 03:56:43'),
('e50432d367de6e5d2b705d1ba0', 'false', '2020-11-21 03:56:27'),
('e605322a36a68c4e04e76e503a', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-08 04:02:11'),
('e63404ca7dc61073028cdd4403', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 19:42:39'),
('e651b593248ea5b358b99ebde4', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-25 21:29:56'),
('e685758e1b2a020a931fec14ce', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:42:20'),
('e69cf065a935e8db4bbb913f7a', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 00:41:56'),
('e742f8775a7ec1b1ad62a264aa', '{\"language\":\"en-gb\",\"currency\":\"SAR\",\"user_id\":\"1\",\"user_token\":\"1inlX9mCYuajSkftx6EfsjCTmzDOjDVG\"}', '2020-12-06 11:02:24'),
('e7f1ce2b987a04960fe5651ee7', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 23:08:32'),
('e7fb111f9d032b8e0c8298a7cb', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 03:57:13'),
('e800992ca49dcaf4e41d5651d0', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 11:28:35'),
('e83ab822382c8928166bf76740', '{\"api_id\":\"5\"}', '2020-11-29 16:40:20'),
('e886e2171360bc5446f971dada', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-11 21:11:41'),
('e889c64f82b721957f17d024cc', '{\"api_id\":\"6\"}', '2020-11-29 18:06:15'),
('e962ff351c75dd665aadcfd309', '{\"api_id\":\"6\"}', '2020-11-29 18:12:43'),
('e99dcb9f8c01f7585b837bb5ac', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 07:54:13'),
('e9a57c22f446101dbfea4aea92', '{\"user_id\":\"1\",\"user_token\":\"662bjKYBPg1XKa4rpl4YB67m7jhZ7os3\"}', '2020-12-12 10:32:40'),
('e9b7f987cae24baaf238f877b2', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-09 03:58:19'),
('ea1f1752d05fbf7d9cb7a34ff4', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 03:55:56'),
('ea1f6c093c857a0ff555f86922', '{\"language\":\"ar\",\"currency\":\"SAR\",\"account\":\"guest\",\"guest\":{\"customer_group_id\":\"1\",\"firstname\":\"vsdgdsgd\",\"lastname\":\"dfgdfg\",\"email\":\"sarsr@asdasd.com\",\"telephone\":\"42342342\",\"custom_field\":[],\"shipping_address\":\"1\"},\"payment_address\":{\"firstname\":\"vsdgdsgd\",\"lastname\":\"dfgdfg\",\"company\":\"\",\"address_1\":\"asdasda\",\"address_2\":\"\",\"postcode\":\"\",\"city\":\"asdsa\",\"country_id\":\"174\",\"zone_id\":\"0\",\"country\":\"Reunion\",\"iso_code_2\":\"RE\",\"iso_code_3\":\"REU\",\"address_format\":\"\",\"custom_field\":[],\"zone\":\"\",\"zone_code\":\"\"},\"shipping_address\":{\"firstname\":\"vsdgdsgd\",\"lastname\":\"dfgdfg\",\"company\":\"\",\"address_1\":\"asdasda\",\"address_2\":\"\",\"postcode\":\"\",\"city\":\"asdsa\",\"country_id\":\"174\",\"zone_id\":\"0\",\"country\":\"Reunion\",\"iso_code_2\":\"RE\",\"iso_code_3\":\"REU\",\"address_format\":\"\",\"zone\":\"\",\"zone_code\":\"\",\"custom_field\":[]},\"shipping_methods\":[]}', '2020-12-12 16:05:06'),
('ea42353cc154f973bc64f2bee7', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-08 04:02:29'),
('ea43e688876fd9de1e8781c347', '{\"language\":\"ar\",\"currency\":\"SAR\",\"redirect\":\"https:\\/\\/lanaapp.space\\/isa_mohab2\\/index.php?route=account\\/order\"}', '2020-12-10 03:55:16'),
('ea9fb11d1f552516f4c93b57d2', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-07 03:55:51'),
('eae8ab3901625bad64c56ea626', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 04:01:39'),
('eaec64f795c0996c5aa82a7448', '{\"api_id\":\"5\"}', '2020-11-29 16:55:19'),
('eaecf866892981b369c35fede9', '{\"api_id\":\"5\"}', '2020-12-05 00:49:37'),
('eafc3866f6313c929886689967', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2020-11-20 19:28:05'),
('eb8ea3f9218bafd17f1134a4fd', '{\"api_id\":\"5\"}', '2020-11-29 17:01:20'),
('ebba51abcb0a00bfe630aa3838', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:43:08'),
('ec329d722ea86253f54092db18', '{\"api_id\":\"5\"}', '2020-12-11 21:11:35'),
('ec55781a560b4fc67028536d95', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 04:03:14'),
('ec62e5c272150a80111db07aad', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:42:31'),
('ec96be3aef5e22426773397898', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 04:04:06'),
('ecb643ba4f4dfb8977feb8f593', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 08:31:54'),
('ecf6c5cdb8e102a79664ca0596', '{\"api_id\":\"6\"}', '2020-11-29 18:09:55'),
('ecfc7b0698cd798f985cb2bb70', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-07 03:55:52'),
('ed83f5f861c96af03576a2cd3e', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 05:34:48'),
('ede779c74fecee58cecab2bc06', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 11:28:39'),
('ee11e25360b93d478ded51ce50', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-07 03:55:46'),
('ee130536a8398099256e9a36d5', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-27 03:01:13'),
('eec15c71b9070c3aa81b4dc84a', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:43:03'),
('ef33dce801ff246580534c510f', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-09-17 07:27:32'),
('ef59d0ad0bd12c58bd21b21be2', 'false', '2020-11-21 03:56:23'),
('efdce04eb9787ea6e4d680c9b8', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:35:45'),
('efe9b7ae08f381cce990d6eb8f', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-26 04:04:13'),
('f00ba46699b18846c8372a389e', '{\"api_id\":\"5\"}', '2020-11-30 16:00:14'),
('f0278ac09d9efc4da77d25b63c', '{\"language\":\"ar\",\"currency\":\"SAR\",\"redirect\":\"https:\\/\\/lanaapp.space\\/isa_mohab2\\/index.php?route=account\\/order\"}', '2020-12-07 03:55:45'),
('f0de5a2d5537ec0cdd125a9cec', '{\"api_id\":\"5\"}', '2020-11-29 17:19:21'),
('f115a42e7db15abe1fba702479', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-03 11:28:33'),
('f19f6bd34fd6cff17196d2f33c', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2020-11-20 19:28:46'),
('f254744db3ea6fe318b8366911', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:42:09'),
('f2cf1efc6a2e69598070f96e32', '{\"api_id\":\"5\"}', '2020-11-29 17:46:53'),
('f302fbbade16ce740697626890', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 15:07:47'),
('f323d49ccd82991a07773be18f', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 03:37:51'),
('f37507a2939af6674e322b743b', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-26 04:04:36'),
('f3a3b74ac186b186ea9c3c3061', '{\"api_id\":\"5\"}', '2020-11-29 18:33:35'),
('f3f78534715d4552d9504f10b0', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 10:54:41'),
('f47dfe02c8d335e97e8a505a90', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:42:25'),
('f4ffd780afc9ba60c44757c7a7', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 12:48:58'),
('f507e4e384896b43e5b3720cb8', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 08:20:13'),
('f5d02015e051d415ca94240bee', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 02:06:34'),
('f696319595a32e6761318ab8da', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 04:02:17');
INSERT INTO `oc_session` (`session_id`, `data`, `expire`) VALUES
('f6d6b5392fcc5482a353461eae', '{\"api_id\":\"5\"}', '2020-12-10 19:23:36'),
('f6f69b95d0ad1b36477f4d7f1f', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 19:35:53'),
('f707e0d9ff7099ee05f2c61c42', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 08:31:08'),
('f70c0eb9f5f6e92727fa00c9f7', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:41:56'),
('f7344c4669112cd7fa640e168a', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-30 05:08:01'),
('f7c27174029fb78d60b7d38298', '{\"api_id\":\"5\"}', '2020-12-10 19:22:09'),
('f7c903629766724e0cb2b5aa5b', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-25 14:06:04'),
('f7d574bc92be0ff9c9b6aa4678', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 04:04:18'),
('f82340fe215114d2d6aef92a6c', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"l2mCHyaVCxXt7ryIZPTlajeXpVriMuTS\"}', '2017-09-14 14:06:55'),
('f877338d4a0c345fa151caec11', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 03:57:51'),
('f8a050d2fb7fdf7a873abfa773', '{\"api_id\":\"5\"}', '2020-11-29 19:19:14'),
('f8eaab8df47b6255fb21ad612e', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 21:17:39'),
('fa04b55d519469506b0bc16243', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-12-03 02:21:16'),
('fa0e5ac4d4eea877589f59e571', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 15:07:44'),
('fa74eebdf4c8d48751e9344db6', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-28 15:24:00'),
('fa9b791cc4074863df6cedad72', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:41:46'),
('fad02cf43454b936fa7bc49c07', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-25 14:03:25'),
('faf710ca86a43b4370d137a80b', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-01 04:05:14'),
('fb7f7f143d280f092ff36fdce2', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 18:40:39'),
('fbbfe2bc49bfdc8d14d9bec7b1', '{\"api_id\":\"5\"}', '2020-11-29 17:46:57'),
('fbec5fe8b1e8e1cacc010833f8', '{\"language\":\"ar\",\"currency\":\"SAR\",\"redirect\":\"https:\\/\\/lanaapp.space\\/isa_mohab\\/index.php?route=account\\/wishlist\"}', '2020-12-01 04:05:48'),
('fc573f11a3ecee17802dd76e74', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 16:54:13'),
('fc762a53473334fb091b36ca99', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-25 16:24:39'),
('fcd7a484b8bbc9f68f8b0724b5', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 20:43:50'),
('fce71122a652e25433aa43f099', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-24 19:57:41'),
('fceaa56a6e35e547d4d76a1728', '{\"api_id\":\"5\",\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-05 00:35:57'),
('fcec776e9233b6e54393be3d3d', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 14:43:19'),
('fda38f3acc7a3d1db58fa83bd1', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-23 23:29:12'),
('fe3b14dd2f847e3c3116be89b5', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-24 12:11:12'),
('fe49010ce85d23b53401ef66fa', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-11-29 02:06:45'),
('fea34940c8e9f49a54b1a5c401', '{\"api_id\":\"5\"}', '2020-12-11 22:04:21'),
('fefd515add7acd0980649e86dc', '{\"language\":\"ar\",\"currency\":\"USD\"}', '2020-11-26 15:28:26'),
('ff863c160276acfe3b2a65ed3c', '{\"language\":\"ar\",\"currency\":\"SAR\"}', '2020-12-02 03:59:21'),
('ffbae10844af7dfd56c44a9b7c', '{\"api_id\":\"5\",\"language\":\"en-gb\",\"currency\":\"USD\"}', '2020-11-29 16:54:58'),
('ffd650633caa84bb4ddaae1e1e', '{\"language\":\"en-gb\",\"currency\":\"SAR\"}', '2020-11-27 13:57:20');

-- --------------------------------------------------------

--
-- Table structure for table `oc_setting`
--

CREATE TABLE `oc_setting` (
  `setting_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(128) NOT NULL,
  `key` varchar(128) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_setting`
--

INSERT INTO `oc_setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES
(4, 0, 'voucher', 'total_voucher_sort_order', '8', 0),
(5, 0, 'voucher', 'total_voucher_status', '1', 0),
(6993, 0, 'module_divido_calculator', 'module_divido_calculator_status', '1', 0),
(7328, 0, 'config', 'config_error_display', '0', 0),
(7329, 0, 'config', 'config_error_log', '1', 0),
(7330, 0, 'config', 'config_error_filename', 'error.log', 0),
(7331, 0, 'config', 'config_socialmedia_facebook', '#', 0),
(7327, 0, 'config', 'config_file_mime_allowed', 'text/plain\r\nimage/png\r\nimage/jpeg\r\nimage/gif\r\nimage/bmp\r\nimage/tiff\r\nimage/svg+xml\r\napplication/zip\r\n&quot;application/zip&quot;\r\napplication/x-zip\r\n&quot;application/x-zip&quot;\r\napplication/x-zip-compressed\r\n&quot;application/x-zip-compressed&quot;\r\napplication/rar\r\n&quot;application/rar&quot;\r\napplication/x-rar\r\n&quot;application/x-rar&quot;\r\napplication/x-rar-compressed\r\n&quot;application/x-rar-compressed&quot;\r\napplication/octet-stream\r\n&quot;application/octet-stream&quot;\r\naudio/mpeg\r\nvideo/quicktime\r\napplication/pdf', 0),
(7324, 0, 'config', 'config_encryption', 'qqPQ7unwkfiLAeJqAL7pcyYRn0CoQ6KNuyDqUlEyb6aghg2gf5Z4O2eP0jexd0bWouBQlV8BNJJG5bUO3V2Ct0gukE1cOxko4hlwKXYGb1YonzJNLAN6dyX4vxBS4ZQdCiooj8GNbV9ubcSFHCPK1B7T60VRu5yzr24a9qe1QkTq7pnePyN8LUt9Qlo0Ufin3T88SHodNNJgExPLvctbczHC1FEHU6Iv6gONW6luhuxuP3mnu3pxgXDayhlLlhq9YnwFWRPqJ16CqOejWKoiwr3x69M87faPmqGaOHmAFJ99TPXX1t6ewWJE15Thuk7tAZV2IemhOXFFPiW5fB2GJPiiPkysdupvrmNF3FFRn9bRRyuD2AYvbGb0CdUA0wjyV19j2vjrOgswWYkzuk60cUznHrfhiDUXkfaUYvqws35kfY31skACZ8CmUFmkJkPKzH7F1HOUnNu8UQX3PBoUDA4QL26H2OdsoDTSFYy8jBbFTBmekAbd0hf2TghdGCeIVaXEKQKYU9SmGykkNHvv9rUJATxg6UXnTD9TT3cMvoBcy00set7C0bCD93Byr1uVUWvq1vhLaxsCoGWMPVFy3Xu7LX3hu6ncweKypOsfxb5Q4xWkp0dbpBCK4AP60B2m5UEOsnIxiOfB4N4N4mbCIJAcYCtQULxOfxkm0ou4lQAhQskick1EchIw14l22uQFEbqvbmtTqnvdDAh0CasehaRmUnMaT6wnuCLDMW87jWmtkLE1oFwvL1Cy7NnwtHiupv0nN9r3lbMlOHcFx5X9R5j2aen2DIXZ1JnWyuGKS7vNXzyPlQrKAE68m1yhGQz83bSi0B8kFuXb0cLYomEnMaQXPRWxAghA3LSTbCMIdcqvkEIFZVYuKCyroCPz5pFr8k5vSFj0pOvRGkFcM3ZidD39USwnWhfSba1mTXZJeA25CD0G1C94BUNMiLG2yMNP1o7nfL3F1BqOeMZYipaAzwZAwdyFklp4hjC8RHHRlO5QSwhpoPcyEYkmBUUI25Cu', 0),
(7325, 0, 'config', 'config_file_max_size', '300000', 0),
(7326, 0, 'config', 'config_file_ext_allowed', 'zip\r\ntxt\r\npng\r\njpe\r\njpeg\r\njpg\r\ngif\r\nbmp\r\nico\r\ntiff\r\ntif\r\nsvg\r\nsvgz\r\nzip\r\nrar\r\nmsi\r\ncab\r\nmp3\r\nqt\r\nmov\r\npdf\r\npsd\r\nai\r\neps\r\nps\r\ndoc', 0),
(7323, 0, 'config', 'config_shared', '0', 0),
(7320, 0, 'config', 'config_compression', '0', 0),
(7321, 0, 'config', 'config_secure', '1', 0),
(7322, 0, 'config', 'config_password', '1', 0),
(7307, 0, 'config', 'config_store_color_theme', '#6c3c7a', 0),
(7308, 0, 'config', 'config_mail_engine', 'mail', 0),
(7309, 0, 'config', 'config_mail_parameter', '', 0),
(7310, 0, 'config', 'config_mail_smtp_hostname', '', 0),
(7311, 0, 'config', 'config_mail_smtp_username', '', 0),
(7312, 0, 'config', 'config_mail_smtp_password', '', 0),
(7313, 0, 'config', 'config_mail_smtp_port', '25', 0),
(7314, 0, 'config', 'config_mail_smtp_timeout', '5', 0),
(7315, 0, 'config', 'config_mail_alert', '[\"order\"]', 1),
(7316, 0, 'config', 'config_mail_alert_email', '', 0),
(7317, 0, 'config', 'config_maintenance', '0', 0),
(7318, 0, 'config', 'config_seo_url', '1', 0),
(7319, 0, 'config', 'config_robots', 'abot\r\ndbot\r\nebot\r\nhbot\r\nkbot\r\nlbot\r\nmbot\r\nnbot\r\nobot\r\npbot\r\nrbot\r\nsbot\r\ntbot\r\nvbot\r\nybot\r\nzbot\r\nbot.\r\nbot/\r\n_bot\r\n.bot\r\n/bot\r\n-bot\r\n:bot\r\n(bot\r\ncrawl\r\nslurp\r\nspider\r\nseek\r\naccoona\r\nacoon\r\nadressendeutschland\r\nah-ha.com\r\nahoy\r\naltavista\r\nananzi\r\nanthill\r\nappie\r\narachnophilia\r\narale\r\naraneo\r\naranha\r\narchitext\r\naretha\r\narks\r\nasterias\r\natlocal\r\natn\r\natomz\r\naugurfind\r\nbackrub\r\nbannana_bot\r\nbaypup\r\nbdfetch\r\nbig brother\r\nbiglotron\r\nbjaaland\r\nblackwidow\r\nblaiz\r\nblog\r\nblo.\r\nbloodhound\r\nboitho\r\nbooch\r\nbradley\r\nbutterfly\r\ncalif\r\ncassandra\r\nccubee\r\ncfetch\r\ncharlotte\r\nchurl\r\ncienciaficcion\r\ncmc\r\ncollective\r\ncomagent\r\ncombine\r\ncomputingsite\r\ncsci\r\ncurl\r\ncusco\r\ndaumoa\r\ndeepindex\r\ndelorie\r\ndepspid\r\ndeweb\r\ndie blinde kuh\r\ndigger\r\nditto\r\ndmoz\r\ndocomo\r\ndownload express\r\ndtaagent\r\ndwcp\r\nebiness\r\nebingbong\r\ne-collector\r\nejupiter\r\nemacs-w3 search engine\r\nesther\r\nevliya celebi\r\nezresult\r\nfalcon\r\nfelix ide\r\nferret\r\nfetchrover\r\nfido\r\nfindlinks\r\nfireball\r\nfish search\r\nfouineur\r\nfunnelweb\r\ngazz\r\ngcreep\r\ngenieknows\r\ngetterroboplus\r\ngeturl\r\nglx\r\ngoforit\r\ngolem\r\ngrabber\r\ngrapnel\r\ngralon\r\ngriffon\r\ngromit\r\ngrub\r\ngulliver\r\nhamahakki\r\nharvest\r\nhavindex\r\nhelix\r\nheritrix\r\nhku www octopus\r\nhomerweb\r\nhtdig\r\nhtml index\r\nhtml_analyzer\r\nhtmlgobble\r\nhubater\r\nhyper-decontextualizer\r\nia_archiver\r\nibm_planetwide\r\nichiro\r\niconsurf\r\niltrovatore\r\nimage.kapsi.net\r\nimagelock\r\nincywincy\r\nindexer\r\ninfobee\r\ninformant\r\ningrid\r\ninktomisearch.com\r\ninspector web\r\nintelliagent\r\ninternet shinchakubin\r\nip3000\r\niron33\r\nisraeli-search\r\nivia\r\njack\r\njakarta\r\njavabee\r\njetbot\r\njumpstation\r\nkatipo\r\nkdd-explorer\r\nkilroy\r\nknowledge\r\nkototoi\r\nkretrieve\r\nlabelgrabber\r\nlachesis\r\nlarbin\r\nlegs\r\nlibwww\r\nlinkalarm\r\nlink validator\r\nlinkscan\r\nlockon\r\nlwp\r\nlycos\r\nmagpie\r\nmantraagent\r\nmapoftheinternet\r\nmarvin/\r\nmattie\r\nmediafox\r\nmediapartners\r\nmercator\r\nmerzscope\r\nmicrosoft url control\r\nminirank\r\nmiva\r\nmj12\r\nmnogosearch\r\nmoget\r\nmonster\r\nmoose\r\nmotor\r\nmultitext\r\nmuncher\r\nmuscatferret\r\nmwd.search\r\nmyweb\r\nnajdi\r\nnameprotect\r\nnationaldirectory\r\nnazilla\r\nncsa beta\r\nnec-meshexplorer\r\nnederland.zoek\r\nnetcarta webmap engine\r\nnetmechanic\r\nnetresearchserver\r\nnetscoop\r\nnewscan-online\r\nnhse\r\nnokia6682/\r\nnomad\r\nnoyona\r\nnutch\r\nnzexplorer\r\nobjectssearch\r\noccam\r\nomni\r\nopen text\r\nopenfind\r\nopenintelligencedata\r\norb search\r\nosis-project\r\npack rat\r\npageboy\r\npagebull\r\npage_verifier\r\npanscient\r\nparasite\r\npartnersite\r\npatric\r\npear.\r\npegasus\r\nperegrinator\r\npgp key agent\r\nphantom\r\nphpdig\r\npicosearch\r\npiltdownman\r\npimptrain\r\npinpoint\r\npioneer\r\npiranha\r\nplumtreewebaccessor\r\npogodak\r\npoirot\r\npompos\r\npoppelsdorf\r\npoppi\r\npopular iconoclast\r\npsycheclone\r\npublisher\r\npython\r\nrambler\r\nraven search\r\nroach\r\nroad runner\r\nroadhouse\r\nrobbie\r\nrobofox\r\nrobozilla\r\nrules\r\nsalty\r\nsbider\r\nscooter\r\nscoutjet\r\nscrubby\r\nsearch.\r\nsearchprocess\r\nsemanticdiscovery\r\nsenrigan\r\nsg-scout\r\nshai\'hulud\r\nshark\r\nshopwiki\r\nsidewinder\r\nsift\r\nsilk\r\nsimmany\r\nsite searcher\r\nsite valet\r\nsitetech-rover\r\nskymob.com\r\nsleek\r\nsmartwit\r\nsna-\r\nsnappy\r\nsnooper\r\nsohu\r\nspeedfind\r\nsphere\r\nsphider\r\nspinner\r\nspyder\r\nsteeler/\r\nsuke\r\nsuntek\r\nsupersnooper\r\nsurfnomore\r\nsven\r\nsygol\r\nszukacz\r\ntach black widow\r\ntarantula\r\ntempleton\r\n/teoma\r\nt-h-u-n-d-e-r-s-t-o-n-e\r\ntheophrastus\r\ntitan\r\ntitin\r\ntkwww\r\ntoutatis\r\nt-rex\r\ntutorgig\r\ntwiceler\r\ntwisted\r\nucsd\r\nudmsearch\r\nurl check\r\nupdated\r\nvagabondo\r\nvalkyrie\r\nverticrawl\r\nvictoria\r\nvision-search\r\nvolcano\r\nvoyager/\r\nvoyager-hc\r\nw3c_validator\r\nw3m2\r\nw3mir\r\nwalker\r\nwallpaper\r\nwanderer\r\nwauuu\r\nwavefire\r\nweb core\r\nweb hopper\r\nweb wombat\r\nwebbandit\r\nwebcatcher\r\nwebcopy\r\nwebfoot\r\nweblayers\r\nweblinker\r\nweblog monitor\r\nwebmirror\r\nwebmonkey\r\nwebquest\r\nwebreaper\r\nwebsitepulse\r\nwebsnarf\r\nwebstolperer\r\nwebvac\r\nwebwalk\r\nwebwatch\r\nwebwombat\r\nwebzinger\r\nwhizbang\r\nwhowhere\r\nwild ferret\r\nworldlight\r\nwwwc\r\nwwwster\r\nxenu\r\nxget\r\nxift\r\nxirq\r\nyandex\r\nyanga\r\nyeti\r\nyodao\r\nzao\r\nzippp\r\nzyborg', 0),
(95, 0, 'payment_free_checkout', 'payment_free_checkout_status', '1', 0),
(96, 0, 'payment_free_checkout', 'free_checkout_order_status_id', '1', 0),
(97, 0, 'payment_free_checkout', 'payment_free_checkout_sort_order', '1', 0),
(315, 0, 'payment_cod', 'payment_cod_sort_order', '1', 0),
(313, 0, 'payment_cod', 'payment_cod_geo_zone_id', '0', 0),
(314, 0, 'payment_cod', 'payment_cod_status', '1', 0),
(7214, 0, 'shipping_flat', 'shipping_flat_sort_order', '1', 0),
(7213, 0, 'shipping_flat', 'shipping_flat_status', '1', 0),
(7212, 0, 'shipping_flat', 'shipping_flat_geo_zone_id', '0', 0),
(7211, 0, 'shipping_flat', 'shipping_flat_tax_class_id', '9', 0),
(7210, 0, 'shipping_flat', 'shipping_flat_cost', '5.00', 0),
(108, 0, 'total_shipping', 'total_shipping_sort_order', '3', 0),
(109, 0, 'total_sub_total', 'sub_total_sort_order', '1', 0),
(110, 0, 'total_sub_total', 'total_sub_total_status', '1', 0),
(111, 0, 'total_tax', 'total_tax_status', '1', 0),
(112, 0, 'total_total', 'total_total_sort_order', '9', 0),
(113, 0, 'total_total', 'total_total_status', '1', 0),
(114, 0, 'total_tax', 'total_tax_sort_order', '5', 0),
(115, 0, 'total_credit', 'total_credit_sort_order', '7', 0),
(116, 0, 'total_credit', 'total_credit_status', '1', 0),
(117, 0, 'total_reward', 'total_reward_sort_order', '2', 0),
(118, 0, 'total_reward', 'total_reward_status', '1', 0),
(119, 0, 'total_shipping', 'total_shipping_status', '1', 0),
(120, 0, 'total_shipping', 'total_shipping_estimator', '1', 0),
(121, 0, 'total_coupon', 'total_coupon_sort_order', '4', 0),
(122, 0, 'total_coupon', 'total_coupon_status', '1', 0),
(6895, 0, 'module_category', 'module_category_status', '1', 0),
(124, 0, 'module_account', 'module_account_status', '1', 0),
(944, 0, 'module_filter', 'module_filter_status', '1', 0),
(2516, 0, 'theme_default', 'theme_default_image_location_height', '40', 0),
(2515, 0, 'theme_default', 'theme_default_image_location_width', '188', 0),
(2514, 0, 'theme_default', 'theme_default_image_cart_height', '50', 0),
(2513, 0, 'theme_default', 'theme_default_image_cart_width', '50', 0),
(2512, 0, 'theme_default', 'theme_default_image_wishlist_height', '60', 0),
(2511, 0, 'theme_default', 'theme_default_image_wishlist_width', '60', 0),
(2510, 0, 'theme_default', 'theme_default_image_compare_height', '90', 0),
(2509, 0, 'theme_default', 'theme_default_image_compare_width', '90', 0),
(2508, 0, 'theme_default', 'theme_default_image_related_height', '354', 0),
(2507, 0, 'theme_default', 'theme_default_image_related_width', '270', 0),
(2506, 0, 'theme_default', 'theme_default_image_additional_height', '190', 0),
(2505, 0, 'theme_default', 'theme_default_image_additional_width', '150', 0),
(2504, 0, 'theme_default', 'theme_default_image_product_height', '354', 0),
(2503, 0, 'theme_default', 'theme_default_image_product_width', '270', 0),
(2502, 0, 'theme_default', 'theme_default_image_popup_height', '870', 0),
(2501, 0, 'theme_default', 'theme_default_image_popup_width', '654', 0),
(2500, 0, 'theme_default', 'theme_default_image_thumb_height', '870', 0),
(2499, 0, 'theme_default', 'theme_default_image_thumb_width', '654', 0),
(149, 0, 'dashboard_activity', 'dashboard_activity_status', '1', 0),
(150, 0, 'dashboard_activity', 'dashboard_activity_sort_order', '7', 0),
(151, 0, 'dashboard_sale', 'dashboard_sale_status', '1', 0),
(152, 0, 'dashboard_sale', 'dashboard_sale_width', '3', 0),
(153, 0, 'dashboard_chart', 'dashboard_chart_status', '1', 0),
(154, 0, 'dashboard_chart', 'dashboard_chart_width', '6', 0),
(155, 0, 'dashboard_customer', 'dashboard_customer_status', '1', 0),
(156, 0, 'dashboard_customer', 'dashboard_customer_width', '3', 0),
(157, 0, 'dashboard_map', 'dashboard_map_status', '1', 0),
(158, 0, 'dashboard_map', 'dashboard_map_width', '6', 0),
(159, 0, 'dashboard_online', 'dashboard_online_status', '1', 0),
(160, 0, 'dashboard_online', 'dashboard_online_width', '3', 0),
(161, 0, 'dashboard_order', 'dashboard_order_sort_order', '1', 0),
(162, 0, 'dashboard_order', 'dashboard_order_status', '1', 0),
(163, 0, 'dashboard_order', 'dashboard_order_width', '3', 0),
(164, 0, 'dashboard_sale', 'dashboard_sale_sort_order', '2', 0),
(165, 0, 'dashboard_customer', 'dashboard_customer_sort_order', '3', 0),
(166, 0, 'dashboard_online', 'dashboard_online_sort_order', '4', 0),
(167, 0, 'dashboard_map', 'dashboard_map_sort_order', '5', 0),
(168, 0, 'dashboard_chart', 'dashboard_chart_sort_order', '6', 0),
(169, 0, 'dashboard_recent', 'dashboard_recent_status', '1', 0),
(170, 0, 'dashboard_recent', 'dashboard_recent_sort_order', '8', 0),
(171, 0, 'dashboard_activity', 'dashboard_activity_width', '4', 0),
(172, 0, 'dashboard_recent', 'dashboard_recent_width', '8', 0),
(173, 0, 'report_customer_activity', 'report_customer_activity_status', '1', 0),
(174, 0, 'report_customer_activity', 'report_customer_activity_sort_order', '1', 0),
(175, 0, 'report_customer_order', 'report_customer_order_status', '1', 0),
(176, 0, 'report_customer_order', 'report_customer_order_sort_order', '2', 0),
(177, 0, 'report_customer_reward', 'report_customer_reward_status', '1', 0),
(178, 0, 'report_customer_reward', 'report_customer_reward_sort_order', '3', 0),
(179, 0, 'report_customer_search', 'report_customer_search_sort_order', '3', 0),
(180, 0, 'report_customer_search', 'report_customer_search_status', '1', 0),
(181, 0, 'report_customer_transaction', 'report_customer_transaction_status', '1', 0),
(182, 0, 'report_customer_transaction', 'report_customer_transaction_status_sort_order', '4', 0),
(183, 0, 'report_sale_tax', 'report_sale_tax_status', '1', 0),
(184, 0, 'report_sale_tax', 'report_sale_tax_sort_order', '5', 0),
(185, 0, 'report_sale_shipping', 'report_sale_shipping_status', '1', 0),
(186, 0, 'report_sale_shipping', 'report_sale_shipping_sort_order', '6', 0),
(187, 0, 'report_sale_return', 'report_sale_return_status', '1', 0),
(188, 0, 'report_sale_return', 'report_sale_return_sort_order', '7', 0),
(189, 0, 'report_sale_order', 'report_sale_order_status', '1', 0),
(190, 0, 'report_sale_order', 'report_sale_order_sort_order', '8', 0),
(191, 0, 'report_sale_coupon', 'report_sale_coupon_status', '1', 0),
(192, 0, 'report_sale_coupon', 'report_sale_coupon_sort_order', '9', 0),
(193, 0, 'report_product_viewed', 'report_product_viewed_status', '1', 0),
(194, 0, 'report_product_viewed', 'report_product_viewed_sort_order', '10', 0),
(195, 0, 'report_product_purchased', 'report_product_purchased_status', '1', 0),
(196, 0, 'report_product_purchased', 'report_product_purchased_sort_order', '11', 0),
(197, 0, 'report_marketing', 'report_marketing_status', '1', 0),
(198, 0, 'report_marketing', 'report_marketing_sort_order', '12', 0),
(207, 0, 'developer', 'developer_sass', '0', 0),
(206, 0, 'developer', 'developer_theme', '0', 0),
(3395, 0, 'payment_paylink', 'payment_paylink_sort_order', '1', 0),
(3394, 0, 'payment_paylink', 'payment_paylink_status', '1', 0),
(3393, 0, 'payment_paylink', 'payment_paylink_geo_zone_id', '0', 0),
(3392, 0, 'payment_paylink', 'payment_paylink_order_status_id', '2', 0),
(3391, 0, 'payment_paylink', 'payment_paylink_total', '1', 0),
(3390, 0, 'payment_paylink', 'payment_paylink_secretKey', 'b520d0b3-ded7-3f07-a038-42bc08643df5', 0),
(3389, 0, 'payment_paylink', 'payment_paylink_apiId', 'APP_ID_1603819975', 0),
(312, 0, 'payment_cod', 'payment_cod_order_status_id', '1', 0),
(311, 0, 'payment_cod', 'payment_cod_total', '0.01', 0),
(7218, 0, 'shipping_free', 'shipping_free_sort_order', '1', 0),
(7217, 0, 'shipping_free', 'shipping_free_status', '1', 0),
(7216, 0, 'shipping_free', 'shipping_free_geo_zone_id', '0', 0),
(2498, 0, 'theme_default', 'theme_default_image_category_height', '80', 0),
(2497, 0, 'theme_default', 'theme_default_image_category_width', '80', 0),
(2496, 0, 'theme_default', 'theme_default_product_description_length', '100', 0),
(2495, 0, 'theme_default', 'theme_default_product_limit', '20', 0),
(2494, 0, 'theme_default', 'theme_default_status', '1', 0),
(2493, 0, 'theme_default', 'theme_default_directory', 'default', 0),
(6995, 0, 'module_store', 'module_store_status', '1', 0),
(6994, 0, 'module_store', 'module_store_admin', '1', 0),
(7306, 0, 'config', 'config_icon', 'catalog/8a62eb75c5895a42984b06913268377c0.42861500 1606958995.png', 0),
(7305, 0, 'config', 'config_logo', 'catalog/c2da2cda86db4e713e2fe3d0269d120f0.80750000 1606946064.png', 0),
(7304, 0, 'config', 'config_captcha_page', '[\"review\",\"return\",\"contact\"]', 1),
(7303, 0, 'config', 'config_captcha', '', 0),
(7300, 0, 'config', 'config_affiliate_id', '4', 0),
(7301, 0, 'config', 'config_return_id', '0', 0),
(7302, 0, 'config', 'config_return_status_id', '2', 0),
(7299, 0, 'config', 'config_affiliate_commission', '5', 0),
(7297, 0, 'config', 'config_affiliate_approval', '0', 0),
(7298, 0, 'config', 'config_affiliate_auto', '0', 0),
(7296, 0, 'config', 'config_affiliate_group_id', '1', 0),
(7295, 0, 'config', 'config_stock_checkout', '0', 0),
(7294, 0, 'config', 'config_stock_warning', '0', 0),
(7293, 0, 'config', 'config_stock_display', '0', 0),
(7292, 0, 'config', 'config_api_id', '5', 0),
(7291, 0, 'config', 'config_fraud_status_id', '7', 0),
(7290, 0, 'config', 'config_complete_status', '[\"3\",\"5\"]', 1),
(7289, 0, 'config', 'config_processing_status', '[\"13\",\"7\",\"3\",\"10\",\"2\",\"1\",\"5\",\"12\"]', 1),
(7288, 0, 'config', 'config_order_status_id', '1', 0),
(7287, 0, 'config', 'config_checkout_id', '5', 0),
(7286, 0, 'config', 'config_checkout_guest', '1', 0),
(7285, 0, 'config', 'config_cart_weight', '1', 0),
(7283, 0, 'config', 'config_account_id', '3', 0),
(7284, 0, 'config', 'config_invoice_prefix', 'INV-2020-00', 0),
(7282, 0, 'config', 'config_login_attempts', '5', 0),
(7281, 0, 'config', 'config_customer_price', '0', 0),
(7280, 0, 'config', 'config_customer_group_display', '[\"1\"]', 1),
(7279, 0, 'config', 'config_customer_group_id', '1', 0),
(7278, 0, 'config', 'config_customer_search', '0', 0),
(7277, 0, 'config', 'config_customer_activity', '0', 0),
(7276, 0, 'config', 'config_customer_online', '0', 0),
(7275, 0, 'config', 'config_tax_customer', 'shipping', 0),
(7274, 0, 'config', 'config_tax_default', 'shipping', 0),
(7273, 0, 'config', 'config_tax', '1', 0),
(7272, 0, 'config', 'config_voucher_max', '1000', 0),
(7271, 0, 'config', 'config_voucher_min', '1', 0),
(7270, 0, 'config', 'config_review_guest', '1', 0),
(2866, 0, 'lana_setting', 'config_store_package_name', 'باقة تجريبية', 0),
(7269, 0, 'config', 'config_review_status', '1', 0),
(7268, 0, 'config', 'config_limit_admin', '20', 0),
(2953, 0, 'lana_setting', 'config_store_package_id', '1', 0),
(2954, 0, 'lana_setting', 'config_store_expire', '2020-12-25', 0),
(2955, 0, 'lana_setting', 'config_lana_store_id', '41', 0),
(2956, 0, 'lana_setting', 'config_store_lanadropshipping', '0', 0),
(2957, 0, 'lana_setting', 'config_store_quickadmin', 'Vh7j', 0),
(2958, 0, 'lana_setting', 'config_goals_target', '6000', 0),
(7267, 0, 'config', 'config_product_count', '1', 0),
(7266, 0, 'config', 'config_weight_class_id', '1', 0),
(7265, 0, 'config', 'config_length_class_id', '1', 0),
(7264, 0, 'config', 'config_currency_auto', '1', 0),
(7263, 0, 'config', 'config_currency', 'SAR', 0),
(7262, 0, 'config', 'config_admin_language', 'ar', 0),
(7261, 0, 'config', 'config_language', 'ar', 0),
(7260, 0, 'config', 'config_zone_id', '2879', 0),
(7259, 0, 'config', 'config_country_id', '184', 0),
(7258, 0, 'config', 'config_comment', '', 0),
(7257, 0, 'config', 'config_open', '', 0),
(7215, 0, 'shipping_free', 'shipping_free_total', '0.1', 0),
(7198, 0, 'module_smsa', 'module_smsa_status', '1', 0),
(7209, 0, 'smsa', 'smsa_country', 'KSA', 0),
(7208, 0, 'smsa', 'smsa_postcode', '11009', 0),
(7207, 0, 'smsa', 'smsa_city', 'Riyad', 0),
(7206, 0, 'smsa', 'smsa_address_2', 'South West', 0),
(7205, 0, 'smsa', 'smsa_address_1', 'R-176 Al Arab', 0),
(7204, 0, 'smsa', 'smsa_telephone', '86745364786', 0),
(7202, 0, 'smsa', 'smsa_name', 'John Doe', 0),
(7256, 0, 'config', 'config_image', 'catalog/favicon.png', 0),
(7255, 0, 'config', 'config_fax', '', 0),
(7254, 0, 'config', 'config_telephone', '123456789', 0),
(7253, 0, 'config', 'config_email', 'admin@admin.com', 0),
(7252, 0, 'config', 'config_geocode', '', 0),
(7251, 0, 'config', 'config_address', 'الرياض, السعودية', 0),
(7250, 0, 'config', 'config_owner', 'LnaSpace', 0),
(7249, 0, 'config', 'config_name', 'LnaSpace', 0),
(7248, 0, 'config', 'config_meta_keyword', '', 0),
(7247, 0, 'config', 'config_meta_description', 'LnaSpace', 0),
(7246, 0, 'config', 'config_meta_title', 'LnaSpace', 0),
(7203, 0, 'smsa', 'smsa_contact', 'John Doe', 0),
(7201, 0, 'smsa', 'smsa_mode', '0', 0),
(7200, 0, 'smsa', 'smsa_passkey', 'Testing0', 0),
(7199, 0, 'smsa', 'smsa_status', '1', 0),
(7219, 0, 'mpmultivendor', 'mpmultivendor_status', '1', 0),
(7220, 0, 'mpmultivendor', 'mpmultivendor_applyseller_page', '0', 0),
(7221, 0, 'mpmultivendor', 'mpmultivendor_received_commission_status_id', '13', 0),
(7222, 0, 'mpmultivendor', 'mpmultivendor_restrict_orderstatus', '[\"2\"]', 1),
(7223, 0, 'mpmultivendor', 'mpmultivendor_commission_rate', '10', 0),
(7224, 0, 'mpmultivendor', 'mpmultivendor_autoapproved_seller', '0', 0),
(7225, 0, 'mpmultivendor', 'mpmultivendor_seller_changereview', '1', 0),
(7226, 0, 'mpmultivendor', 'mpmultivendor_seller_name', '1', 0),
(7227, 0, 'mpmultivendor', 'mpmultivendor_seller_email', '1', 0),
(7228, 0, 'mpmultivendor', 'mpmultivendor_seller_telephone', '1', 0),
(7229, 0, 'mpmultivendor', 'mpmultivendor_seller_address', '1', 0),
(7230, 0, 'mpmultivendor', 'mpmultivendor_seller_image', '1', 0),
(7231, 0, 'mpmultivendor', 'mpmultivendor_seller_list', '20', 0),
(7232, 0, 'mpmultivendor', 'mpmultivendor_store_list', '20', 0),
(7233, 0, 'mpmultivendor', 'mpmultivendor_store_list_product', '20', 0),
(7234, 0, 'mpmultivendor', 'mpmultivendor_store_list_review', '20', 0),
(7235, 0, 'mpmultivendor', 'mpmultivendor_store_logo_width', '245', 0),
(7236, 0, 'mpmultivendor', 'mpmultivendor_store_logo_height', '166', 0),
(7237, 0, 'mpmultivendor', 'mpmultivendor_main_banner_width', '845', 0),
(7238, 0, 'mpmultivendor', 'mpmultivendor_main_banner_height', '220', 0),
(7239, 0, 'mpmultivendor', 'mpmultivendor_profile_image_width', '80', 0),
(7240, 0, 'mpmultivendor', 'mpmultivendor_profile_image_height', '80', 0),
(7241, 0, 'mpmultivendor', 'mpmultivendor_main_banner_width_listing', '385', 0),
(7242, 0, 'mpmultivendor', 'mpmultivendor_main_banner_height_listing', '100', 0),
(7243, 0, 'mpmultivendor', 'mpmultivendor_profile_image_width_listing', '80', 0),
(7244, 0, 'mpmultivendor', 'mpmultivendor_profile_image_height_listing', '80', 0),
(7245, 0, 'module_mpseller_account', 'module_mpseller_account_status', '1', 0),
(7332, 0, 'config', 'config_socialmedia_instagram', '#', 0),
(7333, 0, 'config', 'config_socialmedia_twitter', '#', 0),
(7334, 0, 'config', 'config_socialmedia_whatsapp', '#', 0),
(7335, 0, 'config', 'config_socialmedia_telegram', '#', 0),
(7336, 0, 'config', 'config_socialmedia_snapchat', '#', 0),
(7337, 0, 'config', 'config_socialmedia_youtube', '#', 0),
(7338, 0, 'config', 'config_socialmedia_pinterest', '#', 0),
(7339, 0, 'config', 'config_socialmedia_vimeo', '#', 0),
(7340, 0, 'config', 'config_socialmedia_maroof', '#', 0),
(7341, 0, 'config', 'config_theme', 'default', 0),
(7342, 0, 'config', 'config_layout_id', '4', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_shipping_courier`
--

CREATE TABLE `oc_shipping_courier` (
  `shipping_courier_id` int(11) NOT NULL,
  `shipping_courier_code` varchar(255) NOT NULL DEFAULT '',
  `shipping_courier_name` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_shipping_courier`
--

INSERT INTO `oc_shipping_courier` (`shipping_courier_id`, `shipping_courier_code`, `shipping_courier_name`) VALUES
(1, 'dhl', 'DHL'),
(2, 'fedex', 'Fedex'),
(3, 'ups', 'UPS'),
(4, 'royal-mail', 'Royal Mail'),
(5, 'usps', 'United States Postal Service'),
(6, 'auspost', 'Australia Post'),
(7, 'citylink', 'Citylink');

-- --------------------------------------------------------

--
-- Table structure for table `oc_smsa`
--

CREATE TABLE `oc_smsa` (
  `consignment_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `awb_number` varchar(32) NOT NULL,
  `reference_number` varchar(32) NOT NULL,
  `pickup_date` datetime NOT NULL,
  `shipment_label` text,
  `status` varchar(32) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_statistics`
--

CREATE TABLE `oc_statistics` (
  `statistics_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `value` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_statistics`
--

INSERT INTO `oc_statistics` (`statistics_id`, `code`, `value`) VALUES
(1, 'order_sale', 40440.0000),
(2, 'order_processing', 0.0000),
(3, 'order_complete', 0.0000),
(4, 'order_other', 0.0000),
(5, 'returns', 0.0000),
(6, 'product', 0.0000),
(7, 'review', 2.0000);

-- --------------------------------------------------------

--
-- Table structure for table `oc_stock_status`
--

CREATE TABLE `oc_stock_status` (
  `stock_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_stock_status`
--

INSERT INTO `oc_stock_status` (`stock_status_id`, `language_id`, `name`) VALUES
(7, 2, 'متوفر'),
(8, 2, 'حسب الطلب'),
(5, 2, 'نفذ المخزون'),
(6, 2, 'من يوم إلى ثلاثة أيام'),
(7, 1, 'In Stock'),
(8, 1, 'Pre-Order'),
(5, 1, 'Out Of Stock'),
(6, 1, '2-3 Days');

-- --------------------------------------------------------

--
-- Table structure for table `oc_store`
--

CREATE TABLE `oc_store` (
  `store_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `url` varchar(255) NOT NULL,
  `ssl` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_success_orders_payments_tracking`
--

CREATE TABLE `oc_success_orders_payments_tracking` (
  `id` int(11) NOT NULL,
  `transaction_num` varchar(255) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `payment_company` varchar(255) NOT NULL,
  `response` text NOT NULL,
  `created_at` datetime NOT NULL,
  `lana_payed` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_class`
--

CREATE TABLE `oc_tax_class` (
  `tax_class_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_class`
--

INSERT INTO `oc_tax_class` (`tax_class_id`, `title`, `description`, `date_added`, `date_modified`) VALUES
(9, 'Taxable Goods', 'Taxed goods', '2009-01-06 23:21:53', '2011-09-23 14:07:50'),
(10, 'Downloadable Products', 'Downloadable', '2011-09-21 22:19:39', '2011-09-22 10:27:36');

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_rate`
--

CREATE TABLE `oc_tax_rate` (
  `tax_rate_id` int(11) NOT NULL,
  `geo_zone_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL,
  `rate` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `type` char(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_rate`
--

INSERT INTO `oc_tax_rate` (`tax_rate_id`, `geo_zone_id`, `name`, `rate`, `type`, `date_added`, `date_modified`) VALUES
(86, 3, 'VAT (20%)', 20.0000, 'P', '2011-03-09 21:17:10', '2011-09-22 22:24:29'),
(87, 3, 'Eco Tax (-2.00)', 2.0000, 'F', '2011-09-21 21:49:23', '2011-09-23 00:40:19');

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_rate_to_customer_group`
--

CREATE TABLE `oc_tax_rate_to_customer_group` (
  `tax_rate_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_rate_to_customer_group`
--

INSERT INTO `oc_tax_rate_to_customer_group` (`tax_rate_id`, `customer_group_id`) VALUES
(86, 1),
(87, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_rule`
--

CREATE TABLE `oc_tax_rule` (
  `tax_rule_id` int(11) NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `based` varchar(10) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_rule`
--

INSERT INTO `oc_tax_rule` (`tax_rule_id`, `tax_class_id`, `tax_rate_id`, `based`, `priority`) VALUES
(121, 10, 86, 'payment', 1),
(120, 10, 87, 'store', 0),
(128, 9, 86, 'shipping', 1),
(127, 9, 87, 'shipping', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oc_theme`
--

CREATE TABLE `oc_theme` (
  `theme_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `theme` varchar(64) NOT NULL,
  `route` varchar(64) NOT NULL,
  `code` mediumtext NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_translation`
--

CREATE TABLE `oc_translation` (
  `translation_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_upload`
--

CREATE TABLE `oc_upload` (
  `upload_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_upload`
--

INSERT INTO `oc_upload` (`upload_id`, `name`, `filename`, `code`, `date_added`) VALUES
(1, 'j.jpg', 'j.jpg.k0WQWQXSN34RGaV7M2C45O2iqVrOUlNg', '49245197d773b466666445202b5619ce1db14a54', '2020-11-29 17:22:02'),
(2, '794c45a6-d742-47b5-85e5-e177d5914db6.jpg', '794c45a6-d742-47b5-85e5-e177d5914db6.jpg.0RDkDrKXPx16yiM1oWjdDgjwqRGohoxP', 'a64221f47f7aa36ba67fb576c24799704245eec9', '2020-11-29 19:22:53'),
(3, '1f.jpg', '1f.jpg.8CegRq6GceRlHzz1RlPI6mvL0Y7plgQ9', '5ce3f2cb0c38828f18bb460f8fb35b39f29a93ed', '2020-11-30 08:33:18'),
(4, 'FABRIC B.jpg', 'FABRIC B.jpg.nHWY9lchATKjskVBw1plrYwarhTP6pzx', 'ed97f8f358b467dc2bd98b1ad6a6d126f902c128', '2020-11-30 08:39:13'),
(5, 'favicon.png', 'favicon.png.IpL85E3vF4qK7XcMkl1pbreTpcjRKMgQ', '855207d927d3998e54892a604329c83699a33b79', '2020-11-30 20:17:52'),
(6, 'logo.png', 'logo.png.EnsVFupjSZVXEHstTlROExKURomycP7Q', '8e82832f4c5c1532e1c3f411332a3cd0e46454ef', '2020-12-04 15:13:39');

-- --------------------------------------------------------

--
-- Table structure for table `oc_user`
--

CREATE TABLE `oc_user` (
  `user_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `image` varchar(255) NOT NULL,
  `code` varchar(40) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_user`
--

INSERT INTO `oc_user` (`user_id`, `user_group_id`, `username`, `password`, `salt`, `firstname`, `lastname`, `email`, `image`, `code`, `ip`, `status`, `date_added`) VALUES
(1, 1, 'dfsdfsd9', '720c6a64c5825f11873e37bb0575229936f6b0c0', 'I2Qrz8L7t', 'LANA', 'SPACE', 'dfsdfsdfsdf@dfsdfsdfs.com', '', '', '41.237.220.100', 1, '2020-11-20 16:07:38');

-- --------------------------------------------------------

--
-- Table structure for table `oc_user_group`
--

CREATE TABLE `oc_user_group` (
  `user_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `permission` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_user_group`
--

INSERT INTO `oc_user_group` (`user_group_id`, `name`, `permission`) VALUES
(1, 'الادارة العليا', '{\"access\":[\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/option\",\"catalog\\/product\",\"catalog\\/recurring\",\"catalog\\/review\",\"common\\/column_left\",\"common\\/developer\",\"common\\/filemanager\",\"common\\/profile\",\"common\\/services\",\"common\\/wallet\",\"customer\\/custom_field\",\"customer\\/customer\",\"customer\\/customer_approval\",\"customer\\/customer_group\",\"design\\/banner\",\"design\\/layout\",\"design\\/seo_url\",\"design\\/theme\",\"design\\/translation\",\"event\\/language\",\"event\\/statistics\",\"event\\/theme\",\"extension\\/analytics\\/google\",\"extension\\/captcha\\/basic\",\"extension\\/captcha\\/google\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/chart\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/map\",\"extension\\/dashboard\\/online\",\"extension\\/dashboard\\/order\",\"extension\\/dashboard\\/recent\",\"extension\\/dashboard\\/sale\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/menu\",\"extension\\/extension\\/module\",\"extension\\/extension\\/payment\",\"extension\\/extension\\/report\",\"extension\\/extension\\/shipping\",\"extension\\/extension\\/theme\",\"extension\\/extension\\/total\",\"extension\\/feed\\/google_base\",\"extension\\/feed\\/google_sitemap\",\"extension\\/feed\\/openbaypro\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/module\\/account\",\"extension\\/module\\/banner\",\"extension\\/module\\/bestproduct\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/blogger\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/featured\",\"extension\\/module\\/filter\",\"extension\\/module\\/html\",\"extension\\/module\\/latest\",\"extension\\/module\\/leftbanner\",\"extension\\/module\\/leftsellbanner\",\"extension\\/module\\/mpseller_account\",\"extension\\/module\\/offerbanner\",\"extension\\/module\\/onsaleproduct\",\"extension\\/module\\/openpos\",\"extension\\/module\\/sellbanner\",\"extension\\/module\\/slideshow\",\"extension\\/module\\/smsa\",\"extension\\/module\\/special\",\"extension\\/module\\/store\",\"extension\\/openbay_\\/amazon\",\"extension\\/openbay_\\/amazon_listing\",\"extension\\/openbay_\\/amazon_product\",\"extension\\/openbay_\\/amazonus\",\"extension\\/openbay_\\/amazonus_listing\",\"extension\\/openbay_\\/amazonus_product\",\"extension\\/openbay_\\/ebay\",\"extension\\/openbay_\\/ebay_profile\",\"extension\\/openbay_\\/ebay_template\",\"extension\\/openbay_\\/etsy\",\"extension\\/openbay_\\/etsy_product\",\"extension\\/openbay_\\/etsy_shipping\",\"extension\\/openbay_\\/etsy_shop\",\"extension\\/openbay_\\/fba\",\"extension\\/payment\\/bank_transfer\",\"extension\\/payment\\/cod\",\"extension\\/payment\\/free_checkout\",\"extension\\/payment\\/paylink\",\"extension\\/report\\/customer_activity\",\"extension\\/report\\/customer_order\",\"extension\\/report\\/customer_reward\",\"extension\\/report\\/customer_search\",\"extension\\/report\\/customer_transaction\",\"extension\\/report\\/marketing\",\"extension\\/report\\/product_purchased\",\"extension\\/report\\/product_viewed\",\"extension\\/report\\/sale_coupon\",\"extension\\/report\\/sale_order\",\"extension\\/report\\/sale_return\",\"extension\\/report\\/sale_shipping\",\"extension\\/report\\/sale_tax\",\"extension\\/shipping\\/fedex\",\"extension\\/shipping\\/flat\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/hitdhlexpress\",\"extension\\/shipping\\/item\",\"extension\\/shipping\\/mpseller\",\"extension\\/shipping\\/pickup\",\"extension\\/shipping\\/weight\",\"extension\\/theme\\/default\",\"extension\\/total\\/coupon\",\"extension\\/total\\/credit\",\"extension\\/total\\/handling\",\"extension\\/total\\/klarna_fee\",\"extension\\/total\\/low_order_fee\",\"extension\\/total\\/reward\",\"extension\\/total\\/shipping\",\"extension\\/total\\/sub_total\",\"extension\\/total\\/tax\",\"extension\\/total\\/total\",\"extension\\/total\\/voucher\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/length_class\",\"localisation\\/location\",\"localisation\\/order_status\",\"localisation\\/return_action\",\"localisation\\/return_reason\",\"localisation\\/return_status\",\"localisation\\/stock_status\",\"localisation\\/tax_class\",\"localisation\\/tax_rate\",\"localisation\\/weight_class\",\"localisation\\/zone\",\"mail\\/affiliate\",\"mail\\/customer\",\"mail\\/forgotten\",\"mail\\/return\",\"mail\\/reward\",\"mail\\/transaction\",\"marketing\\/contact\",\"marketing\\/coupon\",\"marketing\\/marketing\",\"marketplace\\/api\",\"marketplace\\/event\",\"marketplace\\/extension\",\"marketplace\\/install\",\"marketplace\\/installer\",\"marketplace\\/marketplace\",\"marketplace\\/modification\",\"marketplace\\/openbay\",\"mpmultivendor\\/commission\",\"mpmultivendor\\/enquiries\",\"mpmultivendor\\/mpmultivendor\",\"mpmultivendor\\/mpseller\",\"mpmultivendor\\/mpseller_message\",\"mpmultivendor\\/order\",\"mpmultivendor\\/payment\",\"mpmultivendor\\/product\",\"mpmultivendor\\/review\",\"report\\/online\",\"report\\/report\",\"report\\/statistics\",\"req\\/lang\",\"req\\/services\",\"sale\\/order\",\"sale\\/recurring\",\"sale\\/return\",\"sale\\/smsa\",\"sale\\/voucher\",\"sale\\/voucher_theme\",\"setting\\/setting\",\"setting\\/store\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"tool\\/backup\",\"tool\\/log\",\"tool\\/upload\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\"],\"modify\":[\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/option\",\"catalog\\/product\",\"catalog\\/recurring\",\"catalog\\/review\",\"common\\/column_left\",\"common\\/developer\",\"common\\/filemanager\",\"common\\/profile\",\"common\\/services\",\"common\\/wallet\",\"customer\\/custom_field\",\"customer\\/customer\",\"customer\\/customer_approval\",\"customer\\/customer_group\",\"design\\/banner\",\"design\\/layout\",\"design\\/seo_url\",\"design\\/theme\",\"design\\/translation\",\"event\\/language\",\"event\\/statistics\",\"event\\/theme\",\"extension\\/analytics\\/google\",\"extension\\/captcha\\/basic\",\"extension\\/captcha\\/google\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/chart\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/map\",\"extension\\/dashboard\\/online\",\"extension\\/dashboard\\/order\",\"extension\\/dashboard\\/recent\",\"extension\\/dashboard\\/sale\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/menu\",\"extension\\/extension\\/module\",\"extension\\/extension\\/payment\",\"extension\\/extension\\/report\",\"extension\\/extension\\/shipping\",\"extension\\/extension\\/theme\",\"extension\\/extension\\/total\",\"extension\\/feed\\/google_base\",\"extension\\/feed\\/google_sitemap\",\"extension\\/feed\\/openbaypro\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/module\\/account\",\"extension\\/module\\/banner\",\"extension\\/module\\/bestproduct\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/blogger\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/featured\",\"extension\\/module\\/filter\",\"extension\\/module\\/html\",\"extension\\/module\\/latest\",\"extension\\/module\\/leftbanner\",\"extension\\/module\\/leftsellbanner\",\"extension\\/module\\/mpseller_account\",\"extension\\/module\\/offerbanner\",\"extension\\/module\\/onsaleproduct\",\"extension\\/module\\/openpos\",\"extension\\/module\\/sellbanner\",\"extension\\/module\\/slideshow\",\"extension\\/module\\/smsa\",\"extension\\/module\\/special\",\"extension\\/module\\/store\",\"extension\\/openbay_\\/amazon\",\"extension\\/openbay_\\/amazon_listing\",\"extension\\/openbay_\\/amazon_product\",\"extension\\/openbay_\\/amazonus\",\"extension\\/openbay_\\/amazonus_listing\",\"extension\\/openbay_\\/amazonus_product\",\"extension\\/openbay_\\/ebay\",\"extension\\/openbay_\\/ebay_profile\",\"extension\\/openbay_\\/ebay_template\",\"extension\\/openbay_\\/etsy\",\"extension\\/openbay_\\/etsy_product\",\"extension\\/openbay_\\/etsy_shipping\",\"extension\\/openbay_\\/etsy_shop\",\"extension\\/openbay_\\/fba\",\"extension\\/payment\\/bank_transfer\",\"extension\\/payment\\/cod\",\"extension\\/payment\\/free_checkout\",\"extension\\/payment\\/paylink\",\"extension\\/report\\/customer_activity\",\"extension\\/report\\/customer_order\",\"extension\\/report\\/customer_reward\",\"extension\\/report\\/customer_search\",\"extension\\/report\\/customer_transaction\",\"extension\\/report\\/marketing\",\"extension\\/report\\/product_purchased\",\"extension\\/report\\/product_viewed\",\"extension\\/report\\/sale_coupon\",\"extension\\/report\\/sale_order\",\"extension\\/report\\/sale_return\",\"extension\\/report\\/sale_shipping\",\"extension\\/report\\/sale_tax\",\"extension\\/shipping\\/fedex\",\"extension\\/shipping\\/flat\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/hitdhlexpress\",\"extension\\/shipping\\/item\",\"extension\\/shipping\\/mpseller\",\"extension\\/shipping\\/pickup\",\"extension\\/shipping\\/weight\",\"extension\\/theme\\/default\",\"extension\\/total\\/coupon\",\"extension\\/total\\/credit\",\"extension\\/total\\/handling\",\"extension\\/total\\/klarna_fee\",\"extension\\/total\\/low_order_fee\",\"extension\\/total\\/reward\",\"extension\\/total\\/shipping\",\"extension\\/total\\/sub_total\",\"extension\\/total\\/tax\",\"extension\\/total\\/total\",\"extension\\/total\\/voucher\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/length_class\",\"localisation\\/location\",\"localisation\\/order_status\",\"localisation\\/return_action\",\"localisation\\/return_reason\",\"localisation\\/return_status\",\"localisation\\/stock_status\",\"localisation\\/tax_class\",\"localisation\\/tax_rate\",\"localisation\\/weight_class\",\"localisation\\/zone\",\"mail\\/affiliate\",\"mail\\/customer\",\"mail\\/forgotten\",\"mail\\/return\",\"mail\\/reward\",\"mail\\/transaction\",\"marketing\\/contact\",\"marketing\\/coupon\",\"marketing\\/marketing\",\"marketplace\\/api\",\"marketplace\\/event\",\"marketplace\\/extension\",\"marketplace\\/install\",\"marketplace\\/installer\",\"marketplace\\/marketplace\",\"marketplace\\/modification\",\"marketplace\\/openbay\",\"mpmultivendor\\/commission\",\"mpmultivendor\\/enquiries\",\"mpmultivendor\\/mpmultivendor\",\"mpmultivendor\\/mpseller\",\"mpmultivendor\\/mpseller_message\",\"mpmultivendor\\/order\",\"mpmultivendor\\/payment\",\"mpmultivendor\\/product\",\"mpmultivendor\\/review\",\"report\\/online\",\"report\\/report\",\"report\\/statistics\",\"req\\/lang\",\"req\\/services\",\"sale\\/order\",\"sale\\/recurring\",\"sale\\/return\",\"sale\\/smsa\",\"sale\\/voucher\",\"sale\\/voucher_theme\",\"setting\\/setting\",\"setting\\/store\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"tool\\/backup\",\"tool\\/log\",\"tool\\/upload\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\"]}');

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher`
--

CREATE TABLE `oc_voucher` (
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher_history`
--

CREATE TABLE `oc_voucher_history` (
  `voucher_history_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher_theme`
--

CREATE TABLE `oc_voucher_theme` (
  `voucher_theme_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_voucher_theme`
--

INSERT INTO `oc_voucher_theme` (`voucher_theme_id`, `image`) VALUES
(8, 'catalog/demo/canon_eos_5d_2.jpg'),
(7, 'catalog/demo/gift-voucher-birthday.jpg'),
(6, 'catalog/demo/apple_logo.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher_theme_description`
--

CREATE TABLE `oc_voucher_theme_description` (
  `voucher_theme_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_voucher_theme_description`
--

INSERT INTO `oc_voucher_theme_description` (`voucher_theme_id`, `language_id`, `name`) VALUES
(6, 1, 'Christmas'),
(7, 1, 'Birthday'),
(8, 1, 'General'),
(6, 2, 'Christmas'),
(7, 2, 'Birthday'),
(8, 2, 'General');

-- --------------------------------------------------------

--
-- Table structure for table `oc_wallet_transaction`
--

CREATE TABLE `oc_wallet_transaction` (
  `transaction_id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `order_id` int(11) NOT NULL,
  `payment_transaction` varchar(255) NOT NULL,
  `fees` float NOT NULL,
  `supplier_cost` float NOT NULL,
  `notes` longtext NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oc_weight_class`
--

CREATE TABLE `oc_weight_class` (
  `weight_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL DEFAULT '0.00000000'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_weight_class`
--

INSERT INTO `oc_weight_class` (`weight_class_id`, `value`) VALUES
(1, 1.00000000),
(2, 1000.00000000),
(5, 2.20460000),
(6, 35.27400000);

-- --------------------------------------------------------

--
-- Table structure for table `oc_weight_class_description`
--

CREATE TABLE `oc_weight_class_description` (
  `weight_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_weight_class_description`
--

INSERT INTO `oc_weight_class_description` (`weight_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Kilogram', 'kg'),
(2, 1, 'Gram', 'g'),
(5, 1, 'Pound ', 'lb'),
(6, 1, 'Ounce', 'oz'),
(1, 2, 'Kilogram', 'kg'),
(2, 2, 'Gram', 'g'),
(5, 2, 'Pound ', 'lb'),
(6, 2, 'Ounce', 'oz');

-- --------------------------------------------------------

--
-- Table structure for table `oc_zone`
--

CREATE TABLE `oc_zone` (
  `zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_zone`
--

INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1, 1, 'Badakhshan', 'BDS', 1),
(2, 1, 'Badghis', 'BDG', 1),
(3, 1, 'Baghlan', 'BGL', 1),
(4, 1, 'Balkh', 'BAL', 1),
(5, 1, 'Bamian', 'BAM', 1),
(6, 1, 'Farah', 'FRA', 1),
(7, 1, 'Faryab', 'FYB', 1),
(8, 1, 'Ghazni', 'GHA', 1),
(9, 1, 'Ghowr', 'GHO', 1),
(10, 1, 'Helmand', 'HEL', 1),
(11, 1, 'Herat', 'HER', 1),
(12, 1, 'Jowzjan', 'JOW', 1),
(13, 1, 'Kabul', 'KAB', 1),
(14, 1, 'Kandahar', 'KAN', 1),
(15, 1, 'Kapisa', 'KAP', 1),
(16, 1, 'Khost', 'KHO', 1),
(17, 1, 'Konar', 'KNR', 1),
(18, 1, 'Kondoz', 'KDZ', 1),
(19, 1, 'Laghman', 'LAG', 1),
(20, 1, 'Lowgar', 'LOW', 1),
(21, 1, 'Nangrahar', 'NAN', 1),
(22, 1, 'Nimruz', 'NIM', 1),
(23, 1, 'Nurestan', 'NUR', 1),
(24, 1, 'Oruzgan', 'ORU', 1),
(25, 1, 'Paktia', 'PIA', 1),
(26, 1, 'Paktika', 'PKA', 1),
(27, 1, 'Parwan', 'PAR', 1),
(28, 1, 'Samangan', 'SAM', 1),
(29, 1, 'Sar-e Pol', 'SAR', 1),
(30, 1, 'Takhar', 'TAK', 1),
(31, 1, 'Wardak', 'WAR', 1),
(32, 1, 'Zabol', 'ZAB', 1),
(33, 2, 'Berat', 'BR', 1),
(34, 2, 'Bulqize', 'BU', 1),
(35, 2, 'Delvine', 'DL', 1),
(36, 2, 'Devoll', 'DV', 1),
(37, 2, 'Diber', 'DI', 1),
(38, 2, 'Durres', 'DR', 1),
(39, 2, 'Elbasan', 'EL', 1),
(40, 2, 'Kolonje', 'ER', 1),
(41, 2, 'Fier', 'FR', 1),
(42, 2, 'Gjirokaster', 'GJ', 1),
(43, 2, 'Gramsh', 'GR', 1),
(44, 2, 'Has', 'HA', 1),
(45, 2, 'Kavaje', 'KA', 1),
(46, 2, 'Kurbin', 'KB', 1),
(47, 2, 'Kucove', 'KC', 1),
(48, 2, 'Korce', 'KO', 1),
(49, 2, 'Kruje', 'KR', 1),
(50, 2, 'Kukes', 'KU', 1),
(51, 2, 'Librazhd', 'LB', 1),
(52, 2, 'Lezhe', 'LE', 1),
(53, 2, 'Lushnje', 'LU', 1),
(54, 2, 'Malesi e Madhe', 'MM', 1),
(55, 2, 'Mallakaster', 'MK', 1),
(56, 2, 'Mat', 'MT', 1),
(57, 2, 'Mirdite', 'MR', 1),
(58, 2, 'Peqin', 'PQ', 1),
(59, 2, 'Permet', 'PR', 1),
(60, 2, 'Pogradec', 'PG', 1),
(61, 2, 'Puke', 'PU', 1),
(62, 2, 'Shkoder', 'SH', 1),
(63, 2, 'Skrapar', 'SK', 1),
(64, 2, 'Sarande', 'SR', 1),
(65, 2, 'Tepelene', 'TE', 1),
(66, 2, 'Tropoje', 'TP', 1),
(67, 2, 'Tirane', 'TR', 1),
(68, 2, 'Vlore', 'VL', 1),
(69, 3, 'Adrar', 'ADR', 1),
(70, 3, 'Ain Defla', 'ADE', 1),
(71, 3, 'Ain Temouchent', 'ATE', 1),
(72, 3, 'Alger', 'ALG', 1),
(73, 3, 'Annaba', 'ANN', 1),
(74, 3, 'Batna', 'BAT', 1),
(75, 3, 'Bechar', 'BEC', 1),
(76, 3, 'Bejaia', 'BEJ', 1),
(77, 3, 'Biskra', 'BIS', 1),
(78, 3, 'Blida', 'BLI', 1),
(79, 3, 'Bordj Bou Arreridj', 'BBA', 1),
(80, 3, 'Bouira', 'BOA', 1),
(81, 3, 'Boumerdes', 'BMD', 1),
(82, 3, 'Chlef', 'CHL', 1),
(83, 3, 'Constantine', 'CON', 1),
(84, 3, 'Djelfa', 'DJE', 1),
(85, 3, 'El Bayadh', 'EBA', 1),
(86, 3, 'El Oued', 'EOU', 1),
(87, 3, 'El Tarf', 'ETA', 1),
(88, 3, 'Ghardaia', 'GHA', 1),
(89, 3, 'Guelma', 'GUE', 1),
(90, 3, 'Illizi', 'ILL', 1),
(91, 3, 'Jijel', 'JIJ', 1),
(92, 3, 'Khenchela', 'KHE', 1),
(93, 3, 'Laghouat', 'LAG', 1),
(94, 3, 'Muaskar', 'MUA', 1),
(95, 3, 'Medea', 'MED', 1),
(96, 3, 'Mila', 'MIL', 1),
(97, 3, 'Mostaganem', 'MOS', 1),
(98, 3, 'M\'Sila', 'MSI', 1),
(99, 3, 'Naama', 'NAA', 1),
(100, 3, 'Oran', 'ORA', 1),
(101, 3, 'Ouargla', 'OUA', 1),
(102, 3, 'Oum el-Bouaghi', 'OEB', 1),
(103, 3, 'Relizane', 'REL', 1),
(104, 3, 'Saida', 'SAI', 1),
(105, 3, 'Setif', 'SET', 1),
(106, 3, 'Sidi Bel Abbes', 'SBA', 1),
(107, 3, 'Skikda', 'SKI', 1),
(108, 3, 'Souk Ahras', 'SAH', 1),
(109, 3, 'Tamanghasset', 'TAM', 1),
(110, 3, 'Tebessa', 'TEB', 1),
(111, 3, 'Tiaret', 'TIA', 1),
(112, 3, 'Tindouf', 'TIN', 1),
(113, 3, 'Tipaza', 'TIP', 1),
(114, 3, 'Tissemsilt', 'TIS', 1),
(115, 3, 'Tizi Ouzou', 'TOU', 1),
(116, 3, 'Tlemcen', 'TLE', 1),
(117, 4, 'Eastern', 'E', 1),
(118, 4, 'Manu\'a', 'M', 1),
(119, 4, 'Rose Island', 'R', 1),
(120, 4, 'Swains Island', 'S', 1),
(121, 4, 'Western', 'W', 1),
(122, 5, 'Andorra la Vella', 'ALV', 1),
(123, 5, 'Canillo', 'CAN', 1),
(124, 5, 'Encamp', 'ENC', 1),
(125, 5, 'Escaldes-Engordany', 'ESE', 1),
(126, 5, 'La Massana', 'LMA', 1),
(127, 5, 'Ordino', 'ORD', 1),
(128, 5, 'Sant Julia de Loria', 'SJL', 1),
(129, 6, 'Bengo', 'BGO', 1),
(130, 6, 'Benguela', 'BGU', 1),
(131, 6, 'Bie', 'BIE', 1),
(132, 6, 'Cabinda', 'CAB', 1),
(133, 6, 'Cuando-Cubango', 'CCU', 1),
(134, 6, 'Cuanza Norte', 'CNO', 1),
(135, 6, 'Cuanza Sul', 'CUS', 1),
(136, 6, 'Cunene', 'CNN', 1),
(137, 6, 'Huambo', 'HUA', 1),
(138, 6, 'Huila', 'HUI', 1),
(139, 6, 'Luanda', 'LUA', 1),
(140, 6, 'Lunda Norte', 'LNO', 1),
(141, 6, 'Lunda Sul', 'LSU', 1),
(142, 6, 'Malange', 'MAL', 1),
(143, 6, 'Moxico', 'MOX', 1),
(144, 6, 'Namibe', 'NAM', 1),
(145, 6, 'Uige', 'UIG', 1),
(146, 6, 'Zaire', 'ZAI', 1),
(147, 9, 'Saint George', 'ASG', 1),
(148, 9, 'Saint John', 'ASJ', 1),
(149, 9, 'Saint Mary', 'ASM', 1),
(150, 9, 'Saint Paul', 'ASL', 1),
(151, 9, 'Saint Peter', 'ASR', 1),
(152, 9, 'Saint Philip', 'ASH', 1),
(153, 9, 'Barbuda', 'BAR', 1),
(154, 9, 'Redonda', 'RED', 1),
(155, 10, 'Antartida e Islas del Atlantico', 'AN', 1),
(156, 10, 'Buenos Aires', 'BA', 1),
(157, 10, 'Catamarca', 'CA', 1),
(158, 10, 'Chaco', 'CH', 1),
(159, 10, 'Chubut', 'CU', 1),
(160, 10, 'Cordoba', 'CO', 1),
(161, 10, 'Corrientes', 'CR', 1),
(162, 10, 'Distrito Federal', 'DF', 1),
(163, 10, 'Entre Rios', 'ER', 1),
(164, 10, 'Formosa', 'FO', 1),
(165, 10, 'Jujuy', 'JU', 1),
(166, 10, 'La Pampa', 'LP', 1),
(167, 10, 'La Rioja', 'LR', 1),
(168, 10, 'Mendoza', 'ME', 1),
(169, 10, 'Misiones', 'MI', 1),
(170, 10, 'Neuquen', 'NE', 1),
(171, 10, 'Rio Negro', 'RN', 1),
(172, 10, 'Salta', 'SA', 1),
(173, 10, 'San Juan', 'SJ', 1),
(174, 10, 'San Luis', 'SL', 1),
(175, 10, 'Santa Cruz', 'SC', 1),
(176, 10, 'Santa Fe', 'SF', 1),
(177, 10, 'Santiago del Estero', 'SD', 1),
(178, 10, 'Tierra del Fuego', 'TF', 1),
(179, 10, 'Tucuman', 'TU', 1),
(180, 11, 'Aragatsotn', 'AGT', 1),
(181, 11, 'Ararat', 'ARR', 1),
(182, 11, 'Armavir', 'ARM', 1),
(183, 11, 'Geghark\'unik\'', 'GEG', 1),
(184, 11, 'Kotayk\'', 'KOT', 1),
(185, 11, 'Lorri', 'LOR', 1),
(186, 11, 'Shirak', 'SHI', 1),
(187, 11, 'Syunik\'', 'SYU', 1),
(188, 11, 'Tavush', 'TAV', 1),
(189, 11, 'Vayots\' Dzor', 'VAY', 1),
(190, 11, 'Yerevan', 'YER', 1),
(191, 13, 'Australian Capital Territory', 'ACT', 1),
(192, 13, 'New South Wales', 'NSW', 1),
(193, 13, 'Northern Territory', 'NT', 1),
(194, 13, 'Queensland', 'QLD', 1),
(195, 13, 'South Australia', 'SA', 1),
(196, 13, 'Tasmania', 'TAS', 1),
(197, 13, 'Victoria', 'VIC', 1),
(198, 13, 'Western Australia', 'WA', 1),
(199, 14, 'Burgenland', 'BUR', 1),
(200, 14, 'Kärnten', 'KAR', 1),
(201, 14, 'Niederösterreich', 'NOS', 1),
(202, 14, 'Oberösterreich', 'OOS', 1),
(203, 14, 'Salzburg', 'SAL', 1),
(204, 14, 'Steiermark', 'STE', 1),
(205, 14, 'Tirol', 'TIR', 1),
(206, 14, 'Vorarlberg', 'VOR', 1),
(207, 14, 'Wien', 'WIE', 1),
(208, 15, 'Ali Bayramli', 'AB', 1),
(209, 15, 'Abseron', 'ABS', 1),
(210, 15, 'AgcabAdi', 'AGC', 1),
(211, 15, 'Agdam', 'AGM', 1),
(212, 15, 'Agdas', 'AGS', 1),
(213, 15, 'Agstafa', 'AGA', 1),
(214, 15, 'Agsu', 'AGU', 1),
(215, 15, 'Astara', 'AST', 1),
(216, 15, 'Baki', 'BA', 1),
(217, 15, 'BabAk', 'BAB', 1),
(218, 15, 'BalakAn', 'BAL', 1),
(219, 15, 'BArdA', 'BAR', 1),
(220, 15, 'Beylaqan', 'BEY', 1),
(221, 15, 'Bilasuvar', 'BIL', 1),
(222, 15, 'Cabrayil', 'CAB', 1),
(223, 15, 'Calilabab', 'CAL', 1),
(224, 15, 'Culfa', 'CUL', 1),
(225, 15, 'Daskasan', 'DAS', 1),
(226, 15, 'Davaci', 'DAV', 1),
(227, 15, 'Fuzuli', 'FUZ', 1),
(228, 15, 'Ganca', 'GA', 1),
(229, 15, 'Gadabay', 'GAD', 1),
(230, 15, 'Goranboy', 'GOR', 1),
(231, 15, 'Goycay', 'GOY', 1),
(232, 15, 'Haciqabul', 'HAC', 1),
(233, 15, 'Imisli', 'IMI', 1),
(234, 15, 'Ismayilli', 'ISM', 1),
(235, 15, 'Kalbacar', 'KAL', 1),
(236, 15, 'Kurdamir', 'KUR', 1),
(237, 15, 'Lankaran', 'LA', 1),
(238, 15, 'Lacin', 'LAC', 1),
(239, 15, 'Lankaran', 'LAN', 1),
(240, 15, 'Lerik', 'LER', 1),
(241, 15, 'Masalli', 'MAS', 1),
(242, 15, 'Mingacevir', 'MI', 1),
(243, 15, 'Naftalan', 'NA', 1),
(244, 15, 'Neftcala', 'NEF', 1),
(245, 15, 'Oguz', 'OGU', 1),
(246, 15, 'Ordubad', 'ORD', 1),
(247, 15, 'Qabala', 'QAB', 1),
(248, 15, 'Qax', 'QAX', 1),
(249, 15, 'Qazax', 'QAZ', 1),
(250, 15, 'Qobustan', 'QOB', 1),
(251, 15, 'Quba', 'QBA', 1),
(252, 15, 'Qubadli', 'QBI', 1),
(253, 15, 'Qusar', 'QUS', 1),
(254, 15, 'Saki', 'SA', 1),
(255, 15, 'Saatli', 'SAT', 1),
(256, 15, 'Sabirabad', 'SAB', 1),
(257, 15, 'Sadarak', 'SAD', 1),
(258, 15, 'Sahbuz', 'SAH', 1),
(259, 15, 'Saki', 'SAK', 1),
(260, 15, 'Salyan', 'SAL', 1),
(261, 15, 'Sumqayit', 'SM', 1),
(262, 15, 'Samaxi', 'SMI', 1),
(263, 15, 'Samkir', 'SKR', 1),
(264, 15, 'Samux', 'SMX', 1),
(265, 15, 'Sarur', 'SAR', 1),
(266, 15, 'Siyazan', 'SIY', 1),
(267, 15, 'Susa', 'SS', 1),
(268, 15, 'Susa', 'SUS', 1),
(269, 15, 'Tartar', 'TAR', 1),
(270, 15, 'Tovuz', 'TOV', 1),
(271, 15, 'Ucar', 'UCA', 1),
(272, 15, 'Xankandi', 'XA', 1),
(273, 15, 'Xacmaz', 'XAC', 1),
(274, 15, 'Xanlar', 'XAN', 1),
(275, 15, 'Xizi', 'XIZ', 1),
(276, 15, 'Xocali', 'XCI', 1),
(277, 15, 'Xocavand', 'XVD', 1),
(278, 15, 'Yardimli', 'YAR', 1),
(279, 15, 'Yevlax', 'YEV', 1),
(280, 15, 'Zangilan', 'ZAN', 1),
(281, 15, 'Zaqatala', 'ZAQ', 1),
(282, 15, 'Zardab', 'ZAR', 1),
(283, 15, 'Naxcivan', 'NX', 1),
(284, 16, 'Acklins', 'ACK', 1),
(285, 16, 'Berry Islands', 'BER', 1),
(286, 16, 'Bimini', 'BIM', 1),
(287, 16, 'Black Point', 'BLK', 1),
(288, 16, 'Cat Island', 'CAT', 1),
(289, 16, 'Central Abaco', 'CAB', 1),
(290, 16, 'Central Andros', 'CAN', 1),
(291, 16, 'Central Eleuthera', 'CEL', 1),
(292, 16, 'City of Freeport', 'FRE', 1),
(293, 16, 'Crooked Island', 'CRO', 1),
(294, 16, 'East Grand Bahama', 'EGB', 1),
(295, 16, 'Exuma', 'EXU', 1),
(296, 16, 'Grand Cay', 'GRD', 1),
(297, 16, 'Harbour Island', 'HAR', 1),
(298, 16, 'Hope Town', 'HOP', 1),
(299, 16, 'Inagua', 'INA', 1),
(300, 16, 'Long Island', 'LNG', 1),
(301, 16, 'Mangrove Cay', 'MAN', 1),
(302, 16, 'Mayaguana', 'MAY', 1),
(303, 16, 'Moore\'s Island', 'MOO', 1),
(304, 16, 'North Abaco', 'NAB', 1),
(305, 16, 'North Andros', 'NAN', 1),
(306, 16, 'North Eleuthera', 'NEL', 1),
(307, 16, 'Ragged Island', 'RAG', 1),
(308, 16, 'Rum Cay', 'RUM', 1),
(309, 16, 'San Salvador', 'SAL', 1),
(310, 16, 'South Abaco', 'SAB', 1),
(311, 16, 'South Andros', 'SAN', 1),
(312, 16, 'South Eleuthera', 'SEL', 1),
(313, 16, 'Spanish Wells', 'SWE', 1),
(314, 16, 'West Grand Bahama', 'WGB', 1),
(315, 17, 'Capital', 'CAP', 1),
(316, 17, 'Central', 'CEN', 1),
(317, 17, 'Muharraq', 'MUH', 1),
(318, 17, 'Northern', 'NOR', 1),
(319, 17, 'Southern', 'SOU', 1),
(320, 18, 'Barisal', 'BAR', 1),
(321, 18, 'Chittagong', 'CHI', 1),
(322, 18, 'Dhaka', 'DHA', 1),
(323, 18, 'Khulna', 'KHU', 1),
(324, 18, 'Rajshahi', 'RAJ', 1),
(325, 18, 'Sylhet', 'SYL', 1),
(326, 19, 'Christ Church', 'CC', 1),
(327, 19, 'Saint Andrew', 'AND', 1),
(328, 19, 'Saint George', 'GEO', 1),
(329, 19, 'Saint James', 'JAM', 1),
(330, 19, 'Saint John', 'JOH', 1),
(331, 19, 'Saint Joseph', 'JOS', 1),
(332, 19, 'Saint Lucy', 'LUC', 1),
(333, 19, 'Saint Michael', 'MIC', 1),
(334, 19, 'Saint Peter', 'PET', 1),
(335, 19, 'Saint Philip', 'PHI', 1),
(336, 19, 'Saint Thomas', 'THO', 1),
(337, 20, 'Brestskaya (Brest)', 'BR', 1),
(338, 20, 'Homyel\'skaya (Homyel\')', 'HO', 1),
(339, 20, 'Horad Minsk', 'HM', 1),
(340, 20, 'Hrodzyenskaya (Hrodna)', 'HR', 1),
(341, 20, 'Mahilyowskaya (Mahilyow)', 'MA', 1),
(342, 20, 'Minskaya', 'MI', 1),
(343, 20, 'Vitsyebskaya (Vitsyebsk)', 'VI', 1),
(344, 21, 'Antwerpen', 'VAN', 1),
(345, 21, 'Brabant Wallon', 'WBR', 1),
(346, 21, 'Hainaut', 'WHT', 1),
(347, 21, 'Liège', 'WLG', 1),
(348, 21, 'Limburg', 'VLI', 1),
(349, 21, 'Luxembourg', 'WLX', 1),
(350, 21, 'Namur', 'WNA', 1),
(351, 21, 'Oost-Vlaanderen', 'VOV', 1),
(352, 21, 'Vlaams Brabant', 'VBR', 1),
(353, 21, 'West-Vlaanderen', 'VWV', 1),
(354, 22, 'Belize', 'BZ', 1),
(355, 22, 'Cayo', 'CY', 1),
(356, 22, 'Corozal', 'CR', 1),
(357, 22, 'Orange Walk', 'OW', 1),
(358, 22, 'Stann Creek', 'SC', 1),
(359, 22, 'Toledo', 'TO', 1),
(360, 23, 'Alibori', 'AL', 1),
(361, 23, 'Atakora', 'AK', 1),
(362, 23, 'Atlantique', 'AQ', 1),
(363, 23, 'Borgou', 'BO', 1),
(364, 23, 'Collines', 'CO', 1),
(365, 23, 'Donga', 'DO', 1),
(366, 23, 'Kouffo', 'KO', 1),
(367, 23, 'Littoral', 'LI', 1),
(368, 23, 'Mono', 'MO', 1),
(369, 23, 'Oueme', 'OU', 1),
(370, 23, 'Plateau', 'PL', 1),
(371, 23, 'Zou', 'ZO', 1),
(372, 24, 'Devonshire', 'DS', 1),
(373, 24, 'Hamilton City', 'HC', 1),
(374, 24, 'Hamilton', 'HA', 1),
(375, 24, 'Paget', 'PG', 1),
(376, 24, 'Pembroke', 'PB', 1),
(377, 24, 'Saint George City', 'GC', 1),
(378, 24, 'Saint George\'s', 'SG', 1),
(379, 24, 'Sandys', 'SA', 1),
(380, 24, 'Smith\'s', 'SM', 1),
(381, 24, 'Southampton', 'SH', 1),
(382, 24, 'Warwick', 'WA', 1),
(383, 25, 'Bumthang', 'BUM', 1),
(384, 25, 'Chukha', 'CHU', 1),
(385, 25, 'Dagana', 'DAG', 1),
(386, 25, 'Gasa', 'GAS', 1),
(387, 25, 'Haa', 'HAA', 1),
(388, 25, 'Lhuntse', 'LHU', 1),
(389, 25, 'Mongar', 'MON', 1),
(390, 25, 'Paro', 'PAR', 1),
(391, 25, 'Pemagatshel', 'PEM', 1),
(392, 25, 'Punakha', 'PUN', 1),
(393, 25, 'Samdrup Jongkhar', 'SJO', 1),
(394, 25, 'Samtse', 'SAT', 1),
(395, 25, 'Sarpang', 'SAR', 1),
(396, 25, 'Thimphu', 'THI', 1),
(397, 25, 'Trashigang', 'TRG', 1),
(398, 25, 'Trashiyangste', 'TRY', 1),
(399, 25, 'Trongsa', 'TRO', 1),
(400, 25, 'Tsirang', 'TSI', 1),
(401, 25, 'Wangdue Phodrang', 'WPH', 1),
(402, 25, 'Zhemgang', 'ZHE', 1),
(403, 26, 'Beni', 'BEN', 1),
(404, 26, 'Chuquisaca', 'CHU', 1),
(405, 26, 'Cochabamba', 'COC', 1),
(406, 26, 'La Paz', 'LPZ', 1),
(407, 26, 'Oruro', 'ORU', 1),
(408, 26, 'Pando', 'PAN', 1),
(409, 26, 'Potosi', 'POT', 1),
(410, 26, 'Santa Cruz', 'SCZ', 1),
(411, 26, 'Tarija', 'TAR', 1),
(412, 27, 'Brcko district', 'BRO', 1),
(413, 27, 'Unsko-Sanski Kanton', 'FUS', 1),
(414, 27, 'Posavski Kanton', 'FPO', 1),
(415, 27, 'Tuzlanski Kanton', 'FTU', 1),
(416, 27, 'Zenicko-Dobojski Kanton', 'FZE', 1),
(417, 27, 'Bosanskopodrinjski Kanton', 'FBP', 1),
(418, 27, 'Srednjebosanski Kanton', 'FSB', 1),
(419, 27, 'Hercegovacko-neretvanski Kanton', 'FHN', 1),
(420, 27, 'Zapadnohercegovacka Zupanija', 'FZH', 1),
(421, 27, 'Kanton Sarajevo', 'FSA', 1),
(422, 27, 'Zapadnobosanska', 'FZA', 1),
(423, 27, 'Banja Luka', 'SBL', 1),
(424, 27, 'Doboj', 'SDO', 1),
(425, 27, 'Bijeljina', 'SBI', 1),
(426, 27, 'Vlasenica', 'SVL', 1),
(427, 27, 'Sarajevo-Romanija or Sokolac', 'SSR', 1),
(428, 27, 'Foca', 'SFO', 1),
(429, 27, 'Trebinje', 'STR', 1),
(430, 28, 'Central', 'CE', 1),
(431, 28, 'Ghanzi', 'GH', 1),
(432, 28, 'Kgalagadi', 'KD', 1),
(433, 28, 'Kgatleng', 'KT', 1),
(434, 28, 'Kweneng', 'KW', 1),
(435, 28, 'Ngamiland', 'NG', 1),
(436, 28, 'North East', 'NE', 1),
(437, 28, 'North West', 'NW', 1),
(438, 28, 'South East', 'SE', 1),
(439, 28, 'Southern', 'SO', 1),
(440, 30, 'Acre', 'AC', 1),
(441, 30, 'Alagoas', 'AL', 1),
(442, 30, 'Amapá', 'AP', 1),
(443, 30, 'Amazonas', 'AM', 1),
(444, 30, 'Bahia', 'BA', 1),
(445, 30, 'Ceará', 'CE', 1),
(446, 30, 'Distrito Federal', 'DF', 1),
(447, 30, 'Espírito Santo', 'ES', 1),
(448, 30, 'Goiás', 'GO', 1),
(449, 30, 'Maranhão', 'MA', 1),
(450, 30, 'Mato Grosso', 'MT', 1),
(451, 30, 'Mato Grosso do Sul', 'MS', 1),
(452, 30, 'Minas Gerais', 'MG', 1),
(453, 30, 'Pará', 'PA', 1),
(454, 30, 'Paraíba', 'PB', 1),
(455, 30, 'Paraná', 'PR', 1),
(456, 30, 'Pernambuco', 'PE', 1),
(457, 30, 'Piauí', 'PI', 1),
(458, 30, 'Rio de Janeiro', 'RJ', 1),
(459, 30, 'Rio Grande do Norte', 'RN', 1),
(460, 30, 'Rio Grande do Sul', 'RS', 1),
(461, 30, 'Rondônia', 'RO', 1),
(462, 30, 'Roraima', 'RR', 1),
(463, 30, 'Santa Catarina', 'SC', 1),
(464, 30, 'São Paulo', 'SP', 1),
(465, 30, 'Sergipe', 'SE', 1),
(466, 30, 'Tocantins', 'TO', 1),
(467, 31, 'Peros Banhos', 'PB', 1),
(468, 31, 'Salomon Islands', 'SI', 1),
(469, 31, 'Nelsons Island', 'NI', 1),
(470, 31, 'Three Brothers', 'TB', 1),
(471, 31, 'Eagle Islands', 'EA', 1),
(472, 31, 'Danger Island', 'DI', 1),
(473, 31, 'Egmont Islands', 'EG', 1),
(474, 31, 'Diego Garcia', 'DG', 1),
(475, 32, 'Belait', 'BEL', 1),
(476, 32, 'Brunei and Muara', 'BRM', 1),
(477, 32, 'Temburong', 'TEM', 1),
(478, 32, 'Tutong', 'TUT', 1),
(479, 33, 'Blagoevgrad', '', 1),
(480, 33, 'Burgas', '', 1),
(481, 33, 'Dobrich', '', 1),
(482, 33, 'Gabrovo', '', 1),
(483, 33, 'Haskovo', '', 1),
(484, 33, 'Kardjali', '', 1),
(485, 33, 'Kyustendil', '', 1),
(486, 33, 'Lovech', '', 1),
(487, 33, 'Montana', '', 1),
(488, 33, 'Pazardjik', '', 1),
(489, 33, 'Pernik', '', 1),
(490, 33, 'Pleven', '', 1),
(491, 33, 'Plovdiv', '', 1),
(492, 33, 'Razgrad', '', 1),
(493, 33, 'Shumen', '', 1),
(494, 33, 'Silistra', '', 1),
(495, 33, 'Sliven', '', 1),
(496, 33, 'Smolyan', '', 1),
(497, 33, 'Sofia', '', 1),
(498, 33, 'Sofia - town', '', 1),
(499, 33, 'Stara Zagora', '', 1),
(500, 33, 'Targovishte', '', 1),
(501, 33, 'Varna', '', 1),
(502, 33, 'Veliko Tarnovo', '', 1),
(503, 33, 'Vidin', '', 1),
(504, 33, 'Vratza', '', 1),
(505, 33, 'Yambol', '', 1),
(506, 34, 'Bale', 'BAL', 1),
(507, 34, 'Bam', 'BAM', 1),
(508, 34, 'Banwa', 'BAN', 1),
(509, 34, 'Bazega', 'BAZ', 1),
(510, 34, 'Bougouriba', 'BOR', 1),
(511, 34, 'Boulgou', 'BLG', 1),
(512, 34, 'Boulkiemde', 'BOK', 1),
(513, 34, 'Comoe', 'COM', 1),
(514, 34, 'Ganzourgou', 'GAN', 1),
(515, 34, 'Gnagna', 'GNA', 1),
(516, 34, 'Gourma', 'GOU', 1),
(517, 34, 'Houet', 'HOU', 1),
(518, 34, 'Ioba', 'IOA', 1),
(519, 34, 'Kadiogo', 'KAD', 1),
(520, 34, 'Kenedougou', 'KEN', 1),
(521, 34, 'Komondjari', 'KOD', 1),
(522, 34, 'Kompienga', 'KOP', 1),
(523, 34, 'Kossi', 'KOS', 1),
(524, 34, 'Koulpelogo', 'KOL', 1),
(525, 34, 'Kouritenga', 'KOT', 1),
(526, 34, 'Kourweogo', 'KOW', 1),
(527, 34, 'Leraba', 'LER', 1),
(528, 34, 'Loroum', 'LOR', 1),
(529, 34, 'Mouhoun', 'MOU', 1),
(530, 34, 'Nahouri', 'NAH', 1),
(531, 34, 'Namentenga', 'NAM', 1),
(532, 34, 'Nayala', 'NAY', 1),
(533, 34, 'Noumbiel', 'NOU', 1),
(534, 34, 'Oubritenga', 'OUB', 1),
(535, 34, 'Oudalan', 'OUD', 1),
(536, 34, 'Passore', 'PAS', 1),
(537, 34, 'Poni', 'PON', 1),
(538, 34, 'Sanguie', 'SAG', 1),
(539, 34, 'Sanmatenga', 'SAM', 1),
(540, 34, 'Seno', 'SEN', 1),
(541, 34, 'Sissili', 'SIS', 1),
(542, 34, 'Soum', 'SOM', 1),
(543, 34, 'Sourou', 'SOR', 1),
(544, 34, 'Tapoa', 'TAP', 1),
(545, 34, 'Tuy', 'TUY', 1),
(546, 34, 'Yagha', 'YAG', 1),
(547, 34, 'Yatenga', 'YAT', 1),
(548, 34, 'Ziro', 'ZIR', 1),
(549, 34, 'Zondoma', 'ZOD', 1),
(550, 34, 'Zoundweogo', 'ZOW', 1),
(551, 35, 'Bubanza', 'BB', 1),
(552, 35, 'Bujumbura', 'BJ', 1),
(553, 35, 'Bururi', 'BR', 1),
(554, 35, 'Cankuzo', 'CA', 1),
(555, 35, 'Cibitoke', 'CI', 1),
(556, 35, 'Gitega', 'GI', 1),
(557, 35, 'Karuzi', 'KR', 1),
(558, 35, 'Kayanza', 'KY', 1),
(559, 35, 'Kirundo', 'KI', 1),
(560, 35, 'Makamba', 'MA', 1),
(561, 35, 'Muramvya', 'MU', 1),
(562, 35, 'Muyinga', 'MY', 1),
(563, 35, 'Mwaro', 'MW', 1),
(564, 35, 'Ngozi', 'NG', 1),
(565, 35, 'Rutana', 'RT', 1),
(566, 35, 'Ruyigi', 'RY', 1),
(567, 36, 'Phnom Penh', 'PP', 1),
(568, 36, 'Preah Seihanu (Kompong Som or Sihanoukville)', 'PS', 1),
(569, 36, 'Pailin', 'PA', 1),
(570, 36, 'Keb', 'KB', 1),
(571, 36, 'Banteay Meanchey', 'BM', 1),
(572, 36, 'Battambang', 'BA', 1),
(573, 36, 'Kampong Cham', 'KM', 1),
(574, 36, 'Kampong Chhnang', 'KN', 1),
(575, 36, 'Kampong Speu', 'KU', 1),
(576, 36, 'Kampong Som', 'KO', 1),
(577, 36, 'Kampong Thom', 'KT', 1),
(578, 36, 'Kampot', 'KP', 1),
(579, 36, 'Kandal', 'KL', 1),
(580, 36, 'Kaoh Kong', 'KK', 1),
(581, 36, 'Kratie', 'KR', 1),
(582, 36, 'Mondul Kiri', 'MK', 1),
(583, 36, 'Oddar Meancheay', 'OM', 1),
(584, 36, 'Pursat', 'PU', 1),
(585, 36, 'Preah Vihear', 'PR', 1),
(586, 36, 'Prey Veng', 'PG', 1),
(587, 36, 'Ratanak Kiri', 'RK', 1),
(588, 36, 'Siemreap', 'SI', 1),
(589, 36, 'Stung Treng', 'ST', 1),
(590, 36, 'Svay Rieng', 'SR', 1),
(591, 36, 'Takeo', 'TK', 1),
(592, 37, 'Adamawa (Adamaoua)', 'ADA', 1),
(593, 37, 'Centre', 'CEN', 1),
(594, 37, 'East (Est)', 'EST', 1),
(595, 37, 'Extreme North (Extreme-Nord)', 'EXN', 1),
(596, 37, 'Littoral', 'LIT', 1),
(597, 37, 'North (Nord)', 'NOR', 1),
(598, 37, 'Northwest (Nord-Ouest)', 'NOT', 1),
(599, 37, 'West (Ouest)', 'OUE', 1),
(600, 37, 'South (Sud)', 'SUD', 1),
(601, 37, 'Southwest (Sud-Ouest).', 'SOU', 1),
(602, 38, 'Alberta', 'AB', 1),
(603, 38, 'British Columbia', 'BC', 1),
(604, 38, 'Manitoba', 'MB', 1),
(605, 38, 'New Brunswick', 'NB', 1),
(606, 38, 'Newfoundland and Labrador', 'NL', 1),
(607, 38, 'Northwest Territories', 'NT', 1),
(608, 38, 'Nova Scotia', 'NS', 1),
(609, 38, 'Nunavut', 'NU', 1),
(610, 38, 'Ontario', 'ON', 1),
(611, 38, 'Prince Edward Island', 'PE', 1),
(612, 38, 'Qu&eacute;bec', 'QC', 1),
(613, 38, 'Saskatchewan', 'SK', 1),
(614, 38, 'Yukon Territory', 'YT', 1),
(615, 39, 'Boa Vista', 'BV', 1),
(616, 39, 'Brava', 'BR', 1),
(617, 39, 'Calheta de Sao Miguel', 'CS', 1),
(618, 39, 'Maio', 'MA', 1),
(619, 39, 'Mosteiros', 'MO', 1),
(620, 39, 'Paul', 'PA', 1),
(621, 39, 'Porto Novo', 'PN', 1),
(622, 39, 'Praia', 'PR', 1),
(623, 39, 'Ribeira Grande', 'RG', 1),
(624, 39, 'Sal', 'SL', 1),
(625, 39, 'Santa Catarina', 'CA', 1),
(626, 39, 'Santa Cruz', 'CR', 1),
(627, 39, 'Sao Domingos', 'SD', 1),
(628, 39, 'Sao Filipe', 'SF', 1),
(629, 39, 'Sao Nicolau', 'SN', 1),
(630, 39, 'Sao Vicente', 'SV', 1),
(631, 39, 'Tarrafal', 'TA', 1),
(632, 40, 'Creek', 'CR', 1),
(633, 40, 'Eastern', 'EA', 1),
(634, 40, 'Midland', 'ML', 1),
(635, 40, 'South Town', 'ST', 1),
(636, 40, 'Spot Bay', 'SP', 1),
(637, 40, 'Stake Bay', 'SK', 1),
(638, 40, 'West End', 'WD', 1),
(639, 40, 'Western', 'WN', 1),
(640, 41, 'Bamingui-Bangoran', 'BBA', 1),
(641, 41, 'Basse-Kotto', 'BKO', 1),
(642, 41, 'Haute-Kotto', 'HKO', 1),
(643, 41, 'Haut-Mbomou', 'HMB', 1),
(644, 41, 'Kemo', 'KEM', 1),
(645, 41, 'Lobaye', 'LOB', 1),
(646, 41, 'Mambere-KadeÔ', 'MKD', 1),
(647, 41, 'Mbomou', 'MBO', 1),
(648, 41, 'Nana-Mambere', 'NMM', 1),
(649, 41, 'Ombella-M\'Poko', 'OMP', 1),
(650, 41, 'Ouaka', 'OUK', 1),
(651, 41, 'Ouham', 'OUH', 1),
(652, 41, 'Ouham-Pende', 'OPE', 1),
(653, 41, 'Vakaga', 'VAK', 1),
(654, 41, 'Nana-Grebizi', 'NGR', 1),
(655, 41, 'Sangha-Mbaere', 'SMB', 1),
(656, 41, 'Bangui', 'BAN', 1),
(657, 42, 'Batha', 'BA', 1),
(658, 42, 'Biltine', 'BI', 1),
(659, 42, 'Borkou-Ennedi-Tibesti', 'BE', 1),
(660, 42, 'Chari-Baguirmi', 'CB', 1),
(661, 42, 'Guera', 'GU', 1),
(662, 42, 'Kanem', 'KA', 1),
(663, 42, 'Lac', 'LA', 1),
(664, 42, 'Logone Occidental', 'LC', 1),
(665, 42, 'Logone Oriental', 'LR', 1),
(666, 42, 'Mayo-Kebbi', 'MK', 1),
(667, 42, 'Moyen-Chari', 'MC', 1),
(668, 42, 'Ouaddai', 'OU', 1),
(669, 42, 'Salamat', 'SA', 1),
(670, 42, 'Tandjile', 'TA', 1),
(671, 43, 'Aisen del General Carlos Ibanez', 'AI', 1),
(672, 43, 'Antofagasta', 'AN', 1),
(673, 43, 'Araucania', 'AR', 1),
(674, 43, 'Atacama', 'AT', 1),
(675, 43, 'Bio-Bio', 'BI', 1),
(676, 43, 'Coquimbo', 'CO', 1),
(677, 43, 'Libertador General Bernardo O\'Higgins', 'LI', 1),
(678, 43, 'Los Lagos', 'LL', 1),
(679, 43, 'Magallanes y de la Antartica Chilena', 'MA', 1),
(680, 43, 'Maule', 'ML', 1),
(681, 43, 'Region Metropolitana', 'RM', 1),
(682, 43, 'Tarapaca', 'TA', 1),
(683, 43, 'Valparaiso', 'VS', 1),
(684, 44, 'Anhui', 'AN', 1),
(685, 44, 'Beijing', 'BE', 1),
(686, 44, 'Chongqing', 'CH', 1),
(687, 44, 'Fujian', 'FU', 1),
(688, 44, 'Gansu', 'GA', 1),
(689, 44, 'Guangdong', 'GU', 1),
(690, 44, 'Guangxi', 'GX', 1),
(691, 44, 'Guizhou', 'GZ', 1),
(692, 44, 'Hainan', 'HA', 1),
(693, 44, 'Hebei', 'HB', 1),
(694, 44, 'Heilongjiang', 'HL', 1),
(695, 44, 'Henan', 'HE', 1),
(696, 44, 'Hong Kong', 'HK', 1),
(697, 44, 'Hubei', 'HU', 1),
(698, 44, 'Hunan', 'HN', 1),
(699, 44, 'Inner Mongolia', 'IM', 1),
(700, 44, 'Jiangsu', 'JI', 1),
(701, 44, 'Jiangxi', 'JX', 1),
(702, 44, 'Jilin', 'JL', 1),
(703, 44, 'Liaoning', 'LI', 1),
(704, 44, 'Macau', 'MA', 1),
(705, 44, 'Ningxia', 'NI', 1),
(706, 44, 'Shaanxi', 'SH', 1),
(707, 44, 'Shandong', 'SA', 1),
(708, 44, 'Shanghai', 'SG', 1),
(709, 44, 'Shanxi', 'SX', 1),
(710, 44, 'Sichuan', 'SI', 1),
(711, 44, 'Tianjin', 'TI', 1),
(712, 44, 'Xinjiang', 'XI', 1),
(713, 44, 'Yunnan', 'YU', 1),
(714, 44, 'Zhejiang', 'ZH', 1),
(715, 46, 'Direction Island', 'D', 1),
(716, 46, 'Home Island', 'H', 1),
(717, 46, 'Horsburgh Island', 'O', 1),
(718, 46, 'South Island', 'S', 1),
(719, 46, 'West Island', 'W', 1),
(720, 47, 'Amazonas', 'AMZ', 1),
(721, 47, 'Antioquia', 'ANT', 1),
(722, 47, 'Arauca', 'ARA', 1),
(723, 47, 'Atlantico', 'ATL', 1),
(724, 47, 'Bogota D.C.', 'BDC', 1),
(725, 47, 'Bolivar', 'BOL', 1),
(726, 47, 'Boyaca', 'BOY', 1),
(727, 47, 'Caldas', 'CAL', 1),
(728, 47, 'Caqueta', 'CAQ', 1),
(729, 47, 'Casanare', 'CAS', 1),
(730, 47, 'Cauca', 'CAU', 1),
(731, 47, 'Cesar', 'CES', 1),
(732, 47, 'Choco', 'CHO', 1),
(733, 47, 'Cordoba', 'COR', 1),
(734, 47, 'Cundinamarca', 'CAM', 1),
(735, 47, 'Guainia', 'GNA', 1),
(736, 47, 'Guajira', 'GJR', 1),
(737, 47, 'Guaviare', 'GVR', 1),
(738, 47, 'Huila', 'HUI', 1),
(739, 47, 'Magdalena', 'MAG', 1),
(740, 47, 'Meta', 'MET', 1),
(741, 47, 'Narino', 'NAR', 1),
(742, 47, 'Norte de Santander', 'NDS', 1),
(743, 47, 'Putumayo', 'PUT', 1),
(744, 47, 'Quindio', 'QUI', 1),
(745, 47, 'Risaralda', 'RIS', 1),
(746, 47, 'San Andres y Providencia', 'SAP', 1),
(747, 47, 'Santander', 'SAN', 1),
(748, 47, 'Sucre', 'SUC', 1),
(749, 47, 'Tolima', 'TOL', 1),
(750, 47, 'Valle del Cauca', 'VDC', 1),
(751, 47, 'Vaupes', 'VAU', 1),
(752, 47, 'Vichada', 'VIC', 1),
(753, 48, 'Grande Comore', 'G', 1),
(754, 48, 'Anjouan', 'A', 1),
(755, 48, 'Moheli', 'M', 1),
(756, 49, 'Bouenza', 'BO', 1),
(757, 49, 'Brazzaville', 'BR', 1),
(758, 49, 'Cuvette', 'CU', 1),
(759, 49, 'Cuvette-Ouest', 'CO', 1),
(760, 49, 'Kouilou', 'KO', 1),
(761, 49, 'Lekoumou', 'LE', 1),
(762, 49, 'Likouala', 'LI', 1),
(763, 49, 'Niari', 'NI', 1),
(764, 49, 'Plateaux', 'PL', 1),
(765, 49, 'Pool', 'PO', 1),
(766, 49, 'Sangha', 'SA', 1),
(767, 50, 'Pukapuka', 'PU', 1),
(768, 50, 'Rakahanga', 'RK', 1),
(769, 50, 'Manihiki', 'MK', 1),
(770, 50, 'Penrhyn', 'PE', 1),
(771, 50, 'Nassau Island', 'NI', 1),
(772, 50, 'Surwarrow', 'SU', 1),
(773, 50, 'Palmerston', 'PA', 1),
(774, 50, 'Aitutaki', 'AI', 1),
(775, 50, 'Manuae', 'MA', 1),
(776, 50, 'Takutea', 'TA', 1),
(777, 50, 'Mitiaro', 'MT', 1),
(778, 50, 'Atiu', 'AT', 1),
(779, 50, 'Mauke', 'MU', 1),
(780, 50, 'Rarotonga', 'RR', 1),
(781, 50, 'Mangaia', 'MG', 1),
(782, 51, 'Alajuela', 'AL', 1),
(783, 51, 'Cartago', 'CA', 1),
(784, 51, 'Guanacaste', 'GU', 1),
(785, 51, 'Heredia', 'HE', 1),
(786, 51, 'Limon', 'LI', 1),
(787, 51, 'Puntarenas', 'PU', 1),
(788, 51, 'San Jose', 'SJ', 1),
(789, 52, 'Abengourou', 'ABE', 1),
(790, 52, 'Abidjan', 'ABI', 1),
(791, 52, 'Aboisso', 'ABO', 1),
(792, 52, 'Adiake', 'ADI', 1),
(793, 52, 'Adzope', 'ADZ', 1),
(794, 52, 'Agboville', 'AGB', 1),
(795, 52, 'Agnibilekrou', 'AGN', 1),
(796, 52, 'Alepe', 'ALE', 1),
(797, 52, 'Bocanda', 'BOC', 1),
(798, 52, 'Bangolo', 'BAN', 1),
(799, 52, 'Beoumi', 'BEO', 1),
(800, 52, 'Biankouma', 'BIA', 1),
(801, 52, 'Bondoukou', 'BDK', 1),
(802, 52, 'Bongouanou', 'BGN', 1),
(803, 52, 'Bouafle', 'BFL', 1),
(804, 52, 'Bouake', 'BKE', 1),
(805, 52, 'Bouna', 'BNA', 1),
(806, 52, 'Boundiali', 'BDL', 1),
(807, 52, 'Dabakala', 'DKL', 1),
(808, 52, 'Dabou', 'DBU', 1),
(809, 52, 'Daloa', 'DAL', 1),
(810, 52, 'Danane', 'DAN', 1),
(811, 52, 'Daoukro', 'DAO', 1),
(812, 52, 'Dimbokro', 'DIM', 1),
(813, 52, 'Divo', 'DIV', 1),
(814, 52, 'Duekoue', 'DUE', 1),
(815, 52, 'Ferkessedougou', 'FER', 1),
(816, 52, 'Gagnoa', 'GAG', 1),
(817, 52, 'Grand-Bassam', 'GBA', 1),
(818, 52, 'Grand-Lahou', 'GLA', 1),
(819, 52, 'Guiglo', 'GUI', 1),
(820, 52, 'Issia', 'ISS', 1),
(821, 52, 'Jacqueville', 'JAC', 1),
(822, 52, 'Katiola', 'KAT', 1),
(823, 52, 'Korhogo', 'KOR', 1),
(824, 52, 'Lakota', 'LAK', 1),
(825, 52, 'Man', 'MAN', 1),
(826, 52, 'Mankono', 'MKN', 1),
(827, 52, 'Mbahiakro', 'MBA', 1),
(828, 52, 'Odienne', 'ODI', 1),
(829, 52, 'Oume', 'OUM', 1),
(830, 52, 'Sakassou', 'SAK', 1),
(831, 52, 'San-Pedro', 'SPE', 1),
(832, 52, 'Sassandra', 'SAS', 1),
(833, 52, 'Seguela', 'SEG', 1),
(834, 52, 'Sinfra', 'SIN', 1),
(835, 52, 'Soubre', 'SOU', 1),
(836, 52, 'Tabou', 'TAB', 1),
(837, 52, 'Tanda', 'TAN', 1),
(838, 52, 'Tiebissou', 'TIE', 1),
(839, 52, 'Tingrela', 'TIN', 1),
(840, 52, 'Tiassale', 'TIA', 1),
(841, 52, 'Touba', 'TBA', 1),
(842, 52, 'Toulepleu', 'TLP', 1),
(843, 52, 'Toumodi', 'TMD', 1),
(844, 52, 'Vavoua', 'VAV', 1),
(845, 52, 'Yamoussoukro', 'YAM', 1),
(846, 52, 'Zuenoula', 'ZUE', 1),
(847, 53, 'Bjelovarsko-bilogorska', 'BB', 1),
(848, 53, 'Grad Zagreb', 'GZ', 1),
(849, 53, 'Dubrovačko-neretvanska', 'DN', 1),
(850, 53, 'Istarska', 'IS', 1),
(851, 53, 'Karlovačka', 'KA', 1),
(852, 53, 'Koprivničko-križevačka', 'KK', 1),
(853, 53, 'Krapinsko-zagorska', 'KZ', 1),
(854, 53, 'Ličko-senjska', 'LS', 1),
(855, 53, 'Međimurska', 'ME', 1),
(856, 53, 'Osječko-baranjska', 'OB', 1),
(857, 53, 'Požeško-slavonska', 'PS', 1),
(858, 53, 'Primorsko-goranska', 'PG', 1),
(859, 53, 'Šibensko-kninska', 'SK', 1),
(860, 53, 'Sisačko-moslavačka', 'SM', 1),
(861, 53, 'Brodsko-posavska', 'BP', 1),
(862, 53, 'Splitsko-dalmatinska', 'SD', 1),
(863, 53, 'Varaždinska', 'VA', 1),
(864, 53, 'Virovitičko-podravska', 'VP', 1),
(865, 53, 'Vukovarsko-srijemska', 'VS', 1),
(866, 53, 'Zadarska', 'ZA', 1),
(867, 53, 'Zagrebačka', 'ZG', 1),
(868, 54, 'Camaguey', 'CA', 1),
(869, 54, 'Ciego de Avila', 'CD', 1),
(870, 54, 'Cienfuegos', 'CI', 1),
(871, 54, 'Ciudad de La Habana', 'CH', 1),
(872, 54, 'Granma', 'GR', 1),
(873, 54, 'Guantanamo', 'GU', 1),
(874, 54, 'Holguin', 'HO', 1),
(875, 54, 'Isla de la Juventud', 'IJ', 1),
(876, 54, 'La Habana', 'LH', 1),
(877, 54, 'Las Tunas', 'LT', 1),
(878, 54, 'Matanzas', 'MA', 1),
(879, 54, 'Pinar del Rio', 'PR', 1),
(880, 54, 'Sancti Spiritus', 'SS', 1),
(881, 54, 'Santiago de Cuba', 'SC', 1),
(882, 54, 'Villa Clara', 'VC', 1),
(883, 55, 'Famagusta', 'F', 1),
(884, 55, 'Kyrenia', 'K', 1),
(885, 55, 'Larnaca', 'A', 1),
(886, 55, 'Limassol', 'I', 1),
(887, 55, 'Nicosia', 'N', 1),
(888, 55, 'Paphos', 'P', 1),
(889, 56, 'Ústecký', 'U', 1),
(890, 56, 'Jihočeský', 'C', 1),
(891, 56, 'Jihomoravský', 'B', 1),
(892, 56, 'Karlovarský', 'K', 1),
(893, 56, 'Královehradecký', 'H', 1),
(894, 56, 'Liberecký', 'L', 1),
(895, 56, 'Moravskoslezský', 'T', 1),
(896, 56, 'Olomoucký', 'M', 1),
(897, 56, 'Pardubický', 'E', 1),
(898, 56, 'Plzeňský', 'P', 1),
(899, 56, 'Praha', 'A', 1),
(900, 56, 'Středočeský', 'S', 1),
(901, 56, 'Vysočina', 'J', 1),
(902, 56, 'Zlínský', 'Z', 1),
(903, 57, 'Arhus', 'AR', 1),
(904, 57, 'Bornholm', 'BH', 1),
(905, 57, 'Copenhagen', 'CO', 1),
(906, 57, 'Faroe Islands', 'FO', 1),
(907, 57, 'Frederiksborg', 'FR', 1),
(908, 57, 'Fyn', 'FY', 1),
(909, 57, 'Kobenhavn', 'KO', 1),
(910, 57, 'Nordjylland', 'NO', 1),
(911, 57, 'Ribe', 'RI', 1),
(912, 57, 'Ringkobing', 'RK', 1),
(913, 57, 'Roskilde', 'RO', 1),
(914, 57, 'Sonderjylland', 'SO', 1),
(915, 57, 'Storstrom', 'ST', 1),
(916, 57, 'Vejle', 'VK', 1),
(917, 57, 'Vestj&aelig;lland', 'VJ', 1),
(918, 57, 'Viborg', 'VB', 1),
(919, 58, '\'Ali Sabih', 'S', 1),
(920, 58, 'Dikhil', 'K', 1),
(921, 58, 'Djibouti', 'J', 1),
(922, 58, 'Obock', 'O', 1),
(923, 58, 'Tadjoura', 'T', 1),
(924, 59, 'Saint Andrew Parish', 'AND', 1),
(925, 59, 'Saint David Parish', 'DAV', 1),
(926, 59, 'Saint George Parish', 'GEO', 1),
(927, 59, 'Saint John Parish', 'JOH', 1),
(928, 59, 'Saint Joseph Parish', 'JOS', 1),
(929, 59, 'Saint Luke Parish', 'LUK', 1),
(930, 59, 'Saint Mark Parish', 'MAR', 1),
(931, 59, 'Saint Patrick Parish', 'PAT', 1),
(932, 59, 'Saint Paul Parish', 'PAU', 1),
(933, 59, 'Saint Peter Parish', 'PET', 1),
(934, 60, 'Distrito Nacional', 'DN', 1),
(935, 60, 'Azua', 'AZ', 1),
(936, 60, 'Baoruco', 'BC', 1),
(937, 60, 'Barahona', 'BH', 1),
(938, 60, 'Dajabon', 'DJ', 1),
(939, 60, 'Duarte', 'DU', 1),
(940, 60, 'Elias Pina', 'EL', 1),
(941, 60, 'El Seybo', 'SY', 1),
(942, 60, 'Espaillat', 'ET', 1),
(943, 60, 'Hato Mayor', 'HM', 1),
(944, 60, 'Independencia', 'IN', 1),
(945, 60, 'La Altagracia', 'AL', 1),
(946, 60, 'La Romana', 'RO', 1),
(947, 60, 'La Vega', 'VE', 1),
(948, 60, 'Maria Trinidad Sanchez', 'MT', 1),
(949, 60, 'Monsenor Nouel', 'MN', 1),
(950, 60, 'Monte Cristi', 'MC', 1),
(951, 60, 'Monte Plata', 'MP', 1),
(952, 60, 'Pedernales', 'PD', 1),
(953, 60, 'Peravia (Bani)', 'PR', 1),
(954, 60, 'Puerto Plata', 'PP', 1),
(955, 60, 'Salcedo', 'SL', 1),
(956, 60, 'Samana', 'SM', 1),
(957, 60, 'Sanchez Ramirez', 'SH', 1),
(958, 60, 'San Cristobal', 'SC', 1),
(959, 60, 'San Jose de Ocoa', 'JO', 1),
(960, 60, 'San Juan', 'SJ', 1),
(961, 60, 'San Pedro de Macoris', 'PM', 1),
(962, 60, 'Santiago', 'SA', 1),
(963, 60, 'Santiago Rodriguez', 'ST', 1),
(964, 60, 'Santo Domingo', 'SD', 1),
(965, 60, 'Valverde', 'VA', 1),
(966, 61, 'Aileu', 'AL', 1),
(967, 61, 'Ainaro', 'AN', 1),
(968, 61, 'Baucau', 'BA', 1),
(969, 61, 'Bobonaro', 'BO', 1),
(970, 61, 'Cova Lima', 'CO', 1),
(971, 61, 'Dili', 'DI', 1),
(972, 61, 'Ermera', 'ER', 1),
(973, 61, 'Lautem', 'LA', 1),
(974, 61, 'Liquica', 'LI', 1),
(975, 61, 'Manatuto', 'MT', 1),
(976, 61, 'Manufahi', 'MF', 1),
(977, 61, 'Oecussi', 'OE', 1),
(978, 61, 'Viqueque', 'VI', 1),
(979, 62, 'Azuay', 'AZU', 1),
(980, 62, 'Bolivar', 'BOL', 1),
(981, 62, 'Ca&ntilde;ar', 'CAN', 1),
(982, 62, 'Carchi', 'CAR', 1),
(983, 62, 'Chimborazo', 'CHI', 1),
(984, 62, 'Cotopaxi', 'COT', 1),
(985, 62, 'El Oro', 'EOR', 1),
(986, 62, 'Esmeraldas', 'ESM', 1),
(987, 62, 'Gal&aacute;pagos', 'GPS', 1),
(988, 62, 'Guayas', 'GUA', 1),
(989, 62, 'Imbabura', 'IMB', 1),
(990, 62, 'Loja', 'LOJ', 1),
(991, 62, 'Los Rios', 'LRO', 1),
(992, 62, 'Manab&iacute;', 'MAN', 1),
(993, 62, 'Morona Santiago', 'MSA', 1),
(994, 62, 'Napo', 'NAP', 1),
(995, 62, 'Orellana', 'ORE', 1),
(996, 62, 'Pastaza', 'PAS', 1),
(997, 62, 'Pichincha', 'PIC', 1),
(998, 62, 'Sucumb&iacute;os', 'SUC', 1),
(999, 62, 'Tungurahua', 'TUN', 1),
(1000, 62, 'Zamora Chinchipe', 'ZCH', 1),
(1001, 63, 'Ad Daqahliyah', 'DHY', 1),
(1002, 63, 'Al Bahr al Ahmar', 'BAM', 1),
(1003, 63, 'Al Buhayrah', 'BHY', 1),
(1004, 63, 'Al Fayyum', 'FYM', 1),
(1005, 63, 'Al Gharbiyah', 'GBY', 1),
(1006, 63, 'Al Iskandariyah', 'IDR', 1),
(1007, 63, 'Al Isma\'iliyah', 'IML', 1),
(1008, 63, 'Al Jizah', 'JZH', 1),
(1009, 63, 'Al Minufiyah', 'MFY', 1),
(1010, 63, 'Al Minya', 'MNY', 1),
(1011, 63, 'Al Qahirah', 'QHR', 1),
(1012, 63, 'Al Qalyubiyah', 'QLY', 1),
(1013, 63, 'Al Wadi al Jadid', 'WJD', 1),
(1014, 63, 'Ash Sharqiyah', 'SHQ', 1),
(1015, 63, 'As Suways', 'SWY', 1),
(1016, 63, 'Aswan', 'ASW', 1),
(1017, 63, 'Asyut', 'ASY', 1),
(1018, 63, 'Bani Suwayf', 'BSW', 1),
(1019, 63, 'Bur Sa\'id', 'BSD', 1),
(1020, 63, 'Dumyat', 'DMY', 1),
(1021, 63, 'Janub Sina\'', 'JNS', 1),
(1022, 63, 'Kafr ash Shaykh', 'KSH', 1),
(1023, 63, 'Matruh', 'MAT', 1),
(1024, 63, 'Qina', 'QIN', 1),
(1025, 63, 'Shamal Sina\'', 'SHS', 1),
(1026, 63, 'Suhaj', 'SUH', 1),
(1027, 64, 'Ahuachapan', 'AH', 1),
(1028, 64, 'Cabanas', 'CA', 1),
(1029, 64, 'Chalatenango', 'CH', 1),
(1030, 64, 'Cuscatlan', 'CU', 1),
(1031, 64, 'La Libertad', 'LB', 1),
(1032, 64, 'La Paz', 'PZ', 1),
(1033, 64, 'La Union', 'UN', 1),
(1034, 64, 'Morazan', 'MO', 1),
(1035, 64, 'San Miguel', 'SM', 1),
(1036, 64, 'San Salvador', 'SS', 1),
(1037, 64, 'San Vicente', 'SV', 1),
(1038, 64, 'Santa Ana', 'SA', 1),
(1039, 64, 'Sonsonate', 'SO', 1),
(1040, 64, 'Usulutan', 'US', 1),
(1041, 65, 'Provincia Annobon', 'AN', 1),
(1042, 65, 'Provincia Bioko Norte', 'BN', 1),
(1043, 65, 'Provincia Bioko Sur', 'BS', 1),
(1044, 65, 'Provincia Centro Sur', 'CS', 1),
(1045, 65, 'Provincia Kie-Ntem', 'KN', 1),
(1046, 65, 'Provincia Litoral', 'LI', 1),
(1047, 65, 'Provincia Wele-Nzas', 'WN', 1),
(1048, 66, 'Central (Maekel)', 'MA', 1),
(1049, 66, 'Anseba (Keren)', 'KE', 1),
(1050, 66, 'Southern Red Sea (Debub-Keih-Bahri)', 'DK', 1),
(1051, 66, 'Northern Red Sea (Semien-Keih-Bahri)', 'SK', 1),
(1052, 66, 'Southern (Debub)', 'DE', 1),
(1053, 66, 'Gash-Barka (Barentu)', 'BR', 1),
(1054, 67, 'Harjumaa (Tallinn)', 'HA', 1),
(1055, 67, 'Hiiumaa (Kardla)', 'HI', 1),
(1056, 67, 'Ida-Virumaa (Johvi)', 'IV', 1),
(1057, 67, 'Jarvamaa (Paide)', 'JA', 1),
(1058, 67, 'Jogevamaa (Jogeva)', 'JO', 1),
(1059, 67, 'Laane-Virumaa (Rakvere)', 'LV', 1),
(1060, 67, 'Laanemaa (Haapsalu)', 'LA', 1),
(1061, 67, 'Parnumaa (Parnu)', 'PA', 1),
(1062, 67, 'Polvamaa (Polva)', 'PO', 1),
(1063, 67, 'Raplamaa (Rapla)', 'RA', 1),
(1064, 67, 'Saaremaa (Kuessaare)', 'SA', 1),
(1065, 67, 'Tartumaa (Tartu)', 'TA', 1),
(1066, 67, 'Valgamaa (Valga)', 'VA', 1),
(1067, 67, 'Viljandimaa (Viljandi)', 'VI', 1),
(1068, 67, 'Vorumaa (Voru)', 'VO', 1),
(1069, 68, 'Afar', 'AF', 1),
(1070, 68, 'Amhara', 'AH', 1),
(1071, 68, 'Benishangul-Gumaz', 'BG', 1),
(1072, 68, 'Gambela', 'GB', 1),
(1073, 68, 'Hariai', 'HR', 1),
(1074, 68, 'Oromia', 'OR', 1),
(1075, 68, 'Somali', 'SM', 1),
(1076, 68, 'Southern Nations - Nationalities and Peoples Region', 'SN', 1),
(1077, 68, 'Tigray', 'TG', 1),
(1078, 68, 'Addis Ababa', 'AA', 1),
(1079, 68, 'Dire Dawa', 'DD', 1),
(1080, 71, 'Central Division', 'C', 1),
(1081, 71, 'Northern Division', 'N', 1),
(1082, 71, 'Eastern Division', 'E', 1),
(1083, 71, 'Western Division', 'W', 1),
(1084, 71, 'Rotuma', 'R', 1),
(1085, 72, 'Ahvenanmaan lääni', 'AL', 1),
(1086, 72, 'Etelä-Suomen lääni', 'ES', 1),
(1087, 72, 'Itä-Suomen lääni', 'IS', 1),
(1088, 72, 'Länsi-Suomen lääni', 'LS', 1),
(1089, 72, 'Lapin lääni', 'LA', 1),
(1090, 72, 'Oulun lääni', 'OU', 1),
(1114, 74, 'Ain', '01', 1),
(1115, 74, 'Aisne', '02', 1),
(1116, 74, 'Allier', '03', 1),
(1117, 74, 'Alpes de Haute Provence', '04', 1),
(1118, 74, 'Hautes-Alpes', '05', 1),
(1119, 74, 'Alpes Maritimes', '06', 1),
(1120, 74, 'Ard&egrave;che', '07', 1),
(1121, 74, 'Ardennes', '08', 1),
(1122, 74, 'Ari&egrave;ge', '09', 1),
(1123, 74, 'Aube', '10', 1),
(1124, 74, 'Aude', '11', 1),
(1125, 74, 'Aveyron', '12', 1),
(1126, 74, 'Bouches du Rh&ocirc;ne', '13', 1),
(1127, 74, 'Calvados', '14', 1),
(1128, 74, 'Cantal', '15', 1),
(1129, 74, 'Charente', '16', 1),
(1130, 74, 'Charente Maritime', '17', 1),
(1131, 74, 'Cher', '18', 1),
(1132, 74, 'Corr&egrave;ze', '19', 1),
(1133, 74, 'Corse du Sud', '2A', 1),
(1134, 74, 'Haute Corse', '2B', 1),
(1135, 74, 'C&ocirc;te d&#039;or', '21', 1),
(1136, 74, 'C&ocirc;tes d&#039;Armor', '22', 1),
(1137, 74, 'Creuse', '23', 1),
(1138, 74, 'Dordogne', '24', 1),
(1139, 74, 'Doubs', '25', 1),
(1140, 74, 'Dr&ocirc;me', '26', 1),
(1141, 74, 'Eure', '27', 1),
(1142, 74, 'Eure et Loir', '28', 1),
(1143, 74, 'Finist&egrave;re', '29', 1),
(1144, 74, 'Gard', '30', 1),
(1145, 74, 'Haute Garonne', '31', 1),
(1146, 74, 'Gers', '32', 1),
(1147, 74, 'Gironde', '33', 1),
(1148, 74, 'H&eacute;rault', '34', 1),
(1149, 74, 'Ille et Vilaine', '35', 1),
(1150, 74, 'Indre', '36', 1),
(1151, 74, 'Indre et Loire', '37', 1),
(1152, 74, 'Is&eacute;re', '38', 1),
(1153, 74, 'Jura', '39', 1),
(1154, 74, 'Landes', '40', 1),
(1155, 74, 'Loir et Cher', '41', 1),
(1156, 74, 'Loire', '42', 1),
(1157, 74, 'Haute Loire', '43', 1),
(1158, 74, 'Loire Atlantique', '44', 1),
(1159, 74, 'Loiret', '45', 1),
(1160, 74, 'Lot', '46', 1),
(1161, 74, 'Lot et Garonne', '47', 1),
(1162, 74, 'Loz&egrave;re', '48', 1),
(1163, 74, 'Maine et Loire', '49', 1),
(1164, 74, 'Manche', '50', 1),
(1165, 74, 'Marne', '51', 1),
(1166, 74, 'Haute Marne', '52', 1),
(1167, 74, 'Mayenne', '53', 1),
(1168, 74, 'Meurthe et Moselle', '54', 1),
(1169, 74, 'Meuse', '55', 1),
(1170, 74, 'Morbihan', '56', 1),
(1171, 74, 'Moselle', '57', 1),
(1172, 74, 'Ni&egrave;vre', '58', 1),
(1173, 74, 'Nord', '59', 1),
(1174, 74, 'Oise', '60', 1),
(1175, 74, 'Orne', '61', 1),
(1176, 74, 'Pas de Calais', '62', 1),
(1177, 74, 'Puy de D&ocirc;me', '63', 1),
(1178, 74, 'Pyr&eacute;n&eacute;es Atlantiques', '64', 1),
(1179, 74, 'Hautes Pyr&eacute;n&eacute;es', '65', 1),
(1180, 74, 'Pyr&eacute;n&eacute;es Orientales', '66', 1),
(1181, 74, 'Bas Rhin', '67', 1),
(1182, 74, 'Haut Rhin', '68', 1),
(1183, 74, 'Rh&ocirc;ne', '69', 1),
(1184, 74, 'Haute Sa&ocirc;ne', '70', 1),
(1185, 74, 'Sa&ocirc;ne et Loire', '71', 1),
(1186, 74, 'Sarthe', '72', 1),
(1187, 74, 'Savoie', '73', 1),
(1188, 74, 'Haute Savoie', '74', 1),
(1189, 74, 'Paris', '75', 1),
(1190, 74, 'Seine Maritime', '76', 1),
(1191, 74, 'Seine et Marne', '77', 1),
(1192, 74, 'Yvelines', '78', 1),
(1193, 74, 'Deux S&egrave;vres', '79', 1),
(1194, 74, 'Somme', '80', 1),
(1195, 74, 'Tarn', '81', 1),
(1196, 74, 'Tarn et Garonne', '82', 1),
(1197, 74, 'Var', '83', 1),
(1198, 74, 'Vaucluse', '84', 1),
(1199, 74, 'Vend&eacute;e', '85', 1),
(1200, 74, 'Vienne', '86', 1),
(1201, 74, 'Haute Vienne', '87', 1),
(1202, 74, 'Vosges', '88', 1),
(1203, 74, 'Yonne', '89', 1),
(1204, 74, 'Territoire de Belfort', '90', 1),
(1205, 74, 'Essonne', '91', 1),
(1206, 74, 'Hauts de Seine', '92', 1),
(1207, 74, 'Seine St-Denis', '93', 1),
(1208, 74, 'Val de Marne', '94', 1),
(1209, 74, 'Val d\'Oise', '95', 1),
(1210, 76, 'Archipel des Marquises', 'M', 1),
(1211, 76, 'Archipel des Tuamotu', 'T', 1),
(1212, 76, 'Archipel des Tubuai', 'I', 1),
(1213, 76, 'Iles du Vent', 'V', 1),
(1214, 76, 'Iles Sous-le-Vent', 'S', 1),
(1215, 77, 'Iles Crozet', 'C', 1),
(1216, 77, 'Iles Kerguelen', 'K', 1),
(1217, 77, 'Ile Amsterdam', 'A', 1),
(1218, 77, 'Ile Saint-Paul', 'P', 1),
(1219, 77, 'Adelie Land', 'D', 1),
(1220, 78, 'Estuaire', 'ES', 1),
(1221, 78, 'Haut-Ogooue', 'HO', 1),
(1222, 78, 'Moyen-Ogooue', 'MO', 1),
(1223, 78, 'Ngounie', 'NG', 1),
(1224, 78, 'Nyanga', 'NY', 1),
(1225, 78, 'Ogooue-Ivindo', 'OI', 1),
(1226, 78, 'Ogooue-Lolo', 'OL', 1),
(1227, 78, 'Ogooue-Maritime', 'OM', 1),
(1228, 78, 'Woleu-Ntem', 'WN', 1),
(1229, 79, 'Banjul', 'BJ', 1),
(1230, 79, 'Basse', 'BS', 1),
(1231, 79, 'Brikama', 'BR', 1),
(1232, 79, 'Janjangbure', 'JA', 1),
(1233, 79, 'Kanifeng', 'KA', 1),
(1234, 79, 'Kerewan', 'KE', 1),
(1235, 79, 'Kuntaur', 'KU', 1),
(1236, 79, 'Mansakonko', 'MA', 1),
(1237, 79, 'Lower River', 'LR', 1),
(1238, 79, 'Central River', 'CR', 1),
(1239, 79, 'North Bank', 'NB', 1),
(1240, 79, 'Upper River', 'UR', 1),
(1241, 79, 'Western', 'WE', 1),
(1242, 80, 'Abkhazia', 'AB', 1),
(1243, 80, 'Ajaria', 'AJ', 1),
(1244, 80, 'Tbilisi', 'TB', 1),
(1245, 80, 'Guria', 'GU', 1),
(1246, 80, 'Imereti', 'IM', 1),
(1247, 80, 'Kakheti', 'KA', 1),
(1248, 80, 'Kvemo Kartli', 'KK', 1),
(1249, 80, 'Mtskheta-Mtianeti', 'MM', 1),
(1250, 80, 'Racha Lechkhumi and Kvemo Svanet', 'RL', 1),
(1251, 80, 'Samegrelo-Zemo Svaneti', 'SZ', 1),
(1252, 80, 'Samtskhe-Javakheti', 'SJ', 1),
(1253, 80, 'Shida Kartli', 'SK', 1),
(1254, 81, 'Baden-Württemberg', 'BAW', 1),
(1255, 81, 'Bayern', 'BAY', 1),
(1256, 81, 'Berlin', 'BER', 1),
(1257, 81, 'Brandenburg', 'BRG', 1),
(1258, 81, 'Bremen', 'BRE', 1),
(1259, 81, 'Hamburg', 'HAM', 1),
(1260, 81, 'Hessen', 'HES', 1),
(1261, 81, 'Mecklenburg-Vorpommern', 'MEC', 1),
(1262, 81, 'Niedersachsen', 'NDS', 1),
(1263, 81, 'Nordrhein-Westfalen', 'NRW', 1),
(1264, 81, 'Rheinland-Pfalz', 'RHE', 1),
(1265, 81, 'Saarland', 'SAR', 1),
(1266, 81, 'Sachsen', 'SAS', 1),
(1267, 81, 'Sachsen-Anhalt', 'SAC', 1),
(1268, 81, 'Schleswig-Holstein', 'SCN', 1),
(1269, 81, 'Thüringen', 'THE', 1),
(1270, 82, 'Ashanti Region', 'AS', 1),
(1271, 82, 'Brong-Ahafo Region', 'BA', 1),
(1272, 82, 'Central Region', 'CE', 1),
(1273, 82, 'Eastern Region', 'EA', 1),
(1274, 82, 'Greater Accra Region', 'GA', 1),
(1275, 82, 'Northern Region', 'NO', 1),
(1276, 82, 'Upper East Region', 'UE', 1),
(1277, 82, 'Upper West Region', 'UW', 1),
(1278, 82, 'Volta Region', 'VO', 1),
(1279, 82, 'Western Region', 'WE', 1),
(1280, 84, 'Attica', 'AT', 1),
(1281, 84, 'Central Greece', 'CN', 1),
(1282, 84, 'Central Macedonia', 'CM', 1),
(1283, 84, 'Crete', 'CR', 1),
(1284, 84, 'East Macedonia and Thrace', 'EM', 1),
(1285, 84, 'Epirus', 'EP', 1),
(1286, 84, 'Ionian Islands', 'II', 1),
(1287, 84, 'North Aegean', 'NA', 1),
(1288, 84, 'Peloponnesos', 'PP', 1),
(1289, 84, 'South Aegean', 'SA', 1),
(1290, 84, 'Thessaly', 'TH', 1),
(1291, 84, 'West Greece', 'WG', 1),
(1292, 84, 'West Macedonia', 'WM', 1),
(1293, 85, 'Avannaa', 'A', 1),
(1294, 85, 'Tunu', 'T', 1),
(1295, 85, 'Kitaa', 'K', 1),
(1296, 86, 'Saint Andrew', 'A', 1),
(1297, 86, 'Saint David', 'D', 1),
(1298, 86, 'Saint George', 'G', 1),
(1299, 86, 'Saint John', 'J', 1),
(1300, 86, 'Saint Mark', 'M', 1),
(1301, 86, 'Saint Patrick', 'P', 1),
(1302, 86, 'Carriacou', 'C', 1),
(1303, 86, 'Petit Martinique', 'Q', 1),
(1304, 89, 'Alta Verapaz', 'AV', 1),
(1305, 89, 'Baja Verapaz', 'BV', 1),
(1306, 89, 'Chimaltenango', 'CM', 1),
(1307, 89, 'Chiquimula', 'CQ', 1),
(1308, 89, 'El Peten', 'PE', 1),
(1309, 89, 'El Progreso', 'PR', 1),
(1310, 89, 'El Quiche', 'QC', 1),
(1311, 89, 'Escuintla', 'ES', 1),
(1312, 89, 'Guatemala', 'GU', 1),
(1313, 89, 'Huehuetenango', 'HU', 1),
(1314, 89, 'Izabal', 'IZ', 1),
(1315, 89, 'Jalapa', 'JA', 1),
(1316, 89, 'Jutiapa', 'JU', 1),
(1317, 89, 'Quetzaltenango', 'QZ', 1),
(1318, 89, 'Retalhuleu', 'RE', 1),
(1319, 89, 'Sacatepequez', 'ST', 1),
(1320, 89, 'San Marcos', 'SM', 1),
(1321, 89, 'Santa Rosa', 'SR', 1),
(1322, 89, 'Solola', 'SO', 1),
(1323, 89, 'Suchitepequez', 'SU', 1),
(1324, 89, 'Totonicapan', 'TO', 1),
(1325, 89, 'Zacapa', 'ZA', 1),
(1326, 90, 'Conakry', 'CNK', 1),
(1327, 90, 'Beyla', 'BYL', 1),
(1328, 90, 'Boffa', 'BFA', 1),
(1329, 90, 'Boke', 'BOK', 1),
(1330, 90, 'Coyah', 'COY', 1),
(1331, 90, 'Dabola', 'DBL', 1),
(1332, 90, 'Dalaba', 'DLB', 1),
(1333, 90, 'Dinguiraye', 'DGR', 1),
(1334, 90, 'Dubreka', 'DBR', 1),
(1335, 90, 'Faranah', 'FRN', 1),
(1336, 90, 'Forecariah', 'FRC', 1),
(1337, 90, 'Fria', 'FRI', 1),
(1338, 90, 'Gaoual', 'GAO', 1),
(1339, 90, 'Gueckedou', 'GCD', 1),
(1340, 90, 'Kankan', 'KNK', 1),
(1341, 90, 'Kerouane', 'KRN', 1),
(1342, 90, 'Kindia', 'KND', 1),
(1343, 90, 'Kissidougou', 'KSD', 1),
(1344, 90, 'Koubia', 'KBA', 1),
(1345, 90, 'Koundara', 'KDA', 1),
(1346, 90, 'Kouroussa', 'KRA', 1),
(1347, 90, 'Labe', 'LAB', 1),
(1348, 90, 'Lelouma', 'LLM', 1),
(1349, 90, 'Lola', 'LOL', 1),
(1350, 90, 'Macenta', 'MCT', 1),
(1351, 90, 'Mali', 'MAL', 1),
(1352, 90, 'Mamou', 'MAM', 1),
(1353, 90, 'Mandiana', 'MAN', 1),
(1354, 90, 'Nzerekore', 'NZR', 1),
(1355, 90, 'Pita', 'PIT', 1),
(1356, 90, 'Siguiri', 'SIG', 1),
(1357, 90, 'Telimele', 'TLM', 1),
(1358, 90, 'Tougue', 'TOG', 1),
(1359, 90, 'Yomou', 'YOM', 1),
(1360, 91, 'Bafata Region', 'BF', 1),
(1361, 91, 'Biombo Region', 'BB', 1),
(1362, 91, 'Bissau Region', 'BS', 1),
(1363, 91, 'Bolama Region', 'BL', 1),
(1364, 91, 'Cacheu Region', 'CA', 1),
(1365, 91, 'Gabu Region', 'GA', 1),
(1366, 91, 'Oio Region', 'OI', 1),
(1367, 91, 'Quinara Region', 'QU', 1),
(1368, 91, 'Tombali Region', 'TO', 1),
(1369, 92, 'Barima-Waini', 'BW', 1),
(1370, 92, 'Cuyuni-Mazaruni', 'CM', 1),
(1371, 92, 'Demerara-Mahaica', 'DM', 1),
(1372, 92, 'East Berbice-Corentyne', 'EC', 1),
(1373, 92, 'Essequibo Islands-West Demerara', 'EW', 1),
(1374, 92, 'Mahaica-Berbice', 'MB', 1),
(1375, 92, 'Pomeroon-Supenaam', 'PM', 1),
(1376, 92, 'Potaro-Siparuni', 'PI', 1),
(1377, 92, 'Upper Demerara-Berbice', 'UD', 1),
(1378, 92, 'Upper Takutu-Upper Essequibo', 'UT', 1),
(1379, 93, 'Artibonite', 'AR', 1),
(1380, 93, 'Centre', 'CE', 1),
(1381, 93, 'Grand\'Anse', 'GA', 1),
(1382, 93, 'Nord', 'ND', 1),
(1383, 93, 'Nord-Est', 'NE', 1),
(1384, 93, 'Nord-Ouest', 'NO', 1),
(1385, 93, 'Ouest', 'OU', 1),
(1386, 93, 'Sud', 'SD', 1),
(1387, 93, 'Sud-Est', 'SE', 1),
(1388, 94, 'Flat Island', 'F', 1),
(1389, 94, 'McDonald Island', 'M', 1),
(1390, 94, 'Shag Island', 'S', 1),
(1391, 94, 'Heard Island', 'H', 1),
(1392, 95, 'Atlantida', 'AT', 1),
(1393, 95, 'Choluteca', 'CH', 1),
(1394, 95, 'Colon', 'CL', 1),
(1395, 95, 'Comayagua', 'CM', 1),
(1396, 95, 'Copan', 'CP', 1),
(1397, 95, 'Cortes', 'CR', 1),
(1398, 95, 'El Paraiso', 'PA', 1),
(1399, 95, 'Francisco Morazan', 'FM', 1),
(1400, 95, 'Gracias a Dios', 'GD', 1),
(1401, 95, 'Intibuca', 'IN', 1),
(1402, 95, 'Islas de la Bahia (Bay Islands)', 'IB', 1),
(1403, 95, 'La Paz', 'PZ', 1),
(1404, 95, 'Lempira', 'LE', 1),
(1405, 95, 'Ocotepeque', 'OC', 1),
(1406, 95, 'Olancho', 'OL', 1),
(1407, 95, 'Santa Barbara', 'SB', 1),
(1408, 95, 'Valle', 'VA', 1),
(1409, 95, 'Yoro', 'YO', 1),
(1410, 96, 'Central and Western Hong Kong Island', 'HCW', 1),
(1411, 96, 'Eastern Hong Kong Island', 'HEA', 1),
(1412, 96, 'Southern Hong Kong Island', 'HSO', 1),
(1413, 96, 'Wan Chai Hong Kong Island', 'HWC', 1),
(1414, 96, 'Kowloon City Kowloon', 'KKC', 1),
(1415, 96, 'Kwun Tong Kowloon', 'KKT', 1),
(1416, 96, 'Sham Shui Po Kowloon', 'KSS', 1),
(1417, 96, 'Wong Tai Sin Kowloon', 'KWT', 1),
(1418, 96, 'Yau Tsim Mong Kowloon', 'KYT', 1),
(1419, 96, 'Islands New Territories', 'NIS', 1),
(1420, 96, 'Kwai Tsing New Territories', 'NKT', 1),
(1421, 96, 'North New Territories', 'NNO', 1),
(1422, 96, 'Sai Kung New Territories', 'NSK', 1),
(1423, 96, 'Sha Tin New Territories', 'NST', 1),
(1424, 96, 'Tai Po New Territories', 'NTP', 1),
(1425, 96, 'Tsuen Wan New Territories', 'NTW', 1),
(1426, 96, 'Tuen Mun New Territories', 'NTM', 1),
(1427, 96, 'Yuen Long New Territories', 'NYL', 1),
(1467, 98, 'Austurland', 'AL', 1),
(1468, 98, 'Hofuoborgarsvaeoi', 'HF', 1),
(1469, 98, 'Norourland eystra', 'NE', 1),
(1470, 98, 'Norourland vestra', 'NV', 1),
(1471, 98, 'Suourland', 'SL', 1),
(1472, 98, 'Suournes', 'SN', 1),
(1473, 98, 'Vestfiroir', 'VF', 1),
(1474, 98, 'Vesturland', 'VL', 1),
(1475, 99, 'Andaman and Nicobar Islands', 'AN', 1),
(1476, 99, 'Andhra Pradesh', 'AP', 1),
(1477, 99, 'Arunachal Pradesh', 'AR', 1),
(1478, 99, 'Assam', 'AS', 1),
(1479, 99, 'Bihar', 'BI', 1),
(1480, 99, 'Chandigarh', 'CH', 1),
(1481, 99, 'Dadra and Nagar Haveli', 'DA', 1),
(1482, 99, 'Daman and Diu', 'DM', 1),
(1483, 99, 'Delhi', 'DE', 1),
(1484, 99, 'Goa', 'GO', 1),
(1485, 99, 'Gujarat', 'GU', 1),
(1486, 99, 'Haryana', 'HA', 1),
(1487, 99, 'Himachal Pradesh', 'HP', 1),
(1488, 99, 'Jammu and Kashmir', 'JA', 1),
(1489, 99, 'Karnataka', 'KA', 1),
(1490, 99, 'Kerala', 'KE', 1),
(1491, 99, 'Lakshadweep Islands', 'LI', 1),
(1492, 99, 'Madhya Pradesh', 'MP', 1),
(1493, 99, 'Maharashtra', 'MA', 1),
(1494, 99, 'Manipur', 'MN', 1),
(1495, 99, 'Meghalaya', 'ME', 1),
(1496, 99, 'Mizoram', 'MI', 1),
(1497, 99, 'Nagaland', 'NA', 1),
(1498, 99, 'Orissa', 'OR', 1),
(1499, 99, 'Puducherry', 'PO', 1),
(1500, 99, 'Punjab', 'PU', 1),
(1501, 99, 'Rajasthan', 'RA', 1),
(1502, 99, 'Sikkim', 'SI', 1),
(1503, 99, 'Tamil Nadu', 'TN', 1),
(1504, 99, 'Tripura', 'TR', 1),
(1505, 99, 'Uttar Pradesh', 'UP', 1),
(1506, 99, 'West Bengal', 'WB', 1),
(1507, 100, 'Aceh', 'AC', 1),
(1508, 100, 'Bali', 'BA', 1),
(1509, 100, 'Banten', 'BT', 1),
(1510, 100, 'Bengkulu', 'BE', 1),
(1511, 100, 'Kalimantan Utara', 'BD', 1),
(1512, 100, 'Gorontalo', 'GO', 1),
(1513, 100, 'Jakarta', 'JK', 1),
(1514, 100, 'Jambi', 'JA', 1),
(1515, 100, 'Jawa Barat', 'JB', 1),
(1516, 100, 'Jawa Tengah', 'JT', 1),
(1517, 100, 'Jawa Timur', 'JI', 1),
(1518, 100, 'Kalimantan Barat', 'KB', 1),
(1519, 100, 'Kalimantan Selatan', 'KS', 1),
(1520, 100, 'Kalimantan Tengah', 'KT', 1),
(1521, 100, 'Kalimantan Timur', 'KI', 1),
(1522, 100, 'Kepulauan Bangka Belitung', 'BB', 1),
(1523, 100, 'Lampung', 'LA', 1),
(1524, 100, 'Maluku', 'MA', 1),
(1525, 100, 'Maluku Utara', 'MU', 1),
(1526, 100, 'Nusa Tenggara Barat', 'NB', 1),
(1527, 100, 'Nusa Tenggara Timur', 'NT', 1),
(1528, 100, 'Papua', 'PA', 1),
(1529, 100, 'Riau', 'RI', 1),
(1530, 100, 'Sulawesi Selatan', 'SN', 1),
(1531, 100, 'Sulawesi Tengah', 'ST', 1),
(1532, 100, 'Sulawesi Tenggara', 'SG', 1),
(1533, 100, 'Sulawesi Utara', 'SA', 1),
(1534, 100, 'Sumatera Barat', 'SB', 1),
(1535, 100, 'Sumatera Selatan', 'SS', 1),
(1536, 100, 'Sumatera Utara', 'SU', 1),
(1537, 100, 'Yogyakarta', 'YO', 1),
(1538, 101, 'Tehran', 'TEH', 1),
(1539, 101, 'Qom', 'QOM', 1),
(1540, 101, 'Markazi', 'MKZ', 1),
(1541, 101, 'Qazvin', 'QAZ', 1),
(1542, 101, 'Gilan', 'GIL', 1),
(1543, 101, 'Ardabil', 'ARD', 1),
(1544, 101, 'Zanjan', 'ZAN', 1),
(1545, 101, 'East Azarbaijan', 'EAZ', 1),
(1546, 101, 'West Azarbaijan', 'WEZ', 1),
(1547, 101, 'Kurdistan', 'KRD', 1),
(1548, 101, 'Hamadan', 'HMD', 1),
(1549, 101, 'Kermanshah', 'KRM', 1),
(1550, 101, 'Ilam', 'ILM', 1),
(1551, 101, 'Lorestan', 'LRS', 1),
(1552, 101, 'Khuzestan', 'KZT', 1),
(1553, 101, 'Chahar Mahaal and Bakhtiari', 'CMB', 1),
(1554, 101, 'Kohkiluyeh and Buyer Ahmad', 'KBA', 1),
(1555, 101, 'Bushehr', 'BSH', 1),
(1556, 101, 'Fars', 'FAR', 1),
(1557, 101, 'Hormozgan', 'HRM', 1),
(1558, 101, 'Sistan and Baluchistan', 'SBL', 1),
(1559, 101, 'Kerman', 'KRB', 1),
(1560, 101, 'Yazd', 'YZD', 1),
(1561, 101, 'Esfahan', 'EFH', 1),
(1562, 101, 'Semnan', 'SMN', 1),
(1563, 101, 'Mazandaran', 'MZD', 1),
(1564, 101, 'Golestan', 'GLS', 1),
(1565, 101, 'North Khorasan', 'NKH', 1),
(1566, 101, 'Razavi Khorasan', 'RKH', 1),
(1567, 101, 'South Khorasan', 'SKH', 1),
(1568, 102, 'Baghdad', 'BD', 1),
(1569, 102, 'Salah ad Din', 'SD', 1),
(1570, 102, 'Diyala', 'DY', 1),
(1571, 102, 'Wasit', 'WS', 1),
(1572, 102, 'Maysan', 'MY', 1),
(1573, 102, 'Al Basrah', 'BA', 1),
(1574, 102, 'Dhi Qar', 'DQ', 1),
(1575, 102, 'Al Muthanna', 'MU', 1),
(1576, 102, 'Al Qadisyah', 'QA', 1),
(1577, 102, 'Babil', 'BB', 1),
(1578, 102, 'Al Karbala', 'KB', 1),
(1579, 102, 'An Najaf', 'NJ', 1),
(1580, 102, 'Al Anbar', 'AB', 1),
(1581, 102, 'Ninawa', 'NN', 1),
(1582, 102, 'Dahuk', 'DH', 1),
(1583, 102, 'Arbil', 'AL', 1),
(1584, 102, 'At Ta\'mim', 'TM', 1),
(1585, 102, 'As Sulaymaniyah', 'SL', 1),
(1586, 103, 'Carlow', 'CA', 1),
(1587, 103, 'Cavan', 'CV', 1),
(1588, 103, 'Clare', 'CL', 1),
(1589, 103, 'Cork', 'CO', 1),
(1590, 103, 'Donegal', 'DO', 1),
(1591, 103, 'Dublin', 'DU', 1),
(1592, 103, 'Galway', 'GA', 1),
(1593, 103, 'Kerry', 'KE', 1),
(1594, 103, 'Kildare', 'KI', 1),
(1595, 103, 'Kilkenny', 'KL', 1),
(1596, 103, 'Laois', 'LA', 1),
(1597, 103, 'Leitrim', 'LE', 1);
INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1598, 103, 'Limerick', 'LI', 1),
(1599, 103, 'Longford', 'LO', 1),
(1600, 103, 'Louth', 'LU', 1),
(1601, 103, 'Mayo', 'MA', 1),
(1602, 103, 'Meath', 'ME', 1),
(1603, 103, 'Monaghan', 'MO', 1),
(1604, 103, 'Offaly', 'OF', 1),
(1605, 103, 'Roscommon', 'RO', 1),
(1606, 103, 'Sligo', 'SL', 1),
(1607, 103, 'Tipperary', 'TI', 1),
(1608, 103, 'Waterford', 'WA', 1),
(1609, 103, 'Westmeath', 'WE', 1),
(1610, 103, 'Wexford', 'WX', 1),
(1611, 103, 'Wicklow', 'WI', 1),
(1612, 104, 'Be\'er Sheva', 'BS', 1),
(1613, 104, 'Bika\'at Hayarden', 'BH', 1),
(1614, 104, 'Eilat and Arava', 'EA', 1),
(1615, 104, 'Galil', 'GA', 1),
(1616, 104, 'Haifa', 'HA', 1),
(1617, 104, 'Jehuda Mountains', 'JM', 1),
(1618, 104, 'Jerusalem', 'JE', 1),
(1619, 104, 'Negev', 'NE', 1),
(1620, 104, 'Semaria', 'SE', 1),
(1621, 104, 'Sharon', 'SH', 1),
(1622, 104, 'Tel Aviv (Gosh Dan)', 'TA', 1),
(3860, 105, 'Caltanissetta', 'CL', 1),
(3842, 105, 'Agrigento', 'AG', 1),
(3843, 105, 'Alessandria', 'AL', 1),
(3844, 105, 'Ancona', 'AN', 1),
(3845, 105, 'Aosta', 'AO', 1),
(3846, 105, 'Arezzo', 'AR', 1),
(3847, 105, 'Ascoli Piceno', 'AP', 1),
(3848, 105, 'Asti', 'AT', 1),
(3849, 105, 'Avellino', 'AV', 1),
(3850, 105, 'Bari', 'BA', 1),
(3851, 105, 'Belluno', 'BL', 1),
(3852, 105, 'Benevento', 'BN', 1),
(3853, 105, 'Bergamo', 'BG', 1),
(3854, 105, 'Biella', 'BI', 1),
(3855, 105, 'Bologna', 'BO', 1),
(3856, 105, 'Bolzano', 'BZ', 1),
(3857, 105, 'Brescia', 'BS', 1),
(3858, 105, 'Brindisi', 'BR', 1),
(3859, 105, 'Cagliari', 'CA', 1),
(1643, 106, 'Clarendon Parish', 'CLA', 1),
(1644, 106, 'Hanover Parish', 'HAN', 1),
(1645, 106, 'Kingston Parish', 'KIN', 1),
(1646, 106, 'Manchester Parish', 'MAN', 1),
(1647, 106, 'Portland Parish', 'POR', 1),
(1648, 106, 'Saint Andrew Parish', 'AND', 1),
(1649, 106, 'Saint Ann Parish', 'ANN', 1),
(1650, 106, 'Saint Catherine Parish', 'CAT', 1),
(1651, 106, 'Saint Elizabeth Parish', 'ELI', 1),
(1652, 106, 'Saint James Parish', 'JAM', 1),
(1653, 106, 'Saint Mary Parish', 'MAR', 1),
(1654, 106, 'Saint Thomas Parish', 'THO', 1),
(1655, 106, 'Trelawny Parish', 'TRL', 1),
(1656, 106, 'Westmoreland Parish', 'WML', 1),
(1657, 107, 'Aichi', 'AI', 1),
(1658, 107, 'Akita', 'AK', 1),
(1659, 107, 'Aomori', 'AO', 1),
(1660, 107, 'Chiba', 'CH', 1),
(1661, 107, 'Ehime', 'EH', 1),
(1662, 107, 'Fukui', 'FK', 1),
(1663, 107, 'Fukuoka', 'FU', 1),
(1664, 107, 'Fukushima', 'FS', 1),
(1665, 107, 'Gifu', 'GI', 1),
(1666, 107, 'Gumma', 'GU', 1),
(1667, 107, 'Hiroshima', 'HI', 1),
(1668, 107, 'Hokkaido', 'HO', 1),
(1669, 107, 'Hyogo', 'HY', 1),
(1670, 107, 'Ibaraki', 'IB', 1),
(1671, 107, 'Ishikawa', 'IS', 1),
(1672, 107, 'Iwate', 'IW', 1),
(1673, 107, 'Kagawa', 'KA', 1),
(1674, 107, 'Kagoshima', 'KG', 1),
(1675, 107, 'Kanagawa', 'KN', 1),
(1676, 107, 'Kochi', 'KO', 1),
(1677, 107, 'Kumamoto', 'KU', 1),
(1678, 107, 'Kyoto', 'KY', 1),
(1679, 107, 'Mie', 'MI', 1),
(1680, 107, 'Miyagi', 'MY', 1),
(1681, 107, 'Miyazaki', 'MZ', 1),
(1682, 107, 'Nagano', 'NA', 1),
(1683, 107, 'Nagasaki', 'NG', 1),
(1684, 107, 'Nara', 'NR', 1),
(1685, 107, 'Niigata', 'NI', 1),
(1686, 107, 'Oita', 'OI', 1),
(1687, 107, 'Okayama', 'OK', 1),
(1688, 107, 'Okinawa', 'ON', 1),
(1689, 107, 'Osaka', 'OS', 1),
(1690, 107, 'Saga', 'SA', 1),
(1691, 107, 'Saitama', 'SI', 1),
(1692, 107, 'Shiga', 'SH', 1),
(1693, 107, 'Shimane', 'SM', 1),
(1694, 107, 'Shizuoka', 'SZ', 1),
(1695, 107, 'Tochigi', 'TO', 1),
(1696, 107, 'Tokushima', 'TS', 1),
(1697, 107, 'Tokyo', 'TK', 1),
(1698, 107, 'Tottori', 'TT', 1),
(1699, 107, 'Toyama', 'TY', 1),
(1700, 107, 'Wakayama', 'WA', 1),
(1701, 107, 'Yamagata', 'YA', 1),
(1702, 107, 'Yamaguchi', 'YM', 1),
(1703, 107, 'Yamanashi', 'YN', 1),
(1704, 108, '\'Amman', 'AM', 1),
(1705, 108, 'Ajlun', 'AJ', 1),
(1706, 108, 'Al \'Aqabah', 'AA', 1),
(1707, 108, 'Al Balqa\'', 'AB', 1),
(1708, 108, 'Al Karak', 'AK', 1),
(1709, 108, 'Al Mafraq', 'AL', 1),
(1710, 108, 'At Tafilah', 'AT', 1),
(1711, 108, 'Az Zarqa\'', 'AZ', 1),
(1712, 108, 'Irbid', 'IR', 1),
(1713, 108, 'Jarash', 'JA', 1),
(1714, 108, 'Ma\'an', 'MA', 1),
(1715, 108, 'Madaba', 'MD', 1),
(1716, 109, 'Almaty', 'AL', 1),
(1717, 109, 'Almaty City', 'AC', 1),
(1718, 109, 'Aqmola', 'AM', 1),
(1719, 109, 'Aqtobe', 'AQ', 1),
(1720, 109, 'Astana City', 'AS', 1),
(1721, 109, 'Atyrau', 'AT', 1),
(1722, 109, 'Batys Qazaqstan', 'BA', 1),
(1723, 109, 'Bayqongyr City', 'BY', 1),
(1724, 109, 'Mangghystau', 'MA', 1),
(1725, 109, 'Ongtustik Qazaqstan', 'ON', 1),
(1726, 109, 'Pavlodar', 'PA', 1),
(1727, 109, 'Qaraghandy', 'QA', 1),
(1728, 109, 'Qostanay', 'QO', 1),
(1729, 109, 'Qyzylorda', 'QY', 1),
(1730, 109, 'Shyghys Qazaqstan', 'SH', 1),
(1731, 109, 'Soltustik Qazaqstan', 'SO', 1),
(1732, 109, 'Zhambyl', 'ZH', 1),
(1733, 110, 'Central', 'CE', 1),
(1734, 110, 'Coast', 'CO', 1),
(1735, 110, 'Eastern', 'EA', 1),
(1736, 110, 'Nairobi Area', 'NA', 1),
(1737, 110, 'North Eastern', 'NE', 1),
(1738, 110, 'Nyanza', 'NY', 1),
(1739, 110, 'Rift Valley', 'RV', 1),
(1740, 110, 'Western', 'WE', 1),
(1741, 111, 'Abaiang', 'AG', 1),
(1742, 111, 'Abemama', 'AM', 1),
(1743, 111, 'Aranuka', 'AK', 1),
(1744, 111, 'Arorae', 'AO', 1),
(1745, 111, 'Banaba', 'BA', 1),
(1746, 111, 'Beru', 'BE', 1),
(1747, 111, 'Butaritari', 'bT', 1),
(1748, 111, 'Kanton', 'KA', 1),
(1749, 111, 'Kiritimati', 'KR', 1),
(1750, 111, 'Kuria', 'KU', 1),
(1751, 111, 'Maiana', 'MI', 1),
(1752, 111, 'Makin', 'MN', 1),
(1753, 111, 'Marakei', 'ME', 1),
(1754, 111, 'Nikunau', 'NI', 1),
(1755, 111, 'Nonouti', 'NO', 1),
(1756, 111, 'Onotoa', 'ON', 1),
(1757, 111, 'Tabiteuea', 'TT', 1),
(1758, 111, 'Tabuaeran', 'TR', 1),
(1759, 111, 'Tamana', 'TM', 1),
(1760, 111, 'Tarawa', 'TW', 1),
(1761, 111, 'Teraina', 'TE', 1),
(1762, 112, 'Chagang-do', 'CHA', 1),
(1763, 112, 'Hamgyong-bukto', 'HAB', 1),
(1764, 112, 'Hamgyong-namdo', 'HAN', 1),
(1765, 112, 'Hwanghae-bukto', 'HWB', 1),
(1766, 112, 'Hwanghae-namdo', 'HWN', 1),
(1767, 112, 'Kangwon-do', 'KAN', 1),
(1768, 112, 'P\'yongan-bukto', 'PYB', 1),
(1769, 112, 'P\'yongan-namdo', 'PYN', 1),
(1770, 112, 'Ryanggang-do (Yanggang-do)', 'YAN', 1),
(1771, 112, 'Rason Directly Governed City', 'NAJ', 1),
(1772, 112, 'P\'yongyang Special City', 'PYO', 1),
(1773, 113, 'Ch\'ungch\'ong-bukto', 'CO', 1),
(1774, 113, 'Ch\'ungch\'ong-namdo', 'CH', 1),
(1775, 113, 'Cheju-do', 'CD', 1),
(1776, 113, 'Cholla-bukto', 'CB', 1),
(1777, 113, 'Cholla-namdo', 'CN', 1),
(1778, 113, 'Inch\'on-gwangyoksi', 'IG', 1),
(1779, 113, 'Kangwon-do', 'KA', 1),
(1780, 113, 'Kwangju-gwangyoksi', 'KG', 1),
(1781, 113, 'Kyonggi-do', 'KD', 1),
(1782, 113, 'Kyongsang-bukto', 'KB', 1),
(1783, 113, 'Kyongsang-namdo', 'KN', 1),
(1784, 113, 'Pusan-gwangyoksi', 'PG', 1),
(1785, 113, 'Soul-t\'ukpyolsi', 'SO', 1),
(1786, 113, 'Taegu-gwangyoksi', 'TA', 1),
(1787, 113, 'Taejon-gwangyoksi', 'TG', 1),
(1788, 114, 'Al \'Asimah', 'AL', 1),
(1789, 114, 'Al Ahmadi', 'AA', 1),
(1790, 114, 'Al Farwaniyah', 'AF', 1),
(1791, 114, 'Al Jahra\'', 'AJ', 1),
(1792, 114, 'Hawalli', 'HA', 1),
(1793, 115, 'Bishkek', 'GB', 1),
(1794, 115, 'Batken', 'B', 1),
(1795, 115, 'Chu', 'C', 1),
(1796, 115, 'Jalal-Abad', 'J', 1),
(1797, 115, 'Naryn', 'N', 1),
(1798, 115, 'Osh', 'O', 1),
(1799, 115, 'Talas', 'T', 1),
(1800, 115, 'Ysyk-Kol', 'Y', 1),
(1801, 116, 'Vientiane', 'VT', 1),
(1802, 116, 'Attapu', 'AT', 1),
(1803, 116, 'Bokeo', 'BK', 1),
(1804, 116, 'Bolikhamxai', 'BL', 1),
(1805, 116, 'Champasak', 'CH', 1),
(1806, 116, 'Houaphan', 'HO', 1),
(1807, 116, 'Khammouan', 'KH', 1),
(1808, 116, 'Louang Namtha', 'LM', 1),
(1809, 116, 'Louangphabang', 'LP', 1),
(1810, 116, 'Oudomxai', 'OU', 1),
(1811, 116, 'Phongsali', 'PH', 1),
(1812, 116, 'Salavan', 'SL', 1),
(1813, 116, 'Savannakhet', 'SV', 1),
(1814, 116, 'Vientiane', 'VI', 1),
(1815, 116, 'Xaignabouli', 'XA', 1),
(1816, 116, 'Xekong', 'XE', 1),
(1817, 116, 'Xiangkhoang', 'XI', 1),
(1818, 116, 'Xaisomboun', 'XN', 1),
(1852, 119, 'Berea', 'BE', 1),
(1853, 119, 'Butha-Buthe', 'BB', 1),
(1854, 119, 'Leribe', 'LE', 1),
(1855, 119, 'Mafeteng', 'MF', 1),
(1856, 119, 'Maseru', 'MS', 1),
(1857, 119, 'Mohale\'s Hoek', 'MH', 1),
(1858, 119, 'Mokhotlong', 'MK', 1),
(1859, 119, 'Qacha\'s Nek', 'QN', 1),
(1860, 119, 'Quthing', 'QT', 1),
(1861, 119, 'Thaba-Tseka', 'TT', 1),
(1862, 120, 'Bomi', 'BI', 1),
(1863, 120, 'Bong', 'BG', 1),
(1864, 120, 'Grand Bassa', 'GB', 1),
(1865, 120, 'Grand Cape Mount', 'CM', 1),
(1866, 120, 'Grand Gedeh', 'GG', 1),
(1867, 120, 'Grand Kru', 'GK', 1),
(1868, 120, 'Lofa', 'LO', 1),
(1869, 120, 'Margibi', 'MG', 1),
(1870, 120, 'Maryland', 'ML', 1),
(1871, 120, 'Montserrado', 'MS', 1),
(1872, 120, 'Nimba', 'NB', 1),
(1873, 120, 'River Cess', 'RC', 1),
(1874, 120, 'Sinoe', 'SN', 1),
(1875, 121, 'Ajdabiya', 'AJ', 1),
(1876, 121, 'Al \'Aziziyah', 'AZ', 1),
(1877, 121, 'Al Fatih', 'FA', 1),
(1878, 121, 'Al Jabal al Akhdar', 'JA', 1),
(1879, 121, 'Al Jufrah', 'JU', 1),
(1880, 121, 'Al Khums', 'KH', 1),
(1881, 121, 'Al Kufrah', 'KU', 1),
(1882, 121, 'An Nuqat al Khams', 'NK', 1),
(1883, 121, 'Ash Shati\'', 'AS', 1),
(1884, 121, 'Awbari', 'AW', 1),
(1885, 121, 'Az Zawiyah', 'ZA', 1),
(1886, 121, 'Banghazi', 'BA', 1),
(1887, 121, 'Darnah', 'DA', 1),
(1888, 121, 'Ghadamis', 'GD', 1),
(1889, 121, 'Gharyan', 'GY', 1),
(1890, 121, 'Misratah', 'MI', 1),
(1891, 121, 'Murzuq', 'MZ', 1),
(1892, 121, 'Sabha', 'SB', 1),
(1893, 121, 'Sawfajjin', 'SW', 1),
(1894, 121, 'Surt', 'SU', 1),
(1895, 121, 'Tarabulus (Tripoli)', 'TL', 1),
(1896, 121, 'Tarhunah', 'TH', 1),
(1897, 121, 'Tubruq', 'TU', 1),
(1898, 121, 'Yafran', 'YA', 1),
(1899, 121, 'Zlitan', 'ZL', 1),
(1900, 122, 'Vaduz', 'V', 1),
(1901, 122, 'Schaan', 'A', 1),
(1902, 122, 'Balzers', 'B', 1),
(1903, 122, 'Triesen', 'N', 1),
(1904, 122, 'Eschen', 'E', 1),
(1905, 122, 'Mauren', 'M', 1),
(1906, 122, 'Triesenberg', 'T', 1),
(1907, 122, 'Ruggell', 'R', 1),
(1908, 122, 'Gamprin', 'G', 1),
(1909, 122, 'Schellenberg', 'L', 1),
(1910, 122, 'Planken', 'P', 1),
(1911, 123, 'Alytus', 'AL', 1),
(1912, 123, 'Kaunas', 'KA', 1),
(1913, 123, 'Klaipeda', 'KL', 1),
(1914, 123, 'Marijampole', 'MA', 1),
(1915, 123, 'Panevezys', 'PA', 1),
(1916, 123, 'Siauliai', 'SI', 1),
(1917, 123, 'Taurage', 'TA', 1),
(1918, 123, 'Telsiai', 'TE', 1),
(1919, 123, 'Utena', 'UT', 1),
(1920, 123, 'Vilnius', 'VI', 1),
(1921, 124, 'Diekirch', 'DD', 1),
(1922, 124, 'Clervaux', 'DC', 1),
(1923, 124, 'Redange', 'DR', 1),
(1924, 124, 'Vianden', 'DV', 1),
(1925, 124, 'Wiltz', 'DW', 1),
(1926, 124, 'Grevenmacher', 'GG', 1),
(1927, 124, 'Echternach', 'GE', 1),
(1928, 124, 'Remich', 'GR', 1),
(1929, 124, 'Luxembourg', 'LL', 1),
(1930, 124, 'Capellen', 'LC', 1),
(1931, 124, 'Esch-sur-Alzette', 'LE', 1),
(1932, 124, 'Mersch', 'LM', 1),
(1933, 125, 'Our Lady Fatima Parish', 'OLF', 1),
(1934, 125, 'St. Anthony Parish', 'ANT', 1),
(1935, 125, 'St. Lazarus Parish', 'LAZ', 1),
(1936, 125, 'Cathedral Parish', 'CAT', 1),
(1937, 125, 'St. Lawrence Parish', 'LAW', 1),
(1938, 127, 'Antananarivo', 'AN', 1),
(1939, 127, 'Antsiranana', 'AS', 1),
(1940, 127, 'Fianarantsoa', 'FN', 1),
(1941, 127, 'Mahajanga', 'MJ', 1),
(1942, 127, 'Toamasina', 'TM', 1),
(1943, 127, 'Toliara', 'TL', 1),
(1944, 128, 'Balaka', 'BLK', 1),
(1945, 128, 'Blantyre', 'BLT', 1),
(1946, 128, 'Chikwawa', 'CKW', 1),
(1947, 128, 'Chiradzulu', 'CRD', 1),
(1948, 128, 'Chitipa', 'CTP', 1),
(1949, 128, 'Dedza', 'DDZ', 1),
(1950, 128, 'Dowa', 'DWA', 1),
(1951, 128, 'Karonga', 'KRG', 1),
(1952, 128, 'Kasungu', 'KSG', 1),
(1953, 128, 'Likoma', 'LKM', 1),
(1954, 128, 'Lilongwe', 'LLG', 1),
(1955, 128, 'Machinga', 'MCG', 1),
(1956, 128, 'Mangochi', 'MGC', 1),
(1957, 128, 'Mchinji', 'MCH', 1),
(1958, 128, 'Mulanje', 'MLJ', 1),
(1959, 128, 'Mwanza', 'MWZ', 1),
(1960, 128, 'Mzimba', 'MZM', 1),
(1961, 128, 'Ntcheu', 'NTU', 1),
(1962, 128, 'Nkhata Bay', 'NKB', 1),
(1963, 128, 'Nkhotakota', 'NKH', 1),
(1964, 128, 'Nsanje', 'NSJ', 1),
(1965, 128, 'Ntchisi', 'NTI', 1),
(1966, 128, 'Phalombe', 'PHL', 1),
(1967, 128, 'Rumphi', 'RMP', 1),
(1968, 128, 'Salima', 'SLM', 1),
(1969, 128, 'Thyolo', 'THY', 1),
(1970, 128, 'Zomba', 'ZBA', 1),
(1971, 129, 'Johor', 'MY-01', 1),
(1972, 129, 'Kedah', 'MY-02', 1),
(1973, 129, 'Kelantan', 'MY-03', 1),
(1974, 129, 'Labuan', 'MY-15', 1),
(1975, 129, 'Melaka', 'MY-04', 1),
(1976, 129, 'Negeri Sembilan', 'MY-05', 1),
(1977, 129, 'Pahang', 'MY-06', 1),
(1978, 129, 'Perak', 'MY-08', 1),
(1979, 129, 'Perlis', 'MY-09', 1),
(1980, 129, 'Pulau Pinang', 'MY-07', 1),
(1981, 129, 'Sabah', 'MY-12', 1),
(1982, 129, 'Sarawak', 'MY-13', 1),
(1983, 129, 'Selangor', 'MY-10', 1),
(1984, 129, 'Terengganu', 'MY-11', 1),
(1985, 129, 'Kuala Lumpur', 'MY-14', 1),
(4035, 129, 'Putrajaya', 'MY-16', 1),
(1986, 130, 'Thiladhunmathi Uthuru', 'THU', 1),
(1987, 130, 'Thiladhunmathi Dhekunu', 'THD', 1),
(1988, 130, 'Miladhunmadulu Uthuru', 'MLU', 1),
(1989, 130, 'Miladhunmadulu Dhekunu', 'MLD', 1),
(1990, 130, 'Maalhosmadulu Uthuru', 'MAU', 1),
(1991, 130, 'Maalhosmadulu Dhekunu', 'MAD', 1),
(1992, 130, 'Faadhippolhu', 'FAA', 1),
(1993, 130, 'Male Atoll', 'MAA', 1),
(1994, 130, 'Ari Atoll Uthuru', 'AAU', 1),
(1995, 130, 'Ari Atoll Dheknu', 'AAD', 1),
(1996, 130, 'Felidhe Atoll', 'FEA', 1),
(1997, 130, 'Mulaku Atoll', 'MUA', 1),
(1998, 130, 'Nilandhe Atoll Uthuru', 'NAU', 1),
(1999, 130, 'Nilandhe Atoll Dhekunu', 'NAD', 1),
(2000, 130, 'Kolhumadulu', 'KLH', 1),
(2001, 130, 'Hadhdhunmathi', 'HDH', 1),
(2002, 130, 'Huvadhu Atoll Uthuru', 'HAU', 1),
(2003, 130, 'Huvadhu Atoll Dhekunu', 'HAD', 1),
(2004, 130, 'Fua Mulaku', 'FMU', 1),
(2005, 130, 'Addu', 'ADD', 1),
(2006, 131, 'Gao', 'GA', 1),
(2007, 131, 'Kayes', 'KY', 1),
(2008, 131, 'Kidal', 'KD', 1),
(2009, 131, 'Koulikoro', 'KL', 1),
(2010, 131, 'Mopti', 'MP', 1),
(2011, 131, 'Segou', 'SG', 1),
(2012, 131, 'Sikasso', 'SK', 1),
(2013, 131, 'Tombouctou', 'TB', 1),
(2014, 131, 'Bamako Capital District', 'CD', 1),
(2015, 132, 'Attard', 'ATT', 1),
(2016, 132, 'Balzan', 'BAL', 1),
(2017, 132, 'Birgu', 'BGU', 1),
(2018, 132, 'Birkirkara', 'BKK', 1),
(2019, 132, 'Birzebbuga', 'BRZ', 1),
(2020, 132, 'Bormla', 'BOR', 1),
(2021, 132, 'Dingli', 'DIN', 1),
(2022, 132, 'Fgura', 'FGU', 1),
(2023, 132, 'Floriana', 'FLO', 1),
(2024, 132, 'Gudja', 'GDJ', 1),
(2025, 132, 'Gzira', 'GZR', 1),
(2026, 132, 'Gargur', 'GRG', 1),
(2027, 132, 'Gaxaq', 'GXQ', 1),
(2028, 132, 'Hamrun', 'HMR', 1),
(2029, 132, 'Iklin', 'IKL', 1),
(2030, 132, 'Isla', 'ISL', 1),
(2031, 132, 'Kalkara', 'KLK', 1),
(2032, 132, 'Kirkop', 'KRK', 1),
(2033, 132, 'Lija', 'LIJ', 1),
(2034, 132, 'Luqa', 'LUQ', 1),
(2035, 132, 'Marsa', 'MRS', 1),
(2036, 132, 'Marsaskala', 'MKL', 1),
(2037, 132, 'Marsaxlokk', 'MXL', 1),
(2038, 132, 'Mdina', 'MDN', 1),
(2039, 132, 'Melliea', 'MEL', 1),
(2040, 132, 'Mgarr', 'MGR', 1),
(2041, 132, 'Mosta', 'MST', 1),
(2042, 132, 'Mqabba', 'MQA', 1),
(2043, 132, 'Msida', 'MSI', 1),
(2044, 132, 'Mtarfa', 'MTF', 1),
(2045, 132, 'Naxxar', 'NAX', 1),
(2046, 132, 'Paola', 'PAO', 1),
(2047, 132, 'Pembroke', 'PEM', 1),
(2048, 132, 'Pieta', 'PIE', 1),
(2049, 132, 'Qormi', 'QOR', 1),
(2050, 132, 'Qrendi', 'QRE', 1),
(2051, 132, 'Rabat', 'RAB', 1),
(2052, 132, 'Safi', 'SAF', 1),
(2053, 132, 'San Giljan', 'SGI', 1),
(2054, 132, 'Santa Lucija', 'SLU', 1),
(2055, 132, 'San Pawl il-Bahar', 'SPB', 1),
(2056, 132, 'San Gwann', 'SGW', 1),
(2057, 132, 'Santa Venera', 'SVE', 1),
(2058, 132, 'Siggiewi', 'SIG', 1),
(2059, 132, 'Sliema', 'SLM', 1),
(2060, 132, 'Swieqi', 'SWQ', 1),
(2061, 132, 'Ta Xbiex', 'TXB', 1),
(2062, 132, 'Tarxien', 'TRX', 1),
(2063, 132, 'Valletta', 'VLT', 1),
(2064, 132, 'Xgajra', 'XGJ', 1),
(2065, 132, 'Zabbar', 'ZBR', 1),
(2066, 132, 'Zebbug', 'ZBG', 1),
(2067, 132, 'Zejtun', 'ZJT', 1),
(2068, 132, 'Zurrieq', 'ZRQ', 1),
(2069, 132, 'Fontana', 'FNT', 1),
(2070, 132, 'Ghajnsielem', 'GHJ', 1),
(2071, 132, 'Gharb', 'GHR', 1),
(2072, 132, 'Ghasri', 'GHS', 1),
(2073, 132, 'Kercem', 'KRC', 1),
(2074, 132, 'Munxar', 'MUN', 1),
(2075, 132, 'Nadur', 'NAD', 1),
(2076, 132, 'Qala', 'QAL', 1),
(2077, 132, 'Victoria', 'VIC', 1),
(2078, 132, 'San Lawrenz', 'SLA', 1),
(2079, 132, 'Sannat', 'SNT', 1),
(2080, 132, 'Xagra', 'ZAG', 1),
(2081, 132, 'Xewkija', 'XEW', 1),
(2082, 132, 'Zebbug', 'ZEB', 1),
(2083, 133, 'Ailinginae', 'ALG', 1),
(2084, 133, 'Ailinglaplap', 'ALL', 1),
(2085, 133, 'Ailuk', 'ALK', 1),
(2086, 133, 'Arno', 'ARN', 1),
(2087, 133, 'Aur', 'AUR', 1),
(2088, 133, 'Bikar', 'BKR', 1),
(2089, 133, 'Bikini', 'BKN', 1),
(2090, 133, 'Bokak', 'BKK', 1),
(2091, 133, 'Ebon', 'EBN', 1),
(2092, 133, 'Enewetak', 'ENT', 1),
(2093, 133, 'Erikub', 'EKB', 1),
(2094, 133, 'Jabat', 'JBT', 1),
(2095, 133, 'Jaluit', 'JLT', 1),
(2096, 133, 'Jemo', 'JEM', 1),
(2097, 133, 'Kili', 'KIL', 1),
(2098, 133, 'Kwajalein', 'KWJ', 1),
(2099, 133, 'Lae', 'LAE', 1),
(2100, 133, 'Lib', 'LIB', 1),
(2101, 133, 'Likiep', 'LKP', 1),
(2102, 133, 'Majuro', 'MJR', 1),
(2103, 133, 'Maloelap', 'MLP', 1),
(2104, 133, 'Mejit', 'MJT', 1),
(2105, 133, 'Mili', 'MIL', 1),
(2106, 133, 'Namorik', 'NMK', 1),
(2107, 133, 'Namu', 'NAM', 1),
(2108, 133, 'Rongelap', 'RGL', 1),
(2109, 133, 'Rongrik', 'RGK', 1),
(2110, 133, 'Toke', 'TOK', 1),
(2111, 133, 'Ujae', 'UJA', 1),
(2112, 133, 'Ujelang', 'UJL', 1),
(2113, 133, 'Utirik', 'UTK', 1),
(2114, 133, 'Wotho', 'WTH', 1),
(2115, 133, 'Wotje', 'WTJ', 1),
(2116, 135, 'Adrar', 'AD', 1),
(2117, 135, 'Assaba', 'AS', 1),
(2118, 135, 'Brakna', 'BR', 1),
(2119, 135, 'Dakhlet Nouadhibou', 'DN', 1),
(2120, 135, 'Gorgol', 'GO', 1),
(2121, 135, 'Guidimaka', 'GM', 1),
(2122, 135, 'Hodh Ech Chargui', 'HC', 1),
(2123, 135, 'Hodh El Gharbi', 'HG', 1),
(2124, 135, 'Inchiri', 'IN', 1),
(2125, 135, 'Tagant', 'TA', 1),
(2126, 135, 'Tiris Zemmour', 'TZ', 1),
(2127, 135, 'Trarza', 'TR', 1),
(2128, 135, 'Nouakchott', 'NO', 1),
(2129, 136, 'Beau Bassin-Rose Hill', 'BR', 1),
(2130, 136, 'Curepipe', 'CU', 1),
(2131, 136, 'Port Louis', 'PU', 1),
(2132, 136, 'Quatre Bornes', 'QB', 1),
(2133, 136, 'Vacoas-Phoenix', 'VP', 1),
(2134, 136, 'Agalega Islands', 'AG', 1),
(2135, 136, 'Cargados Carajos Shoals (Saint Brandon Islands)', 'CC', 1),
(2136, 136, 'Rodrigues', 'RO', 1),
(2137, 136, 'Black River', 'BL', 1),
(2138, 136, 'Flacq', 'FL', 1),
(2139, 136, 'Grand Port', 'GP', 1),
(2140, 136, 'Moka', 'MO', 1),
(2141, 136, 'Pamplemousses', 'PA', 1),
(2142, 136, 'Plaines Wilhems', 'PW', 1),
(2143, 136, 'Port Louis', 'PL', 1),
(2144, 136, 'Riviere du Rempart', 'RR', 1),
(2145, 136, 'Savanne', 'SA', 1),
(2146, 138, 'Baja California Norte', 'BN', 1),
(2147, 138, 'Baja California Sur', 'BS', 1),
(2148, 138, 'Campeche', 'CA', 1),
(2149, 138, 'Chiapas', 'CI', 1),
(2150, 138, 'Chihuahua', 'CH', 1),
(2151, 138, 'Coahuila de Zaragoza', 'CZ', 1),
(2152, 138, 'Colima', 'CL', 1),
(2153, 138, 'Distrito Federal', 'DF', 1),
(2154, 138, 'Durango', 'DU', 1),
(2155, 138, 'Guanajuato', 'GA', 1),
(2156, 138, 'Guerrero', 'GE', 1),
(2157, 138, 'Hidalgo', 'HI', 1),
(2158, 138, 'Jalisco', 'JA', 1),
(2159, 138, 'Mexico', 'ME', 1),
(2160, 138, 'Michoacan de Ocampo', 'MI', 1),
(2161, 138, 'Morelos', 'MO', 1),
(2162, 138, 'Nayarit', 'NA', 1),
(2163, 138, 'Nuevo Leon', 'NL', 1),
(2164, 138, 'Oaxaca', 'OA', 1),
(2165, 138, 'Puebla', 'PU', 1),
(2166, 138, 'Queretaro de Arteaga', 'QA', 1),
(2167, 138, 'Quintana Roo', 'QR', 1),
(2168, 138, 'San Luis Potosi', 'SA', 1),
(2169, 138, 'Sinaloa', 'SI', 1),
(2170, 138, 'Sonora', 'SO', 1),
(2171, 138, 'Tabasco', 'TB', 1),
(2172, 138, 'Tamaulipas', 'TM', 1),
(2173, 138, 'Tlaxcala', 'TL', 1),
(2174, 138, 'Veracruz-Llave', 'VE', 1),
(2175, 138, 'Yucatan', 'YU', 1),
(2176, 138, 'Zacatecas', 'ZA', 1),
(2177, 139, 'Chuuk', 'C', 1),
(2178, 139, 'Kosrae', 'K', 1),
(2179, 139, 'Pohnpei', 'P', 1),
(2180, 139, 'Yap', 'Y', 1),
(2181, 140, 'Gagauzia', 'GA', 1),
(2182, 140, 'Chisinau', 'CU', 1),
(2183, 140, 'Balti', 'BA', 1),
(2184, 140, 'Cahul', 'CA', 1),
(2185, 140, 'Edinet', 'ED', 1),
(2186, 140, 'Lapusna', 'LA', 1),
(2187, 140, 'Orhei', 'OR', 1),
(2188, 140, 'Soroca', 'SO', 1),
(2189, 140, 'Tighina', 'TI', 1),
(2190, 140, 'Ungheni', 'UN', 1),
(2191, 140, 'St‚nga Nistrului', 'SN', 1),
(2192, 141, 'Fontvieille', 'FV', 1),
(2193, 141, 'La Condamine', 'LC', 1),
(2194, 141, 'Monaco-Ville', 'MV', 1),
(2195, 141, 'Monte-Carlo', 'MC', 1),
(2196, 142, 'Ulanbaatar', '1', 1),
(2197, 142, 'Orhon', '035', 1),
(2198, 142, 'Darhan uul', '037', 1),
(2199, 142, 'Hentiy', '039', 1),
(2200, 142, 'Hovsgol', '041', 1),
(2201, 142, 'Hovd', '043', 1),
(2202, 142, 'Uvs', '046', 1),
(2203, 142, 'Tov', '047', 1),
(2204, 142, 'Selenge', '049', 1),
(2205, 142, 'Suhbaatar', '051', 1),
(2206, 142, 'Omnogovi', '053', 1),
(2207, 142, 'Ovorhangay', '055', 1),
(2208, 142, 'Dzavhan', '057', 1),
(2209, 142, 'DundgovL', '059', 1),
(2210, 142, 'Dornod', '061', 1),
(2211, 142, 'Dornogov', '063', 1),
(2212, 142, 'Govi-Sumber', '064', 1),
(2213, 142, 'Govi-Altay', '065', 1),
(2214, 142, 'Bulgan', '067', 1),
(2215, 142, 'Bayanhongor', '069', 1),
(2216, 142, 'Bayan-Olgiy', '071', 1),
(2217, 142, 'Arhangay', '073', 1),
(2218, 143, 'Saint Anthony', 'A', 1),
(2219, 143, 'Saint Georges', 'G', 1),
(2220, 143, 'Saint Peter', 'P', 1),
(2221, 144, 'Agadir', 'AGD', 1),
(2222, 144, 'Al Hoceima', 'HOC', 1),
(2223, 144, 'Azilal', 'AZI', 1),
(2224, 144, 'Beni Mellal', 'BME', 1),
(2225, 144, 'Ben Slimane', 'BSL', 1),
(2226, 144, 'Boulemane', 'BLM', 1),
(2227, 144, 'Casablanca', 'CBL', 1),
(2228, 144, 'Chaouen', 'CHA', 1),
(2229, 144, 'El Jadida', 'EJA', 1),
(2230, 144, 'El Kelaa des Sraghna', 'EKS', 1),
(2231, 144, 'Er Rachidia', 'ERA', 1),
(2232, 144, 'Essaouira', 'ESS', 1),
(2233, 144, 'Fes', 'FES', 1),
(2234, 144, 'Figuig', 'FIG', 1),
(2235, 144, 'Guelmim', 'GLM', 1),
(2236, 144, 'Ifrane', 'IFR', 1),
(2237, 144, 'Kenitra', 'KEN', 1),
(2238, 144, 'Khemisset', 'KHM', 1),
(2239, 144, 'Khenifra', 'KHN', 1),
(2240, 144, 'Khouribga', 'KHO', 1),
(2241, 144, 'Laayoune', 'LYN', 1),
(2242, 144, 'Larache', 'LAR', 1),
(2243, 144, 'Marrakech', 'MRK', 1),
(2244, 144, 'Meknes', 'MKN', 1),
(2245, 144, 'Nador', 'NAD', 1),
(2246, 144, 'Ouarzazate', 'ORZ', 1),
(2247, 144, 'Oujda', 'OUJ', 1),
(2248, 144, 'Rabat-Sale', 'RSA', 1),
(2249, 144, 'Safi', 'SAF', 1),
(2250, 144, 'Settat', 'SET', 1),
(2251, 144, 'Sidi Kacem', 'SKA', 1),
(2252, 144, 'Tangier', 'TGR', 1),
(2253, 144, 'Tan-Tan', 'TAN', 1),
(2254, 144, 'Taounate', 'TAO', 1),
(2255, 144, 'Taroudannt', 'TRD', 1),
(2256, 144, 'Tata', 'TAT', 1),
(2257, 144, 'Taza', 'TAZ', 1),
(2258, 144, 'Tetouan', 'TET', 1),
(2259, 144, 'Tiznit', 'TIZ', 1),
(2260, 144, 'Ad Dakhla', 'ADK', 1),
(2261, 144, 'Boujdour', 'BJD', 1),
(2262, 144, 'Es Smara', 'ESM', 1),
(2263, 145, 'Cabo Delgado', 'CD', 1),
(2264, 145, 'Gaza', 'GZ', 1),
(2265, 145, 'Inhambane', 'IN', 1),
(2266, 145, 'Manica', 'MN', 1),
(2267, 145, 'Maputo (city)', 'MC', 1),
(2268, 145, 'Maputo', 'MP', 1),
(2269, 145, 'Nampula', 'NA', 1),
(2270, 145, 'Niassa', 'NI', 1),
(2271, 145, 'Sofala', 'SO', 1),
(2272, 145, 'Tete', 'TE', 1),
(2273, 145, 'Zambezia', 'ZA', 1),
(2274, 146, 'Ayeyarwady', 'AY', 1),
(2275, 146, 'Bago', 'BG', 1),
(2276, 146, 'Magway', 'MG', 1),
(2277, 146, 'Mandalay', 'MD', 1),
(2278, 146, 'Sagaing', 'SG', 1),
(2279, 146, 'Tanintharyi', 'TN', 1),
(2280, 146, 'Yangon', 'YG', 1),
(2281, 146, 'Chin State', 'CH', 1),
(2282, 146, 'Kachin State', 'KC', 1),
(2283, 146, 'Kayah State', 'KH', 1),
(2284, 146, 'Kayin State', 'KN', 1),
(2285, 146, 'Mon State', 'MN', 1),
(2286, 146, 'Rakhine State', 'RK', 1),
(2287, 146, 'Shan State', 'SH', 1),
(2288, 147, 'Caprivi', 'CA', 1),
(2289, 147, 'Erongo', 'ER', 1),
(2290, 147, 'Hardap', 'HA', 1),
(2291, 147, 'Karas', 'KR', 1),
(2292, 147, 'Kavango', 'KV', 1),
(2293, 147, 'Khomas', 'KH', 1),
(2294, 147, 'Kunene', 'KU', 1),
(2295, 147, 'Ohangwena', 'OW', 1),
(2296, 147, 'Omaheke', 'OK', 1),
(2297, 147, 'Omusati', 'OT', 1),
(2298, 147, 'Oshana', 'ON', 1),
(2299, 147, 'Oshikoto', 'OO', 1),
(2300, 147, 'Otjozondjupa', 'OJ', 1),
(2301, 148, 'Aiwo', 'AO', 1),
(2302, 148, 'Anabar', 'AA', 1),
(2303, 148, 'Anetan', 'AT', 1),
(2304, 148, 'Anibare', 'AI', 1),
(2305, 148, 'Baiti', 'BA', 1),
(2306, 148, 'Boe', 'BO', 1),
(2307, 148, 'Buada', 'BU', 1),
(2308, 148, 'Denigomodu', 'DE', 1),
(2309, 148, 'Ewa', 'EW', 1),
(2310, 148, 'Ijuw', 'IJ', 1),
(2311, 148, 'Meneng', 'ME', 1),
(2312, 148, 'Nibok', 'NI', 1),
(2313, 148, 'Uaboe', 'UA', 1),
(2314, 148, 'Yaren', 'YA', 1),
(2315, 149, 'Bagmati', 'BA', 1),
(2316, 149, 'Bheri', 'BH', 1),
(2317, 149, 'Dhawalagiri', 'DH', 1),
(2318, 149, 'Gandaki', 'GA', 1),
(2319, 149, 'Janakpur', 'JA', 1),
(2320, 149, 'Karnali', 'KA', 1),
(2321, 149, 'Kosi', 'KO', 1),
(2322, 149, 'Lumbini', 'LU', 1),
(2323, 149, 'Mahakali', 'MA', 1),
(2324, 149, 'Mechi', 'ME', 1),
(2325, 149, 'Narayani', 'NA', 1),
(2326, 149, 'Rapti', 'RA', 1),
(2327, 149, 'Sagarmatha', 'SA', 1),
(2328, 149, 'Seti', 'SE', 1),
(2329, 150, 'Drenthe', 'DR', 1),
(2330, 150, 'Flevoland', 'FL', 1),
(2331, 150, 'Friesland', 'FR', 1),
(2332, 150, 'Gelderland', 'GE', 1),
(2333, 150, 'Groningen', 'GR', 1),
(2334, 150, 'Limburg', 'LI', 1),
(2335, 150, 'Noord-Brabant', 'NB', 1),
(2336, 150, 'Noord-Holland', 'NH', 1),
(2337, 150, 'Overijssel', 'OV', 1),
(2338, 150, 'Utrecht', 'UT', 1),
(2339, 150, 'Zeeland', 'ZE', 1),
(2340, 150, 'Zuid-Holland', 'ZH', 1),
(2341, 152, 'Iles Loyaute', 'L', 1),
(2342, 152, 'Nord', 'N', 1),
(2343, 152, 'Sud', 'S', 1),
(2344, 153, 'Auckland', 'AUK', 1),
(2345, 153, 'Bay of Plenty', 'BOP', 1),
(2346, 153, 'Canterbury', 'CAN', 1),
(2347, 153, 'Coromandel', 'COR', 1),
(2348, 153, 'Gisborne', 'GIS', 1),
(2349, 153, 'Fiordland', 'FIO', 1),
(2350, 153, 'Hawke\'s Bay', 'HKB', 1),
(2351, 153, 'Marlborough', 'MBH', 1),
(2352, 153, 'Manawatu-Wanganui', 'MWT', 1),
(2353, 153, 'Mt Cook-Mackenzie', 'MCM', 1),
(2354, 153, 'Nelson', 'NSN', 1),
(2355, 153, 'Northland', 'NTL', 1),
(2356, 153, 'Otago', 'OTA', 1),
(2357, 153, 'Southland', 'STL', 1),
(2358, 153, 'Taranaki', 'TKI', 1),
(2359, 153, 'Wellington', 'WGN', 1),
(2360, 153, 'Waikato', 'WKO', 1),
(2361, 153, 'Wairarapa', 'WAI', 1),
(2362, 153, 'West Coast', 'WTC', 1),
(2363, 154, 'Atlantico Norte', 'AN', 1),
(2364, 154, 'Atlantico Sur', 'AS', 1),
(2365, 154, 'Boaco', 'BO', 1),
(2366, 154, 'Carazo', 'CA', 1),
(2367, 154, 'Chinandega', 'CI', 1),
(2368, 154, 'Chontales', 'CO', 1),
(2369, 154, 'Esteli', 'ES', 1),
(2370, 154, 'Granada', 'GR', 1),
(2371, 154, 'Jinotega', 'JI', 1),
(2372, 154, 'Leon', 'LE', 1),
(2373, 154, 'Madriz', 'MD', 1),
(2374, 154, 'Managua', 'MN', 1),
(2375, 154, 'Masaya', 'MS', 1),
(2376, 154, 'Matagalpa', 'MT', 1),
(2377, 154, 'Nuevo Segovia', 'NS', 1),
(2378, 154, 'Rio San Juan', 'RS', 1),
(2379, 154, 'Rivas', 'RI', 1),
(2380, 155, 'Agadez', 'AG', 1),
(2381, 155, 'Diffa', 'DF', 1),
(2382, 155, 'Dosso', 'DS', 1),
(2383, 155, 'Maradi', 'MA', 1),
(2384, 155, 'Niamey', 'NM', 1),
(2385, 155, 'Tahoua', 'TH', 1),
(2386, 155, 'Tillaberi', 'TL', 1),
(2387, 155, 'Zinder', 'ZD', 1),
(2388, 156, 'Abia', 'AB', 1),
(2389, 156, 'Abuja Federal Capital Territory', 'CT', 1),
(2390, 156, 'Adamawa', 'AD', 1),
(2391, 156, 'Akwa Ibom', 'AK', 1),
(2392, 156, 'Anambra', 'AN', 1),
(2393, 156, 'Bauchi', 'BC', 1),
(2394, 156, 'Bayelsa', 'BY', 1),
(2395, 156, 'Benue', 'BN', 1),
(2396, 156, 'Borno', 'BO', 1),
(2397, 156, 'Cross River', 'CR', 1),
(2398, 156, 'Delta', 'DE', 1),
(2399, 156, 'Ebonyi', 'EB', 1),
(2400, 156, 'Edo', 'ED', 1),
(2401, 156, 'Ekiti', 'EK', 1),
(2402, 156, 'Enugu', 'EN', 1),
(2403, 156, 'Gombe', 'GO', 1),
(2404, 156, 'Imo', 'IM', 1),
(2405, 156, 'Jigawa', 'JI', 1),
(2406, 156, 'Kaduna', 'KD', 1),
(2407, 156, 'Kano', 'KN', 1),
(2408, 156, 'Katsina', 'KT', 1),
(2409, 156, 'Kebbi', 'KE', 1),
(2410, 156, 'Kogi', 'KO', 1),
(2411, 156, 'Kwara', 'KW', 1),
(2412, 156, 'Lagos', 'LA', 1),
(2413, 156, 'Nassarawa', 'NA', 1),
(2414, 156, 'Niger', 'NI', 1),
(2415, 156, 'Ogun', 'OG', 1),
(2416, 156, 'Ondo', 'ONG', 1),
(2417, 156, 'Osun', 'OS', 1),
(2418, 156, 'Oyo', 'OY', 1),
(2419, 156, 'Plateau', 'PL', 1),
(2420, 156, 'Rivers', 'RI', 1),
(2421, 156, 'Sokoto', 'SO', 1),
(2422, 156, 'Taraba', 'TA', 1),
(2423, 156, 'Yobe', 'YO', 1),
(2424, 156, 'Zamfara', 'ZA', 1),
(2425, 159, 'Northern Islands', 'N', 1),
(2426, 159, 'Rota', 'R', 1),
(2427, 159, 'Saipan', 'S', 1),
(2428, 159, 'Tinian', 'T', 1),
(2429, 160, 'Akershus', 'AK', 1),
(2430, 160, 'Aust-Agder', 'AA', 1),
(2431, 160, 'Buskerud', 'BU', 1),
(2432, 160, 'Finnmark', 'FM', 1),
(2433, 160, 'Hedmark', 'HM', 1),
(2434, 160, 'Hordaland', 'HL', 1),
(2435, 160, 'More og Romdal', 'MR', 1),
(2436, 160, 'Nord-Trondelag', 'NT', 1),
(2437, 160, 'Nordland', 'NL', 1),
(2438, 160, 'Ostfold', 'OF', 1),
(2439, 160, 'Oppland', 'OP', 1),
(2440, 160, 'Oslo', 'OL', 1),
(2441, 160, 'Rogaland', 'RL', 1),
(2442, 160, 'Sor-Trondelag', 'ST', 1),
(2443, 160, 'Sogn og Fjordane', 'SJ', 1),
(2444, 160, 'Svalbard', 'SV', 1),
(2445, 160, 'Telemark', 'TM', 1),
(2446, 160, 'Troms', 'TR', 1),
(2447, 160, 'Vest-Agder', 'VA', 1),
(2448, 160, 'Vestfold', 'VF', 1),
(2449, 161, 'Ad Dakhiliyah', 'DA', 1),
(2450, 161, 'Al Batinah', 'BA', 1),
(2451, 161, 'Al Wusta', 'WU', 1),
(2452, 161, 'Ash Sharqiyah', 'SH', 1),
(2453, 161, 'Az Zahirah', 'ZA', 1),
(2454, 161, 'Masqat', 'MA', 1),
(2455, 161, 'Musandam', 'MU', 1),
(2456, 161, 'Zufar', 'ZU', 1),
(2457, 162, 'Balochistan', 'B', 1),
(2458, 162, 'Federally Administered Tribal Areas', 'T', 1),
(2459, 162, 'Islamabad Capital Territory', 'I', 1),
(2460, 162, 'North-West Frontier', 'N', 1),
(2461, 162, 'Punjab', 'P', 1),
(2462, 162, 'Sindh', 'S', 1),
(2463, 163, 'Aimeliik', 'AM', 1),
(2464, 163, 'Airai', 'AR', 1),
(2465, 163, 'Angaur', 'AN', 1),
(2466, 163, 'Hatohobei', 'HA', 1),
(2467, 163, 'Kayangel', 'KA', 1),
(2468, 163, 'Koror', 'KO', 1),
(2469, 163, 'Melekeok', 'ME', 1),
(2470, 163, 'Ngaraard', 'NA', 1),
(2471, 163, 'Ngarchelong', 'NG', 1),
(2472, 163, 'Ngardmau', 'ND', 1),
(2473, 163, 'Ngatpang', 'NT', 1),
(2474, 163, 'Ngchesar', 'NC', 1),
(2475, 163, 'Ngeremlengui', 'NR', 1),
(2476, 163, 'Ngiwal', 'NW', 1),
(2477, 163, 'Peleliu', 'PE', 1),
(2478, 163, 'Sonsorol', 'SO', 1),
(2479, 164, 'Bocas del Toro', 'BT', 1),
(2480, 164, 'Chiriqui', 'CH', 1),
(2481, 164, 'Cocle', 'CC', 1),
(2482, 164, 'Colon', 'CL', 1),
(2483, 164, 'Darien', 'DA', 1),
(2484, 164, 'Herrera', 'HE', 1),
(2485, 164, 'Los Santos', 'LS', 1),
(2486, 164, 'Panama', 'PA', 1),
(2487, 164, 'San Blas', 'SB', 1),
(2488, 164, 'Veraguas', 'VG', 1),
(2489, 165, 'Bougainville', 'BV', 1),
(2490, 165, 'Central', 'CE', 1),
(2491, 165, 'Chimbu', 'CH', 1),
(2492, 165, 'Eastern Highlands', 'EH', 1),
(2493, 165, 'East New Britain', 'EB', 1),
(2494, 165, 'East Sepik', 'ES', 1),
(2495, 165, 'Enga', 'EN', 1),
(2496, 165, 'Gulf', 'GU', 1),
(2497, 165, 'Madang', 'MD', 1),
(2498, 165, 'Manus', 'MN', 1),
(2499, 165, 'Milne Bay', 'MB', 1),
(2500, 165, 'Morobe', 'MR', 1),
(2501, 165, 'National Capital', 'NC', 1),
(2502, 165, 'New Ireland', 'NI', 1),
(2503, 165, 'Northern', 'NO', 1),
(2504, 165, 'Sandaun', 'SA', 1),
(2505, 165, 'Southern Highlands', 'SH', 1),
(2506, 165, 'Western', 'WE', 1),
(2507, 165, 'Western Highlands', 'WH', 1),
(2508, 165, 'West New Britain', 'WB', 1),
(2509, 166, 'Alto Paraguay', 'AG', 1),
(2510, 166, 'Alto Parana', 'AN', 1),
(2511, 166, 'Amambay', 'AM', 1),
(2512, 166, 'Asuncion', 'AS', 1),
(2513, 166, 'Boqueron', 'BO', 1),
(2514, 166, 'Caaguazu', 'CG', 1),
(2515, 166, 'Caazapa', 'CZ', 1),
(2516, 166, 'Canindeyu', 'CN', 1),
(2517, 166, 'Central', 'CE', 1),
(2518, 166, 'Concepcion', 'CC', 1),
(2519, 166, 'Cordillera', 'CD', 1),
(2520, 166, 'Guaira', 'GU', 1),
(2521, 166, 'Itapua', 'IT', 1),
(2522, 166, 'Misiones', 'MI', 1),
(2523, 166, 'Neembucu', 'NE', 1),
(2524, 166, 'Paraguari', 'PA', 1),
(2525, 166, 'Presidente Hayes', 'PH', 1),
(2526, 166, 'San Pedro', 'SP', 1),
(2527, 167, 'Amazonas', 'AM', 1),
(2528, 167, 'Ancash', 'AN', 1),
(2529, 167, 'Apurimac', 'AP', 1),
(2530, 167, 'Arequipa', 'AR', 1),
(2531, 167, 'Ayacucho', 'AY', 1),
(2532, 167, 'Cajamarca', 'CJ', 1),
(2533, 167, 'Callao', 'CL', 1),
(2534, 167, 'Cusco', 'CU', 1),
(2535, 167, 'Huancavelica', 'HV', 1),
(2536, 167, 'Huanuco', 'HO', 1),
(2537, 167, 'Ica', 'IC', 1),
(2538, 167, 'Junin', 'JU', 1),
(2539, 167, 'La Libertad', 'LD', 1),
(2540, 167, 'Lambayeque', 'LY', 1),
(2541, 167, 'Lima', 'LI', 1),
(2542, 167, 'Loreto', 'LO', 1),
(2543, 167, 'Madre de Dios', 'MD', 1),
(2544, 167, 'Moquegua', 'MO', 1),
(2545, 167, 'Pasco', 'PA', 1),
(2546, 167, 'Piura', 'PI', 1),
(2547, 167, 'Puno', 'PU', 1),
(2548, 167, 'San Martin', 'SM', 1),
(2549, 167, 'Tacna', 'TA', 1),
(2550, 167, 'Tumbes', 'TU', 1),
(2551, 167, 'Ucayali', 'UC', 1),
(2552, 168, 'Abra', 'ABR', 1),
(2553, 168, 'Agusan del Norte', 'ANO', 1),
(2554, 168, 'Agusan del Sur', 'ASU', 1),
(2555, 168, 'Aklan', 'AKL', 1),
(2556, 168, 'Albay', 'ALB', 1),
(2557, 168, 'Antique', 'ANT', 1),
(2558, 168, 'Apayao', 'APY', 1),
(2559, 168, 'Aurora', 'AUR', 1),
(2560, 168, 'Basilan', 'BAS', 1),
(2561, 168, 'Bataan', 'BTA', 1),
(2562, 168, 'Batanes', 'BTE', 1),
(2563, 168, 'Batangas', 'BTG', 1),
(2564, 168, 'Biliran', 'BLR', 1),
(2565, 168, 'Benguet', 'BEN', 1),
(2566, 168, 'Bohol', 'BOL', 1),
(2567, 168, 'Bukidnon', 'BUK', 1),
(2568, 168, 'Bulacan', 'BUL', 1),
(2569, 168, 'Cagayan', 'CAG', 1),
(2570, 168, 'Camarines Norte', 'CNO', 1),
(2571, 168, 'Camarines Sur', 'CSU', 1),
(2572, 168, 'Camiguin', 'CAM', 1),
(2573, 168, 'Capiz', 'CAP', 1),
(2574, 168, 'Catanduanes', 'CAT', 1),
(2575, 168, 'Cavite', 'CAV', 1),
(2576, 168, 'Cebu', 'CEB', 1),
(2577, 168, 'Compostela', 'CMP', 1),
(2578, 168, 'Davao del Norte', 'DNO', 1),
(2579, 168, 'Davao del Sur', 'DSU', 1),
(2580, 168, 'Davao Oriental', 'DOR', 1),
(2581, 168, 'Eastern Samar', 'ESA', 1),
(2582, 168, 'Guimaras', 'GUI', 1),
(2583, 168, 'Ifugao', 'IFU', 1),
(2584, 168, 'Ilocos Norte', 'INO', 1),
(2585, 168, 'Ilocos Sur', 'ISU', 1),
(2586, 168, 'Iloilo', 'ILO', 1),
(2587, 168, 'Isabela', 'ISA', 1),
(2588, 168, 'Kalinga', 'KAL', 1),
(2589, 168, 'Laguna', 'LAG', 1),
(2590, 168, 'Lanao del Norte', 'LNO', 1),
(2591, 168, 'Lanao del Sur', 'LSU', 1),
(2592, 168, 'La Union', 'UNI', 1),
(2593, 168, 'Leyte', 'LEY', 1),
(2594, 168, 'Maguindanao', 'MAG', 1),
(2595, 168, 'Marinduque', 'MRN', 1),
(2596, 168, 'Masbate', 'MSB', 1),
(2597, 168, 'Mindoro Occidental', 'MIC', 1),
(2598, 168, 'Mindoro Oriental', 'MIR', 1),
(2599, 168, 'Misamis Occidental', 'MSC', 1),
(2600, 168, 'Misamis Oriental', 'MOR', 1),
(2601, 168, 'Mountain', 'MOP', 1),
(2602, 168, 'Negros Occidental', 'NOC', 1),
(2603, 168, 'Negros Oriental', 'NOR', 1),
(2604, 168, 'North Cotabato', 'NCT', 1),
(2605, 168, 'Northern Samar', 'NSM', 1),
(2606, 168, 'Nueva Ecija', 'NEC', 1),
(2607, 168, 'Nueva Vizcaya', 'NVZ', 1),
(2608, 168, 'Palawan', 'PLW', 1),
(2609, 168, 'Pampanga', 'PMP', 1),
(2610, 168, 'Pangasinan', 'PNG', 1),
(2611, 168, 'Quezon', 'QZN', 1),
(2612, 168, 'Quirino', 'QRN', 1),
(2613, 168, 'Rizal', 'RIZ', 1),
(2614, 168, 'Romblon', 'ROM', 1),
(2615, 168, 'Samar', 'SMR', 1),
(2616, 168, 'Sarangani', 'SRG', 1),
(2617, 168, 'Siquijor', 'SQJ', 1),
(2618, 168, 'Sorsogon', 'SRS', 1),
(2619, 168, 'South Cotabato', 'SCO', 1),
(2620, 168, 'Southern Leyte', 'SLE', 1),
(2621, 168, 'Sultan Kudarat', 'SKU', 1),
(2622, 168, 'Sulu', 'SLU', 1),
(2623, 168, 'Surigao del Norte', 'SNO', 1),
(2624, 168, 'Surigao del Sur', 'SSU', 1),
(2625, 168, 'Tarlac', 'TAR', 1),
(2626, 168, 'Tawi-Tawi', 'TAW', 1),
(2627, 168, 'Zambales', 'ZBL', 1),
(2628, 168, 'Zamboanga del Norte', 'ZNO', 1),
(2629, 168, 'Zamboanga del Sur', 'ZSU', 1),
(2630, 168, 'Zamboanga Sibugay', 'ZSI', 1),
(2631, 170, 'Dolnoslaskie', 'DO', 1),
(2632, 170, 'Kujawsko-Pomorskie', 'KP', 1),
(2633, 170, 'Lodzkie', 'LO', 1),
(2634, 170, 'Lubelskie', 'LL', 1),
(2635, 170, 'Lubuskie', 'LU', 1),
(2636, 170, 'Malopolskie', 'ML', 1),
(2637, 170, 'Mazowieckie', 'MZ', 1),
(2638, 170, 'Opolskie', 'OP', 1),
(2639, 170, 'Podkarpackie', 'PP', 1),
(2640, 170, 'Podlaskie', 'PL', 1),
(2641, 170, 'Pomorskie', 'PM', 1),
(2642, 170, 'Slaskie', 'SL', 1),
(2643, 170, 'Swietokrzyskie', 'SW', 1),
(2644, 170, 'Warminsko-Mazurskie', 'WM', 1),
(2645, 170, 'Wielkopolskie', 'WP', 1),
(2646, 170, 'Zachodniopomorskie', 'ZA', 1),
(2647, 198, 'Saint Pierre', 'P', 1),
(2648, 198, 'Miquelon', 'M', 1),
(2649, 171, 'A&ccedil;ores', 'AC', 1),
(2650, 171, 'Aveiro', 'AV', 1),
(2651, 171, 'Beja', 'BE', 1),
(2652, 171, 'Braga', 'BR', 1),
(2653, 171, 'Bragan&ccedil;a', 'BA', 1),
(2654, 171, 'Castelo Branco', 'CB', 1),
(2655, 171, 'Coimbra', 'CO', 1),
(2656, 171, '&Eacute;vora', 'EV', 1),
(2657, 171, 'Faro', 'FA', 1),
(2658, 171, 'Guarda', 'GU', 1),
(2659, 171, 'Leiria', 'LE', 1),
(2660, 171, 'Lisboa', 'LI', 1),
(2661, 171, 'Madeira', 'ME', 1),
(2662, 171, 'Portalegre', 'PO', 1),
(2663, 171, 'Porto', 'PR', 1),
(2664, 171, 'Santar&eacute;m', 'SA', 1),
(2665, 171, 'Set&uacute;bal', 'SE', 1),
(2666, 171, 'Viana do Castelo', 'VC', 1),
(2667, 171, 'Vila Real', 'VR', 1),
(2668, 171, 'Viseu', 'VI', 1),
(2669, 173, 'Ad Dawhah', 'DW', 1),
(2670, 173, 'Al Ghuwayriyah', 'GW', 1),
(2671, 173, 'Al Jumayliyah', 'JM', 1),
(2672, 173, 'Al Khawr', 'KR', 1),
(2673, 173, 'Al Wakrah', 'WK', 1),
(2674, 173, 'Ar Rayyan', 'RN', 1),
(2675, 173, 'Jarayan al Batinah', 'JB', 1),
(2676, 173, 'Madinat ash Shamal', 'MS', 1),
(2677, 173, 'Umm Sa\'id', 'UD', 1),
(2678, 173, 'Umm Salal', 'UL', 1),
(2679, 175, 'Alba', 'AB', 1),
(2680, 175, 'Arad', 'AR', 1),
(2681, 175, 'Arges', 'AG', 1),
(2682, 175, 'Bacau', 'BC', 1),
(2683, 175, 'Bihor', 'BH', 1),
(2684, 175, 'Bistrita-Nasaud', 'BN', 1),
(2685, 175, 'Botosani', 'BT', 1),
(2686, 175, 'Brasov', 'BV', 1),
(2687, 175, 'Braila', 'BR', 1),
(2688, 175, 'Bucuresti', 'B', 1),
(2689, 175, 'Buzau', 'BZ', 1),
(2690, 175, 'Caras-Severin', 'CS', 1),
(2691, 175, 'Calarasi', 'CL', 1),
(2692, 175, 'Cluj', 'CJ', 1),
(2693, 175, 'Constanta', 'CT', 1),
(2694, 175, 'Covasna', 'CV', 1),
(2695, 175, 'Dimbovita', 'DB', 1),
(2696, 175, 'Dolj', 'DJ', 1),
(2697, 175, 'Galati', 'GL', 1),
(2698, 175, 'Giurgiu', 'GR', 1),
(2699, 175, 'Gorj', 'GJ', 1),
(2700, 175, 'Harghita', 'HR', 1),
(2701, 175, 'Hunedoara', 'HD', 1),
(2702, 175, 'Ialomita', 'IL', 1),
(2703, 175, 'Iasi', 'IS', 1),
(2704, 175, 'Ilfov', 'IF', 1),
(2705, 175, 'Maramures', 'MM', 1),
(2706, 175, 'Mehedinti', 'MH', 1),
(2707, 175, 'Mures', 'MS', 1),
(2708, 175, 'Neamt', 'NT', 1),
(2709, 175, 'Olt', 'OT', 1),
(2710, 175, 'Prahova', 'PH', 1),
(2711, 175, 'Satu-Mare', 'SM', 1),
(2712, 175, 'Salaj', 'SJ', 1),
(2713, 175, 'Sibiu', 'SB', 1),
(2714, 175, 'Suceava', 'SV', 1),
(2715, 175, 'Teleorman', 'TR', 1),
(2716, 175, 'Timis', 'TM', 1),
(2717, 175, 'Tulcea', 'TL', 1),
(2718, 175, 'Vaslui', 'VS', 1),
(2719, 175, 'Valcea', 'VL', 1),
(2720, 175, 'Vrancea', 'VN', 1),
(2721, 176, 'Abakan', 'AB', 1),
(2722, 176, 'Aginskoye', 'AG', 1),
(2723, 176, 'Anadyr', 'AN', 1),
(2724, 176, 'Arkahangelsk', 'AR', 1),
(2725, 176, 'Astrakhan', 'AS', 1),
(2726, 176, 'Barnaul', 'BA', 1),
(2727, 176, 'Belgorod', 'BE', 1),
(2728, 176, 'Birobidzhan', 'BI', 1),
(2729, 176, 'Blagoveshchensk', 'BL', 1),
(2730, 176, 'Bryansk', 'BR', 1),
(2731, 176, 'Cheboksary', 'CH', 1),
(2732, 176, 'Chelyabinsk', 'CL', 1),
(2733, 176, 'Cherkessk', 'CR', 1),
(2734, 176, 'Chita', 'CI', 1),
(2735, 176, 'Dudinka', 'DU', 1),
(2736, 176, 'Elista', 'EL', 1),
(2738, 176, 'Gorno-Altaysk', 'GA', 1),
(2739, 176, 'Groznyy', 'GR', 1),
(2740, 176, 'Irkutsk', 'IR', 1),
(2741, 176, 'Ivanovo', 'IV', 1),
(2742, 176, 'Izhevsk', 'IZ', 1),
(2743, 176, 'Kalinigrad', 'KA', 1),
(2744, 176, 'Kaluga', 'KL', 1),
(2745, 176, 'Kasnodar', 'KS', 1),
(2746, 176, 'Kazan', 'KZ', 1),
(2747, 176, 'Kemerovo', 'KE', 1),
(2748, 176, 'Khabarovsk', 'KH', 1),
(2749, 176, 'Khanty-Mansiysk', 'KM', 1),
(2750, 176, 'Kostroma', 'KO', 1),
(2751, 176, 'Krasnodar', 'KR', 1),
(2752, 176, 'Krasnoyarsk', 'KN', 1),
(2753, 176, 'Kudymkar', 'KU', 1),
(2754, 176, 'Kurgan', 'KG', 1),
(2755, 176, 'Kursk', 'KK', 1),
(2756, 176, 'Kyzyl', 'KY', 1),
(2757, 176, 'Lipetsk', 'LI', 1),
(2758, 176, 'Magadan', 'MA', 1),
(2759, 176, 'Makhachkala', 'MK', 1),
(2760, 176, 'Maykop', 'MY', 1),
(2761, 176, 'Moscow', 'MO', 1),
(2762, 176, 'Murmansk', 'MU', 1),
(2763, 176, 'Nalchik', 'NA', 1),
(2764, 176, 'Naryan Mar', 'NR', 1),
(2765, 176, 'Nazran', 'NZ', 1),
(2766, 176, 'Nizhniy Novgorod', 'NI', 1),
(2767, 176, 'Novgorod', 'NO', 1),
(2768, 176, 'Novosibirsk', 'NV', 1),
(2769, 176, 'Omsk', 'OM', 1),
(2770, 176, 'Orel', 'OR', 1),
(2771, 176, 'Orenburg', 'OE', 1),
(2772, 176, 'Palana', 'PA', 1),
(2773, 176, 'Penza', 'PE', 1),
(2774, 176, 'Perm', 'PR', 1),
(2775, 176, 'Petropavlovsk-Kamchatskiy', 'PK', 1),
(2776, 176, 'Petrozavodsk', 'PT', 1),
(2777, 176, 'Pskov', 'PS', 1),
(2778, 176, 'Rostov-na-Donu', 'RO', 1),
(2779, 176, 'Ryazan', 'RY', 1),
(2780, 176, 'Salekhard', 'SL', 1),
(2781, 176, 'Samara', 'SA', 1),
(2782, 176, 'Saransk', 'SR', 1),
(2783, 176, 'Saratov', 'SV', 1),
(2784, 176, 'Smolensk', 'SM', 1),
(2785, 176, 'St. Petersburg', 'SP', 1),
(2786, 176, 'Stavropol', 'ST', 1),
(2787, 176, 'Syktyvkar', 'SY', 1),
(2788, 176, 'Tambov', 'TA', 1),
(2789, 176, 'Tomsk', 'TO', 1),
(2790, 176, 'Tula', 'TU', 1),
(2791, 176, 'Tura', 'TR', 1),
(2792, 176, 'Tver', 'TV', 1),
(2793, 176, 'Tyumen', 'TY', 1),
(2794, 176, 'Ufa', 'UF', 1),
(2795, 176, 'Ul\'yanovsk', 'UL', 1),
(2796, 176, 'Ulan-Ude', 'UU', 1),
(2797, 176, 'Ust\'-Ordynskiy', 'US', 1),
(2798, 176, 'Vladikavkaz', 'VL', 1),
(2799, 176, 'Vladimir', 'VA', 1),
(2800, 176, 'Vladivostok', 'VV', 1),
(2801, 176, 'Volgograd', 'VG', 1),
(2802, 176, 'Vologda', 'VD', 1),
(2803, 176, 'Voronezh', 'VO', 1),
(2804, 176, 'Vyatka', 'VY', 1),
(2805, 176, 'Yakutsk', 'YA', 1),
(2806, 176, 'Yaroslavl', 'YR', 1),
(2807, 176, 'Yekaterinburg', 'YE', 1),
(2808, 176, 'Yoshkar-Ola', 'YO', 1),
(2809, 177, 'Butare', 'BU', 1),
(2810, 177, 'Byumba', 'BY', 1),
(2811, 177, 'Cyangugu', 'CY', 1),
(2812, 177, 'Gikongoro', 'GK', 1),
(2813, 177, 'Gisenyi', 'GS', 1),
(2814, 177, 'Gitarama', 'GT', 1),
(2815, 177, 'Kibungo', 'KG', 1),
(2816, 177, 'Kibuye', 'KY', 1),
(2817, 177, 'Kigali Rurale', 'KR', 1),
(2818, 177, 'Kigali-ville', 'KV', 1),
(2819, 177, 'Ruhengeri', 'RU', 1),
(2820, 177, 'Umutara', 'UM', 1),
(2821, 178, 'Christ Church Nichola Town', 'CCN', 1),
(2822, 178, 'Saint Anne Sandy Point', 'SAS', 1),
(2823, 178, 'Saint George Basseterre', 'SGB', 1),
(2824, 178, 'Saint George Gingerland', 'SGG', 1),
(2825, 178, 'Saint James Windward', 'SJW', 1),
(2826, 178, 'Saint John Capesterre', 'SJC', 1),
(2827, 178, 'Saint John Figtree', 'SJF', 1),
(2828, 178, 'Saint Mary Cayon', 'SMC', 1),
(2829, 178, 'Saint Paul Capesterre', 'CAP', 1),
(2830, 178, 'Saint Paul Charlestown', 'CHA', 1),
(2831, 178, 'Saint Peter Basseterre', 'SPB', 1),
(2832, 178, 'Saint Thomas Lowland', 'STL', 1),
(2833, 178, 'Saint Thomas Middle Island', 'STM', 1),
(2834, 178, 'Trinity Palmetto Point', 'TPP', 1),
(2835, 179, 'Anse-la-Raye', 'AR', 1),
(2836, 179, 'Castries', 'CA', 1),
(2837, 179, 'Choiseul', 'CH', 1),
(2838, 179, 'Dauphin', 'DA', 1),
(2839, 179, 'Dennery', 'DE', 1),
(2840, 179, 'Gros-Islet', 'GI', 1),
(2841, 179, 'Laborie', 'LA', 1),
(2842, 179, 'Micoud', 'MI', 1),
(2843, 179, 'Praslin', 'PR', 1),
(2844, 179, 'Soufriere', 'SO', 1),
(2845, 179, 'Vieux-Fort', 'VF', 1),
(2846, 180, 'Charlotte', 'C', 1),
(2847, 180, 'Grenadines', 'R', 1),
(2848, 180, 'Saint Andrew', 'A', 1),
(2849, 180, 'Saint David', 'D', 1),
(2850, 180, 'Saint George', 'G', 1),
(2851, 180, 'Saint Patrick', 'P', 1),
(2852, 181, 'A\'ana', 'AN', 1),
(2853, 181, 'Aiga-i-le-Tai', 'AI', 1),
(2854, 181, 'Atua', 'AT', 1),
(2855, 181, 'Fa\'asaleleaga', 'FA', 1),
(2856, 181, 'Gaga\'emauga', 'GE', 1),
(2857, 181, 'Gagaifomauga', 'GF', 1),
(2858, 181, 'Palauli', 'PA', 1),
(2859, 181, 'Satupa\'itea', 'SA', 1),
(2860, 181, 'Tuamasaga', 'TU', 1),
(2861, 181, 'Va\'a-o-Fonoti', 'VF', 1),
(2862, 181, 'Vaisigano', 'VS', 1),
(2863, 182, 'Acquaviva', 'AC', 1),
(2864, 182, 'Borgo Maggiore', 'BM', 1),
(2865, 182, 'Chiesanuova', 'CH', 1),
(2866, 182, 'Domagnano', 'DO', 1),
(2867, 182, 'Faetano', 'FA', 1),
(2868, 182, 'Fiorentino', 'FI', 1),
(2869, 182, 'Montegiardino', 'MO', 1),
(2870, 182, 'Citta di San Marino', 'SM', 1),
(2871, 182, 'Serravalle', 'SE', 1),
(2872, 183, 'Sao Tome', 'S', 1),
(2873, 183, 'Principe', 'P', 1),
(2874, 184, 'Al Bahah', 'BH', 1),
(2875, 184, 'Al Hudud ash Shamaliyah', 'HS', 1),
(2876, 184, 'Al Jawf', 'JF', 1),
(2877, 184, 'Al Madinah', 'MD', 1),
(2878, 184, 'Al Qasim', 'QS', 1),
(2879, 184, 'Ar Riyad', 'RD', 1),
(2880, 184, 'Ash Sharqiyah (Eastern)', 'AQ', 1),
(2881, 184, '\'Asir', 'AS', 1),
(2882, 184, 'Ha\'il', 'HL', 1),
(2883, 184, 'Jizan', 'JZ', 1),
(2884, 184, 'Makkah', 'ML', 1),
(2885, 184, 'Najran', 'NR', 1),
(2886, 184, 'Tabuk', 'TB', 1),
(2887, 185, 'Dakar', 'DA', 1),
(2888, 185, 'Diourbel', 'DI', 1),
(2889, 185, 'Fatick', 'FA', 1),
(2890, 185, 'Kaolack', 'KA', 1),
(2891, 185, 'Kolda', 'KO', 1),
(2892, 185, 'Louga', 'LO', 1),
(2893, 185, 'Matam', 'MA', 1),
(2894, 185, 'Saint-Louis', 'SL', 1),
(2895, 185, 'Tambacounda', 'TA', 1),
(2896, 185, 'Thies', 'TH', 1),
(2897, 185, 'Ziguinchor', 'ZI', 1),
(2898, 186, 'Anse aux Pins', 'AP', 1),
(2899, 186, 'Anse Boileau', 'AB', 1),
(2900, 186, 'Anse Etoile', 'AE', 1),
(2901, 186, 'Anse Louis', 'AL', 1),
(2902, 186, 'Anse Royale', 'AR', 1),
(2903, 186, 'Baie Lazare', 'BL', 1),
(2904, 186, 'Baie Sainte Anne', 'BS', 1),
(2905, 186, 'Beau Vallon', 'BV', 1),
(2906, 186, 'Bel Air', 'BA', 1),
(2907, 186, 'Bel Ombre', 'BO', 1),
(2908, 186, 'Cascade', 'CA', 1),
(2909, 186, 'Glacis', 'GL', 1),
(2910, 186, 'Grand\' Anse (on Mahe)', 'GM', 1),
(2911, 186, 'Grand\' Anse (on Praslin)', 'GP', 1),
(2912, 186, 'La Digue', 'DG', 1),
(2913, 186, 'La Riviere Anglaise', 'RA', 1),
(2914, 186, 'Mont Buxton', 'MB', 1),
(2915, 186, 'Mont Fleuri', 'MF', 1),
(2916, 186, 'Plaisance', 'PL', 1),
(2917, 186, 'Pointe La Rue', 'PR', 1),
(2918, 186, 'Port Glaud', 'PG', 1),
(2919, 186, 'Saint Louis', 'SL', 1),
(2920, 186, 'Takamaka', 'TA', 1),
(2921, 187, 'Eastern', 'E', 1),
(2922, 187, 'Northern', 'N', 1),
(2923, 187, 'Southern', 'S', 1),
(2924, 187, 'Western', 'W', 1),
(2925, 189, 'Banskobystrický', 'BA', 1),
(2926, 189, 'Bratislavský', 'BR', 1),
(2927, 189, 'Košický', 'KO', 1),
(2928, 189, 'Nitriansky', 'NI', 1),
(2929, 189, 'Prešovský', 'PR', 1),
(2930, 189, 'Trenčiansky', 'TC', 1),
(2931, 189, 'Trnavský', 'TV', 1),
(2932, 189, 'Žilinský', 'ZI', 1),
(2933, 191, 'Central', 'CE', 1),
(2934, 191, 'Choiseul', 'CH', 1),
(2935, 191, 'Guadalcanal', 'GC', 1),
(2936, 191, 'Honiara', 'HO', 1),
(2937, 191, 'Isabel', 'IS', 1),
(2938, 191, 'Makira', 'MK', 1),
(2939, 191, 'Malaita', 'ML', 1),
(2940, 191, 'Rennell and Bellona', 'RB', 1),
(2941, 191, 'Temotu', 'TM', 1),
(2942, 191, 'Western', 'WE', 1),
(2943, 192, 'Awdal', 'AW', 1),
(2944, 192, 'Bakool', 'BK', 1),
(2945, 192, 'Banaadir', 'BN', 1),
(2946, 192, 'Bari', 'BR', 1),
(2947, 192, 'Bay', 'BY', 1),
(2948, 192, 'Galguduud', 'GA', 1),
(2949, 192, 'Gedo', 'GE', 1),
(2950, 192, 'Hiiraan', 'HI', 1),
(2951, 192, 'Jubbada Dhexe', 'JD', 1),
(2952, 192, 'Jubbada Hoose', 'JH', 1),
(2953, 192, 'Mudug', 'MU', 1),
(2954, 192, 'Nugaal', 'NU', 1),
(2955, 192, 'Sanaag', 'SA', 1),
(2956, 192, 'Shabeellaha Dhexe', 'SD', 1),
(2957, 192, 'Shabeellaha Hoose', 'SH', 1),
(2958, 192, 'Sool', 'SL', 1),
(2959, 192, 'Togdheer', 'TO', 1),
(2960, 192, 'Woqooyi Galbeed', 'WG', 1),
(2961, 193, 'Eastern Cape', 'EC', 1),
(2962, 193, 'Free State', 'FS', 1),
(2963, 193, 'Gauteng', 'GT', 1),
(2964, 193, 'KwaZulu-Natal', 'KN', 1),
(2965, 193, 'Limpopo', 'LP', 1),
(2966, 193, 'Mpumalanga', 'MP', 1),
(2967, 193, 'North West', 'NW', 1),
(2968, 193, 'Northern Cape', 'NC', 1),
(2969, 193, 'Western Cape', 'WC', 1),
(2970, 195, 'La Coru&ntilde;a', 'CA', 1),
(2971, 195, '&Aacute;lava', 'AL', 1),
(2972, 195, 'Albacete', 'AB', 1),
(2973, 195, 'Alicante', 'AC', 1),
(2974, 195, 'Almeria', 'AM', 1),
(2975, 195, 'Asturias', 'AS', 1),
(2976, 195, '&Aacute;vila', 'AV', 1),
(2977, 195, 'Badajoz', 'BJ', 1),
(2978, 195, 'Baleares', 'IB', 1),
(2979, 195, 'Barcelona', 'BA', 1),
(2980, 195, 'Burgos', 'BU', 1),
(2981, 195, 'C&aacute;ceres', 'CC', 1),
(2982, 195, 'C&aacute;diz', 'CZ', 1),
(2983, 195, 'Cantabria', 'CT', 1),
(2984, 195, 'Castell&oacute;n', 'CL', 1),
(2985, 195, 'Ceuta', 'CE', 1),
(2986, 195, 'Ciudad Real', 'CR', 1),
(2987, 195, 'C&oacute;rdoba', 'CD', 1),
(2988, 195, 'Cuenca', 'CU', 1),
(2989, 195, 'Girona', 'GI', 1),
(2990, 195, 'Granada', 'GD', 1),
(2991, 195, 'Guadalajara', 'GJ', 1),
(2992, 195, 'Guip&uacute;zcoa', 'GP', 1),
(2993, 195, 'Huelva', 'HL', 1),
(2994, 195, 'Huesca', 'HS', 1),
(2995, 195, 'Ja&eacute;n', 'JN', 1),
(2996, 195, 'La Rioja', 'RJ', 1),
(2997, 195, 'Las Palmas', 'PM', 1),
(2998, 195, 'Leon', 'LE', 1),
(2999, 195, 'Lleida', 'LL', 1),
(3000, 195, 'Lugo', 'LG', 1),
(3001, 195, 'Madrid', 'MD', 1),
(3002, 195, 'Malaga', 'MA', 1),
(3003, 195, 'Melilla', 'ML', 1),
(3004, 195, 'Murcia', 'MU', 1),
(3005, 195, 'Navarra', 'NV', 1),
(3006, 195, 'Ourense', 'OU', 1),
(3007, 195, 'Palencia', 'PL', 1),
(3008, 195, 'Pontevedra', 'PO', 1),
(3009, 195, 'Salamanca', 'SL', 1),
(3010, 195, 'Santa Cruz de Tenerife', 'SC', 1),
(3011, 195, 'Segovia', 'SG', 1),
(3012, 195, 'Sevilla', 'SV', 1),
(3013, 195, 'Soria', 'SO', 1),
(3014, 195, 'Tarragona', 'TA', 1),
(3015, 195, 'Teruel', 'TE', 1),
(3016, 195, 'Toledo', 'TO', 1),
(3017, 195, 'Valencia', 'VC', 1),
(3018, 195, 'Valladolid', 'VD', 1),
(3019, 195, 'Vizcaya', 'VZ', 1),
(3020, 195, 'Zamora', 'ZM', 1),
(3021, 195, 'Zaragoza', 'ZR', 1),
(3022, 196, 'Central', 'CE', 1),
(3023, 196, 'Eastern', 'EA', 1),
(3024, 196, 'North Central', 'NC', 1),
(3025, 196, 'Northern', 'NO', 1),
(3026, 196, 'North Western', 'NW', 1),
(3027, 196, 'Sabaragamuwa', 'SA', 1),
(3028, 196, 'Southern', 'SO', 1),
(3029, 196, 'Uva', 'UV', 1),
(3030, 196, 'Western', 'WE', 1),
(3032, 197, 'Saint Helena', 'S', 1),
(3034, 199, 'A\'ali an Nil', 'ANL', 1),
(3035, 199, 'Al Bahr al Ahmar', 'BAM', 1),
(3036, 199, 'Al Buhayrat', 'BRT', 1),
(3037, 199, 'Al Jazirah', 'JZR', 1),
(3038, 199, 'Al Khartum', 'KRT', 1),
(3039, 199, 'Al Qadarif', 'QDR', 1),
(3040, 199, 'Al Wahdah', 'WDH', 1),
(3041, 199, 'An Nil al Abyad', 'ANB', 1),
(3042, 199, 'An Nil al Azraq', 'ANZ', 1),
(3043, 199, 'Ash Shamaliyah', 'ASH', 1),
(3044, 199, 'Bahr al Jabal', 'BJA', 1),
(3045, 199, 'Gharb al Istiwa\'iyah', 'GIS', 1),
(3046, 199, 'Gharb Bahr al Ghazal', 'GBG', 1),
(3047, 199, 'Gharb Darfur', 'GDA', 1),
(3048, 199, 'Gharb Kurdufan', 'GKU', 1),
(3049, 199, 'Janub Darfur', 'JDA', 1),
(3050, 199, 'Janub Kurdufan', 'JKU', 1),
(3051, 199, 'Junqali', 'JQL', 1),
(3052, 199, 'Kassala', 'KSL', 1),
(3053, 199, 'Nahr an Nil', 'NNL', 1),
(3054, 199, 'Shamal Bahr al Ghazal', 'SBG', 1),
(3055, 199, 'Shamal Darfur', 'SDA', 1),
(3056, 199, 'Shamal Kurdufan', 'SKU', 1),
(3057, 199, 'Sharq al Istiwa\'iyah', 'SIS', 1),
(3058, 199, 'Sinnar', 'SNR', 1),
(3059, 199, 'Warab', 'WRB', 1),
(3060, 200, 'Brokopondo', 'BR', 1),
(3061, 200, 'Commewijne', 'CM', 1),
(3062, 200, 'Coronie', 'CR', 1),
(3063, 200, 'Marowijne', 'MA', 1),
(3064, 200, 'Nickerie', 'NI', 1),
(3065, 200, 'Para', 'PA', 1),
(3066, 200, 'Paramaribo', 'PM', 1),
(3067, 200, 'Saramacca', 'SA', 1),
(3068, 200, 'Sipaliwini', 'SI', 1),
(3069, 200, 'Wanica', 'WA', 1),
(3070, 202, 'Hhohho', 'H', 1),
(3071, 202, 'Lubombo', 'L', 1),
(3072, 202, 'Manzini', 'M', 1),
(3073, 202, 'Shishelweni', 'S', 1),
(3074, 203, 'Blekinge', 'K', 1),
(3075, 203, 'Dalarna', 'W', 1),
(3076, 203, 'Gävleborg', 'X', 1),
(3077, 203, 'Gotland', 'I', 1),
(3078, 203, 'Halland', 'N', 1),
(3079, 203, 'Jämtland', 'Z', 1),
(3080, 203, 'Jönköping', 'F', 1),
(3081, 203, 'Kalmar', 'H', 1),
(3082, 203, 'Kronoberg', 'G', 1),
(3083, 203, 'Norrbotten', 'BD', 1),
(3084, 203, 'Örebro', 'T', 1),
(3085, 203, 'Östergötland', 'E', 1),
(3086, 203, 'Sk&aring;ne', 'M', 1),
(3087, 203, 'Södermanland', 'D', 1),
(3088, 203, 'Stockholm', 'AB', 1),
(3089, 203, 'Uppsala', 'C', 1),
(3090, 203, 'Värmland', 'S', 1),
(3091, 203, 'Västerbotten', 'AC', 1),
(3092, 203, 'Västernorrland', 'Y', 1),
(3093, 203, 'Västmanland', 'U', 1),
(3094, 203, 'Västra Götaland', 'O', 1),
(3095, 204, 'Aargau', 'AG', 1),
(3096, 204, 'Appenzell Ausserrhoden', 'AR', 1),
(3097, 204, 'Appenzell Innerrhoden', 'AI', 1),
(3098, 204, 'Basel-Stadt', 'BS', 1),
(3099, 204, 'Basel-Landschaft', 'BL', 1),
(3100, 204, 'Bern', 'BE', 1),
(3101, 204, 'Fribourg', 'FR', 1),
(3102, 204, 'Gen&egrave;ve', 'GE', 1),
(3103, 204, 'Glarus', 'GL', 1),
(3104, 204, 'Graubünden', 'GR', 1),
(3105, 204, 'Jura', 'JU', 1),
(3106, 204, 'Luzern', 'LU', 1),
(3107, 204, 'Neuch&acirc;tel', 'NE', 1),
(3108, 204, 'Nidwald', 'NW', 1),
(3109, 204, 'Obwald', 'OW', 1),
(3110, 204, 'St. Gallen', 'SG', 1),
(3111, 204, 'Schaffhausen', 'SH', 1),
(3112, 204, 'Schwyz', 'SZ', 1),
(3113, 204, 'Solothurn', 'SO', 1),
(3114, 204, 'Thurgau', 'TG', 1),
(3115, 204, 'Ticino', 'TI', 1),
(3116, 204, 'Uri', 'UR', 1),
(3117, 204, 'Valais', 'VS', 1),
(3118, 204, 'Vaud', 'VD', 1),
(3119, 204, 'Zug', 'ZG', 1),
(3120, 204, 'Zürich', 'ZH', 1),
(3121, 205, 'Al Hasakah', 'HA', 1),
(3122, 205, 'Al Ladhiqiyah', 'LA', 1),
(3123, 205, 'Al Qunaytirah', 'QU', 1),
(3124, 205, 'Ar Raqqah', 'RQ', 1),
(3125, 205, 'As Suwayda', 'SU', 1),
(3126, 205, 'Dara', 'DA', 1),
(3127, 205, 'Dayr az Zawr', 'DZ', 1),
(3128, 205, 'Dimashq', 'DI', 1),
(3129, 205, 'Halab', 'HL', 1),
(3130, 205, 'Hamah', 'HM', 1),
(3131, 205, 'Hims', 'HI', 1),
(3132, 205, 'Idlib', 'ID', 1),
(3133, 205, 'Rif Dimashq', 'RD', 1),
(3134, 205, 'Tartus', 'TA', 1),
(3135, 206, 'Chang-hua', 'CH', 1),
(3136, 206, 'Chia-i', 'CI', 1);
INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(3137, 206, 'Hsin-chu', 'HS', 1),
(3138, 206, 'Hua-lien', 'HL', 1),
(3139, 206, 'I-lan', 'IL', 1),
(3140, 206, 'Kao-hsiung county', 'KH', 1),
(3141, 206, 'Kin-men', 'KM', 1),
(3142, 206, 'Lien-chiang', 'LC', 1),
(3143, 206, 'Miao-li', 'ML', 1),
(3144, 206, 'Nan-t\'ou', 'NT', 1),
(3145, 206, 'P\'eng-hu', 'PH', 1),
(3146, 206, 'P\'ing-tung', 'PT', 1),
(3147, 206, 'T\'ai-chung', 'TG', 1),
(3148, 206, 'T\'ai-nan', 'TA', 1),
(3149, 206, 'T\'ai-pei county', 'TP', 1),
(3150, 206, 'T\'ai-tung', 'TT', 1),
(3151, 206, 'T\'ao-yuan', 'TY', 1),
(3152, 206, 'Yun-lin', 'YL', 1),
(3153, 206, 'Chia-i city', 'CC', 1),
(3154, 206, 'Chi-lung', 'CL', 1),
(3155, 206, 'Hsin-chu', 'HC', 1),
(3156, 206, 'T\'ai-chung', 'TH', 1),
(3157, 206, 'T\'ai-nan', 'TN', 1),
(3158, 206, 'Kao-hsiung city', 'KC', 1),
(3159, 206, 'T\'ai-pei city', 'TC', 1),
(3160, 207, 'Gorno-Badakhstan', 'GB', 1),
(3161, 207, 'Khatlon', 'KT', 1),
(3162, 207, 'Sughd', 'SU', 1),
(3163, 208, 'Arusha', 'AR', 1),
(3164, 208, 'Dar es Salaam', 'DS', 1),
(3165, 208, 'Dodoma', 'DO', 1),
(3166, 208, 'Iringa', 'IR', 1),
(3167, 208, 'Kagera', 'KA', 1),
(3168, 208, 'Kigoma', 'KI', 1),
(3169, 208, 'Kilimanjaro', 'KJ', 1),
(3170, 208, 'Lindi', 'LN', 1),
(3171, 208, 'Manyara', 'MY', 1),
(3172, 208, 'Mara', 'MR', 1),
(3173, 208, 'Mbeya', 'MB', 1),
(3174, 208, 'Morogoro', 'MO', 1),
(3175, 208, 'Mtwara', 'MT', 1),
(3176, 208, 'Mwanza', 'MW', 1),
(3177, 208, 'Pemba North', 'PN', 1),
(3178, 208, 'Pemba South', 'PS', 1),
(3179, 208, 'Pwani', 'PW', 1),
(3180, 208, 'Rukwa', 'RK', 1),
(3181, 208, 'Ruvuma', 'RV', 1),
(3182, 208, 'Shinyanga', 'SH', 1),
(3183, 208, 'Singida', 'SI', 1),
(3184, 208, 'Tabora', 'TB', 1),
(3185, 208, 'Tanga', 'TN', 1),
(3186, 208, 'Zanzibar Central/South', 'ZC', 1),
(3187, 208, 'Zanzibar North', 'ZN', 1),
(3188, 208, 'Zanzibar Urban/West', 'ZU', 1),
(3189, 209, 'Amnat Charoen', 'Amnat Charoen', 1),
(3190, 209, 'Ang Thong', 'Ang Thong', 1),
(3191, 209, 'Ayutthaya', 'Ayutthaya', 1),
(3192, 209, 'Bangkok', 'Bangkok', 1),
(3193, 209, 'Buriram', 'Buriram', 1),
(3194, 209, 'Chachoengsao', 'Chachoengsao', 1),
(3195, 209, 'Chai Nat', 'Chai Nat', 1),
(3196, 209, 'Chaiyaphum', 'Chaiyaphum', 1),
(3197, 209, 'Chanthaburi', 'Chanthaburi', 1),
(3198, 209, 'Chiang Mai', 'Chiang Mai', 1),
(3199, 209, 'Chiang Rai', 'Chiang Rai', 1),
(3200, 209, 'Chon Buri', 'Chon Buri', 1),
(3201, 209, 'Chumphon', 'Chumphon', 1),
(3202, 209, 'Kalasin', 'Kalasin', 1),
(3203, 209, 'Kamphaeng Phet', 'Kamphaeng Phet', 1),
(3204, 209, 'Kanchanaburi', 'Kanchanaburi', 1),
(3205, 209, 'Khon Kaen', 'Khon Kaen', 1),
(3206, 209, 'Krabi', 'Krabi', 1),
(3207, 209, 'Lampang', 'Lampang', 1),
(3208, 209, 'Lamphun', 'Lamphun', 1),
(3209, 209, 'Loei', 'Loei', 1),
(3210, 209, 'Lop Buri', 'Lop Buri', 1),
(3211, 209, 'Mae Hong Son', 'Mae Hong Son', 1),
(3212, 209, 'Maha Sarakham', 'Maha Sarakham', 1),
(3213, 209, 'Mukdahan', 'Mukdahan', 1),
(3214, 209, 'Nakhon Nayok', 'Nakhon Nayok', 1),
(3215, 209, 'Nakhon Pathom', 'Nakhon Pathom', 1),
(3216, 209, 'Nakhon Phanom', 'Nakhon Phanom', 1),
(3217, 209, 'Nakhon Ratchasima', 'Nakhon Ratchasima', 1),
(3218, 209, 'Nakhon Sawan', 'Nakhon Sawan', 1),
(3219, 209, 'Nakhon Si Thammarat', 'Nakhon Si Thammarat', 1),
(3220, 209, 'Nan', 'Nan', 1),
(3221, 209, 'Narathiwat', 'Narathiwat', 1),
(3222, 209, 'Nong Bua Lamphu', 'Nong Bua Lamphu', 1),
(3223, 209, 'Nong Khai', 'Nong Khai', 1),
(3224, 209, 'Nonthaburi', 'Nonthaburi', 1),
(3225, 209, 'Pathum Thani', 'Pathum Thani', 1),
(3226, 209, 'Pattani', 'Pattani', 1),
(3227, 209, 'Phangnga', 'Phangnga', 1),
(3228, 209, 'Phatthalung', 'Phatthalung', 1),
(3229, 209, 'Phayao', 'Phayao', 1),
(3230, 209, 'Phetchabun', 'Phetchabun', 1),
(3231, 209, 'Phetchaburi', 'Phetchaburi', 1),
(3232, 209, 'Phichit', 'Phichit', 1),
(3233, 209, 'Phitsanulok', 'Phitsanulok', 1),
(3234, 209, 'Phrae', 'Phrae', 1),
(3235, 209, 'Phuket', 'Phuket', 1),
(3236, 209, 'Prachin Buri', 'Prachin Buri', 1),
(3237, 209, 'Prachuap Khiri Khan', 'Prachuap Khiri Khan', 1),
(3238, 209, 'Ranong', 'Ranong', 1),
(3239, 209, 'Ratchaburi', 'Ratchaburi', 1),
(3240, 209, 'Rayong', 'Rayong', 1),
(3241, 209, 'Roi Et', 'Roi Et', 1),
(3242, 209, 'Sa Kaeo', 'Sa Kaeo', 1),
(3243, 209, 'Sakon Nakhon', 'Sakon Nakhon', 1),
(3244, 209, 'Samut Prakan', 'Samut Prakan', 1),
(3245, 209, 'Samut Sakhon', 'Samut Sakhon', 1),
(3246, 209, 'Samut Songkhram', 'Samut Songkhram', 1),
(3247, 209, 'Sara Buri', 'Sara Buri', 1),
(3248, 209, 'Satun', 'Satun', 1),
(3249, 209, 'Sing Buri', 'Sing Buri', 1),
(3250, 209, 'Sisaket', 'Sisaket', 1),
(3251, 209, 'Songkhla', 'Songkhla', 1),
(3252, 209, 'Sukhothai', 'Sukhothai', 1),
(3253, 209, 'Suphan Buri', 'Suphan Buri', 1),
(3254, 209, 'Surat Thani', 'Surat Thani', 1),
(3255, 209, 'Surin', 'Surin', 1),
(3256, 209, 'Tak', 'Tak', 1),
(3257, 209, 'Trang', 'Trang', 1),
(3258, 209, 'Trat', 'Trat', 1),
(3259, 209, 'Ubon Ratchathani', 'Ubon Ratchathani', 1),
(3260, 209, 'Udon Thani', 'Udon Thani', 1),
(3261, 209, 'Uthai Thani', 'Uthai Thani', 1),
(3262, 209, 'Uttaradit', 'Uttaradit', 1),
(3263, 209, 'Yala', 'Yala', 1),
(3264, 209, 'Yasothon', 'Yasothon', 1),
(3265, 210, 'Kara', 'K', 1),
(3266, 210, 'Plateaux', 'P', 1),
(3267, 210, 'Savanes', 'S', 1),
(3268, 210, 'Centrale', 'C', 1),
(3269, 210, 'Maritime', 'M', 1),
(3270, 211, 'Atafu', 'A', 1),
(3271, 211, 'Fakaofo', 'F', 1),
(3272, 211, 'Nukunonu', 'N', 1),
(3273, 212, 'Ha\'apai', 'H', 1),
(3274, 212, 'Tongatapu', 'T', 1),
(3275, 212, 'Vava\'u', 'V', 1),
(3276, 213, 'Couva/Tabaquite/Talparo', 'CT', 1),
(3277, 213, 'Diego Martin', 'DM', 1),
(3278, 213, 'Mayaro/Rio Claro', 'MR', 1),
(3279, 213, 'Penal/Debe', 'PD', 1),
(3280, 213, 'Princes Town', 'PT', 1),
(3281, 213, 'Sangre Grande', 'SG', 1),
(3282, 213, 'San Juan/Laventille', 'SL', 1),
(3283, 213, 'Siparia', 'SI', 1),
(3284, 213, 'Tunapuna/Piarco', 'TP', 1),
(3285, 213, 'Port of Spain', 'PS', 1),
(3286, 213, 'San Fernando', 'SF', 1),
(3287, 213, 'Arima', 'AR', 1),
(3288, 213, 'Point Fortin', 'PF', 1),
(3289, 213, 'Chaguanas', 'CH', 1),
(3290, 213, 'Tobago', 'TO', 1),
(3291, 214, 'Ariana', 'AR', 1),
(3292, 214, 'Beja', 'BJ', 1),
(3293, 214, 'Ben Arous', 'BA', 1),
(3294, 214, 'Bizerte', 'BI', 1),
(3295, 214, 'Gabes', 'GB', 1),
(3296, 214, 'Gafsa', 'GF', 1),
(3297, 214, 'Jendouba', 'JE', 1),
(3298, 214, 'Kairouan', 'KR', 1),
(3299, 214, 'Kasserine', 'KS', 1),
(3300, 214, 'Kebili', 'KB', 1),
(3301, 214, 'Kef', 'KF', 1),
(3302, 214, 'Mahdia', 'MH', 1),
(3303, 214, 'Manouba', 'MN', 1),
(3304, 214, 'Medenine', 'ME', 1),
(3305, 214, 'Monastir', 'MO', 1),
(3306, 214, 'Nabeul', 'NA', 1),
(3307, 214, 'Sfax', 'SF', 1),
(3308, 214, 'Sidi', 'SD', 1),
(3309, 214, 'Siliana', 'SL', 1),
(3310, 214, 'Sousse', 'SO', 1),
(3311, 214, 'Tataouine', 'TA', 1),
(3312, 214, 'Tozeur', 'TO', 1),
(3313, 214, 'Tunis', 'TU', 1),
(3314, 214, 'Zaghouan', 'ZA', 1),
(3315, 215, 'Adana', 'ADA', 1),
(3316, 215, 'Adıyaman', 'ADI', 1),
(3317, 215, 'Afyonkarahisar', 'AFY', 1),
(3318, 215, 'Ağrı', 'AGR', 1),
(3319, 215, 'Aksaray', 'AKS', 1),
(3320, 215, 'Amasya', 'AMA', 1),
(3321, 215, 'Ankara', 'ANK', 1),
(3322, 215, 'Antalya', 'ANT', 1),
(3323, 215, 'Ardahan', 'ARD', 1),
(3324, 215, 'Artvin', 'ART', 1),
(3325, 215, 'Aydın', 'AYI', 1),
(3326, 215, 'Balıkesir', 'BAL', 1),
(3327, 215, 'Bartın', 'BAR', 1),
(3328, 215, 'Batman', 'BAT', 1),
(3329, 215, 'Bayburt', 'BAY', 1),
(3330, 215, 'Bilecik', 'BIL', 1),
(3331, 215, 'Bingöl', 'BIN', 1),
(3332, 215, 'Bitlis', 'BIT', 1),
(3333, 215, 'Bolu', 'BOL', 1),
(3334, 215, 'Burdur', 'BRD', 1),
(3335, 215, 'Bursa', 'BRS', 1),
(3336, 215, 'Çanakkale', 'CKL', 1),
(3337, 215, 'Çankırı', 'CKR', 1),
(3338, 215, 'Çorum', 'COR', 1),
(3339, 215, 'Denizli', 'DEN', 1),
(3340, 215, 'Diyarbakır', 'DIY', 1),
(3341, 215, 'Düzce', 'DUZ', 1),
(3342, 215, 'Edirne', 'EDI', 1),
(3343, 215, 'Elazığ', 'ELA', 1),
(3344, 215, 'Erzincan', 'EZC', 1),
(3345, 215, 'Erzurum', 'EZR', 1),
(3346, 215, 'Eskişehir', 'ESK', 1),
(3347, 215, 'Gaziantep', 'GAZ', 1),
(3348, 215, 'Giresun', 'GIR', 1),
(3349, 215, 'Gümüşhane', 'GMS', 1),
(3350, 215, 'Hakkari', 'HKR', 1),
(3351, 215, 'Hatay', 'HTY', 1),
(3352, 215, 'Iğdır', 'IGD', 1),
(3353, 215, 'Isparta', 'ISP', 1),
(3354, 215, 'İstanbul', 'IST', 1),
(3355, 215, 'İzmir', 'IZM', 1),
(3356, 215, 'Kahramanmaraş', 'KAH', 1),
(3357, 215, 'Karabük', 'KRB', 1),
(3358, 215, 'Karaman', 'KRM', 1),
(3359, 215, 'Kars', 'KRS', 1),
(3360, 215, 'Kastamonu', 'KAS', 1),
(3361, 215, 'Kayseri', 'KAY', 1),
(3362, 215, 'Kilis', 'KLS', 1),
(3363, 215, 'Kırıkkale', 'KRK', 1),
(3364, 215, 'Kırklareli', 'KLR', 1),
(3365, 215, 'Kırşehir', 'KRH', 1),
(3366, 215, 'Kocaeli', 'KOC', 1),
(3367, 215, 'Konya', 'KON', 1),
(3368, 215, 'Kütahya', 'KUT', 1),
(3369, 215, 'Malatya', 'MAL', 1),
(3370, 215, 'Manisa', 'MAN', 1),
(3371, 215, 'Mardin', 'MAR', 1),
(3372, 215, 'Mersin', 'MER', 1),
(3373, 215, 'Muğla', 'MUG', 1),
(3374, 215, 'Muş', 'MUS', 1),
(3375, 215, 'Nevşehir', 'NEV', 1),
(3376, 215, 'Niğde', 'NIG', 1),
(3377, 215, 'Ordu', 'ORD', 1),
(3378, 215, 'Osmaniye', 'OSM', 1),
(3379, 215, 'Rize', 'RIZ', 1),
(3380, 215, 'Sakarya', 'SAK', 1),
(3381, 215, 'Samsun', 'SAM', 1),
(3382, 215, 'Şanlıurfa', 'SAN', 1),
(3383, 215, 'Siirt', 'SII', 1),
(3384, 215, 'Sinop', 'SIN', 1),
(3385, 215, 'Şırnak', 'SIR', 1),
(3386, 215, 'Sivas', 'SIV', 1),
(3387, 215, 'Tekirdağ', 'TEL', 1),
(3388, 215, 'Tokat', 'TOK', 1),
(3389, 215, 'Trabzon', 'TRA', 1),
(3390, 215, 'Tunceli', 'TUN', 1),
(3391, 215, 'Uşak', 'USK', 1),
(3392, 215, 'Van', 'VAN', 1),
(3393, 215, 'Yalova', 'YAL', 1),
(3394, 215, 'Yozgat', 'YOZ', 1),
(3395, 215, 'Zonguldak', 'ZON', 1),
(3396, 216, 'Ahal Welayaty', 'A', 1),
(3397, 216, 'Balkan Welayaty', 'B', 1),
(3398, 216, 'Dashhowuz Welayaty', 'D', 1),
(3399, 216, 'Lebap Welayaty', 'L', 1),
(3400, 216, 'Mary Welayaty', 'M', 1),
(3401, 217, 'Ambergris Cays', 'AC', 1),
(3402, 217, 'Dellis Cay', 'DC', 1),
(3403, 217, 'French Cay', 'FC', 1),
(3404, 217, 'Little Water Cay', 'LW', 1),
(3405, 217, 'Parrot Cay', 'RC', 1),
(3406, 217, 'Pine Cay', 'PN', 1),
(3407, 217, 'Salt Cay', 'SL', 1),
(3408, 217, 'Grand Turk', 'GT', 1),
(3409, 217, 'South Caicos', 'SC', 1),
(3410, 217, 'East Caicos', 'EC', 1),
(3411, 217, 'Middle Caicos', 'MC', 1),
(3412, 217, 'North Caicos', 'NC', 1),
(3413, 217, 'Providenciales', 'PR', 1),
(3414, 217, 'West Caicos', 'WC', 1),
(3415, 218, 'Nanumanga', 'NMG', 1),
(3416, 218, 'Niulakita', 'NLK', 1),
(3417, 218, 'Niutao', 'NTO', 1),
(3418, 218, 'Funafuti', 'FUN', 1),
(3419, 218, 'Nanumea', 'NME', 1),
(3420, 218, 'Nui', 'NUI', 1),
(3421, 218, 'Nukufetau', 'NFT', 1),
(3422, 218, 'Nukulaelae', 'NLL', 1),
(3423, 218, 'Vaitupu', 'VAI', 1),
(3424, 219, 'Kalangala', 'KAL', 1),
(3425, 219, 'Kampala', 'KMP', 1),
(3426, 219, 'Kayunga', 'KAY', 1),
(3427, 219, 'Kiboga', 'KIB', 1),
(3428, 219, 'Luwero', 'LUW', 1),
(3429, 219, 'Masaka', 'MAS', 1),
(3430, 219, 'Mpigi', 'MPI', 1),
(3431, 219, 'Mubende', 'MUB', 1),
(3432, 219, 'Mukono', 'MUK', 1),
(3433, 219, 'Nakasongola', 'NKS', 1),
(3434, 219, 'Rakai', 'RAK', 1),
(3435, 219, 'Sembabule', 'SEM', 1),
(3436, 219, 'Wakiso', 'WAK', 1),
(3437, 219, 'Bugiri', 'BUG', 1),
(3438, 219, 'Busia', 'BUS', 1),
(3439, 219, 'Iganga', 'IGA', 1),
(3440, 219, 'Jinja', 'JIN', 1),
(3441, 219, 'Kaberamaido', 'KAB', 1),
(3442, 219, 'Kamuli', 'KML', 1),
(3443, 219, 'Kapchorwa', 'KPC', 1),
(3444, 219, 'Katakwi', 'KTK', 1),
(3445, 219, 'Kumi', 'KUM', 1),
(3446, 219, 'Mayuge', 'MAY', 1),
(3447, 219, 'Mbale', 'MBA', 1),
(3448, 219, 'Pallisa', 'PAL', 1),
(3449, 219, 'Sironko', 'SIR', 1),
(3450, 219, 'Soroti', 'SOR', 1),
(3451, 219, 'Tororo', 'TOR', 1),
(3452, 219, 'Adjumani', 'ADJ', 1),
(3453, 219, 'Apac', 'APC', 1),
(3454, 219, 'Arua', 'ARU', 1),
(3455, 219, 'Gulu', 'GUL', 1),
(3456, 219, 'Kitgum', 'KIT', 1),
(3457, 219, 'Kotido', 'KOT', 1),
(3458, 219, 'Lira', 'LIR', 1),
(3459, 219, 'Moroto', 'MRT', 1),
(3460, 219, 'Moyo', 'MOY', 1),
(3461, 219, 'Nakapiripirit', 'NAK', 1),
(3462, 219, 'Nebbi', 'NEB', 1),
(3463, 219, 'Pader', 'PAD', 1),
(3464, 219, 'Yumbe', 'YUM', 1),
(3465, 219, 'Bundibugyo', 'BUN', 1),
(3466, 219, 'Bushenyi', 'BSH', 1),
(3467, 219, 'Hoima', 'HOI', 1),
(3468, 219, 'Kabale', 'KBL', 1),
(3469, 219, 'Kabarole', 'KAR', 1),
(3470, 219, 'Kamwenge', 'KAM', 1),
(3471, 219, 'Kanungu', 'KAN', 1),
(3472, 219, 'Kasese', 'KAS', 1),
(3473, 219, 'Kibaale', 'KBA', 1),
(3474, 219, 'Kisoro', 'KIS', 1),
(3475, 219, 'Kyenjojo', 'KYE', 1),
(3476, 219, 'Masindi', 'MSN', 1),
(3477, 219, 'Mbarara', 'MBR', 1),
(3478, 219, 'Ntungamo', 'NTU', 1),
(3479, 219, 'Rukungiri', 'RUK', 1),
(3480, 220, 'Cherkas\'ka Oblast\'', '71', 1),
(3481, 220, 'Chernihivs\'ka Oblast\'', '74', 1),
(3482, 220, 'Chernivets\'ka Oblast\'', '77', 1),
(3483, 220, 'Crimea', '43', 1),
(3484, 220, 'Dnipropetrovs\'ka Oblast\'', '12', 1),
(3485, 220, 'Donets\'ka Oblast\'', '14', 1),
(3486, 220, 'Ivano-Frankivs\'ka Oblast\'', '26', 1),
(3487, 220, 'Khersons\'ka Oblast\'', '65', 1),
(3488, 220, 'Khmel\'nyts\'ka Oblast\'', '68', 1),
(3489, 220, 'Kirovohrads\'ka Oblast\'', '35', 1),
(3490, 220, 'Kyiv', '30', 1),
(3491, 220, 'Kyivs\'ka Oblast\'', '32', 1),
(3492, 220, 'Luhans\'ka Oblast\'', '09', 1),
(3493, 220, 'L\'vivs\'ka Oblast\'', '46', 1),
(3494, 220, 'Mykolayivs\'ka Oblast\'', '48', 1),
(3495, 220, 'Odes\'ka Oblast\'', '51', 1),
(3496, 220, 'Poltavs\'ka Oblast\'', '53', 1),
(3497, 220, 'Rivnens\'ka Oblast\'', '56', 1),
(3498, 220, 'Sevastopol\'', '40', 1),
(3499, 220, 'Sums\'ka Oblast\'', '59', 1),
(3500, 220, 'Ternopil\'s\'ka Oblast\'', '61', 1),
(3501, 220, 'Vinnyts\'ka Oblast\'', '05', 1),
(3502, 220, 'Volyns\'ka Oblast\'', '07', 1),
(3503, 220, 'Zakarpats\'ka Oblast\'', '21', 1),
(3504, 220, 'Zaporiz\'ka Oblast\'', '23', 1),
(3505, 220, 'Zhytomyrs\'ka oblast\'', '18', 1),
(3506, 221, 'Abu Dhabi', 'ADH', 1),
(3507, 221, '\'Ajman', 'AJ', 1),
(3508, 221, 'Al Fujayrah', 'FU', 1),
(3509, 221, 'Ash Shariqah', 'SH', 1),
(3510, 221, 'Dubai', 'DU', 1),
(3511, 221, 'R\'as al Khaymah', 'RK', 1),
(3512, 221, 'Umm al Qaywayn', 'UQ', 1),
(3513, 222, 'Aberdeen', 'ABN', 1),
(3514, 222, 'Aberdeenshire', 'ABNS', 1),
(3515, 222, 'Anglesey', 'ANG', 1),
(3516, 222, 'Angus', 'AGS', 1),
(3517, 222, 'Argyll and Bute', 'ARY', 1),
(3518, 222, 'Bedfordshire', 'BEDS', 1),
(3519, 222, 'Berkshire', 'BERKS', 1),
(3520, 222, 'Blaenau Gwent', 'BLA', 1),
(3521, 222, 'Bridgend', 'BRI', 1),
(3522, 222, 'Bristol', 'BSTL', 1),
(3523, 222, 'Buckinghamshire', 'BUCKS', 1),
(3524, 222, 'Caerphilly', 'CAE', 1),
(3525, 222, 'Cambridgeshire', 'CAMBS', 1),
(3526, 222, 'Cardiff', 'CDF', 1),
(3527, 222, 'Carmarthenshire', 'CARM', 1),
(3528, 222, 'Ceredigion', 'CDGN', 1),
(3529, 222, 'Cheshire', 'CHES', 1),
(3530, 222, 'Clackmannanshire', 'CLACK', 1),
(3531, 222, 'Conwy', 'CON', 1),
(3532, 222, 'Cornwall', 'CORN', 1),
(3533, 222, 'Denbighshire', 'DNBG', 1),
(3534, 222, 'Derbyshire', 'DERBY', 1),
(3535, 222, 'Devon', 'DVN', 1),
(3536, 222, 'Dorset', 'DOR', 1),
(3537, 222, 'Dumfries and Galloway', 'DGL', 1),
(3538, 222, 'Dundee', 'DUND', 1),
(3539, 222, 'Durham', 'DHM', 1),
(3540, 222, 'East Ayrshire', 'ARYE', 1),
(3541, 222, 'East Dunbartonshire', 'DUNBE', 1),
(3542, 222, 'East Lothian', 'LOTE', 1),
(3543, 222, 'East Renfrewshire', 'RENE', 1),
(3544, 222, 'East Riding of Yorkshire', 'ERYS', 1),
(3545, 222, 'East Sussex', 'SXE', 1),
(3546, 222, 'Edinburgh', 'EDIN', 1),
(3547, 222, 'Essex', 'ESX', 1),
(3548, 222, 'Falkirk', 'FALK', 1),
(3549, 222, 'Fife', 'FFE', 1),
(3550, 222, 'Flintshire', 'FLINT', 1),
(3551, 222, 'Glasgow', 'GLAS', 1),
(3552, 222, 'Gloucestershire', 'GLOS', 1),
(3553, 222, 'Greater London', 'LDN', 1),
(3554, 222, 'Greater Manchester', 'MCH', 1),
(3555, 222, 'Gwynedd', 'GDD', 1),
(3556, 222, 'Hampshire', 'HANTS', 1),
(3557, 222, 'Herefordshire', 'HWR', 1),
(3558, 222, 'Hertfordshire', 'HERTS', 1),
(3559, 222, 'Highlands', 'HLD', 1),
(3560, 222, 'Inverclyde', 'IVER', 1),
(3561, 222, 'Isle of Wight', 'IOW', 1),
(3562, 222, 'Kent', 'KNT', 1),
(3563, 222, 'Lancashire', 'LANCS', 1),
(3564, 222, 'Leicestershire', 'LEICS', 1),
(3565, 222, 'Lincolnshire', 'LINCS', 1),
(3566, 222, 'Merseyside', 'MSY', 1),
(3567, 222, 'Merthyr Tydfil', 'MERT', 1),
(3568, 222, 'Midlothian', 'MLOT', 1),
(3569, 222, 'Monmouthshire', 'MMOUTH', 1),
(3570, 222, 'Moray', 'MORAY', 1),
(3571, 222, 'Neath Port Talbot', 'NPRTAL', 1),
(3572, 222, 'Newport', 'NEWPT', 1),
(3573, 222, 'Norfolk', 'NOR', 1),
(3574, 222, 'North Ayrshire', 'ARYN', 1),
(3575, 222, 'North Lanarkshire', 'LANN', 1),
(3576, 222, 'North Yorkshire', 'YSN', 1),
(3577, 222, 'Northamptonshire', 'NHM', 1),
(3578, 222, 'Northumberland', 'NLD', 1),
(3579, 222, 'Nottinghamshire', 'NOT', 1),
(3580, 222, 'Orkney Islands', 'ORK', 1),
(3581, 222, 'Oxfordshire', 'OFE', 1),
(3582, 222, 'Pembrokeshire', 'PEM', 1),
(3583, 222, 'Perth and Kinross', 'PERTH', 1),
(3584, 222, 'Powys', 'PWS', 1),
(3585, 222, 'Renfrewshire', 'REN', 1),
(3586, 222, 'Rhondda Cynon Taff', 'RHON', 1),
(3587, 222, 'Rutland', 'RUT', 1),
(3588, 222, 'Scottish Borders', 'BOR', 1),
(3589, 222, 'Shetland Islands', 'SHET', 1),
(3590, 222, 'Shropshire', 'SPE', 1),
(3591, 222, 'Somerset', 'SOM', 1),
(3592, 222, 'South Ayrshire', 'ARYS', 1),
(3593, 222, 'South Lanarkshire', 'LANS', 1),
(3594, 222, 'South Yorkshire', 'YSS', 1),
(3595, 222, 'Staffordshire', 'SFD', 1),
(3596, 222, 'Stirling', 'STIR', 1),
(3597, 222, 'Suffolk', 'SFK', 1),
(3598, 222, 'Surrey', 'SRY', 1),
(3599, 222, 'Swansea', 'SWAN', 1),
(3600, 222, 'Torfaen', 'TORF', 1),
(3601, 222, 'Tyne and Wear', 'TWR', 1),
(3602, 222, 'Vale of Glamorgan', 'VGLAM', 1),
(3603, 222, 'Warwickshire', 'WARKS', 1),
(3604, 222, 'West Dunbartonshire', 'WDUN', 1),
(3605, 222, 'West Lothian', 'WLOT', 1),
(3606, 222, 'West Midlands', 'WMD', 1),
(3607, 222, 'West Sussex', 'SXW', 1),
(3608, 222, 'West Yorkshire', 'YSW', 1),
(3609, 222, 'Western Isles', 'WIL', 1),
(3610, 222, 'Wiltshire', 'WLT', 1),
(3611, 222, 'Worcestershire', 'WORCS', 1),
(3612, 222, 'Wrexham', 'WRX', 1),
(3613, 223, 'Alabama', 'AL', 1),
(3614, 223, 'Alaska', 'AK', 1),
(3615, 223, 'American Samoa', 'AS', 1),
(3616, 223, 'Arizona', 'AZ', 1),
(3617, 223, 'Arkansas', 'AR', 1),
(3618, 223, 'Armed Forces Africa', 'AF', 1),
(3619, 223, 'Armed Forces Americas', 'AA', 1),
(3620, 223, 'Armed Forces Canada', 'AC', 1),
(3621, 223, 'Armed Forces Europe', 'AE', 1),
(3622, 223, 'Armed Forces Middle East', 'AM', 1),
(3623, 223, 'Armed Forces Pacific', 'AP', 1),
(3624, 223, 'California', 'CA', 1),
(3625, 223, 'Colorado', 'CO', 1),
(3626, 223, 'Connecticut', 'CT', 1),
(3627, 223, 'Delaware', 'DE', 1),
(3628, 223, 'District of Columbia', 'DC', 1),
(3629, 223, 'Federated States Of Micronesia', 'FM', 1),
(3630, 223, 'Florida', 'FL', 1),
(3631, 223, 'Georgia', 'GA', 1),
(3632, 223, 'Guam', 'GU', 1),
(3633, 223, 'Hawaii', 'HI', 1),
(3634, 223, 'Idaho', 'ID', 1),
(3635, 223, 'Illinois', 'IL', 1),
(3636, 223, 'Indiana', 'IN', 1),
(3637, 223, 'Iowa', 'IA', 1),
(3638, 223, 'Kansas', 'KS', 1),
(3639, 223, 'Kentucky', 'KY', 1),
(3640, 223, 'Louisiana', 'LA', 1),
(3641, 223, 'Maine', 'ME', 1),
(3642, 223, 'Marshall Islands', 'MH', 1),
(3643, 223, 'Maryland', 'MD', 1),
(3644, 223, 'Massachusetts', 'MA', 1),
(3645, 223, 'Michigan', 'MI', 1),
(3646, 223, 'Minnesota', 'MN', 1),
(3647, 223, 'Mississippi', 'MS', 1),
(3648, 223, 'Missouri', 'MO', 1),
(3649, 223, 'Montana', 'MT', 1),
(3650, 223, 'Nebraska', 'NE', 1),
(3651, 223, 'Nevada', 'NV', 1),
(3652, 223, 'New Hampshire', 'NH', 1),
(3653, 223, 'New Jersey', 'NJ', 1),
(3654, 223, 'New Mexico', 'NM', 1),
(3655, 223, 'New York', 'NY', 1),
(3656, 223, 'North Carolina', 'NC', 1),
(3657, 223, 'North Dakota', 'ND', 1),
(3658, 223, 'Northern Mariana Islands', 'MP', 1),
(3659, 223, 'Ohio', 'OH', 1),
(3660, 223, 'Oklahoma', 'OK', 1),
(3661, 223, 'Oregon', 'OR', 1),
(3662, 223, 'Palau', 'PW', 1),
(3663, 223, 'Pennsylvania', 'PA', 1),
(3664, 223, 'Puerto Rico', 'PR', 1),
(3665, 223, 'Rhode Island', 'RI', 1),
(3666, 223, 'South Carolina', 'SC', 1),
(3667, 223, 'South Dakota', 'SD', 1),
(3668, 223, 'Tennessee', 'TN', 1),
(3669, 223, 'Texas', 'TX', 1),
(3670, 223, 'Utah', 'UT', 1),
(3671, 223, 'Vermont', 'VT', 1),
(3672, 223, 'Virgin Islands', 'VI', 1),
(3673, 223, 'Virginia', 'VA', 1),
(3674, 223, 'Washington', 'WA', 1),
(3675, 223, 'West Virginia', 'WV', 1),
(3676, 223, 'Wisconsin', 'WI', 1),
(3677, 223, 'Wyoming', 'WY', 1),
(3678, 224, 'Baker Island', 'BI', 1),
(3679, 224, 'Howland Island', 'HI', 1),
(3680, 224, 'Jarvis Island', 'JI', 1),
(3681, 224, 'Johnston Atoll', 'JA', 1),
(3682, 224, 'Kingman Reef', 'KR', 1),
(3683, 224, 'Midway Atoll', 'MA', 1),
(3684, 224, 'Navassa Island', 'NI', 1),
(3685, 224, 'Palmyra Atoll', 'PA', 1),
(3686, 224, 'Wake Island', 'WI', 1),
(3687, 225, 'Artigas', 'AR', 1),
(3688, 225, 'Canelones', 'CA', 1),
(3689, 225, 'Cerro Largo', 'CL', 1),
(3690, 225, 'Colonia', 'CO', 1),
(3691, 225, 'Durazno', 'DU', 1),
(3692, 225, 'Flores', 'FS', 1),
(3693, 225, 'Florida', 'FA', 1),
(3694, 225, 'Lavalleja', 'LA', 1),
(3695, 225, 'Maldonado', 'MA', 1),
(3696, 225, 'Montevideo', 'MO', 1),
(3697, 225, 'Paysandu', 'PA', 1),
(3698, 225, 'Rio Negro', 'RN', 1),
(3699, 225, 'Rivera', 'RV', 1),
(3700, 225, 'Rocha', 'RO', 1),
(3701, 225, 'Salto', 'SL', 1),
(3702, 225, 'San Jose', 'SJ', 1),
(3703, 225, 'Soriano', 'SO', 1),
(3704, 225, 'Tacuarembo', 'TA', 1),
(3705, 225, 'Treinta y Tres', 'TT', 1),
(3706, 226, 'Andijon', 'AN', 1),
(3707, 226, 'Buxoro', 'BU', 1),
(3708, 226, 'Farg\'ona', 'FA', 1),
(3709, 226, 'Jizzax', 'JI', 1),
(3710, 226, 'Namangan', 'NG', 1),
(3711, 226, 'Navoiy', 'NW', 1),
(3712, 226, 'Qashqadaryo', 'QA', 1),
(3713, 226, 'Qoraqalpog\'iston Republikasi', 'QR', 1),
(3714, 226, 'Samarqand', 'SA', 1),
(3715, 226, 'Sirdaryo', 'SI', 1),
(3716, 226, 'Surxondaryo', 'SU', 1),
(3717, 226, 'Toshkent City', 'TK', 1),
(3718, 226, 'Toshkent Region', 'TO', 1),
(3719, 226, 'Xorazm', 'XO', 1),
(3720, 227, 'Malampa', 'MA', 1),
(3721, 227, 'Penama', 'PE', 1),
(3722, 227, 'Sanma', 'SA', 1),
(3723, 227, 'Shefa', 'SH', 1),
(3724, 227, 'Tafea', 'TA', 1),
(3725, 227, 'Torba', 'TO', 1),
(3726, 229, 'Amazonas', 'AM', 1),
(3727, 229, 'Anzoategui', 'AN', 1),
(3728, 229, 'Apure', 'AP', 1),
(3729, 229, 'Aragua', 'AR', 1),
(3730, 229, 'Barinas', 'BA', 1),
(3731, 229, 'Bolivar', 'BO', 1),
(3732, 229, 'Carabobo', 'CA', 1),
(3733, 229, 'Cojedes', 'CO', 1),
(3734, 229, 'Delta Amacuro', 'DA', 1),
(3735, 229, 'Dependencias Federales', 'DF', 1),
(3736, 229, 'Distrito Federal', 'DI', 1),
(3737, 229, 'Falcon', 'FA', 1),
(3738, 229, 'Guarico', 'GU', 1),
(3739, 229, 'Lara', 'LA', 1),
(3740, 229, 'Merida', 'ME', 1),
(3741, 229, 'Miranda', 'MI', 1),
(3742, 229, 'Monagas', 'MO', 1),
(3743, 229, 'Nueva Esparta', 'NE', 1),
(3744, 229, 'Portuguesa', 'PO', 1),
(3745, 229, 'Sucre', 'SU', 1),
(3746, 229, 'Tachira', 'TA', 1),
(3747, 229, 'Trujillo', 'TR', 1),
(3748, 229, 'Vargas', 'VA', 1),
(3749, 229, 'Yaracuy', 'YA', 1),
(3750, 229, 'Zulia', 'ZU', 1),
(3751, 230, 'An Giang', 'AG', 1),
(3752, 230, 'Bac Giang', 'BG', 1),
(3753, 230, 'Bac Kan', 'BK', 1),
(3754, 230, 'Bac Lieu', 'BL', 1),
(3755, 230, 'Bac Ninh', 'BC', 1),
(3756, 230, 'Ba Ria-Vung Tau', 'BR', 1),
(3757, 230, 'Ben Tre', 'BN', 1),
(3758, 230, 'Binh Dinh', 'BH', 1),
(3759, 230, 'Binh Duong', 'BU', 1),
(3760, 230, 'Binh Phuoc', 'BP', 1),
(3761, 230, 'Binh Thuan', 'BT', 1),
(3762, 230, 'Ca Mau', 'CM', 1),
(3763, 230, 'Can Tho', 'CT', 1),
(3764, 230, 'Cao Bang', 'CB', 1),
(3765, 230, 'Dak Lak', 'DL', 1),
(3766, 230, 'Dak Nong', 'DG', 1),
(3767, 230, 'Da Nang', 'DN', 1),
(3768, 230, 'Dien Bien', 'DB', 1),
(3769, 230, 'Dong Nai', 'DI', 1),
(3770, 230, 'Dong Thap', 'DT', 1),
(3771, 230, 'Gia Lai', 'GL', 1),
(3772, 230, 'Ha Giang', 'HG', 1),
(3773, 230, 'Hai Duong', 'HD', 1),
(3774, 230, 'Hai Phong', 'HP', 1),
(3775, 230, 'Ha Nam', 'HM', 1),
(3776, 230, 'Ha Noi', 'HI', 1),
(3777, 230, 'Ha Tay', 'HT', 1),
(3778, 230, 'Ha Tinh', 'HH', 1),
(3779, 230, 'Hoa Binh', 'HB', 1),
(3780, 230, 'Ho Chi Minh City', 'HC', 1),
(3781, 230, 'Hau Giang', 'HU', 1),
(3782, 230, 'Hung Yen', 'HY', 1),
(3783, 232, 'Saint Croix', 'C', 1),
(3784, 232, 'Saint John', 'J', 1),
(3785, 232, 'Saint Thomas', 'T', 1),
(3786, 233, 'Alo', 'A', 1),
(3787, 233, 'Sigave', 'S', 1),
(3788, 233, 'Wallis', 'W', 1),
(3789, 235, 'Abyan', 'AB', 1),
(3790, 235, 'Adan', 'AD', 1),
(3791, 235, 'Amran', 'AM', 1),
(3792, 235, 'Al Bayda', 'BA', 1),
(3793, 235, 'Ad Dali', 'DA', 1),
(3794, 235, 'Dhamar', 'DH', 1),
(3795, 235, 'Hadramawt', 'HD', 1),
(3796, 235, 'Hajjah', 'HJ', 1),
(3797, 235, 'Al Hudaydah', 'HU', 1),
(3798, 235, 'Ibb', 'IB', 1),
(3799, 235, 'Al Jawf', 'JA', 1),
(3800, 235, 'Lahij', 'LA', 1),
(3801, 235, 'Ma\'rib', 'MA', 1),
(3802, 235, 'Al Mahrah', 'MR', 1),
(3803, 235, 'Al Mahwit', 'MW', 1),
(3804, 235, 'Sa\'dah', 'SD', 1),
(3805, 235, 'San\'a', 'SN', 1),
(3806, 235, 'Shabwah', 'SH', 1),
(3807, 235, 'Ta\'izz', 'TA', 1),
(3812, 237, 'Bas-Congo', 'BC', 1),
(3813, 237, 'Bandundu', 'BN', 1),
(3814, 237, 'Equateur', 'EQ', 1),
(3815, 237, 'Katanga', 'KA', 1),
(3816, 237, 'Kasai-Oriental', 'KE', 1),
(3817, 237, 'Kinshasa', 'KN', 1),
(3818, 237, 'Kasai-Occidental', 'KW', 1),
(3819, 237, 'Maniema', 'MA', 1),
(3820, 237, 'Nord-Kivu', 'NK', 1),
(3821, 237, 'Orientale', 'OR', 1),
(3822, 237, 'Sud-Kivu', 'SK', 1),
(3823, 238, 'Central', 'CE', 1),
(3824, 238, 'Copperbelt', 'CB', 1),
(3825, 238, 'Eastern', 'EA', 1),
(3826, 238, 'Luapula', 'LP', 1),
(3827, 238, 'Lusaka', 'LK', 1),
(3828, 238, 'Northern', 'NO', 1),
(3829, 238, 'North-Western', 'NW', 1),
(3830, 238, 'Southern', 'SO', 1),
(3831, 238, 'Western', 'WE', 1),
(3832, 239, 'Bulawayo', 'BU', 1),
(3833, 239, 'Harare', 'HA', 1),
(3834, 239, 'Manicaland', 'ML', 1),
(3835, 239, 'Mashonaland Central', 'MC', 1),
(3836, 239, 'Mashonaland East', 'ME', 1),
(3837, 239, 'Mashonaland West', 'MW', 1),
(3838, 239, 'Masvingo', 'MV', 1),
(3839, 239, 'Matabeleland North', 'MN', 1),
(3840, 239, 'Matabeleland South', 'MS', 1),
(3841, 239, 'Midlands', 'MD', 1),
(3861, 105, 'Campobasso', 'CB', 1),
(3863, 105, 'Caserta', 'CE', 1),
(3864, 105, 'Catania', 'CT', 1),
(3865, 105, 'Catanzaro', 'CZ', 1),
(3866, 105, 'Chieti', 'CH', 1),
(3867, 105, 'Como', 'CO', 1),
(3868, 105, 'Cosenza', 'CS', 1),
(3869, 105, 'Cremona', 'CR', 1),
(3870, 105, 'Crotone', 'KR', 1),
(3871, 105, 'Cuneo', 'CN', 1),
(3872, 105, 'Enna', 'EN', 1),
(3873, 105, 'Ferrara', 'FE', 1),
(3874, 105, 'Firenze', 'FI', 1),
(3875, 105, 'Foggia', 'FG', 1),
(3876, 105, 'Forli-Cesena', 'FC', 1),
(3877, 105, 'Frosinone', 'FR', 1),
(3878, 105, 'Genova', 'GE', 1),
(3879, 105, 'Gorizia', 'GO', 1),
(3880, 105, 'Grosseto', 'GR', 1),
(3881, 105, 'Imperia', 'IM', 1),
(3882, 105, 'Isernia', 'IS', 1),
(3883, 105, 'L&#39;Aquila', 'AQ', 1),
(3884, 105, 'La Spezia', 'SP', 1),
(3885, 105, 'Latina', 'LT', 1),
(3886, 105, 'Lecce', 'LE', 1),
(3887, 105, 'Lecco', 'LC', 1),
(3888, 105, 'Livorno', 'LI', 1),
(3889, 105, 'Lodi', 'LO', 1),
(3890, 105, 'Lucca', 'LU', 1),
(3891, 105, 'Macerata', 'MC', 1),
(3892, 105, 'Mantova', 'MN', 1),
(3893, 105, 'Massa-Carrara', 'MS', 1),
(3894, 105, 'Matera', 'MT', 1),
(3896, 105, 'Messina', 'ME', 1),
(3897, 105, 'Milano', 'MI', 1),
(3898, 105, 'Modena', 'MO', 1),
(3899, 105, 'Napoli', 'NA', 1),
(3900, 105, 'Novara', 'NO', 1),
(3901, 105, 'Nuoro', 'NU', 1),
(3904, 105, 'Oristano', 'OR', 1),
(3905, 105, 'Padova', 'PD', 1),
(3906, 105, 'Palermo', 'PA', 1),
(3907, 105, 'Parma', 'PR', 1),
(3908, 105, 'Pavia', 'PV', 1),
(3909, 105, 'Perugia', 'PG', 1),
(3910, 105, 'Pesaro e Urbino', 'PU', 1),
(3911, 105, 'Pescara', 'PE', 1),
(3912, 105, 'Piacenza', 'PC', 1),
(3913, 105, 'Pisa', 'PI', 1),
(3914, 105, 'Pistoia', 'PT', 1),
(3915, 105, 'Pordenone', 'PN', 1),
(3916, 105, 'Potenza', 'PZ', 1),
(3917, 105, 'Prato', 'PO', 1),
(3918, 105, 'Ragusa', 'RG', 1),
(3919, 105, 'Ravenna', 'RA', 1),
(3920, 105, 'Reggio Calabria', 'RC', 1),
(3921, 105, 'Reggio Emilia', 'RE', 1),
(3922, 105, 'Rieti', 'RI', 1),
(3923, 105, 'Rimini', 'RN', 1),
(3924, 105, 'Roma', 'RM', 1),
(3925, 105, 'Rovigo', 'RO', 1),
(3926, 105, 'Salerno', 'SA', 1),
(3927, 105, 'Sassari', 'SS', 1),
(3928, 105, 'Savona', 'SV', 1),
(3929, 105, 'Siena', 'SI', 1),
(3930, 105, 'Siracusa', 'SR', 1),
(3931, 105, 'Sondrio', 'SO', 1),
(3932, 105, 'Taranto', 'TA', 1),
(3933, 105, 'Teramo', 'TE', 1),
(3934, 105, 'Terni', 'TR', 1),
(3935, 105, 'Torino', 'TO', 1),
(3936, 105, 'Trapani', 'TP', 1),
(3937, 105, 'Trento', 'TN', 1),
(3938, 105, 'Treviso', 'TV', 1),
(3939, 105, 'Trieste', 'TS', 1),
(3940, 105, 'Udine', 'UD', 1),
(3941, 105, 'Varese', 'VA', 1),
(3942, 105, 'Venezia', 'VE', 1),
(3943, 105, 'Verbano-Cusio-Ossola', 'VB', 1),
(3944, 105, 'Vercelli', 'VC', 1),
(3945, 105, 'Verona', 'VR', 1),
(3946, 105, 'Vibo Valentia', 'VV', 1),
(3947, 105, 'Vicenza', 'VI', 1),
(3948, 105, 'Viterbo', 'VT', 1),
(3949, 222, 'County Antrim', 'ANT', 1),
(3950, 222, 'County Armagh', 'ARM', 1),
(3951, 222, 'County Down', 'DOW', 1),
(3952, 222, 'County Fermanagh', 'FER', 1),
(3953, 222, 'County Londonderry', 'LDY', 1),
(3954, 222, 'County Tyrone', 'TYR', 1),
(3955, 222, 'Cumbria', 'CMA', 1),
(3956, 190, 'Pomurska', '1', 1),
(3957, 190, 'Podravska', '2', 1),
(3958, 190, 'Koroška', '3', 1),
(3959, 190, 'Savinjska', '4', 1),
(3960, 190, 'Zasavska', '5', 1),
(3961, 190, 'Spodnjeposavska', '6', 1),
(3962, 190, 'Jugovzhodna Slovenija', '7', 1),
(3963, 190, 'Osrednjeslovenska', '8', 1),
(3964, 190, 'Gorenjska', '9', 1),
(3965, 190, 'Notranjsko-kraška', '10', 1),
(3966, 190, 'Goriška', '11', 1),
(3967, 190, 'Obalno-kraška', '12', 1),
(3968, 33, 'Ruse', '', 1),
(3969, 101, 'Alborz', 'ALB', 1),
(3970, 21, 'Brussels-Capital Region', 'BRU', 1),
(3971, 138, 'Aguascalientes', 'AG', 1),
(3973, 242, 'Andrijevica', '01', 1),
(3974, 242, 'Bar', '02', 1),
(3975, 242, 'Berane', '03', 1),
(3976, 242, 'Bijelo Polje', '04', 1),
(3977, 242, 'Budva', '05', 1),
(3978, 242, 'Cetinje', '06', 1),
(3979, 242, 'Danilovgrad', '07', 1),
(3980, 242, 'Herceg-Novi', '08', 1),
(3981, 242, 'Kolašin', '09', 1),
(3982, 242, 'Kotor', '10', 1),
(3983, 242, 'Mojkovac', '11', 1),
(3984, 242, 'Nikšić', '12', 1),
(3985, 242, 'Plav', '13', 1),
(3986, 242, 'Pljevlja', '14', 1),
(3987, 242, 'Plužine', '15', 1),
(3988, 242, 'Podgorica', '16', 1),
(3989, 242, 'Rožaje', '17', 1),
(3990, 242, 'Šavnik', '18', 1),
(3991, 242, 'Tivat', '19', 1),
(3992, 242, 'Ulcinj', '20', 1),
(3993, 242, 'Žabljak', '21', 1),
(3994, 243, 'Belgrade', '00', 1),
(3995, 243, 'North Bačka', '01', 1),
(3996, 243, 'Central Banat', '02', 1),
(3997, 243, 'North Banat', '03', 1),
(3998, 243, 'South Banat', '04', 1),
(3999, 243, 'West Bačka', '05', 1),
(4000, 243, 'South Bačka', '06', 1),
(4001, 243, 'Srem', '07', 1),
(4002, 243, 'Mačva', '08', 1),
(4003, 243, 'Kolubara', '09', 1),
(4004, 243, 'Podunavlje', '10', 1),
(4005, 243, 'Braničevo', '11', 1),
(4006, 243, 'Šumadija', '12', 1),
(4007, 243, 'Pomoravlje', '13', 1),
(4008, 243, 'Bor', '14', 1),
(4009, 243, 'Zaječar', '15', 1),
(4010, 243, 'Zlatibor', '16', 1),
(4011, 243, 'Moravica', '17', 1),
(4012, 243, 'Raška', '18', 1),
(4013, 243, 'Rasina', '19', 1),
(4014, 243, 'Nišava', '20', 1),
(4015, 243, 'Toplica', '21', 1),
(4016, 243, 'Pirot', '22', 1),
(4017, 243, 'Jablanica', '23', 1),
(4018, 243, 'Pčinja', '24', 1),
(4020, 245, 'Bonaire', 'BO', 1),
(4021, 245, 'Saba', 'SA', 1),
(4022, 245, 'Sint Eustatius', 'SE', 1),
(4023, 248, 'Central Equatoria', 'EC', 1),
(4024, 248, 'Eastern Equatoria', 'EE', 1),
(4025, 248, 'Jonglei', 'JG', 1),
(4026, 248, 'Lakes', 'LK', 1),
(4027, 248, 'Northern Bahr el-Ghazal', 'BN', 1),
(4028, 248, 'Unity', 'UY', 1),
(4029, 248, 'Upper Nile', 'NU', 1),
(4030, 248, 'Warrap', 'WR', 1),
(4031, 248, 'Western Bahr el-Ghazal', 'BW', 1),
(4032, 248, 'Western Equatoria', 'EW', 1),
(4036, 117, 'Ainaži, Salacgrīvas novads', '0661405', 1),
(4037, 117, 'Aizkraukle, Aizkraukles novads', '0320201', 1),
(4038, 117, 'Aizkraukles novads', '0320200', 1),
(4039, 117, 'Aizpute, Aizputes novads', '0640605', 1),
(4040, 117, 'Aizputes novads', '0640600', 1),
(4041, 117, 'Aknīste, Aknīstes novads', '0560805', 1),
(4042, 117, 'Aknīstes novads', '0560800', 1),
(4043, 117, 'Aloja, Alojas novads', '0661007', 1),
(4044, 117, 'Alojas novads', '0661000', 1),
(4045, 117, 'Alsungas novads', '0624200', 1),
(4046, 117, 'Alūksne, Alūksnes novads', '0360201', 1),
(4047, 117, 'Alūksnes novads', '0360200', 1),
(4048, 117, 'Amatas novads', '0424701', 1),
(4049, 117, 'Ape, Apes novads', '0360805', 1),
(4050, 117, 'Apes novads', '0360800', 1),
(4051, 117, 'Auce, Auces novads', '0460805', 1),
(4052, 117, 'Auces novads', '0460800', 1),
(4053, 117, 'Ādažu novads', '0804400', 1),
(4054, 117, 'Babītes novads', '0804900', 1),
(4055, 117, 'Baldone, Baldones novads', '0800605', 1),
(4056, 117, 'Baldones novads', '0800600', 1),
(4057, 117, 'Baloži, Ķekavas novads', '0800807', 1),
(4058, 117, 'Baltinavas novads', '0384400', 1),
(4059, 117, 'Balvi, Balvu novads', '0380201', 1),
(4060, 117, 'Balvu novads', '0380200', 1),
(4061, 117, 'Bauska, Bauskas novads', '0400201', 1),
(4062, 117, 'Bauskas novads', '0400200', 1),
(4063, 117, 'Beverīnas novads', '0964700', 1),
(4064, 117, 'Brocēni, Brocēnu novads', '0840605', 1),
(4065, 117, 'Brocēnu novads', '0840601', 1),
(4066, 117, 'Burtnieku novads', '0967101', 1),
(4067, 117, 'Carnikavas novads', '0805200', 1),
(4068, 117, 'Cesvaine, Cesvaines novads', '0700807', 1),
(4069, 117, 'Cesvaines novads', '0700800', 1),
(4070, 117, 'Cēsis, Cēsu novads', '0420201', 1),
(4071, 117, 'Cēsu novads', '0420200', 1),
(4072, 117, 'Ciblas novads', '0684901', 1),
(4073, 117, 'Dagda, Dagdas novads', '0601009', 1),
(4074, 117, 'Dagdas novads', '0601000', 1),
(4075, 117, 'Daugavpils', '0050000', 1),
(4076, 117, 'Daugavpils novads', '0440200', 1),
(4077, 117, 'Dobele, Dobeles novads', '0460201', 1),
(4078, 117, 'Dobeles novads', '0460200', 1),
(4079, 117, 'Dundagas novads', '0885100', 1),
(4080, 117, 'Durbe, Durbes novads', '0640807', 1),
(4081, 117, 'Durbes novads', '0640801', 1),
(4082, 117, 'Engures novads', '0905100', 1),
(4083, 117, 'Ērgļu novads', '0705500', 1),
(4084, 117, 'Garkalnes novads', '0806000', 1),
(4085, 117, 'Grobiņa, Grobiņas novads', '0641009', 1),
(4086, 117, 'Grobiņas novads', '0641000', 1),
(4087, 117, 'Gulbene, Gulbenes novads', '0500201', 1),
(4088, 117, 'Gulbenes novads', '0500200', 1),
(4089, 117, 'Iecavas novads', '0406400', 1),
(4090, 117, 'Ikšķile, Ikšķiles novads', '0740605', 1),
(4091, 117, 'Ikšķiles novads', '0740600', 1),
(4092, 117, 'Ilūkste, Ilūkstes novads', '0440807', 1),
(4093, 117, 'Ilūkstes novads', '0440801', 1),
(4094, 117, 'Inčukalna novads', '0801800', 1),
(4095, 117, 'Jaunjelgava, Jaunjelgavas novads', '0321007', 1),
(4096, 117, 'Jaunjelgavas novads', '0321000', 1),
(4097, 117, 'Jaunpiebalgas novads', '0425700', 1),
(4098, 117, 'Jaunpils novads', '0905700', 1),
(4099, 117, 'Jelgava', '0090000', 1),
(4100, 117, 'Jelgavas novads', '0540200', 1),
(4101, 117, 'Jēkabpils', '0110000', 1),
(4102, 117, 'Jēkabpils novads', '0560200', 1),
(4103, 117, 'Jūrmala', '0130000', 1),
(4104, 117, 'Kalnciems, Jelgavas novads', '0540211', 1),
(4105, 117, 'Kandava, Kandavas novads', '0901211', 1),
(4106, 117, 'Kandavas novads', '0901201', 1),
(4107, 117, 'Kārsava, Kārsavas novads', '0681009', 1),
(4108, 117, 'Kārsavas novads', '0681000', 1),
(4109, 117, 'Kocēnu novads ,bij. Valmieras)', '0960200', 1),
(4110, 117, 'Kokneses novads', '0326100', 1),
(4111, 117, 'Krāslava, Krāslavas novads', '0600201', 1),
(4112, 117, 'Krāslavas novads', '0600202', 1),
(4113, 117, 'Krimuldas novads', '0806900', 1),
(4114, 117, 'Krustpils novads', '0566900', 1),
(4115, 117, 'Kuldīga, Kuldīgas novads', '0620201', 1),
(4116, 117, 'Kuldīgas novads', '0620200', 1),
(4117, 117, 'Ķeguma novads', '0741001', 1),
(4118, 117, 'Ķegums, Ķeguma novads', '0741009', 1),
(4119, 117, 'Ķekavas novads', '0800800', 1),
(4120, 117, 'Lielvārde, Lielvārdes novads', '0741413', 1),
(4121, 117, 'Lielvārdes novads', '0741401', 1),
(4122, 117, 'Liepāja', '0170000', 1),
(4123, 117, 'Limbaži, Limbažu novads', '0660201', 1),
(4124, 117, 'Limbažu novads', '0660200', 1),
(4125, 117, 'Līgatne, Līgatnes novads', '0421211', 1),
(4126, 117, 'Līgatnes novads', '0421200', 1),
(4127, 117, 'Līvāni, Līvānu novads', '0761211', 1),
(4128, 117, 'Līvānu novads', '0761201', 1),
(4129, 117, 'Lubāna, Lubānas novads', '0701413', 1),
(4130, 117, 'Lubānas novads', '0701400', 1),
(4131, 117, 'Ludza, Ludzas novads', '0680201', 1),
(4132, 117, 'Ludzas novads', '0680200', 1),
(4133, 117, 'Madona, Madonas novads', '0700201', 1),
(4134, 117, 'Madonas novads', '0700200', 1),
(4135, 117, 'Mazsalaca, Mazsalacas novads', '0961011', 1),
(4136, 117, 'Mazsalacas novads', '0961000', 1),
(4137, 117, 'Mālpils novads', '0807400', 1),
(4138, 117, 'Mārupes novads', '0807600', 1),
(4139, 117, 'Mērsraga novads', '0887600', 1),
(4140, 117, 'Naukšēnu novads', '0967300', 1),
(4141, 117, 'Neretas novads', '0327100', 1),
(4142, 117, 'Nīcas novads', '0647900', 1),
(4143, 117, 'Ogre, Ogres novads', '0740201', 1),
(4144, 117, 'Ogres novads', '0740202', 1),
(4145, 117, 'Olaine, Olaines novads', '0801009', 1),
(4146, 117, 'Olaines novads', '0801000', 1),
(4147, 117, 'Ozolnieku novads', '0546701', 1),
(4148, 117, 'Pārgaujas novads', '0427500', 1),
(4149, 117, 'Pāvilosta, Pāvilostas novads', '0641413', 1),
(4150, 117, 'Pāvilostas novads', '0641401', 1),
(4151, 117, 'Piltene, Ventspils novads', '0980213', 1),
(4152, 117, 'Pļaviņas, Pļaviņu novads', '0321413', 1),
(4153, 117, 'Pļaviņu novads', '0321400', 1),
(4154, 117, 'Preiļi, Preiļu novads', '0760201', 1),
(4155, 117, 'Preiļu novads', '0760202', 1),
(4156, 117, 'Priekule, Priekules novads', '0641615', 1),
(4157, 117, 'Priekules novads', '0641600', 1),
(4158, 117, 'Priekuļu novads', '0427300', 1),
(4159, 117, 'Raunas novads', '0427700', 1),
(4160, 117, 'Rēzekne', '0210000', 1),
(4161, 117, 'Rēzeknes novads', '0780200', 1),
(4162, 117, 'Riebiņu novads', '0766300', 1),
(4163, 117, 'Rīga', '0010000', 1),
(4164, 117, 'Rojas novads', '0888300', 1),
(4165, 117, 'Ropažu novads', '0808400', 1),
(4166, 117, 'Rucavas novads', '0648500', 1),
(4167, 117, 'Rugāju novads', '0387500', 1),
(4168, 117, 'Rundāles novads', '0407700', 1),
(4169, 117, 'Rūjiena, Rūjienas novads', '0961615', 1),
(4170, 117, 'Rūjienas novads', '0961600', 1),
(4171, 117, 'Sabile, Talsu novads', '0880213', 1),
(4172, 117, 'Salacgrīva, Salacgrīvas novads', '0661415', 1),
(4173, 117, 'Salacgrīvas novads', '0661400', 1),
(4174, 117, 'Salas novads', '0568700', 1),
(4175, 117, 'Salaspils novads', '0801200', 1),
(4176, 117, 'Salaspils, Salaspils novads', '0801211', 1),
(4177, 117, 'Saldus novads', '0840200', 1),
(4178, 117, 'Saldus, Saldus novads', '0840201', 1),
(4179, 117, 'Saulkrasti, Saulkrastu novads', '0801413', 1),
(4180, 117, 'Saulkrastu novads', '0801400', 1),
(4181, 117, 'Seda, Strenču novads', '0941813', 1),
(4182, 117, 'Sējas novads', '0809200', 1),
(4183, 117, 'Sigulda, Siguldas novads', '0801615', 1),
(4184, 117, 'Siguldas novads', '0801601', 1),
(4185, 117, 'Skrīveru novads', '0328200', 1),
(4186, 117, 'Skrunda, Skrundas novads', '0621209', 1),
(4187, 117, 'Skrundas novads', '0621200', 1),
(4188, 117, 'Smiltene, Smiltenes novads', '0941615', 1),
(4189, 117, 'Smiltenes novads', '0941600', 1),
(4190, 117, 'Staicele, Alojas novads', '0661017', 1),
(4191, 117, 'Stende, Talsu novads', '0880215', 1),
(4192, 117, 'Stopiņu novads', '0809600', 1),
(4193, 117, 'Strenči, Strenču novads', '0941817', 1),
(4194, 117, 'Strenču novads', '0941800', 1),
(4195, 117, 'Subate, Ilūkstes novads', '0440815', 1),
(4196, 117, 'Talsi, Talsu novads', '0880201', 1),
(4197, 117, 'Talsu novads', '0880200', 1),
(4198, 117, 'Tērvetes novads', '0468900', 1),
(4199, 117, 'Tukuma novads', '0900200', 1),
(4200, 117, 'Tukums, Tukuma novads', '0900201', 1),
(4201, 117, 'Vaiņodes novads', '0649300', 1),
(4202, 117, 'Valdemārpils, Talsu novads', '0880217', 1),
(4203, 117, 'Valka, Valkas novads', '0940201', 1),
(4204, 117, 'Valkas novads', '0940200', 1),
(4205, 117, 'Valmiera', '0250000', 1),
(4206, 117, 'Vangaži, Inčukalna novads', '0801817', 1),
(4207, 117, 'Varakļāni, Varakļānu novads', '0701817', 1),
(4208, 117, 'Varakļānu novads', '0701800', 1),
(4209, 117, 'Vārkavas novads', '0769101', 1),
(4210, 117, 'Vecpiebalgas novads', '0429300', 1),
(4211, 117, 'Vecumnieku novads', '0409500', 1),
(4212, 117, 'Ventspils', '0270000', 1),
(4213, 117, 'Ventspils novads', '0980200', 1),
(4214, 117, 'Viesīte, Viesītes novads', '0561815', 1),
(4215, 117, 'Viesītes novads', '0561800', 1),
(4216, 117, 'Viļaka, Viļakas novads', '0381615', 1),
(4217, 117, 'Viļakas novads', '0381600', 1),
(4218, 117, 'Viļāni, Viļānu novads', '0781817', 1),
(4219, 117, 'Viļānu novads', '0781800', 1),
(4220, 117, 'Zilupe, Zilupes novads', '0681817', 1),
(4221, 117, 'Zilupes novads', '0681801', 1),
(4222, 43, 'Arica y Parinacota', 'AP', 1),
(4223, 43, 'Los Rios', 'LR', 1),
(4224, 220, 'Kharkivs\'ka Oblast\'', '63', 1),
(4225, 118, 'Beirut', 'LB-BR', 1),
(4226, 118, 'Bekaa', 'LB-BE', 1),
(4227, 118, 'Mount Lebanon', 'LB-ML', 1),
(4228, 118, 'Nabatieh', 'LB-NB', 1),
(4229, 118, 'North', 'LB-NR', 1),
(4230, 118, 'South', 'LB-ST', 1),
(4231, 99, 'Telangana', 'TS', 1),
(4232, 44, 'Qinghai', 'QH', 1),
(4233, 100, 'Papua Barat', 'PB', 1),
(4234, 100, 'Sulawesi Barat', 'SR', 1),
(4235, 100, 'Kepulauan Riau', 'KR', 1),
(4236, 105, 'Barletta-Andria-Trani', 'BT', 1),
(4237, 105, 'Fermo', 'FM', 1),
(4238, 105, 'Monza Brianza', 'MB', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_zone_to_geo_zone`
--

CREATE TABLE `oc_zone_to_geo_zone` (
  `zone_to_geo_zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `geo_zone_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_zone_to_geo_zone`
--

INSERT INTO `oc_zone_to_geo_zone` (`zone_to_geo_zone_id`, `country_id`, `zone_id`, `geo_zone_id`, `date_added`, `date_modified`) VALUES
(111, 184, 0, 4, '2020-11-21 01:47:27', '0000-00-00 00:00:00'),
(2, 222, 3513, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 222, 3514, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 222, 3515, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 222, 3516, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 222, 3517, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 222, 3518, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 222, 3519, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 222, 3520, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 222, 3521, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 222, 3522, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 222, 3523, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 222, 3524, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 222, 3525, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 222, 3526, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 222, 3527, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 222, 3528, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 222, 3529, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 222, 3530, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 222, 3531, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 222, 3532, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 222, 3533, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 222, 3534, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 222, 3535, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 222, 3536, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 222, 3537, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 222, 3538, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 222, 3539, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 222, 3540, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 222, 3541, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 222, 3542, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 222, 3543, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 222, 3544, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 222, 3545, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 222, 3546, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 222, 3547, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 222, 3548, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 222, 3549, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 222, 3550, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 222, 3551, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 222, 3552, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 222, 3553, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 222, 3554, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 222, 3555, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 222, 3556, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 222, 3557, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 222, 3558, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 222, 3559, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 222, 3560, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 222, 3561, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 222, 3562, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 222, 3563, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 222, 3564, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 222, 3565, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 222, 3566, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 222, 3567, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 222, 3568, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 222, 3569, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 222, 3570, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 222, 3571, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 222, 3572, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 222, 3573, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 222, 3574, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 222, 3575, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 222, 3576, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 222, 3577, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 222, 3578, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 222, 3579, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 222, 3580, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 222, 3581, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 222, 3582, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 222, 3583, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 222, 3584, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 222, 3585, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 222, 3586, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 222, 3587, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 222, 3588, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 222, 3589, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 222, 3590, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 222, 3591, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 222, 3592, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 222, 3593, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 222, 3594, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 222, 3595, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 222, 3596, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 222, 3597, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 222, 3598, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 222, 3599, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 222, 3600, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 222, 3601, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 222, 3602, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 222, 3603, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 222, 3604, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 222, 3605, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 222, 3606, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 222, 3607, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 222, 3608, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 222, 3609, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 222, 3610, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 222, 3611, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 222, 3612, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 222, 3949, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 222, 3950, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 222, 3951, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 222, 3952, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 222, 3953, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 222, 3954, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 222, 3955, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 222, 3972, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `oc_address`
--
ALTER TABLE `oc_address`
  ADD PRIMARY KEY (`address_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `oc_api`
--
ALTER TABLE `oc_api`
  ADD PRIMARY KEY (`api_id`);

--
-- Indexes for table `oc_api_ip`
--
ALTER TABLE `oc_api_ip`
  ADD PRIMARY KEY (`api_ip_id`);

--
-- Indexes for table `oc_api_session`
--
ALTER TABLE `oc_api_session`
  ADD PRIMARY KEY (`api_session_id`);

--
-- Indexes for table `oc_attribute`
--
ALTER TABLE `oc_attribute`
  ADD PRIMARY KEY (`attribute_id`);

--
-- Indexes for table `oc_attribute_description`
--
ALTER TABLE `oc_attribute_description`
  ADD PRIMARY KEY (`attribute_id`,`language_id`);

--
-- Indexes for table `oc_attribute_group`
--
ALTER TABLE `oc_attribute_group`
  ADD PRIMARY KEY (`attribute_group_id`);

--
-- Indexes for table `oc_attribute_group_description`
--
ALTER TABLE `oc_attribute_group_description`
  ADD PRIMARY KEY (`attribute_group_id`,`language_id`);

--
-- Indexes for table `oc_banner`
--
ALTER TABLE `oc_banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  ADD PRIMARY KEY (`banner_image_id`);

--
-- Indexes for table `oc_blogger`
--
ALTER TABLE `oc_blogger`
  ADD PRIMARY KEY (`blogger_id`);

--
-- Indexes for table `oc_blogger_comment`
--
ALTER TABLE `oc_blogger_comment`
  ADD PRIMARY KEY (`blogger_comment_id`,`blogger_id`);

--
-- Indexes for table `oc_blogger_comment_description`
--
ALTER TABLE `oc_blogger_comment_description`
  ADD PRIMARY KEY (`blogger_comment_id`,`language_id`);

--
-- Indexes for table `oc_blogger_description`
--
ALTER TABLE `oc_blogger_description`
  ADD PRIMARY KEY (`blogger_id`,`language_id`);

--
-- Indexes for table `oc_cart`
--
ALTER TABLE `oc_cart`
  ADD PRIMARY KEY (`cart_id`),
  ADD KEY `cart_id` (`api_id`,`customer_id`,`session_id`,`product_id`,`recurring_id`);

--
-- Indexes for table `oc_category`
--
ALTER TABLE `oc_category`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `oc_category_description`
--
ALTER TABLE `oc_category_description`
  ADD PRIMARY KEY (`category_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_category_filter`
--
ALTER TABLE `oc_category_filter`
  ADD PRIMARY KEY (`category_id`,`filter_id`);

--
-- Indexes for table `oc_category_path`
--
ALTER TABLE `oc_category_path`
  ADD PRIMARY KEY (`category_id`,`path_id`);

--
-- Indexes for table `oc_category_to_layout`
--
ALTER TABLE `oc_category_to_layout`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Indexes for table `oc_category_to_store`
--
ALTER TABLE `oc_category_to_store`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Indexes for table `oc_country`
--
ALTER TABLE `oc_country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `oc_coupon`
--
ALTER TABLE `oc_coupon`
  ADD PRIMARY KEY (`coupon_id`);

--
-- Indexes for table `oc_coupon_category`
--
ALTER TABLE `oc_coupon_category`
  ADD PRIMARY KEY (`coupon_id`,`category_id`);

--
-- Indexes for table `oc_coupon_history`
--
ALTER TABLE `oc_coupon_history`
  ADD PRIMARY KEY (`coupon_history_id`);

--
-- Indexes for table `oc_coupon_product`
--
ALTER TABLE `oc_coupon_product`
  ADD PRIMARY KEY (`coupon_product_id`);

--
-- Indexes for table `oc_currency`
--
ALTER TABLE `oc_currency`
  ADD PRIMARY KEY (`currency_id`);

--
-- Indexes for table `oc_customer`
--
ALTER TABLE `oc_customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `oc_customer_activity`
--
ALTER TABLE `oc_customer_activity`
  ADD PRIMARY KEY (`customer_activity_id`);

--
-- Indexes for table `oc_customer_affiliate`
--
ALTER TABLE `oc_customer_affiliate`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `oc_customer_approval`
--
ALTER TABLE `oc_customer_approval`
  ADD PRIMARY KEY (`customer_approval_id`);

--
-- Indexes for table `oc_customer_group`
--
ALTER TABLE `oc_customer_group`
  ADD PRIMARY KEY (`customer_group_id`);

--
-- Indexes for table `oc_customer_group_description`
--
ALTER TABLE `oc_customer_group_description`
  ADD PRIMARY KEY (`customer_group_id`,`language_id`);

--
-- Indexes for table `oc_customer_history`
--
ALTER TABLE `oc_customer_history`
  ADD PRIMARY KEY (`customer_history_id`);

--
-- Indexes for table `oc_customer_ip`
--
ALTER TABLE `oc_customer_ip`
  ADD PRIMARY KEY (`customer_ip_id`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `oc_customer_login`
--
ALTER TABLE `oc_customer_login`
  ADD PRIMARY KEY (`customer_login_id`),
  ADD KEY `email` (`email`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `oc_customer_online`
--
ALTER TABLE `oc_customer_online`
  ADD PRIMARY KEY (`ip`);

--
-- Indexes for table `oc_customer_reward`
--
ALTER TABLE `oc_customer_reward`
  ADD PRIMARY KEY (`customer_reward_id`);

--
-- Indexes for table `oc_customer_search`
--
ALTER TABLE `oc_customer_search`
  ADD PRIMARY KEY (`customer_search_id`);

--
-- Indexes for table `oc_customer_transaction`
--
ALTER TABLE `oc_customer_transaction`
  ADD PRIMARY KEY (`customer_transaction_id`);

--
-- Indexes for table `oc_customer_wishlist`
--
ALTER TABLE `oc_customer_wishlist`
  ADD PRIMARY KEY (`customer_id`,`product_id`);

--
-- Indexes for table `oc_custom_field`
--
ALTER TABLE `oc_custom_field`
  ADD PRIMARY KEY (`custom_field_id`);

--
-- Indexes for table `oc_custom_field_customer_group`
--
ALTER TABLE `oc_custom_field_customer_group`
  ADD PRIMARY KEY (`custom_field_id`,`customer_group_id`);

--
-- Indexes for table `oc_custom_field_description`
--
ALTER TABLE `oc_custom_field_description`
  ADD PRIMARY KEY (`custom_field_id`,`language_id`);

--
-- Indexes for table `oc_custom_field_value`
--
ALTER TABLE `oc_custom_field_value`
  ADD PRIMARY KEY (`custom_field_value_id`);

--
-- Indexes for table `oc_custom_field_value_description`
--
ALTER TABLE `oc_custom_field_value_description`
  ADD PRIMARY KEY (`custom_field_value_id`,`language_id`);

--
-- Indexes for table `oc_download`
--
ALTER TABLE `oc_download`
  ADD PRIMARY KEY (`download_id`);

--
-- Indexes for table `oc_download_description`
--
ALTER TABLE `oc_download_description`
  ADD PRIMARY KEY (`download_id`,`language_id`);

--
-- Indexes for table `oc_event`
--
ALTER TABLE `oc_event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `oc_extension`
--
ALTER TABLE `oc_extension`
  ADD PRIMARY KEY (`extension_id`);

--
-- Indexes for table `oc_extension_install`
--
ALTER TABLE `oc_extension_install`
  ADD PRIMARY KEY (`extension_install_id`);

--
-- Indexes for table `oc_extension_path`
--
ALTER TABLE `oc_extension_path`
  ADD PRIMARY KEY (`extension_path_id`);

--
-- Indexes for table `oc_filter`
--
ALTER TABLE `oc_filter`
  ADD PRIMARY KEY (`filter_id`);

--
-- Indexes for table `oc_filter_description`
--
ALTER TABLE `oc_filter_description`
  ADD PRIMARY KEY (`filter_id`,`language_id`);

--
-- Indexes for table `oc_filter_group`
--
ALTER TABLE `oc_filter_group`
  ADD PRIMARY KEY (`filter_group_id`);

--
-- Indexes for table `oc_filter_group_description`
--
ALTER TABLE `oc_filter_group_description`
  ADD PRIMARY KEY (`filter_group_id`,`language_id`);

--
-- Indexes for table `oc_geo_zone`
--
ALTER TABLE `oc_geo_zone`
  ADD PRIMARY KEY (`geo_zone_id`);

--
-- Indexes for table `oc_hittech_dhl_details_new`
--
ALTER TABLE `oc_hittech_dhl_details_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_information`
--
ALTER TABLE `oc_information`
  ADD PRIMARY KEY (`information_id`);

--
-- Indexes for table `oc_information_description`
--
ALTER TABLE `oc_information_description`
  ADD PRIMARY KEY (`information_id`,`language_id`);

--
-- Indexes for table `oc_information_to_layout`
--
ALTER TABLE `oc_information_to_layout`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Indexes for table `oc_information_to_store`
--
ALTER TABLE `oc_information_to_store`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Indexes for table `oc_language`
--
ALTER TABLE `oc_language`
  ADD PRIMARY KEY (`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_layout`
--
ALTER TABLE `oc_layout`
  ADD PRIMARY KEY (`layout_id`);

--
-- Indexes for table `oc_layout_module`
--
ALTER TABLE `oc_layout_module`
  ADD PRIMARY KEY (`layout_module_id`);

--
-- Indexes for table `oc_layout_route`
--
ALTER TABLE `oc_layout_route`
  ADD PRIMARY KEY (`layout_route_id`);

--
-- Indexes for table `oc_length_class`
--
ALTER TABLE `oc_length_class`
  ADD PRIMARY KEY (`length_class_id`);

--
-- Indexes for table `oc_length_class_description`
--
ALTER TABLE `oc_length_class_description`
  ADD PRIMARY KEY (`length_class_id`,`language_id`);

--
-- Indexes for table `oc_location`
--
ALTER TABLE `oc_location`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  ADD PRIMARY KEY (`manufacturer_id`);

--
-- Indexes for table `oc_manufacturer_to_store`
--
ALTER TABLE `oc_manufacturer_to_store`
  ADD PRIMARY KEY (`manufacturer_id`,`store_id`);

--
-- Indexes for table `oc_marketing`
--
ALTER TABLE `oc_marketing`
  ADD PRIMARY KEY (`marketing_id`);

--
-- Indexes for table `oc_modification`
--
ALTER TABLE `oc_modification`
  ADD PRIMARY KEY (`modification_id`);

--
-- Indexes for table `oc_module`
--
ALTER TABLE `oc_module`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `oc_mpseller`
--
ALTER TABLE `oc_mpseller`
  ADD PRIMARY KEY (`mpseller_id`);

--
-- Indexes for table `oc_mpseller_commission`
--
ALTER TABLE `oc_mpseller_commission`
  ADD PRIMARY KEY (`mpseller_commission_id`);

--
-- Indexes for table `oc_mpseller_enquiry`
--
ALTER TABLE `oc_mpseller_enquiry`
  ADD PRIMARY KEY (`mpseller_enquiry_id`);

--
-- Indexes for table `oc_mpseller_enquiry_chat`
--
ALTER TABLE `oc_mpseller_enquiry_chat`
  ADD PRIMARY KEY (`mpseller_enquiry_chat_id`);

--
-- Indexes for table `oc_mpseller_information_section`
--
ALTER TABLE `oc_mpseller_information_section`
  ADD PRIMARY KEY (`mpseller_information_section_id`);

--
-- Indexes for table `oc_mpseller_message`
--
ALTER TABLE `oc_mpseller_message`
  ADD PRIMARY KEY (`mpseller_message_id`);

--
-- Indexes for table `oc_mpseller_order_history`
--
ALTER TABLE `oc_mpseller_order_history`
  ADD PRIMARY KEY (`mpseller_order_history_id`);

--
-- Indexes for table `oc_mpseller_order_product`
--
ALTER TABLE `oc_mpseller_order_product`
  ADD PRIMARY KEY (`mpseller_order_product_id`);

--
-- Indexes for table `oc_mpseller_order_total`
--
ALTER TABLE `oc_mpseller_order_total`
  ADD PRIMARY KEY (`mpseller_order_total_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `oc_mpseller_review`
--
ALTER TABLE `oc_mpseller_review`
  ADD PRIMARY KEY (`mpseller_review_id`);

--
-- Indexes for table `oc_openpos_order`
--
ALTER TABLE `oc_openpos_order`
  ADD PRIMARY KEY (`pos_order_id`);

--
-- Indexes for table `oc_openpos_order_payment`
--
ALTER TABLE `oc_openpos_order_payment`
  ADD PRIMARY KEY (`pos_payment_id`);

--
-- Indexes for table `oc_openpos_product_log`
--
ALTER TABLE `oc_openpos_product_log`
  ADD PRIMARY KEY (`product_log_id`);

--
-- Indexes for table `oc_openpos_receipt_template`
--
ALTER TABLE `oc_openpos_receipt_template`
  ADD PRIMARY KEY (`template_id`);

--
-- Indexes for table `oc_openpos_register`
--
ALTER TABLE `oc_openpos_register`
  ADD PRIMARY KEY (`register_id`);

--
-- Indexes for table `oc_openpos_register_user`
--
ALTER TABLE `oc_openpos_register_user`
  ADD PRIMARY KEY (`register_user_id`);

--
-- Indexes for table `oc_openpos_transaction`
--
ALTER TABLE `oc_openpos_transaction`
  ADD PRIMARY KEY (`transaction_id`);

--
-- Indexes for table `oc_openpos_z_report`
--
ALTER TABLE `oc_openpos_z_report`
  ADD PRIMARY KEY (`z_id`);

--
-- Indexes for table `oc_option`
--
ALTER TABLE `oc_option`
  ADD PRIMARY KEY (`option_id`);

--
-- Indexes for table `oc_option_description`
--
ALTER TABLE `oc_option_description`
  ADD PRIMARY KEY (`option_id`,`language_id`);

--
-- Indexes for table `oc_option_value`
--
ALTER TABLE `oc_option_value`
  ADD PRIMARY KEY (`option_value_id`);

--
-- Indexes for table `oc_option_value_description`
--
ALTER TABLE `oc_option_value_description`
  ADD PRIMARY KEY (`option_value_id`,`language_id`);

--
-- Indexes for table `oc_order`
--
ALTER TABLE `oc_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `oc_order_history`
--
ALTER TABLE `oc_order_history`
  ADD PRIMARY KEY (`order_history_id`);

--
-- Indexes for table `oc_order_option`
--
ALTER TABLE `oc_order_option`
  ADD PRIMARY KEY (`order_option_id`);

--
-- Indexes for table `oc_order_product`
--
ALTER TABLE `oc_order_product`
  ADD PRIMARY KEY (`order_product_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `oc_order_recurring`
--
ALTER TABLE `oc_order_recurring`
  ADD PRIMARY KEY (`order_recurring_id`);

--
-- Indexes for table `oc_order_recurring_transaction`
--
ALTER TABLE `oc_order_recurring_transaction`
  ADD PRIMARY KEY (`order_recurring_transaction_id`);

--
-- Indexes for table `oc_order_shipment`
--
ALTER TABLE `oc_order_shipment`
  ADD PRIMARY KEY (`order_shipment_id`);

--
-- Indexes for table `oc_order_status`
--
ALTER TABLE `oc_order_status`
  ADD PRIMARY KEY (`order_status_id`,`language_id`);

--
-- Indexes for table `oc_order_total`
--
ALTER TABLE `oc_order_total`
  ADD PRIMARY KEY (`order_total_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `oc_order_voucher`
--
ALTER TABLE `oc_order_voucher`
  ADD PRIMARY KEY (`order_voucher_id`);

--
-- Indexes for table `oc_product`
--
ALTER TABLE `oc_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `oc_product_attribute`
--
ALTER TABLE `oc_product_attribute`
  ADD PRIMARY KEY (`product_id`,`attribute_id`,`language_id`);

--
-- Indexes for table `oc_product_description`
--
ALTER TABLE `oc_product_description`
  ADD PRIMARY KEY (`product_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  ADD PRIMARY KEY (`product_discount_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_product_filter`
--
ALTER TABLE `oc_product_filter`
  ADD PRIMARY KEY (`product_id`,`filter_id`);

--
-- Indexes for table `oc_product_image`
--
ALTER TABLE `oc_product_image`
  ADD PRIMARY KEY (`product_image_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_product_option`
--
ALTER TABLE `oc_product_option`
  ADD PRIMARY KEY (`product_option_id`);

--
-- Indexes for table `oc_product_option_value`
--
ALTER TABLE `oc_product_option_value`
  ADD PRIMARY KEY (`product_option_value_id`);

--
-- Indexes for table `oc_product_recurring`
--
ALTER TABLE `oc_product_recurring`
  ADD PRIMARY KEY (`product_id`,`recurring_id`,`customer_group_id`);

--
-- Indexes for table `oc_product_related`
--
ALTER TABLE `oc_product_related`
  ADD PRIMARY KEY (`product_id`,`related_id`);

--
-- Indexes for table `oc_product_reward`
--
ALTER TABLE `oc_product_reward`
  ADD PRIMARY KEY (`product_reward_id`);

--
-- Indexes for table `oc_product_special`
--
ALTER TABLE `oc_product_special`
  ADD PRIMARY KEY (`product_special_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_product_to_category`
--
ALTER TABLE `oc_product_to_category`
  ADD PRIMARY KEY (`product_id`,`category_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `oc_product_to_download`
--
ALTER TABLE `oc_product_to_download`
  ADD PRIMARY KEY (`product_id`,`download_id`);

--
-- Indexes for table `oc_product_to_layout`
--
ALTER TABLE `oc_product_to_layout`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Indexes for table `oc_product_to_store`
--
ALTER TABLE `oc_product_to_store`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Indexes for table `oc_recurring`
--
ALTER TABLE `oc_recurring`
  ADD PRIMARY KEY (`recurring_id`);

--
-- Indexes for table `oc_recurring_description`
--
ALTER TABLE `oc_recurring_description`
  ADD PRIMARY KEY (`recurring_id`,`language_id`);

--
-- Indexes for table `oc_return`
--
ALTER TABLE `oc_return`
  ADD PRIMARY KEY (`return_id`);

--
-- Indexes for table `oc_return_action`
--
ALTER TABLE `oc_return_action`
  ADD PRIMARY KEY (`return_action_id`,`language_id`);

--
-- Indexes for table `oc_return_history`
--
ALTER TABLE `oc_return_history`
  ADD PRIMARY KEY (`return_history_id`);

--
-- Indexes for table `oc_return_reason`
--
ALTER TABLE `oc_return_reason`
  ADD PRIMARY KEY (`return_reason_id`,`language_id`);

--
-- Indexes for table `oc_return_status`
--
ALTER TABLE `oc_return_status`
  ADD PRIMARY KEY (`return_status_id`,`language_id`);

--
-- Indexes for table `oc_review`
--
ALTER TABLE `oc_review`
  ADD PRIMARY KEY (`review_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_seo_url`
--
ALTER TABLE `oc_seo_url`
  ADD PRIMARY KEY (`seo_url_id`),
  ADD KEY `query` (`query`),
  ADD KEY `keyword` (`keyword`);

--
-- Indexes for table `oc_session`
--
ALTER TABLE `oc_session`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `oc_setting`
--
ALTER TABLE `oc_setting`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `oc_shipping_courier`
--
ALTER TABLE `oc_shipping_courier`
  ADD PRIMARY KEY (`shipping_courier_id`);

--
-- Indexes for table `oc_smsa`
--
ALTER TABLE `oc_smsa`
  ADD PRIMARY KEY (`consignment_id`);

--
-- Indexes for table `oc_statistics`
--
ALTER TABLE `oc_statistics`
  ADD PRIMARY KEY (`statistics_id`);

--
-- Indexes for table `oc_stock_status`
--
ALTER TABLE `oc_stock_status`
  ADD PRIMARY KEY (`stock_status_id`,`language_id`);

--
-- Indexes for table `oc_store`
--
ALTER TABLE `oc_store`
  ADD PRIMARY KEY (`store_id`);

--
-- Indexes for table `oc_success_orders_payments_tracking`
--
ALTER TABLE `oc_success_orders_payments_tracking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  ADD PRIMARY KEY (`tax_class_id`);

--
-- Indexes for table `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  ADD PRIMARY KEY (`tax_rate_id`);

--
-- Indexes for table `oc_tax_rate_to_customer_group`
--
ALTER TABLE `oc_tax_rate_to_customer_group`
  ADD PRIMARY KEY (`tax_rate_id`,`customer_group_id`);

--
-- Indexes for table `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  ADD PRIMARY KEY (`tax_rule_id`);

--
-- Indexes for table `oc_theme`
--
ALTER TABLE `oc_theme`
  ADD PRIMARY KEY (`theme_id`);

--
-- Indexes for table `oc_translation`
--
ALTER TABLE `oc_translation`
  ADD PRIMARY KEY (`translation_id`);

--
-- Indexes for table `oc_upload`
--
ALTER TABLE `oc_upload`
  ADD PRIMARY KEY (`upload_id`);

--
-- Indexes for table `oc_user`
--
ALTER TABLE `oc_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `oc_user_group`
--
ALTER TABLE `oc_user_group`
  ADD PRIMARY KEY (`user_group_id`);

--
-- Indexes for table `oc_voucher`
--
ALTER TABLE `oc_voucher`
  ADD PRIMARY KEY (`voucher_id`);

--
-- Indexes for table `oc_voucher_history`
--
ALTER TABLE `oc_voucher_history`
  ADD PRIMARY KEY (`voucher_history_id`);

--
-- Indexes for table `oc_voucher_theme`
--
ALTER TABLE `oc_voucher_theme`
  ADD PRIMARY KEY (`voucher_theme_id`);

--
-- Indexes for table `oc_voucher_theme_description`
--
ALTER TABLE `oc_voucher_theme_description`
  ADD PRIMARY KEY (`voucher_theme_id`,`language_id`);

--
-- Indexes for table `oc_wallet_transaction`
--
ALTER TABLE `oc_wallet_transaction`
  ADD PRIMARY KEY (`transaction_id`);

--
-- Indexes for table `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  ADD PRIMARY KEY (`weight_class_id`);

--
-- Indexes for table `oc_weight_class_description`
--
ALTER TABLE `oc_weight_class_description`
  ADD PRIMARY KEY (`weight_class_id`,`language_id`);

--
-- Indexes for table `oc_zone`
--
ALTER TABLE `oc_zone`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `oc_zone_to_geo_zone`
--
ALTER TABLE `oc_zone_to_geo_zone`
  ADD PRIMARY KEY (`zone_to_geo_zone_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `oc_address`
--
ALTER TABLE `oc_address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oc_api`
--
ALTER TABLE `oc_api`
  MODIFY `api_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oc_api_ip`
--
ALTER TABLE `oc_api_ip`
  MODIFY `api_ip_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `oc_api_session`
--
ALTER TABLE `oc_api_session`
  MODIFY `api_session_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;

--
-- AUTO_INCREMENT for table `oc_attribute`
--
ALTER TABLE `oc_attribute`
  MODIFY `attribute_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `oc_attribute_group`
--
ALTER TABLE `oc_attribute_group`
  MODIFY `attribute_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oc_banner`
--
ALTER TABLE `oc_banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  MODIFY `banner_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=279;

--
-- AUTO_INCREMENT for table `oc_blogger`
--
ALTER TABLE `oc_blogger`
  MODIFY `blogger_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oc_blogger_comment`
--
ALTER TABLE `oc_blogger_comment`
  MODIFY `blogger_comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oc_cart`
--
ALTER TABLE `oc_cart`
  MODIFY `cart_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `oc_category`
--
ALTER TABLE `oc_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `oc_country`
--
ALTER TABLE `oc_country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;

--
-- AUTO_INCREMENT for table `oc_coupon`
--
ALTER TABLE `oc_coupon`
  MODIFY `coupon_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oc_coupon_history`
--
ALTER TABLE `oc_coupon_history`
  MODIFY `coupon_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_coupon_product`
--
ALTER TABLE `oc_coupon_product`
  MODIFY `coupon_product_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_currency`
--
ALTER TABLE `oc_currency`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `oc_customer`
--
ALTER TABLE `oc_customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oc_customer_activity`
--
ALTER TABLE `oc_customer_activity`
  MODIFY `customer_activity_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_customer_approval`
--
ALTER TABLE `oc_customer_approval`
  MODIFY `customer_approval_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_customer_group`
--
ALTER TABLE `oc_customer_group`
  MODIFY `customer_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oc_customer_history`
--
ALTER TABLE `oc_customer_history`
  MODIFY `customer_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_customer_ip`
--
ALTER TABLE `oc_customer_ip`
  MODIFY `customer_ip_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `oc_customer_login`
--
ALTER TABLE `oc_customer_login`
  MODIFY `customer_login_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oc_customer_reward`
--
ALTER TABLE `oc_customer_reward`
  MODIFY `customer_reward_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oc_customer_search`
--
ALTER TABLE `oc_customer_search`
  MODIFY `customer_search_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_customer_transaction`
--
ALTER TABLE `oc_customer_transaction`
  MODIFY `customer_transaction_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oc_custom_field`
--
ALTER TABLE `oc_custom_field`
  MODIFY `custom_field_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_custom_field_value`
--
ALTER TABLE `oc_custom_field_value`
  MODIFY `custom_field_value_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_download`
--
ALTER TABLE `oc_download`
  MODIFY `download_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_event`
--
ALTER TABLE `oc_event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `oc_extension`
--
ALTER TABLE `oc_extension`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `oc_extension_install`
--
ALTER TABLE `oc_extension_install`
  MODIFY `extension_install_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oc_extension_path`
--
ALTER TABLE `oc_extension_path`
  MODIFY `extension_path_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `oc_filter`
--
ALTER TABLE `oc_filter`
  MODIFY `filter_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `oc_filter_group`
--
ALTER TABLE `oc_filter_group`
  MODIFY `filter_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oc_geo_zone`
--
ALTER TABLE `oc_geo_zone`
  MODIFY `geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oc_hittech_dhl_details_new`
--
ALTER TABLE `oc_hittech_dhl_details_new`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_information`
--
ALTER TABLE `oc_information`
  MODIFY `information_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oc_language`
--
ALTER TABLE `oc_language`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oc_layout`
--
ALTER TABLE `oc_layout`
  MODIFY `layout_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `oc_layout_module`
--
ALTER TABLE `oc_layout_module`
  MODIFY `layout_module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1013;

--
-- AUTO_INCREMENT for table `oc_layout_route`
--
ALTER TABLE `oc_layout_route`
  MODIFY `layout_route_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;

--
-- AUTO_INCREMENT for table `oc_length_class`
--
ALTER TABLE `oc_length_class`
  MODIFY `length_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `oc_location`
--
ALTER TABLE `oc_location`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  MODIFY `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `oc_marketing`
--
ALTER TABLE `oc_marketing`
  MODIFY `marketing_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_modification`
--
ALTER TABLE `oc_modification`
  MODIFY `modification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oc_module`
--
ALTER TABLE `oc_module`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `oc_mpseller`
--
ALTER TABLE `oc_mpseller`
  MODIFY `mpseller_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_mpseller_commission`
--
ALTER TABLE `oc_mpseller_commission`
  MODIFY `mpseller_commission_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_mpseller_enquiry`
--
ALTER TABLE `oc_mpseller_enquiry`
  MODIFY `mpseller_enquiry_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_mpseller_enquiry_chat`
--
ALTER TABLE `oc_mpseller_enquiry_chat`
  MODIFY `mpseller_enquiry_chat_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_mpseller_information_section`
--
ALTER TABLE `oc_mpseller_information_section`
  MODIFY `mpseller_information_section_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_mpseller_message`
--
ALTER TABLE `oc_mpseller_message`
  MODIFY `mpseller_message_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_mpseller_order_history`
--
ALTER TABLE `oc_mpseller_order_history`
  MODIFY `mpseller_order_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_mpseller_order_product`
--
ALTER TABLE `oc_mpseller_order_product`
  MODIFY `mpseller_order_product_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_mpseller_order_total`
--
ALTER TABLE `oc_mpseller_order_total`
  MODIFY `mpseller_order_total_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_mpseller_review`
--
ALTER TABLE `oc_mpseller_review`
  MODIFY `mpseller_review_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_openpos_order`
--
ALTER TABLE `oc_openpos_order`
  MODIFY `pos_order_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_openpos_order_payment`
--
ALTER TABLE `oc_openpos_order_payment`
  MODIFY `pos_payment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_openpos_product_log`
--
ALTER TABLE `oc_openpos_product_log`
  MODIFY `product_log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_openpos_receipt_template`
--
ALTER TABLE `oc_openpos_receipt_template`
  MODIFY `template_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_openpos_register`
--
ALTER TABLE `oc_openpos_register`
  MODIFY `register_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_openpos_register_user`
--
ALTER TABLE `oc_openpos_register_user`
  MODIFY `register_user_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_openpos_transaction`
--
ALTER TABLE `oc_openpos_transaction`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_openpos_z_report`
--
ALTER TABLE `oc_openpos_z_report`
  MODIFY `z_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_option`
--
ALTER TABLE `oc_option`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `oc_option_value`
--
ALTER TABLE `oc_option_value`
  MODIFY `option_value_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `oc_order`
--
ALTER TABLE `oc_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `oc_order_history`
--
ALTER TABLE `oc_order_history`
  MODIFY `order_history_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `oc_order_option`
--
ALTER TABLE `oc_order_option`
  MODIFY `order_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `oc_order_product`
--
ALTER TABLE `oc_order_product`
  MODIFY `order_product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `oc_order_recurring`
--
ALTER TABLE `oc_order_recurring`
  MODIFY `order_recurring_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_order_recurring_transaction`
--
ALTER TABLE `oc_order_recurring_transaction`
  MODIFY `order_recurring_transaction_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_order_shipment`
--
ALTER TABLE `oc_order_shipment`
  MODIFY `order_shipment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_order_status`
--
ALTER TABLE `oc_order_status`
  MODIFY `order_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `oc_order_total`
--
ALTER TABLE `oc_order_total`
  MODIFY `order_total_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;

--
-- AUTO_INCREMENT for table `oc_order_voucher`
--
ALTER TABLE `oc_order_voucher`
  MODIFY `order_voucher_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_product`
--
ALTER TABLE `oc_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  MODIFY `product_discount_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=489;

--
-- AUTO_INCREMENT for table `oc_product_image`
--
ALTER TABLE `oc_product_image`
  MODIFY `product_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2668;

--
-- AUTO_INCREMENT for table `oc_product_option`
--
ALTER TABLE `oc_product_option`
  MODIFY `product_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=227;

--
-- AUTO_INCREMENT for table `oc_product_option_value`
--
ALTER TABLE `oc_product_option_value`
  MODIFY `product_option_value_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `oc_product_reward`
--
ALTER TABLE `oc_product_reward`
  MODIFY `product_reward_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=588;

--
-- AUTO_INCREMENT for table `oc_product_special`
--
ALTER TABLE `oc_product_special`
  MODIFY `product_special_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=485;

--
-- AUTO_INCREMENT for table `oc_recurring`
--
ALTER TABLE `oc_recurring`
  MODIFY `recurring_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_return`
--
ALTER TABLE `oc_return`
  MODIFY `return_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_return_action`
--
ALTER TABLE `oc_return_action`
  MODIFY `return_action_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `oc_return_history`
--
ALTER TABLE `oc_return_history`
  MODIFY `return_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_return_reason`
--
ALTER TABLE `oc_return_reason`
  MODIFY `return_reason_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `oc_return_status`
--
ALTER TABLE `oc_return_status`
  MODIFY `return_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `oc_review`
--
ALTER TABLE `oc_review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oc_seo_url`
--
ALTER TABLE `oc_seo_url`
  MODIFY `seo_url_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=953;

--
-- AUTO_INCREMENT for table `oc_setting`
--
ALTER TABLE `oc_setting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7343;

--
-- AUTO_INCREMENT for table `oc_smsa`
--
ALTER TABLE `oc_smsa`
  MODIFY `consignment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_statistics`
--
ALTER TABLE `oc_statistics`
  MODIFY `statistics_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `oc_stock_status`
--
ALTER TABLE `oc_stock_status`
  MODIFY `stock_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `oc_store`
--
ALTER TABLE `oc_store`
  MODIFY `store_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_success_orders_payments_tracking`
--
ALTER TABLE `oc_success_orders_payments_tracking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  MODIFY `tax_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  MODIFY `tax_rate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  MODIFY `tax_rule_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;

--
-- AUTO_INCREMENT for table `oc_theme`
--
ALTER TABLE `oc_theme`
  MODIFY `theme_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_translation`
--
ALTER TABLE `oc_translation`
  MODIFY `translation_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_upload`
--
ALTER TABLE `oc_upload`
  MODIFY `upload_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oc_user`
--
ALTER TABLE `oc_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `oc_user_group`
--
ALTER TABLE `oc_user_group`
  MODIFY `user_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `oc_voucher`
--
ALTER TABLE `oc_voucher`
  MODIFY `voucher_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_voucher_history`
--
ALTER TABLE `oc_voucher_history`
  MODIFY `voucher_history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oc_voucher_theme`
--
ALTER TABLE `oc_voucher_theme`
  MODIFY `voucher_theme_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `oc_wallet_transaction`
--
ALTER TABLE `oc_wallet_transaction`
  MODIFY `transaction_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  MODIFY `weight_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `oc_zone`
--
ALTER TABLE `oc_zone`
  MODIFY `zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4239;

--
-- AUTO_INCREMENT for table `oc_zone_to_geo_zone`
--
ALTER TABLE `oc_zone_to_geo_zone`
  MODIFY `zone_to_geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
