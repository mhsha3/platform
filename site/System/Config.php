<?php
/*
error_reporting(0);
ini_set('display_errors', 0);
*/
// Builder Assistant
define('Builder', false);
// Heey Framework Version
define( 'HEEY_VERSION', '4' );
// ERROR Directory
define( 'ERRORSDIR', './application/pages/Errors/' );
// Language Directory
define( 'LANGSDIR', './application/langs/' );
// Static Files 
define( 'ASSETSDIR', 'application/assets' );
// Defulte language
define( 'LANG', 'english' );
