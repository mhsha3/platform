<?php
define( 'BASEDIR', str_replace("index.php","",$_SERVER['PHP_SELF']) );
session_start();

if(Builder){Route::set('Builder',function(){View::make('Builder');});}

function _error($x){return '<span style="color:red;">'.$x.'</span>';}
function __($x){(!is_array($x)) ? print($x) : print_r($x);}

function _Param($x=false){
	$param = explode('/', str_replace(BASEDIR, '', $_SERVER['REQUEST_URI']));
	return ($x) ? (isset($param[$x])) ? $param[$x] : _error('Not Found This Parametar') : $param;
}

function _SPA(){
  if(isset($_POST['_SPA__'])){
     return true;
  }else{
    return false;
  }
}

function _Session($x,$v=true){
  $_SESSION[$x] = $v;
}

function _Lang(){
	if(!isset($_SESSION['_LANG_'])){
       	_Session('_LANG_',LANG);
	}
	if(isset($_GET['lang'])){
       if(file_exists(LANGSDIR.$_GET['lang'].'.php')){
           _Session('_LANG_',$_GET['lang']);
       }
	}
	return $_SESSION['_LANG_'];
}

_Lang();

function _t($x=''){
      require_once(LANGSDIR._Lang().'.php');
      if(isset($x) && !empty($x) && isset($lang[$x])){
      	return $lang[$x];
      }else{
      	return _error('Not Found This Word In Language File');
      }
  }
  
function IP() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}