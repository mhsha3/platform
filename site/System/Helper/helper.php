<?php
function send_sms($num,$msg,$alert = false){
  $postRequest = '{
    "userName": "lanaapp",
    "numbers": "'.str_replace(' ','', $num).'",
    "userSender": "lanaappsa",
    "apiKey": "394a8ae59f7363e9c93d9f698cb88a53",
    "msg": "'.$msg.'"
  }';

  $cURLConnection = curl_init('https://www.msegat.com/gw/sendsms.php');
  curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, $postRequest);
  curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);
  $apiResponse = curl_exec($cURLConnection);
  curl_close($cURLConnection);
  $jsonArrayResponse = json_decode($apiResponse);

  if($alert) {
    if($jsonArrayResponse->code = '1') {
      $x = '<div class="alert alert-success"> تم ارسال رسالة نصية لجوالك</div>';
    } elseif($jsonArrayResponse->code = '1120') {
      $x = '<div class="alert alert-danger"> الرقم الجوال خطأ</div>';
    } elseif($jsonArrayResponse->code = '1061') {
      $x = '<div class="alert alert-info"> تم ارسال الرسالة بالفعل </div>';
    } else {
      $x = 'error';
    }
  } else {
    $x =  $jsonArrayResponse->code;
  }

  return $x;
}
function RandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
// Copy Store -->
function folder_copy($src,$dst) {
    $dir = opendir($src);
    @mkdir($dst);
    while(false !== ( $file = readdir($dir)) ) {
        if (( $file != '.' ) && ( $file != '..' )) {
            if ( is_dir($src . '/' . $file) ) {
                folder_copy($src . '/' . $file,$dst . '/' . $file);
            }
            else {
                copy($src . '/' . $file,$dst . '/' . $file);
            }
        }
    }
    closedir($dir);
}
function CreateDB($dbname,$id,$username,$email,$password,$storename){
    $conn = new mysqli(PLATFORM_DB_HOST, PLATFORM_DB_USER, PLATFORM_DB_PASS);
    $conn->query("CREATE DATABASE $dbname");
    mysqli_select_db($conn, $dbname);
    mysqli_query($conn,"SET NAMES utf8");
    $query = '';
    $sqlScript = file(HOME_DIR . '/site/source/database.sql');
    foreach ($sqlScript as $line){
      $startWith = substr(trim($line), 0 ,2);
      $endWith = substr(trim($line), -1 ,1);
      if (empty($line) || $startWith == '--' || $startWith == '/*' || $startWith == '//') {
        continue;
      }
      $query = $query . $line;
      if ($endWith == ';') {
        mysqli_query($conn,str_replace('LnaSpace',$storename,$query));
        $query= '';
      }
    }
    $salt = RandomString(9);
    $password = sha1($salt . sha1($salt . sha1($password)));
    $date = date('Y-m-d',strtotime("+15 days"));
    $conn->query("UPDATE `oc_setting` SET `value`='$id' WHERE `key` = 'config_lana_store_id'");
    $conn->query("UPDATE `oc_setting` SET `value`='$date' WHERE `key` = 'config_store_expire'");
    $conn->query("UPDATE `oc_user` SET `username`='$username', `email`='$email',`salt`='$salt', `password`='$password' WHERE `user_id` = '1'");
}
// Create New Store Function -->
function CreateNewStore($db,$data=[]){
   $database_name = 'dbstore_'.$data['path'].'_'.RandomString(4);
   $data['database_name'] = $database_name;
   $data['store_type'] = '0';
   $data['created_at'] = date("Y-m-d H:i:s");
   $data['expire_date'] = date("Y-m-d H:i:s",strtotime("+15 days"));
   $data['status'] = '1';
   $data['package_id'] = '1';
   $data['sales_status'] = '0';
   $path = $data['path'];
   $username = $data['username'];
   $email = $data['email'];
   $password = $data['password'];
   $storename = $data['storename'];
   $id = $db->insert('stores',$data);
   if($id){
      if(!is_dir(HOME_DIR . '/'.$data['path'])){
        folder_copy(HOME_DIR . '/site/source/dfsdfsd9', HOME_DIR . '/lnasa.space/'.$data['path']);
        $content_Config = file_get_contents(HOME_DIR . '/lnasa.space/'.$data['path'].'/config.php');
        $content_htaccess = file_get_contents(HOME_DIR . '/lnasa.space/'.$data['path'].'/.htaccess');
        $find = ['dbstore_dfsdfsd9_9BmY','dfsdfsd9'];
        $replace = [$data['database_name'],$data['path']];
        file_put_contents(HOME_DIR . '/lnasa.space/'.$data['path'].'/config.php',str_replace($find, $replace, $content_Config));
        file_put_contents(HOME_DIR . '/lnasa.space/'.$data['path'].'/.htaccess',str_replace($find, $replace, $content_htaccess));
         CreateDB($data['database_name'],$id,$username,$email,$password,$storename);
         $_SESSION['_DBNAME_'] = $data['database_name'];
         $_SESSION['_PATH_'] = $data['path'];
         if(isset($_SESSION['_PARTNER_TRACKING_'])){
              $db->update('partners_process',['store_id'=>$id],['id'=>$_SESSION['_PARTNER_TRACKING_']]);
         }
       // echo '<div class"alert alert-success">done created store</div>';
        unset($_SESSION['_DATA__']);
        unset($_SESSION['_ins']);
        unset($_SESSION['_verifyPhoneCode_']);
        echo '<script>location.href="https://lanaapp.com/site/login"</script>';
    }else{
       echo 'Some Error #205 Contact To Lana Support Team';
    }
   }
}
