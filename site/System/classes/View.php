<?php

class View {
  public static function make($view) {
    if (Route::isRouteValid()) {
        require_once( './application/pages/'.$view.'.php' );
        return 1;
    }
  }
   public static function Content($sidebar){
      if(file_exists('./application/content/'.$sidebar.'.php')){
         require_once ('./application/content/'.$sidebar.'.php');
        }else{
          __( _error('<h1 style="red">Not Found This Content</h1>') );
             // require_once(ERRORSDIR.'404.php');
        }
    }
}
