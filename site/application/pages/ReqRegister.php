<?php
if(isset($_GET['req'])){
    if (isset($_SESSION['verifyCode']) && isset($_GET['_token']) && md5($_SESSION['verifyCode']) == $_GET['_token']) {
        $msg = '';
        if (
            isset($_POST['name']) && !empty($_POST['name']) &&
            isset($_POST['store_name']) && !empty($_POST['store_name']) &&
            isset($_POST['email']) && !empty($_POST['email']) &&
            isset($_POST['password']) && !empty($_POST['password']) &&
            isset($_POST['phone']) && !empty($_POST['phone']) &&
            isset($_POST['path']) && !empty($_POST['path'])
        ) {
           
            function limit($min, $max, $value)
            {
                if ($value < $min) return false;
                if ($value > $max) return false;
                return true;
            }

            function validate_phone_number($phone)
            {
                $filtered_phone_number = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);
                $phone_to_check = str_replace("-", "", $filtered_phone_number);
                if (strlen($phone_to_check) < 10 || strlen($phone_to_check) > 14) {
                    return false;
                } else {
                    return true;
                }
            }

            $db = new DB;

      
            if (!limit(3, 15, strlen($_POST['store_name']))) {
                $msg = '<div class="alert alert-danger"> اسم المتجر يجب ان يكون مابين 3 الى 15 </div>';
            }
            if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                $msg = '<div class="alert alert-danger"> صيغة الايميل غير صحيحة </div>';
            }
            if ($db->select('stores', ['where' => ['email' => $_POST['email']]])) {
                $msg = '<div class="alert alert-danger"> الايميل مستخدم من قبل </div>';
            }
            if (!limit(5, 20, strlen($_POST['password']))) {
                $msg = '<div class="alert alert-danger"> كلمة المرور يجب ان يكون مابين 5 الى 20 </div>';
            }
            if (!preg_match("/^[a-zA-Z0-9]+$/", $_POST['path'])) {
                $msg = '<div class="alert alert-danger"> يجب ان يكون اسم المتجر باللغة الانجليزية </div>';
            }
            if (is_numeric($_POST['path'])) {
                $msg = '<div class="alert alert-danger"> يجب ان يكون اسم المتجر باللغة الانجليزية </div>';
            }
            if (!limit(5, 10, strlen($_POST['path']))) {
                $msg = '<div class="alert alert-danger"> اسم المتجر يجب ان يكون ما بين 5 الى 10 باللغة الانجليزية </div>';
            }
            if ($db->select('stores', ['where' => ['path' => strtolower($_POST['path'])]])) {
                $msg = '<div class="alert alert-danger"> اسم مسار المتجر مستخدم من قبل </div>';
            }
            $phone = str_replace([' ', '-'], '', $_POST['phone']);
            if (validate_phone_number($phone) != true) {
                $msg = '<div class="alert alert-danger">  رقم الجوال خطأ </div>';
            }
            if ($db->select('stores', ['where' => ['phone' => $phone]])) {
                $msg = '<div class="alert alert-danger"> رقم الجوال مستخدم من قبل </div>';
            }

            if ($msg == '') {
                $data = [
                    'firstname' => strip_tags($_POST['name']),
                    'email' => $_POST['email'],
                    'username' => 'admin',
                    'password' => $_POST['password'],
                    'storename' => strip_tags($_POST['store_name']),
                    'path' => strip_tags($_POST['path']),
                    'phone' => $phone
                ];
                $_SESSION['_DATA__'] = $data;
                $_SESSION['_ins'] = md5(microtime());
                if (send_sms($phone, $_SESSION['verifyCode'])) {
                    echo "<script type='text/javascript'>$('#verifyPhone').modal('show');$('#_Vp').html('{$phone}');</script>";
                }
            }

        } else {
            $msg = '<div class="alert alert-danger"> من فضلك ادخل كافة البيانات </div>';
        }
        echo $msg;
    }
}
if(isset($_GET['_cVP']) && isset($_GET['_token']) &&
    isset($_POST['cVP']) && isset($_SESSION['verifyCode'])
){
    if(is_numeric($_POST['cVP']) && $_SESSION['verifyCode'] == $_POST['cVP']){
        $_SESSION['_xX'] = $_Xx = md5(uniqid());
        echo "<script type='text/javascript'>location.href='setup?_X={$_Xx}';</script>";
    }else{
        echo '<div class="alert alert-danger"> لقد ادخلت كود التاكيد خطأ  </div>';
    }
}
?>