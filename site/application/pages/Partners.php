<?php
if(isset($_GET['id']) && !empty($_GET['id'])){
    $db = new DB;
    $check = $db->select('partners',['where'=>['partner_id'=>(int)$_GET['id']]]);
    if($check){
        $data = [
            'partner_id'=>$_GET['id'],
            'client_data'=>json_encode(['ip'=>IP(),'referer'=>$_SERVER['HTTP_REFERER'] ?? '']),
            'created_at'=>date("Y-m-d H:i:s")
            ];
        $id = $db->insert('partners_process',$data);
        $_SESSION['_PARTNER_TRACKING_'] = $id;
    }
}

  header("Location: ${PLATFORM_DOMAIN}/site/register");

?>