<?php
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL); */
if(isset($_SESSION['_DBNAME_']) && isset($_SESSION['_PATH_'])){
   exit();
}
// login -->
if(isset($_GET['_c']) && isset($_POST['_d']) && !empty($_POST['_d'])){
   $db = new DB;
   $key = [
            'lanaapp.com/',
            'https://',
            'http://',
            'www.',
            '/'
          ];
   $domain = str_replace($key,'',str_replace(["'",'"','DROP'],'',$_POST['_d']));
   $data = $db->query("SELECT * FROM `stores` where `domain` = '$domain' OR `path` = '$domain' limit 1");
   if($data){
        if(isset($_SESSION['user_token'])){unset($_SESSION['user_token']);}
        $_SESSION['_DBNAME_'] = $data[0]['database_name'];
        $_SESSION['_PATH_'] = $data[0]['path'];
        if($data[0]['domain'] != '' && !empty($data[0]['domain'])){
            $_SESSION['_DOMAIN_'] = $data[0]['domain'];
        }
            echo '<div class="alert alert-success"> جاري تحويلك لمتجرك</div>';
            echo '<script>setTimeout(function(){window.location="'. PLATFORM_DOMAIN .'/admin";},1500);</script>';
    }else{
       echo '<div class="alert alert-danger">لم يتم العثور على هذا المتجر</div>';
   }
}
// registration phone check send verify phone -->
if(isset($_GET['_vp']) && isset($_POST['_vpT']) && isset($_POST['_ph'])){
  if($_POST['_vpT'] == $_SESSION['_t'] && !empty($_POST['_ph'])){
    if(preg_match('/^[\+0-9\-\(\)\s]*$/', $_POST['_ph'])){
        if(!isset($_SESSION['_verifyPhoneCode_'])){
            $_SESSION['_verifyPhoneCode_'] = rand('1001','9999');
        }
       echo send_sms($_POST['_ph'],$_SESSION['_verifyPhoneCode_'],true);
    }else{
      echo '<div class="alert alert-danger"> رقم الجوال غير صحيح </div>';
    }
 }
}
// Registeration Check -->
if(isset($_GET['_r']) && isset($_GET['_t'])){
  if($_GET['_t'] == $_SESSION['_t']){
  $msg = '';
  if( isset($_POST['firstname']) && isset($_POST['lastname']) && isset($_POST['email']) && isset($_POST['username']) && isset($_POST['password']) && isset($_POST['storename']) && isset($_POST['path']) && isset($_POST['phonecode']) && isset($_POST['phonenumber']) && isset($_POST['verify_code']) && !empty($_POST['firstname']) && !empty($_POST['lastname']) && !empty($_POST['email']) && !empty($_POST['username']) && !empty($_POST['password']) && !empty($_POST['storename']) && !empty($_POST['path']) && !empty($_POST['phonecode']) && !empty($_POST['phonenumber']) && !empty($_POST['verify_code'])){
       $db = new DB;
       function limit($min,$max,$value){
          if( $value < $min ) return false;
          if( $value > $max ) return false;
         return true;
       }
       if(!filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)){
         $msg = '<div class="alert alert-danger"> صيغة الايميل غير صحيحة </div>';
       }
       if($db->select('stores',['where'=>['email'=>$_POST['email']]])){
         $msg = '<div class="alert alert-danger"> الايميل مستخدم من قبل </div>';
       }
       if(is_numeric($_POST['username'])){
         $msg = '<div class="alert alert-danger"> اسم المستخدم غير صحيح </div>';
       }
       if(!limit(5,10,strlen($_POST['username']))){
         $msg = '<div class="alert alert-danger"> اسم المستخدم يجب ان يكون مابين 5 الى 10 </div>';
       }
       if(!limit(5,20,strlen($_POST['password']))){
         $msg = '<div class="alert alert-danger"> كلمة المرور يجب ان تكون ما بين 5 الى 20 </div>';
       }
       if($db->select('stores',['where'=>['username'=>$_POST['username']]])){
         $msg = '<div class="alert alert-danger"> اسم المستخدم مستخدم من قبل </div>';
       }
       if(!preg_match("/^[a-zA-Z0-9]+$/", $_POST['path'])){
         $msg = '<div class="alert alert-danger"> يجب ان يكون اسم المتجر باللغة الانجليزية </div>';
       }
       if(is_numeric($_POST['path'])){
         $msg = '<div class="alert alert-danger"> يجب ان يكون اسم المتجر باللغة الانجليزية </div>';
       }
       if(!limit(5,10,strlen($_POST['path']))){
         $msg = '<div class="alert alert-danger"> اسم المتجر يجب ان يكون ما بين 5 الى 10 باللغة الانجليزية </div>';
       }
       if($db->select('stores',['where'=>['path'=>strtolower($_POST['path'])]])){
         $msg = '<div class="alert alert-danger"> اسم المتجر مستخدم من قبل </div>';
       }
       if($db->select('stores',['where'=>['phone'=>$_POST['phonecode'].$_POST['phonenumber']]])){
         $msg = '<div class="alert alert-danger"> رقم الهاتف مستخدم من قبل </div>';
       }
       if($_SESSION['_verifyPhoneCode_'] != $_POST['verify_code'] ){
         $msg = '<div class="alert alert-danger"> كود تاكيد الهاتف خطأ </div>';
       }
       if($msg == ''){
          $data = [
            'firstname'=> strip_tags($_POST['firstname']),
            'lastname' => strip_tags($_POST['lastname']),
            'email' => $_POST['email'],
            'username' => $_POST['username'],
            'password' => $_POST['password'],
            'storename'=> $_POST['storename'],
            'path'=> $_POST['path'],
            'phone'=> $_POST['phonecode'].$_POST['phonenumber']
          ];
          $_SESSION['_DATA__'] = $data;
          $_SESSION['_ins'] = md5(microtime());
          echo "<script type='text/javascript'>
  $('div.spanner').addClass('show');
  $('div.overlay').addClass('show');
  $.post('_ins',{_ins:'".$_SESSION['_ins']."'},function(data){
    $('.msg').html(data);
});";
       }
  }else{
    $msg = '<div class="alert alert-danger"> من فضلك ادخل كافة البيانات </div>';
  }
  echo $msg;
 }
}
?>
