<?php

if(isset($_SESSION['_DBNAME_']) && isset($_SESSION['_PATH_'])){
    header("Location: ${PLATFORM_DOMAIN}/site/login");
    exit();
}
$useragent=$_SERVER['HTTP_USER_AGENT'];
if(!preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
    header("Location: ${PLATFORM_DOMAIN}/site/register");
}
$db = new DB;
$db->insert('visitors',[
    'ip'=>IP(),
    'user_agent'=>$_SERVER['HTTP_USER_AGENT'] ?? '',
    'visited_at'=>date("Y-m-d H:i:s"),
    'referrer'=>$_SERVER['HTTP_REFERER'] ?? '',
    'page'=>'mobile_register',
    ]);
$_SESSION['_verifyPhoneCode_'] = rand('1001','9999');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>إنشاء متجر | لنا</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="application/assets/images/icons/favicon.ico"/>
    <link rel="stylesheet" type="text/css" href="application/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="application/assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="application/assets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <link rel="stylesheet" type="text/css" href="application/assets/css/util.css">
    <link rel="stylesheet" type="text/css" href="application/assets/css/main.css">
    <style type="text/css">.spanner{position:fixed;top:50%;left:0;background:#2a2a2a55;width:100%;display:block;text-align:center;height:100%;color:#fff;transform:translateY(-50%);z-index:1000;visibility:hidden}.overlay{position:fixed;width:100%;height:100%;background:rgba(0,0,0,.5);visibility:hidden}.loader,.loader:after,.loader:before{border-radius:50%;width:2.5em;height:2.5em;-webkit-animation-fill-mode:both;animation-fill-mode:both;-webkit-animation:load7 1.8s infinite ease-in-out;animation:load7 1.8s infinite ease-in-out}.loader{color:#fff;font-size:10px;margin:80px auto;position:relative;text-indent:-9999em;-webkit-transform:translateZ(0);-ms-transform:translateZ(0);transform:translateZ(0);-webkit-animation-delay:-.16s;animation-delay:-.16s}.loader:after,.loader:before{content:'';position:absolute;top:0}.loader:before{left:-3.5em;-webkit-animation-delay:-.32s;animation-delay:-.32s}.loader:after{left:3.5em}@-webkit-keyframes load7{0%,100%,80%{box-shadow:0 2.5em 0 -1.3em}40%{box-shadow:0 2.5em 0 0}}@keyframes load7{0%,100%,80%{box-shadow:0 2.5em 0 -1.3em}40%{box-shadow:0 2.5em 0 0}}.show{visibility:visible}.overlay,.spanner{opacity:0;-webkit-transition:all .3s;-moz-transition:all .3s;transition:all .3s}.overlay.show,.spanner.show{opacity:1}
    </style>
</head>
<body style="background-color: #666666;">
<div class="overlay"></div>
<div class="spanner">
    <div class="loader"></div>
    <p class="text-white">الرجاء الانتظار حتي يتم انشاء متجرك</p>
</div>
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <form id="login100" class="login100-form validate-form text-right"  dir="rtl">
                <div class="text-center pb-3">
                    <img src="application/assets/logo.webp" width="20%">
                </div>
<div class="row">
    <div class="col">
        <div class="wrap-input100 validate-input" data-validate="من فضلك ادخل الاسم الاول">
            <input class="input100" name="firstname" required placeholder="الاسم الاول" autocomplete="off" type="text">
        </div>
    </div>
    <div class="col">
        <div class="wrap-input100 validate-input" data-validate="من فضلك ادخل اسم العائلة او اللقب">
            <input class="input100" name="lastname" required placeholder="اسم العائلة او اللقب" autocomplete="off" type="text">
        </div>
    </div>
</div>

                <div class="wrap-input100 validate-input" data-validate = "من فضلك ادخل البريد الالكترونى">
                    <input class="input100" name="email" required placeholder="البريد الالكترونى" autocomplete="off" type="email">
                </div>

                <div class="wrap-input100 validate-input" data-validate="ادخل اسم المستخدم باللغة الانجليزية">
                    <input class="input100" name="username" required placeholder="اسم المستخدم ( باللغة الانجليزية )" autocomplete="off" type="text">
                </div>

                <div class="wrap-input100 validate-input" data-validate="من فضلك ادخل كلمة المرور لا تقل عن 5 احروف">
                    <input class="input100" name="password" required placeholder="كلمة المرور" autocomplete="off" type="password">
                </div>

                <div class="wrap-input100 validate-input" data-validate="ادخل اسم المتجر ">
                    <input class="input100" type="text" required placeholder="اسم المتجر" autocomplete="off" name="storename">
                </div>

                <div class="wrap-input100 validate-input" data-validate="ادخل عنوان المتجر باللغة الانجليزية">
                    <input class="input100" type="text" required placeholder="عنوان المتجر ( باللغة العربية )" name="path" autocomplete="off">
                </div>

                <div class="row">
                    <div class="col-8">
                        <div class="wrap-input100 validate-input" data-validate="ادخل عنوان المتجر باللغة الانجليزية">
                            <input class="input100 _inp" required placeholder="رقم الجوال" onkeyup="show()" autocomplete="off" name="phonenumber" type="text" name="text">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group wrap-input100 p-2">
                            <label>كود الدولة</label>
                            <select dir="ltr" required name="phonecode" style="background:transparent;" class="form-control _inc">
                                <option value="+966">+966</option>
                                <option value="+965">+965</option>
                                <option value="+974">+974</option>
                                <option value="+973">+973</option>
                                <option value="+968">+968</option>
                                <option value="+971">+971</option>
                                <option value="+20">+20</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="container-login100-form-btn text-center m-3" style="display:none;">
                    <button type="button" onclick="send()"  class="btn btn-dark _bV text-white"><i class="fa fa-mobile  p-1 text-white"></i> ارسال كود التفعيل على الجوال</button>
                </div>

                <div style="display:none;" class="wrap-input100 validate-input" data-validate="ادخل كود التفعيل المكون من 4 ارقام المرسل على رقم جوالك">
                    <input class="input100 text-center" required placeholder="ادخل كود التفعيل المكون من 4 ارقام" name="verify_code" pattern="\d*" autocomplete="off" type="text" maxlength="4" name="text"  <?php echo 'value="'.$_SESSION['_verifyPhoneCode_'].'"'; ?>>
                </div>
                <div class="msg"></div>

                <div class="container-login100-form-btn">
                    <button type="submit" class="login100-form-btn btn btn-warning">
                        <b> <i class="fa fa-rocket p-1"></i> إنشاء متجرك الان </b>
                    </button>
                </div>

            </form>

            <div class="login100-more" style="background-image: url('application/assets/images/art.jpg');">
            </div>
        </div>
    </div>
</div>
<script src="application/assets/vendor/jquery/jquery-3.2.1.min.js"></script>
<script src="application/assets/vendor/bootstrap/js/popper.js"></script>
<script src="application/assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script>
    var _t = "<?php $_SESSION['_t'] = md5(microtime()); echo $_SESSION['_t']; ?>";
    function send(){
        if($('._inp').val() != '' && $('._inc').val() != ''){
            $.post('req?_vp',{_vpT:_t,_ph:$('._inc').val()+$('._inp').val()},function(data){
                $('.msg').html(data);
            });
        }else{
            $('.msg').html('<div class="alert alert-danger"> ادخل رقم الهاتف </div>');
        }
    }
    function show(){if($('._inp').val().length > 7){$('._bV').removeAttr("disabled")}else{$("._bV").attr("disabled", true);}}
    $('#login100').submit(function (e){
        e.preventDefault();
        $.post('req?_r&_t='+_t,$('.login100-form').serialize(),function(data){
            $('.msg').html(data);
        });
    });
</script>
</body>
</html>
