<?php
if(isset($_SESSION['_DBNAME_']) && isset($_SESSION['_PATH_'])){
  header("Location: ${PLATFORM_DOMAIN}/admin");
  exit();
}
?>
<!DOCTYPE html>
<html lang="ar">
<head>
<title>تسجيل الدخول</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/x-icon" href="https://<?php echo PLATFORM_DOMAIN; ?>/admin/view/image/favicon.png">
<link rel="stylesheet" href="application/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="application/assets/css/login.css">
<style type="text/css">
  @font-face {
    font-family: 'Tajawal';
    font-style: normal;
    font-weight: 300;
    font-display: swap;
    src: url(https://fonts.gstatic.com/s/tajawal/v3/Iurf6YBj_oCad4k1l5qjHrRpiYlJ.woff2) format('woff2');
    unicode-range: U+0600-06FF, U+200C-200E, U+2010-2011, U+204F, U+2E41, U+FB50-FDFF, U+FE80-FEFC;
  }
  *{
    font-family:'Tajawal'!important;direction:rtl;text-align:right;
  }
  @media only screen and (max-width: 600px){.wrap-login100{padding: 65px 30px!important;}}
</style>
</head>
<body>
<div class="limiter _rtl _trtl">
<div class="container-login100">
<div class="wrap-login100">
<div class="login100-pic js-tilt" data-tilt>
<img src="application/assets/images/lanalogin.png" alt="IMG">
</div>
<div class="login100-form validate-form">
<span class="login100-form-title">
<a class="txt2" href="https://lna.sa"> <img src="application/assets/images/Logo Lna Final-1.png" width="150px"></a>
</span>
  <div id="msg"></div>
<div class="wrap-input100">
<input class="input100 domain" dir="rtl" type="text" name="domain" placeholder="ادخل اسم الدومين او الدومين الفرعي">
</div>
<div class="container-login100-form-btn">
  <button class="login100-form-btn submit">تسجيل الدخول</button>
</div>
<div class="text-center p-t-136">
        <br>
<div class="text-center">
      
        مستخدم جديد؟
        <a class="text-primary" href="/site/register">
          انقر هنا لإنشاء حساب جديد
        </a>
      
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script type="text/javascript">
  $('.submit').click(function(e){
     if($('.domain').val() != ''){
         $.post('req?_c',{_d:$('.domain').val()},function(data){
               $('#msg').html(data);
         });
     }else{
       $('#msg').html('<div class="alert alert-danger">من فضلك ادخل  اسم الدومين</div>');
     }
  });
  $(".domain").keydown(function (e) {
  if (e.keyCode == 13) {
    $('.submit').click();
  }
});
</script>
</html>