<?php
$_SESSION['verifyCode'] = $verifyCode = rand(1001,9999);
?>
<html>
<head>
    <meta charset='utf-8'><meta name='viewport' content='width=device-width, initial-scale=1'><title> لنا | إنشاء متجر جديد </title>
    <link href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
    <link rel="stylesheet" type="text/css" href="https://www.jqueryscript.net/demo/jQuery-International-Telephone-Input-With-Flags-Dial-Codes/build/css/intlTelInput.css">
    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <style>@import url(https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400&display=swap);*{margin:0;padding:0;box-sizing:border-box;font-family:Cairo,sans-serif;direction:rtl;text-align:right}body{background-color:#575756}.container{padding:20px 50px;min-height:450px}a{color:#333}a:hover{text-decoration:none;color:#444}button.btn,div.btn{background-color:#575756;color:#eee}button.btn:hover,div.btn:hover{background-color:#575756d7}.navbar-light .navbar-nav .nav-link{color:#333}nav{float:left}#language select{border:none;outline:0}.wrapper{width:85%;margin:20px auto}label{display:block;font-size:.8rem;font-weight:700}input{border:none;outline:0;border-bottom:2px solid #ddd;width:100%;padding-bottom:10px}.wrapper{clear:both}.wrapper .col-md-6:hover label{color:#575756}.wrapper .col-md-6:hover #country,.wrapper .col-md-6:hover input{border-color:#575756;cursor:pointer}.wrapper .col-md-6 input:focus{border-color:#575756}.option input{display:none}.option input:checked~.checkmark:after{display:block}.option .checkmark:after{content:"\2713";width:10px;height:10px;display:block;position:absolute;top:30%;left:50%;transform:translate(-50%,-50%) scale(0);transition:.2s ease-in-out 0s}.option:hover input[type=radio]~.checkmark{background-color:#f4f4f4}.option input[type=radio]:checked~.checkmark{background:#575756;color:#fff;transition:.3s ease-in-out 0s}.option input[type=radio]:checked~.checkmark:after{transform:translate(-50%,-50%) scale(1);color:#fff}@media (min-width:992px){.navbar-expand-lg .navbar-nav .nav-link{padding-right:1.5rem;padding-left:0}}@media(max-width:767px){.search input{width:90%}div.btn{width:100%}.container{padding:20px;margin-left:0}}@media(max-width:600px){body{background-color:#fff}._lo{width:50%}}.intl-tel-input .flag-dropdown .selected-flag{padding:0}.intl-tel-input{direction:ltr!important;text-align:left!important;text-align-last:left}#phone{direction:ltr!important}.rounded-lg{border-radius:10px}.error{color:#dc3545!important}#_Vp{direction:ltr!important;text-align:center!important}.flag-dropdown{direction:ltr!important}</style>
</head>
<body class='snippet-body'>
<div class="container bg-white mt-sm-4 mb-4 rounded-lg">
    <div class="wrapper d-flex justify-content-center flex-column px-md-4 px-1">
        <div class="text-center mb-4">
            <img class="_lo" loading="lazy" src="<?php echo PLATFORM_DOMAIN; ?>/site/application/assets/images/Logo%20Lna%20Final-1.png" width="18%">
        </div>
        <div class="m-2" id="msg"></div>
        <form autocomplete="off" id="_f">
            <div class="row my-4">
                <div class="col-md-6">
                    <label>الأسم الكريم</label>
                    <input name="name" id="name" required type="text" placeholder="أدخل الاسم الكريم">
                </div>
                <div class="col-md-6 pt-md-0 pt-4">
                    <label>أسم المتجر</label>
                    <input name="store_name" id="store_name" required type="text" placeholder="أدخل اسم المتجر">
                </div>
            </div>
            <div class="row my-md-4 my-2">
                <div class="col-md-6">
                    <label>البريد الإلكتروني</label>
                    <input name="email" id="email" required type="email" placeholder="أدخل البريد الالكتروني">
                </div>
                <div class="col-md-6 pt-md-0 pt-4">
                    <label>كلمة السر</label>
                    <input name="password" id="password" required type="password" placeholder="أدخل كلمة السر">
                </div>
            </div>
            <div class="row my-md-4 my-2">
                <div class="col-md-6">
                    <label>رقم الجوال</label>
                    <input name="phone" type="text" style="direction:ltr!important;text-align:left!important;padding-bottom:1px" required id="phone" placeholder="أدخل رقم الجوال صحيحا للتاكيد">
                </div>
                <div class="col-md-6 pt-md-0 pt-4">
                    <input name="path" id="path" required type="text" placeholder="اسم مسار المتجر ( باالغة الانجليزية )">
                    <small class="text-muted">
                        سيكون عنوان متجرك بهذا المثال ( lnasa.space/<span id="_xpN">path_name</span> ) يمكنك فيما بعد ربط اسم نطاق خاص بك  ( www.store.com )
                    </small>
                </div>
            </div>
            <div class="m-3">
                <hr>
                <small class="text-muted">بالتسجيل فانت توافق على الشروط و الاحكام </small>
            </div>
            <div class="d-flex justify-content-end">
                <button type="submit" class="btn"><i class="fa fa-rocket"></i> إنشاء متجرك الأن</button>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="verifyPhone" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="fa fa-mobile"></i> تاكيد رقم الجوال</h5>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <div id="Vmsq"></div>
                    <h5 class="text-center"> تم ارسال رسالة نصية الى رقم</h5>
                    <div class="p-2" id="_Vp"></div>
                    <input type="text" maxlength="4" id="cVP" class="form-control text-center" placeholder="ادخل رمز التاكيد المكون من 4 ارقام" <?php echo (isset($_GET['_test'])) ? 'value="'.$_SESSION['verifyCode'].'"' : ''; ?>>
                    <div class="mt-3 text-center">
                        <button class="btn btn-success" id="_cVP"> تاكيد </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://www.jqueryscript.net/demo/jQuery-International-Telephone-Input-With-Flags-Dial-Codes/build/js/intlTelInput.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/additional-methods.min.js"></script>
<script>
    $("#phone").intlTelInput({
        onlyCountries: ["sa","kw","qa","bh","are","om","eg"],
    });
    var validator = $("#_f").validate({
        rules: {
            name: {
                required: true,
                minlength: 3,
            },
            store_name: {
                required: true,
                minlength: 3,
                maxlength: 15,
            },
            password: {
                required: true,
                minlength: 5
            },
            path: {
                required: true,
                minlength: 5,
                maxlength: 10,
                alphanumeric:true
            },
            cVP: {
                required: true,
                minlength: 4,
                maxlength: 4,
                digit:true
            },
            email: {
                required: true,
                email: true
            },
        },
        messages: {
            name: {
                required: "من فضلك أدخل الاسم الكريم",
                minlength: "يجب ان ل ايقل الاسم عن 3",
            },
            store_name: {
                required: "من فضلك أدخل اسم المتجر",
                minlength: "يجب ان لا يقل الاسم عن 3",
                maxlength: "يجب ان لا يزيد الاسم عن 15",
            },
            password: {
                required: "من فضلك ادخل كلمة السر",
                minlength: "يجب ان لا تقل عن 5 احروف و ارقام"
            },
            path: {
                required: "من فضلك ادخل اسم مسار المتجر  باللغة الانجليزية",
                minlength: "يجب ان لا يقل عن 5 احروف",
                maxlength: "يجب ان لا يزيد عن 10 احروف",
                alphanumeric: "ادخل اسم مسار المتجر باللغة الانجليزية"
            },
            path: {
                required: "ادخال اربع ارقام فقط",
                maxlength: "ادخال اربع ارقام فقط",
                digit: "ادخال اربع ارقام فقط"
            },
            email: "من فضلك ادخل البريد الالكتروني",
        }
    });
    $('#path').change(function(){
        $('#_xpN').html($(this).val());
    });
    $('#_f').submit(function(e){
        e.preventDefault();
        if(!validator.numberOfInvalids()){
            $.post('_xReq?req&_token=<?php echo md5($verifyCode); ?>',$(this).serialize(),function(data){
                $("#msg").html(data);
                $(window).scrollTop(0);
            });
        }
    });
    $('#_cVP').click(function(){
            $.post('_xReq?_cVP&_token=<?php echo md5($verifyCode); ?>', {cVP:$('#cVP').val()}, function (data) {
                $("#Vmsq").html(data);
            });
    });
</script>
</body>
</html>