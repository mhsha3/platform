<?php
if(isset($_SESSION['_DBNAME_']) && isset($_SESSION['_PATH_'])){
   header("Location: https://lanaapp.com/site/login");
   exit();
}

$useragent=$_SERVER['HTTP_USER_AGENT'];
if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
    header('Location: https://lanaapp.com/site/_mobile');
}
$db = new DB;
$db->insert('visitors',[
    'ip'=>IP(),
    'user_agent'=>$_SERVER['HTTP_USER_AGENT'] ?? '',
    'visited_at'=>date("Y-m-d H:i:s"),
    'referrer'=>$_SERVER['HTTP_REFERER'] ?? '',
    'page'=>'desktop_register',
    ]);
$_SESSION['_verifyPhoneCode_'] = rand('1001','9999');
?>
<!DOCTYPE html>
<html lang="ar">
  <head>
      <meta charset="utf-8">
      <title>إنشاء متجر | لنا</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="author" content="lna">
      <script src="application/assets/js/jquery-3.3.1.min.js"></script>
      <link rel="stylesheet" href="application/assets/fonts/material-design-iconic-font/css/material-design-iconic-font.css">
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
      <link rel="stylesheet" href="application/assets/css/style.css">
      <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Cairo:wght@300;400&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA==" crossorigin="anonymous" />
      <style type="text/css">small,button,a,label,input{font-family:'Cairo'!important;}.steps{display:none}.spanner{position:fixed;top:50%;left:0;background:#2a2a2a55;width:100%;display:block;text-align:center;height:100%;color:#fff;transform:translateY(-50%);z-index:1000;visibility:hidden}.overlay{position:fixed;width:100%;height:100%;background:rgba(0,0,0,.5);visibility:hidden}.loader,.loader:after,.loader:before{border-radius:50%;width:2.5em;height:2.5em;-webkit-animation-fill-mode:both;animation-fill-mode:both;-webkit-animation:load7 1.8s infinite ease-in-out;animation:load7 1.8s infinite ease-in-out}.loader{color:#fff;font-size:10px;margin:80px auto;position:relative;text-indent:-9999em;-webkit-transform:translateZ(0);-ms-transform:translateZ(0);transform:translateZ(0);-webkit-animation-delay:-.16s;animation-delay:-.16s}.loader:after,.loader:before{content:'';position:absolute;top:0}.loader:before{left:-3.5em;-webkit-animation-delay:-.32s;animation-delay:-.32s}.loader:after{left:3.5em}@-webkit-keyframes load7{0%,100%,80%{box-shadow:0 2.5em 0 -1.3em}40%{box-shadow:0 2.5em 0 0}}@keyframes load7{0%,100%,80%{box-shadow:0 2.5em 0 -1.3em}40%{box-shadow:0 2.5em 0 0}}.show{visibility:visible}.overlay,.spanner{opacity:0;-webkit-transition:all .3s;-moz-transition:all .3s;transition:all .3s}.overlay.show,.spanner.show{opacity:1}
.form-content{
  direction:rtl;
  text-align:right;
}
</style>
  </head>
  <body>
<div class="overlay"></div>
<div class="spanner">
  <div class="loader"></div>
  <p class="text-white">الرجاء الانتظار حتي يتم انشاء متجرك</p>
</div>
    <div class="wrapper">
            <form action="" id="wizard">
                <h2></h2>
            <section>
            <div class="inner">
            <div class="image-holder">
              <img src="application/assets/images/art.jpg" alt="">
            </div>
            <div class="form-content" >
              <div class="form-header">
                <img src="<?php echo PLATFORM_DOMAIN; ?>/site/application/assets/logo.webp" width="22%">
              </div>
  <div class="form-row">
    <div class="col">
    <input type="text" name="firstname" autocomplete="off" class="form-control" placeholder="الاسم الاول">
    </div>
    <div class="col">
    <input type="text" name="lastname" autocomplete="off" class="form-control" placeholder="العائلة">
    </div>
  </div>
  <div class="form-row">
    <div class="col">
    <input type="email" name="email" autocomplete="off" class="form-control" placeholder="البريد الالكترونى">
    </div>
    <div class="col">
    <input type="text" style="font-size:13px;" name="username" autocomplete="off" class="form-control" placeholder="اسم المستخدم ( باللغة الانجليزية )">
    </div>
  </div>
  <div class="form-group">
    <input type="password" name="password" autocomplete="off" class="form-control" placeholder="كلمة المرور">
  </div>
  <div class="form-row">
    <div class="col">
    <input type="text" class="form-control" autocomplete="off" name="storename" placeholder="اسم متجرك">
    </div>
  </div>
  <div class="msg"></div>
            </div>
          </div>
          </section>
          <!-- SECTION 2 -->
            <h2></h2>
            <section>
            <div class="inner">
            <div class="image-holder">
              <img src="application/assets/images/art.jpg" alt="">
            </div>
            <div class="form-content">
              <div class="form-header">
                <img src="<?php echo PLATFORM_DOMAIN; ?>/site/application/assets/logo.webp" width="22%">
              </div>
    <div class="form-row" dir="ltr">
    <div class="col">
      <div class="input-group">
        <div class="input-group-prepend">
          <div class="input-group-text">lnasa.space/</div>
        </div>
        <input type="text" name="path" autocomplete="off" class="form-control" placeholder="...">
      </div>
    </div>
  </div>
  <div class="form-row d-flex" dir="ltr">
    <div class="col-3">
    <select type="text" name="phonecode" class="form-control _inc">
        <option value="+966">+966</option>
        <option value="+965">+965</option>
        <option value="+974">+974</option>
        <option value="+973">+973</option>
        <option value="+968">+968</option>
        <option value="+971">+971</option>
    </select>
    </div>
    <div class="col">
    <input type="number" class="form-control _inp" onkeyup="show()" autocomplete="off" name="phonenumber" placeholder="رقم الجوال">
    <small style="display:none;" class="form-text text-muted">اضغط فى الجهة اليمنى لارسال كود التفعيل</small>
    </div>
    <div class="col-3" style="display:none;">
      <button onclick="send()" type="button" disabled="true" class="btn btn-info _bV"><i class="fa fa-sms"></i> اضغط</button>
    </div>
  </div>
  <div class="form-row text-center" dir="ltr" style="display:none;">
    <div class="col">
    <input type="text" name="verify_code" pattern="\d*" maxlength="4" autocomplete="off" class="form-control" placeholder="ادخل كود التفعيل المكون من 4 ارقام"
    <?php echo 'value="'.$_SESSION['_verifyPhoneCode_'].'"'; ?> >
    </div>
  </div>
  <div class="form-group">
    <div class="form-check">
      <input class="form-check-input" type="checkbox" id="gridCheck">
      <label class="form-check-label mr-4" for="gridCheck">
<a href="https://www.lna.sa/%D8%B3%D9%8A%D8%A7%D8%B3%D8%A9-%D8%A7%D9%84%D8%AE%D8%B5%D9%88%D8%B5%D9%8A%D8%A9" target="_blank">         بالضغط هنا فانت توافق على الشروط و الاحكام الخاصة بالمنصة</a>
       </label>
    </div>
  </div>
              <div class="msg"></div>
            </div>
          </div>
          </section>
            </form>
    </div>
    <script src="application/assets/js/jquery.steps.js"></script>
<script type="text/javascript">
var _t = "<?php $_SESSION['_t'] = md5(microtime()); echo $_SESSION['_t']; ?>";
function send(){
if($('._inp').val() != '' && $('._inc').val() != ''){
$.post('req?_vp',{_vpT:_t,_ph:$('._inc').val()+$('._inp').val()},function(data){
    $('.msg').html(data);
});
}else{
  $('.msg').html('<div class="alert alert-danger"> ادخل رقم الهاتف </div>');
}
}
function show(){if($('._inp').val().length > 7){$('._bV').removeAttr("disabled")}else{$("._bV").attr("disabled", true);}}

$("#wizard").steps({
        headerTag: "h2",
        bodyTag: "section",
        transitionEffect: "fade",
        enableAllSteps: true,
        labels: {
            finish: "أنطلق",
            next: "التالي",
            previous: "الرجوع"
        },onFinished: function (event, currentIndex){
          $.post('req?_r&_t='+_t,$("#wizard").serialize(),function(data){
            $('.msg').html(data);
          });
        }
    });
    $('.wizard > .steps li a').click(function(){
      $(this).parent().addClass('checked');
    $(this).parent().prevAll().addClass('checked');
    $(this).parent().nextAll().removeClass('checked');
    });
    // Custome Jquery Step Button
    $('.forward').click(function(){
      $("#wizard").steps('next');
    })
    $('.backward').click(function(){
        $("#wizard").steps('previous');
    })
    // Select Dropdown
    $('html').click(function() {
        $('.select .dropdown').hide();
    });
    $('.select').click(function(event){
        event.stopPropagation();
    });
    $('.select .select-control').click(function(){
        $(this).parent().next().toggle();
    })
    $('.select .dropdown li').click(function(){
        $(this).parent().toggle();
        var text = $(this).attr('rel');
        $(this).parent().prev().find('div').text(text);
    });
</script>
</body>
</html>
