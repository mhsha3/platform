<?php
class ModelExtensionShippingClex extends Model {
	function getQuote($address) {
		$this->load->language('extension/shipping/clex');

		$status = true;
 
		$method_data = array();

		if ($status) {
			$quote_data = array();

			$quote_data['clex'] = array(
				'code'         => 'clex.clex',
				'title'        => $this->language->get('text_description'),
				'cost'         => $this->config->get('shipping_clex_cost'),
				'tax_class_id' => 0,
				'text'         => $this->currency->format($this->tax->calculate($this->config->get('shipping_clex_cost'), 0, $this->config->get('config_tax')), $this->session->data['currency'])
			);
 
			$method_data = array(
				'code'       => 'clex',
				'title'      => $this->language->get('text_title'),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('shipping_clex_sort_order'),
				'error'      => false
			);
		}

		return $method_data;
	}

	protected function getStringBetween($string, $start, $end){
		$string = ' ' . $string;
		$ini = strpos($string, $start);
		if ($ini == 0) return '';

		$ini += strlen($start);
		$len = strpos($string, $end, $ini) - $ini;
		return substr($string, $ini, $len);
	}

	public function getConsignmentId($order_id) {
        $query = $this->db->query("SELECT oh.date_added, os.name AS status, oh.comment, oh.notify FROM " . DB_PREFIX . "order_history oh LEFT JOIN " . DB_PREFIX . "order_status os ON oh.order_status_id = os.order_status_id WHERE oh.comment LIKE '%ConsignmentId%' AND oh.order_id = '" . (int)$order_id . "' AND os.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY oh.date_added ASC");
        $shipments = $query->rows;
        if($shipments) {
            foreach($shipments as $key=>$comment) {
                $cmnt_txt = ($comment['comment'])?$comment['comment']:'';
				$ref_no = $this->getStringBetween($cmnt_txt, '[ConsignmentId: ', ']');
                break;
            }
                
            return $ref_no;
        }else{
            return 0;
        }
	}

	public function getShipmentStatus($order_id) {
		require_once '/home/lanaapps/public_html/admin/controller/extension/shipping/clex/api.php';
        $this->api = new \Clex\Api($this->config->get('shipping_clex_access_token'));

		$ConsignmentIdNo = $this->getConsignmentId($order_id);
		if($ConsignmentIdNo > 0) {
			$trackStatusResponse = $this->api->trackStatus($ConsignmentIdNo);
			if (isset($trackStatusResponse['code']) && $trackStatusResponse['code'] == 200) {
				$output = $trackStatusResponse['data'][$ConsignmentIdNo];
				return $output['code'] . ': ' . $output['detail'];
			}
		}

		return null;
	}
}