<?php
class ModelExtensionModuleTabby extends Model {
    var $tabbyProducts = [
        'tabby_paylater'    => 'payLater',
        'tabby_installments'=> 'installments'
    ];
    var $apiUrl = 'https://api.tabby.ai/api/v1/payments/';
    var $errors = [];

	public function getCheckoutData() {
        $data = [];

        $data['tabbyApiKey'] = $this->config->get('module_tabby_public_key');
        $data['tabbyDebug']  = $this->config->get('module_tabby_debug');
        $data['merchantCode']  = $this->getMerchantCode();
        $data['tabbyProduct']= array_key_exists($this->session->data['payment_method']['code'], $this->tabbyProducts)
            ? $this->tabbyProducts[$this->session->data['payment_method']['code']]
            : $this->tabbyProducts['tabby_paylater'];
        $data['tabbyPayment']= $this->getPaymentObject();

        return $data;
	}
    public function addTransaction($data) {
        $this->db->query("INSERT INTO `" . DB_PREFIX . "tabby_transaction`
            (`order_id`, `transaction_id`, `body`)
            VALUES (
                '".$this->db->escape($data['order_id'])."',
                '".$this->db->escape($data['transaction_id'])."',
                '".$this->db->escape($data['body'])."'
            )");
    }
    protected function getMerchantCode() {
        return $this->session->data['shipping_address']['iso_code_2'];
    }
    protected function getPaymentObject() {
        $payment = [];
        

        $payment["order"] = []; 
        $payment['order']['reference_id'] = (string)$this->session->data['order_id'];
        $payment["order"]["items"] = []; 

        $this->load->model('checkout/order');

        $order_data = $this->model_checkout_order->getOrder($this->session->data['order_id']);

        $payment['amount']      = $this->formatAmount($order_data['total']);
        $payment['currency']    = $order_data['currency_code'];

        $images = [];
        foreach ($this->cart->getProducts() as $product) {
            $images[$product['product_id']] =  $product['image'];
        }

        foreach ($this->model_checkout_order->getOrderProducts($this->session->data['order_id']) as $product) {
            $product['image'] = $images[$product['product_id']] ?: 'placeholder.png';
            $payment["order"]["items"][] = array(
                'title'         => $product['name'],
                'reference_id'  => $product['model'],
                'unit_price'    => $this->formatAmount($product['price'] + $product['tax']),
                'tax_amount'    => $this->formatAmount($product['tax']),
                'quantity'      => (int)$product['quantity'],
                'product_url'   => $this->url->link('product/product', 'product_id=' . $product['product_id'], true),
                'image_url'     => $this->getBaseUrl() .'image/'. $product['image']
            );

        }
        // get discount
        $totals = $this->model_checkout_order->getOrderTotals($this->session->data['order_id']);
        $discount = 0;
        foreach ($totals as $total) {
            $discount += (in_array($total['code'], ['voucher', 'coupon'])) ? -$total['value'] : 0;
        }
        $payment['order']['discount_amount']  = $this->formatAmount($discount);

        $sub_total  = $this->cart->getSubTotal();
        $total      = $this->cart->getTotal();
        $tax_total  = $total - $sub_total;
        $shipping   = $shipping_cost = 0;

        if ($this->cart->hasShipping()) {
            $shipping_cost  = $this->session->data['shipping_method']['cost'];
            $tax_class      = $this->session->data['shipping_method']['tax_class_id'];

            $shipping       = $this->tax->calculate($shipping_cost, $tax_class, $this->config->get('config_tax')); 
        
            $payment["shipping_address"] = [
                'address'   => $this->session->data['shipping_address']['address_1'] . 
                    (!empty($this->session->data['shipping_address']['address_2']) ? ' ' : '') . 
                    $this->session->data['shipping_address']['address_2'],
                'city'      => $this->session->data['shipping_address']['city']
            ];
        }

        $payment['order']['tax_amount']  = $this->formatAmount($tax_total + $shipping - $shipping_cost);
        $payment['order']['shipping_amount']  = $this->formatAmount($shipping);

        $payment['buyer']   = $this->getBuyerObject();
        $payment['order_history'] = $this->getOrderHistoryObject($payment['buyer']);

        return json_encode($payment);
    }
    protected function getOrderHistoryObject($buyer) {
        $order_history = [];

        $this->load->model("checkout/order");

        // get Order details by email and phone

        $where_fields = [
            'email'            => $this->customer->isLogged() ? $this->customer->getEmail() : $this->session->data['guest']['email']
        ];

        if ($this->customer->isLogged()) {
            if ($this->customer->getTelephone()) $where['telephone'] = $this->customer->getTelephone();
        } else {
            if ($this->session->data['guest']['telephone']) {
                $where['telephone'] = $this->session->data['guest']['telephone'];
            };
        }

        $query = "SELECT order_id FROM `" . DB_PREFIX . "order`";
        $where = [];
        foreach ($where_fields as $name => $value) {
            $where[] = "$name = '".$this->db->escape($value)."'";
        }
        
        $order_query = $this->db->query($query . " WHERE order_status_id > 0 AND (" . implode(" OR ", $where) . ")");
        foreach ($order_query->rows as $row) {
            $order = $this->model_checkout_order->getOrder($row['order_id']);
            // TODO: bypass wrong orders
            if (false) continue;
    
            $order_history[] = [
                "amount"            => $this->formatAmount($order['total']),
                "buyer"             => $this->getOrderHistoryBuyerObject($order),
                "items"             => $this->getOrderHistoryItemsObject($order),
                "payment_method"    => $order['payment_code'],
                "purchased_at"      => date(\DateTime::RFC3339, strtotime($order['date_added'])),
                "shipping_address"  => $this->getOrderHistoryShippingAddressObject($order),
                "status"            => $this->getOrderHistoryStatus($order)
            ];
        }
        return $order_history;
    }
    protected function getOrderHistoryStatus($order) {
        $status = 'processing';
        switch ($order['order_status_id']) {
            case 7:
            case 9:
                $status = 'canceled';
                break;
            case 5:
            case 15:
                $status = 'complete';
                break;
            case 11:
            case 12:
            case 16:
                $status = 'refunded';
                break;
            case 1:
                $status = 'new';
        };
        return $status;
    }
    protected function getOrderHistoryShippingAddressObject($order) {
        return [
            'address'   => $order['shipping_address_1'] . ' ' . $order['shipping_address_2'],
            'city'      => $order['shipping_city']
        ];
    }
    protected function getOrderHistoryItemsObject($order) {
        $result = [];

        $products = $this->model_checkout_order->getOrderProducts($order['order_id']);
        
        foreach ($products as $product) {
            $result[] = [
                'quantity'      => (int)$product['quantity'],
                'title'         => $product['name'],
                'unit_price'    => $this->formatAmount($product['price'] + $product['tax']),
                'reference_id'  => $product['model'],
                'ordered'       => (int)$product['quantity']
            ];
        }

        return $result;
    }
    protected function getOrderHistoryBuyerObject($order) {
        return [
            'name'  => $order['firstname'] . ' ' . $order['lastname'],
            'phone' => $order['telephone']
        ];
    }

    protected function getBuyerObject() {

        $dob = null;

        if ($this->customer->isLogged()) {

            $this->load->model('account/customer');

            $ci = $this->model_account_customer->getCustomer($this->customer->getId());

            $name = $ci['firstname'] . ' ' . $ci['lastname'];
            $email = $ci['email'];
            $phone = $ci['telephone'];
        } else {
            $name   = $this->session->data['guest']['firstname'] . ' ' . $this->session->data['guest']['lastname'];
            $email  = $this->session->data['guest']['email'];
            $phone  = $this->session->data['guest']['telephone'];
        }
        
        return [
            "dob"   => $dob,
            "email" => $email,
            "name"  => $name,
            "phone" => $phone
        ];
    }
    protected function formatAmount($amount) {
        return number_format($amount, 2, '.', '');
    
    }
    public function execute($method, $endpoint, $data = array()) {

        $this->errors = [];

        $url = $this->apiUrl . $endpoint;

        $curl_options = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => array(),
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => 10
        );

        $curl_options[CURLOPT_HTTPHEADER][] = 'Accept-Charset: utf-8';
        $curl_options[CURLOPT_HTTPHEADER][] = 'Accept: application/json';
        $curl_options[CURLOPT_HTTPHEADER][] = 'Authorization: Bearer ' . $this->config->get('module_tabby_secret_key');;

        if ($method != "GET") {
            $data_json = json_encode($data);
            $curl_options[CURLOPT_HTTPHEADER][] = 'Content-Type: application/json';
            $curl_options[CURLOPT_HTTPHEADER][] = 'Content-Length: ' . strlen($data_json);
            $curl_options[CURLOPT_CUSTOMREQUEST] = strtoupper($method);
            $curl_options[CURLOPT_POSTFIELDS] = $data_json;
        }

        $ch = curl_init();
        curl_setopt_array($ch, $curl_options);
        $response = curl_exec($ch);

        $result = [];

        if (curl_errno($ch)) {
            $curl_code = curl_errno($ch);

            $constant = get_defined_constants(true);
            $curl_constant = preg_grep('/^CURLE_/', array_flip($constant['curl']));

            $this->errors[] = array('name' => $curl_constant[$curl_code], 'message' => curl_strerror($curl_code));
        }

        try {
            $result = json_decode($response);
        } catch (Exception $e) {
            $this->errors[] = ['name' => 'json', 'message' => $e->getMessage()];
        }

        return $result;
    }
    protected function getBaseUrl() {
        $server = null;
        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }
        return $server;
    }
}
class OrderHistoryInvalidOrderException extends Exception {};
