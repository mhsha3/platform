<?php
class ModelToolImage extends Model {
	public function resize($filename, $width, $height,$watermark = false) {
		if (!is_file(DIR_IMAGE . $filename) || substr(str_replace('\\', '/', realpath(DIR_IMAGE . $filename)), 0, strlen(DIR_IMAGE)) != str_replace('\\', '/', DIR_IMAGE)) {
			return;
		}

		$extension = pathinfo($filename, PATHINFO_EXTENSION);

		$image_old = $filename;
		$image_new = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . (int)$width . 'x' . (int)$height . '.' . $extension;

		if (!is_file(DIR_IMAGE . $image_new) || (filemtime(DIR_IMAGE . $image_old) > filemtime(DIR_IMAGE . $image_new))) {
			list($width_orig, $height_orig, $image_type) = getimagesize(DIR_IMAGE . $image_old);

			if (!in_array($image_type, array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF))) {
				return DIR_IMAGE . $image_old;
			}

			$path = '';

			$directories = explode('/', dirname($image_new));

			foreach ($directories as $directory) {
				$path = $path . '/' . $directory;

				if (!is_dir(DIR_IMAGE . $path)) {
					@mkdir(DIR_IMAGE . $path, 0777);
				}
			}

			if ($width_orig != $width || $height_orig != $height) {
				$image = new Image(DIR_IMAGE . $image_old);
				$image->resize($width, $height);
				$image->save(DIR_IMAGE . $image_new);
			} else {
				copy(DIR_IMAGE . $image_old, DIR_IMAGE . $image_new);
            }
            if($watermark == true && $this->config->get('config_watermark_status') == '1' && $this->config->get('config_watermark_image')){
                $main_img = DIR_IMAGE . $image_new;
                $watermark_img = DIR_IMAGE . $this->config->get('config_watermark_image');
                $image_size = getimagesize($main_img);
                $watermark_size = getimagesize($watermark_img);
                $image_mime = isset($image_size['mime']) ? $image_size['mime'] : '';
                $padding = 5;
                $opacity = 100;
                $watermark=imagecreatefrompng($watermark_img);
                if ($image_mime == 'image/gif'){$image = imagecreatefromgif($main_img);}
                elseif($image_mime == 'image/png'){$image=imagecreatefrompng($main_img);}
                elseif($image_mime == 'image/jpeg'){$image = imagecreatefromjpeg($main_img);}
                if(!$image || !$watermark) die("main image or watermark image could not be loaded!");
                $watermark_width = $watermark_size[0];
                $watermark_height = $watermark_size[1];
                $dest_x = $image_size[0] - $watermark_width - $padding;
                $dest_y = $image_size[1] - $watermark_height - $padding;
                imagecopymerge($image, $watermark, $dest_x, $dest_y, 0, 0, $watermark_width, $watermark_height, $opacity);
                imagejpeg($image, $main_img, 100);
                imagedestroy($image);
                imagedestroy($watermark);
            }
		}

		$image_new = str_replace(' ', '%20', $image_new);  // fix bug when attach image on email (gmail.com). it is automatic changing space " " to +

		if ($this->request->server['HTTPS']) {
			return $this->config->get('config_ssl') . 'image/' . $image_new;
		} else {
			return $this->config->get('config_url') . 'image/' . $image_new;
		}
	}
}
