<?php
class ControllerCommonHeaderLeft extends Controller {
	public function index() {
	    $data['socialmedia_facebook'] = $this->config->get('config_socialmedia_facebook');
	    $data['socialmedia_twitter'] = $this->config->get('config_socialmedia_twitter');
	    $data['socialmedia_instagram'] = $this->config->get('config_socialmedia_instagram');
	    $data['socialmedia_telegram'] = $this->config->get('config_socialmedia_telegram');
	    $data['socialmedia_snapchat'] = $this->config->get('config_socialmedia_snapchat');
	    $data['socialmedia_whatsapp'] = $this->config->get('config_socialmedia_whatsapp');
	    $data['socialmedia_youtube'] = $this->config->get('config_socialmedia_youtube');
	    $data['socialmedia_pinterest'] = $this->config->get('config_socialmedia_pinterest');
	    $data['socialmedia_vimeo'] = $this->config->get('config_socialmedia_vimeo');
		return $this->load->view('common/header_left', $data);
	}
}
