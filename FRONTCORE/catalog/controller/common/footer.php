<?php
class ControllerCommonFooter extends Controller {
	public function index() {
		$this->load->language('common/footer');
		$this->load->model('catalog/information');
		$data['informations'] = array();
		$data['footer_left'] = $this->load->controller('common/footer_left');
		$data['footer_right'] = $this->load->controller('common/footer_right');
		$data['ftop_full'] = $this->load->controller('common/ftop_full');
		$data['fbottom_full'] = $this->load->controller('common/fbottom_full');
		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);
			}
		}
		$data['contact'] = $this->url->link('information/contact');
		$data['return'] = $this->url->link('account/return/add', '', true);
		$data['sitemap'] = $this->url->link('information/sitemap');
		$data['tracking'] = $this->url->link('information/tracking');
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['voucher'] = $this->url->link('account/voucher', '', true);
		$data['affiliate'] = $this->url->link('affiliate/login', '', true);
		$data['special'] = $this->url->link('product/special');
		$data['account'] = $this->url->link('account/account', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['newsletter'] = $this->url->link('account/newsletter', '', true);

		$data['text_telephone'] = $this->config->get('config_telephone');
        $data['text_address'] = $this->config->get('config_address');
        $data['text_email'] = $this->config->get('config_email');
		$data['text_name'] = $this->config->get('config_name');
		$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));
        $data['logo'] = $this->config->get('config_logo');
        $data['description'] = $this->config->get('config_meta_description');
        $data['config_commercial_register'] = $this->config->get('config_commercial_register');
        $data['config_show_commercial_register'] = $this->config->get('config_show_commercial_register');
        $data['config_tax_number'] = $this->config->get('config_tax_number');
        $data['config_show_tax_number'] = $this->config->get('config_show_tax_number');

        $data['socialmedia_facebook'] = $this->config->get('config_socialmedia_facebook');
        $data['socialmedia_twitter'] = $this->config->get('config_socialmedia_twitter');
        $data['socialmedia_instagram'] = $this->config->get('config_socialmedia_instagram');
        $data['socialmedia_telegram'] = $this->config->get('config_socialmedia_telegram');
        $data['socialmedia_snapchat'] = $this->config->get('config_socialmedia_snapchat');
        $data['socialmedia_whatsapp'] = $this->config->get('config_socialmedia_whatsapp');
        $data['socialmedia_youtube'] = $this->config->get('config_socialmedia_youtube');
        $data['socialmedia_pinterest'] = $this->config->get('config_socialmedia_pinterest');
        $data['socialmedia_vimeo'] = $this->config->get('config_socialmedia_vimeo');
        $data['socialmedia_maroof'] = $this->config->get('config_socialmedia_maroof');

        $data['check_packages'] = $this->config->get('config_packages_status') ?? 0;
        $data['packages_title'] = $this->config->get('config_packages_title')[$this->config->get('config_language_id')] ?? '';
        $data['packages_link'] = $this->url->link('common/packages');
        
		// Whos Online
			$this->load->model('tool/online');
			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}
			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = ($this->request->server['HTTPS'] ? 'https://' : 'http://') . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}
			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}
			$this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);

		$data['scripts'] = $this->document->getScripts('footer');
		
		return $this->load->view('common/footer', $data);
	}
}
