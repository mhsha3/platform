<?php
class ControllerCommonPackages extends Controller {
    public function index(){
        if ($this->config->get('config_packages_status') == 1){
            $this->load->language('common/global');
            $this->document->setTitle($this->config->get('config_meta_title'));
            $this->document->setDescription($this->config->get('config_meta_description'));
            $this->document->setKeywords($this->config->get('config_meta_keyword'));
            $data['breadcrumbs'] = array();
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/home')
            );
            $data['breadcrumbs'][] = array(
                'text' => $this->config->get('config_packages_title')[$this->config->get('config_language_id')],
                'href' => $this->url->link('common/packages')
            );
            $data['packages_title'] = $this->config->get('config_packages_title')[$this->config->get('config_language_id')] ?? '';
            $data['packages_description'] = $this->config->get('config_packages_description')[$this->config->get('config_language_id')] ?? '';
            foreach ($this->db->query("SELECT * FROM `" . DB_PREFIX . "store_packages` WHERE `status` = '1' ORDER BY sort_by ASC")->rows as $package) {
                $data['packages'][] = [
                    'link' => $this->url->link('common/packages/show', 'id=' . $package['id']),
                    'title' => json_decode($package['title'],true)[$this->config->get('config_language_id')] ?? '',
                    'description' => json_decode($package['description'],true)[$this->config->get('config_language_id')] ?? '',
                    'price' => $this->currency->format($package['price'], $this->config->get('config_currency'), '', true),
                    'recommended' => $package['recommended']
                ];
            }
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');
            $this->response->setOutput($this->load->view('common/packages', $data));
        }else{
            $this->response->redirect($this->url->link('common/home'));
        }
    }

    public function show() {
        if(isset($this->request->get['id']) && $package = $this->db->query("SELECT * FROM `".DB_PREFIX."store_packages` WHERE `status` = '1' AND `id` = '".(int)$this->request->get['id']."'")->row){
            $this->load->language('common/global');
            $this->document->setTitle($this->config->get('config_meta_title'));
            $this->document->setDescription($this->config->get('config_meta_description'));
            $this->document->setKeywords($this->config->get('config_meta_keyword'));
            $data['breadcrumbs'] = array();
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/home')
            );
            $data['breadcrumbs'][] = array(
                'text' => $this->config->get('config_packages_title')[$this->config->get('config_language_id')],
                'href' => $this->url->link('common/packages')
            );
            $data['breadcrumbs'][] = array(
                'text' => json_decode($package['title'],true)[$this->config->get('config_language_id')] ?? '',
                'href' => $this->url->link('common/packages/show','id='.$package['id'])
            );

            $data['package'] = [
                'id' => $package['id'],
                'title' => json_decode($package['title'],true)[$this->config->get('config_language_id')] ?? '',
                'description' => json_decode($package['description'],true)[$this->config->get('config_language_id')] ?? '',
                'price' => $this->currency->format($package['price'],$this->config->get('config_currency'),'', true),
                'recommended' => $package['recommended'],
                'inputs_data' => json_decode($package['inputs_data'],true),
            ];

            $this->session->data['_token'] = $data['_token'] = token(32);

            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');
            $this->response->setOutput($this->load->view('common/show_package', $data));
        }else{
            $this->response->redirect($this->url->link('common/home'));
        }
    }

    protected function Auth(){
        $apiId = $this->config->get('payment_paylink_apiId');
        $secretKey = $this->config->get('payment_paylink_secretKey');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://restapi.paylink.sa/api/auth');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,'{"apiId":"'.$apiId.'","persistToken":false,"secretKey":"'.$secretKey.'"}');
        $headers = [];
        $headers[] = 'Accept: */*';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        return json_decode(curl_exec($ch), true)['id_token'];
    }
    public function addInvoice(){
        if(isset($this->session->data['_token']) && isset($this->request->get['id']) && isset($this->request->post['_token']) && $this->request->post['_token'] ==  $this->session->data['_token'] && $package = $this->db->query("SELECT * FROM `".DB_PREFIX."store_packages` WHERE `status` = '1' AND `id` = '".(int)$this->request->get['id']."'")->row){
            if(isset($this->request->post['inputs'])){
                $this->session->data['dataInputs'] = json_encode($this->request->post['inputs']);
            }
            $amount = $this->currency->format($package['price'],$this->config->get('config_currency'),'', false);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://restapi.paylink.sa/api/addInvoice');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,'{
           "amount": '.$amount.',
           "callBackUrl": "'.$this->url->link('common/packages/confirm').'",
           "clientEmail": "",
           "clientMobile": "",
           "clientName": "",
           "note": "'.$this->config->get('config_lana_store_id').'_Pacakge_'.$this->request->get['id'].'_'.HTTP_SERVER.'",
           "orderNumber": "'.$this->config->get('config_lana_store_id').'_'.$this->request->get['id'].'",
           "products": []}');
            $headers = array();
            $headers[] = 'Accept: application/json;charset=UTF-8';
            $headers[] = 'Authorization: Bearer '.$this->Auth();
            $headers[] = 'Content-Type: application/json';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $result = json_decode(curl_exec($ch),true);
            if (curl_errno($ch)){ echo 'Error:' . curl_error($ch);}
            curl_close($ch);
            if(strtolower($result['orderStatus']) == 'created'){
                $this->session->data['PayLink_transactionNo'] = $result['transactionNo'];
                $this->session->data['package_id'] = $this->request->get['id'];
                echo "<script>location.href='{$result['url']}';</script>";
            }else{
                echo $result['detail'] ?? '';
            }
        }
    }

    public function confirm(){
        $json = [];
        if (isset($this->session->data['package_id']) && isset($this->session->data['PayLink_transactionNo']) && isset($this->request->get['orderNumber']) && isset($this->request->get['transactionNo'])){
            if ($this->session->data['PayLink_transactionNo'] == $this->request->get['transactionNo']){
                $ch = curl_init();
                curl_setopt($ch,CURLOPT_URL,'https://restapi.paylink.sa/api/getInvoice/'.(int)$this->session->data['PayLink_transactionNo']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                $headers = [];
                $headers[] = 'Accept: application/json;charset=UTF-8';
                $headers[] = 'Authorization: Bearer ' . $this->Auth();
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                $result = curl_exec($ch);
                if (curl_errno($ch)) {
                    echo 'Error:' . curl_error($ch);
                }
                curl_close($ch);
                $result = json_decode($result, true);
                $customer_id = 0;
                if ($result['orderStatus'] == 'Paid') {
                    $this->db->query("INSERT INTO `".DB_PREFIX."store_packages_orders` (`id`, `package_id`, `amount`, `customer_id`, `inputs_data`, `status`, `created_at`) VALUES (NULL, '".(int)$this->session->data['package_id']."', '".$result['amount']."', '".$customer_id."', '".$this->db->escape($this->session->data['dataInputs'])."', '1', '".date("Y-m-d H:i:s")."');");
                    $this->db->query("INSERT INTO `".DB_PREFIX."success_orders_payments_tracking` (`id`, `transaction_num`,`order_id`,`amount`, `payment_company`, `response`, `created_at`, `lana_payed`) VALUES (NULL, '".$this->request->get['transactionNo']."', '0','".$result['amount']."','paylink' ,'".$this->db->escape(json_encode($result))."', '".date("Y-m-d H:i:s")."', '0')");
                    unset($this->session->data['_token']);
                    unset($this->session->data['package_id']);
                    unset($this->session->data['dataInputs']);
                    $this->response->redirect($this->url->link('checkout/success'));
                }else{
                    $this->session->data['FAILED_PAYMENT'] = ['payment_method'=>'paylink','payment_transaction'=>$this->request->get['transactionNo']];
                    $this->response->redirect($this->url->link('checkout/failed'));
                }
            }else{
                $this->response->redirect($this->url->link('common/home'));
            }
        }else{
            $this->response->redirect($this->url->link('common/home'));
        }
    }
}
