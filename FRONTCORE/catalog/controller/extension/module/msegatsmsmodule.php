<? /* This file encoded by Raizlabs PHP Obfuscator http://www.raizlabs.com/software */ ?>
<?php

class ControllerExtensionModulemsegatsmsmodule extends Controller
{
    private $error = array();

    public function index($setting)
    {
        
        $this->load->language('extension/module/msegatsmsmodule');
        $data['heading_title'] = $this->language->get('heading_title');
        $data['text_nohp'] = $this->language->get('text_nohp');
        $data['text_message'] = $this->language->get('text_message');
        $data['text_characters'] = $this->language->get('text_characters');
        $data['button_send'] = $this->language->get('button_send');
        $data['text_success'] = $this->language->get('text_success');
        $data['error_nohp'] = '';
        $data['error_message'] = '';
        $data['success'] = '';
        $data['error'] = '';
        $data['error_limit'] = '';


        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }

            $tgl = date('y-m-d');
            if (!$this->request->post['nohp']) {
                $data['error_nohp'] = $this->language->get('error_nohp');
            } else if (!$this->request->post['message']) {
                $data['error_message'] = $this->language->get('error_message');
            } else {
                $msegatsms = $this->config->get('msegatsms');

                if(isset($msegatsms['smslimit']) && !empty($msegatsms['smslimit'])){
                    $ceklimit = $msegatsms['smslimit'];
                }else{
                    $ceklimit = '';
                }
               
                $traceip = $this->config->get($ip);
                if ($traceip == "") {
                    $visitor_data = array($ip => '1:' . $tgl);
                    $this->model_setting_setting->msegateditSetting('msegatsendsms', $visitor_data);
                    $traceip = '0';
                    $tglnya = $tgl;
                } else {
                    $traceip = $this->config->get($ip);
                    $pecah = explode(":", $traceip);
                    $traceip = $pecah[0];
                    $tglnya = $pecah[1];
                }


                if (($ceklimit != 0 || $ceklimit != "") && $ceklimit <= $traceip && $tgl == $tglnya) {
                    $data['error_limit'] = $this->language->get('error_limit');
                } else {
                    if ($tgl != $tglnya) {
                        $traceip = 1;
                    } else {
                        $traceip = $traceip + 1;
                    }

                    $this->load->model('extension/module/msegatsms');
                    $gateway = $this->config->get('gateway');
                    $text = urlencode(substr($this->request->post['message'], 0, 150));
                    $destination = $this->request->post['nohp'];
                    $getresponse = $this->model_extension_module_msegatsms->send_message($destination, $text, $gateway);
                    $status = $getresponse;
                    if ($status == "Success") {
                        $data['success'] = $this->language->get('text_success');
                        $this->db->query("UPDATE " . DB_PREFIX . "setting SET `value` = '" . $traceip . ":" . $tgl . "' WHERE `key` = '" . $ip . "'");
                    } else {
                        $data['error'] = $this->language->get('text_error');
                    }
                }
            }
        }
			
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . 'template/extension/module/msegatsmsmodule')) {
            return $this->load->view($this->config->get('config_template') . 'template/extension/module/msegatsmsmodule', $data);
        } else {
            return $this->load->view('extension/module/msegatsmsmodule', $data);
        }
    } 

    protected function validate()
    {
        if (!$this->request->post['nohp']) {
            $this->error['nohp'] = $this->language->get('error_nohp');
        }
        if (!$this->request->post['message']) {
            $this->error['message'] = $this->language->get('error_message');
        }
        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    public function getValue(){

    }
}

?>