<?php
class ControllerExtensionPaymentStcpay extends Controller
{
    public function index(){
        return $this->load->view('extension/payment/stcpay');
    }
    public function Stcpay(){
        if (isset($this->session->data['order_id'])) {
            $this->load->model('checkout/order');
            $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
            $amount = $this->currency->format($order_info['total'], $this->config->get('config_currency'), '', false);
            if (isset($this->request->post['num'])) {

                if (isset($this->session->data['OtpReference']) && isset($this->session->data['STCPayPmtReference'])) {
                    unset($this->session->data['OtpReference']);
                    unset($this->session->data['STCPayPmtReference']);
                }
                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://b2b.stcpay.com.sa/B2B.DirectPayment.WebApi/DirectPayment/V4/DirectPaymentAuthorize",
                    CURLOPT_SSLCERT => '/home/lanaapps/public_html/admin/ca.pem',
                    CURLOPT_SSLKEY => '/home/lanaapps/public_html/admin/key.pem',
                    CURLOPT_SSLCERTTYPE => "PEM",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => '{
                    "DirectPaymentAuthorizeV4RequestMessage": {
                     "BranchID": "string",
                     "TellerID": "string",
                     "DeviceID": "string",
                     "RefNum": "'.$this->session->data['order_id'].'",
                     "BillNumber": "6777",
                     "MobileNo": "' . $this->request->post['num'] . '",
                     "Amount": ' . $amount . ',
                     "MerchantNote": "#'. $this->session->data['order_id'] .' - '.HTTP_SERVER . '"
                     }
                    }',
                    CURLOPT_HTTPHEADER => array(
                        "X-ClientCode: ".$this->config->get('payment_stcpay_merchant_id'),
                        "Content-Type: application/json"
                    ),
                ));
                $response = curl_exec($curl);
                $err = curl_errno($curl);
                $info = curl_getinfo($curl);
                curl_close($curl);
                $json = json_decode($response, true);
                if (isset($json['DirectPaymentAuthorizeV4ResponseMessage'])) {
                    $this->session->data['OtpReference'] = $json['DirectPaymentAuthorizeV4ResponseMessage']['OtpReference'];
                    $this->session->data['STCPayPmtReference'] = $json['DirectPaymentAuthorizeV4ResponseMessage']['STCPayPmtReference'];
                    echo '1';
                } else {
                    echo '<div class="alert alert-danger">الرقم الذى ادخلتة خطاء </div>';
                }
            } else {
                echo '<div class="alert alert-danger">الرقم الذى ادخلتة خطاء </div>';
            }
        }
    }

    public function confitmStcpay(){
        if(isset($this->request->post['code']) && isset($this->session->data['OtpReference']) && isset($this->session->data['STCPayPmtReference'])){
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://b2b.stcpay.com.sa/B2B.DirectPayment.WebApi/DirectPayment/V4/DirectPaymentConfirm",
                CURLOPT_SSLCERT => "/home/lanaapps/public_html/admin/ca.pem",
                CURLOPT_SSLKEY => "/home/lanaapps/public_html/admin/key.pem",
                CURLOPT_SSLCERTTYPE => "PEM",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS =>'{
                   "DirectPaymentConfirmV4RequestMessage": {
                     "OtpReference": "'.$this->session->data['OtpReference'].'",
                     "OtpValue": "'.(int)$this->request->post['code'].'",
                     "STCPayPmtReference": "'.$this->session->data['STCPayPmtReference'].'",
                     "TokenReference": "string",
                     "TokenizeYn": true
                     }
                    }',
                CURLOPT_HTTPHEADER => array(
                    "X-ClientCode: 74482575955",
                    "Content-Type: application/json"
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_errno($curl);
            $info = curl_getinfo($curl);
            curl_close($curl);
            $json = json_decode($response,true);

            if(isset($json['DirectPaymentConfirmV4ResponseMessage'])){
                if($json['DirectPaymentConfirmV4ResponseMessage']['PaymentStatus'] == '2' && $json['DirectPaymentConfirmV4ResponseMessage']['PaymentStatusDesc'] == 'Paid') {
                    // Add order
                    $this->load->model('checkout/order');
                    $this->model_checkout_order->addOrderHistory($this->session->data['order_id'],$this->config->get('payment_stcpay_order_status_id'));
                    echo '<script>location.href="'.$this->url->link('checkout/success').'";</script>';

                }elseif($json['DirectPaymentConfirmV4ResponseMessage']['PaymentStatus'] == '1' && $json['DirectPaymentConfirmV4ResponseMessage']['PaymentStatusDesc'] == 'Pending'){
                    echo '<div class="alert alert-info">  العملية قيد المراجعة من STCPay </div>';
                }elseif($json['DirectPaymentConfirmV4ResponseMessage']['PaymentStatus'] == '4' && $json['DirectPaymentConfirmV4ResponseMessage']['PaymentStatusDesc'] == 'Cancelled'){
                    echo '<div class="alert alert-danger"> تم رفض العملية يرجى المرجعة مرة اخري</div>';
                }else{
                    echo '<div class="alert alert-danger"> العملية انتهت يرجى اعادة المحاولة مرة اخري </div>';
                }

            }elseif(isset($json['Code'])){
                echo '<div class="alert alert-danger">'.$json['Text'].'</div>';
            }else{
                echo '<div class="alert alert-danger"> كود التاكيد الدفع خطاء </div>';
            }
        }else{
            echo '<div class="alert alert-danger"> كود التاكيد الدفع خطاء </div>';
        }
    }
}