<?php
class ControllerExtensionPaymentTabby extends Controller {
    var $code = 'tabby';

	public function index() {
        $this->load->model('extension/module/tabby');
        $data = $this->model_extension_module_tabby->getCheckoutData();
        
		return $this->load->view('extension/payment/tabby', $data);
	}

	public function confirm() {
		$json = array();

        // use only for current payment method
		if (preg_match('#^' . $this->code . '#', $this->session->data['payment_method']['code'])) {
            $this->load->model('extension/module/tabby');
		
            $res = $this->model_extension_module_tabby->execute("GET", $_GET['payment_id']);

            $this->load->model('checkout/order');
            $order = $this->model_checkout_order->getOrder($this->session->data['order_id']);

            if (empty($res)) {
                $json['error'] = "Transaction not found.";
            } elseif ($res->status !== 'AUTHORIZED') {
                $json['error'] = "Transaction state is not valid.";
            } elseif ($res->amount != $this->formatAmount($order['total']) || $res->currency != $order['currency_code']) {
                $json['error'] = "Transaction amount or currency issue.";
            } else {
                // post order
			    $this->model_checkout_order->addOrderHistory(
                    $this->session->data['order_id'], 
                    $this->config->get('payment_'.$order['payment_code'].'_order_status_id'),
                    sprintf("Authorization transaction #%s. Amount %s %s", $_GET['payment_id'], $res->amount, $res->currency)
                );
                // assign transaction to order
                $this->model_extension_module_tabby->addTransaction([
                    'order_id'          => $this->session->data['order_id'],
                    'transaction_id'    => $_GET['payment_id'],
                    'body'              => json_encode($res)
                ]);

			    $json['redirect'] = $this->url->link('checkout/success');

            }
            
            $json['success'] = !array_key_exists('error', $json);
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));		
	}
    public function formatAmount($amount) {
        return number_format($amount, 2, '.', '');
    }
}
