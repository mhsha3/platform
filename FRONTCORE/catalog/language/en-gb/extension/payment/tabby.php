<?php
// Text
$_['text_title']						= 'Tabby';
$_['text_tabby_checkout']				= 'Tabby Checkout';
$_['text_tabby_installments']           = 'Tabby Installments';
$_['text_wait']							= 'Please wait!';

// Button
$_['button_pay']						= 'Pay with Tabby';
