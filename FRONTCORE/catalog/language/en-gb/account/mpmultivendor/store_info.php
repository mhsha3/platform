<?php
// Heading
$_['heading_title']      = 'Store Information';

// Text
$_['text_account']       = 'Account';
$_['text_store_info']    = 'Store Information';
$_['text_success']       = 'Success: Store information has been successfully updated.';
$_['text_default']       = 'Default';
$_['text_keyword']       = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';

// Message
$_['message_need_approval']       = 'Attention : Your seller account <strong>require approval</strong> from admin. Please wait approval. However you can add your store details. Kindly fill your store details, so admin can easily identify your store during approval process. <strong>Thank you for your patience.</strong>';
$_['message_status_disabled']       = 'Warning : <strong>Your seller account has been disabled.</strong> Please contact with admin regarding your account suspention.';


$_['button_save']    	= 'Save';
$_['button_cancel']    	= 'Cancel';

$_['tab_general'] 		= 'General';
$_['tab_store'] 		= 'Store';
$_['tab_local'] 		= 'Local';
$_['tab_image'] 		= 'Image';
$_['tab_social_profiles'] = 'Social Profiles';
$_['tab_seo'] 			= 'SEO';

// Entry
$_['entry_description'] 			= 'Description';
$_['entry_meta_description'] 		= 'Meta Description';
$_['entry_meta_keyword'] 			= 'Meta Keyword';
$_['entry_store_owner'] 			= 'Seller Name';
$_['entry_store_name'] 				= 'Store Name';
$_['entry_address'] 				= 'Address';
$_['entry_email'] 					= 'Email';
$_['entry_telephone'] 				= 'Telephone';
$_['entry_fax'] 					= 'Fax';
$_['entry_keyword'] 			= 'Seo Keyword for Seller Information Page';
$_['entry_review_seo_keyword'] 		= 'Seo Keyword for Review Page';
$_['entry_city'] 					= 'City';
$_['entry_state'] 					= 'State';
$_['entry_zone'] 					= 'Zone';
$_['entry_country'] 				= 'Country';
$_['entry_logo'] 					= 'Logo';
$_['entry_banner'] 					= 'Banner';
$_['entry_image'] 					= 'Profile Picture';
$_['entry_facebook'] 				= 'Facebook';
$_['entry_google_plus'] 			= 'Google Plus';
$_['entry_twitter'] 				= 'Twitter';
$_['entry_pinterest'] 				= 'Pinterest';
$_['entry_linkedin'] 				= 'Linkedin';
$_['entry_youtube'] 				= 'Youtube';
$_['entry_instagram'] 				= 'Instagram';
$_['entry_flickr'] 					= 'Flickr';
$_['entry_store']            		= 'Stores';

// Help
$_['help_banner'] 					= 'W: %s X H: %s';
$_['help_logo'] 					= 'W: %s X H: %s';
$_['help_image'] 					= 'W: %s X H: %s';

// Error
$_['error_status_disabled'] = 'Warning: Your seller account suspended!';
$_['error_store_owner']     = 'Seller name must be between 3 and 64 characters!';
$_['error_email']          	= 'E-Mail Address does not appear to be valid!';
$_['error_telephone']      	= 'Telephone must be between 3 and 32 characters!';
$_['error_address']        	= 'Address must be between 3 and 128 characters!';
$_['error_city']           	= 'City must be between 2 and 128 characters!';
$_['error_country']        	= 'Please select a country!';
$_['error_zone']           	= 'Please select a region / state!';
$_['error_description']    	= 'Description required!';
$_['error_meta_description'] = 'Meta Description required!';
$_['error_meta_title']     	= 'Meta Title must be between 3 and 32 characters!';
$_['error_store_name']   	= 'Store Name must be between 3 and 255 characters!';
$_['error_meta_keyword']   	= 'Meta Keyword required!';
$_['error_keyword']          = 'SEO URL already in use!';
$_['error_seo_keyword']		= 'SEO URL already in use!';
$_['error_review_seo_keyword'] = 'SEO URL already in use!';
$_['error_warning']         = 'Warning: Please check the form carefully for errors!';