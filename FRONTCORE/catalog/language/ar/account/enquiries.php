<?php
// Heading
$_['heading_title']     = 'استفسارات البائع الخاصة بي';

// Button
$_['button_view_store']	= 'عرض المتجر';
$_['button_view']	    = 'عرض الاستفسار';

// Text
$_['text_account']      = 'الحساب';


// Column
$_['entry_store']		= 'متجر البائع: ';
$_['entry_name']		= 'الاسم';
$_['entry_email']		= 'الايميل';
$_['entry_message']		= 'الرسالة';
$_['entry_date_added']	= 'انشاء: ';
$_['entry_date_modified'] = 'النشاط الاخير: ';
$_['entry_action']		= 'خيارات';