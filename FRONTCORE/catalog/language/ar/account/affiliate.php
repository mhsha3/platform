<?php
// Heading
$_['heading_title']             = 'معلومات الإحالة الخاصة بك';

// Text
$_['text_account']              = 'الحساب';
$_['text_affiliate']            = 'شركة تابعة';
$_['text_my_affiliate']         = 'حسابي التابع';
$_['text_payment']              = 'معلومات الدفع';
$_['text_cheque']               = 'التحقق من';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'حوالة بنكية';
$_['text_success']              = 'نجاح: لقد تم تحديث حسابك بنجاح.';
$_['text_agree']                = 'لقد قرات ووافقت على ال <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_company']             = 'الشركة';
$_['entry_website']             = 'موقع الكتروني';
$_['entry_tax']                 = 'الرقم الضريبي';
$_['entry_payment']             = 'طريقة الدفع او السداد';
$_['entry_cheque']              = 'تحقق من اسم المدفوع لأمره';
$_['entry_paypal']              = 'حساب البريد الإلكتروني على PayPal';
$_['entry_bank_name']           = 'اسم البنك';
$_['entry_bank_branch_number']  = 'رقم ABA / BSB (رقم الفرع)';
$_['entry_bank_swift_code']     = 'رمز السرعة';
$_['entry_bank_account_name']   = 'أسم الحساب';
$_['entry_bank_account_number'] = 'رقم حساب';

// Error
$_['error_agree']               = 'تحذير: يجب أن توافق على %s!';
$_['error_cheque']              = 'التحقق من اسم المدفوع لأمره مطلوب!';
$_['error_paypal']              = 'يبدو أن عنوان البريد الإلكتروني لـ PayPal غير صالح!';
$_['error_bank_account_name']   = 'اسم الحساب مطلوب!';
$_['error_bank_account_number'] = 'رقم الحساب مطلوب!';
$_['error_custom_field']        = '%s مطلوب!';