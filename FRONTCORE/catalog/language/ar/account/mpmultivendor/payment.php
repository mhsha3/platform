<?php
// Heading
$_['heading_title']     	= 'الدفع';

// Text
$_['text_account']      	= 'الحساب';
$_['text_payment']   		= 'الدفع';
$_['text_final_payment']   	= 'إجمالي المدفوع';

// Column
$_['column_amount']			= 'القيمة';
$_['column_date_added']		= 'تاريخ الإضافة';

// Entry
$_['entry_date_start']		= 'تاريخ البداية';
$_['entry_date_end']		= 'تاريخ النهاية';