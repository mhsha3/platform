<?php
// Heading
$_['heading_title']     = 'تصدير المنتجات';
$_['export_title']		= 'المنتجات (%s)';

// Text
$_['text_account']      = 'الحساب';
$_['text_product']      = 'المنتجات';
$_['text_enabled']      = 'تفعيل';
$_['text_disabled']     = 'تعطيل';
$_['text_default']      = 'إفتراضي';
$_['text_all_store']				= 'جميع المتاجر';
$_['text_all_stock_status']	 		= 'حالة كل المخزون';
$_['text_all_status']	 			= 'حالة الكل';
$_['text_xls']	 					= 'XLS';
$_['text_xlsx']	 					= 'XLSX';
$_['text_csv']	 					= 'CSV';

// Entry
$_['entry_store']				= 'المتجر';
$_['entry_language']			= 'اللغة';
$_['entry_qty']					= 'الكمية';
$_['entry_price']				= 'السعر';
$_['entry_product_limit']		= 'حد المنتج';
$_['entry_model']				= 'الموديل';
$_['entry_status']				= 'الحالة';
$_['entry_stock_status']		= 'حد المخزون';
$_['entry_format']				= 'الشكل';
$_['entry_product']				= 'المنتج';

$_['entry_help_product']		= '';
$_['entry_manufacturer']		= '';
$_['entry_category']			= '';

// Place holder 
$_['placeholder_quantity_start']	= 'من';
$_['placeholder_quantity_limit']	= 'الى';
$_['placeholder_price_start']		= 'من';
$_['placeholder_price_limit']		= 'الى';
$_['placeholder_product_start']		= 'البداية';
$_['placeholder_product_limit']		= 'الحد';

// Help
$_['help_product']				= 'المنتج';
$_['help_qty']          	 	= 'مجموعة من الكمية. إذا لم يتم إعطاء الحد الأدنى للكمية ولكن تم إعطاء الحد الأقصى للكمية من 0 إلى الحد الأقصى المعطى. إذا لم يتم تحديد الحد الأقصى والحد الأدنى ، فسيتم تجاهل الكمية.';
$_['help_price']          	 	= 'مجموعة من السعر. إذا لم يتم تحديد الحد الأدنى للسعر ولكن تم تحديد الحد الأقصى لسعر التصدير من 0 إلى الحد الأقصى المحدد. إذا لم يتم تحديد الحد الأقصى والحد الأدنى ، فسيتم تجاهل السعر.';
$_['help_product_limit']     	= 'مجموعة من حدود المنتج. إذا لم يتم تحديد الحد الأدنى للمنتج ولكن تم إعطاء الحد الأقصى للحد الأقصى المسموح به للتصدير من 0 إلى الحد الأقصى المسموح به. إذا لم يتم تحديد كل من الحد الأقصى والحد الأدنى ، فسيتم تجاهل حد المنتج.';

// Button
$_['button_filter'] 				= 'فلتر';
$_['button_export'] 				= 'انقر هنا لتصدير المنتجات';
$_['button_back'] 					= 'رجوع';


// Export
$_['export_product_id']				= 'معرف المنتج';
$_['export_language']				= 'رمز اللغة';
$_['export_store']					= 'المتجر (المتجر 1 :: المتجر 2)';
$_['export_product_name']			= 'اسم المنتج';
$_['export_model']					= 'رقم الموديل';
$_['export_description']			= 'الوصف';
$_['export_meta_title']				= 'العنوان ';
$_['export_meta_description'] 		= 'الوصف';
$_['export_meta_keyword']			= 'الكلمة الرئيسية';
$_['export_tag']					= 'Tag';
$_['export_sku']					= 'SKU';
$_['export_upc']					= 'UPC';
$_['export_ean']					= 'EAN';
$_['export_jan']					= 'JAN';
$_['export_isbn']					= 'ISBN';
$_['export_mpn']					= 'MPN';
$_['export_location']				= 'الموقع';
$_['export_price']					= 'السعر';
$_['export_quantity']				= 'الكمية';
$_['export_min_quantity']			= 'أقل كمية';
$_['export_subtract']				= "طرح \n(نعم=1/لا=0)";
$_['export_stock_status_id']		= 'معرف حالة المخزون';
$_['export_stock_status']			= 'حالة المخزون';
$_['export_shipping']				= "الشحن مطلوب \n(نعم=1للاا0)";
$_['export_date_avaiable']  		= 'تاريخ التوفر (YYYY-MM-DD)';
$_['export_status']					= "الحالة \n(تفعيل=1/تعطيل=0)";


// Error
$_['error_warning']     = 'تحذير: الرجاء مراجعة الخطاء في النموذج!';
$_['error_permission']  = 'تحذير: ليس لديك صلاحية لتغيير بيانات المنتج!';