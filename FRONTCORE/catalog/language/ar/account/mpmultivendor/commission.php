<?php
// Heading
$_['heading_title']     	= 'اتعمولة المنتجات';

// Text
$_['text_account']      	= 'حسابي';
$_['text_commission']   	= 'عمولة المنتج';
$_['text_final_commission']   	= 'مجموع العمولة';

// Column
$_['column_order_id']		= 'رقم الطلب';
$_['column_product_name']	= 'اسم المنتج';
$_['column_price']			= 'السعر';
$_['column_quantity']		= 'العدد';
$_['column_total']			= 'المجموع';
$_['column_amount']			= 'الكمية';
$_['column_date_added']		= 'اضافة البيانات';

// Entry
$_['entry_date_start']		= 'بداية الوقت';
$_['entry_date_end']		= 'نهاية الوقت';