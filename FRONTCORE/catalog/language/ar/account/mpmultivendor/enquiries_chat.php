<?php
// Heading
$_['heading_title']     		= 'استفسارات العملاء';

$_['heading_title_chat']     	= 'رسائل الاستفسارات';

// Text
$_['text_account']      		= 'الحساب';
$_['text_enquiry']            	= 'معلومات الاستفسار';
$_['text_enquiryby']      		= 'استفسار ب %s';
$_['text_contact_details']      = 'تفاصيل التواصل';
$_['text_enquiry_details']      = 'استفسار:';
$_['text_name']					= 'الاسم';
$_['text_email']				= 'الايميل';
$_['text_date_added']			= 'صنع';
$_['text_date_modified']		= 'اخر تفاعل';
$_['text_message']				= 'رسالة';
$_['text_seller_type']			= 'التاجر';
$_['text_customer_type']		= 'العميل';
$_['text_error']            	= 'تعذر العثور على الاستفسار!';

// Entry
$_['entry_message']				= 'اضافة الى المحادثة';

// Error
$_['error_message'] 	   		= 'تحذير ! يجب ألا يقل نص الرسالة عن حرفين';