<?php
// Heading
$_['heading_title']      			= 'التنزيلات';

// Text
$_['text_account']       			= 'الحساب';
$_['text_account']       			= 'الحساب';
$_['text_store_info']    			= 'التنزيلات';
$_['text_success']      			= 'تم بنجاح:    s!';
$_['text_add']       				= 'اضافة';
$_['text_edit']       				= 'تعديل ';
$_['text_upload']       			= 'تم رفع الملف بنجاح!';
$_['button_add']    				= 'اضافة';
$_['button_save']    				= 'حفظ';
$_['button_cancel']    				= 'الغاء';

// Entry
$_['entry_name']        			= 'اسم التنزيل';
$_['entry_filename']    			= 'اسم الملف';
$_['entry_mask']        			= 'القناع';

// Column
$_['column_name']       			= 'اسم التنزيل';
$_['column_date_added'] 			= 'تاريخ الاضافة';
$_['column_action']     			= 'فعل';

// Help
$_['help_filename']     = 'يمكنك التحميل عبر زر التحميل .';
$_['help_mask']         = 'من المستحسن أن يكون اسم الملف والقناع مختلفين لمنع الأشخاص من محاولة الارتباط مباشرة بتنزيلاتك.';

// Error
$_['error_warning']          		= 'تحذير: الرجاء التحقق من الخطاء في النموذج!';
$_['error_name']        			= 'يجب ان يكون الاسم من 3 حتى 64 حرف !';
$_['error_upload']      			= 'يتطلب رفع!';
$_['error_filename']    			= 'اسم الملف يجب ان يكون بين 3 و 128 حرف!';
$_['error_exists']      			= 'الملف لم يظهر!';
$_['error_mask']        			= 'القناع يجب ان يكون بين 3 و 128 حرف!';
$_['error_filetype']    			= 'نوع الملف غير صحيح!';
$_['error_product']     			= 'تحذير: لا يمكن حذف هذا الملف بسبب ارتباطة بمنتج!';