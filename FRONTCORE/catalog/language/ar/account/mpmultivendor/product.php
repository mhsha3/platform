<?php
// Heading
$_['heading_title']     = 'المنتجات';

// Text
$_['text_account']      = 'الحساب';
$_['text_product']      = 'المنتجات';
$_['text_enabled']      = 'تفعيل';
$_['text_disabled']     = 'تعطيل';
$_['text_default']      = 'إفتراضي';
$_['text_choose']        = 'اختر';
$_['text_select']        = 'تحديد';
$_['text_radio']         = 'الراديو';
$_['text_checkbox']      = 'خانة الإختيار';
$_['text_input']         = 'ادخل';
$_['text_text']          = 'نص';
$_['text_textarea']      = 'منطقة النص';
$_['text_file']          = 'ملف';
$_['text_date']          = 'التاريخ';
$_['text_datetime']      = 'التاريخ &amp; الوقت';
$_['text_time']          = 'الوقت';
$_['text_confirm']       = 'هل انت متاكد?';
$_['text_success']       = 'تم بنجاح: لقد قمت بتحديث المنتجات!';
$_['text_keyword']       = 'لا تستخدم المسافة ، واستخدم بدلا منها علامة - وتأكد من أن SEO URL لم يتم استخدامه من قبل';

// Button
$_['button_attribute_add']	= 'اضافة سمة';
$_['button_discount_add']	= 'إضافة خصم';
$_['button_special_add']	= 'اضافة مميز';
$_['button_image_add']		= 'اضافة صورة';
$_['button_option_value_add']= 'إضافة قيمة للخيار';
$_['button_save']= 'Save';
$_['button_import_products']	= 'استيراد المنتجات';
$_['button_export_products']	= 'تصدير المنتجات';

// Tab
$_['tab_general']		= 'عام';
$_['tab_data']			= 'البيانات';
$_['tab_links']			= 'الرابط';
$_['tab_attribute']		= 'السمة';
$_['tab_option']		= 'الخيارات';
$_['tab_discount']		= 'الخصم';
$_['tab_special']		= 'مميز';
$_['tab_image']			= 'الصورة';
$_['tab_reward']		= 'نقاط المكافأة';
$_['tab_seo']			= 'SEO';

// Column
$_['column_image']		= 'الصورة';
$_['column_name']		= 'اسم المنتج';
$_['column_model']		= 'الموديل';
$_['column_price']		= 'السعر';
$_['column_quantity']	= 'الكمية';
$_['column_status']		= 'الحالة';
$_['column_action']		= 'عمل';

// Entry
$_['entry_name'] 		= 'اسم المنتج';
$_['entry_model'] 		= 'المنتج';
$_['entry_price'] 		= 'السعر';
$_['entry_quantity'] 	= 'الكمية';
$_['entry_status'] 		= 'الحالة';
$_['entry_image'] 		= 'الصورة';
$_['entry_description']      = 'الوصف';
$_['entry_meta_description'] = 'وصف العلامة';
$_['entry_meta_title'] 	     = 'عنوان العلامة';
$_['entry_meta_keyword'] 	 = 'الكلمات الأساسية للعلامة';
$_['entry_tag']          	 = 'علامات المنتج';
$_['entry_sku']              = 'SKU';
$_['entry_upc']              = 'UPC';
$_['entry_ean']              = 'EAN';
$_['entry_jan']              = 'JAN';
$_['entry_isbn']             = 'ISBN';
$_['entry_mpn']              = 'MPN';
$_['entry_location']         = 'الموقع';
$_['entry_tax_class']        = 'نوع الضريبة';
$_['entry_minimum']          = 'اقل كمية لطلب';
$_['entry_subtract']         = 'الطرح من المخزون';
$_['entry_stock_status']     = 'حالة نفاذ المخزون';
$_['entry_price']            = 'السعر';
$_['entry_tax_class']        = 'نوع الضريبة';
$_['entry_points']           = 'النقاط';
$_['entry_option_points']    = 'النقاط';
$_['entry_subtract']         = 'الطرح من المخزون';
$_['entry_weight_class']     = 'فئة الوزن';
$_['entry_weight']           = 'الوزن';
$_['entry_dimension']        = 'الابعاد (الطول x العرض x الارتفاع)';
$_['entry_length_class']     = 'فئة الطول';
$_['entry_length']           = 'الطول';
$_['entry_width']            = 'العرض';
$_['entry_height']           = 'الارتفاع';
$_['entry_image']            = 'الصورة';
$_['entry_additional_image'] = 'صور اضافية';
$_['entry_customer_group']   = 'مجموعة العملاء';
$_['entry_date_start']       = 'تاريخ البداية';
$_['entry_date_end']         = 'تاريخ النهاية';
$_['entry_priority']         = 'الافضلية';
$_['entry_attribute']        = 'السمة';
$_['entry_attribute_group']  = 'مجموعة السمات';
$_['entry_text']             = 'نص';
$_['entry_option']           = 'خيار';
$_['entry_option_value']     = 'قيمة الخيار';
$_['entry_required']         = 'مطلوب';
$_['entry_status']           = 'الحالات';
$_['entry_sort_order']       = ' الترتيب';
$_['entry_category']         = 'التصنيفات';
$_['entry_filter']           = 'الفلاتر';
$_['entry_download']         = 'التنزيلات';
$_['entry_related']          = 'منتجات ذات صلة';
$_['entry_tag']          	 = 'علامات المنتج';
$_['entry_reward']           = 'نقاط المكافات';
$_['entry_date_available']   = 'تاريخ التوفر';
$_['entry_shipping']         = 'طلب الشحن';
$_['entry_keyword']          = 'SEO URL';
$_['entry_manufacturer']     = 'الصانع';
$_['entry_store']            = 'المتاجر';

// Help
$_['help_keyword']           = 'لا تستخدم المسافة ، واستخدم بدلا منها علامة - وتأكد من أن SEO URL لم يتم استخدامه من قبل';
$_['help_sku']               = 'وحدة حفظ المخزون SKU';
$_['help_upc']               = 'رمز المنتج العالمي UPC';
$_['help_ean']               = 'رقم المادة الأوروبية EAN';
$_['help_jan']               = 'رقم المقالة اليابانية JAN';
$_['help_isbn']              = 'الرقم العالمي الموحد للكتاب ISBN';
$_['help_mpn']               = 'رقم قطع غيار الشركة المصنعة MPN';
$_['help_manufacturer']      = '(الإكمال التلقائي)';
$_['help_minimum']           = 'فرض الحد الأدنى للمبلغ المطلوب';
$_['help_stock_status']      = 'تظهر الحالة عندما يكون المنتج غير متوفر';
$_['help_points']            = 'عدد النقاط اللازمة لشراء هذا العنصر. إذا كنت لا تريد أن يتم شراء هذا المنتج بالنقاط ، فاترك 0.';
$_['help_category']          = '(الإكمال التلقائي)';
$_['help_filter']            = '(الإكمال التلقائي)';
$_['help_download']          = '(الإكمال التلقائي)';
$_['help_related']           = '(الإكمال التلقائي)';
$_['help_tag']               = 'مفصولة بفواصل';

// Action
$_['button_filter'] 	= 'فلتر';
$_['button_add'] 		= 'اضافة جديد';

// Error
$_['error_warning']          = 'تحذير: يرجى التحقق من النموذج بعناية بحثًا عن أخطاء!';
$_['error_permission']       = 'تحذير: ليس لديك الصلاحية للتعديل على المنتج!';
$_['error_name']             = 'اسم المنتج يجب أن يكون بين ٤ حتى ٢٥٥ حرف';
$_['error_meta_title']       = 'عنوان الميتا يجب أن يكون بين ٤ حتى ٢٥٥ حرف';
$_['error_model']            = 'نوع المنتج يجب أن يكون بين ٢ حتى ٦٤ حرف';
$_['error_keyword']          = 'SEO URL تم استخدامه من قبل';
