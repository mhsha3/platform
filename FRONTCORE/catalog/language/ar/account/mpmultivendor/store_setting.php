<?php
// Heading
$_['heading_title']      = 'الاعداداات';

// Text
$_['text_account']       = 'الحساب';
$_['text_store_info']    = 'الاعدادات';
$_['text_success']       = 'نجاح: تم تحديث الإعداد الخاص بك بنجاح.';
$_['text_order_wise']    = 'Order Wise';
$_['text_product_wise']  = 'Product Wise';
$_['text_paypal']        = 'PayPal';
$_['text_bank_transfer'] = 'تحويل بنكى';
$_['text_cheque']        = 'شيك';

// Button
$_['button_save']    	= 'حفظ';
$_['button_cancel']    	= 'اغلاق';

// Tab
$_['tab_shipping'] 		= 'الشحن';
$_['tab_payment'] 		= 'الدفع';

// Entry
$_['entry_paypal_email'] 			= 'Paypal Email';
$_['entry_bank_details'] 			= 'تفاصيل البنك';
$_['entry_cheque_payee'] 			= 'تحقق من اسم المدفوع لأمره';
$_['entry_payment_type'] 			= 'نوع الدفع';
$_['entry_shipping_type'] 			= 'نوع الشحن';
$_['entry_shipping_amount'] 		= 'مبلغ الشحن';

// Error
$_['error_paypal_email']			= 'يبدو أن عنوان البريد الإلكتروني غير صالح!';
$_['error_bank_details']			= 'التفاصيل المصرفية مطلوبة!';
$_['error_cheque_payee_name']		= 'يجب أن يكون اسم المدفوع لأمره بين 2 و 255 حرفًا!';
$_['error_payment_type']			= 'نوع الدفع مطلوب!';
$_['error_shipping_amount']			= 'مطلوب مبلغ الشحن!';
$_['error_warning']         		= 'تحذير: يرجى التحقق من النموذج بعناية لمعرفة الأخطاء!';