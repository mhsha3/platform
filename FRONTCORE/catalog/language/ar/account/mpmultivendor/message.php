<?php
// Heading
$_['heading_title']     	= 'رسائل المسؤول';

// Text
$_['text_account']      	= 'الحساب';
$_['text_administrator']    = 'المدير';
$_['text_success_sent']     = 'تم إرسال الرسالة بنجاح.';

// Button
$_['button_submit']			= 'إرسال رسالة';

// Column
$_['entry_message']			= 'إضافة إلى المحادثات';

// Error
$_['error_message'] 	   		= 'الرسالة يجب أن تكون حرفين أو أكثر!';