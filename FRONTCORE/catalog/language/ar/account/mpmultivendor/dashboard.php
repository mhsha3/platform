<?php
// Heading
$_['heading_title']     		= 'لوحة تحكم البائع';

// Text
$_['text_account']      		= 'الحساب';
$_['text_dashboard']    		= 'حساب البائع';
$_['text_sales']    			= 'المبيعات';
$_['text_orders']    			= 'الطلبات';
$_['text_commission']    		= 'إجمالي العمولة';
$_['text_recevied_payment']    	= 'تم استلام المبلغ';
$_['text_available_balance'] 	= 'الرصيد المتوفر';
$_['text_reviews']    			= 'التعليقات';
$_['text_products']    			= 'إجمالي المنتجات  ';
$_['text_product']    			= 'المنتجات';
$_['text_viewmore']    			= 'عرض المزيد';
$_['text_all']    				= 'الكل';
$_['text_total']    				= 'المجموع';

// Button
$_['button_new_product']    	= ' (اضافة منتج  )';            
$_['button_viewmore']    		= '  مشاهدة الكل  ';