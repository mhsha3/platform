<?php
// Heading
$_['heading_title']     = 'المراجعات';

// Text
$_['text_account']      = 'الحساب';
$_['text_product']      = 'المراجعات';
$_['text_enabled']      = 'تمكين';
$_['text_disabled']     = 'تعطيل';
$_['text_star']     	= 'نجمة';
$_['text_all']     		= 'الكل';

// Column
$_['column_date_added']	= 'أضافة تاريخ';
$_['column_author']		= 'مؤلف';
$_['column_title']		= 'عنوان';
$_['column_description']= 'وصف';
$_['column_status']		= 'الحالة';
$_['column_rating']		= 'تقييم';

// Entry
$_['entry_author']		= 'مؤلف';
$_['entry_status']		= 'الحالة';
$_['entry_rating']		= 'تقييم';
$_['entry_date_added']	= 'أضافة تاريخ';