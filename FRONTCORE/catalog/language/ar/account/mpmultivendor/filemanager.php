<?php
// Heading
$_['heading_title']    = 'مدير الصور';

// Text
$_['text_uploaded']    = 'بنجاح: تم رفع المنتج!';
$_['text_directory']   = 'بنجاح: انشاء الدليل!';
$_['text_delete']      = 'بنجاح: تم حذف الدليل!';

// Button
$_['button_parent']		= 'طباعة';
$_['button_refresh']	= 'تحديث';
$_['button_folder']		= 'مجلد جديد';

// Entry
$_['entry_search']     = 'بحث..';
$_['entry_folder']     = 'اسم المجلد';

// Error
$_['error_permission'] = 'تحذير: الطلب مرفوض!';
$_['error_filename']   = 'تحذير: اسم الملف يجب ان يكون بين 3 حتى 255 حرف !';
$_['error_folder']     = 'تحذير: اسم المجلد يجب ان يكون بين 3 حتى 255 حرف!';
$_['error_exists']     = 'تحذير: الملف موجود مسبقا او يوجد ملف بنفس الاسم!';
$_['error_directory']  = 'تحذير: الدليل غير موجود!';
$_['error_filesize']   = 'تحذير: حجم الملف غير صحيح!';
$_['error_filetype']   = 'تحذير: نوع الملف غير صحيح!';
$_['error_upload']     = 'تحذير: لايمكن رفع الملف بسبب غير معروف!';
$_['error_delete']     = 'تحذير: لاتستطيع حذف الدليل!';
