<?php
// Heading
$_['heading_title']      = 'بيانات المتجر';

// Text
$_['text_account']       = 'الحساب';
$_['text_store_info']    = 'بيانات المتجر';
$_['text_success']       = 'نجاح: تم تحديث معلومات المتجر بنجاح.';
$_['text_default']       = 'الافتراضى';
$_['text_keyword']       = 'لا تستخدم مسافات ، وبدلاً من ذلك استبدل المسافات بـ - وتأكد من أن عنوان URL لـ SEO فريد بشكل عام.';

// Message
$_['message_need_approval']       = 'انتباه: حساب البائع الخاص بك <strong>تتطلب الموافقة</strong> من الادارة. الرجاء انتظار الموافقة. ومع ذلك يمكنك إضافة تفاصيل متجرك. يرجى ملء تفاصيل متجرك ، حتى يتمكن المسؤول من التعرف على متجرك بسهولة أثناء عملية الموافقة.<strong>شكرا لك على صبرك.</strong>';
$_['message_status_disabled']       = 'تحذير :<strong>تم تعطيل حساب البائع الخاص بك.</strong> يرجى التواصل مع المسؤول بخصوص تعليق حسابك.';


$_['button_save']    	= 'حفظ';
$_['button_cancel']    	= 'اغلاق';

$_['tab_general'] 		= 'عام';
$_['tab_store'] 		= 'متجر';
$_['tab_local'] 		= 'محلى';
$_['tab_image'] 		= 'صورة';
$_['tab_social_profiles'] = 'لمحات اجتماعية';
$_['tab_seo'] 			= 'SEO';

// Entry
$_['entry_description'] 			= 'الوصف';
$_['entry_meta_description'] 		= 'ميتا الوصف';
$_['entry_meta_keyword'] 			= 'كلمات دلالية';
$_['entry_store_owner'] 			= 'اسم البائع';
$_['entry_store_name'] 				= 'اسم المتجر';
$_['entry_address'] 				= 'العنوان';
$_['entry_email'] 					= 'الايميل';
$_['entry_telephone'] 				= 'الهاتف';
$_['entry_fax'] 					= 'فاكس';
$_['entry_keyword'] 			= 'الكلمات الرئيسية لصفحة معلومات البائع';
$_['entry_review_seo_keyword'] 		= 'الكلمات الرئيسية سيو لصفحة المراجعة';
$_['entry_city'] 					= 'المدينة';
$_['entry_state'] 					= 'State';
$_['entry_zone'] 					= 'مجال';
$_['entry_country'] 				= 'البلد';
$_['entry_logo'] 					= 'الشعار';
$_['entry_banner'] 					= 'لافتة';
$_['entry_image'] 					= 'الصوره الشخصيه';
$_['entry_facebook'] 				= 'Facebook';
$_['entry_google_plus'] 			= 'Google Plus';
$_['entry_twitter'] 				= 'Twitter';
$_['entry_pinterest'] 				= 'Pinterest';
$_['entry_linkedin'] 				= 'Linkedin';
$_['entry_youtube'] 				= 'Youtube';
$_['entry_instagram'] 				= 'Instagram';
$_['entry_flickr'] 					= 'Flickr';
$_['entry_store']            		= 'Stores';

// Help
$_['help_banner'] 					= 'W: %s X H: %s';
$_['help_logo'] 					= 'W: %s X H: %s';
$_['help_image'] 					= 'W: %s X H: %s';

// Error
$_['error_status_disabled'] = 'تحذير: حساب البائع الخاص بك معلق!';
$_['error_store_owner']     = 'يجب أن يتراوح اسم البائع بين 3 و 64 حرفًا!';
$_['error_email']          	= 'يبدو أن عنوان البريد الإلكتروني غير صالح!';
$_['error_telephone']      	= 'يجب أن يكون الهاتف بين 3 و 32 حرفًا!';
$_['error_address']        	= 'يجب أن يكون العنوان بين 3 و 128 حرفًا!';
$_['error_city']           	= 'يجب أن تكون المدينة بين 2 و 128 حرفًا!';
$_['error_country']        	= 'رجاء قم بإختيار دوله!';
$_['error_zone']           	= 'الرجاء تحديد منطقة / ولاية!';
$_['error_description']    	= 'الوصف مطلوب!';
$_['error_meta_description'] = 'الوصف التعريفي مطلوب!';
$_['error_meta_title']     	= 'يجب أن يكون عنوان Meta بين 3 و 32 حرفًا!';
$_['error_store_name']   	= 'يجب أن يتراوح اسم المتجر بين 3 و 255 حرفًا!';
$_['error_meta_keyword']   	= 'الكلمات الرئيسية الوصفية مطلوبة!';
$_['error_keyword']          = 'عنوان URL للسيو قيد الاستخدام بالفعل!';
$_['error_seo_keyword']		= 'عنوان URL للسيو قيد الاستخدام بالفعل!';
$_['error_review_seo_keyword'] = 'عنوان URL للسيو قيد الاستخدام بالفعل!';
$_['error_warning']         = 'تحذير: يرجى التحقق من النموذج بعناية لمعرفة الأخطاء!';
