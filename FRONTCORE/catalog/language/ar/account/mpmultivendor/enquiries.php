<?php
// Heading
$_['heading_title']     = 'استفسارات العملاء';

// Text
$_['text_account']      = 'الحساب';
$_['text_product']      = 'استفسارات العملاء';

// Column
$_['entry_name']			= 'الاسم: ';
$_['entry_email']			= 'الايميل: ';
$_['entry_message']			= 'الرسالة';
$_['entry_date_added']		= 'صنع: ';
$_['entry_date_modified'] 	= 'اخر تفاعل: ';
$_['entry_action']			= 'اجراء';