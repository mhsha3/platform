<?php
// Text
$_['text_seller_menus']   = 'قائمة البائع';
$_['text_enabled_seller']   = 'كن بائعاً!';
$_['text_dashboard'] 		= 'لوحة تحكم البائع';
$_['text_profile'] 			= 'الملف الشخصي';
$_['text_product'] 			= 'المنتجات ';
$_['text_orders'] 			= 'الطلبات';
$_['text_reviews'] 			= 'التقييم';
$_['text_commission'] 		= 'عمولات المنتج';
$_['text_payment'] 			= 'الدفع';
$_['text_information_section'] = 'صفحة المعلومات';
$_['text_store_information']= 'معلومات المتجر';
$_['text_store_setting']	= 'الضبط';
$_['text_visit_store'] 		= 'زيارة المتجر';
$_['text_enquiries'] 		= 'الاستفسارات';