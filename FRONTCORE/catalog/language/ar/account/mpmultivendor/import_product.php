<?php
// Heading
$_['heading_title']     				 = 'استيراد المنتجات';

// Text
$_['text_account']      				 = 'الحساب';
$_['text_product']      				 = 'المنتجات';
$_['text_form']          		 		 = 'استيراد المنتجات';
$_['text_product_id'] 	 		 		 = 'ID';
$_['text_model'] 						 = 'الموديل';
$_['text_default'] 					 	 = 'افتراضي';
$_['text_no_result'] 				 	 = 'لم يتم العثور على نتيجة!';
$_['text_success'] 				 		 = '<b>تم استيراد ملفك بنجاح.</b>';
$_['text_success_zero']   	 		 	 = '<b>لم يتم تحديث او ادراج اي منتج.</b>';
$_['text_success_insert']				 = '<br/>&nbsp; &nbsp; &nbsp;<b>(%s) المنتجات المدرجة.</b>';
$_['text_success_update']		 		 = '<br/>&nbsp; &nbsp; &nbsp;<b>(%s) المنتجات المحدثة.</b>';
$_['entry_great'] 				 	 	 = '<i class="fa fa-check-circle"></i> رائع ,تم تحديد ملفك .';
$_['text_xls'] 						 	 = 'XLS';
$_['text_xlsx'] 						 = 'XLSX';
$_['text_csv'] 						 	 = 'CSV';
$_['text_product_name']			 		 = 'الاسم';
$_['text_all']			 		 		 = 'تحديد الكل';
$_['text_unall']			 		 	 = 'الغاء تحديد الكل';

// Help 
$_['help_file']							 = 'اسحب وافلت الملف او اضغط في المكان المحدد (XLS, XLSX, CSV) ملف.';
$_['help_store']						 = 'حدد المتجر الذي سيتم استيراد بيانات الاستيراد فيه.';
$_['help_language']					 	 = 'حدد اللغة التي سيتم استيراد بيانات الاستيراد بها.';
$_['help_importon']					 	 = 'عمود فريد للمنتج <br/>(Product ID / Model / Product Name)';
$_['help_existsupdate']			 		 = 'حدد نعم إذا كنت تريد تحديث المنتجات الموجودة. ملاحظة: إذا حددت "لا" وتم العثور على منتج مطابق ، فاحتفظ النظام بالمنتج الحالي.';
$_['help_format']		 				 = 'تنسيق المستند المستورد. عدم التطابق في المستند المحدد والمحمّل غير مقبول.';
$_['help_cell_operation']		 		 = 'ما هي الحقول التي تريد استيرادها؟ سيتم تجاهل الحقول غير المحددة أثناء استيراد البيانات.';

// Button
$_['button_import']				 	 	 = 'استيراد';
$_['button_download']				 	 = 'تنزيل بيانات نموذجية';

// Entry
$_['entry_store'] 					 	 = 'اختر المتجر';
$_['entry_language'] 				 	 = 'اختر اللغة';
$_['entry_importon'] 				 	 = 'عمود فريد للمنتج';
$_['entry_file'] 						 = 'استيراد الملف';
$_['entry_dragfile'] 					 = '<i class="fa fa-cloud-upload"></i> انقر هنا أو اسحب لاستيراد ملف';
$_['entry_format'] 					 	 = 'تنسيق المستند المستورد';
$_['entry_images'] 					 	 = 'استيراد صور إضافية';
$_['entry_review'] 					 	 = 'مراجعات الاستيراد';
$_['entry_custom_fields']		 	 	 = 'استيراد بيانات الحقول المخصصة';
$_['entry_existsupdate']		 	 	 = 'مطابقة تحديث المنتج';
$_['entry_cell_operation']		 	 	 = 'حدد الحقول التي تريد استيرادها';

// Error
$_['error_file']          	 			 = 'تحذير: يرجى تحديد الملف (XLS، XLSX، CSV) فقط!';
$_['error_store']          	 			 = 'الرجاء تحديد المتجر!';
$_['error_language']         			 = 'الرجاء تحديد اللغة!';
$_['error_importon']         			 = 'يرجى تحديد العمود الفريد للمنتج!';
$_['error_format_diff']      			 = 'يرجى تحميل الملف الوحيد (XLS، XLSX، CSV)!';
$_['error_loading_file']      		 	 = 'حدث خطأ ما ، يرجى تحديث الصفحة.';
$_['error_filetype'] 					 = 'نوع الملف غير صالح!';
$_['error_warning']          			 = 'تحذير: يرجى التحقق من النموذج بعناية لمعرفة الأخطاء!';
$_['error_permission']       			 = 'تحذير: ليس لديك إذن بتعديل مجموعة استيراد المنتج!';

// Cell Values
$_['cell_model'] 			= 'رقم الموديل';
$_['cell_sku'] 				= 'SKU';
$_['cell_upc'] 				= 'UPC';
$_['cell_ean'] 				= 'EAN';
$_['cell_jan'] 				= 'JAN';
$_['cell_isbn'] 			= 'ISBN';
$_['cell_mpn'] 				= 'MPN';
$_['cell_location'] 		= 'الموقع';
$_['cell_quantity'] 		= 'الكمية';
$_['cell_minimum_quantity'] = 'اقل كمية';
$_['cell_date_available'] 	= 'تاريخ التوفر';
$_['cell_shipping_required']= 'الشحن مطلوب';
$_['cell_price'] 			= 'السعر';
$_['cell_product_name'] 	= 'اسم المنتج';
$_['cell_description'] 		= 'الوصف';
$_['cell_meta_tag'] 		= 'علامة ';
$_['cell_meta_title'] 		= 'عنوان ';
$_['cell_meta_description'] = 'وصف ';
$_['cell_meta_keyword'] 	= 'كلمة متغيرة';
$_['cell_store'] 			= 'المتجر';