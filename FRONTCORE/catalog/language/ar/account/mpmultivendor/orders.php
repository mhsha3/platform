<?php
// Heading
$_['heading_title']         = 'الطلبات';

// Text
$_['text_account']          = 'الحساب';
$_['text_orders']           = 'الطلبات';
$_['text_order']            = 'معلومات الطلب';
$_['text_order_detail']     = 'تفاصيل الطلب';
$_['text_customer_detail']  = 'تفاصيل العميل';
$_['text_customer_name']    = 'أسم العميل:';
$_['text_email']       		= 'الايميل:';
$_['text_telephone']        = 'تلفون:';
$_['text_invoice_no']       = 'رقم الفاتوره.:';
$_['text_order_id']         = 'رقم الطلب:';
$_['text_date_added']       = 'إضافه التاريخ:';
$_['text_shipping_address'] = 'عنوان الشحن';
$_['text_shipping_method']  = 'طريقه الشحن:';
$_['text_payment_address']  = 'عنوان الدفع';
$_['text_payment_method']   = 'طريقه الدفع والسداد:';
$_['text_comment']          = 'التعليق على الطلب';
$_['text_history']          = 'تاريخ الطلب';
$_['text_success']          = 'بنجاح:لقد تم تحديث الطلب!';
$_['text_empty']            = 'لم تقم بأي أوامر سابقه!';
$_['text_error']            = 'لايمكن العثور على الطلب الذي طلبته !';
$_['text_history_add']      = 'إضافه تاريخ الطلب';
$_['text_all']      		= 'الكل';
$_['text_invoice']          = 'فاتوره';
$_['text_website']          = 'موقع الكتروني:';
$_['text_shipping']         = 'الشحن';
$_['text_picklist']         = 'مذكره ';
$_['text_contact']          = 'معلومات اتصال';

// Button 
$_['button_history_add']	= 'اضافة تاريخ';
$_['button_invoice_print'] 	= 'فاتوره طباعه';
$_['button_shipping_print'] = 'طباعه قائمه الشحن';
$_['button_cancel'] 		= 'عودة';

// Entry
$_['entry_override']      	= 'تجاوز';
$_['entry_notify']      	= 'أخطار';
$_['entry_comment']      	= 'تعليق';
$_['entry_order_id']      	= 'رقم الطلب';
$_['entry_order_status']    = 'حالة طلب العميل';
$_['entry_admin_order_status']= 'حالة طلب المسؤول';
$_['entry_date_added']		= 'تم أضافه التاريخ';

// Help
$_['help_override']			= 'إذا تم حظر طلب العميل من تغيير حالة الطلب بسبب  تمكين مكافحة الاحتيال.';
// Column
$_['column_order_id']       = 'رقم الطلب';
$_['column_customer']       = 'العميل';
$_['column_name']           = 'أسم المنتج';
$_['column_model']          = 'نموذج';
$_['column_quantity']       = 'كمية';
$_['column_price']          = 'السعر';
$_['column_total']          = 'مجموع';
$_['column_action']         = 'فعل';
$_['column_date_added']     = 'أضافة تاريخ';
$_['column_status']         = 'حالة طلب البائع';
$_['column_by_admin_status']= 'حالة طلب المسؤول';
$_['column_comment']        = 'تعليق';
$_['column_action']        	= 'فعل';
$_['column_notify']        	= 'أخطار العميل';
$_['column_weight']         = 'وزن المنتج';
$_['column_location']       = 'موقعك';
$_['column_reference']      = 'مرجع';

// Error
$_['error_reorder']         = 'الطلب غير موجود!';