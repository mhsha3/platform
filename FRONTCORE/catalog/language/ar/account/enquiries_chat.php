<?php
// Heading
$_['heading_title']     		= 'استفسارات البائع الخاصة بي';

$_['heading_title_chat']     	= 'استفسارات الدردشة';

// Text
$_['text_account']      		= 'الحساب';
$_['text_enquiry']            	= 'معلومات الاستفسار';
$_['text_enquiryby']      		= 'تعيين الاستفسار إلى %s';
$_['text_contact_details']      = 'بيانات المتصل';
$_['text_enquiry_details']      = 'Enquiry:';
$_['text_name']					= 'الاسم: ';
$_['text_email']				= 'الايميل: ';
$_['text_date_added']			= 'تم إنشاؤها على: ';
$_['text_date_modified']		= 'النشاط الاخير: ';
$_['text_message']				= 'رسالة';
$_['text_seller_type']			= 'البائع';
$_['text_customer_type']		= 'العميل';
$_['text_error']            	= 'لا يمكن العثور على الاستفسار!';

// Entry
$_['entry_message']				= 'أضف الرد إلى المحادثة';

// Error
$_['error_message'] 	   		= 'تحذير: يجب ألا يقل نص الرسالة عن حرفين!';