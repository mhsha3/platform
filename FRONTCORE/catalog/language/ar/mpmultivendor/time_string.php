<?php
// Text
$_['text_0second']                 = 'منذ 0 ثانية \'s';
$_['text_ago']                 = 'منذ';
$_['text_year']                 = 'سنوات';
$_['text_month']                 = 'الشهور';
$_['text_day']                 = 'أيام';
$_['text_hour']                 = 'ساعة';
$_['text_minute']                 = 'الدقائق';
$_['text_second']                 = 'ثانية';