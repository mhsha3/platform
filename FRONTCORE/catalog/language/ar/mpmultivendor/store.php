<?php
// Heading
$_['heading_title'] 		= 'الملف التعريفي للبائع';

// Text
$_['text_default']      = 'إفتراضي';
$_['text_name_asc']     = 'الاسم (أ - ي)';
$_['text_name_desc']    = 'الاسم (ي - أ)';
$_['text_price_asc']    = 'السعر (مرتفع؛ منخفض)';
$_['text_price_desc']   = 'السعر (مرتفع؛ منخفض)';
$_['text_rating_asc']   = 'التصنيف (الأدنى)';
$_['text_rating_desc']  = 'التصنيف (الأعلى)';
$_['text_model_asc']    = 'النموذج (أ - ي)';
$_['text_model_desc']   = 'النموذج (Z - A)';
$_['text_tax']          = 'الضرائب:';
$_['text_no_result']    = 'لا يوجد منتج!';

$_['text_mv_seller']    = 'الباعة';
$_['text_all_category'] = 'جميع الفئات';

$_['text_error'] 		= 'المتجر غير موجود!';

// Entry
$_['entry_sort']		= 'فرز';
$_['entry_limit']		= 'حد';
$_['entry_category']	= 'فرز حسب الفئات';