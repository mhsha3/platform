<?php
// Heading
$_['contact_seller_title']	= 'تواصل مع البائع';

// Text

$_['success_send_enquiry']	= 'تم إرسال استفسارك بنجاح.';

$_['text_subject']			= 'رسالة جديدة من';


// Entry
$_['entry_name'] 			= 'اسمك';
$_['entry_email_address'] 	= 'عنوان البريد الالكترونى';
$_['entry_message'] 		= 'رسالة';

// Button
$_['button_send_message'] 	= 'أرسل رسالة';

// Error
$_['error_name'] 			= 'يجب أن يكون الاسم بين 1 و 32 حرفًا!';
$_['error_email']           = 'يبدو أن عنوان البريد الإلكتروني غير صالح!';
$_['error_message'] 	 	= 'يجب أن تكون الرسالة أكثر من 10 أحرف!';
$_['error_seller_notfound']	= 'الصفحة المطلوبة لا يمكن العثور عليه.';