<?php
// Heading
$_['heading_title'] 	= 'مراجعات البائع';

// Text
$_['mv_seller']    		= 'البائع';
$_['title_reviews'] 	= 'المرتجعات (%s)';
$_['text_error'] 		= 'المتجر غير موجود!';
$_['text_no_reviews']   = 'لا توجد مرتجعات لهذا المتجر.';