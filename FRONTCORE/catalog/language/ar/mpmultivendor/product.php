<?php
// Header
$_['text_all_sellers'] 		= 'تصفح البائعين';

// Product
$_['text_soldby'] 			= 'تم بيعها من قبل';
$_['text_seller_rating'] 	= 'تقييم البائع';
$_['text_seller_reviews'] 	= 'مراجعات البائع';
$_['text_seller_enquiries'] = 'استفسارات البائع الخاصة بي';

// Seller Panel
$_['text_seller_panel']		= 'لوحة البائع';
$_['text_seller_dashboard']	= 'لوحة تحكم البائع';
$_['text_enabled_seller']	= 'كن بائعا';

// Customer Orders
$_['column_postby']			= 'تم التحديث بواسطة';
$_['column_seller_info'] 	= 'معلومات البائع';
$_['column_product_name']	= 'منتجات';
$_['column_store_name']		= 'متجر';
$_['column_mpseller_order_status'] = 'الحالة';

// Text
$_['text_customer_seller']	= 'استفسارات البائع';
$_['text_order_admin']		= 'مشرف';
$_['text_all_products']		= 'الكل';