<?php
// Heading
$_['review_title'] 			= 'أكتب مراجعة';
$_['contact_seller_title']	= 'تواصل مع البائع';

// Text
$_['text_reviews']			= 'المراجعات';
$_['success_send_enquiry']	= 'تم إرسال استفسارك بنجاح.';
$_['text_success_review']	= 'شكرا لك، لمراجعتك.';
$_['text_success_review_approval']	= 'شكرا لك، لمراجعتك. تم تقديمه إلى مسؤول الموقع للموافقة عليه.';

// Entry
$_['entry_your_name'] 		= 'اسمك';
$_['entry_title'] 			= 'عنوان';
$_['entry_description'] 	= 'وصف';
$_['entry_rating'] 			= 'تقييم';
$_['entry_name'] 			= 'اسمك';
$_['entry_email_address'] 	= 'عنوان البريد الالكترونى';
$_['entry_message'] 		= 'رسالة';

// Button
$_['button_continue'] 		= 'استمر';
$_['button_write_review'] 	= 'أكتب مراجعة';
$_['button_send_message'] 	= 'أرسل رسالة';

// Error
$_['error_author'] 			= 'يجب أن يكون المؤلف بين 1 و 32 حرفًا!';
$_['error_title'] 			= 'يجب أن يكون العنوان بين 3 و 255 حرفًا!';
$_['error_description'] 	= 'يجب أن يكون الوصف أكثر من 10 أحرف!';
$_['error_rating'] 			= 'التصنيف مطلوب!';
$_['error_name'] 			= 'يجب أن يكون الاسم بين 1 و 32 حرفًا!';
$_['error_email']           = 'يبدو أن عنوان البريد الإلكتروني غير صالح!';
$_['error_message'] 	 	= 'يجب أن تكون الرسالة أكثر من 10 أحرف!';
$_['error_seller_notfound']	= 'الصفحة المطلوبة لا يمكن العثور عليه.';