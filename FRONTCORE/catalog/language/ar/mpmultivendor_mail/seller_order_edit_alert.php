<?php
// Text
$_['text_subject']   	= '%s - تحديث الطلب %s';
$_['text_order_id']     = 'رقم التعريف الخاص بالطلب:';
$_['text_date_added']   = 'تم إضافة التاريخ:';
$_['text_order_status'] 	= '%s قم بتحديث الطلب إلى الحالة التالية.';
$_['text_comment']         	= 'تعليقات البائع على الطلب هي:';
