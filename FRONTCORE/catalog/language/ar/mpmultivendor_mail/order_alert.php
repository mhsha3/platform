<?php
// Text
$_['text_subject']      = 'طلب';
$_['text_received']     = 'لقد تلقيت طلبًا.';
$_['text_order_id']     = 'رقم التعريف الخاص بالطلب:';
$_['text_date_added']   = 'تم إضافة التاريخ:';
$_['text_order_status'] = 'حالة الطلب:';
$_['text_product']      = 'منتجات';
$_['text_total']        = 'المجاميع';
$_['text_comment']      = 'التعليقات على طلبك هي:';
