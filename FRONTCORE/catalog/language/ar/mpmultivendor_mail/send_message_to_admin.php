<?php
// Text
$_['text_mail_subject']		= 'رسالة جديدة من%s';
$_['text_mail_sender']		= '%s';
$_['text_mail_details']		= 'بيانات المتصل';
$_['text_mail_name']		= 'اسم البائع ';
$_['text_mail_store_name']	= 'اسم متجر البائع: ';
$_['text_mail_email']		= 'البريد الإلكتروني للبائع: ';
$_['text_mail_enquiry']		= 'رسالة: ';
$_['text_mail_reply']		= '%s الدعم';