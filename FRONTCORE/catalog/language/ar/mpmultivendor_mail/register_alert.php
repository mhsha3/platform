<?php
// Text
$_['text_new_seller']   	= 'بائع جديد';
$_['text_signup']         	= 'قام بائع جديد بالتسجيل:';
$_['text_store_owner']      = 'اسم البائع';
$_['text_store_name']      	= 'اسم المتجر';
$_['text_email']          	= 'البريد الإلكتروني:';
$_['text_telephone']      	= 'هاتف:';
$_['text_address']      	= 'عنوان:';