<?php
// Text
$_['text_mail_subject']		= 'استفسار بواسطة (#%s)';
$_['text_mail_details']		= 'بيانات المتصل';
$_['text_mail_name']		= 'اسم: ';
$_['text_mail_email']		= 'البريد الإلكتروني: ';
$_['text_date_added']		= 'خلقت: ';
$_['text_mail_enquiry']		= 'انشئت ';
$_['text_mail_sender']		= '%s ( الدعم)';
$_['text_mail_reply']		= '%s الدعم';
$_['text_mail_view']		= 'أضف الرد على استفسار العميل عن طريق النقر على الرابط أدناه <br/>';
$_['text_mail_dontreply']	= 'يرجى عدم الرد على هذه الرسالة. هذا بريد يتم إنشاؤه تلقائيًا ولا يحضر الرد على معرف البريد الإلكتروني هذا. لأية توضيحات يرجى الزيارة';