﻿<?php
// Heading 
$_['heading_title']    = 'التحقق من رقم الجوال';

// Text 
$_['text_phone']    = 'الجوال';
$_['text_start']    = 'ارسال رمز التحقق';
$_['text_verification_code']    = 'رمز التحقق';
$_['text_provide_valid_number'] = 'الرجاء ادخال رقم جوال صحيح';
$_['text_connection_problem'] = 'تعذر الاتصال .. الرجاء المحاولة مره اخرى';
$_['text_provide_valid_mobile_number'] = 'الرجاء ادخال رقم جوال صحيح';
$_['text_invalid_pin'] = 'عفوا .. كود التحقق غير صحيح';
$_['text_verify'] = 'تحقق';
$_['text_max_retries_exceeded'] = "عفوا .. لقد تجاوزت الحد المسموح به للتحقق";
$_['text_please_wait'] = "الرجاء الانتظار";
$_['text_please_wait_next'] = "الرجاء انتظر %s ثانية لاجراء التحقق";
$_['text_resend'] = "اعادة ارسال رمز التحقق";
$_['text_send_success'] = "تم ارسال رمز التحقق لرقم جوالك المدخل";
$_['text_explain1'] = "سيتم ارسال رمز التحقق الى <font color='red'>%s</font> , نرجو التأكد من ان الجوال يعمل لديك";
$_['text_explain_select_type'] = "الرجاء اختيار طريقة التحقق";
$_['text_explain_phone_call'] = "سوف تتلقى مكالمة هاتفية للتحقق";
$_['text_explain_sms'] = "سوف تصلك رسالة قصيرة تحتوي على رمز التحقق";
$_['text_explain_phone_call2'] = "تم الاتصال . سيتم تزوديك برقم التحقق عبر الهاتف";
$_['text_explain_sms2'] = "تم ارسال رمز التحقق لرقم جوالك";
$_['text_explain_started'] = "الرجاء ادخال رمز التحقق ";
$_['text_explain_same_number'] = "تم تجاوز العدد المسموح به ";
?>
