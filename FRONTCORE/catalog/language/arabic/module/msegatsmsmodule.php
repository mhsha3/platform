﻿<?php
// Heading 
$_['heading_title']    		= 'ارسال رسائل قصيرة';

// Text
$_['text_nohp']    		 		= 'رقم الجوال مع رمز الدولة';
$_['text_message']     		= 'الرسالة';
$_['text_characters']  		= '<span class="help"></span>';
$_['text_success']  			= 'تم ارسال الرسالة';
$_['text_error']        	= 'لم يتم ارسال الرسالة <br />';

// Error
$_['error_nohp']       		= 'رقم الجوال مطلوب !';
$_['error_message']       = 'نص الرسالة مطلوب !';
$_['error_limit']       	= 'عفوا , لقد تجاوزت الحد المسموح به اليوم';

// Button
$_['button_send']					 = 'ارسال رسالة';
?>
