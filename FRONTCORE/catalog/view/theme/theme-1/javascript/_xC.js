$(document).ready(function() {
     $("._xOwB").owlCarousel({
         autoPlay: 3000,
         items : 5,
         pagination: false,
         itemsDesktop: [1199,3],
         itemsTablet:	[768,2],
         itemsMobile:	[700,2],
     });
});