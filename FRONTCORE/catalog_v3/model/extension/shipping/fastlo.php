<?php
class ModelExtensionShippingFastlo extends Model {
	function getQuote($address) {
		$this->load->language('extension/shipping/fastlo');

		$status = true;
 
		$method_data = array();

		if ($status) {
			$quote_data = array();

			$quote_data['fastlo'] = array(
				'code'         => 'fastlo.fastlo',
				'title'        => $this->language->get('text_description'),
				'cost'         => $this->config->get('shipping_fastlo_cost'),
				'tax_class_id' => 0,
				'text'         => $this->currency->format($this->tax->calculate($this->config->get('shipping_fastlo_cost'), 0, $this->config->get('config_tax')), $this->session->data['currency'])
			);
 
			$method_data = array(
				'code'       => 'fastlo',
				'title'      => $this->language->get('text_title'),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('shipping_fastlo_sort_order'),
				'error'      => false
			);
		}

		return $method_data;
	}

	protected function getStringBetween($string, $start, $end){
		$string = ' ' . $string;
		$ini = strpos($string, $start);
		if ($ini == 0) return '';

		$ini += strlen($start);
		$len = strpos($string, $end, $ini) - $ini;
		return substr($string, $ini, $len);
	}

	public function getTracknumber($order_id) {
        $query = $this->db->query("SELECT oh.date_added, os.name AS status, oh.comment, oh.notify FROM " . DB_PREFIX . "order_history oh LEFT JOIN " . DB_PREFIX . "order_status os ON oh.order_status_id = os.order_status_id WHERE oh.comment LIKE '%Tracknumber%' AND oh.order_id = '" . (int)$order_id . "' AND os.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY oh.date_added ASC");
        $shipments = $query->rows;
        if($shipments) {
            foreach($shipments as $key => $comment) {
                $cmnt_txt = ($comment['comment'])?$comment['comment']:'';
				$track_number = $this->getStringBetween($cmnt_txt, '[Tracknumber: ', ']');
                break;
            }
            return $track_number;
        }else{
            return 0;
        }
    }

	public function getShipmentStatus($order_id) {
		require_once DIR_SYSTEM . '../admin/controller/extension/shipping/fastlo/api.php';
        $this->api = new \Fastlo\Api($this->config->get('shipping_fastlo_api_key'), $this->config->get('shipping_fastlo_client_secret'), $this->config->get('shipping_fastlo_api_key'), $this->config->get('shipping_fastlo_test'));

		$trackNumber = $this->getTracknumber($order_id);
		if($trackNumber > 0) {
			$response = $this->api->fastloGetShipmentsStatus($trackNumber);
			if ($response['status_code'] == 200) {
				$output = $response['output'];		
				$statuses = [
					'10' => 'New',
					'20' => 'Pickup In Progress',
					'30' => 'Picked Up',
					'40' => 'In Distribution Center',
					'50' => 'Shipping In Progress',
					'60' => 'Delivery In Progress',
					'70' => 'Canceled',
					'80' => 'Returned',
					'90' => 'Dispatched',
					'100' => 'Delivered'
				];		
				return $statuses[$output['status_list'][$trackNumber]];
			}
		}

		return null;
	}
}