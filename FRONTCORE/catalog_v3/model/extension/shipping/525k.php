<?php
class ModelExtensionShipping525k extends Model {
	function getQuote($address) {
		$this->load->language('extension/shipping/525k');

		$status = true;

		$method_data = array();

		if ($status) {
			$quote_data = array();

			$quote_data['525k'] = array(
				'code'         => '525k.525k',
				'title'        => $this->language->get('text_description'),
				'cost'         => 0,
				'tax_class_id' => 0,
				'text'         => $this->currency->format($this->tax->calculate($this->config->get('payment_pilibaba_shipping_fee'), 0, $this->config->get('config_tax')), $this->session->data['currency'])
			);
 
			$method_data = array(
				'code'       => '525k',
				'title'      => $this->language->get('text_title'),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('shipping_525k_sort_order'),
				'error'      => false
			);
		}

		return $method_data;
	}

	protected function getStringBetween($string, $start, $end){
		$string = ' ' . $string;
		$ini = strpos($string, $start);
		if ($ini == 0) return '';

		$ini += strlen($start);
		$len = strpos($string, $end, $ini) - $ini;
		return substr($string, $ini, $len);
	}

	public function getWaybill($order_id) {
        $query = $this->db->query("SELECT oh.date_added, os.name AS status, oh.comment, oh.notify FROM " . DB_PREFIX . "order_history oh LEFT JOIN " . DB_PREFIX . "order_status os ON oh.order_status_id = os.order_status_id WHERE oh.comment LIKE '%Waybill No%' AND oh.order_id = '" . (int)$order_id . "' AND os.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY oh.date_added ASC");
        $shipments = $query->rows;
        if($shipments) {
            foreach($shipments as $key=>$comment) {
                $cmnt_txt = ($comment['comment'])?$comment['comment']:'';
				$waybill_no = $this->getStringBetween($cmnt_txt, '[Waybill No. ', ']');
                break;
            }
                
            return $waybill_no;
        }else{
            return null;
        }
	}

	public function getTrackingURL($order_id) {
		$waybill = $this->getWaybill($order_id);
		return (isset($waybill) ? "https://shipping.mlcgo.com/shipment/track/" . $this->getWaybill($order_id) : null);
	}

}