<?php
class ModelExtensionShippingSaee extends Model {
	function getQuote($address) {
		$this->load->language('extension/shipping/saee');

		$status = true;

		$method_data = array();

		if ($status) {
			$quote_data = array();

			$quote_data['saee'] = array(
				'code'         => 'saee.saee',
				'title'        => $this->language->get('text_description'),
				'cost'         => 0,
				'tax_class_id' => 0,
				'text'         => $this->currency->format($this->tax->calculate($this->config->get('payment_pilibaba_shipping_fee'), 0, $this->config->get('config_tax')), $this->session->data['currency'])
			);
 
			$method_data = array(
				'code'       => 'saee',
				'title'      => $this->language->get('text_title'),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('shipping_saee_sort_order'),
				'error'      => false
			);
		}

		return $method_data;
	}

	public function getSaeePayURL($order_id) {
        $query = $this->db->query("SELECT oh.date_added, os.name AS status, oh.comment, oh.notify FROM " . DB_PREFIX . "order_history oh LEFT JOIN " . DB_PREFIX . "order_status os ON oh.order_status_id = os.order_status_id WHERE oh.comment LIKE '%Waybill No%' AND oh.order_id = '" . (int)$order_id . "' AND os.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY oh.date_added ASC");
        $shipments = $query->rows;
        if($shipments) {
            foreach($shipments as $key=>$comment) {
                $cmnt_txt = ($comment['comment'])?$comment['comment']:'';
                $shourl = strstr($cmnt_txt,"]",true);
                $shourl = trim(strstr($shourl,"["), "[");
                break;
            }
                
            return $shourl;
        }else{
            return null;
        }
	}
		
}