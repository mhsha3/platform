<?php
class ControllerCommonFooterRight extends Controller {
	public function index() {
		
		$data['socialmedia_maroof'] = $this->config->get('config_socialmedia_maroof');
		$data['telephone'] = $this->config->get('config_telephone');
		$data['address'] = $this->config->get('config_address');
		$data['email'] = $this->config->get('config_email');

		return $this->load->view('common/footer_right', $data);
	}
}
