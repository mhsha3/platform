<?php
class ControllerExtensionPaymentPaylink extends Controller
{
    public function index(){
        return $this->load->view('extension/payment/paylink');
    }
    protected function Auth(){
        $apiId = $this->config->get('payment_paylink_apiId');
        $secretKey = $this->config->get('payment_paylink_secretKey');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://restapi.paylink.sa/api/auth');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,'{"apiId":"'.$apiId.'","persistToken":false,"secretKey":"'.$secretKey.'"}');
        $headers = [];
        $headers[] = 'Accept: */*';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        return json_decode(curl_exec($ch), true)['id_token'];
    }
   public function addInvoice(){
       if(isset($this->session->data['order_id'])){
       $this->load->model('checkout/order');
       $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
       $amount = $this->currency->format($order_info['total'],$this->config->get('config_currency'),'', false);
       $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, 'https://restapi.paylink.sa/api/addInvoice');
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
       curl_setopt($ch, CURLOPT_POST, 1);
       curl_setopt($ch, CURLOPT_POSTFIELDS,'{
           "amount": '.$amount.',
           "callBackUrl": "'.$this->url->link('extension/payment/paylink/confirm').'",
           "clientEmail": "'.$order_info['email'].'",
           "clientMobile": "'.$order_info['telephone'].'",
           "clientName": "'.$order_info['firstname'].' '.$order_info['lastname'].'",
           "note": "'.$this->config->get('config_lana_store_id').'_'.$this->session->data['order_id'].'_'.HTTP_SERVER.'",
           "orderNumber": "'.$this->config->get('config_lana_store_id').'_'.$this->session->data['order_id'].'_'.HTTP_SERVER.'",
           "products": []
        }');
        $headers = array();
        $headers[] = 'Accept: application/json;charset=UTF-8';
        $headers[] = 'Authorization: Bearer '.$this->Auth();
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = json_decode(curl_exec($ch),true);
        if (curl_errno($ch)){ echo 'Error:' . curl_error($ch);}
        curl_close($ch);
    $json = array();
    if(strtolower($result['orderStatus']) == 'created'){
      $this->session->data['PayLink_transactionNo'] = $result['transactionNo'];
      $json['redirect'] = $result['url'];
    }
    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
   }
    }

    public function confirm(){
        $json = [];
        if ($this->session->data['payment_method']['code'] == 'paylink' &&
            isset($this->session->data['PayLink_transactionNo']) &&
            isset($this->request->get['orderNumber']) &&
            isset($this->request->get['transactionNo'])){
            if ($this->session->data['PayLink_transactionNo'] == $this->request->get['transactionNo']){
                $ch = curl_init();
                curl_setopt($ch,CURLOPT_URL,'https://restapi.paylink.sa/api/getInvoice/'.(int)$this->session->data['PayLink_transactionNo']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                $headers = [];
                $headers[] = 'Accept: application/json;charset=UTF-8';
                $headers[] = 'Authorization: Bearer ' . $this->Auth();
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                $result = curl_exec($ch);
                if (curl_errno($ch)) {
                    echo 'Error:' . curl_error($ch);
                }
                curl_close($ch);
                $result = json_decode($result, true);
                if ($result['orderStatus'] == 'Paid') {
                        $this->load->model('checkout/order');
                        $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
                        $amount = $this->currency->format($order_info['total'],$this->config->get('config_currency'),'', false);

                        $cart = $this->cart->getProducts();
                        if(isset($this->session->data['in_cart_dropshipping'])){
                            unset($this->session->data['in_cart_dropshipping']);
                        }
                        if(isset($this->session->data['notes_cart_data'])){
                            unset($this->session->data['notes_cart_data']);
                        }
                        if(isset($this->session->data['in_cart_Notdropshipping'])){
                            unset($this->session->data['in_cart_Notdropshipping']);
                        }
                        $dropshipping_cost[] = [];
                        foreach($cart as $value){
                            if($value['dropshipping']){
                                $this->session->data['notes_cart_data'][] = ['product_id'=>$value['product_id'],'price'=>$value['price'],'product_cost'=>$value['product_cost'],'quantity'=>$value['quantity'],'balance'=>($value['product_cost']*$value['quantity']),'dropshipping'=>$value['dropshipping']];
                                $dropshipping_cost[] = ($value['product_cost']*$value['quantity']);
                                $this->session->data['in_cart_dropshipping'] = '1';
                            }else{
                                $this->session->data['notes_cart_data'][] = ['product_id'=>$value['product_id'],'price'=>$value['price'],'product_cost'=>$value['product_cost'],'quantity'=>$value['quantity'],'balance'=>($value['product_cost']*$value['quantity']),'dropshipping'=>$value['dropshipping']];
                                $this->session->data['in_cart_Notdropshipping'] = '1';
                            }
                            }
                            $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('payment_cod_order_status_id'));
                            $supplier_cost = array_sum($dropshipping_cost);
                            $this->db->query("INSERT INTO `" . DB_PREFIX . "success_orders_payments_tracking` (`id`, `transaction_num`,`order_id`,`amount`, `payment_company`, `response`, `created_at`, `lana_payed`) VALUES (NULL, '".$this->request->get['transactionNo']."', '".(int)$this->session->data['order_id']."','".$result['amount']."','paylink' ,'".json_encode($result)."', '".date("Y-m-d H:i:s")."', '0')");
                            $this->db->query("INSERT INTO `" . DB_PREFIX . "wallet_transaction` (`transaction_id`, `amount`, `type`, `created_at`, `order_id`, `payment_transaction`,`fees`,`notes`,`status`,`supplier_cost`) VALUES (NULL, '".$amount."', 'paylink_payment', '".date("Y-m-d H:i:s")."', '".(int)$this->session->data['order_id']."', '".$this->request->get['transactionNo']."', ".(float)$fees.",'".json_encode($this->session->data['notes_cart_data'])."','1','".(float)$supplier_cost."')");
                    $this->response->redirect($this->url->link('checkout/success'));
                }else{
                    $this->session->data['FAILED_PAYMENT'] = ['payment_method'=>'paylink','payment_transaction'=>$this->request->get['transactionNo']];
                    $this->response->redirect($this->url->link('checkout/failed'));
                }
            }else{
                $this->response->redirect($this->url->link('common/home'));
            }
        }else{
            $this->response->redirect($this->url->link('common/home'));
        }
    }
}
