<?php
class ControllerExtensionPaymentCod extends Controller {
	public function index() {
		return $this->load->view('extension/payment/cod');
	}

	public function confirm() {
		$json = array();
		
		if ($this->session->data['payment_method']['code'] == 'cod') {
			$this->load->model('checkout/order');
			$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
            $amount = $this->currency->format($order_info['total'],$this->config->get('config_currency'),'', false);
			
			$cart = $this->cart->getProducts();
			if(isset($this->session->data['in_cart_dropshipping'])){
				unset($this->session->data['in_cart_dropshipping']);
			}
			if(isset($this->session->data['notes_cart_data'])){
				unset($this->session->data['notes_cart_data']);
			}
			if(isset($this->session->data['in_cart_Notdropshipping'])){
				unset($this->session->data['in_cart_Notdropshipping']);
			}
			foreach($cart as $value){
			if($value['dropshipping']){
				$this->session->data['notes_cart_data'][] = ['product_id'=>$value['product_id'],'price'=>$value['price'],'product_cost'=>$value['product_cost'],'quantity'=>$value['quantity'],'balance'=>($value['product_cost']*$value['quantity']),'dropshipping'=>$value['dropshipping']];
				$this->session->data['in_cart_dropshipping'] = '1';
			 }else{
				$this->session->data['in_cart_Notdropshipping'] = '1';
			 }
			}
			$this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('payment_cod_order_status_id'));			
			if(!isset($this->session->data['in_cart_Notdropshipping']) && isset($this->session->data['in_cart_dropshipping']) && isset($this->session->data['notes_cart_data'])){
				$fees = 0;
				$balance = 0;
				$amount = -1 * $amount;
				foreach($this->session->data['notes_cart_data'] as $dd => $xx){
					if(isset($xx['balance'])){
						$x[] = $xx['balance'];
					}
				}
				$balance = array_sum($x);
				$this->db->query("INSERT INTO `" . DB_PREFIX . "wallet_transaction` (`transaction_id`, `amount`, `type`, `created_at`, `order_id`, `payment_transaction`,`fees`,`notes`,`status`,`supplier_cost`) VALUES (NULL, '".$amount."', 'cod_payment', '".date("Y-m-d H:i:s")."', '".(int)$this->session->data['order_id']."', '', ".(float)$fees.",'".json_encode($this->session->data['notes_cart_data'])."','1','".(float)$balance."')");
			}	
	
			$json['redirect'] = $this->url->link('checkout/success');
		}
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));		
	}
}
