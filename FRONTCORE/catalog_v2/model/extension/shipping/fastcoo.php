<?php
class ModelExtensionShippingFastcoo extends Model {
	function getQuote($address) {
		$this->load->language('extension/shipping/fastcoo');

		$status = true;
 
		$method_data = array();

		if ($status) {
			$quote_data = array();

			$quote_data['fastcoo'] = array(
				'code'         => 'fastcoo.fastcoo',
				'title'        => $this->language->get('text_description'),
				'cost'         => $this->config->get('shipping_fastcoo_cost'),
				'tax_class_id' => 0,
				'text'         => $this->currency->format($this->tax->calculate($this->config->get('shipping_fastcoo_cost'), 0, $this->config->get('config_tax')), $this->session->data['currency'])
			);
 
			$method_data = array(
				'code'       => 'fastcoo',
				'title'      => $this->language->get('text_title'),
				'quote'      => $quote_data,
				'sort_order' => $this->config->get('shipping_fastcoo_sort_order'),
				'error'      => false
			);
		}

		return $method_data;
	}

	protected function getStringBetween($string, $start, $end){
		$string = ' ' . $string;
		$ini = strpos($string, $start);
		if ($ini == 0) return '';

		$ini += strlen($start);
		$len = strpos($string, $end, $ini) - $ini;
		return substr($string, $ini, $len);
	}

	public function getAWBNumber($order_id) {
        $query = $this->db->query("SELECT oh.date_added, os.name AS status, oh.comment, oh.notify FROM " . DB_PREFIX . "order_history oh LEFT JOIN " . DB_PREFIX . "order_status os ON oh.order_status_id = os.order_status_id WHERE oh.comment LIKE '%AWB%' AND oh.order_id = '" . (int)$order_id . "' AND os.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY oh.date_added ASC");
        $shipments = $query->rows;
        if($shipments) {
            foreach($shipments as $key=>$comment) {
                $cmnt_txt = ($comment['comment'])?$comment['comment']:'';
				$awb_no = $this->getStringBetween($cmnt_txt, 'AWB No. ', ' [');
                break;
            }
                
            return $awb_no;
        }else{
            return 0;
        }
	}

	public function getShipmentStatus($order_id) {
        require_once DIR_SYSTEM . '../admin/controller/extension/shipping/fastcoo/api.php';
        $api = new \Fastcoo\Api($this->config->get('shipping_fastcoo_email_id'), $this->config->get('shipping_fastcoo_password'));

		$AWBNumber = $this->getAWBNumber($order_id);
		if($AWBNumber) {
			$trackingOrderResponse = $api->trackingOrder(['awb_no' => $AWBNumber]);
			if(!isset($trackingOrderResponse['error'])) {
				return $trackingOrderResponse['shipment_data']['status'];
			}
		}

		return null;
	}
}