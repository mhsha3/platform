<?php
/**
 * Created by PhpStorm.
 * User: anhvnit
 * Date: 10/29/18
 * Time: 09:29
 */
require_once DIR_SYSTEM.'library/openpos/vendor/autoload.php';
require_once DIR_SYSTEM.'library/openpos/session.php';
class Openpos{
    private $registry;
    public $barcode_generator;
    public $pos_version = "3.1";
    public $pos_session;
    public $admin_folder = 'admin'; // change this if change admin folder name
    public $base_setting = false;
    public $store_setting = false;
    public function __construct($registry) {
        // openpos Pro
        $this->registry = $registry;
        $this->logger = new \Log('openpos.log');
        $this->barcode_generator =  new \Picqer\Barcode\BarcodeGeneratorPNG();
        $this->pos_session = new \Openpos\Session();

    }
    public function __get($name) {
        return $this->registry->get($name);
    }

    public function version(){
        return $this->pos_version;
    }

    public function generateBarcode($barcode,$barcode_mode = 'code_128')
    {
        $barcode_generator = $this->barcode_generator;
        $mode = $barcode_generator::TYPE_CODE_128;
        switch ($barcode_mode)
        {
            case 'code_128':
                $mode = $barcode_generator::TYPE_CODE_128;
                break;
            case 'ean_13':
                $mode = $barcode_generator::TYPE_EAN_13;
                break;
            case 'ean_8':
                $mode = $barcode_generator::TYPE_EAN_8;
                break;
            case 'code_39':
                $mode = $barcode_generator::TYPE_CODE_39;
                break;
            case 'upc_a':
                $mode = $barcode_generator::TYPE_UPC_A;
                break;
            case 'upc_e':
                $mode = $barcode_generator::TYPE_UPC_E;
                break;
            default:
                $mode = $barcode_generator::TYPE_CODE_128;
        }
        $img_data = $barcode_generator->getBarcode($barcode, $mode);
        return base64_encode($img_data);
    }
    public function validateSession($session_id){
            return $this->pos_session->validate($session_id);
    }

    public function getPosSession(){
        return $this->pos_session;
    }
    public function getAllSessions(){
        $sessions = $this->pos_session->sessions();
        $result = array();

        foreach($sessions as $session)
        {
            $prefix= 'sess_';
            $session_id = str_replace($prefix,'',$session);
            $result[$session_id] = $this->pos_session->read($session_id);
        }
        return $result;
    }

    public function get_product_formatted_data($product_data,$store_id = 0){
        $this->load->model('tool/image');
        $this->load->model('catalog/product');
        $this->load->model('extension/module/openpos');
        $setting = $this->getAllSettingValues($store_id);
        $main_setting = $this->getAllSettingValues(0);
        $pos_tax_class = $main_setting['pos_tax_class'];
        $image_width = $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_width');
        $image_height = $this->config->get('theme_' . $this->config->get('config_theme') . '_image_product_height');

        if($setting['image_width'])
        {
            $image_width = $setting['image_width'];
        }
        if($setting['image_height'])
        {
            $image_height = $setting['image_height'];
        }

        if ($product_data['image']) {
            $image = $this->model_tool_image->resize($product_data['image'], $image_width, $image_height);
        } else {
            $image = $this->model_tool_image->resize('placeholder.png', $image_width, $image_height);
        }



        $option_data = $this->model_catalog_product->getProductOptions($product_data['product_id']);
        $options = array();
        
        foreach($option_data as $o)
        {
            //radio , select , checkbox
            
            if($o['type'] == 'textarea')
            {
                $o['type'] = 'text';
               
            }
            if($o['type'] == 'file')
            {
                $o['type'] = 'upload';
               
            }
            $type = $o['type'];
            $support = true;
            if(!in_array($o['type'],array('radio','checkbox','select','text','upload')))
            {
                $type = 'text';
                $support = false;
            }
            

            if($support)
            {
                $values = array();
                $value_data = $o['product_option_value'];
                foreach($value_data as $v)
                {
                    $cost = $v['price'];
                    if($v['price_prefix'] != '+')
                    {
                        $cost = 0 - $cost;
                    }
                    $tm = array(
                        'value_id' => $v['product_option_value_id'],
                        'label' => $v['name'],
                        'cost' => $cost
                    );
                    $values[] = $tm;
                }
                $option = array(
                    'label' => $o['name'],
                    'option_id' => $o['product_option_id'],
                    'type' => $type,
                    'require' => $o['required'] == 1 ? true : false,
                    'options' => $values
                );
                $options[] = $option;
            }
        }

        $category_data = $this->model_catalog_product->getCategories($product_data['product_id']);
        $categories = array();
        foreach($category_data as $c)
        {
            $categories[] = $c['category_id'];
        }

        $price = $product_data['price'];

        $price_included_tax = $this->tax->calculate($price, $product_data['tax_class_id'], true);
        $tax_amount = $price_included_tax - $price;
        $tax = array(
            'code' => 'tax_'.$product_data['tax_class_id'],
            'rate' => 0,
            'shipping' => 'yes',
            'rate_id' => $product_data['tax_class_id'],
            'label' => 'TAX',
            'compound' => 'yes'
        );
        $setting = $this->getAllSettingValues(0);

        $pos_barcode_field = $setting['pos_barcode_field'];
        if(!$pos_barcode_field)
        {
            $pos_barcode_field = 'product_id';
        }

        $tax_setting = $setting['pos_tax_class'];



        // from, to , price , priority, customer_id,customer_group_id,qty

        $product_specials = $this->model_extension_module_openpos->getProductSpecials($product_data['product_id']);
        $product_discounts = $this->model_extension_module_openpos->getProductDiscounts($product_data['product_id']);
        $special_price_rules = array();



        $discounts = array();
        $discount_group_id = array();
        foreach ($product_discounts as $discount) {
            $key = $discount['customer_group_id'];
            if(!isset($discount[$key]))
            {
                $customer_group_id =  $discount['customer_group_id'];
                $discount_group_id[$customer_group_id] = $key;
                $discounts[$key] = array(
                    'customer_id' => '',
                    'customer_group_id' => $discount['customer_group_id'],
                    'from' => $discount['date_start'],
                    'to' => $discount['date_end'],
                    'quantity' => $discount['quantity'],
                    'price'    => $discount['price'],
                    'priority'    => $discount['priority']
                );
            }
        }
        $specials = array();
        foreach ($product_specials as $discount) {
            $key = $discount['customer_group_id'];
            if(!isset($specials[$key]))
            {
                $customer_group_id =  $discount['customer_group_id'];
                if(isset($discount_group_id[$customer_group_id]))
                {
                    unset($discount_group_id[$customer_group_id]);
                }
                $specials[$key] = array(
                    'customer_id' => '',
                    'customer_group_id' => $discount['customer_group_id'],
                    'from' => $discount['date_start'],
                    'to' => $discount['date_end'],
                    'quantity' => 1,
                    'price'    => $discount['price'],
                    'priority'    => $discount['priority']
                );
            }
        }
        foreach($discount_group_id as $key)
        {
            $special_price_rules[] = $discounts[$key];
        }
        foreach($specials as $special)
        {
            $special_price_rules[] = $special;
        }

        $is_price_included_tax = false;
        $final_tax = array();
        if($pos_tax_class == 'op_productax')
        {
            $tax['rate'] = $price > 0 ? 100*($tax_amount / $price) : 0;
            $price = $price_included_tax - $tax_amount;
            $is_price_included_tax  = true;
            $final_tax = array($tax);

        }else{
            $tax_amount = 0;
        }
        $barcode = isset($product_data[$pos_barcode_field]) ? strtolower($product_data[$pos_barcode_field]) : $product_data['product_id'];

        $tmp = array(
            'name' => $product_data['name'],
            'id' => $product_data['product_id'],
            'parent_id' => $product_data['product_id'],
            'sku' => $product_data['sku'],
            'qty' => 1 * $product_data['quantity'],
            'manage_stock' =>true,
            'stock_status' => $product_data['stock_status'],
            'barcode' => trim($barcode),
            'image' => $image,
            'price' => 1 * $price,
            'final_price' => 1 * $price,
            'special_price' => 1 * $price,
            'regular_price' => 1 * $price,
            'price_rules' => $special_price_rules,
            'sale_from' => '',
            'sale_to' => '',
            'status' => $product_data['status'],
            'categories' => $categories,
            'tax' => $final_tax,
            'tax_amount' => $tax_amount,
            'price_included_tax' => $is_price_included_tax,
            'group_items' => array(),
            'variations' => array(),
            'options' => $options,
            'bundles' => array(),
            'display_special_price' => false,
            'price_display_html' => '',
            'display' => true
        );
        $lang = $setting['pos_language'];
        
        if($lang == 'vi')
        {
            $tmp['search_keyword'] = $this->custom_vnsearch_slug($tmp['name']);
        }
        return $tmp;
    }

    public  function custom_vnsearch_slug($str) {
        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        $str = preg_replace('/([\s]+)/', ' ', $str);
        $str = str_replace('  ',' ',$str);
        return $str;
    }
    public function getAvailablePayments($is_front = false){
        $data= array();

        $data[] = array(
            'name' => 'Chip & PIN',
            'code' => 'chip_pin',
        );
        // Compatibility code for old extension folders
        $files = glob(DIR_APPLICATION . 'controller/extension/payment/*.php');

        if ($files) {
            foreach ($files as $file) {
                $extension = basename($file, '.php');
                $this->load->language('extension/payment/' . $extension, 'extension');
                $all_lang = $this->language->all();
                $all_lang_data = $all_lang['extension']->data;
                $name = $extension;
                if(isset($all_lang_data['heading_title']))
                {
                    $name = $this->language->get('extension')->get('heading_title');

                }
                if(isset($all_lang_data['text_title']))
                {
                    $name = $this->language->get('extension')->get('text_title');
                }
                $data[] = array(
                    'name'       => $name,
                    'code'       => $extension,
                );
            }
        }
        return $data;
    }

    public function getAvailableShipments(){
        $data= array();

        $admin_folder_name = $this->admin_folder;

        $admin_dir = dirname(DIR_SYSTEM).'/'.$admin_folder_name.'/';
        $files = glob($admin_dir . 'controller/extension/shipping/*.php');

        if ($files) {
            foreach ($files as $file) {
                $extension = basename($file, '.php');
                $this->load->language('extension/shipping/' . $extension, 'extension');
                $all_lang = $this->language->all();
                $all_lang_data = $all_lang['extension']->data;
                $name = $extension;
                if(isset($all_lang_data['heading_title']))
                {
                    $name = $this->language->get('extension')->get('heading_title');

                }
                if(isset($all_lang_data['text_title']))
                {
                    $name = $this->language->get('extension')->get('text_title');
                }
                $data[] = array(
                    'name'       => $name,
                    'code'       => $extension,
                );
            }
        }

        return $data;
    }

    public function getSettingConfig($store_id = 0){
        global $_front_openpos;
        $refund_duration = array();
        $this->load->model('localisation/order_status');
        $order_status_data = $this->model_localisation_order_status->getOrderStatuses();
        $this->load->language('extension/openpos/setting');
        $this->load->model('setting/extension');
        $token = '';
        if(!$_front_openpos)
        {
            $admin_session = $this->registry->get('session');

            $token = isset($admin_session->data['user_token']) ? $admin_session->data['user_token'] : '';
        }


        $payment_method_data = $this->getAvailablePayments();

        $payment_methods = array();

        foreach($payment_method_data as $payment)
        {
            $payment_methods[] = array(
                'name' => $payment['name'],
                'value' => $payment['code'],
            );

        }
        $shipping_method_data = $this->getAvailableShipments();
        $shipping_methods = array();
        foreach($shipping_method_data as $shipment)
        {
            $shipping_methods[] = array(
                'name' => $shipment['name'],
                'value' => $shipment['code'],
            );

        }

        $order_status = array();
        foreach($order_status_data as $s)
        {
            $order_status[] = array(
                'name' => $s['name'],
                'value' => $s['order_status_id'],
            );
        }

        $setting_general = array(
            
            array(
                'key' => 'pos_stock_manage',
                'type' => 'select',
                'title' => $this->language->get('title_pos_stock_manage'),
                'default' => 'yes',
                'options' => array(
                    ['name' => $this->language->get('label_yes'),'value' => 'yes'],
                    ['name' => $this->language->get('label_no'),'value' => 'no'],
                ),
                'description' => 'Enable add out of stock product in pos panel'
            ),
            array(
                'key'    => 'pos_order_status',
                'title'   => 'POS Order Status',
                'description'    => 'status for those order created by POS',
                'type'    => 'select',
                'default' => '5',
                'options' =>  $order_status
            ),
            array(
                'key'    => 'pos_allow_refund',
                'title'   => 'Allow Refund',
                'description'    => 'Refund offline via pos panel',
                'type'    => 'select',
                'default' => 'yes',
                'options' =>  array(
                    ['name' => 'Always allow','value' => 'yes'],
                    ['name' => 'Allow with durations','value' => 'yes_day'],
                    ['name' => 'No Refund','value' => 'no'],
                )
            ),
            $refund_duration,
            array(
                'key'    => 'pos_tax_class',
                'title'   => 'Pos Tax Class',
                'description' => 'Tax Class assign for POS system',
                'type'    => 'select',
                'default' => 'op_productax',
                'options' =>  array(
                    ['name' => 'Use Product Tax Class','value' => 'op_productax'],
                    ['name' => 'No Tax','value' => 'op_notax'],
                )
            ),
            array(
                'key'    => 'pos_point_rate',
                'title'   => 'Customer Points Rate',
                'description' => 'Rate of point to redeem in POS',
                'type'    => 'number',
                'default' => '0',
                'options' =>  array()
            )

        );

        $setting_barcode = array(
            array(
                'key' => 'pos_barcode_field',
                'type' => 'select',
                'title' => 'Barcode Field',
                'default' => 'product_id',
                'options' => array(
                    ['name' => 'Product Id','value' => 'product_id'],
                    ['name' => 'Product Model','value' => 'model'],
                    ['name' => 'Product Sku','value' => 'sku'],
                    ['name' => 'Product UPC','value' => 'upc'],
                    ['name' => 'Product JAN','value' => 'jan'],
                    ['name' => 'Product ISBN','value' => 'isbn'],
                    ['name' => 'Product MPN','value' => 'mpn'],
                    ['name' => 'Product EAN','value' => 'ean'],
                ),
                'description' => 'Field use as barcode to identify product on POS. Make sure this field is unique and entered on all product'
            ),
            array(
                'key' => 'unit',
                'type' => 'select',
                'title' => 'Unit',
                'default' => 'in',
                'options' => array(
                    ['name' => 'Inch','value' => 'in'],
                    ['name' => 'Minimeter','value' => 'mm'],
                ),
                'description' => ''
            ),
            array(
                'key' => 'sheet_width',
                'type' => 'number',
                'title' => 'Sheet Width',
                'default' => '8.5',
                'description' => ''
            ),
            array(
                'key' => 'sheet_height',
                'type' => 'number',
                'title' => 'Sheet Height',
                'default' => '11',
                'description' => ''
            ),
            array(
                'key' => 'vertical_space',
                'type' => 'number',
                'title' => 'Vertical Space',
                'default' => '0',
                'description' => ''
            ),
            array(
                'key' => 'horizontal_space',
                'type' => 'number',
                'title' => 'Horizontal Space',
                'default' => '0.125',
                'description' => ''
            ),
            array(
                'key' => 'margin_top',
                'type' => 'number',
                'title' => 'Margin Top',
                'default' => '0.5',
                'description' => ''
            ),
            array(
                'key' => 'margin_right',
                'type' => 'number',
                'title' => 'Margin Right',
                'default' => '0.188',
                'description' => ''
            ),
            array(
                'key' => 'margin_bottom',
                'type' => 'number',
                'title' => 'Margin Bottom',
                'default' => '0.5',
                'description' => ''
            ),
            array(
                'key' => 'margin_left',
                'type' => 'number',
                'title' => 'Margin Left',
                'default' => '0.188',
                'description' => ''
            ),
            array(
                'key' => 'label_width',
                'type' => 'number',
                'title' => 'Label Width',
                'default' => '2.625',
                'description' => ''
            ),
            array(
                'key' => 'label_height',
                'type' => 'number',
                'title' => 'Label Height',
                'default' => '1',
                'description' => ''
            ),
            array(
                'key' => 'padding_top',
                'type' => 'number',
                'title' => 'Padding Top',
                'default' => '0.1',
                'description' => ''
            ),
            array(
                'key' => 'padding_right',
                'type' => 'number',
                'title' => 'Padding Right',
                'default' => '0.1',
                'description' => ''
            ),
            array(
                'key' => 'padding_bottom',
                'type' => 'number',
                'title' => 'Padding Bottom',
                'default' => '0.1',
                'description' => ''
            ),
            array(
                'key' => 'padding_left',
                'type' => 'number',
                'title' => 'Padding Left',
                'default' => '0.1',
                'description' => ''
            ),
            array(
                'key' => 'label_template',
                'type' => 'textarea',
                'title' => 'Label Template',
                'default' => '<p>[product_name]</p><p>[barcode_image]</p><p>[product_barcode_number]</p>',
                'description' => ''
            ),
            array(
                'key' => 'barcode_mode',
                'type' => 'select',
                'title' => 'Barcode Mode',
                'default' => 'code_128',
                'description' => '',
                'options' => array(
                    ['name' => 'Code 128','value' => 'code_128'],
                    ['name' => 'EAN-13','value' => 'ean_13'],
                    ['name' => 'EAN-8','value' => 'ean_8'],
                    ['name' => 'Code-39','value' => 'code_39'],
                    ['name' => 'UPC-A','value' => 'upc_a'],
                    ['name' => 'UPC-E','value' => 'upc_e'],
                )
            ),
            array(
                'key' => 'barcode_width',
                'type' => 'number',
                'title' => 'Barcode Width',
                'default' => '2.625',
                'description' => ''
            ),
            array(
                'key' => 'barcode_height',
                'type' => 'number',
                'title' => 'Barcode height',
                'default' => '1',
                'description' => ''
            )
        );

        $setting_receipt = array(

            array(
                'key'              => 'receipt_width',
                'title'             => 'Receipt Width',
                'description'       => 'inch',
                'type'              => 'text',
                'default'           => '2.28',
                'sanitize_callback' => 'sanitize_text_field'
            ),
            array(
                'key'              => 'receipt_padding_top',
                'title'             => 'Padding Top',
                'description'       => 'inch',
                'type'              => 'number',
                'default'           => '0',
                'sanitize_callback' => 'sanitize_text_field'
            ),
            array(
                'key'              => 'receipt_padding_right',
                'title'             => 'Padding Right',
                'description'              => 'inch',
                'type'              => 'number',
                'default'           => '0',
                'sanitize_callback' => 'sanitize_text_field'
            ),
            array(
                'key'              => 'receipt_padding_bottom',
                'title'             => 'Padding Bottom',
                'description'              => 'inch',
                'type'              => 'number',
                'default'           => '0',
                'sanitize_callback' => 'sanitize_text_field'
            ),
            array(
                'key'              => 'receipt_padding_left',
                'title'             => 'Padding Left',
                'description'              => 'inch',
                'type'              => 'number',
                'default'           => '0',
                'sanitize_callback' => 'sanitize_text_field'
            ),

            array(
                'key'              => 'receipt_template_header',
                'title'            => 'Receipt Template Header' ,
                'description'      =>  'use [customer_name], [customer_phone],[sale_person], [created_at], [order_number], [customer_email] shortcode to adjust receipt information, accept html string'  ,
                'type'              => 'wysiwyg',
                'default'           => $this->getDefaultSettingFile('receipt_template_header')
            ),
            array(
                'key'              => 'receipt_template_footer',
                'title'            =>  'Receipt Template Footer' ,
                'description'      =>   'use [customer_name], [customer_phone], [sale_person], [created_at], [order_number], [customer_email] shortcode to adjust receipt information, accept html string' ,
                'type'             => 'wysiwyg',
                'default'          => $this->getDefaultSettingFile('receipt_template_footer')
            ),
            array(
                'key'              => 'receipt_css',
                'title'            =>   'Receipt Style' ,
                'description'      =>  '<a  target="_blank" href="index.php?route=extension/module/openpos/preview_receipt&user_token='.$token.'">click here</a> to preview receipt' ,
                'type'             => 'textarea',
                'default'          =>  $this->getDefaultSettingFile('receipt_css'),
            ),
        );


        $cutom_css = array('key' => 'custom_css',
            'type' => 'textarea',
            'title' => 'POS Custom CSS',
            'default' => '',
            'options' => array(),
            'description' => ''
        );
        if($store_id > 0)
        {
            $cutom_css = [];
        }
        $setting_pos = array(
            array(
                'key'              => 'pos_language',
                'title'             => 'Default POS Language',
                'description'              => 'Default language on POS. To translate goto pos/assets/i18n/_you_lang.json and update this file',
                'default'           => '_auto',
                'type'              => 'select',
                'options' => $this->allLanguageSupport()
            ),
            array(
                'key'              => 'dashboard_display',
                'title'             => 'Default Dashboard Display',
                'description'              => 'Default display for POS , in case category please set category item on category setting',
                'default'           => 'product',
                'type'              => 'select',
                'options' => array(
                    ['name' => 'New DashBoard','value' => 'board'],
                    ['name' => 'Products','value' => 'product'],
                    ['name' => 'Categories','value' => 'category']
                )
            ),
            array(
                'key'              => 'pos_search_product_online',
                'title'             => 'Search Product Type',
                'description'              => 'Default search product type with keyword on search bar',
                'default'           => 'no',
                'type'              => 'select',
                'options' => array(
                    ['name' => 'Offline Data','value' => 'no'],
                    ['name' => 'Online ','value' => 'yes']
                )
            ),
            array(
                'key'              => 'pos_allow_custom_item',
                'title'             => 'Add custom product',
                'description'              => 'Allow add product do not exist in system',
                'default'           => 'yes',
                'type'              => 'select',
                'options' => array(
                    ['name' => 'Yes','value' => 'yes'],
                    ['name' => 'No','value' => 'no'],
                )
            ),
            array(
                'key'              => 'accept_negative_checkout',
                'title'             => 'Allow Negative Qty',
                'description'              => 'Allow negative qty , grand total  use as refund',
                'default'           => 'no',
                'type'              => 'select',
                'options' => array(
                    ['name' => 'Yes','value' => 'yes'],
                    ['name' => 'No','value' => 'no'],
                )
            ),

            array(
                'key'              => 'time_frequency',
                'title'             => 'Time Frequency',
                'description'              => 'Time duration POS state checking (in mini seconds)',
                'default'           => '3000',
                'type'              => 'number',
                'options' => array()
            ),
            array(
                'key'              => 'pos_auto_sync',
                'title'             => 'Product Auto Sync',
                'description'              => 'Auto sync product qty by running process in background',
                'default'           => 'yes',
                'type'              => 'select',
                'options' => array(
                    ['name' => 'Yes','value' => 'yes'],
                    ['name' => 'No','value' => 'no'],
                )
            ),
            array(
                'key'              => 'pos_clear_product',
                'title'             => 'Clear Product List ',
                'description'              => 'Auto clear product list on your local data after logout. It help if you have a lots of products.',
                'default'           => 'yes',
                'type'              => 'select',
                'options' => array(
                    ['name' => 'Yes','value' => 'yes'],
                    ['name' => 'No','value' => 'no'],
                )
            ),
            array(
                'key'              => 'pos_display_outofstock',
                'title'             => 'Display Out of stock',
                'description'              => 'Display out of stock product in POS panel',
                'default'           => 'no',
                'type'              => 'select',
                'options' => array(
                    ['name' => 'Yes','value' => 'yes'],
                    ['name' => 'No','value' => 'no'],
                )
            ),
            array(
                'key'              => 'pos_categories',
                'title'             => 'POS Category',
                'description'              => 'Categories display on POS panel',
                'default'           => array(),
                'type'              => 'categories'
            ),
            array('key' => 'image_width',
                'type' => 'number',
                'title' => 'POS Image Width',
                'default' => '208',
                'options' => array(),
                'description' => 'Width of image on pos panel (px)'
            ),
            array(
                'key' => 'image_height',
                'type' => 'number',
                'title' => 'POS Image Height',
                'default' => '195',
                'options' => array(),
                'description' => 'Width of image on pos panel (px)'
            ),
            $cutom_css,

            array(
                'key'              => 'pos_money',
                'title'             =>  'POS Money List',
                'description'              => 'List of money values in your pos. Separate by "|" character. Example: 10|20|30',
                'default'           => '',
                'options' => array(),
                'type'              => 'text'
            ),
            array(
                'key'              => 'pos_custom_item_discount_amount',
                'title'             =>  'Quick Item Discount Amount',
                'description'              => 'List of quick discount values in your pos. Separate by "|" character. Example: 5|5%|10%',
                'default'           => '',
                'options' => array(),
                'type'              => 'text'
            ),
            array(
                'key'              => 'pos_custom_cart_discount_amount',
                'title'             => 'Quick Cart Discount Amount',
                'description'              => 'List of quick discount values in your pos. Separate by "|" character. Example: 5|5%|10%',
                'default'           => '',
                'options' => array(),
                'type'              => 'text'
            ),
            array(
                'key'              => 'pos_default_checkout_mode',
                'title'             => 'Payment Type',
                'description'              => 'Logic for Payment method type use in POS checkout',
                'default'           => 'multi',
                'type'              => 'select',
                'options' => array(
                    ['name' => 'Split Multi Payment','value' => 'multi'],
                    ['name' => 'Single Payment','value' => 'single'],
                )
            ),

        );

        $setting_payment = array(
            array(
                'key'    => 'payment_methods',
                'title'   =>  'POS Addition Payment Methods',
                'description'    => 'Payment methods for POS beside cash(default)',
                'type'    => 'multicheck',
                'default' => array(),
                'options' => $payment_methods
            ),
        );

        $setting_shipping_methods = array(
            array(
                'key'    => 'shipping_methods',
                'title'   =>  'POS Addition Shipping Methods',
                'description'    => 'Shipping methods for POS ',
                'type'    => 'multicheck',
                'default' => array(),
                'options' => $shipping_methods
            ),
        );

        if($store_id > 0)
        {
            return array(
                'general' => array('title' => 'General','code' => 'general','settings' => $setting_general) ,
                'openpos_payment' => array('title' => 'POS Payment Methods','code' => 'openpos_payment','settings' => $setting_payment) ,
                'openpos_shipping' => array('title' => 'POS Shipping Methods','code' => 'openpos_shipping','settings' => $setting_shipping_methods) ,
                'receipt' => array('title' => 'Receipt','code' => 'receipt','settings' => $setting_receipt) ,
                'pos' => array('title' => 'POS','code' => 'pos','settings' => $setting_pos) ,
            );
        }else{
            return array(
                'general' => array('title' => 'General','code' => 'general','settings' => $setting_general) ,
                'openpos_payment' => array('title' => 'POS Payment Methods','code' => 'openpos_payment','settings' => $setting_payment) ,
                'openpos_shipping' => array('title' => 'POS Shipping Methods','code' => 'openpos_shipping','settings' => $setting_shipping_methods) ,
                'barcode' => array('title' => 'Barcode','code' => 'barcode','settings' => $setting_barcode) ,
                'receipt' => array('title' => 'Receipt','code' => 'receipt','settings' => $setting_receipt) ,
                'pos' => array('title' => 'POS','code' => 'pos','settings' => $setting_pos) ,
            );
        }

    }

    public function getSettings($store_id = 0){
        $settings = $this->getSettingConfig($store_id);
        $result = array();
        foreach($settings as $session_key => $session_settings)
        {
            $result[$session_key] = $session_settings;
            $s_setting = array();
            foreach($session_settings['settings'] as  $setting)
            {
                if(!empty($setting))
                {
                    $s_setting[$setting['key']] = $setting;
                    if($this->getSetting($setting['key'],$session_key,$store_id) != null)
                    {
                        $s_setting[$setting['key']]['default'] = $this->getSetting($setting['key'],$session_key,$store_id);
                    }
                }
            }
            if(!empty($s_setting))
            {
                $result[$session_key]['settings'] = $s_setting;
            }
        }

        return $result;
    }
    public function getDefautSetting($key,$session){
        $settings = $this->getSettingConfig();
        $setting_session = isset($settings[$session]) ? $settings[$session] : false;
        if(!$setting_session)
        {
            return '';
        }
        foreach($settings[$session]['settings'] as $key => $setting)
        {
            if($setting['key'] == $key)
            {
                return isset($setting['default']) ? $setting['default'] : '';
            }
        }
        return null;
    }
    
    public function getConfigSetting($key, $store_id = 0){
        $code = 'config';
		$query = $this->db->query("SELECT value FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `key` = '" . $this->db->escape($key) . "' AND `code` = '".$this->db->escape($code)."'");

		if ($query->num_rows) {
			return $query->row['value'];
		} else {
			return null;	
		}
    }

    public function getLanaConfigData($store_id = 0){
        $this->load->model('tool/image');

        $config_icon = $this->getConfigSetting('config_icon');
        $config_data = [
            'config_name' => $this->getConfigSetting('config_name'),
            // 'config_icon32' => $this->model_tool_image->resize($config_icon, 32, 32),
            // 'config_icon64' => $this->model_tool_image->resize($config_icon, 64, 64),
            // 'config_icon128' => $this->model_tool_image->resize($config_icon, 128, 128),
            // 'config_icon256' => $this->model_tool_image->resize($config_icon, 256, 256),
            // 'config_icon512' => $this->model_tool_image->resize($config_icon, 512, 512)
        ];
        
        return $config_data;
    }

    public function getSetting($key,$session='',$store_id = 0){
        $default_setting_value = null ;
        $code = 'openpos';
        $query = $this->db->query("SELECT value FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `key` = '" . $this->db->escape($session) . "' AND `code` = '".$this->db->escape($code)."'");
        $setting_data = null;
        if ($query->num_rows) {
            $setting_data = $query->row['value'];
        }
        if($setting_data == null)
        {
            return $default_setting_value;
        }
        $setting = json_decode($setting_data,true);
        return isset($setting[$key]) ? $setting[$key] : $default_setting_value;
    }
    public function getAllSettingValues($store_id = 0){
        global $_front_openpos;

        if($store_id == 0 && $this->base_setting)
        {
            return $this->base_setting;
        }
        if($store_id > 0 && $this->store_setting)
        {
            return $this->store_setting;
        }

        $settings = $this->getSettings($store_id);
        $result = array();
        foreach($settings as $session => $session_values)
        {
            foreach($session_values['settings'] as $s)
            {
                $key = $s['key'];
                $value = $s['default'];
                if($_front_openpos)
                {
                    switch ($key)
                    {
                        case 'shipping_methods':
                            $tmp = $this->getAvailableShipments();

                            $shipments = array();
                            foreach($tmp as $shipment)
                            {
                                if(in_array($shipment['code'],$value))
                                {
                                    $tmp_shipment = array(
                                        'code' => $shipment['code'],
                                        'title' => $shipment['name'],
                                        'cost' => 0
                                    );
                                    $shipments[] = $tmp_shipment;
                                }

                            }
                            $value = $shipments;
                            break;
                        case 'pos_money':
                            $value = $this->_formatCash($value);
                            break;
                        case 'pos_custom_item_discount_amount':
                        case 'pos_custom_cart_discount_amount':
                            $value = $this->_formatDiscountAmount($value);
                            break;
                    }


                }
                $result[$key] = $value;
            }
        }
        if($store_id == 0 )
        {
            $this->base_setting = $result;
        }
        if($store_id > 0 )
        {
            $this->store_setting = $result;
        }
        return $result;
    }
    public function getDefaultSettingFile($file){
        $file_path = DIR_SYSTEM.'library/openpos/default/'.$file.'.txt';

        if(file_exists($file_path))
        {
            return file_get_contents($file_path);
        }
        return '';

    }

    public function getProductIdByBarcode($barcode,$store_id = 0)
    {
        try{
            $setting = $this->getAllSettingValues($store_id);
            $field = $setting['pos_barcode_field'];
            $sql = "SELECT product_id FROM " . DB_PREFIX . "product WHERE  `" . $this->db->escape($field) . "` = '" . $this->db->escape($barcode) . "'";
            $query = $this->db->query($sql);
            $product_data = $query->row;
            if(empty($product_data))
            {
                return false;
            }
            return $product_data['product_id'];
        }catch (Exception $e)
        {
            return false;
        }

    }

    public function getProductQty($product_id,$store_id = 0)
    {
        if($store_id == 0)
        {
            $sql = "SELECT quantity FROM " . DB_PREFIX . "product WHERE  `product_id` = '" . (int)$product_id . "'";
            $query = $this->db->query($sql);
            $product_data = $query->row;
            if(empty($product_data))
            {
                throw new Exception('Product do not exist. Please check again');
            }
            return $product_data['quantity'];
        }

        return 0;
    }
    public function getCustomJs(){
        return $this->getDefaultSettingFile('custom_js_track');
    }
    public function allLanguageSupport(){
        $result = array();
        $langs = array(
            "ab",
            "aa",
            "af",
            "ak",
            "sq",
            "am",
            "ar",
            "an",
            "hy",
            "as",
            "av",
            "ae",
            "ay",
            "az",
            "bm",
            "ba",
            "eu",
            "be",
            "bn",
            "bh",
            "bi",
            "bs",
            "br",
            "bg",
            "my",
            "ca",
            "ch",
            "ce",
            "ny",
            "zh",
            "zh-Hans",
            "zh-Hant",
            "cv",
            "kw",
            "co",
            "cr",
            "hr",
            "cs",
            "da",
            "dv",
            "nl",
            "dz",
            "en",
            "eo",
            "et",
            "ee",
            "fo",
            "fj",
            "fi",
            "fr",
            "ff",
            "gl",
            "gd",
            "gv",
            "ka",
            "de",
            "el",
            "kl",
            "gn",
            "gu",
            "ht",
            "ha",
            "he",
            "hz",
            "hi",
            "ho",
            "hu",
            "is",
            "io",
            "ig",
            "id",
            "in",
            "ia",
            "ie",
            "iu",
            "ik",
            "ga",
            "it",
            "ja",
            "jv",
            "kl",
            "kn",
            "kr",
            "ks",
            "kk",
            "km",
            "ki",
            "rw",
            "rn",
            "ky",
            "kv",
            "kg",
            "ko",
            "ku",
            "kj",
            "lo",
            "la",
            "lv",
            "li",
            "ln",
            "lt",
            "lu",
            "lg",
            "lb",
            "gv",
            "mk",
            "mg",
            "ms",
            "ml",
            "mt",
            "mi",
            "mr",
            "mh",
            "mo",
            "mn",
            "na",
            "nv",
            "ng",
            "nd",
            "ne",
            "no",
            "nb",
            "nn",
            "ii",
            "oc",
            "oj",
            "cu",
            "or",
            "om",
            "os",
            "pi",
            "ps",
            "fa",
            "pl",
            "pt",
            "pa",
            "qu",
            "rm",
            "ro",
            "ru",
            "se",
            "sm",
            "sg",
            "sa",
            "sr",
            "sh",
            "st",
            "tn",
            "sn",
            "ii",
            "sd",
            "si",
            "ss",
            "sk",
            "sl",
            "so",
            "nr",
            "es",
            "su",
            "sw",
            "ss",
            "sv",
            "tl",
            "ty",
            "tg",
            "ta",
            "tt",
            "te",
            "th",
            "bo",
            "ti",
            "to",
            "ts",
            "tr",
            "tk",
            "tw",
            "ug",
            "uk",
            "ur",
            "uz",
            "ve",
            "vi",
            "vo",
            "wa",
            "cy",
            "wo",
            "fy",
            "xh",
            "yi",
            "ji",
            "yo",
            "za",
            "zu",
        );
        $result[] = array(
            'value' => '_auto',
            'name' => 'Auto Detect'
        );
        foreach($langs as $lang)
        {
            $result[] = array(
                'value' => $lang,
                'name' => $lang
            );
        }
        return $result;
    }
    public function _formatReceiptTemplate($template){
        $rules = array(
            '[customer_name]' => '<%= customer.name %>',
            '[customer_phone]' => '<%= customer.phone %>',
            '[customer_firstname]' => '<%= customer.firstname %>',
            '[customer_lastname]' => '<%= customer.lastname %>',
            '[sale_person]' => '<%= sale_person_name %>',
            '[created_at]' => '<%= created_at %>',
            '[order_number]' => '<%= order_id %>',
            '[customer_email]' => '<%= customer.email %>',
        );
       $template = htmlspecialchars_decode($template);
       $result = str_replace(array_keys($rules), array_values($rules), html_entity_decode($template));
       return $result;
    }
    public function receiptBodyTemplate(){
        $file = 'receipt_template_body.txt';
        $file_path = DIR_SYSTEM.'library/openpos/default/'.$file;
        
        if(file_exists($file_path))
        {
            $receipt_template = file_get_contents($file_path);
        }else{
            $receipt_template = '<table>
            <tr class="tabletitle">
                <td class="item"><h2>'.$this->language->get('text_op_receipt_table_item').'</h2></td>
                <td class="qty"><h2>'.$this->language->get('text_op_receipt_table_price').'</h2></td>
                <td class="qty"><h2>'.$this->language->get('text_op_receipt_table_qty').'</h2></td>
                <td class="qty"><h2>'.$this->language->get('text_op_receipt_table_discount').'</h2></td>
                <td class="total"><h2>'.$this->language->get('text_op_receipt_table_total').'</h2></td>
            </tr>
            <% items.forEach(function(item){ %>
            <tr class="service">
                <td class="tableitem item-name">
                    <p class="itemtext"><%= item.name %></p>
                    <% if(item.sub_name.length > 0){ %>   
                         
                           <p class="option-item"> <%- item.sub_name  %> </p>
                        
                    <% }; %>
                </td>
                <td class="tableitem item-price"><p class="itemtext"><%= item.price_currency_formatted %></p></td>
                <td class="tableitem item-qty"><p class="itemtext"><%= item.qty %></p></td>
                <td class="tableitem item-discount"><p class="itemtext"><%= item.final_discount_amount_currency_formatted %></p></td>
                <td class="tableitem item-total"><p class="itemtext"><%= item.total_currency_formatted %></p></td>
            </tr>
            <% }); %>
            <tr class="tabletitle">
                <td></td>
                <td class="Rate sub-total-title" colspan="3"><h2>'.$this->language->get('text_op_receipt_total_subtotal').'</h2></td>
                <td class="payment sub-total-amount"><h2><%= sub_total_currency_formatted %></h2></td>
            </tr>
             <% if(shipping_cost > 0) {%>
            <tr class="tabletitle">
                <td></td>
                <td class="Rate shipping-title" colspan="3"><h2>'.$this->language->get('text_op_receipt_total_shipping_total').'</h2></td>
                <td class="payment shipping-amount"><h2><%= shipping_cost_currency_formatted %></h2></td>
            </tr>
             <% } %>
            <% if(final_discount_amount > 0) {%>
            <tr class="tabletitle">
                <td></td>
                <td class="Rate cart-discount-title" colspan="3"><h2>'.$this->language->get('text_op_receipt_total_discount_total').'</h2></td>
                <td class="payment cart-discount-amount"><h2><%= final_discount_amount_currency_formatted %></h2></td>
            </tr>
            <% } %>
            <% if(tax_amount > 0) {%>
            <tr class="tabletitle">
                <td></td>
                <td class="Rate tax-title" colspan="3"><h2>'.$this->language->get('text_op_receipt_total_tax_total').'</h2></td>
                <td class="payment tax-amount"><h2><%= tax_amount_currency_formatted %></h2></td>
            </tr>
            <% } %>
            <tr class="tabletitle">
                <td></td>
                <td class="Rate grand-total-title" colspan="3"><h2>'.$this->language->get('text_op_receipt_total_grand_total').'</h2></td>
                <td class="payment grand-total-amount"><h2><%= grand_total_currency_formatted %></h2></td>
            </tr>
            </table>';
        }
        
        return html_entity_decode($receipt_template);
    }
 

    function _formatCash($cash_str){
        $result = array();
        if($cash_str)
        {
            $tmp_cashes = explode('|',$cash_str);
            foreach($tmp_cashes as $c_str)
            {
                $c_str = trim($c_str);
                if(is_numeric($c_str))
                {
                    $money_value = (float)$c_str;
                    $tmp_money = array(
                        'name' => (float)($money_value),
                        'value' => $money_value
                    );
                    $result[] = $tmp_money;
                }
            }
        }


        return $result;
    }
    function _formatDiscountAmount($discount_str){
        $result = array();
        if($discount_str)
        {
            $tmp_cashes = explode('|',$discount_str);
            foreach($tmp_cashes as $c_str)
            {
                $c_str = trim($c_str);
                $type = 'fixed';
                if(strpos($c_str,'%') > 0)
                {
                    $type = 'percent';
                }
                $number_amount = str_replace('%','',$c_str);
                if(is_numeric($number_amount))
                {
                    $money_value = 1 * (float)$number_amount;
                    $tmp_money = array(
                        'type' => $type,
                        'amount' => $money_value
                    );
                    $result[] = $tmp_money;
                }
            }
        }
        return $result;
    }
    function buildTree($items) {
        $childs = array();
        foreach($items as &$item) $childs[$item['parent_id']][] = &$item;
        unset($item);
        foreach($items as &$item) if (isset($childs[$item['id']]))
            $item['child'] = $childs[$item['id']];
        return $childs[0];
    }

    

}