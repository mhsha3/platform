<?php
namespace Openpos;
class Session {
	private $directory;
	private $register_directory;
    public function __construct()
    {
        $openpos_dir = DIR_STORAGE . 'openpos';
        $this->directory = $openpos_dir . '/sessions';
        $this->register_directory = $openpos_dir . '/registers';
        if(!file_exists($openpos_dir))
        {
            mkdir($openpos_dir);
        }
        if(!file_exists( $this->directory))
        {
            mkdir($this->directory);
        }
        if(!file_exists( $this->register_directory))
        {
            mkdir($this->register_directory);
        }
    }
    public function sessions(){
        $files = array_diff(scandir($this->directory), array('.', '..'));

        return $files;
    }

    public function validate($session_id)
    {
        $file = $this->directory . '/sess_' . basename($session_id);
        if(file_exists($file))
        {
            return true;
        }
        return false;
    }

    public function read($session_id) {
		$file = $this->directory . '/sess_' . basename($session_id);

		if (is_file($file)) {
			$handle = fopen($file, 'r');

			flock($handle, LOCK_SH);

			$data = fread($handle, filesize($file));

			flock($handle, LOCK_UN);

			fclose($handle);

			return json_decode($data,true);
		} else {
			return array();
		}
	}

	public function write($session_id, $data) {
		$file = $this->directory . '/sess_' . basename($session_id);

		$handle = fopen($file, 'w');

		flock($handle, LOCK_EX);

		fwrite($handle, json_encode($data));

		fflush($handle);

		flock($handle, LOCK_UN);

		fclose($handle);

		return true;
	}

    public function save_cart($register_id, $data) {
        $file = $this->register_directory . '/register_' . basename($register_id);

        $handle = fopen($file, 'w');

        flock($handle, LOCK_EX);

        fwrite($handle, json_encode($data));

        fflush($handle);

        flock($handle, LOCK_UN);

        fclose($handle);

        return true;
    }
    public function read_cart($register_id) {
        $file = $this->register_directory . '/register_' . basename($register_id);

        if (is_file($file)) {
            $handle = fopen($file, 'r');

            flock($handle, LOCK_SH);

            $data = fread($handle, filesize($file));

            flock($handle, LOCK_UN);

            fclose($handle);

            return json_decode($data,true);
        } else {
            return array();
        }
    }


    public function destroy($session_id) {
		$file = $this->directory . '/sess_' . basename($session_id);

		if (is_file($file)) {
			unset($file);
		}
	}

	public function clean($session_id)
    {
        $file = $this->directory . '/sess_' . basename($session_id);

        if (is_file($file)) {
            unlink($file);
        }
    }

	public function __destruct() {
		// if (ini_get('session.gc_divisor')) {
		// 	$gc_divisor = ini_get('session.gc_divisor');
		// } else {
		// 	$gc_divisor = 1;
		// }

		// if (ini_get('session.gc_probability')) {
		// 	$gc_probability = ini_get('session.gc_probability');
		// } else {
		// 	$gc_probability = 1;
		// }

		// if ((rand() % $gc_divisor) < $gc_probability) {
		// 	$expire = time() - ini_get('session.gc_maxlifetime');

		// 	$files = glob($this->directory . '/sess_*');

		// 	foreach ($files as $file) {
		// 		if (filemtime($file) < $expire) {
		// 			unlink($file);
		// 		}
		// 	}
		// }
	}
}