<?php


define('PLATFORM_DOMAIN', 'https://' . $_SERVER['HTTP_HOST']);
define('STORES_DOMAIN', 'https://' . $_SERVER['HTTP_HOST']);

define('PLATFORM_DB_HOST', 'localhost');
define('PLATFORM_DB_USER', 'root');
define('PLATFORM_DB_PASS', '');
define('PLATFORM_DB_NAME', 'demo_platform');

define('HOME_DIR', 'C:\xampp\htdocs\platform');
define('STORE_SOURCE', 'C:\xampp\htdocs\platform/site/source/');

// define('PLATFORM_DB_HOST', 'db');
// define('PLATFORM_DB_USER', 'root');
// define('PLATFORM_DB_PASS', 'example');
// define('PLATFORM_DB_NAME', 'lanaapps_system');

