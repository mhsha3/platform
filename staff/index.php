<?php
ob_start();
require_once( './System/Config.php' );
foreach (array_diff(scandir('./System/Helper/'), array('.', '..')) as $key) {
	require_once( './System/Helper/'.$key );
}
require_once( './System/Routes.php' );
function __autoload($class_name) {
      require_once './System/classes/'.$class_name.'.php';
}
$heey = new Heey();
$heey->run();
ob_end_flush();