<?php
function Add_LOG($action,$store_id=0,$note='...'){
   $db = new DB;
   $data = [
     'staff_id'=>$_SESSION['_SSID_'],
     'store_id'=>$store_id,
     'action'=>$action,
     'note'=>$note,
     'created_at'=>date("Y-m-d H:i:s")
    ];
   $db->insert('staff_activity_log',$data);
}

if(isset($_GET['login']) && isset($_POST['username']) && isset($_POST['password'])){
    $db = new DB;
    $data = $db->select('staff',['where'=>['username'=>$_POST['username']]]);
    if($data && password_verify($_POST['password'], $data[0]['password'])){
    	 $_SESSION['_SSID_'] = $data[0]['id'];
    	 $_SESSION['_TOKEN_'] = md5($data[0]['id'].IP());
    	 $_SESSION['_ROOT_'] = $data[0]['root'];
    	 $_SESSION['_STAFF_NAME_'] = $data[0]['name'];
    	 $name = $data[0]['name'];
    	 ADD_LOG('new_login',0,"staff #{$data[0]['id']} make new login");
       echo "
       <script>Swal.fire({icon: 'success',title: 'Hi, $name'});setTimeout(function(){ location.href='dashboard'; },2000);</script>";
    }else{
       echo "<script>Swal.fire({icon: 'error',title: 'Oops...',text: 'Something went wrong!'})</script>";
    }
}

if(!isset($_SESSION['_SSID_'])){exit();}
// islogin -->
$db = new DB;
// Email Sender -->
if(isset($_GET['email_sender'])){
    if(isset($_POST['all_clients'])){
        $data = $db->select('stores');
        foreach($data as $key){
            SendEmail($_POST['email'],$_POST['subject'],$_POST['title'],$_POST['content']);
        }
        $check = 'done';
    }else{
        $check = SendEmail($_POST['email'],$_POST['subject'],$_POST['title'],$_POST['content']);
    }
   if($check){
      echo '<div class="alert alert-success">Done Send Emails</div>';
   }else{
      echo '<div class="alert alert-danger">Error !! in Emails</div>';
   }
}

// add new Staff -->
if(isset($_GET['add_staff'])){
   $check = $db->select('staff',['where'=>['username'=>$_POST['username']]]);
   if(!$check){
       $data = [
       'name'=>$_POST['name'],
       'username'=>$_POST['username'],
       'password'=>password_hash($_POST['password'], PASSWORD_DEFAULT),
       'email'=>$_POST['email'],
       'root'=>implode(',', $_POST['root']),
       'created_at'=>date("Y-m-d H:i:s"),
       'last_active'=>'1997-05-20 08:00:00',
       'status'=>$_POST['status']
       ];
       $id = $db->insert('staff',$data);
       Add_LOG('add_staff','0',"staff id is $id and root is [".implode(',', $_POST['root']).']');
       echo "<script>
       Swal.fire({icon: 'success',title: 'Done Add New Staff #{$id}'});
       setTimeout(function(){ location.reload(); },2000);
       </script>";
   }else{
     echo "<script>Swal.fire({icon: 'error',title: 'Found This user in database !!'});</script>";
   }
}
// Edit Staff -->
if(isset($_GET['edit_staff']) && isset($_GET['id'])){
  show_error();
  $db = new DB;
       $data = [
       'name'=>$_POST['name'],
       'username'=>$_POST['username'],
       'email'=>$_POST['email'],
       'root'=>implode(',', $_POST['root']),
       'status'=>$_POST['status']
       ];
       $id = $_GET['id'];
       $db->update('staff',$data,['id'=>$_GET['id']]);
       Add_LOG('edit_staff','0',"staff id is $id and root is [".implode(',', $_POST['root']).']');
       echo "<script>
       Swal.fire({icon: 'success',title: 'Done Edit Staff #{$id}'});
       setTimeout(function(){ location.href = 'dashboard?p=staff'; },2000);
       </script>";
}
// delete staff -->
if(isset($_GET['remove_staff']) && isset($_GET['id'])){
    $db = new DB;
    $db->delete('staff',['id'=>$_GET['id']]);
    $id = $_GET['id'];
    Add_LOG('remove_staff','0',"staff id is $id");
    echo "<script>
       Swal.fire({icon: 'success',title: 'Done Remove Staff #{$id}'});
       setTimeout(function(){ location.href = 'dashboard?p=staff'; },2000);
       </script>";
}
// change package -->
if(isset($_GET['upgrade_package']) && isset($_POST['package_id']) && isset($_POST['store_id'])){
    $db = new DB;
    $db->update('stores',['package_id'=>$_POST['package_id']],['id'=>$_POST['store_id']]);
    GetStore::query($_POST['store_id'],"UPDATE `oc_setting` SET `value` = '{$_POST['package_id']}' WHERE `oc_setting`.`key` = 'config_store_package_id'");
    Add_LOG('change_package',$_POST['store_id'],"change package to {$_POST['package_id']}");
    echo "<script>
       Swal.fire({icon: 'success',title: 'Done Change Package To #{$_POST['store_id']}'});
       setTimeout(function(){ location.reload(); },1500);
       </script>";
}
// upgeade expire date -->
if(isset($_GET['upgrade_expire_date']) && isset($_POST['expire_date']) && isset($_POST['store_id'])){
    $db = new DB;
    echo $_POST['expire_date'];
    $db->update('stores',['expire_date'=>$_POST['expire_date']],['id'=>$_POST['store_id']]);
    GetStore::query($_POST['store_id'],"UPDATE `oc_setting` SET `value` = '{$_POST['expire_date']}' WHERE `oc_setting`.`key` = 'config_store_expire'");
    Add_LOG('change_expire_date',$_POST['store_id'],"change package to {$_POST['expire_date']}");
    echo "<script>
       Swal.fire({icon: 'success',title: 'Done Change Expire Date To #{$_POST['store_id']}'});
       setTimeout(function(){ location.reload(); },1500);
       </script>";
}

// Email Sender -->
if(isset($_GET['add_platform_updates'])){
    show_error();
    $db = new DB;
    $data = [
        'title'=>$_POST['title'],
        'img'=>$_POST['img'],
        'description'=>$_POST['content'],
        'status'=>1,
        'created_at'=>date("Y-m-d H:i:s"),
        'status'=>$_POST['status'],
        'dolike'=>0,
        'dislike'=>0,
    ];
    $data = $db->insert('platform_updates',$data);
    Add_LOG('add_platform_updates','0',"Done add Post in Updates id is #{$data}");
    echo "<script>
       Swal.fire({icon: 'success',title: 'Done add Post in Updates id is #{$data}'});
       setTimeout(function(){ location.reload(); },1500);
       </script>";
}
// Email Sender -->
if(isset($_GET['edit_platform_updates'])){
    $db = new DB;
    $data = [
        'title'=>$_POST['title'],
        'img'=>$_POST['img'],
        'description'=>$_POST['content'],
        'status'=>$_POST['status']
    ];
    $data = $db->update('platform_updates',$data,['id'=>$_GET['id']]);
    Add_LOG('add_platform_updates','0',"Done Edit Post in Updates id is #{$_GET['id']}");
    echo "<script>
       Swal.fire({icon: 'success',title: 'Done Edit Post in Updates id is #{$_GET['id']}'});
       setTimeout(function(){ location.reload(); },1500);
       </script>";
}
// Email Sender -->
if(isset($_GET['delete_platform_updates'])){
    $db = new DB;
    $data = $db->delete('platform_updates',['id'=>$_GET['id']]);
    Add_LOG('delete_platform_updates','0',"Done Delete Post in Updates id is #{$_GET['id']}");
    echo "<script>
       Swal.fire({icon: 'success',title: 'Done Delete Post in Updates id is #{$_GET['id']}'});
       setTimeout(function(){ location.href = 'dashboard?p=platform_updates'; },1500);
       </script>";
}
// Make Root To Access Store -->
if(isset($_GET['get_access_root'])){
    $password = rand();
    $pass = sha1('YWUr9xdWU' . sha1('YWUr9xdWU' . sha1($password)));
    $date = date('Y-m-d H:i:s');
    GetStore::query($_GET['store_id'],"INSERT INTO `oc_user` (`user_id`, `user_group_id`, `username`, `password`, `salt`, `firstname`, `lastname`, `email`, `image`, `code`, `ip`, `status`, `date_added`) VALUES (NULL, '1', 'LANA_SUPPORT', '{$pass}', 'YWUr9xdWU', 'LANA', 'SUPPORT', 'support@lanaapp.sa', '', '', '', '1', '{$date}');");
    Add_LOG('make_access_root',$_GET['store_id'],"Done access root  by staff id #{$_SESSION['_SSID_']}");
    echo "<script> $('#password').html('{$password}');$('#access_msg').show();</script>";
}
// Done Documents -->
if(isset($_GET['doc_done'])){
    $db = new DB;
    $data = [
        'status'=>3
    ];
    $data = $db->update('documents',$data,['store_id'=>$_GET['id']]);
    Add_LOG('review_documents',$_GET['id'],"Done Edit Post in Updates id is #{$_GET['id']}");
    echo "<script>
       Swal.fire({icon: 'success',title: 'Done Complate Documents store id is #{$_GET['id']}'});
       setTimeout(function(){ location.reload(); },1500);
       </script>";
}
// Refuse Documents -->
if(isset($_GET['doc_return'])){
    $db = new DB;
    $data = [
        'status'=>2
    ];
    $data = $db->update('documents',$data,['store_id'=>$_GET['id']]);
    Add_LOG('review_documents',$_GET['id'],"refuse Documents store id is #{$_GET['id']}");
    echo "<script>
       Swal.fire({icon: 'success',title: 'Done Refuse Documents store id is #{$_GET['id']}'});
       setTimeout(function(){ location.reload(); },1500);
       </script>";
}
// Sms Sender -->
if(isset($_GET['sms_sender'])){
    show_error();
    $phone = '+'.str_replace('+','',$_POST['phone']);
    $data = send_sms($phone,$_POST['content'],true);
    if($data = 'done_sent_phone'){
        Add_LOG('send_sms',$_GET['store_id'],"Send Sms ({$_POST['content']}) to #{$_POST['phone']}");
        echo "<script>
       Swal.fire({icon: 'success',title: 'Done Send Sms'});
       setTimeout(function(){ location.href = 'dashboard?p=sms_sender'; },1500);
       </script>";
    }else{
        echo $data;
    }
}
// Sales Status -->
if(isset($_GET['sales_status'])) {
    $db = new DB;
    $status = (isset($_GET['done'])) ? '2' : '1';
    $data = $db->update('stores',['sales_status'=>$status],['id'=>$_GET['id']]);
    redirect('dashboard?p=sales');
}
// Withdrawal Status -->
if(isset($_GET['withdrawal'])) {
    $db = new DB;
    $data = [
        'store_id'=>$_GET['store_id'],
        'order_id'=>$_GET['order_id'],
        'amount'=>$_GET['amount'],
        'status'=>1,
        'created_at'=>date("Y-m-d H:i:s")
    ];
    $data = $db->insert('withdrawal',$data);
    Add_LOG('withdrawal_order',$_GET['store_id'],"Done paid order #{$_GET['order_id']} amount {$_GET['amount']}");
    redirect($_SERVER['HTTP_REFERER']);
}
// Change Service Status -->
if(isset($_GET['start_services'])) {
    $db = new DB;
    $data = [
        'employee_id'=>$_SESSION['_SSID_'],
        'start_at'=>date("Y-m-d H:i:s"),
        'status'=>1
    ];
    $data = $db->update('services',$data,['id'=>$_GET['id']]);
    Add_LOG('start_service',$_GET['store_id'],"Done Start Make the Service #{$_GET['id']}");
    echo "<script>
       Swal.fire({icon: 'success',title: 'Done Start Service'});
       setTimeout(function(){ location.reload(); },1500);
       </script>";
}
if(isset($_GET['finished_services'])) {
    $db = new DB;
    $data = [
        'employee_id'=>$_SESSION['_SSID_'],
        'finished_at'=>date("Y-m-d H:i:s"),
        'status'=>2
    ];
    $data = $db->update('services',$data,['id'=>$_GET['id']]);
    Add_LOG('finished_service',$_GET['store_id'],"Done Finished the Service #{$_GET['id']}");
    echo "<script>
       Swal.fire({icon: 'success',title: 'Done Finished Service'});
       setTimeout(function(){ location.reload(); },1500);
       </script>";
}
if(isset($_GET['refuse_services'])) {
    $db = new DB;
    $data = [
        'employee_id'=>$_SESSION['_SSID_'],
        'finished_at'=>date("Y-m-d H:i:s"),
        'status'=>3
    ];
    $data = $db->update('services',$data,['id'=>$_GET['id']]);
    Add_LOG('refuse_service',$_GET['store_id'],"Done refuse the Service #{$_GET['id']}");
    echo "<script>
       Swal.fire({icon: 'success',title: 'Done refuse Service'});
       setTimeout(function(){ location.reload(); },1500);
       </script>";
}
// Add Ticket -->
if(isset($_GET['add_ticket'])) {
    show_error();
    $db = new DB;
    $db->query("SET SQL_MODE = ''");
    $data = [
        'store_id'=>$_POST['store_id'],
        'section'=>$_POST['section'],
        'type'=>$_POST['type'],
        'content'=>$_POST['content'],
        'add_by'=>$_SESSION['_SSID_'],
        'status'=>0,
        'employee_id'=>0,
        'created_at'=>date("Y-m-d H:i:s"),
        'started_at'=>'0000-00-00',
        'finished_at'=>'0000-00-00'
    ];
    $data = $db->insert('tickets',$data);
    Add_LOG('add_ticket',$_POST['store_id'],"Done Add Ticket By #{$_SESSION['_SSID_']} ticket id is {$data}");
    echo "<script>
       Swal.fire({icon: 'success',title: 'Done Add Ticket #{$data}'});
       setTimeout(function(){ location.href = 'dashboard?p=ticket&customer_services'; },1500);
       </script>";
}
// Edit Ticket -->
if(isset($_GET['edit_ticket'])) {
    $db = new DB;
    $db->query("SET SQL_MODE = ''");
    $data = [
        'store_id'=>$_POST['store_id'],
        'section'=>$_POST['section'],
        'type'=>$_POST['type'],
        'content'=>$_POST['content'],
        'add_by'=>$_SESSION['_SSID_'],
        'status'=>0,
        'employee_id'=>0,
        'created_at'=>date("Y-m-d H:i:s"),
        'started_at'=>'0000-00-00',
        'finished_at'=>'0000-00-00',
        'edition'=>json_encode([
            'edit_at'=>date("Y-m-d H:i:s"),
            'store_id'=>$_POST['store_id'],
            'old_content'=>$_POST['old_content'],
            'old_type'=>$_POST['old_type'],
            'old_section'=>$_POST['old_section'],
            ])
    ];
    $data = $db->update('tickets',$data,['id'=>$_GET['id']]);
    Add_LOG('edit_ticket',$_POST['store_id'],"Done Edit Ticket By #{$_SESSION['_SSID_']} ticket id is {$_GET['id']}");
    echo "<script>
       Swal.fire({icon: 'success',title: 'Done Edit Ticket #{$data}'});
       setTimeout(function(){ location.href = 'dashboard?p=ticket&customer_services'; },1500);
       </script>";
}
if(isset($_GET['start_ticket'])) {
    $db = new DB;
    $data = [
        'employee_id'=>$_SESSION['_SSID_'],
        'started_at'=>date("Y-m-d H:i:s"),
        'status'=>1
    ];
    $data = $db->update('tickets',$data,['id'=>$_GET['id']]);
    Add_LOG('start_service',$_GET['store_id'],"Done Start Make the Ticket #{$_GET['id']}");
    echo "<script>
       Swal.fire({icon: 'success',title: 'Done Start Ticket'});
       setTimeout(function(){ location.reload(); },1500);
       </script>";
}
if(isset($_GET['finished_ticket'])) {
    $db = new DB;
    $data = [
       // 'employee_id'=>$_SESSION['_SSID_'],
        'finished_at'=>date("Y-m-d H:i:s"),
        'status'=>2
    ];
    $data = $db->update('tickets',$data,['id'=>$_GET['id']]);
    Add_LOG('finished_service',$_GET['store_id'],"Done Finished the Ticket #{$_GET['id']}");
    echo "<script>
       Swal.fire({icon: 'success',title: 'Done Finished Ticket'});
       setTimeout(function(){ location.reload(); },1500);
       </script>";
}
// GET Ajax Data -->
if(isset($_GET['ajax_tickets'])){
  $db = new DB;
  if($_GET['ajax_tickets'] == 'customer_services'){
    $tickets = $db->select('tickets');
  }else{
    $tickets = $db->select('tickets',['where'=>['section'=>$_GET['ajax_tickets']]]);
  }
  echo json_encode(['data'=>$tickets],JSON_UNESCAPED_UNICODE);
}
// Add Notes -->
if(isset($_GET['addnote'])){
    show_error();
    $db = new DB;
    $data = [
        'store_id'=>$_GET['store_id'],
        'employee_id'=>$_SESSION['_SSID_'],
        'content'=>$_POST['content'],
        'section'=> $_GET['s']?? '', // section
        'tags' => $_GET['a'] ?? '',  // Tag
        'created_at'=>date("Y-m-d H:i:s"),
    ];
    $data = $db->insert('notes',$data);
    Add_LOG('add_notes',$_GET['store_id'],"Done add Note id is #{$data} and staff is {$_SESSION['_SSID_']}");
    echo "<script>
       Swal.fire({icon: 'success',title: 'Done add Note id is #{$data}'});
       setTimeout(function(){ location.reload(); },1500);
       </script>";
}

// Send Notifications -->

if(isset($_GET['send_notifications'])){
    show_error();
    GetStore::query($_POST['store_id'],"INSERT INTO `oc_store_notifications` (`notification_id`, `title`, `tag`, `content`, `added_at`, `viewed_at`) VALUES (NULL, '".$_POST['title']."', '".$_POST['tag']."', '".$_POST['content']."', '".date("Y-m-d H:i:s")."', '');");
        Add_LOG('add_notifications',$_POST['store_id'],"Done add Notifications content is {$_POST['content']}");
    echo "<script>
       Swal.fire({icon: 'success',title: 'Done add Notifications to Client'});
       setTimeout(function(){ location.reload(); },1500);
       </script>";
}
// change package -->
if(isset($_GET['add_new_domain']) && isset($_GET['store_id']) && isset($_POST['domain'])){
    show_error();
    $db = new DB;
    $db->update('stores',['domain'=>$_POST['domain']],['id'=>$_GET['store_id']]);
    Add_LOG('add_domain',$_GET['store_id'],"add domain");
    echo "<script>
       Swal.fire({icon: 'success',title: 'Done Add new Domain To #{$_GET['store_id']}'});
       setTimeout(function(){ location.reload(); },1500);
       </script>";
}
// Add  FAQ -->
if(isset($_GET['add_faq'])){
    show_error();
    $db = new DB;
    $data = [
        'title'=>$_POST['title'],
        'content'=>$_POST['content'],
        'status'=>1,
        'sort_by'=>$_POST['sort_by'],
        'created_at'=>date("Y-m-d H:i:s"),
    ];
    $data = $db->insert('faq_platform',$data);
    Add_LOG('add_platform','0',"Done add Post in FAQ id is #{$data}");
    echo "<script>
       Swal.fire({icon: 'success',title: 'Done add Post in FAQ id is #{$data}'});
       setTimeout(function(){ location.reload(); },1500);
       </script>";
}
?>
