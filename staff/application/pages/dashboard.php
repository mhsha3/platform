<?php
if(!IsLogin()){redirect();}
Content('header');
if(isset($_GET['p']) && !empty($_GET['p'])){
   if(file_exists('./application/content/'.$_GET['p'].'.php') && Root($_GET['p']) ){
     Content($_GET['p']);
   }else{
     Content('404');
   }
}else{
     Content('home');
}
Content('footer');
?>