<?php 
if(IsLogin()){redirect('dashboard');}
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login - LnaSpace</title>
    <link href="https://bootstrap.rtlcss.com/docs/4.2/dist/css/rtl/bootstrap.css" rel="stylesheet">
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }
      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link href="https://getbootstrap.com/docs/4.5/examples/sign-in/signin.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
  </head>
  <body class="text-center">
<form class="form-signin">
  <img class="mb-4" src="application/assets/logo.png" alt="" width="130" height="40">
  <div id="msg"></div>
  <input type="text" name="username" class="form-control" placeholder="UserName" required autofocus>
  <input type="password" name="password" class="form-control" placeholder="Password" required>
  <button class="btn btn-lg btn-primary btn-block submit" type="submit"> Login</button>
  <p class="mt-5 mb-3 text-muted">LanaApp&copy; <?php __(date('Y')); ?></p>
</form>
<script type="text/javascript">
	$('form').submit(function(e){
        e.preventDefault();
        $.post('requests?login',$(this).serialize(),function(data){
          $('#msg').html(data);
        });
    });
</script>
</body>
</html>