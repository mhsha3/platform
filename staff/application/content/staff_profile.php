<?php
$db = new DB;
$staff = $db->select('staff',['where'=>['id'=>$_GET['id']]]);
$root = $_SESSION['_ALL_ROOT_'];
$logs = [];
if($_SESSION['_SSID_'] == 1 || $_SESSION['_SSID_'] == 25 ){
    $logs = $db->select('staff_activity_log',['where'=>['staff_id'=>$_GET['id']]]);
}
?>
<link rel="stylesheet" href="application/assets/multi-select.css"/>
<script src="application/assets/multi-select.js"></script>
<div class="float-right m-1">
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add">Edit Staff</button>
<button class="btn btn-danger" id="remove_staff">Remove Staff</button>
</div>

<div class="modal fade" id="add" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Edit Staff</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div id="editmsg"></div>
<form id="addform">
  <div class="form-group">
    <label>Name</label>
    <input type="text" required class="form-control" value="<?php echo $staff[0]['name']; ?>" name="name" placeholder="Name">
  </div>
  <div class="form-group">
    <label>Username</label>
    <input type="text" required class="form-control" value="<?php echo $staff[0]['username']; ?>" name="username" placeholder="Username">
  </div>
  <div class="form-group">
    <label>Email</label>
    <input type="email" required class="form-control" value="<?php echo $staff[0]['email']; ?>" name="email" placeholder="Email">
  </div>
  <div class="form-group">
    <label>Add Staff Root</label>
    <select multiple required class="form-control" name="root[]" id="selectadd">
         <?php
         foreach ($root as $key => $value) {
          if(in_array($key,explode(',', $staff[0]['root']))){
            echo '<option selected value="'.$key.'">'.$value.'</option>';
          }else{
            echo '<option value="'.$key.'">'.$value.'</option>';
          }
         }
         ?>
    </select>
  </div>
  <div class="form-group">
    <label>Status</label>
    <select required class="form-control" name="status">
      <?php
      if($staff[0]['status'] == 1){
         echo '<option  selected value="1">Avaliable</option>';
         echo '<option value="0">UnAvaliable</option>';
      }else{
         echo '<option value="1">Avaliable</option>';
         echo '<option selected value="0">UnAvaliable</option>';
      }
        ?>
    </select>
  </div>
  <button class="btn btn-info" type="submit">Edit</button>
</form>
      </div>
    </div>
  </div>
</div>
        <div id="msg"></div>
<?php
if($_SESSION['_SSID_'] == 1 || $_SESSION['_SSID_'] == 25 ){
?>
        <h1>Staff Tracking</h1>
<table id="staff_logs" class="table-hover table-striped bg-white border shadow p-1" style="width:100%">
    <thead>
        <tr>
        	  <td>id</td>
            <td>Staff id</td>
            <td>Store id</td>
            <td>Action</td>
            <td>note</td>
            <td>Date</td>
        </tr>
    </thead>
</table>
<?php } ?>
<script type="text/javascript">
$(document).ready(function(){
  $('#remove_staff').click(function(){
    if(confirm('Are You Sure Remove This Staff !!')){
    $.post('requests?remove_staff&id=<?php echo $_GET['id'];?>',function(data){
            $('#msg').html(data);
    });
    }
  });
	$('#selectadd').multiSelect();
	$('#addform').submit(function(e){
		e.preventDefault();
		$.post('requests?edit_staff&id=<?php echo $_GET['id'];?>',$(this).serialize(),function(data){
            $('#editmsg').html(data);
		});
	});
    $('#staff_logs').DataTable({
        <?php if($_SESSION['_SSID_'] == 1){?>
            dom: 'Blfrtip',
            "buttons": [
                    {
                        "extend": "csv",
                        "text": "Export as CSV",
                        "filename": "Stores Report Name",
                        "className": "btn btn-success",
                        "charset": "utf-8",
                        "bom": "true",
                        init: function(api, node, config) {
                            $(node).removeClass("btn-success");
                        }
                    },
                    {
                        "extend": "pdf",
                        "text": "Export as PDF",
                        "filename": "Stores Report Name",
                        "className": "btn btn-info",
                        "charset": "utf-8",
                        "bom": "true",
                        init: function(api, node, config) {
                            $(node).removeClass("btn-info");
                        }
                    }
                ], <?php }?>
      "data":<?php echo json_encode($logs,JSON_UNESCAPED_UNICODE);?>,
      "columns": [
            { "data": "id" },
            { "data": "staff_id" },
            { "data": "store_id" },
            { "data": "action" },
            { "data": "note" },
            { "data": "created_at" }
        ]
    });
} );
</script>
