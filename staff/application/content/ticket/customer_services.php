<?php
$db = new DB;
$total_finished = $db->query('SELECT COUNT(*) as total FROM `tickets` where status = 2')[0]['total'];
$total_pendding = $db->query('SELECT COUNT(*) as total FROM `tickets` where status = 1')[0]['total'];
$total_new = $db->query('SELECT COUNT(*) as total FROM `tickets` where status = 0')[0]['total'];
//$all_stores = $db->select('tickets');
?>
<div class="row">
    <div class="col">
        <div class="card text-white bg-info mb-3">
            <div class="card-header">Total New</div>
            <div class="card-body">
                <h5 class="card-title"><?php echo $total_new; ?></h5>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card text-white bg-success mb-3">
            <div class="card-header">Total Finished</div>
            <div class="card-body">
                <h5 class="card-title"><?php echo $total_finished; ?></h5>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card text-white bg-primary mb-3">
            <div class="card-header">Total Pendding</div>
            <div class="card-body">
                <h5 class="card-title"><?php echo $total_pendding; ?></h5>
            </div>
        </div>
    </div>
</div>
<button class="btn btn-primary m-3" id="ajax_off">Disable Auto Reload</button>
<table id="sales" class="table-hover table-striped bg-white border shadow p-1" style="width:100%">
    <thead>
    <tr>
        <td>Ticket ID</td>
        <td>Store ID</td>
        <td>Type</td>
        <td>Section</td>
        <td>Status</td>
        <td>Add in</td>
        <td>Action</td>
    </tr>
    </thead>
</table>

<script type="text/javascript">
    $(document).ready( function () {
        var table = $('#sales').DataTable({
           // "data":<?php //echo json_encode($all_stores,JSON_UNESCAPED_UNICODE);?>,
            "ajax": "requests?ajax_tickets=customer_services",
            "bPaginate":true,
            "bProcessing": true,
            "columns": [
                {"render": function(data,type,row){
                        return '<a href="dashboard?p=ticket&show_ticket&id='+row['id']+'">'+row['id']+'</a>';
                }},
                {"render": function(data,type,row){
                        return '<a href="dashboard?p=store&id='+row['store_id']+'">'+row['store_id']+'</a>';
                }},
                {"render": function(data,type,row){
                    var x;
                        if(row['type'] == 1){
                            x = '<div class="badge badge-warning"> Problem </div>';
                        }
                        if(row['type'] == 2){
                            x = '<div class="badge badge-warning"> Service </div>';
                        }
                        if(row['type'] == 3){
                            x = '<div class="badge badge-warning"> Inquiry </div>';
                        }
                        return x;
                    }},
                    { "data": "section" },
                {"render": function(data,type,row){
                    var x;
                        if(row['status'] == 0){
                            x = '<div class="badge badge-info"> New </div>';
                        }
                        if(row['status'] == 1){
                            x = '<div class="badge badge-primary"> Working </div>';
                        }
                        if(row['status'] == 2){
                            x = '<div class="badge badge-success"> Finished </div>';
                        }
                        return x;
                    }},
                    { "data": "created_at" },
                {"render": function(data,type,row){
                        return '<a class="btn btn-success" href="dashboard?p=ticket&edit_ticket&id='+row['id']+'"><i class="fa fa-edit"></i></a>';
                }},
            ]
        });

        localStorage.setItem('auto_ajax', 'on');
        $('#ajax_off').click(function(){
            localStorage.setItem('auto_ajax', 'off');
        });
setInterval( function () {
    if(localStorage.getItem('auto_ajax') == 'on'){
        table.ajax.reload(null, false);
    }
}, 10000 );

    } );
</script>
