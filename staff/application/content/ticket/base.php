<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<div class="bg-white shadow border p-4">
	<div id="msg"></div>
<form>
  <div class="form-group">
     <label>Store ID</label>
     <input type="number" required name="store_id" value="" class="form-control" placeholder="Store ID">
  </div>
  <div class="form-group">
     <label>To Section</label>
     <select class="form-control" name="section" required>
        <option value="sales">Sales</option>
        <option value="support">Support</option>
        <option value="finance">Finance</option>
        <option value="design">Design</option>
     </select>
  </div>
  <div class="form-group">
     <label>TicKet Type</label>
     <select class="form-control" name="type" required>
        <option value="1">Problem</option>
        <option value="2">Service</option>
        <option value="3">Inquiry</option>
     </select>
  </div>
  <div class="form-group">
       <label>TicKet Content</label>
       <textarea id="summernote" name="content" required ></textarea>
  </div>
  <button type="submit" class="btn btn-primary">Send</button>
</form>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('#summernote').summernote({
        placeholder: 'Ticket Content',
        tabsize: 2,
        height: 100
      });
  $('form').submit(function(e){
    e.preventDefault();
      $.post('requests?add_ticket',$(this).serialize(),function(data){
  	     $('#msg').html(data);
     });
  });
});
</script>
