<?php
$db = new DB;
$id = $_GET['id'] ?? '0';
$data = $db->select('tickets',['where'=>['id'=>$id]])[0];
$find = ['finance','design'];
$to = ['finance_orders','design_order'];

if(!CheckRoot('customer_services')){
if(!CheckRoot(str_replace($find,$to,$data['section']))){
    redirect('dashboard');
    exit();
}
}
$notes = $db->select('notes',['where'=>['tags'=>'comment_ticket_'.$_GET['id'],'store_id'=>$data['store_id']]]);

?>
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<div id="msg"></div>
<h1 class="text-center">Ticket #<?php echo $data['id']; ?></h1>
<div class="">
    <button id="refuse" class="btn btn-danger m-2">Mohab</button>
    <table class="table table-hover table-striped">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Details</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Ticket Type</td>
            <td><?php 
if($data['type'] == 1){
    $x = '<div class="badge badge-danger">Problem</div>';
}elseif($data['type'] == 2){
    $x = '<div class="badge badge-info">Order</div>';
}elseif($data['type'] == 3){
    $x = '<div class="badge badge-primary">Inquiry</div>';
}else{
    $x = $data['type'];
}
            echo $x;
            
            ?></td>
        </tr>
        <tr>
            <td>Section</td>
            <td><?php echo $data['section']; ?></td>
        </tr>
        <tr>
            <td>Store ID</td>
            <td><a href="dashboard?p=store&id=<?php echo $data['store_id']; ?>"><?php echo $data['store_id']; ?></a></td>
        </tr>
        <tr>
            <td>Content</td>
            <td><textarea id="summernote"><?php echo $data['content']; ?></textarea></td>
        </tr>
        <tr>
            <td>Edition</td>
            <td>
                <?php
                foreach (json_decode($data['edition']) as $key => $value){
                    echo '<b>'.str_replace('_',' ',$key)."</b>: <span>$value</span><br>";
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>Created at</td>
            <td><?php echo $data['created_at']; ?></td>
        </tr>
        <tr>
            <td>Employee</td>
            <td><?php echo $data['employee_id']; ?></td>
        </tr>
        <tr>
            <td>Started at</td>
            <td><?php echo $data['started_at']; ?></td>
        </tr>
        <tr>
            <td>Finished at</td>
            <td><?php echo $data['finished_at']; ?></td>
        </tr>
        </tbody>
    </table>
    <div class="text-center">
        <?php
        if($data['status'] < 2){
            if($data['status'] != 1){
                echo '<button id="start" class="btn btn-info m-2">Working Ticket</button>';
            }
            echo '<button id="finished" class="btn btn-success m-2">Finished Ticket</button>';
        }elseif($data['status'] == 1){
            echo '<h3 class="text-danger text-center">This Ticket is Working ...</h3>';
        }else{
            echo '<h3 class="text-success text-center">This Ticket is Finished ...</h3>';
        }
        ?>
    </div>
</div>
<br>
<form id="form" class="bg-dark p-4">
<div class="form-group">
    <label class="text-white">Add Notes</label>
    <textarea class="form-control" name="content" rows="3"></textarea>
</div>
<button type="submit" class="btn btn-primary">Add Notes</button>
</form>
<br>
<table id="sales_note" class="table-hover table-striped bg-white border shadow p-1" style="width:100%">
    <thead>
    <tr>
        <td>ID</td>
        <td>Employee</td>
        <td>Content</td>
        <td>Date</td>
    </tr>
    </thead>
</table>
<script>
  $('#summernote').summernote({
        placeholder: 'Ticket Content',
        tabsize: 2,
        height: 100
      });
    $('#start').click(function(){
        $.post('requests?start_ticket&store_id=<?php echo $data['store_id']; ?>&id=<?php echo $_GET['id']; ?>',function (data) {
            $('#msg').html(data);
        });
    });
    $('#finished').click(function(){
        $.post('requests?finished_ticket&store_id=<?php echo $data['store_id']; ?>&id=<?php echo $_GET['id']; ?>',function (data) {
            $('#msg').html(data);
        });
    });
    /*
    $('#refuse').click(function(){
        $.post('requests?refuse_services&store_id=<?php echo $data['store_id']; ?>&id=<?php echo $_GET['id']; ?>',function (data) {
            $('#msg').html(data); 
        });
    });*/ 
    
    $('#form').submit(function (e){
        e.preventDefault();
        $.post('requests?addnote&s=tickets&a=comment_ticket_<?php echo $_GET['id']; ?>&store_id=<?php echo $data['store_id']; ?>',$(this).serialize(),function(data){
            $('#msg').html(data);
        });
    });

    $('#sales_note').DataTable({
            "data":<?php echo json_encode($notes,JSON_UNESCAPED_UNICODE);?>,
            "columns": [
                { "data": "id" },
                { "data": "employee_id" },
                { "data": "content" },
                { "data": "created_at" }
            ]
        });
</script>