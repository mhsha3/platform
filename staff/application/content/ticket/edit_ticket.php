<?php 
show_error();
$id = $_GET['id'] ?? '0';
$db = new DB;
$data = $db->select('tickets',['where'=>['id'=>$id]])[0];
if($data['status'] == 1){
    echo 'Sorry SomeBody Working Now On this Ticket Please Contact The Supervisor!!';
    exit();
}
?>
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<div class="bg-white shadow border p-4">
	<div id="msg"></div>
<form>
<input type="hidden" name='old_content' value="<?php echo $data['content'] ?? ''; ?>"/>
<input type="hidden" name='old_type' value="<?php echo $data['type'] ?? ''; ?>"/>
<input type="hidden" name='old_section' value="<?php echo $data['section'] ?? ''; ?>"/>
<input type="hidden" name='old_store_id' value="<?php echo $data['store_id'] ?? ''; ?>"/>
  <div class="form-group">
     <label>Store ID</label>
     <input type="number" required name="store_id" value="<?php echo $data['store_id'] ?? ''; ?>" class="form-control" placeholder="Store ID">
  </div>
  <div class="form-group">
     <label>To Section</label>
     <select class="form-control" name="section" required>
        <option value="sales" <?php echo ($data['section'] == 'sales') ? 'selected' : '';?> >Sales</option>
        <option value="support" <?php echo ($data['section'] == 'support') ? 'selected' : '';?>>Support</option>
        <option value="finance" <?php echo ($data['section'] == 'finance') ? 'selected' : '';?>>Finance</option>
        <option value="design" <?php echo ($data['section'] == 'design') ? 'selected' : '';?>>Design</option>
     </select>
  </div>
  <div class="form-group">
     <label>TicKet Type</label>
     <select class="form-control" name="type" required>
        <option value="1" <?php echo ($data['type'] == '1') ? 'selected' : '';?>>Problem</option>
        <option value="2" <?php echo ($data['type'] == '2') ? 'selected' : '';?>>Service</option>
        <option value="3" <?php echo ($data['type'] == '3') ? 'selected' : '';?>>Inquiry</option>
     </select>
  </div>
  <div class="form-group">
       <label>TicKet Content</label>
       <textarea id="summernote" name="content" required ><?php echo $data['content'] ?? ''; ?></textarea>
  </div>
  <button type="submit" class="btn btn-primary">Edit</button>
</form>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('#summernote').summernote({
        placeholder: 'Ticket Content',
        tabsize: 2,
        height: 100
      });
  $('form').submit(function(e){
    e.preventDefault();
      $.post('requests?edit_ticket&id=<?php echo $_GET['id'];?>',$(this).serialize(),function(data){
  	     $('#msg').html(data);
     });
  });
});
</script>
