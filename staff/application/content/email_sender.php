<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<div class="bg-white shadow border p-4">
	<div id="msg"></div>
<form>
  <div class="form-group">
     <label>Enter Client Email</label>
     <input type="email" required name="email" value="<?php echo ($_GET['email']) ?? ''; ?>" class="form-control" placeholder="Enter Client Email">
  </div>
  <div class="form-group">
     <label>Subject</label>
     <input type="text" required name="subject" class="form-control" placeholder="Subject">
  </div>
  <div class="form-group">
     <label>Title</label>
     <input type="text" name="title" class="form-control" placeholder="Title">
  </div>
  <div class="form-group">
     <label>Content</label>
       <textarea id="summernote" name="content"></textarea>
  </div>
  <div class="form-group form-check">
    <input type="checkbox" name="all_clients" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label text-danger" for="exampleCheck1">All Clients</label>
  </div>
  <button type="submit" class="btn btn-primary">Send</button>
</form>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('#summernote').summernote({
        placeholder: 'Email Content',
        tabsize: 2,
        height: 100
      });
  $('form').submit(function(e){
    e.preventDefault();
      $.post('requests?email_sender',$(this).serialize(),function(data){
  	     $('#msg').html(data);
     });
  });
});
</script>
