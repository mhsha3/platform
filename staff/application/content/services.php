<?php
$service = [
    'domain_order',
    'design_order',
    'other_order',
    'all_order',
];
$Root = explode(',',$_SESSION['_ROOT_']);

if(isset($_GET['type']) && in_array($_GET['type'],$service)){
    Content("services/{$_GET['type']}");
    exit();
}elseif(isset($_GET['show'])){
    Content("services/service_show");
}else{
    echo '<ul class="list-group">';
    foreach (array_intersect($service, $Root) as $key) {
        echo '<li class="list-group-item"><a href="dashboard?p=services&type=' . $key . '">' . str_replace('_', ' ', $key) . '</a></li>';
    }
    echo '</ul>';
}
?>
