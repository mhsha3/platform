<?php
show_error();
if(!isset($_GET['id']) || !is_numeric($_GET['id'])){
    echo '<div class="alert alert-danger">Error Not Found Store </div>';
    exit();
}
$db = new DB;
$check = $db->select('stores',['where'=>['id'=>$_GET['id']]])[0];
if(!$check){
	echo '<div class="alert alert-danger">Error Not Found Store </div>';
    exit();
}
if($check['domain']){
    $domain = 'https://'.$check['domain'];
}else{
    $domain = 'https://lnasa.space/'.$check['path'];
}
$package = [
    '1'=>'Demo Package',
    '2'=>'Paid Package',
    '10'=>'Monshaat Package',
    ];
$_SESSION['_PACKAGE_'] = $package;
?>
<div id="msg"></div>
<div class="row">
	<div class="col-md-4">
		<div class="card border" style="height:210px">
		  <div class="card-header bg-dark text-white">
		    Store Profile
		  </div>
		  <div class="card-body text-center">
		     <img src="https://lnasa.space/<?php echo $check['path'].'/image/'.GetStore::GetSetting($_GET['id'],"config_logo"); ?>" style="max-width:43px;">
		     <h2>#<b><?php echo (int)$_GET['id']; ?></b></h2>
              <h4><?php echo GetStore::GetSetting($_GET['id'],"config_name"); ?></h4>
              <h4><a href="<?php echo $domain ?>"><?php echo $domain ?></a></h4>
		  </div>
		</div>
	</div>
	<div class="col-md-8">
		<div class="card border" style="height:210px">
		  <div class="card-header bg-dark text-white">
		    Basic Information To Store #<?php echo (int)$_GET['id']; ?>
		    <div class="float-right">
              <a href="dashboard?p=sms_sender&phone=<?php echo $check['phone'];  ?>" class="btn btn-success btn-xs">Send SMS</a>
              <a href="dashboard?p=email_sender&email=<?php echo $check['email'];  ?>" class="btn btn-primary btn-xs">Send Email</a>
              <a href="dashboard?p=notifications&id=<?php echo $_GET['id']; ?>" class="btn btn-danger btn-xs">Send Notifications</a>
		    </div>
		  </div>
		  <div class="card-body">
		     <div class="row">
		     	<div class="col">
		     		<h5><b>Firstname</b> : <?php echo $check['firstname'];  ?> </h5>
		     		<h5><b>Lastname</b> : <?php echo $check['lastname'];  ?> </h5>
		     		<h5><b>Phone</b> : <?php echo $check['phone'];  ?> </h5>
                    <h5><b>Email</b> : <?php echo $check['email'];  ?> </h5>
                    <h5><b>DropShipping</b> :
                        <?php
                         if(GetStore::GetSetting($_GET['id'],"config_store_lanadropshipping")){
                             echo '<div class="badge badge-warning">Yes</div>';
                         }else{
                             echo '<div class="badge badge-primary">No</div>';
                         }
                        ?>
                    </h5>
		     	</div>
		     	<div class="col">
		     		<h5><b>Store Name</b> : <?php echo GetStore::GetSetting($_GET['id'],"config_name");  ?> </h5>
		     		<h5><b>Package</b> :
		     			<div class="badge badge-primary p-1"> <?php echo $_SESSION['_PACKAGE_'][GetStore::GetSetting($_GET['id'],"config_store_package_id")]?? 'unKnow ... ';  ?></div>
		     		</h5>
		     		<h5><b>Expire Date</b> : <?php echo GetStore::GetSetting($_GET['id'],"config_store_expire");  ?> </h5>
		     		<h5><b>Created Date</b> : <?php echo $check['created_at'];  ?> </h5>
		     	</div>
		     </div>
		  </div>
		</div>
	</div>
</div>
<br>
<div class="bg-white p-3">
<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item" role="presentation">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Support</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Sales</a>
  </li>
    <li class="nav-item" role="presentation">
        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#finance" role="tab" aria-controls="contact" aria-selected="false">Finance</a>
    </li>
    <li class="nav-item" role="presentation">
        <a class="nav-link" id="documents-tab" data-toggle="tab" href="#documents" role="tab" aria-controls="contact" aria-selected="false">Documents</a>
    </li>
</ul>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
    <?php
       if(CheckRoot('support')){
       	    Content('support/base');
       }else{
          echo '<div class="text-center p-5">No Acccess This Side !!</div>';
       }
    ?>
  </div>
  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
  	<?php
       if(CheckRoot('sales')){
       	    Content('sales/base');
       }else{
          echo '<div class="text-center p-5">No Acccess This Side !!</div>';
       }
    ?>
  </div>
    <div class="tab-pane fade" id="finance" role="tabpanel" aria-labelledby="contact-tab">
        <?php
        if(CheckRoot('finance_orders')){
            Content('finance/base');
        }else{
            echo '<div class="text-center p-5">No Acccess This Side !!</div>';
        }
        ?>
    </div>
    <div class="tab-pane fade" id="documents" role="tabpanel" aria-labelledby="contact-tab">
        <?php
        if(CheckRoot('documents')){
            Content('store_documents');
        }else{
            echo '<div class="text-center p-5">No Acccess This Side !!</div>';
        }
        ?>
    </div>
</div>
</div>
