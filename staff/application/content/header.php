<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Eng.Mohab">
    <link rel="icon" type="image/png" sizes="16x16" href="application/assets/favicon.png">
    <title>Lna - Dashboard</title>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link href="application/assets/css/style.min.css" rel="stylesheet">
    <script src="application/assets/libs/jquery/dist/jquery.min.js"></script>
    <script src="application/assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="application/assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="//cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="application/assets/extra-libs/sparkline/sparkline.js"></script>
    <script src="application/assets/js/waves.js"></script>
    <script src="application/assets/js/sidebarmenu.js"></script>
    <script src="application/assets/js/custom.min.js"></script></head>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper" data-navbarbg="skin6" data-theme="light" data-layout="vertical" data-sidebartype="full" data-boxed-layout="full">
        <header class="topbar" data-navbarbg="skin6">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <div class="navbar-header" data-logobg="skin5">
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)">
                        <i class="ti-menu ti-close"></i>
                    </a>
                    <div class="navbar-brand text-center">
                        <a href="dashboard" class="logo">
                            <span class="logo-text">
                                <img src="application/assets/logo.png" width="50%" class="dark-logo" />
                                <img src="application/assets/logo.png" width="50%" class="light-logo" />
                            </span>
                        </a>
                    </div>
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="ti-more"></i>
                    </a>
                </div>
                <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin6">
                    <ul class="navbar-nav float-left mr-auto">
                        <h4 class="pl-4"> Hi, <?php echo $_SESSION['_STAFF_NAME_']; ?>! </h4>
                    </ul>
                    <ul class="navbar-nav float-right">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="https://via.placeholder.com/300/09f/fff.png?text=<?php echo $_SESSION['_STAFF_NAME_'];?>" alt="user" class="rounded-circle" width="31"></a>
                            <div class="dropdown-menu dropdown-menu-right user-dd animated">
                                <a class="dropdown-item" href="dashboard?logout">
                                <i class="ti-power-off m-r-5 m-l-5"></i> LogOut
                            </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <aside class="left-sidebar" data-sidebarbg="skin5">
            <div class="scroll-sidebar">
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="sidebar-item">
                          <a class="sidebar-link waves-effect waves-dark sidebar-link" href="dashboard" aria-expanded="false">
                                <i class="mdi mdi-av-timer"></i>
                                <span class="hide-menu">Dashboard</span>
                            </a>
                        </li>
                        <?php if(CheckRoot('all_stores')){ ?>
                            <li class="sidebar-item">
                                <a class="sidebar-link waves-effect waves-dark sidebar-link" href="dashboard?p=all_stores">
                                    <i class="mdi mdi-store"></i>
                                    <span class="hide-menu">All Stores</span>
                                </a>
                            </li>
                        <? }?>
                        <?php if(CheckRoot('sales')){ ?>
                            <li class="sidebar-item">
                                <a class="sidebar-link waves-effect waves-dark sidebar-link" href="dashboard?p=sales">
                                    <i class="mdi mdi-store"></i>
                                    <span class="hide-menu">Sales</span>
                                </a>
                            </li>
                        <? }?>
                        <?php if(CheckRoot('ticket')){ ?>
                            <li class="sidebar-item">
                                <a class="sidebar-link waves-effect waves-dark sidebar-link" href="dashboard?p=ticket">
                                    <i class="fa fa-tag"></i>
                                    <span class="hide-menu">Tickets</span>
                                </a>
                            </li>
                        <? }?>
                        <?php if(CheckRoot('email_sender')){ ?>
                            <li class="sidebar-item">
                                <a class="sidebar-link waves-effect waves-dark sidebar-link" href="dashboard?p=email_sender">
                                    <i class="mdi mdi-email"></i>
                                    <span class="hide-menu">Emails Sender</span>
                                </a>
                            </li>
                        <? }?>
                        <?php if(CheckRoot('sms_sender')){ ?>
                            <li class="sidebar-item">
                                <a class="sidebar-link waves-effect waves-dark sidebar-link" href="dashboard?p=sms_sender">
                                    <i class="mdi mdi-phone"></i>
                                    <span class="hide-menu">Sms Sender</span>
                                </a>
                            </li>
                        <? }?>
                        <?php if(CheckRoot('services')){ ?>
                            <li class="sidebar-item">
                                <a class="sidebar-link waves-effect waves-dark sidebar-link" href="dashboard?p=services">
                                    <i class="fa fa-cubes"></i>
                                    <span class="hide-menu">Services</span>
                                </a>
                            </li>
                        <? }?>
                        <?php if(CheckRoot('documents')){ ?>
                            <li class="sidebar-item">
                                <a class="sidebar-link waves-effect waves-dark sidebar-link" href="dashboard?p=documents">
                                    <i class="mdi mdi-file"></i>
                                    <span class="hide-menu">Documents</span>
                                </a>
                            </li>
                        <? }?>
                        <?php if(CheckRoot('finance_orders')){ ?>
                            <li class="sidebar-item">
                                <a class="sidebar-link waves-effect waves-dark sidebar-link" href="dashboard?p=finance_orders">
                                    <i class="fa fa-dollar-sign"></i>
                                    <span class="hide-menu">Finance</span>
                                </a>
                            </li>
                        <? }?>
                        <?php if(CheckRoot('platform_updates')){ ?>
                            <li class="sidebar-item">
                                <a class="sidebar-link waves-effect waves-dark sidebar-link" href="dashboard?p=platform_updates">
                                    <i class="mdi mdi-gift"></i>
                                    <span class="hide-menu">Platform Updates</span>
                                </a>
                            </li>
                        <? }?>
                        <?php if(CheckRoot('staff')){ ?>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="dashboard?p=staff">
                                <i class="ti ti-user"></i>
                                <span class="hide-menu"> Staff </span>
                            </a>
                        </li>
                        <? }?>
                        <?php if(CheckRoot('visitors')){ ?>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="dashboard?p=visitors">
                                <i class="fa fa-users"></i>
                                <span class="hide-menu"> Visitors </span>
                            </a>
                        </li>
                        <? }?>
                        <?php if(CheckRoot('partners')){ ?>
                        <li class="sidebar-item">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="dashboard?p=partners">
                                <i class="fa fa-users"></i>
                                <span class="hide-menu"> Partners </span>
                            </a>
                        </li>
                        <? }?>
                    </ul>
                </nav>
            </div>
        </aside>
        <div class="page-wrapper">
            <div class="container-fluid">
