<?php
$db = new DB;
$status = $db->query("SELECT `status` FROM `documents` WHERE store_id = '{$_GET['id']}' ORDER BY id DESC LIMIT 1");
if($status){
if($status[0]['status'] == 3){
    $status = '<div class="alert alert-success">Accept</div>';
}elseif($status[0]['status'] == 2){
    $status = '<div class="alert alert-danger">Refuse</div>';
}else{
    $status = '<div class="alert alert-info">Pendding</div>';
}
?>
<div style="padding:10px;">
    <div class="text-center">
        <h4> <i class="fa fa-file"></i> Documents </h4>
        Store Doc Status is <?php echo $status ?>
        <br>
    </div>
    <div id="msg_doc"></div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Client Docments</th>
            <th scope="col">
                <ul>
                    <?php
                    $x = 1;
                    $data = $db->select('documents',['where'=>['store_id'=>$_GET['id']]]);
                    foreach ($data as $key) {
                        foreach (explode(',', $key['documents']) as $key) {
                            echo '<li><a target="_blank" href="https://staff.lanaapp.space/application/assets/documents/'.$key.'">'.$x.' - '.$key.'</a></li>';
                            $x++;
                        }
                    }
                    ?>
                </ul>
            </th>
        </tr>
        </thead>
    </table>
    <button class="btn btn-primary done"><i class="fa fa-check"></i> Accept </button>
    <button class="btn btn-danger return"><i class="fa fa-times"></i>  Refuse</button>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.done').click(function(){
                $.get('requests?doc_done&id=<?php echo $_GET['id'];?>',function(data){
                    $('#msg_doc').html(data);
                });
            });
            $('.return').click(function(){
                $.get('requests?doc_return&id=<?php echo $_GET['id'];?>',function(data){
                    $('#msg_doc').html(data);
                });
            });
        });
    </script>
</div>
<?php }else{
    echo '<div class="text-center p-4">Not Found Any Documents !!</div>';
} ?>
