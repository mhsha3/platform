<?php
$db = new DB;
$total_partners = $db->query('SELECT COUNT(*) as total FROM `partners` where 1')[0]['total'];
$all_partners = $db->select('partners');
$total_in_day = $db->query("SELECT count(*)as total FROM `partners` WHERE `created_at` > DATE_SUB(CURDATE(), INTERVAL 2 DAY)")[0]['total'];
$total_register = $db->query("SELECT count(*) as total from `partners_process` where `store_id` != 0 ")[0]['total'];
$total_clicks = $db->query("SELECT count(*) as total from `partners_process` where 1")[0]['total'];
?>
<a href="dashboard?p=partners_process" class="btn btn-primary">Show All Partners Process</a>
<div class="row">
	<div class="col">
		<div class="card text-white bg-success mb-3">
		  <div class="card-header">Total Partners</div>
		  <div class="card-body">
		    <h5 class="card-title"><?php echo $total_partners; ?></h5>
		  </div>
		</div>
	</div>
	<div class="col">
		<div class="card text-white bg-dark mb-3">
		  <div class="card-header">Total Clicks</div>
		  <div class="card-body">
		    <h5 class="card-title"><?php echo $total_clicks; ?></h5>
		  </div>
		</div>
	</div>
	<div class="col">
		<div class="card text-white bg-info mb-3">
		  <div class="card-header">Total in 24H</div>
		  <div class="card-body">
		    <h5 class="card-title"><?php echo $total_in_day;?></h5>
		  </div>
		</div>
	</div>
	<div class="col">
		<div class="card text-white bg-primary mb-3">
		  <div class="card-header">Total Register</div>
		  <div class="card-body">
		    <h5 class="card-title"><?php echo $total_register;?></h5>
		  </div>
		</div>
	</div>
</div>

<table id="all_stores" class="table-hover table-striped bg-white border shadow p-1" style="width:100%">
    <thead>
        <tr>
        	<td>ID</td>
            <td>Full Name</td>
            <td>email</td>
            <td>Phone</td>
            <td>status</td>
            <td>Created at</td>
        </tr>
    </thead>
</table>

<script type="text/javascript">
$(document).ready( function () {
    $('#all_stores').DataTable({
        <?php if($_SESSION['_SSID_'] == 1){?>
            dom: 'Blfrtip',
            "buttons": [
                    {
                        "extend": "csv",
                        "text": "Export as CSV",
                        "filename": "Stores Report Name",
                        "className": "btn btn-success",
                        "charset": "utf-8",
                        "bom": "true",
                        init: function(api, node, config) {
                            $(node).removeClass("btn-success");
                        }
                    },
                    {
                        "extend": "pdf",
                        "text": "Export as PDF",
                        "filename": "Stores Report Name",
                        "className": "btn btn-info",
                        "charset": "utf-8",
                        "bom": "true",
                        init: function(api, node, config) {
                            $(node).removeClass("btn-info");
                        }
                    }
                ], <?php }?>
        "data":<?php echo json_encode($all_partners,JSON_UNESCAPED_UNICODE);?>,
      "columns": [
            { "data": "partner_id" },
            { "data": "full_name" },
            { "data": "email" },
            { "data": "phone" },
            { "data": "status" },
            { "data": "created_at" }
        ]
    });
} );
</script>