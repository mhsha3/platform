<?php
$db = new DB;
$all_partners_process = $db->select('partners_process');
?>
<table id="all_stores" class="table-hover table-striped bg-white border shadow p-1" style="width:100%">
    <thead>
        <tr>
        	<td>ID</td>
            <td>Partner ID</td>
            <td>Store ID</td>
            <td>is Subscription</td>
            <td>Payed</td>
            <td>Created at</td>
        </tr>
    </thead>
</table>

<script type="text/javascript">
$(document).ready( function () {
    $('#all_stores').DataTable({
        <?php if($_SESSION['_SSID_'] == 1){?>
            dom: 'Blfrtip',
            "buttons": [
                    {
                        "extend": "csv",
                        "text": "Export as CSV",
                        "filename": "Stores Report Name",
                        "className": "btn btn-success",
                        "charset": "utf-8",
                        "bom": "true",
                        init: function(api, node, config) {
                            $(node).removeClass("btn-success");
                        }
                    },
                    {
                        "extend": "pdf",
                        "text": "Export as PDF",
                        "filename": "Stores Report Name",
                        "className": "btn btn-info",
                        "charset": "utf-8",
                        "bom": "true",
                        init: function(api, node, config) {
                            $(node).removeClass("btn-info");
                        }
                    }
                ], <?php }?>
        "data":<?php echo json_encode($all_partners_process,JSON_UNESCAPED_UNICODE);?>,
      "columns": [
            { "data": "id" },
            { "data": "partner_id" },
            {"render": function(data,type,row){
              return '<a href="dashboard?p=store&id='+row['store_id']+'">'+row['store_id']+'</a>';
            }},
            { "data": "store_subscription" },
            { "data": "lana_payed" },
            { "data": "created_at" }
        ]
    });
} );
</script>