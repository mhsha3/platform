<?php
$db = new DB;
$total_stores = $db->query('SELECT COUNT(*) as total FROM `stores` where 1')[0]['total'];
$all_stores = $db->select('stores');
if(isset($_GET['expire_stores'])){
    $all_stores = $db->query("SELECT * FROM `stores` WHERE expire_date < '".date("Y-m-d H:i:s")."'");
}
show_error();
$total_expired = $db->query("SELECT COUNT(*) AS total FROM `stores` WHERE expire_date < '".date("Y-m-d H:i:s")."'")[0]['total'];
$total_in_day = $db->query("SELECT count(*)as total FROM `stores` WHERE `created_at` > DATE_SUB(CURDATE(), INTERVAL 2 DAY)")[0]['total'];
$total_in_week = $db->query("SELECT count(*)as total FROM `stores` WHERE `created_at` > DATE_SUB(CURDATE(), INTERVAL 7 DAY)")[0]['total'];
?>
<div class="row">
	<div class="col">
		<div class="card text-white bg-success mb-3">
		  <div class="card-header">Total Stores</div>
		  <div class="card-body">
		    <h5 class="card-title"><?php echo $total_stores; ?> - working(<?php echo ($total_stores-$total_expired); ?>)</h5>
		  </div>
		</div>
	</div>
	<div class="col">
		<div class="card text-white bg-danger mb-3">
		  <div class="card-header">Total Expired</div>
		  <div class="card-body">
		    <h5 class="card-title"><a class="text-white" href="dashboard?p=all_stores&expire_stores"><?php echo $total_expired; ?></a></h5>
		  </div>
		</div>
	</div>
	<div class="col">
		<div class="card text-white bg-info mb-3">
		  <div class="card-header">Total in 24H</div>
		  <div class="card-body">
		    <h5 class="card-title"><?php echo $total_in_day;?></h5>
		  </div>
		</div>
	</div>
	<div class="col">
		<div class="card text-white bg-primary mb-3">
		  <div class="card-header">Total in Week</div>
		  <div class="card-body">
		    <h5 class="card-title"><?php echo $total_in_week;?></h5>
		  </div>
		</div>
	</div>
</div>

<table id="all_stores" class="table-hover table-striped bg-white border shadow p-1" style="width:100%">
    <thead>
        <tr>
        	<td>ID</td>
            <td>Store Name</td>
            <td>path</td>
            <td>Domain</td>
            <td>Phone</td>
            <td>Email</td>
            <td>Username</td>
        </tr>
    </thead>
</table>

<script type="text/javascript">
$(document).ready( function () {
    $('#all_stores').DataTable({
        <?php if($_SESSION['_SSID_'] == 1){?>
            dom: 'Blfrtip',
            "buttons": [
                    {
                        "extend": "csv",
                        "text": "Export as CSV",
                        "filename": "Stores Report Name",
                        "className": "btn btn-success",
                        "charset": "utf-8",
                        "bom": "true",
                        init: function(api, node, config) {
                            $(node).removeClass("btn-success");
                        }
                    },
                    {
                        "extend": "pdf",
                        "text": "Export as PDF",
                        "filename": "Stores Report Name",
                        "className": "btn btn-info",
                        "charset": "utf-8",
                        "bom": "true",
                        init: function(api, node, config) {
                            $(node).removeClass("btn-info");
                        }
                    }
                ], <?php }?>
        "data":<?php echo json_encode($all_stores,JSON_UNESCAPED_UNICODE);?>,
      "columns": [
            {"render": function(data,type,row){
              return '<a href="dashboard?p=store&id='+row['id']+'">'+row['id']+'</a>';
            }},
            { "data": "storename" },
            { "data": "path" },
            { "data": "domain" },
            { "data": "phone" },
            { "data": "email" },
            { "data": "username" }
        ]
    });
} );
</script>