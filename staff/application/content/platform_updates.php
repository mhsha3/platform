<?php
$db = new DB;
$updates = $db->select('platform_updates');
if(isset($_GET['edit'])){
    $x = $db->select('platform_updates',['where'=>['id'=>$_GET['edit']]])[0];
}
?>
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<?php if($_GET['edit']){
    echo '<a class="btn btn-info mb-2" href="dashboard?p=platform_updates">Add Updates</a>';
}else{
    echo '<button class="btn btn-info mb-2" id="add_updates">Add Updates</button>';
}?>
<div class="bg-white shadow border p-4">
    <div id="msg"></div>
    <?php if(isset($_GET['edit'])){
        echo '<button class="btn btn-danger mb-2" id="del_updates">Delete Update</button>';
    }?>
    <form style="<?php if($_GET['edit']){echo 'display:block;';}else{echo 'display:none;';}?>">
        <div class="form-group">
            <label>Title</label>
            <input type="text" name="title" class="form-control" value="<?php echo $x['title']??''; ?>" placeholder="Title">
        </div>
        <div class="form-group">
            <label>Image</label>
            <input type="text" name="img" class="form-control" value="<?php echo $x['img']??''; ?>" placeholder="Image">
        </div>
        <div class="form-group">
            <label>Content</label>
            <textarea id="summernote" name="content"><?php echo $x['description']??''; ?></textarea>
        </div>
        <div class="form-group">
            <label>Status</label>
            <select name="status" class="form-control">
                <option value="1">Avaliable</option>
                <option value="0">unAvaliable</option>
            </select>
        </div>
        <?php
        if(isset($_GET['edit'])){
            echo '<button type="submit" class="btn btn-primary">Edit</button>';
        }else{
            echo '<button type="submit" class="btn btn-primary">Add</button>';
        }
        ?>
    </form>
    <br><hr><br>
    <div>
        <table id="staff" class="table-hover table-striped bg-white border shadow p-1" style="width:100%">
            <thead>
            <tr>
                <td>id</td>
                <td>Title</td>
                <td>Status</td>
                <td>Options</td>
            </tr>
            </thead>
        </table>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#del_updates').click(function(){
            $.post('requests?delete_platform_updates&id=<?php echo $_GET['edit']; ?>',function(data){
                $('#msg').html(data);
            });
        });
        var action = '<?php
            if(isset($_GET['edit'])){
                echo 'edit_platform_updates&id='.$_GET['edit'];
            }else{
                echo 'add_platform_updates';
            }
            ?>';
        $('#add_updates').click(function () {
            $('form').toggle();
        });
        $('#summernote').summernote({
            placeholder: 'Updates Content',
            tabsize: 2,
            height: 100
        });
        $('form').submit(function(e){
            e.preventDefault();
            $.post('requests?'+action,$(this).serialize(),function(data){
                $('#msg').html(data);
            });
        });
        $('#staff').DataTable({
            "data":<?php echo json_encode($updates,JSON_UNESCAPED_UNICODE);?>,
            "columns": [
                { "data": "id" },
                { "data": "title" },
                {"render": function(data,type,row){
                        var x = row['status'];
                        if(row['status'] == '1'){
                            x = '<div class="badge badge-success">Avaliable</div>';
                        }
                        if(row['status'] == '0'){
                            x = '<div class="badge badge-danger">Unavaliable</div>';
                        }
                        return x;
                }},
                {"render": function(data,type,row){
                        return '<a class="btn btn-info" href="dashboard?p=platform_updates&edit='+row['id']+'"><i class="ti ti-eye"></></a>';
                }}
            ]
        });
    });
</script>
