<?php
$db = new DB;
$data = $db->select('stores',['where'=>['id'=>$_GET['id']]])[0];
if($data['domain']){
    $domain = 'https://'.$data['domain'];
}else{
    $domain = 'https://lnasa.space/'.$data['path'];
}
?>
<div class="m-4">
    <table class="table">
        <tbody>
        <tr>
            <th scope="row"><b>DataBase Name:</b></th>
            <td><?php echo $data['database_name']; ?></td>
        </tr>
        <tr>
            <th scope="row"><b>Path:</b></th>
            <td><?php echo $data['path']; ?></td>
        </tr>
        <tr>
            <th scope="row"><b>Domain:</b></th>
            <td><a href="<?php echo $domain; ?>" target="_blank"><?php echo $domain; ?></a></td>
        </tr>
        <tr>
            <th scope="row"><b>Access Root: </b></th>
            <td>
                <b>Username: </b>LANA_SUPPORT | <b>Password: </b><span id="password"><button class="btn btn-info" id="access">Access</button></span>
                <div class="alert alert-danger" id="access_msg" style="display:none">Please When Finished Your Work in store Go to Users and make Reload </div>
            </td>

        </tr>
        <?php if($_SESSION['_SSID_'] == 1){?>
        <tr>
            <th scope="row"><b>Password: </b></th>
            <td>
                <b>Username: </b><?php echo $data['username']; ?> | <b>Password: </b> <?php echo $data['password']; ?>
            </td>

        </tr> <? } ?>
        <tr>
            <th scope="row"><b>Add New Domain: </b></th>
            <td>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_domain"> Add Domain </button>
            </td>

        </tr>
        </tbody>
    </table>
</div>

<div class="modal fade" id="add_domain" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Domain</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="text" class="form-control" placeholder="ex. mohab.com" id="domain_value">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="new_domain">Save changes</button>
      </div>
    </div>
  </div>
</div>
<script>
    $('#access').click(function () {
        $.post('requests?get_access_root&store_id=<?php echo $_GET['id']; ?>',function (data) {
            $('#msg').html(data);
        });
    });
    $('#new_domain').click(function () {
        $.post('requests?add_new_domain&store_id=<?php echo $_GET['id']; ?>',{domain:$('#domain_value').val()},function (data) {
            $('#msg').html(data);
        });
    });
</script>