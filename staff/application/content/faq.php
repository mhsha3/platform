<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<div class="bg-white shadow border p-4">
	<div id="msg"></div>
<form>
  <div class="form-group">
     <label>Title</label>
     <input type="text" name="title" class="form-control" placeholder="Title">
  </div>
  <div class="form-group">
     <label>Content</label>
       <textarea id="summernote" name="content"></textarea>
  </div>
  <div class="form-group">
     <label>Sort number</label>
     <input type="number" name="sort_by" class="form-control" placeholder="Sort Number">
  </div>
  <button type="submit" class="btn btn-primary">Add</button>
</form>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('#summernote').summernote({
        placeholder: 'Faq Content',
        tabsize: 2,
        height: 100
      });
  $('form').submit(function(e){
    e.preventDefault();
      $.post('requests?add_faq',$(this).serialize(),function(data){
  	     $('#msg').html(data);
     });
  });
});
</script>
