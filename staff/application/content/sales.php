<?php
$db = new DB;
$total_contact = $db->query('SELECT COUNT(*) as total FROM `stores` where sales_status = 2')[0]['total'];
$total_pendding = $db->query('SELECT COUNT(*) as total FROM `stores` where sales_status = 1')[0]['total'];
$total_new = $db->query('SELECT COUNT(*) as total FROM `stores` where sales_status = 0')[0]['total'];
$all_stores = $db->select('stores');
?>
<div class="row">
    <div class="col">
        <div class="card text-white bg-info mb-3">
            <div class="card-header">Total New</div>
            <div class="card-body">
                <h5 class="card-title"><?php echo $total_new; ?></h5>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card text-white bg-success mb-3">
            <div class="card-header">Total Contact</div>
            <div class="card-body">
                <h5 class="card-title"><?php echo $total_contact; ?></h5>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card text-white bg-primary mb-3">
            <div class="card-header">Total Pendding</div>
            <div class="card-body">
                <h5 class="card-title"><?php echo $total_pendding; ?></h5>
            </div>
        </div>
    </div>
</div>

<table id="sales" class="table-hover table-striped bg-white border shadow p-1" style="width:100%">
    <thead>
    <tr>
        <td>ID</td>
        <td>Store Name</td>
        <td>Status</td>
        <td>Phone</td>
        <td>Email</td>
        <td>Package</td>
        <td>Options</td>
    </tr>
    </thead>
</table>

<script type="text/javascript">
    $(document).ready( function () {
        $('#sales').DataTable({
            "data":<?php echo json_encode($all_stores,JSON_UNESCAPED_UNICODE);?>,
            "columns": [
                {"render": function(data,type,row){
                        return '<a href="dashboard?p=store&id='+row['id']+'">'+row['id']+'</a>';
                }},
                { "data": "storename" },
                {"render": function(data,type,row){
                    var x;
                        if(row['sales_status'] == 0){
                            x = '<div class="badge badge-info"> New Store </div>';
                        }
                        if(row['sales_status'] == 1){
                            x = '<div class="badge badge-primary"> Pendding </div>';
                        }
                        if(row['sales_status'] == 2){
                            x = '<div class="badge badge-success"> Complate </div>';
                        }
                        return x;
                    }},
                { "data": "phone" },
                { "data": "email" },
                {"render": function(data,type,row){
                    var x;
                        if(row['package_id'] == 1){
                            x = '<div class="badge badge-danger"> unPaid </div>';
                        }else if(row['package_id'] == 2){
                            x = '<div class="badge badge-success"> Paid </div>';
                        }else{
                            x = '<div class="badge badge-danger"> UnKnow !! </div>';
                        }
                        return x;
                }},
                {"render": function(data,type,row){
                        return '<a class="btn btn-success" href="requests?sales_status&done&id='+row['id']+'"><i class="fa fa-check"></i></a> <a class="btn btn-primary" href="requests?sales_status&pedding&id='+row['id']+'"><i class="fa fa-phone"></i></a>';
                }},
            ]
        });
    } );
</script>
