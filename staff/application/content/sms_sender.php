<div class="bg-white shadow border p-4">
    <div id="msg"></div>
    <form>
        <div class="form-group">
            <label>Enter Client Phone</label>
            <input type="text" required name="phone" value="<?php echo ($_GET['phone']) ?? ''; ?>" class="form-control" placeholder="Enter Client Email">
        </div>
        <div class="form-group">
            <label>Content</label>
            <textarea id="summernote" class="form-control" name="content"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Send</button>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('form').submit(function(e){
            e.preventDefault();
            $.post('requests?sms_sender&store_id=<?php echo $_GET['store_id'] ?? '0'; ?>',$(this).serialize(),function(data){
                $('#msg').html(data);
            });
        });
    });
</script>
