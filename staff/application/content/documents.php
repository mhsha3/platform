<?php
show_error();
$db = new DB;
$documents = $db->select('documents');
$store_person = $db->query("SELECT count(*) FROM `documents` WHERE type = '1' ")[0]['count(*)'];
$store_company = $db->query("SELECT count(*) FROM `documents` WHERE type = '2' ")[0]['count(*)'];
$pendding = $db->query("SELECT count(*) FROM `documents` WHERE status = '1' ")[0]['count(*)'];
$complate = $db->query("SELECT count(*) FROM `documents` WHERE status = '3' ")[0]['count(*)'];
$refuse = $db->query("SELECT count(*) FROM `documents` WHERE status = '2' ")[0]['count(*)'];
?>
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <div class="row">
        <style>
            .card{min-width:124px;}
            .card-body {padding:4px;text-align:center}
        </style>
        <div class="col">
            <div class="card text-white bg-secondary">
                <div class="card-header">Single</div>
                <div class="card-body">
                    <h5 class="card-title"><?php __($store_person)?></h5>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card text-white bg-warning">
                <div class="card-header"> Company </div>
                <div class="card-body">
                    <h5 class="card-title"><?php __($store_company)?></h5>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card text-white bg-info">
                <div class="card-header"> New </div>
                <div class="card-body">
                    <h5 class="card-title"><?php __($pendding)?></h5>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card text-white bg-success">
                <div class="card-header"> Complate </div>
                <div class="card-body">
                    <h5 class="card-title"><?php __($complate)?></h5>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card text-white bg-danger">
                <div class="card-header"> Refuse </div>
                <div class="card-body">
                    <h5 class="card-title"><?php __($refuse)?></h5>
                </div>
            </div>
        </div>

    </div>
</div>
<div id="msg"></div>
<table id="orders" class="display">
    <thead>
    <tr>
        <th>ID</th>
        <th>Store ID</th>
        <th>Store Type</th>
        <th>Status</th>
        <th>Date</th>
    </tr>
    </thead>
</table>
<script type="text/javascript">
    $(document).ready(function(){
        $('#orders').DataTable({
            "data":<?php echo json_encode($documents,JSON_UNESCAPED_UNICODE);?>,
            "columns": [
                {"render": function(data,type,row){
                        return row['id'];
                    }},
                {"render": function(data,type,row){
                        return '<a href="dashboard?p=store&id='+row['store_id']+'">#'+row['store_id']+'<a>';
                    }},
                {"render":function(data,type,row){
                        var x = ''
                        if(row['type'] == '1'){
                            x = '<span class="badge badge-info">Single</span>';
                        }
                        if(row['type'] == '2'){
                            x = '<span class="badge badge-primary"> Company</span>';
                        }
                        return x;
                    },
                },
                {"render":function(data,type,row){
                        var x = ''
                        if(row['status'] == '1'){
                            x = '<span class="badge badge-info">Pendding</span>';
                        }
                        if(row['status'] == '3'){
                            x = '<span class="badge badge-success"> Complate</span>';
                        }
                        if(row['status'] == '2'){
                            x = '<span class="badge badge-danger">Refuse</span>';
                        }
                        return x;
                    },
                },
                {"render": function(data,type,row){
                        return (row['created_at']);
                    }},
            ]
        });
    });

</script>
