<?php
$db = new DB;
$total_visitors = $db->query('SELECT COUNT(*) as total FROM `visitors` where 1')[0]['total'];
$visitors = $db->select('visitors');
?>
<div class="row">
	<div class="col">
		<div class="card text-white bg-success mb-3">
		  <div class="card-header">Total Stores</div>
		  <div class="card-body">
		    <h5 class="card-title"><?php echo $total_visitors; ?></h5>
		  </div>
		</div>
	</div>
	<div class="col">
		<div class="card text-white bg-primary mb-3">
		  <div class="card-header">Total Stores</div>
		  <div class="card-body">
		    <h5 class="card-title">0</h5>
		  </div>
		</div>
	</div>
	<div class="col">
		<div class="card text-white bg-info mb-3">
		  <div class="card-header">Total Stores</div>
		  <div class="card-body">
		    <h5 class="card-title">0</h5>
		  </div>
		</div>
	</div>
	<div class="col">
		<div class="card text-white bg-danger mb-3">
		  <div class="card-header">Total Stores</div>
		  <div class="card-body">
		    <h5 class="card-title">0</h5>
		  </div>
		</div>
	</div>
</div>

<table id="all_stores" class="table-hover table-striped bg-white border shadow p-1" style="width:100%">
    <thead>
        <tr>
        	<td>ID</td>
            <td>Ip</td>
            <td>Visite at</td>
            <td>page</td>
            <td>Referrer From</td>
            <td>User Agent</td>
        </tr>
    </thead>
</table>
<script src="https://cdnjs.cloudflare.com/ajax/libs/UAParser.js/0.7.20/ua-parser.min.js" integrity="sha512-70OZ+iUuudMmd7ikkUWcEogIw/+MIE17z17bboHhj56voxuy+OPB71GWef47xDEt5+MxFdrigC1tRDmuvIVrCA==" crossorigin="anonymous"></script>
<script type="text/javascript">
$(document).ready( function () {
    $('#all_stores').DataTable({
        <?php if($_SESSION['_SSID_'] == 1){echo "
            dom: 'Blfrtip',
            buttons: [
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],"; }?>
        "data":<?php echo json_encode($visitors,JSON_UNESCAPED_UNICODE);?>,
      "columns": [
            { "data": "id" },
            { "render": function(data,type,row){
                return '<a href="http://ip-api.com/json/'+row['ip']+'">' +row['ip']+ '</a>';   
            }},
            { "data": "visited_at" },
            { "data": "page" },

            { "render": function(data,type,row){
                var x = new UAParser(row['user_agent']);
               return x.getDevice().type +' - ' + x.getDevice().vendor + ' - ' + x.getDevice().model;   
            }},
            { "render": function(data,type,row){
               return '<a href="'+ row['referrer'] +'"> From </a>';   
            }},
        ]
    });
} );
</script>
