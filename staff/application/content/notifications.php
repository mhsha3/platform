<?php

if(!isset($_GET['id']) && !empty($_GET['id'])){
    redirect('dashboard');
}

?>
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<div class="bg-white shadow border p-4">
    <h1 class="text-center text-info">Store # <?php echo $_GET['id']; ?></h1>
	<div id="msg"></div>
<form>
  <div class="form-group">
     <label>Title</label>
     <input type="text" name="title" required class="form-control" placeholder="Title">
  </div>
  <div class="form-group">
     <label>Title</label>
     <select name="tag" class="form-control" required>
         <option value="lana_support">Lana Support</option>
         <option value="lana_sales">Lana Sales</option>
         <option value="lana_finance">Lana Finance</option>
     </select>
  </div>
  <div class="form-group">
     <label>Content</label>
       <textarea id="summernote" name="content"></textarea>
  </div>
  <input type="hidden" name="store_id" value="<?php echo $_GET['id'] ?? 0; ?>"/>
  <button type="submit" class="btn btn-primary">Send</button>
</form>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('#summernote').summernote({
        placeholder: 'Notification Content',
        tabsize: 2,
        height: 100
      });
  $('form').submit(function(e){
    e.preventDefault();
      $.post('requests?send_notifications',$(this).serialize(),function(data){
  	     $('#msg').html(data);
     });
  });
});
</script>
