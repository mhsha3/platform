<?php
$db = new DB;
$data = $db->select('stores',['where'=>['id'=>$_GET['id']]])[0];
$data_sales = $db->select('notes',['where'=>['section'=>'sales','tags'=>'sales_tracking','store_id'=>$_GET['id']]]);
$package = $_SESSION['_PACKAGE_'];
?>
<div id="msg"></div>
<div class="m-4">
    <table class="table">
        <tbody>
        <tr>
            <th scope="row"><b>Upgrade Package:</b></th>
            <td>
                <select class="form-control" id="upgrade_package_value">
                    <?php
                      foreach ($package as $key => $value){
                          echo '<option value="'.$key.'">'.$value.'</option>';
                      }
                    ?>
                </select>
            </td>
            <td>
                <button class="btn btn-success" id="upgrade_package">Upgrade</button>
            </td>
        </tr>
        <tr>
            <th scope="row"><b>Upgrade Expire Date:</b></th>
            <td>
                <input type="date" class="form-control" id="expire_date_value"/>
            </td>
            <td>
                <button class="btn btn-success" id="expire_date">Upgrade</button>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<br>
<form id="form" class="bg-dark p-4">
<div class="form-group">
    <label class="text-white">Add Notes</label>
    <textarea class="form-control" name="content" rows="3"></textarea>
</div>
<button type="submit" class="btn btn-primary">Add Notes</button>
</form>
<br>
<table id="sales_note" class="table-hover table-striped bg-white border shadow p-1" style="width:100%">
    <thead>
    <tr>
        <td>ID</td>
        <td>Employee</td>
        <td>Content</td>
        <td>Date</td>
    </tr>
    </thead>
</table>
<script>
    $('#upgrade_package').click(function (){
        $.post('requests?upgrade_package',{package_id:$('#upgrade_package_value').val(),store_id:<?php echo $_GET['id']; ?>},function(data){
            $('#msg').html(data);
        });
    });
    $('#expire_date').click(function (){
        $.post('requests?upgrade_expire_date',{expire_date:$('#expire_date_value').val(),store_id:<?php echo $_GET['id']; ?>},function(data){
            $('#msg').html(data);
        });
    });
    $('#form').submit(function (e){
        e.preventDefault();
        $.post('requests?addnote&s=sales&a=sales_tracking&store_id=<?php echo $_GET['id']; ?>',$(this).serialize(),function(data){
            $('#msg').html(data);
        });
    });

    $('#sales_note').DataTable({
            "data":<?php echo json_encode($data_sales,JSON_UNESCAPED_UNICODE);?>,
            "columns": [
                { "data": "id" },
                { "data": "employee_id" },
                { "data": "content" },
                { "data": "created_at" }
            ]
        });
</script>
