<?php
$db = new DB;
$staff = $db->select('staff');
$root = [
  	'all_stores'=> 'All Stores',
  	'email_sender'=> 'Emails Sender',
  	'sms_sender'=> 'Sms Sender',
    'staff'=> 'Staff',
    'support'=> 'Support',
    'sales'=> 'Sales',
	'staff_profile'=> 'Staff Profile',
    'store'=> 'Store Profile',
    'documents'=> 'Documents',
    'notifications'=> 'Notifications',
    'sms_sender'=> 'Sms Sender',
    'finance_orders'=> 'Finance',
    'services'=> 'Services Details',
    'domain_order'=> 'Domain Orders',
    'design_order'=> 'Design Orders',
    'other_order'=> 'Other Orders',
    'all_order'=> 'All Orders',
    'platform_updates'=> 'Platform Updates',
    'ticket' => 'Ticket System',
    'support_supervisor' => 'Support SuperVisor',
    'sales_supervisor' => 'Sales SuperVisor',
    'customer_services' => 'Customer Services',
    'visitors' => 'Platform Visitors',
    'partners' => 'Partners',
    'faq' => 'FAQ',
    'partners_process' => 'Partners Process'
];
$_SESSION['_ALL_ROOT_'] = $root;
?>
<link rel="stylesheet" href="application/assets/multi-select.css"/>
<script src="application/assets/multi-select.js"></script>
<div class="float-right m-1">
<button class="btn btn-primary" type="button" class="btn btn-primary" data-toggle="modal" data-target="#add">Add Staff</button>
</div>

<div class="modal fade" id="add" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Add New Staff</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div id="addmsg"></div>
<form id="addform">
  <div class="form-group">
    <label>Name</label>
    <input type="text" required class="form-control" name="name" placeholder="Name">
  </div>
  <div class="form-group">
    <label>Username</label>
    <input type="text" required class="form-control" name="username" placeholder="Username">
  </div>
  <div class="form-group">
    <label>Email</label>
    <input type="email" required class="form-control" name="email" placeholder="Email">
  </div>
  <div class="form-group">
    <label>Password</label>
    <input type="text" required class="form-control" name="password" placeholder="Password">
  </div>
  <div class="form-group">
    <label>Add Staff Root</label>
    <select multiple required class="form-control" name="root[]" id="selectadd">
         <?php
         foreach ($root as $key => $value) {
         	echo '<option value="'.$key.'">'.$value.'</option>';
         }
         ?>
    </select>
  </div>
  <div class="form-group">
    <label>Status</label>
    <select required class="form-control" name="status">
         <option value="1">Avaliable</option>
         <option value="0">UnAvaliable</option>
    </select>
  </div>
  <button class="btn btn-info" type="submit">Add</button>
</form>
      </div>
    </div>
  </div>
</div>

<table id="staff" class="table-hover table-striped bg-white border shadow p-1" style="width:100%">
    <thead>
        <tr>
        	<td>Name</td>
            <td>Username</td>
            <td>Status</td>
            <td>Options</td>
        </tr>
    </thead>
</table>

<script type="text/javascript">
$(document).ready(function(){
	$('#selectadd').multiSelect();
	$('#addform').submit(function(e){
		e.preventDefault();
		$.post('requests?add_staff',$(this).serialize(),function(data){
            $('#addmsg').html(data);
		});
	});
    $('#staff').DataTable({
      "data":<?php echo json_encode($staff,JSON_UNESCAPED_UNICODE);?>,
      "columns": [
            { "data": "name" },
            { "data": "username" },
            {"render": function(data,type,row){
            	var x = row['status'];
            	if(row['status'] == '1'){
                   x = '<div class="badge badge-success">Avaliable</div>';
            	}
            	if(row['status'] == '0'){
                   x = '<div class="badge badge-danger">Unavaliable</div>';
            	}
              return x;
            }},
            {"render": function(data,type,row){
              return '<a class="btn btn-info" href="dashboard?p=staff_profile&id='+row['id']+'"><i class="ti ti-eye"></></a>';
            }}
        ]
    });
} );
</script>
