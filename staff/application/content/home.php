<?php
date_default_timezone_set('Asia/Riyadh');
echo date("h:i:s");
$db = new DB;
?>


<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

<?php
if(CheckRoot('sales_supervisor')){
   show_error();
   function stores_chart($db,$month = 0,$year=2021){
       $data = $db->query("SELECT count(*) as total from `stores` where YEAR(created_at) = {$year} AND MONTH(created_at) = {$month} ")[0]['total'];
       echo $data;
   }
   echo '<canvas id="all_stores"></canvas>';
}

?>

<script>
var ctx = document.getElementById('all_stores').getContext('2d');
var chart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July','August','September','October','November','December'],
        datasets: [{
            label: 'Stores in 2021',
            borderColor: '#f6c522',
            data: [
            <?php stores_chart($db,1) ?>, 
            <?php stores_chart($db,2) ?>, 
            <?php stores_chart($db,3) ?>, 
            <?php stores_chart($db,4) ?>, 
            <?php stores_chart($db,5) ?>, 
            <?php stores_chart($db,6) ?>, 
            <?php stores_chart($db,7) ?>,
            <?php stores_chart($db,8) ?>,
            <?php stores_chart($db,9) ?>,
            <?php stores_chart($db,10) ?>,
            <?php stores_chart($db,11) ?>,
            <?php stores_chart($db,12) ?>
            ]
        }]
    },
    options: {}
});
</script>