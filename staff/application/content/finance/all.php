<?php
function getOrders($id = 0,$data = array()) {
    $sql = "SELECT '$id' AS store_id, o.order_id, CONCAT(o.firstname, ' ', o.lastname) AS customer, (SELECT os.name FROM oc_order_status os WHERE os.order_status_id = o.order_status_id AND os.language_id = '2') AS order_status, o.shipping_method, o.total, o.currency_code, o.currency_value, o.date_added, o.date_modified,o.payment_method FROM `oc_order` o";

    if (!empty($data['filter_order_status'])) {
        $implode = array();

        $order_statuses = explode(',', $data['filter_order_status']);

        foreach ($order_statuses as $order_status_id) {
            $implode[] = "o.order_status_id = '" . (int)$order_status_id . "'";
        }

        if ($implode) {
            $sql .= " WHERE (" . implode(" OR ", $implode) . ")";
        }
    } elseif (isset($data['filter_order_status_id']) && $data['filter_order_status_id'] !== '') {
        $sql .= " WHERE o.order_status_id = '" . (int)$data['filter_order_status_id'] . "'";
    } else {
        $sql .= " WHERE o.order_status_id > '0'";
    }

    if (!empty($data['filter_order_id'])) {
        $sql .= " AND o.order_id = '" . (int)$data['filter_order_id'] . "'";
    }

    if (!empty($data['filter_customer'])) {
        $sql .= " AND CONCAT(o.firstname, ' ', o.lastname) LIKE '%" . ($data['filter_customer']) . "%'";
    }

    if (!empty($data['filter_date_added'])) {
        $sql .= " AND DATE(o.date_added) = DATE('" . ($data['filter_date_added']) . "')";
    }

    if (!empty($data['filter_date_modified'])) {
        $sql .= " AND DATE(o.date_modified) = DATE('" . ($data['filter_date_modified']) . "')";
    }

    if (!empty($data['filter_total'])) {
        $sql .= " AND o.total = '" . (float)$data['filter_total'] . "'";
    }

    $sort_data = array(
        'o.order_id',
        'customer',
        'order_status',
        'o.date_added',
        'o.date_modified',
        'o.total'
    );

    if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
        $sql .= " ORDER BY " . $data['sort'];
    } else {
        $sql .= " ORDER BY o.order_id";
    }

    if (isset($data['order']) && ($data['order'] == 'DESC')) {
        $sql .= " DESC";
    } else {
        $sql .= " ASC";
    }

    if (isset($data['start']) || isset($data['limit'])) {
        if ($data['start'] < 0) {
            $data['start'] = 0;
        }

        if ($data['limit'] < 1) {
            $data['limit'] = 20;
        }

        $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
    }

    $query = GetStore::query($id,$sql);
    return $query;
}

$db = new DB;
$get_all_id = $db->select('stores');
foreach ($get_all_id as $key){
    foreach (getOrders($key['id']) as $xx){
        $xx['withdrawal'] = $db->select('withdrawal',['where'=>['store_id'=>$xx['store_id'],'order_id'=>$xx['order_id']]])[0]['status'] ?? '0';
        if(isset($_GET['show_paid'])){
            if($xx['withdrawal'] == 1){
                $total_orders[] = $xx;
            }
        }elseif(isset($_GET['show_unpaid'])){
            if($xx['withdrawal'] == 0){
                $total_orders[] = $xx;
            }
        }else{
            $total_orders[] = $xx;
        }
    }
}
if(isset($_GET['show'])){
    __($total_orders);
}
?>
<br>
<table id="orders" class="table-hover table-striped bg-white border shadow p-1" style="width:100%">
    <thead>
    <tr>
        <td>Store ID</td>
        <td>Order ID</td>
        <td>Withdrawal</td>
        <td>Customer</td>
        <td>Order Status</td>
        <td>Total</td>
        <td>Shipping Code</td>
        <td>Payment Code</td>
        <td>Date</td>
        <td>Actions</td>
    </tr>
    </thead>
</table>
<script type="text/javascript">
    $(document).ready( function () {
        $('#orders').DataTable({
            <?php if($_SESSION['_SSID_'] == 1){echo "
            dom: 'Blfrtip',
            buttons: [
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],"; }?>
            "data":<?php echo json_encode($total_orders,JSON_UNESCAPED_UNICODE);?>,
            "columns": [
                {"render": function(data,type,row){
                        return '<a href="dashboard?p=store&id='+row['store_id']+'">'+row['store_id']+'</a>';
                }},
                {"render": function(data,type,row){
                        return '<a href="dashboard?p=finance_orders&a=get_orders&store_id='+row['store_id']+'&order_id='+row['order_id']+'">'+row['order_id']+'</a>';
                }},
                {"render": function(data,type,row){
                    var x = row['withdrawal'];
                        if(row['withdrawal'] == 0){
                            x = '<div class="badge badge-danger">UnPaid</div>';
                        }if(row['withdrawal'] == 1){
                            x = '<div class="badge badge-success">Paid</div>';
                        }
                        return x;
                }},
                { "data": "customer" },
                { "data": "order_status" },
                {"render": function(data,type,row){
                        return row['total']+' '+row['currency_code'];
                }},
                { "data": "shipping_method" },
                { "data": "payment_method" },
                { "data": "date_added" },
                {"render": function(data,type,row){
                    return '<a href="requests?withdrawal&store_id='+row['store_id']+'&order_id='+row['order_id']+'&amount='+row['total']+'"><button class="btn btn-success"><i class="fa fa-check"></i></button></a>';
                }},
            ]
        });
    } );
</script>
