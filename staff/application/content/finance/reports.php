<?php
$db = new DB;
$total_withdrawal = $db->query("SELECT SUM(amount) as total FROM `withdrawal` where status = 1")[0]['total'];
?>
<div class="m-3">
    <a href="dashboard?p=finance_orders&a=all" class="btn btn-info">Total Orders</a>
    <a href="dashboard?p=finance_orders&a=all&show_paid" class="btn btn-info">Total Paid Orders</a>
    <a href="dashboard?p=finance_orders&a=all&show_unpaid" class="btn btn-info">Total unPaid Orders</a>
</div>
<table class="table table-hover table-striped ">
    <tbody>
      <tr>
          <td>Total Withdrawal </td>
          <td><?php echo $total_withdrawal ?? 0; ?> SAR</td>
      </tr>
    </tbody>
</table>
