<?php
$db = new DB;
$type = 'others';
$services = $db->select('services',['where'=>['type'=>$type]]);
$total_services = $db->query("SELECT COUNT(*) as total FROM `services` where `type` = '{$type}'")[0]['total'];
$total_complate = $db->query("SELECT COUNT(*) as total FROM `services` where `type` = '{$type}' AND status = 2")[0]['total'];
$total_pendding = $db->query("SELECT COUNT(*) as total FROM `services` where `type` = '{$type}' AND status = 1")[0]['total'];
$total_new = $db->query("SELECT COUNT(*) as total FROM `services` where `type` = '{$type}' AND status = 0")[0]['total'];
$total_refuse = $db->query("SELECT COUNT(*) as total FROM `services` where `type` = '{$type}' AND status = 3")[0]['total'];
?>
<div class="row">
    <div class="col">
        <div class="card text-white bg-dark mb-3">
            <div class="card-header">Total Services</div>
            <div class="card-body">
                <h5 class="card-title"><?php echo $total_services; ?></h5>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card text-white bg-primary mb-3">
            <div class="card-header">Total New</div>
            <div class="card-body">
                <h5 class="card-title"><?php echo $total_new; ?></h5>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card text-white bg-info mb-3">
            <div class="card-header">Total Pendding</div>
            <div class="card-body">
                <h5 class="card-title"><?php echo $total_pendding; ?></h5>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card text-white bg-success mb-3">
            <div class="card-header">Total Complate</div>
            <div class="card-body">
                <h5 class="card-title"><?php echo $total_complate; ?></h5>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card text-white bg-danger mb-3">
            <div class="card-header">Total Refuse</div>
            <div class="card-body">
                <h5 class="card-title"><?php echo $total_refuse; ?></h5>
            </div>
        </div>
    </div>
</div>

<table id="services" class="table-hover table-striped bg-white border shadow p-1" style="width:100%">
    <thead>
    <tr>
        <td>ID</td>
        <td>Store ID</td>
        <td>Status</td>
        <td>Created At</td>
        <td>Staff ID</td>
        <td>Started At</td>
        <td>Finished At</td>
    </tr>
    </thead>
</table>

<script type="text/javascript">
    $(document).ready( function () {
        $('#services').DataTable({
            "data":<?php echo json_encode($services,JSON_UNESCAPED_UNICODE);?>,
            "columns": [
                {"render": function(data,type,row){
                        return '<a href="dashboard?p=services&show&id='+row['id']+'">'+row['id']+'</a>';
                    }},
                { "data": "store_id" },
                {"render": function(data,type,row){
                        var x;
                        if(row['status'] == 0){
                            x = '<div class="badge badge-primary"> New Order</div>';
                        }if(row['status'] == 1){
                            x = '<div class="badge badge-info"> Pendding</div>';
                        }if(row['status'] == 2){
                            x = '<div class="badge badge-success"> Complate</div>';
                        }if(row['status'] == 3){
                            x = '<div class="badge badge-danger"> Refuse</div>';
                        }
                        return x;
                    }},
                { "data": "created_at" },
                { "data": "employee_id" },
                { "data": "start_at" },
                { "data": "finished_at" }
            ]
        });
    } );
</script>
