<?php
$db = new DB;
$service = $db->select('services',['where'=>['id'=>$_GET['id']]])[0];
?>
<div id="msg"></div>
<h1 class="text-center">Order #<?php echo $service['id']; ?></h1>
<div class="">
    <button id="refuse" class="btn btn-danger m-2">Refuse</button>
    <table class="table table-hover table-striped">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Details</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Order Type</td>
            <td><?php echo $service['type']; ?></td>
        </tr>
        <tr>
            <td>Store ID</td>
            <td><a href="dashboard?p=store&id=<?php echo $service['store_id']; ?>"><?php echo $service['store_id']; ?></a></td>
        </tr>
        <tr>
            <td>Data</td>
            <td>
                <?php
                foreach (json_decode($service['data']) as $key => $value){
                    echo '<b>'.str_replace('_',' ',$key)."</b>: <span>$value</span><br>";
                }
                ?>
            </td>
        </tr>
        <tr>
            <td>Created at</td>
            <td><?php echo $service['created_at']; ?></td>
        </tr>
        <tr>
            <td>Employee</td>
            <td><?php echo $service['employee_id']; ?></td>
        </tr>
        <tr>
            <td>Started at</td>
            <td><?php echo $service['start_at']; ?></td>
        </tr>
        <tr>
            <td>Finished at</td>
            <td><?php echo $service['finished_at']; ?></td>
        </tr>
        </tbody>
    </table>
    <div class="text-center">
        <?php
        if($service['status'] < 2){
            if($service['status'] != 1){
                echo '<button id="start" class="btn btn-info m-2">Start Services</button>';
            }
            echo '<button id="finished" class="btn btn-success m-2">Finished Services</button>';
        }elseif($service['status'] == 3){
            echo '<h3 class="text-danger text-center">This Service is Refuse ...</h3>';
        }else{
            echo '<h3 class="text-success text-center">This Service is Finished ...</h3>';
        }
        ?>
    </div>
</div>
<script>
    $('#start').click(function(){
        $.post('requests?start_services&store_id=<?php echo $service['store_id']; ?>&id=<?php echo $_GET['id']; ?>',function (data) {
            $('#msg').html(data);
        });
    });
    $('#finished').click(function(){
        $.post('requests?finished_services&store_id=<?php echo $service['store_id']; ?>&id=<?php echo $_GET['id']; ?>',function (data) {
            $('#msg').html(data);
        });
    });
    $('#refuse').click(function(){
        $.post('requests?refuse_services&store_id=<?php echo $service['store_id']; ?>&id=<?php echo $_GET['id']; ?>',function (data) {
            $('#msg').html(data);
        });
    });
</script>
