<?php

class Assets {
 public static function Get($file = Null,$format = false){
 	if(file_exists('./application/'.ASSETSDIR.'/'.$file)){
 		print('./application/'.ASSETSDIR.'/'.$file);
 	}else{
 		if($format){
    		print('./application/'.ASSETSDIR.'/images/not_found.jpg');
 		}else{
 			print("File Not Found");
 		}
 	}
 }

}
