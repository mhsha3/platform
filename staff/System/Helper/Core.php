<?php
define( 'BASEDIR', str_replace("index.php","",$_SERVER['PHP_SELF']) );
session_start();

if(Builder){Route::set('Builder',function(){View::make('Builder');});}

function _error($x){return '<span style="color:red;">'.$x.'</span>';}
function __($x){(!is_array($x)) ? print($x) : print_r($x);}

function _Param($x=false){
  $param = explode('/', str_replace(BASEDIR, '', $_SERVER['REQUEST_URI']));
  return ($x) ? (isset($param[$x])) ? $param[$x] : _error('Not Found This Parametar') : $param;
}

function _SPA(){
  if(isset($_POST['_SPA__'])){
     return true;
  }else{
    return false;
  }
}

 function show_error(){
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
 }

function _Session($x,$v=true){
  $_SESSION[$x] = $v;
}

function _Lang(){
  if(!isset($_SESSION['_LANG_'])){
        _Session('_LANG_',LANG);
  }
  if(isset($_GET['lang'])){
       if(file_exists(LANGSDIR.$_GET['lang'].'.php')){
           _Session('_LANG_',$_GET['lang']);
       }
  }
  return $_SESSION['_LANG_'];
}

_Lang();

function _t($x=''){
      require_once(LANGSDIR._Lang().'.php');
      if(isset($x) && !empty($x) && isset($lang[$x])){
        return $lang[$x];
      }else{
        return _error('Not Found This Word In Language File');
      }
  }

function IsLogin(){
  if(isset($_SESSION['_SSID_']) && isset($_SESSION['_TOKEN_']) && $_SESSION['_TOKEN_'] == md5($_SESSION['_SSID_'].IP())){
        return true;
  }
}

function _Logout($x = false){
   unset($_SESSION);
   session_destroy();
   (!$x) ? $x = BASEDIR : $x = $x; 
   header('Location: '.$x);
}

function redirect($x = false){
   (!$x) ? $x = BASEDIR : $x = $x; 
   header('Location: '.$x);
}

function Content($sidebar,$x=false){
    if($x == 'static'){
        if(isset($_POST['_SPA_'])){
           return '';
        }else{
           if(file_exists('./application/content/'.$sidebar.'.php')){
               require_once ('./application/content/'.$sidebar.'.php');
            }else{__( _error('<h1 style="red">Not Found This Content</h1>'));} 
            }
    }else{
      if(file_exists('./application/content/'.$sidebar.'.php')){
         require_once ('./application/content/'.$sidebar.'.php');
        }else{
          __( _error('<h1 style="red">Not Found This Content</h1>') );
        }
      }
    }

function Root($pageRoot){
 $Root = explode(',',$_SESSION['_ROOT_']);
  if(in_array($pageRoot,$Root)){
      return true;
  }else{
      redirect('404');
      exit();
  }
}
function CheckRoot($pageRoot){
 $Root = explode(',',$_SESSION['_ROOT_']);
  if(in_array($pageRoot,$Root)){
      return true;
  }else{
      return false; 
  }
}

if(isset($_GET['logout'])){
    _Logout();
}

function IP()
{
    // Get real visitor IP behind CloudFlare network
    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
              $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
              $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
    }
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }

    return $ip;
}