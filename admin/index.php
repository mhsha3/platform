<?php
// // Version

// error_reporting(E_ALL);
// ini_set('display_errors', 0);
require_once( './../global.php' );
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

ini_set('memory_limit', '25600M');
ob_start();
define('VERSION', '3.0.2.0');
session_start();
if(!isset($_SESSION['_DBNAME_']) && !isset($_SESSION['_PATH_'])){
    header("Location: ${PLATFORM_DOMAIN}/site/login");
    exit();
}
// Configuration
if (is_file('config.php')) {
	require_once('config.php');
}

// Startup
require_once(DIR_SYSTEM . 'startup.php');

start('admin');

ob_end_flush();