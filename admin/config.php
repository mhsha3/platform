<?php
// HTTP

// define('HTTP_SERVER', 'http://lanaapp.com/admin/');
// define('HTTPS_SERVER', 'https://lanaapp.com/admin/');

define('HTTP_SERVER', PLATFORM_DOMAIN . "/admin/");
define('HTTPS_SERVER', PLATFORM_DOMAIN . "/admin/");

if(isset($_SESSION['_DOMAIN_']) && isset($_SESSION['_DOMAIN_'])){
    define('HTTP_CATALOG', 'http://'.$_SESSION['_DOMAIN_'].'/'); // edit
    define('HTTPS_CATALOG', 'https://'.$_SESSION['_DOMAIN_'].'/'); // edit
}else{
    define('HTTP_CATALOG', 'http://lnasa.space/'.$_SESSION['_PATH_'].'/'); // edit
    define('HTTPS_CATALOG', 'https://lnasa.space/'.$_SESSION['_PATH_'].'/'); // edit
}
// DIR
define('DIR_APPLICATION', HOME_DIR . '/admin/');
define('DIR_SYSTEM', HOME_DIR . '/FRONTCORE/system/'); // edit
define('DIR_SYSTEM_STORAGE', HOME_DIR . '/lnasa.space/'.$_SESSION['_PATH_'].'/system/'); // edit
define('DIR_IMAGE', HOME_DIR . '/lnasa.space/'.$_SESSION['_PATH_'].'/image/'); // edit
//define('DIR_STORE_TEMPLATE',HOME_DIR . '/lnasa.space/'.$_SESSION['_PATH_'].'/catalog/view/theme/');
define('DIR_STORE_TEMPLATE',HOME_DIR . '/FRONTCORE/catalog/view/theme/');
define('DIR_STORAGE', DIR_SYSTEM_STORAGE . 'storage/');
//define('DIR_CATALOG', HOME_DIR . '/lnasa.space/'.$_SESSION['_PATH_'].'/catalog/');
define('DIR_CATALOG', HOME_DIR . '/FRONTCORE/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', PLATFORM_DB_HOST);
define('DB_USERNAME', PLATFORM_DB_USER);
define('DB_PASSWORD', PLATFORM_DB_PASS);
define('DB_DATABASE', $_SESSION['_DBNAME_']); // edit
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', '');
