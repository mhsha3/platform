<?php
class ControllerHelpFaq extends Controller {
    public function index() {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        
        $this->load->language('common/global');
        $this->document->setTitle($this->language->get('text_faq'));
        $data['user_token'] = $this->session->data['user_token'];
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_faq'),
            'href' => $this->url->link('common/platformupdates', 'user_token=' . $this->session->data['user_token'], true)
        );
        
        $this->load->model('common/lanaapp');
        $data['faqs'] = $this->model_common_lanaapp->getFAQ();
        
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('help/faq', $data));
    }
}
