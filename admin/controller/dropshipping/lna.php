<?php
class ControllerDropshippingLna extends Controller {
    public function index() {
        $assets_image = 'https://assets.dropshipping.lanaapp.com/';
        $this->load->language('common/global');
        $this->document->setTitle($this->language->get('textext_dropshipping_lnat_faq'));
        $data['user_token'] = $this->session->data['user_token'];
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_dropshipping_lna'),
            'href' => $this->url->link('dropshipping/lna', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['products'] = [];
        $this->load->model('common/lanaapp');
        foreach($this->model_common_lanaapp->getLnaProducts() as $result){
            $data['products'][] = [
                'product_id'  =>$result['product_id'],
                'name' => ($this->language->get('code') == 'ar') ? $result['name_arabic'] : $result['name_english'],
                'image' =>  $assets_image.$result['image'],
                'price' =>  $this->currency->format($result['price'],$this->config->get('config_currency')),
                'quantity'=>$result['quantity'],
            ];
        }
        $data['categories'] = $this->model_common_lanaapp->getLnaCategories();
        $data['subcategories'] = $this->model_common_lanaapp->getLnaSubCategories();

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('dropshipping/lna', $data));
    }

    public function save_image($inPath,$outPath){
        $in   = fopen($inPath, "rb");
        if(!is_dir($outPath)){mkdir($outPath);}
        $name = $outPath.'/'.uniqid().md5(microtime()).time().'.jpg';
        $out  = fopen($name, "wb");
        while ($chunk = fread($in,8192)) {fwrite($out, $chunk, 8192);}
        fclose($in);
        fclose($out);
        return str_replace(DIR_IMAGE, '', $name);
    }

    public function _add(){
        if(isset($this->request->post['_p'])){

            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
            $assets_image = 'https://assets.dropshipping.lanaapp.com/';
            $this->load->model('common/lanaapp');
            $product = $this->model_common_lanaapp->getLnaGetProduct((int)$this->request->post['_p']);
            if($product){
                $data['model']       		=  $product['product_id'].'-'.$product['supplier_code'];
                $data['product_description'][1]['name']        =  $product['name_english'];
                $data['product_description'][1]['description'] =  $product['description_en'];
                $data['product_description'][1]['meta_title']  =  $product['name_english'];
                $data['product_description'][1]['meta_description'] = $product['name_english'];
                $data['product_description'][1]['tag'] = $product['name_english'];
                $data['product_description'][1]['meta_keyword'] = $product['name_english'];

                $data['product_description'][2]['name']        =  $product['name_arabic'];
                $data['product_description'][2]['description'] =  $product['description_ar'];
                $data['product_description'][2]['meta_title']  =  $product['name_arabic'];
                $data['product_description'][2]['meta_description'] = $product['name_arabic'];
                $data['product_description'][2]['tag'] = $product['name_arabic'];
                $data['product_description'][2]['meta_keyword'] = $product['name_arabic'];

                $img = $this->save_image($assets_image.$product['image'],DIR_IMAGE.'catalog/dropshipping');
                $data['product_image']       =  $img;
                $data['price']      			=  ( $product['price'] * (100+20/100 ) );
                $data['product_cost']           =  $product['price'];
                $data['quantity']         =  $product['quantity'];
                $data['dropshipping'] = 1;
                $data['image'] = $img;
                //default values, can be change later
                $data['status'] = 1; //true
                $data['minimum'] = 1; //true
                $data['subtract'] = 1; //true
                $data['stock_status_id'] = 7; // in stock
                $data['date_available'] = date('Y-m-d', strtotime("-1 day"));
                //these fields are not required
                $data['location'] = '';
                $data['sku'] = $product['product_id'].'-'.$product['supplier_code'];
                $data['upc'] = '';
                $data['ean'] = '';
                $data['jan'] = '';
                $data['isbn'] = '';
                $data['mpn'] = '';
                $data['manufacturer_id'] = '';
                $data['shipping'] = '';
                $data['points'] = '';
                $data['weight'] = '';
                $data['weight_class_id'] = '';
                $data['length'] = '';
                $data['width'] = '';
                $data['height'] = '';
                $data['length_class_id'] = '';
                $data['tax_class_id'] = '';
                $data['sort_order'] = '';
                // product discount
                $data['product_discount'] = array();
                //empty arrays
                $data['product_image']	= array();
                $data['product_download']	= array();
                $data['product_filter']	= array();
                $data['product_related']	= array();
                $data['product_reward']	= array();
                $data['product_layout']	= array();
                $data['product_store'] = array('0');
                $this->load->model('catalog/product');
                $product_id = $this->model_catalog_product->addProduct($data);
                if($product_id) {
                    echo 'done isa';
                }
            }else{
                echo '0';
            }
        }
    }
}
