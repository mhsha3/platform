<?php
class ControllerStartupLogin extends Controller {
	public function index() {
	    // Edit mohab
        if(!isset($_SESSION['_DBNAME_']) || !isset($_SESSION['_PATH_'])){
            $this->user->logout();
            unset($this->session->data['user_token']);
            unset($this->session->data['user_id']);
            session_unset();
            session_destroy();
            header("Location: ${PLATFORM_DOMAIN}/site/login");
            exit();
        }

		$route = isset($this->request->get['route']) ? $this->request->get['route'] : '';

		$ignore = array(
			'common/login',
			'common/forgotten',
			'common/reset'
		);

		// User
		$this->registry->set('user', new Cart\User($this->registry));

		if (!$this->user->isLogged() && !in_array($route, $ignore)) {
			return new Action('common/login');
		}

		if (isset($this->request->get['route'])) {
			$ignore = array(
				'common/login',
				'common/logout',
				'common/forgotten',
				'common/reset',
				'error/not_found',
				'error/permission'
			);

			if (!in_array($route, $ignore) && (!isset($this->request->get['user_token']) || !isset($this->session->data['user_token']) || ($this->request->get['user_token'] != $this->session->data['user_token']))) {
				return new Action('common/login');
			}
		} else {
			if (!isset($this->request->get['user_token']) || !isset($this->session->data['user_token']) || ($this->request->get['user_token'] != $this->session->data['user_token'])) {
				return new Action('common/login');
			}
		}
	}
}
