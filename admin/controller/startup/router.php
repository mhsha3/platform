<?php
class ControllerStartupRouter extends Controller {
	public function index() {
		// Eng.Mohab Mamdouh Edition -->
		$ignore = array(
				'startup/router',
				'marketplace/marketplace',
				'marketplace/installer',
			//	'marketplace/extension',
				'marketplace/modification',
				'marketplace/event',
			//	'design/layout',
				'design/theme',
				'design/translation',
				'design/seo_url',
			//	'tool/backup',
				'tool/upload',
				'tool/log',
			 //  'user/api'
			);
		// Route
		if (isset($this->request->get['route']) && !in_array($this->request->get['route'], $ignore)) {
			// When renew package Eng.Mohab Mamdouh ->
			if(max(0,ceil((strtotime($this->config->get('config_store_expire'))-time())/86400)) != 0){
		     	$route = $this->request->get['route'];
			}elseif($this->request->get['route'] == 'common/login'){
				$route = $this->request->get['route'];
			}elseif($this->request->get['route'] == 'common/logout'){
				$route = $this->request->get['route'];
			}else{
				$route = 'common/subscription';
			}
		} else {
			$route = $this->config->get('action_default');
		}
		
		$data = array();
		
		// Sanitize the call
		$route = preg_replace('/[^a-zA-Z0-9_\/]/', '', (string)$route);
		
		// Trigger the pre events
		$result = $this->event->trigger('controller/' . $route . '/before', array(&$route, &$data));
		
		if (!is_null($result)) {
			return $result;
		}
		
		$action = new Action($route);
		
		// Any output needs to be another Action object. 
		$output = $action->execute($this->registry, $data);
		
		// Trigger the post events
		$result = $this->event->trigger('controller/' . $route . '/after', array(&$route, &$output));
		
		if (!is_null($result)) {
			return $result;
		}
		
		return $output;
	}
}
