<?php
// mohab
class ControllerCommonColumnLeft extends Controller {
	public function index() {
		if (isset($this->request->get['user_token']) && isset($this->session->data['user_token']) && ($this->request->get['user_token'] == $this->session->data['user_token'])) {
			$this->load->language('common/column_left');

			// Create a 3 level menu array
			// Level 2 can not have children
            $this->load->model('setting/extension');
            $installed = $this->model_setting_extension->getInstalled('module');
            
			// Menu
			$data['menus'][] = array(
				'id'       => 'menu-dashboard',
				'icon'	   => 'fa-home',
				'name'	   => $this->language->get('text_dashboard'),
				'href'     => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true),
				'children' => array()
			);
			
			// Catalog
			$catalog = array();

			if ($this->user->hasPermission('access', 'catalog/product')) {
				$catalog[] = array(
					'name'	   => $this->language->get('text_product'),
					'href'     => $this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'catalog/category')) {
				$catalog[] = array(
					'name'	   => $this->language->get('text_category'),
					'href'     => $this->url->link('catalog/category', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}
            if(in_array('knawat_dropshipping',$installed)){
                if ($this->user->hasPermission('access', 'extension/module/knawat_dropshipping')) {
                    $catalog[] = array(
                        'name' => $this->language->get('text_knawat'),
                        'href' => $this->url->link('extension/module/knawat_dropshipping', 'user_token=' . $this->session->data['user_token'], true),
                        'children' => array()
                    );
                }
            }
			if ($this->user->hasPermission('access', 'catalog/recurring')) {
				$catalog[] = array(
					'name'	   => $this->language->get('text_recurring'),
					'href'     => $this->url->link('catalog/recurring', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}
			if ($this->user->hasPermission('access', 'catalog/filter')) {
				$catalog[] = array(
					'name'	   => $this->language->get('text_filter'),
					'href'     => $this->url->link('catalog/filter', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}
			// Attributes
			$attribute = array();
			if ($this->user->hasPermission('access', 'catalog/attribute')) {
				$attribute[] = array(
					'name'     => $this->language->get('text_attribute'),
					'href'     => $this->url->link('catalog/attribute', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'catalog/attribute_group')) {
				$attribute[] = array(
					'name'	   => $this->language->get('text_attribute_group'),
					'href'     => $this->url->link('catalog/attribute_group', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($attribute) {
				$catalog[] = array(
					'name'	   => $this->language->get('text_attribute'),
					'href'     => '',
					'children' => $attribute
				);
			}

			if ($this->user->hasPermission('access', 'catalog/option')) {
				$catalog[] = array(
					'name'	   => $this->language->get('text_option'),
					'href'     => $this->url->link('catalog/option', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'catalog/manufacturer')) {
				$catalog[] = array(
					'name'	   => $this->language->get('text_manufacturer'),
					'href'     => $this->url->link('catalog/manufacturer', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'catalog/download')) {
				$catalog[] = array(
					'name'	   => $this->language->get('text_download'),
					'href'     => $this->url->link('catalog/download', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

            if($this->user->hasPermission('access', 'design/banner')){
                $catalog[] = array(
                    'name'	   => ($this->config->get('config_packages_title')[$this->config->get('config_language_id')] ?? $this->language->get('text_package_service')) . $this->language->get('text_demo'),
                    'href'     => $this->url->link('catalog/packages', 'user_token=' . $this->session->data['user_token'], true),
                    'children' => array()
                );
            }
            
            if ($this->user->hasPermission('access', 'design/banner')) {
                $catalog[] = array(
                    'name'	   => $this->language->get('text_banner'),
                    'href'     => $this->url->link('design/banner', 'user_token=' . $this->session->data['user_token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'design/banner')) {
                $catalog[] = array(
                    'name'	   => $this->language->get('text_header_noteice'),
                    'href'     => $this->url->link('design/headernotice', 'user_token=' . $this->session->data['user_token'], true),
                    'children' => array()
                );
            }

            if ($catalog) {
				$data['menus'][] = array(
					'id'       => 'menu-catalog',
					'icon'	   => 'fa-cubes',
					'name'	   => $this->language->get('text_catalog'),
					'href'     => '',
					'children' => $catalog
				);
			}

			$sale = array();

			if ($this->user->hasPermission('access', 'sale/order')) {
				$sale[] = array(
					'name'	   => $this->language->get('text_sale'),
					'href'     => $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'sale/recurring')) {
				$sale[] = array(
					'name'	   => $this->language->get('text_recurring'),
					'href'     => $this->url->link('sale/recurring', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'sale/return')) {
				$sale[] = array(
					'name'	   => $this->language->get('text_return'),
					'href'     => $this->url->link('sale/return', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			// Voucher
			$voucher = array();

			if ($this->user->hasPermission('access', 'sale/voucher')) {
				$voucher[] = array(
					'name'	   => $this->language->get('text_voucher'),
					'href'     => $this->url->link('sale/voucher', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'sale/voucher_theme')) {
				$voucher[] = array(
					'name'	   => $this->language->get('text_voucher_theme'),
					'href'     => $this->url->link('sale/voucher_theme', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($voucher) {
				$sale[] = array(
					'name'	   => $this->language->get('text_voucher'),
					'href'     => '',
					'children' => $voucher
				);
			}

			if ($sale) {
				$data['menus'][] = array(
					'id'       => 'menu-sale',
					'icon'	   => 'fa-shopping-cart',
					'name'	   => $this->language->get('text_order'),
					'href'     => '',
					'children' => $sale
				);
			}


			// Customer
			$customer = array();

			if ($this->user->hasPermission('access', 'customer/customer')) {
				$customer[] = array(
					'name'	   => $this->language->get('text_customer'),
					'href'     => $this->url->link('customer/customer', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'customer/customer_group')) {
				$customer[] = array(
					'name'	   => $this->language->get('text_customer_group'),
					'href'     => $this->url->link('customer/customer_group', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'customer/customer_approval')) {
				$customer[] = array(
					'name'	   => $this->language->get('text_customer_approval'),
					'href'     => $this->url->link('customer/customer_approval', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'customer/custom_field')) {
				$customer[] = array(
					'name'	   => $this->language->get('text_custom_field'),
					'href'     => $this->url->link('customer/custom_field', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($customer) {
				$data['menus'][] = array(
					'id'       => 'menu-customer',
					'icon'	   => 'fa-user',
					'name'	   => $this->language->get('text_customer'),
					'href'     => '',
					'children' => $customer
				);
			}

			$report = array();

            if ($this->user->hasPermission('access', 'extension/analytics/google')) {
                $report[] = array(
                    'name'	   => $this->language->get('text_google_analytics'),
                    'href'     => $this->url->link('extension/analytics/google', 'user_token=' . $this->session->data['user_token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'report/report')) {
                $report[] = array(
                    'name'	   => $this->language->get('text_reports'),
                    'href'     => $this->url->link('report/report', 'user_token=' . $this->session->data['user_token'], true),
                    'children' => array()
                );
            }

			if ($this->user->hasPermission('access', 'report/online')) {
				$report[] = array(
					'name'	   => $this->language->get('text_online'),
					'href'     => $this->url->link('report/online', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'report/statistics')) {
				$report[] = array(
					'name'	   => $this->language->get('text_statistics'),
					'href'     => $this->url->link('report/statistics', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			$data['menus'][] = array(
				'id'       => 'menu-report',
				'icon'	   => 'fa-chart-pie',
				'name'	   => $this->language->get('text_reports'),
				'href'     => '',
				'children' => $report
			);

			// Reviews
			if ($this->user->hasPermission('access', 'catalog/review')) {
				$data['menus'][] = array(
					'id'       => 'menu-review',
					'icon'	   => 'fa-star-half-alt',
					'name'	   => $this->language->get('text_review'),
					'href'     => $this->url->link('catalog/review', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			// Information Pages
			if ($this->user->hasPermission('access', 'catalog/information')) {
				$data['menus'][] = array(
					'id'       => 'menu-information',
					'icon'	   => 'fa-newspaper',
					'name'	   => $this->language->get('text_information'),
					'href'     => $this->url->link('catalog/information', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			// Marketing
			$marketing = array();
			
            if ($this->user->hasPermission('access', 'extension/module/msegatsendsms')) { 
                $marketing[] = array( 
                    'name' => $this->language->get('text_sms_services'), 
                    'href' => $this->url->link('extension/module/msegatsendsms', 'user_token=' . $this->session->data['user_token'], true), 
                    'children' => array() 
                    ); 
            };
	
			if ($this->user->hasPermission('access', 'marketing/marketing')) {
				$marketing[] = array(
					'name'	   => $this->language->get('text_marketing'),
					'href'     => $this->url->link('marketing/marketing', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'marketing/coupon')) {
				$marketing[] = array(
					'name'	   => $this->language->get('text_coupon'),
					'href'     => $this->url->link('marketing/coupon', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'marketing/contact')) {
				$marketing[] = array(
					'name'	   => $this->language->get('text_contact'),
					'href'     => $this->url->link('marketing/contact', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($marketing) {
				$data['menus'][] = array(
					'id'       => 'menu-marketing',
					'icon'	   => 'fa-bullhorn',
					'name'	   => $this->language->get('text_marketing'),
					'href'     => '',
					'children' => $marketing
				);
			}
			// Multi-Vendor
			$mpmultivendor = array();
			$this->language->load('mpmultivendor/adminlink');
			if ($this->user->hasPermission('access', 'mpmultivendor/mpmultivendor')) {
				$mpmultivendor[] = array(
					'name'	   => $this->language->get('text_mpsetting'),
					'href'     => $this->url->link('mpmultivendor/mpmultivendor', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'mpmultivendor/mpseller')) {
				$mpmultivendor[] = array(
					'name'	   => $this->language->get('text_mpseller'),
					'href'     => $this->url->link('mpmultivendor/mpseller', 'user_token=' . $this->session->data['user_token'],true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'mpmultivendor/order')) {
				$mpmultivendor[] = array(
					'name'	   => $this->language->get('text_seller_orders'),
					'href'     => $this->url->link('mpmultivendor/order', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'mpmultivendor/commission')) {
				$mpmultivendor[] = array(
					'name'	   => $this->language->get('text_seller_commissions'),
					'href'     => $this->url->link('mpmultivendor/commission', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'mpmultivendor/payment')) {
				$mpmultivendor[] = array(
					'name'	   => $this->language->get('text_seller_payments'),
					'href'     => $this->url->link('mpmultivendor/payment', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'mpmultivendor/product')) {
				$mpmultivendor[] = array(
					'name'	   => $this->language->get('text_seller_products'),
					'href'     => $this->url->link('mpmultivendor/product', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'mpmultivendor/enquiries')) {
				$mpmultivendor[] = array(
					'name'	   => $this->language->get('text_seller_enquiries'),
					'href'     => $this->url->link('mpmultivendor/enquiries', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'mpmultivendor/mpseller_message')) {
				if(!$this->config->has('mpmultivendor_status')) {
					$this->db->query("CREATE TABLE IF NOT EXISTS `". DB_PREFIX ."mpseller_message` (`mpseller_message_id` int(11) NOT NULL AUTO_INCREMENT, `mpseller_id` int(11) NOT NULL, `from` varchar(100) NOT NULL, `message` text NOT NULL, `read_status` int(11) NOT NULL, `date_added` datetime NOT NULL, PRIMARY KEY (`mpseller_message_id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=0");
				}

				// Total unread Messages
				$this->load->model('mpmultivendor/mpseller_message');
				$total_unreads_messages = $this->model_mpmultivendor_mpseller_message->getTotalAllUnreadMessages();

				$text_unreads_messages = sprintf($this->language->get('total_unreads_messages'), $total_unreads_messages);

				$text_seller_message = $this->language->get('text_seller_message');

				if($total_unreads_messages) {
					 $text_seller_message .=  $text_unreads_messages;
				}

				$mpmultivendor[] = array(
					'name'	   => $text_seller_message,
					'href'     => $this->url->link('mpmultivendor/mpseller_message', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'mpmultivendor/review')) {
				$mpmultivendor[] = array(
					'name'	   => $this->language->get('text_seller_review'),
					'href'     => $this->url->link('mpmultivendor/review', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($mpmultivendor) {
				$data['menus'][] = array(
					'id'       => 'menu-mpmultivendor',
					'icon'	   => 'fa-users',
					'name'	   => $this->language->get('text_mpmultivendor'),
					'href'     => '',
					'children' => $mpmultivendor
				);
			}
			/**** MP Seller Ends ****/

			// POS
			if ($this->user->hasPermission('access', 'sale/order')) {
                $openpos = array();
                $openpos[] = array(
					'name'	   => $this->language->get('text_dashboard'),
					'href'     => $this->url->link('extension/module/openpos', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
				$openpos[] = array(
					'name'	   => $this->language->get('text_product'),
					'href'     => $this->url->link('extension/module/openpos/products', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
				$openpos[] = array(
					'name'	   => $this->language->get('text_cash_management'),
					'href'     => $this->url->link('extension/module/openpos/transactions', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
				$openpos[] = array(
					'name'	   => $this->language->get('text_registers'),
					'href'     => $this->url->link('extension/module/openpos/registers', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
				$openpos[] = array(
					'name'	   => $this->language->get('text_setting'),
					'href'     => $this->url->link('extension/module/openpos/stores', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
                $openpos[] = array(
					'name'	   => $this->language->get('text_login_session'),
					'href'     => $this->url->link('extension/module/openpos/sessions', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
                $data['menus'][] = array(
					'id'       => 'menu-pos',
					'icon'	   => 'fa-qrcode',
					'name'	   => $this->language->get('text_pos'),
					'href'     => '',
					'children' => $openpos
				);

			}

			// services
			$services = array();

			if ($this->user->hasPermission('access', 'common/services')) {
				$services[] = array(
					'name'	   => $this->language->get('text_services'),
					'href'     => $this->url->link('common/services', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'common/services')) {
				$services[] = array(
					'name'	   => $this->language->get('text_payment'),
					'href'     => $this->url->link('common/services/payment', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'common/services')) {
				$services[] = array(
					'name'	   => $this->language->get('text_shipping'),
					'href'     => $this->url->link('common/services/shipping', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			$data['menus'][] = array(
				'id'       => 'menu-star',
				'icon'	   => 'fa-star',
				'name'	   => $this->language->get('text_services'),
				'href'     => '',
				'children' => $services
			);

			// System
			$system = array();

			if ($this->user->hasPermission('access', 'setting/setting')) {
				$system[] = array(
					'name'	   => $this->language->get('text_setting'),
					'href'     => $this->url->link('setting/store', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			// Users
			$user = array();

			if ($this->user->hasPermission('access', 'user/user')) {
				$user[] = array(
					'name'	   => $this->language->get('text_users'),
					'href'     => $this->url->link('user/user', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'user/user_permission')) {
				$user[] = array(
					'name'	   => $this->language->get('text_user_group'),
					'href'     => $this->url->link('user/user_permission', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}
            /*
			if ($this->user->hasPermission('access', 'user/api')) {
				$user[] = array(
					'name'	   => $this->language->get('text_api'),
					'href'     => $this->url->link('user/api', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}*/

			if ($user) {
				$system[] = array(
					'name'	   => $this->language->get('text_users'),
					'href'     => '',
					'children' => $user
				);
			}

			// Localisation
			$localisation = array();

			if ($this->user->hasPermission('access', 'localisation/location')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_location'),
					'href'     => $this->url->link('localisation/location', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'localisation/language')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_language'),
					'href'     => $this->url->link('localisation/language', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'localisation/currency')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_currency'),
					'href'     => $this->url->link('localisation/currency', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'localisation/stock_status')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_stock_status'),
					'href'     => $this->url->link('localisation/stock_status', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'localisation/order_status')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_order_status'),
					'href'     => $this->url->link('localisation/order_status', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			// Returns Mohab
			$return = array();

			if ($this->user->hasPermission('access', 'localisation/return_status')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_return_status'),
					'href'     => $this->url->link('localisation/return_status', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'localisation/return_action')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_return_action'),
					'href'     => $this->url->link('localisation/return_action', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'localisation/return_reason')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_return_reason'),
					'href'     => $this->url->link('localisation/return_reason', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}
/*
			if ($return) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_return'),
					'href'     => '',
					'children' => $return
				);
			} */

			if ($this->user->hasPermission('access', 'localisation/country')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_country'),
					'href'     => $this->url->link('localisation/country', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'localisation/zone')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_zone'),
					'href'     => $this->url->link('localisation/zone', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'localisation/geo_zone')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_geo_zone'),
					'href'     => $this->url->link('localisation/geo_zone', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			// Tax
			$tax = array();

			if ($this->user->hasPermission('access', 'localisation/tax_class')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_tax_class'),
					'href'     => $this->url->link('localisation/tax_class', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'localisation/tax_rate')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_tax_rate'),
					'href'     => $this->url->link('localisation/tax_rate', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}


			if ($this->user->hasPermission('access', 'localisation/length_class')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_length_class'),
					'href'     => $this->url->link('localisation/length_class', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($this->user->hasPermission('access', 'localisation/weight_class')) {
				$localisation[] = array(
					'name'	   => $this->language->get('text_weight_class'),
					'href'     => $this->url->link('localisation/weight_class', 'user_token=' . $this->session->data['user_token'], true),
					'children' => array()
				);
			}

			if ($localisation) {
				$system[] = array(
					'name'	   => $this->language->get('text_localisation'),
					'href'     => '',
					'children' => $localisation
				);
			}

			if ($system) {
				$data['menus'][] = array(
					'id'       => 'menu-system',
					'icon'	   => 'fa-cogs',
					'name'	   => $this->language->get('text_setting'),
					'href'     => '',
					'children' => $system
				);
			}

            $data['subscription_url'] = $this->url->link('common/subscription', 'user_token=' . $this->session->data['user_token'], true);

			// // Stats
			// $this->load->model('sale/order');

			// $order_total = $this->model_sale_order->getTotalOrders();

			// $this->load->model('report/statistics');

			// $complete_total = $this->model_report_statistics->getValue('order_complete');

			// if ((float)$complete_total && $order_total) {
			// 	$data['complete_status'] = round(($complete_total / $order_total) * 100);
			// } else {
			// 	$data['complete_status'] = 0;
			// }

			// $processing_total = $this->model_report_statistics->getValue('order_processing');

			// if ((float)$processing_total && $order_total) {
			// 	$data['processing_status'] = round(($processing_total / $order_total) * 100);
			// } else {
			// 	$data['processing_status'] = 0;
			// }

			// $other_total = $this->model_report_statistics->getValue('order_other');

			// if ((float)$other_total && $order_total) {
			// 	$data['other_status'] = round(($other_total / $order_total) * 100);
			// } else {
			// 	$data['other_status'] = 0;
			// }


			// User Info
			$data['logged'] = true;

			$data['home'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true);
			$data['logout'] = $this->url->link('common/logout', 'user_token=' . $this->session->data['user_token'], true);
			$data['profile'] = $this->url->link('common/profile', 'user_token=' . $this->session->data['user_token'], true);
			$data['wallet_url'] = $this->url->link('common/wallet', 'user_token=' . $this->session->data['user_token'], true);

			$this->load->model('user/user');

			$this->load->model('tool/image');

			$user_info = $this->model_user_user->getUser($this->user->getId());
			if ($user_info) {
				$data['firstname'] = $user_info['firstname'];
				$data['lastname'] = $user_info['lastname'];
				$data['username']  = $user_info['username'];
				$data['user_group'] = $user_info['user_group'];

				if (is_file(DIR_IMAGE . $user_info['image'])) {
					$data['image'] = $this->model_tool_image->resize($user_info['image'], 45, 45);
				} else {
					$data['image'] = $this->model_tool_image->resize('profile.png', 45, 45);
				}
			} else {
				$data['firstname'] = '';
				$data['lastname'] = '';
				$data['user_group'] = '';
				$data['image'] = '';
			}

			// Online Stores
			$data['stores'] = array();

			$data['stores'][] = array(
				'name' => $this->config->get('config_name'),
				'href' => HTTP_CATALOG
			);

			$this->load->model('setting/store');

			$results = $this->model_setting_store->getStores();

			foreach ($results as $result) {
				$data['stores'][] = array(
					'name' => $result['name'],
					'href' => $result['url']
				);
			}
		   // Mohab Mamdouh Edition -->
           $packageslist = [
               '1'=>'demo_package',
               '2'=>'paid_package',
               '10'=>'monshaat_package'
           ];
		   $data['change_language_url'] = 'index.php?route=common/column_left/req&user_token='.$this->session->data['user_token'];
		   $this->load->model('localisation/language');
		   $this->load->model('sale/order');
		   $this->load->model('common/wallet');
		   $this->load->model('common/storenotifications');
		   $data['store_expire'] = max(0,ceil((strtotime($this->config->get('config_store_expire'))-time())/86400));
		   $data['store_logo'] = $this->model_tool_image->resize($this->config->get('config_logo'), 120, 45);
		   $data['store_name'] = $this->config->get('config_name');
		   $data['store_package_name'] = $this->language->get('text_'.$packageslist[$this->config->get('config_store_package_id')]);
		   $data['store_id'] = (int)$this->config->get('config_lana_store_id');
		   $data['text_store_id'] = $this->language->get('text_store_id');
		   $data['text_controller'] = $this->language->get('text_controller');
		   $data['text_orders'] = $this->language->get('text_order');
		   $data['total_orders'] = $this->model_sale_order->getTotalOrders();
		   $data['orders_url'] = $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'], true);
	       $data['store_url'] = HTTPS_CATALOG;
		   $data['languages'] = $this->model_localisation_language->getLanguages();
		   $data['is_language'] = $this->model_localisation_language->getLanguageByCode($this->config->get('config_admin_language'));
		   $data['total_balance'] = (int)$this->model_common_wallet->TotalBalance();
		   $data['total_notifications'] = (int)$this->model_common_storenotifications->getTotalNotifications();
		   $data['notifications_url'] = $this->url->link('common/notifications', 'user_token=' . $this->session->data['user_token'], true);

			return $this->load->view('common/column_left', $data);
		}
	}

    public function req() {
        if (isset($this->request->get['user_token']) && isset($this->session->data['user_token']) && ($this->request->get['user_token'] == $this->session->data['user_token']) && isset($this->request->post['_lang'])){
            $this->load->model('localisation/language');
            $lang = $this->model_localisation_language->getLanguageByCode($this->request->post['_lang']);
            if($lang){
                $this->db->query("UPDATE " . DB_PREFIX . "setting SET `value` = '" . $lang['code'] . "' WHERE `key`='config_admin_language' and `code`='config' and store_id = '0'");
                echo 1;
            }
        }else{
            $this->response->redirect($this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true));
        }
    }
}
