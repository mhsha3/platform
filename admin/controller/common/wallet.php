<?php
class ControllerCommonWallet extends Controller {
	public function index() {
		$this->load->language('common/wallet');
		$this->document->setTitle($this->language->get('heading_title'));
		$data['user_token'] = $this->session->data['user_token'];
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('common/wallet', 'user_token=' . $this->session->data['user_token'], true)
		);
		$this->load->model('common/wallet');
		$data['total_balance'] = $this->currency->format($this->model_common_wallet->TotalBalance(),$this->config->get('config_currency'),'', true);
		$transactions = $this->model_common_wallet->GetTransactions();
		foreach ($transactions as $key => $value) {
			$transactions[$key]['proccess'] = ($value['amount'] < 0) ? '1' : '0';
			if($value['type'] == 'paylink_payment'){
				$value['amount'] = $value['amount'] - ($value['amount'] * 2.5/100);
			}
			($value['supplier_cost'] > 0) ? $shipping = 30 : $shipping = 0;
			if($value['fees'] > 0){
				$value['amount'] = $value['amount'] - $value['fees'];
			}
			$transactions[$key]['amount'] = $value['amount'];
			$transactions[$key]['balance'] = abs($value['amount']) - abs($value['supplier_cost']) - $shipping;
			if($value['amount'] < 0){
               $total[] = $value['amount'];
			}else{
				$total[] = $transactions[$key]['balance'];
			}
		}
		$data['transactions']  = $transactions;
		$data['total_balance'] = $this->currency->format(array_sum($total),$this->config->get('config_currency'),'', true);
		$data['balance_color'] = ($this->model_common_wallet->TotalBalance() < 0) ? 'danger' : 'primary';
		$data['goals_target'] = $this->config->get('config_goals_target');
		$data['target_bar'] = round(($this->model_common_wallet->TotalTransactionsByMonth(date("m"))/$this->config->get('config_goals_target'))*100);
		$data['goals_date'] = date("Y M");
		$data['text_wallet'] = $this->language->get('heading_title');
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('common/wallet', $data));		
	}
}