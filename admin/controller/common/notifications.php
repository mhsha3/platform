<?php
class ControllerCommonNotifications extends Controller {
    public function index() {
        $this->load->language('common/global');
        $this->document->setTitle($this->language->get('text_notifications_center'));
        $data['user_token'] = $this->session->data['user_token'];
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_notifications_center'),
            'href' => $this->url->link('common/notifications', 'user_token=' . $this->session->data['user_token'], true)
        );
        $this->load->model('common/storenotifications');
        $data['notifications'] = $this->model_common_storenotifications->getNotifications();
        
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('common/notifications', $data));
    }    
    public function read() {
        if(isset($this->request->get['id'])){
        $this->load->language('common/global');
        $this->document->setTitle($this->language->get('text_notifications_center'));
        $data['user_token'] = $this->session->data['user_token'];
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_notifications_center'),
            'href' => $this->url->link('common/notifications', 'user_token=' . $this->session->data['user_token'], true)
        );
        $this->load->model('common/storenotifications');
        $data['notification'] = $this->model_common_storenotifications->getNotificationById($this->request->get['id']);
        if($data['notification']){
            $this->model_common_storenotifications->viewedNotificationById($this->request->get['id']);
        }else{
            $this->response->redirect($this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true));
        }
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
            $this->response->setOutput($this->load->view('common/notification_content', $data));
        }else{
            $this->response->redirect($this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true));
        }
    }
}
