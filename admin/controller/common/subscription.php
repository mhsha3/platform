<?php
class ControllerCommonSubscription extends Controller{
    protected function Auth(){
        $apiId = 'APP_ID_1603819975';
        $secretKey = 'b520d0b3-ded7-3f07-a038-42bc08643df5';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://restapi.paylink.sa/api/auth');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, '{"apiId":"' . $apiId . '","persistToken":false,"secretKey":"' . $secretKey . '"}');
        $headers = [];
        $headers[] = 'Accept: */*';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        return json_decode(curl_exec($ch), true)['id_token'];
    }

    protected function PaylinkInvoice($amount = 0, $type=''){
        $this->load->model('user/user');
        $user_info = $this->model_user_user->getUser($this->user->getId());
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://restapi.paylink.sa/api/addInvoice');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        if($type == 'month'){
            $ar_type = 'اشتراك شهري';
        }else{
            $ar_type = 'اشتراك سنوى';
        }
        curl_setopt($ch, CURLOPT_POSTFIELDS, '{
           "amount": ' . $amount . ',
           "callBackUrl": "' . $this->url->link('common/subscription/confirm','', true) . '&user_token='.$this->session->data['user_token'].'",
           "clientEmail": "' . $user_info['email'] . '",
           "clientMobile": "",
           "clientName": "' . $user_info['firstname'] . ' ' . $user_info['lastname'] . '",
           "note": "#' . $this->config->get('config_lana_store_id') . '- Subscription @' . $type . ' buy ' . $amount . '",
           "orderNumber": "' . $this->config->get('config_lana_store_id') . '",
           "products": [{
              "description": "'.$ar_type.'",
              "imageSrc": "https://lanaapp.com/admin/view/image/logo.png",
              "price": '.$amount.',
              "qty": 1,
              "title": "اشتراك فى منصة لنا"
            }]
        }');
        $headers = array();
        $headers[] = 'Accept: application/json;charset=UTF-8';
        $headers[] = 'Authorization: Bearer ' . $this->Auth();
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = json_decode(curl_exec($ch), true);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        return $result;
    }

    public function index(){
        $this->load->language('common/global');
        $this->document->setTitle($this->language->get('text_subscription'));
        $data['user_token'] = $this->session->data['user_token'];
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_subscription'),
            'href' => $this->url->link('common/subscription', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['month_paylink_pay'] = $this->PaylinkInvoice('349', 'month')['url'];
        $data['year_paylink_pay'] = $this->PaylinkInvoice('2999', 'year')['url'];
        $data['month_stc_pay'] = '';
        $data['year_stc_pay'] = '';

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('common/subscription', $data));
    }

    public function confirm(){
        $this->load->language('common/global');
        if (isset($this->request->get['orderNumber']) && isset($this->request->get['transactionNo'])) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://restapi.paylink.sa/api/getInvoice/' . (int)$this->request->get['transactionNo']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                $headers = [];
                $headers[] = 'Accept: application/json;charset=UTF-8';
                $headers[] = 'Authorization: Bearer ' . $this->Auth();
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                $result = curl_exec($ch);
                if (curl_errno($ch)) {
                    echo 'Error:' . curl_error($ch);
                }
                curl_close($ch);
                $result = json_decode($result, true);


                if($result['orderStatus'] == 'Paid'){

                    if($this->config->get('config_store_package_id') == '1'){
                        if(strpos($result['gatewayOrderRequest']['note'], '@year')){
                            $new_date = date('Y-m-d',strtotime("+365 days"));
                        }else{
                            $new_date = date('Y-m-d',strtotime("+30 days"));
                        }
                    }else{
                        $have = max(0,ceil((strtotime($this->config->get('config_store_expire'))-time())/86400));
                        if(strpos($result['gatewayOrderRequest']['note'], '@year')){
                            $new_date = date('Y-m-d',strtotime("+".($have+365)." days"));
                        }else{
                            $new_date = date('Y-m-d',strtotime("+".($have+30)." days"));
                        }
                    }
                    $this->load->model('setting/setting');
                    $this->load->model('common/lanaapp');
                    $this->model_common_lanaapp->action("UPDATE `stores` SET `expire_date`='$new_date' WHERE `id` = '".$this->config->get('config_lana_store_id')."'");
                    $this->model_setting_setting->editSettingValue('lana_setting','config_store_expire',$new_date);
                    $this->model_setting_setting->editSettingValue('lana_setting','config_store_package_id','2');
                    $this->model_setting_setting->editSettingValue('lana_setting','config_store_package_name','باقة مدفوعة');

                    $this->session->data['subscription_msg'] = '<div class="alert alert-seccuss">'.$this->language->get('text_done_paid').'</div>';

                    $this->response->redirect($this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true));

                }else{
                    $this->session->data['subscription_msg'] = '<div class="alert alert-danger">'.$this->language->get('text_invalid_paid').'</div>';
                    $this->response->redirect($this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true));
                }

        }
    }

    public function Stcpay(){
        $this->load->language('common/global');
        if(isset($this->request->post['num']) && isset($this->request->post['x'])){
            if( isset($this->session->data['OtpReference']) && isset($this->session->data['STCPayPmtReference']) ){
                unset($this->session->data['OtpReference']);
                unset($this->session->data['STCPayPmtReference']);
            }
            if($this->request->post['x'] == 'y'){
                $amount = 2999;
            }else{
                $amount = 349;
            }
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://b2b.stcpay.com.sa/B2B.DirectPayment.WebApi/DirectPayment/V4/DirectPaymentAuthorize",
                CURLOPT_SSLCERT => "/home/lanaapps/public_html/admin/ca.pem",
                CURLOPT_SSLKEY => "/home/lanaapps/public_html/admin/key.pem",
                CURLOPT_SSLCERTTYPE => "PEM",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS =>'{
                    "DirectPaymentAuthorizeV4RequestMessage": {
                     "BranchID": "string",
                     "TellerID": "string",
                     "DeviceID": "string",
                     "RefNum": "123456",
                     "BillNumber": "6777",
                     "MobileNo": "'.$this->request->post['num'].'",
                     "Amount": '.$amount.',
                     "MerchantNote": "#'.$this->config->get('config_lana_store_id').'"
                     }
                    }',
                CURLOPT_HTTPHEADER => array(
                    "X-ClientCode: 74482575955",
                    "Content-Type: application/json"
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_errno($curl);
            $info = curl_getinfo($curl);
            curl_close($curl);
            $json = json_decode($response,true);
            if(isset($json['DirectPaymentAuthorizeV4ResponseMessage'])){
                $this->session->data['OtpReference'] = $json['DirectPaymentAuthorizeV4ResponseMessage']['OtpReference'];
                $this->session->data['STCPayPmtReference'] = $json['DirectPaymentAuthorizeV4ResponseMessage']['STCPayPmtReference'];
                echo '1';
            }else{
                echo '<div class="alert alert-danger">'.$this->language->get('text_invalid_phone_number').'</div>';
            }
        }else{
            echo '<div class="alert alert-danger">'.$this->language->get('text_invalid_phone_number').'</div>';
        }
    }

    public function confitmStcpay(){
        $this->load->language('common/global');
        if(isset($this->request->post['code']) && isset($this->session->data['OtpReference']) && isset($this->session->data['STCPayPmtReference'])){
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://b2b.stcpay.com.sa/B2B.DirectPayment.WebApi/DirectPayment/V4/DirectPaymentConfirm",
                CURLOPT_SSLCERT => "/home/lanaapps/public_html/admin/ca.pem",
                CURLOPT_SSLKEY => "/home/lanaapps/public_html/admin/key.pem",
                CURLOPT_SSLCERTTYPE => "PEM",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS =>'{
                   "DirectPaymentConfirmV4RequestMessage": {
                     "OtpReference": "'.$this->session->data['OtpReference'].'",
                     "OtpValue": "'.(int)$this->request->post['code'].'",
                     "STCPayPmtReference": "'.$this->session->data['STCPayPmtReference'].'",
                     "TokenReference": "string",
                     "TokenizeYn": true
                     }
                    }',
                CURLOPT_HTTPHEADER => array(
                    "X-ClientCode: 74482575955",
                    "Content-Type: application/json"
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_errno($curl);
            $info = curl_getinfo($curl);
            curl_close($curl);
            $json = json_decode($response,true);
            
            if(isset($json['DirectPaymentConfirmV4ResponseMessage'])){
                if($json['DirectPaymentConfirmV4ResponseMessage']['PaymentStatus'] == '2' && $json['DirectPaymentConfirmV4ResponseMessage']['PaymentStatusDesc'] == 'Paid') {

                        if($this->config->get('config_store_package_id') == '1'){
                            if($json['DirectPaymentConfirmV4ResponseMessage']['Amount'] == 2999){
                                $new_date = date('Y-m-d',strtotime("+365 days"));
                            }else{
                                $new_date = date('Y-m-d',strtotime("+30 days"));
                            }
                        }else{
                            $have = max(0,ceil((strtotime($this->config->get('config_store_expire'))-time())/86400));
                            if($json['DirectPaymentConfirmV4ResponseMessage']['Amount'] == 2999){
                                $new_date = date('Y-m-d',strtotime("+".($have+365)." days"));
                            }else{
                                $new_date = date('Y-m-d',strtotime("+".($have+30)." days"));
                            }
                        }
                        $this->load->model('setting/setting');
                        $this->load->model('common/lanaapp');
                        $this->model_common_lanaapp->action("UPDATE `stores` SET `expire_date`='$new_date' WHERE `id` = '".$this->config->get('config_lana_store_id')."'");
                        $this->model_setting_setting->editSettingValue('lana_setting','config_store_expire',$new_date);
                        $this->model_setting_setting->editSettingValue('lana_setting','config_store_package_id','2');
                        $this->model_setting_setting->editSettingValue('lana_setting','config_store_package_name','باقة مدفوعة');

                        $this->session->data['subscription_msg'] = '<div class="alert alert-seccuss">'.$this->language->get('text_done_paid').'</div>';

                        $this->response->redirect($this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true));

                }elseif($json['DirectPaymentConfirmV4ResponseMessage']['PaymentStatus'] == '1' && $json['DirectPaymentConfirmV4ResponseMessage']['PaymentStatusDesc'] == 'Pending'){

                    $this->session->data['subscription_msg'] = 'Pending';
                    echo '<script> location.href = "'.$this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true).'"; </script>';

                }elseif($json['DirectPaymentConfirmV4ResponseMessage']['PaymentStatus'] == '4' && $json['DirectPaymentConfirmV4ResponseMessage']['PaymentStatusDesc'] == 'Cancelled'){

                    $this->session->data['subscription_msg'] = 'Cancelled';
                    echo '<script> location.href = "'.$this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true).'"; </script>';

                }else{

                    $this->session->data['subscription_msg'] = 'Expired';
                    echo '<script> location.href = "'.$this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true).'"; </script>';

                }
            }elseif(isset($json['Code'])){
                echo '<div class="alert alert-danger">'.$json['Text'].'</div>';
            }else{
                echo '<div class="alert alert-danger">'.$this->language->get('text_invalid_code_activation').'</div>';
            }
        }else{
            echo '#000';
        }
    }

}