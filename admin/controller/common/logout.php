<?php
class ControllerCommonLogout extends Controller {
	public function index() {
		$this->user->logout();

		unset($this->session->data['user_token']);
		unset($_SESSION['_DBNAME_']);
		unset($_SESSION['_PATH_']);
		unset($_SESSION['_DOMAIN_']);

		$this->response->redirect($this->url->link('common/login', '', true));
	}
}