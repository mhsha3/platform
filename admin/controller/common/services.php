<?php
class ControllerCommonServices extends Controller {
	public function index() {
		$this->load->language('common/services');

		$this->document->setTitle($this->language->get('services'));

		$data['user_token'] = $this->session->data['user_token'];

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('services'),
			'href' => $this->url->link('common/services', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['services_ajax_url'] = 'index.php?route=common/services/addservice&user_token='.$this->session->data['user_token'];
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('common/services', $data));
	}
	public function payment() {
		$this->load->language('common/services');

		$this->document->setTitle($this->language->get('payment_methods'));

		$data['user_token'] = $this->session->data['user_token'];

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('payment_methods'),
			'href' => $this->url->link('common/services', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['services_ajax_url'] = 'index.php?route=common/services/addservice&user_token='.$this->session->data['user_token'];
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->load->model('setting/extension');
		$data['installed_payment_methods'] = $this->model_setting_extension->getInstalled('payment');
		
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            $this->session->data['success'] = '';
        } else {
            $data['success'] = '';
        }
        
        if(isset($_GET['new'])){
            print_r($data['installed_payment_methods']);
        }

		$this->response->setOutput($this->load->view('common/payment', $data));
	}
    public function shipping() {
        $this->load->language('common/services');

        $this->document->setTitle($this->language->get('shipping_methods'));

        $data['user_token'] = $this->session->data['user_token'];

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('shipping_methods'),
            'href' => $this->url->link('common/services', 'user_token=' . $this->session->data['user_token'], true)
        );
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
        $data['success'] = $this->session->data['success'];
        $this->session->data['success'] = '';
        $data['freeshipping'] = (int)$this->config->get('shipping_free_status');
        $data['flatshipping'] = (int)$this->config->get('shipping_flat_status');
        $data['smsashipping'] = (int)$this->config->get('smsa_status');
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('common/shipping', $data));
    }
    public function domain() {
        $this->load->language('common/services');
        $this->document->setTitle($this->language->get('shipping_methods'));
        $data['user_token'] = $this->session->data['user_token'];
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('pay_domain'),
            'href' => $this->url->link('common/services/domain', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('common/domain', $data));
    }

    public function design() {
        $this->load->language('common/services');
        $this->document->setTitle($this->language->get('shipping_methods'));
        $data['user_token'] = $this->session->data['user_token'];
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('pay_design'),
            'href' => $this->url->link('common/services/design', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('common/design_order', $data));
    }

    public function addservice(){
        if (isset($this->request->get['user_token']) && isset($this->session->data['user_token']) && ($this->request->get['user_token'] == $this->session->data['user_token']) && isset($this->request->post['_order'])){
            $this->load->model('common/lanaapp');
            $data = ($this->request->post['data']) ?? [];
            $order = $this->model_common_lanaapp->add_services((string)$this->request->post['_order'],$data);
            echo (int)$order;
        }else{
            $this->response->redirect($this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true));
        }
    }
}
