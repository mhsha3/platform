<?php
class ControllerCommonDashboard extends Controller {
	public function index() {
		$this->load->language('common/dashboard');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['user_token'] = $this->session->data['user_token'];

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		// Dashboard Extensions
		$dashboards = array();

		$this->load->model('setting/extension');

		// Get a list of installed modules
		$extensions = $this->model_setting_extension->getInstalled('dashboard');

		// Add all the modules which have multiple settings for each module
		foreach ($extensions as $code) {
			if ($this->config->get('dashboard_' . $code . '_status') && $this->user->hasPermission('access', 'extension/dashboard/' . $code)) {
				$output = $this->load->controller('extension/dashboard/' . $code . '/dashboard');

				if ($output) {
					$dashboards[] = array(
						'code'       => $code,
						'width'      => $this->config->get('dashboard_' . $code . '_width'),
						'sort_order' => $this->config->get('dashboard_' . $code . '_sort_order'),
						'output'     => $output
					);
				}
			}
		}

		$sort_order = array();

		foreach ($dashboards as $key => $value) {
			$sort_order[$key] = $value['sort_order'];
		}

		array_multisort($sort_order, SORT_ASC, $dashboards);

		// Split the array so the columns width is not more than 12 on each row.
		$width = 0;
		$column = array();
		$data['rows'] = array();

		foreach ($dashboards as $dashboard) {
			$column[] = $dashboard;

			$width = ($width + $dashboard['width']);

			if ($width >= 12) {
				$data['rows'][] = $column;

				$width = 0;
				$column = array();
			}
		}


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		// Run currency update
		if ($this->config->get('config_currency_auto')) {
			$this->load->model('localisation/currency');
			$this->model_localisation_currency->refresh();
		}
		$data['store_setting'] = $this->url->link('setting/setting', 'user_token=' . $this->session->data['user_token'], true);
        $data['add_product'] = $this->url->link('catalog/product', 'user_token=' . $this->session->data['user_token'], true);
        $data['payment_methods_url'] = $this->url->link('common/services/payment', 'user_token=' . $this->session->data['user_token'], true);
        $data['shipping_methods_url'] = $this->url->link('common/services/shipping', 'user_token=' . $this->session->data['user_token'], true);

		$data['subscription_msg'] = '';
		if(isset($this->session->data['subscription_msg'])){
		    if($this->session->data['subscription_msg'] == 'paid'){
                $data['subscription_msg'] = '<div class="alert alert-success">تهانينا تمت عملية الدفع بنجاح</div>';
            }else{
                $data['subscription_msg'] = '<div class="alert alert-danger">لم تتم عملية الدفع بنحاج</div>';
            }
		    unset($this->session->data['subscription_msg']);
        }
		// renew package -->
        if(max(0,ceil((strtotime($this->config->get('config_store_expire'))-time())/86400)) < 10 ){
            $renew_msg = str_replace('subscription_url',$this->url->link('common/subscription', 'user_token=' . $this->session->data['user_token'], true),$this->language->get('text_renew_package_msg'));
            $data['renew_package'] = '
                <div class="alert alert-custom alert-notice alert-light-danger fade show shadow" role="alert">
                    <div class="alert-icon"><i class="flaticon-warning"></i></div>
                    <div class="alert-text">'.$renew_msg.'</div>
                </div>
            ';
        }
        $data['documents_msg'] = '';
        $this->load->model('common/lanaapp');
        $documents = $this->model_common_lanaapp->checkDocuments();
        if($documents == 2){
            $data['documents_msg'] = '<div class="alert alert-custom alert-notice alert-light-danger">'.str_replace('documents_url',$this->url->link('common/documents', 'user_token=' . $this->session->data['user_token'], true),$this->language->get('text_refuse_documents_msg')).'</div>';
        }
        if($documents == 0){
            $data['documents_msg'] = '<div class="alert alert-custom alert-notice alert-light-primary">'.str_replace('documents_url',$this->url->link('common/documents', 'user_token=' . $this->session->data['user_token'], true),$this->language->get('text_upload_documents_msg')).'</div>';
        }

        $data['tutorial_steps'] = true;
        if($this->config->get('config_tutorial_steps') == 200){
            $data['tutorial_steps'] = false;
        }

        // Eng.Mohab Dashboard Analytics -- >
        $this->load->model('extension/dashboard/sale');
        $total_sales = $this->model_extension_dashboard_sale->getTotalSales();
        if ($total_sales > 1000000000000) {
            $data['total_sales'] = round($total_sales / 1000000000000, 1) . 'T';
        } elseif ($total_sales > 1000000000) {
            $data['total_sales'] = round($total_sales / 1000000000, 1) . 'B';
        } elseif ($total_sales > 1000000) {
            $data['total_sales'] = round($total_sales / 1000000, 1) . 'M';
        } elseif ($total_sales > 1000) {
            $data['total_sales'] = round($total_sales / 1000, 1) . 'K';
        } else {
            $data['total_sales'] = round($total_sales);
        }
        // total orders -->
        $this->load->model('sale/order');
        $order_total = $this->model_sale_order->getTotalOrders();
        if ($order_total > 1000000000000) {
            $data['order_total'] = round($order_total / 1000000000000, 1) . 'T';
        } elseif ($order_total > 1000000000) {
            $data['order_total'] = round($order_total / 1000000000, 1) . 'B';
        } elseif ($order_total > 1000000) {
            $data['order_total'] = round($order_total / 1000000, 1) . 'M';
        } elseif ($order_total > 1000) {
            $data['order_total'] = round($order_total / 1000, 1) . 'K';
        } else {
            $data['order_total'] = $order_total;
        }
        // Total Customer -->
        $this->load->model('customer/customer');
        $customer_total = $this->model_customer_customer->getTotalCustomers();
        if ($customer_total > 1000000000000) {
            $data['customer_total'] = round($customer_total / 1000000000000, 1) . 'T';
        } elseif ($customer_total > 1000000000) {
            $data['customer_total'] = round($customer_total / 1000000000, 1) . 'B';
        } elseif ($customer_total > 1000000) {
            $data['customer_total'] = round($customer_total / 1000000, 1) . 'M';
        } elseif ($customer_total > 1000) {
            $data['customer_total'] = round($customer_total / 1000, 1) . 'K';
        } else {
            $data['customer_total'] = $customer_total;
        }
        // Customers Online
        $this->load->model('extension/dashboard/online');
        $online_total = $this->model_extension_dashboard_online->getTotalOnline();
        if ($online_total > 1000000000000) {
            $data['online_total'] = round($online_total / 1000000000000, 1) . 'T';
        } elseif ($online_total > 1000000000) {
            $data['online_total'] = round($online_total / 1000000000, 1) . 'B';
        } elseif ($online_total > 1000000) {
            $data['online_total'] = round($online_total / 1000000, 1) . 'M';
        } elseif ($online_total > 1000) {
            $data['online_total'] = round($online_total / 1000, 1) . 'K';
        } else {
            $data['online_total'] = $online_total;
        }

        // Show Orders -->
        $this->load->model('sale/order');
        $filter_data = array(
            'sort'  => 'o.date_added',
            'order' => 'DESC',
            'start' => 0,
            'limit' => 4
        );
        $results = $this->model_sale_order->getOrders($filter_data);
        foreach ($results as $result) {
            $data['orders'][] = array(
                'order_id'   => $result['order_id'],
                'customer'   => $result['customer'],
                'location'   => $result['shipping_zone'],
                'status'     => $result['order_status'],
                'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                'total'      => $this->currency->format($result['total'], $result['currency_code'], $result['currency_value']),
                'view'       => $this->url->link('sale/order/info', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $result['order_id'], true),
            );
        }
        // Total Reviews -->
        $this->load->model('catalog/review');
        $data['rating'][1] = $this->model_catalog_review->getTotalRating(1);
        $data['rating'][2] = $this->model_catalog_review->getTotalRating(2);
        $data['rating'][3] = $this->model_catalog_review->getTotalRating(3);
        $data['rating'][4] = $this->model_catalog_review->getTotalRating(4);
        $data['rating'][5] = $this->model_catalog_review->getTotalRating(5);

        // Activity -->
        $data['activities'] = array();

        $this->load->model('extension/dashboard/activity');

        $results = $this->model_extension_dashboard_activity->getActivities();

        foreach ($results as $result) {
            $comment = vsprintf($this->language->get('text_activity_' . $result['key']), json_decode($result['data'], true));

            $find = array(
                'customer_id=',
                'order_id=',
                'return_id='
            );

            $replace = array(
                $this->url->link('customer/customer/edit', 'user_token=' . $this->session->data['user_token'] . '&customer_id=', true),
                $this->url->link('sale/order/info', 'user_token=' . $this->session->data['user_token'] . '&order_id=', true),
                $this->url->link('sale/return/edit', 'user_token=' . $this->session->data['user_token'] . '&return_id=', true)
            );

            if($result['key'] == 'order_account'){
                $color_key = 'success';
                $icon_key  = '<i class="fa fa-cubes text-success"></i>';
            }elseif ($result['key'] == 'register'){
                $color_key = 'primary';
                $icon_key  = '<i class="fa fa-user text-primary"></i>';
            }elseif ($result['key'] == 'address_add'){
                $color_key = 'danger';
                $icon_key  = '<i class="fa fa-map-marked-alt text-danger"></i>';
            }elseif ($result['key'] == 'register'){
                $color_key = 'danger';
                $icon_key  = '<i class="fa fa-map-marked-alt text-danger"></i>';
            }elseif ($result['key'] == 'login'){
                $color_key = 'success';
                $icon_key  = '<i class="fa fa-sign-in-alt text-success"></i>';
            }elseif ($result['key'] == 'order_guest'){
                $color_key = 'warning';
                $icon_key  = '<i class="fa fa-shopping-cart text-warning"></i>';
            }elseif ($result['key'] == 'forgotten'){
                $color_key = 'info';
                $icon_key  = '<i class="fa fa-key text-info"></i>';
            }else{
                $color_key = 'primary';
                $icon_key  = '<i class="fa fa-more text-primary"></i>';
            }

            $data['activities'][] = array(
                'color_key'       => $color_key,
                'icon_key'        => $icon_key,
                'comment'    => str_replace($find, $replace, $comment),
                'date_added' => date($this->language->get('datetime_format'), strtotime($result['date_added']))
            );
        }

        // Get Product 0 Quantitiy -->
        $this->load->model('catalog/product');
        $this->load->model('tool/image');
        foreach ($this->model_catalog_product->getProducts(['filter_quantity'=>0]) as $key){
            if (is_file(DIR_IMAGE . $key['image'])) {
                $key['image'] = $this->model_tool_image->resize($key['image'], 45, 45);
            } else {
                $key['image'] = $this->model_tool_image->resize('no_image.png', 45, 45);
            }
            $data['loss_products'][] = [
                'product_id' => $this->url->link('catalog/product/edit', 'user_token=' . $this->session->data['user_token'] . '&product_id='.$key['product_id'], true),
                'name' => $key['name'],
                'model' => $key['product_id'],
                'image' => $key['image'],
            ];
        }

        $data['welcome_store_massage'] = true;
        if($this->config->get('config_end_welcome_massage') == 200){
            $data['welcome_store_massage'] = false;
        }

        $this->response->setOutput($this->load->view('common/dashboard', $data));
	}

	public function req(){
	    if($this->user->isLogged() && isset($this->session->data['user_token']) && isset($this->request->get['tutorial_steps']) ){
            $this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE store_id = '0' AND `key` = 'config_tutorial_steps'");
            $this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '0', `code` = 'lanaapp_setting', `key` = 'config_tutorial_steps', `value` = '200'");
            echo '1';
        }
        if($this->user->isLogged() && isset($this->session->data['user_token']) && isset($this->request->get['end_welcome']) ){
            $this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE store_id = '0' AND `key` = 'config_end_welcome_massage'");
            $this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '0', `code` = 'lanaapp_setting', `key` = 'config_end_welcome_massage', `value` = '200'");
            echo '1';
        }
    }

}
