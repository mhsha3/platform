<?php
class ControllerCommonDocuments extends Controller {
    public function index() {
        $this->load->model('common/lanaapp');
        if($this->model_common_lanaapp->checkDocuments() == 1 || $this->model_common_lanaapp->checkDocuments() == 3){
            $this->response->redirect($this->url->link('common/dashboard'));
        }
        $this->load->language('common/global');
        $this->document->setTitle($this->language->get('text_documents'));
        $data['user_token'] = $this->session->data['user_token'];
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_documents'),
            'href' => $this->url->link('common/documents', 'user_token=' . $this->session->data['user_token'], true)
        );
        $this->document->addStyle('view/javascript/dropzone/custom.css');
        $this->document->addScript('view/javascript/jquery/jquery-ui/jquery-ui-sortable.min.js');

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('common/documents', $data));
    }
    public function _u(){
        if ($this->user->isLogged() && isset($this->request->get['user_token']) && ($this->request->get['user_token'] == $this->session->data['user_token'])) {
            $ext  = ['png','jpg','jpeg','pdf','doc'];
            if(!empty($_FILES)){
                $temp = explode(".", $_FILES["file"]["name"]);
                if(in_array(end($temp), $ext)){
                    $temp_file = $_FILES['file']['tmp_name'];
                    $secure_name = md5(round(microtime(true))). time() . '.' . end($temp);
                    $location = '/home/lanaapps/public_html/staff/application/assets/documents/' . $secure_name;
                    if(move_uploaded_file($temp_file, $location)){
                        echo $secure_name;
                    }
                }
            }
        }
    }
    public function send(){
        if ($this->user->isLogged() && isset($this->request->get['user_token']) && ($this->request->get['user_token'] == $this->session->data['user_token'])) {
            $this->load->language('common/global');
            $this->load->model('common/lanaapp');
            $date = date("Y-m-d H:i:s");
            $this->model_common_lanaapp->action("INSERT INTO `documents` (`id`, `store_id`, `type`,`documents`, `status`, `created_at`) VALUES (NULL, '{$this->config->get('config_lana_store_id')}','{$this->request->post['type']}','{$this->request->post['docs']}', '1', '{$date}')");
            echo "<script>Swal.fire('".$this->language->get('text_done_upload_doc')."','','success');setTimeout(()=>{location.href='index.php?route=common/dashboard'},'1500');</script>";
        }
    }
}
