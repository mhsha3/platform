<?php
class ControllerExtensionModuleSMSA extends Controller {
	private $error = array();

	public function install() {
		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."smsa` (
		`consignment_id` int(11) NOT NULL AUTO_INCREMENT,
		`order_id` int(11) NOT NULL,
		`awb_number` varchar(32) NOT NULL,
		`reference_number` varchar(32) NOT NULL,
		`pickup_date` datetime NOT NULL,
		`shipment_label` text,
		`status` varchar(32) NOT NULL,
		`date_added` datetime NOT NULL,
		 PRIMARY KEY (`consignment_id`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8");

		if (!is_dir(str_replace('image', 'awb', DIR_IMAGE))) {
			mkdir(str_replace('image', 'awb', DIR_IMAGE), 0777);
			chmod(str_replace('image', 'awb', DIR_IMAGE), 0777);

			@touch(str_replace('image', 'awb', DIR_IMAGE) . 'index.html');
		}

	}

	public function index() {
		$this->load->language('extension/module/smsa');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('module_smsa', array('module_smsa_status' => $this->request->post['smsa_status']));
			$this->model_setting_setting->editSetting('smsa', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('common/services/shipping', 'user_token=' . $this->session->data['user_token'] , true));
		}


		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['passkey'])) {
			$data['error_passkey'] = $this->error['passkey'];
		} else {
			$data['error_passkey'] = '';
		}

		// if (isset($this->error['username'])) {
		// 	$data['error_username'] = $this->error['username'];
		// } else {
		// 	$data['error_username'] = '';
		// }

		// if (isset($this->error['password'])) {
		// 	$data['error_password'] = $this->error['password'];
		// } else {
		// 	$data['error_password'] = '';
		// }

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = '';
		}

		if (isset($this->error['contact'])) {
			$data['error_contact'] = $this->error['contact'];
		} else {
			$data['error_contact'] = '';
		}

		if (isset($this->error['telephone'])) {
			$data['error_telephone'] = $this->error['telephone'];
		} else {
			$data['error_telephone'] = '';
		}

		if (isset($this->error['postcode'])) {
			$data['error_postcode'] = $this->error['postcode'];
		} else {
			$data['error_postcode'] = '';
		}

		if (isset($this->error['address_1'])) {
			$data['error_address_1'] = $this->error['address_1'];
		} else {
			$data['error_address_1'] = '';
		}

		if (isset($this->error['address_2'])) {
			$data['error_address_2'] = $this->error['address_2'];
		} else {
			$data['error_address_2'] = '';
		}

		if (isset($this->error['city'])) {
			$data['error_city'] = $this->error['city'];
		} else {
			$data['error_city'] = '';
		}

		if (isset($this->error['country'])) {
			$data['error_country'] = $this->error['country'];
		} else {
			$data['error_country'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('common/services/shipping', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/smsa', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/module/smsa', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('common/services/shipping', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

		$data['user_token'] = $this->session->data['user_token'];

		if (isset($this->request->post['smsa_passkey'])) {
			$data['smsa_passkey'] = $this->request->post['smsa_passkey'];
		} elseif($this->config->get('smsa_passkey')) {
			$data['smsa_passkey'] = $this->config->get('smsa_passkey');
		} else {
			$data['smsa_passkey'] = 'Testing0';
		}

		if (isset($this->request->post['smsa_mode'])) {
			$data['smsa_mode'] = $this->request->post['smsa_mode'];
		} else {
			$data['smsa_mode'] = $this->config->get('smsa_mode');
		}

		/*if (isset($this->request->post['smsa_username'])) {
			$data['smsa_username'] = $this->request->post['smsa_username'];
		} else {
			$data['smsa_username'] = $this->config->get('smsa_username');
		}

		if (isset($this->request->post['smsa_password'])) {
			$data['smsa_password'] = $this->request->post['smsa_password'];
		} else {
			$data['smsa_password'] = $this->config->get('smsa_password');
		}*/

		if (isset($this->request->post['smsa_name'])) {
			$data['smsa_name'] = $this->request->post['smsa_name'];
		} elseif($this->config->get('smsa_name')) {
			$data['smsa_name'] = $this->config->get('smsa_name');
		} else {
			$data['smsa_name'] = 'John Doe';
		}

		if (isset($this->request->post['smsa_contact'])) {
			$data['smsa_contact'] = $this->request->post['smsa_contact'];
		} elseif($this->config->get('smsa_contact')) {
			$data['smsa_contact'] = $this->config->get('smsa_contact');
		} else {
			$data['smsa_contact'] = 'John Doe';
		}

		if (isset($this->request->post['smsa_telephone'])) {
			$data['smsa_telephone'] = $this->request->post['smsa_telephone'];
		} elseif($this->config->get('smsa_telephone')) {
			$data['smsa_telephone'] = $this->config->get('smsa_telephone');
		} else {
			$data['smsa_telephone'] = '86745364786';
		}

		if (isset($this->request->post['smsa_postcode'])) {
			$data['smsa_postcode'] = $this->request->post['smsa_postcode'];
		} elseif($this->config->get('smsa_postcode')) {
			$data['smsa_postcode'] = $this->config->get('smsa_postcode');
		} else {
			$data['smsa_postcode'] = '11009';
		}

		if (isset($this->request->post['smsa_address_1'])) {
			$data['smsa_address_1'] = $this->request->post['smsa_address_1'];
		} elseif($this->config->get('smsa_address_1')) {
			$data['smsa_address_1'] = $this->config->get('smsa_address_1');
		} else {
			$data['smsa_address_1'] = 'R-176 Al Arab';
		}

		if (isset($this->request->post['smsa_address_2'])) {
			$data['smsa_address_2'] = $this->request->post['smsa_address_2'];
		} elseif($this->config->get('smsa_address_2')) {
			$data['smsa_address_2'] = $this->config->get('smsa_address_2');
		} else {
			$data['smsa_address_2'] = 'South West';
		}

		if (isset($this->request->post['smsa_city'])) {
			$data['smsa_city'] = $this->request->post['smsa_city'];
		} elseif($this->config->get('smsa_city')) {
			$data['smsa_city'] = $this->config->get('smsa_city');
		} else {
			$data['smsa_city'] = 'Riyad';
		}

		if (isset($this->request->post['smsa_country'])) {
			$data['smsa_country'] = $this->request->post['smsa_country'];
		} elseif($this->config->get('smsa_country')) {
			$data['smsa_country'] = $this->config->get('smsa_country');
		} else {
			$data['smsa_country'] = 'KSA';
		}

		if (isset($this->request->post['smsa_status'])) {
			$data['smsa_status'] = $this->request->post['smsa_status'];
		} else {
			$data['smsa_status'] = $this->config->get('smsa_status');
		}

// 		$data['shipping_methods'] = array();

// 		$this->load->model('setting/extension');

// 		$results = $this->model_setting_extension->getInstalled('payment');

// 		foreach ($results as $code) {
// 			if ($this->config->get('payment_'.$code. '_status')) {
// 				$this->load->language('extension/payment/'.$code);
// 				$data['payment_methods'][] = array(
// 					'code'      => $code,
// 					'title'     => $this->language->get('heading_title'),
// 				);
// 			}
// 		}

// 		if (isset($this->request->post['smsa_cod_method'])) {
// 			$data['smsa_cod_method'] = $this->request->post['smsa_cod_method'];
// 		} elseif ($this->config->get('smsa_cod_method')) {
// 			$data['smsa_cod_method'] = $this->config->get('smsa_cod_method');
// 		} else {
// 			$data['smsa_cod_method'] = array();
// 		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/smsa', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/smsa')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (empty($this->request->post['smsa_passkey'])) {
			$this->error['passkey'] = $this->language->get('error_passkey');
		}

		// if (empty($this->request->post['smsa_password'])) {
		// 	$this->error['password'] = $this->language->get('error_password');
		// }

		// if (empty($this->request->post['smsa_username'])) {
		// 	$this->error['username'] = $this->language->get('error_username');
		// }

		if (empty($this->request->post['smsa_name'])) {
			$this->error['name'] = $this->language->get('error_name');
		}

		if (empty($this->request->post['smsa_contact'])) {
			$this->error['contact'] = $this->language->get('error_contact');
		}

		if ((utf8_strlen($this->request->post['smsa_telephone']) < 3) || (utf8_strlen($this->request->post['smsa_telephone']) > 32)) {
			$this->error['telephone'] = $this->language->get('error_telephone');
		}

		if (empty($this->request->post['smsa_address_1'])) {
			$this->error['address_1'] = $this->language->get('error_address_1');
		}

		if (empty($this->request->post['smsa_address_2'])) {
			$this->error['address_2'] = $this->language->get('error_address_2');
		}

		if (empty($this->request->post['smsa_city'])) {
			$this->error['city'] = $this->language->get('error_city');
		}

		if (empty($this->request->post['smsa_country'])) {
			$this->error['country'] = $this->language->get('error_country');
		}

		if (empty($this->request->post['smsa_postcode'])) {
			$this->error['postcode'] = $this->language->get('error_postcode');
		}

		return !$this->error;
	}

}
