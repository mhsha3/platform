<?php

class ControllerExtensionModulemsegatSMS extends Controller {

    private $error = array();

    public function install() {
        $this->load->model('extension/module/msegatsms');
        $this->model_extension_module_msegatsms->createMseTables();
    }

    public function uninstall() {
        $this->load->model('extension/module/msegatsms');
        $this->model_extension_module_msegatsms->deleteMseTables();
    }

    public function index() {
        $this->load->language('extension/module/msegatsms');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('setting/setting');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $prefix = array(
                'AD' => 376,
                'AE' => 971,
                'AF' => 93,
                'AG' => 1268,
                'AI' => 1264,
                'AL' => 355,
                'AM' => 374,
                'AN' => 599,
                'AO' => 244,
                'AQ' => 672,
                'AR' => 54,
                'AS' => 1684,
                'AT' => 43,
                'AU' => 61,
                'AW' => 297,
                'AX' => '',
                'AZ' => 994,
                'BA' => 387,
                'BB' => 1246,
                'BD' => 880,
                'BE' => 32,
                'BF' => 226,
                'BG' => 359,
                'BH' => 973,
                'BI' => 257,
                'BJ' => 229,
                'BL' => 590,
                'BM' => 1441,
                'BN' => 673,
                'BO' => 591,
                'BR' => 55,
                'BS' => 1242,
                'BT' => 975,
                'BV' => '',
                'BW' => 267,
                'BY' => 375,
                'BZ' => 501,
                'CA' => 1,
                'CC' => 61,
                'CD' => 242,
                'CF' => 236,
                'CG' => 243,
                'CH' => 41,
                'CI' => 225,
                'CK' => 682,
                'CL' => 56,
                'CM' => 237,
                'CN' => 86,
                'CO' => 57,
                'CR' => 506,
                'CU' => 53,
                'CV' => 238,
                'CX' => 61,
                'CY' => 357,
                'CZ' => 420,
                'DE' => 49,
                'DJ' => 253,
                'DK' => 45,
                'DM' => 1767,
                'DO' => 1809,
                'DZ' => 213,
                'EC' => 593,
                'EE' => 372,
                'EG' => 20,
                'EH' => '',
                'ER' => 291,
                'ES' => 34,
                'ET' => 251,
                'FI' => 358,
                'FJ' => 679,
                'FK' => 500,
                'FM' => 691,
                'FO' => 298,
                'FR' => 33,
                'GA' => 241,
                'GB' => 44,
                'GD' => 1473,
                'GE' => 995,
                'GF' => 594,
                'GG' => 44,
                'GH' => 233,
                'GI' => 350,
                'GL' => 299,
                'GM' => 220,
                'GN' => 224,
                'GP' => 590,
                'GQ' => 240,
                'GR' => 30,
                'GS' => '',
                'GT' => 502,
                'GU' => 1671,
                'GW' => 245,
                'GY' => 592,
                'HK' => 852,
                'HM' => '',
                'HN' => 504,
                'HR' => 385,
                'HT' => 509,
                'HU' => 36,
                'ID' => 62,
                'IE' => 353,
                'IL' => 972,
                'IM' => 44,
                'IN' => 91,
                'IO' => 1284,
                'IQ' => 964,
                'IR' => 98,
                'IS' => 354,
                'IT' => 39,
                'JE' => 44,
                'JM' => 1876,
                'JO' => 962,
                'JP' => 81,
                'KE' => 254,
                'KG' => 996,
                'KH' => 855,
                'KI' => 686,
                'KM' => 269,
                'KN' => 1869,
                'KP' => 850,
                'KR' => 82,
                'KW' => 965,
                'KY' => 1345,
                'KZ' => 7,
                'LA' => 856,
                'LB' => 961,
                'LC' => 1758,
                'LI' => 423,
                'LK' => 94,
                'LR' => 231,
                'LS' => 266,
                'LT' => 370,
                'LU' => 352,
                'LV' => 371,
                'LY' => 218,
                'MA' => 212,
                'MC' => 377,
                'MD' => 373,
                'ME' => 382,
                'MF' => 1599,
                'MG' => 261,
                'MH' => 692,
                'MK' => 389,
                'ML' => 223,
                'MM' => 95,
                'MN' => 976,
                'MO' => 853,
                'MP' => 1670,
                'MQ' => 596,
                'MR' => 222,
                'MS' => 1664,
                'MT' => 356,
                'MU' => 230,
                'MV' => 960,
                'MW' => 265,
                'MX' => 52,
                'MY' => 60,
                'MZ' => 258,
                'NA' => 264,
                'NC' => 687,
                'NE' => 227,
                'NF' => 672,
                'NG' => 234,
                'NI' => 505,
                'NL' => 31,
                'NO' => 47,
                'NP' => 977,
                'NR' => 674,
                'NU' => 683,
                'NZ' => 64,
                'OM' => 968,
                'PA' => 507,
                'PE' => 51,
                'PF' => 689,
                'PG' => 675,
                'PH' => 63,
                'PK' => 92,
                'PL' => 48,
                'PM' => 508,
                'PN' => 870,
                'PR' => 1,
                'PS' => '',
                'PT' => 351,
                'PW' => 680,
                'PY' => 595,
                'QA' => 974,
                'RE' => 262,
                'RO' => 40,
                'RS' => 381,
                'RU' => 7,
                'RW' => 250,
                'SA' => 966,
                'SB' => 677,
                'SC' => 248,
                'SD' => 249,
                'SE' => 46,
                'SG' => 65,
                'SI' => 386,
                'SJ' => '',
                'SK' => 421,
                'SL' => 232,
                'SM' => 378,
                'SN' => 221,
                'SO' => 252,
                'SR' => 597,
                'ST' => 239,
                'SV' => 503,
                'SY' => 963,
                'SZ' => 268,
                'TC' => 1649,
                'TD' => 235,
                'TF' => '',
                'TG' => 228,
                'TH' => 66,
                'TJ' => 992,
                'TK' => 690,
                'TL' => 670,
                'TM' => 993,
                'TN' => 216,
                'TO' => 676,
                'TR' => 90,
                'TT' => 1868,
                'TV' => 688,
                'TW' => 886,
                'TZ' => 255,
                'UA' => 380,
                'UG' => 256,
                'US' => 1,
                'UY' => 598,
                'UZ' => 998,
                'VA' => 379,
                'VC' => 1784,
                'VE' => 58,
                'VG' => 1284,
                'VI' => 1340,
                'VN' => 84,
                'VU' => 678,
                'WF' => 681,
                'WS' => 685,
                'YE' => 967,
                'YT' => 262,
                'ZA' => 27,
                'ZM' => 260,
                'ZW' => 263
            );
            $array = array();
            
            $array['msegatsms'] = array_merge($this->request->post, $prefix);
            $this->model_setting_setting->editSetting('msegatsms', $array);
            $this->session->data['success'] = $this->language->get('text_success');
            $this->response->redirect($this->url->link('extension/module/msegatsms', 'user_token=' . $this->session->data['user_token'], 'SSL'));
        }
        $data['heading_title'] = $this->language->get('heading_title');
        $data['intruction_title'] = $this->language->get('intruction_title');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_content_top'] = $this->language->get('text_content_top');
        $data['text_content_bottom'] = $this->language->get('text_content_bottom');
        $data['text_column_left'] = $this->language->get('text_column_left');
        $data['text_column_right'] = $this->language->get('text_column_right');
        $data['text_instruction'] = $this->language->get('text_instruction');
        $data['text_none'] = $this->language->get('text_none');
        $data['text_limit'] = $this->language->get('text_limit');
        $data['text_yes'] = $this->language->get('text_yes');
        $data['text_no'] = $this->language->get('text_no');
        $data['text_status_none'] = $this->language->get('text_status_none');
        $data['text_enable_verify'] = $this->language->get('text_enable_verify');
        $data['entry_gateway'] = $this->language->get('entry_gateway');
        $data['entry_userkey'] = $this->language->get('entry_userkey');
        $data['entry_passkey'] = $this->language->get('entry_passkey');
        $data['entry_httpapi'] = $this->language->get('entry_httpapi');
        $data['httpapi_example'] = $this->language->get('httpapi_example');
        $data['entry_userkey_msegat'] = $this->language->get('entry_userkey_msegat');
        $data['entry_passkey_msegat'] = $this->language->get('entry_passkey_msegat');
        $data['entry_httpapi_msegat'] = $this->language->get('entry_httpapi_msegat');
        $data['entry_senderid_msegat'] = $this->language->get('entry_senderid_msegat');
        $data['httpapi_example_msegat'] = $this->language->get('httpapi_example_msegat');
        $data['entry_layout'] = $this->language->get('entry_layout');
        $data['entry_position'] = $this->language->get('entry_position');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $data['entry_smslimit'] = $this->language->get('entry_smslimit');
        $data['entry_alert_reg'] = $this->language->get('entry_alert_reg');
        $data['entry_alert_blank'] = $this->language->get('entry_alert_blank');
        $data['entry_alert_order'] = $this->language->get('entry_alert_order');
        $data['entry_alert_changestate'] = $this->language->get('entry_alert_changestate');
        $data['entry_additional_alert'] = $this->language->get('entry_additional_alert');
        $data['entry_alert_sms'] = $this->language->get('entry_alert_sms');
        $data['entry_account_sms'] = $this->language->get('entry_account_sms');
        $data['entry_status_order_alert'] = $this->language->get('entry_status_order_alert');
        $data['entry_verify_code'] = $this->language->get('entry_verify_code');
        $data['entry_skip_group'] = $this->language->get('entry_skip_group');
        $data['entry_skip_group_help'] = $this->language->get('entry_skip_group_help');
        $data['entry_code_digit'] = $this->language->get('entry_code_digit');
        $data['entry_max_retry'] = $this->language->get('entry_max_retry');
        $data['entry_limit_blank'] = $this->language->get('entry_limit_blank');
        $data['entry_parsing'] = $this->language->get('entry_parsing');
        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['button_add_module'] = $this->language->get('button_add_module');
        $data['button_remove'] = $this->language->get('button_remove');
        $data['entry_edit'] = $this->language->get('entry_edit');
        $data['entry_userkey_msegat_span'] = $this->language->get('entry_userkey_msegat_span');
        $data['entry_passkey_msegat_span'] = $this->language->get('entry_passkey_msegat_span');
        $data['entry_senderid_msegat_span'] = $this->language->get('entry_senderid_msegat_span');
        $data['entry_smslimit_span'] = $this->language->get('entry_smslimit_span');
        $data['entry_alert_reg_span'] = $this->language->get('entry_alert_reg_span');
        $data['entry_alert_order_span'] = $this->language->get('entry_alert_order_span');
        $data['entry_status_order_alert_span'] = $this->language->get('entry_status_order_alert_span');
        $data['entry_alert_changestate_span'] = $this->language->get('entry_alert_changestate_span');
        $data['entry_account_sms_span'] = $this->language->get('entry_account_sms_span');
        $data['entry_additional_alert_span'] = $this->language->get('entry_additional_alert_span');
        $data['text_enable_verify_span'] = $this->language->get('text_enable_verify_span');
        $data['entry_code_digit_span'] = $this->language->get('entry_code_digit_span');
        $data['entry_max_retry_span'] = $this->language->get('entry_max_retry_span');
        $data['entry_verify_code_span'] = $this->language->get('entry_verify_code_span');
        $data['entry_skip_group_span'] = $this->language->get('entry_skip_group_span');
        $data['entry_alert_sms_span'] = $this->language->get('entry_alert_sms_span');

        $this->load->model('localisation/language');
        $data['languages'] = $this->model_localisation_language->getLanguages();

        $this->load->model('localisation/order_status');
        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
        if (isset($this->error['code_digit'])) {
            $data['error_code_digit'] = $this->error['code_digit'];
        } else {
            $data['error_code_digit'] = '';
        }
        if (isset($this->error['message_code_verification'])) {
            $data['error_message_code_verification'] = $this->error['message_code_verification'];
        } else {
            $data['error_message_code_verification'] = '';
        }
        if (isset($this->error['gateway'])) {
            $data['error_gateway'] = $this->error['gateway'];
        } else {
            $data['error_gateway'] = '';
        }
        if (isset($this->error['userkey'])) {
            $data['error_userkey'] = $this->error['userkey'];
        } else {
            $data['error_userkey'] = '';
        }
        if (isset($this->error['passkey'])) {
            $data['error_passkey'] = $this->error['passkey'];
        } else {
            $data['error_passkey'] = '';
        }
        if (isset($this->error['httpapi'])) {
            $data['error_httpapi'] = $this->error['httpapi'];
        } else {
            $data['error_httpapi'] = '';
        }
        if (isset($this->error['userkey_msegat'])) {
            $data['error_userkey_msegat'] = $this->error['userkey_msegat'];
        } else {
            $data['error_userkey_msegat'] = '';
        }
        if (isset($this->error['passkey_msegat'])) {
            $data['error_passkey_msegat'] = $this->error['passkey_msegat'];
        } else {
            $data['error_passkey_msegat'] = '';
        }
        if (isset($this->error['httpapi_msegat'])) {
            $data['error_httpapi_msegat'] = $this->error['httpapi_msegat'];
        } else {
            $data['error_httpapi_msegat'] = '';
        }
        if (isset($this->error['senderid_msegat'])) {
            $data['error_senderid_msegat'] = $this->error['senderid_msegat'];
        } else {
            $data['error_senderid_msegat'] = '';
        }
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], 'SSL'),
            'separator' => false
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_module'),
            'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'], 'SSL'),
            'separator' => ' :: '
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/msegatsms', 'user_token=' . $this->session->data['user_token'], 'SSL'),
            'separator' => ' :: '
        );
        $data['action'] = $this->url->link('extension/module/msegatsms', 'user_token=' . $this->session->data['user_token'], 'SSL');
        $data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'], 'SSL');
        $msegatsms = $this->config->get('msegatsms');
        if (isset($this->request->post['gateway'])) {
            $data['gateway'] = $this->request->post['gateway'];
        } else {
            if (!empty($msegatsms['gateway'])) {
                $data['gateway'] = $msegatsms['gateway'];
            } else {
                $data['gateway'] = '';
            }
        }
        if (isset($this->request->post['smslimit'])) {
            $data['smslimit'] = $this->request->post['smslimit'];
        } else {
            if (!empty($msegatsms['smslimit'])) {
                $data['smslimit'] = $msegatsms['smslimit'];
            } else {
                $data['smslimit'] = '';
            }
        }
        if (isset($this->request->post['message_order'])) {
            $data['message_order'] = $this->request->post['message_order'];
        } else {
            if (!empty($msegatsms['message_order'])) {
                $data['message_order'] = $msegatsms['message_order'];
            } else {
                $data['message_order'] = '';
            }
        }
        if (isset($this->request->post['message_reg'])) {
            $data['message_reg'] = $this->request->post['message_reg'];
        } else {
            if (!empty($msegatsms['message_reg'])) {
                $data['message_reg'] = $msegatsms['message_reg'];
            } else {
                $data['message_reg'] = '';
            }
        }

        if (isset($this->request->post['message_alert'])) {
            $data['message_alert'] = $this->request->post['message_alert'];
        } else {
            if (!empty($msegatsms['message_alert'])) {
                $data['message_alert'] = $msegatsms['message_alert'];
            } else {
                $data['message_alert'] = '';
            }
        }

        if (isset($this->request->post['mse_sms_status'])) {
            $data['mse_sms_status'] = $this->request->post['mse_sms_status'];
        } else {
            if (!empty($msegatsms['mse_sms_status'])) {
                $data['mse_sms_status'] = $msegatsms['mse_sms_status'];
            } else {
                $data['mse_sms_status'] = '';
            }
        }

        if (isset($this->request->post['mse_sms_template'])) {
            $data['mse_sms_template'] = $this->request->post['mse_sms_template'];
        } else {
            if (!empty($msegatsms['mse_sms_template'])) {
                $data['mse_sms_template'] = $msegatsms['mse_sms_template'];
            } else {
                $data['mse_sms_template'] = '';
            }
        }

        if (isset($this->request->post['config_alert_sms'])) {
            $data['config_alert_sms'] = $this->request->post['config_alert_sms'];
        } else {
            if (!empty($msegatsms['config_alert_sms'])) {
                $data['config_alert_sms'] = $msegatsms['config_alert_sms'];
            } else {
                $data['config_alert_sms'] = '';
            }
        }


        if (isset($this->request->post['config_account_sms'])) {
            $data['config_account_sms'] = $this->request->post['config_account_sms'];
        } else {
            if (!empty($msegatsms['config_account_sms'])) {
                $data['config_account_sms'] = $msegatsms['config_account_sms'];
            } else {
                $data['config_account_sms'] = '';
            }
        }
        
        if (isset($this->request->post['verify'])) {
            $data['verify'] = $this->request->post['verify'];
        } else {
            if (!empty($msegatsms['verify'])) {
                $data['verify'] = $msegatsms['verify'];
            } else {
                $data['verify'] = '';
            }
        }
        
        if (isset($this->request->post['code_digit'])) {
            $data['code_digit'] = $this->request->post['code_digit'];
        } else {
            if (!empty($msegatsms['code_digit'])) {
                $data['code_digit'] = $msegatsms['code_digit'];
            } else {
                $data['code_digit'] = '';
            }
        }
        
        if (isset($this->request->post['max_retry'])) {
            $data['max_retry'] = $this->request->post['max_retry'];
        } else {
            if (!empty($msegatsms['max_retry'])) {
                $data['max_retry'] = $msegatsms['max_retry'];
            } else {
                $data['max_retry'] = '';
            }
        }
        if (isset($this->request->post['message_code_verification'])) {
            $data['message_code_verification'] = $this->request->post['message_code_verification'];
        } else {
            if (!empty($msegatsms['message_code_verification'])) {
                $data['message_code_verification'] = $msegatsms['message_code_verification'];
            } else {
                $data['message_code_verification'] = '';
            }
        }
        $skip_group_id = $this->fillin('skip_group_id', array(
            0
                ), $msegatsms);
        $data['skip_group_id'] = $skip_group_id;
        $this->load->model('customer/customer_group');
        $data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups(0);
        if (isset($this->request->post['userkey'])) {
            $data['userkey'] = $this->request->post['userkey'];
        } else {
            if (!empty($msegatsms['userkey'])) {
                $data['userkey'] = $msegatsms['userkey'];
            } else {
                $data['userkey'] = '';
            }
        }
        if (isset($this->request->post['passkey'])) {
            $data['passkey'] = $this->request->post['passkey'];
        } else {
            if (!empty($msegatsms['passkey'])) {
                $data['passkey'] = $msegatsms['passkey'];
            } else {
                $data['passkey'] = '';
            }
        }
        if (isset($this->request->post['httpapi'])) {
            $data['httpapi'] = $this->request->post['httpapi'];
        } else {
            if (!empty($msegatsms['httpapi'])) {
                $data['httpapi'] = $msegatsms['httpapi'];
            } else {
                $data['httpapi'] = '';
            }
        }
        if (isset($this->request->post['userkey_msegat'])) {
            $data['userkey_msegat'] = $this->request->post['userkey_msegat'];
        } else {
            if (!empty($msegatsms['userkey_msegat'])) {
                $data['userkey_msegat'] = $msegatsms['userkey_msegat'];
            } else {
                $data['userkey_msegat'] = '';
            }
        }
        if (isset($this->request->post['passkey_msegat'])) {
            $data['passkey_msegat'] = $this->request->post['passkey_msegat'];
        } else {
            if (!empty($msegatsms['passkey_msegat'])) {
                $data['passkey_msegat'] = $msegatsms['passkey_msegat'];
            } else {
                $data['passkey_msegat'] = '';
            }
        }
        if (isset($this->request->post['httpapi_msegat'])) {
            $data['httpapi_msegat'] = $this->request->post['httpapi_msegat'];
        } else {
            if (!empty($msegatsms['httpapi_msegat'])) {
                $data['httpapi_msegat'] = $msegatsms['httpapi_msegat'];
            } else {
                $data['httpapi_msegat'] = '';
            }
        }
        if (isset($this->request->post['senderid_msegat'])) {
            $data['senderid_msegat'] = $this->request->post['senderid_msegat'];
        } else {
            if (!empty($msegatsms['senderid_msegat'])) {
                $data['senderid_msegat'] = $msegatsms['senderid_msegat'];
            } else {
                $data['senderid_msegat'] = '';
            }
        }
        $this->load->model('design/layout');
        $data['layouts'] = $this->model_design_layout->getLayouts();
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('extension/module/msegatsms', $data));
    }

    private function fillin($var, $default = 0, $msegatsms) {
        if (isset($this->request->post[$var])) {
            $var = $this->request->post[$var];
        } else {
            if (!empty($msegatsms[$var])) {
                $var = $msegatsms[$var];
            } else {
                $var = '';
            }
            if ($default)
                if (!$var)
                    $var = $default;
        }
        return $var;
    }

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'extension/module/msegatsms')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        if ($this->request->post['verify'] == 1) {
            if (!$this->request->post['code_digit']) {
                $this->error['code_digit'] = $this->language->get('error_userkey');
            }
            if (!$this->request->post['message_code_verification']) {
                $this->error['message_code_verification'] = $this->language->get('error_userkey');
            }
        }
        if (!$this->request->post['userkey_msegat']) {
            $this->error['userkey_msegat'] = $this->language->get('error_userkey');
        }
        if (!$this->request->post['passkey_msegat']) {
            $this->error['passkey_msegat'] = $this->language->get('error_passkey');
        }
        if (!$this->request->post['httpapi_msegat']) {
            $this->error['httpapi_msegat'] = $this->language->get('error_httpapi');
        }
        if (!$this->request->post['senderid_msegat']) {
            $this->error['senderid_msegat'] = $this->language->get('error_senderid');
        }
        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

}

?>