<?php class ControllerExtensionModulemsegatSendSMS extends Controller
{
    private $error = array();


    public function index()
    {
        $this->load->language('extension/module/msegatsendsms');

        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->load->model('customer/customer');
            $this->load->model('customer/customer_group');
            $this->load->model('sale/order');

            $numbers = array();                  
            switch ($this->request->post['to']) {
                case 'nohp':
                    $numbers[] = $this->request->post['nohp'] . ',' . 'null';
                    break;
                case 'newsletter':
                    $customer_data = array('filter_newsletter' => 1);
                    $email_total = $this->model_sale_customer->getTotalCustomers($customer_data);
                    $results = $this->model_sale_customer->getCustomers($customer_data);
                    foreach ($results as $result) {
                        $query = $this->db->query("SELECT `country_id` FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$result['customer_id'] . "'");
                        $cid = $query->row['country_id'];
                        $query2 = $this->db->query("SELECT `iso_code_2` FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$cid . "'");
                        $isoc = $query2->row['iso_code_2'];
                        $numbers[] = $result['telephone'] . ',' . $isoc;
                    }
                    break;
                case 'customer_all':
                    $customer_data = array();
                    $email_total = $this->model_sale_customer->getTotalCustomers($customer_data);
                    $results = $this->model_sale_customer->getCustomers($customer_data);
                    foreach ($results as $result) {
                        $query = $this->db->query("SELECT `country_id` FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$result['customer_id'] . "'");
                        $cid = $query->row['country_id'];
                        $query2 = $this->db->query("SELECT `iso_code_2` FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$cid . "'");
                        $isoc = $query2->row['iso_code_2'];
                        $numbers[] = $result['telephone'] . ',' . $isoc;
                    }
                    break;
                case 'customer_group':
                    $customer_data = array('filter_customer_group_id' => $this->request->post['customer_group_id']);
                    $email_total = $this->model_sale_customer->getTotalCustomers($customer_data);
                    $results = $this->model_sale_customer->getCustomers($customer_data);
                    foreach ($results as $result) {
                        $query = $this->db->query("SELECT `country_id` FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$result['customer_id'] . "'");
                        $cid = $query->row['country_id'];
                        $query2 = $this->db->query("SELECT `iso_code_2` FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$cid . "'");
                        $isoc = $query2->row['iso_code_2'];
                        $numbers[] = $result['telephone'] . ',' . $isoc;
                    }
                    break;
                case 'customer':
                    if (!empty($this->request->post['customers'])) {
                        foreach ($this->request->post['customers'] as $customer_id) {
                            $customer_info = $this->model_sale_customer->getCustomer($customer_id);
                            if ($customer_info) {
                                $query = $this->db->query("SELECT `country_id` FROM " . DB_PREFIX . "address WHERE customer_id = '" . (int)$customer_id . "'");
                                $cid = $query->row['country_id'];
                                $query2 = $this->db->query("SELECT `iso_code_2` FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$cid . "'");
                                $isoc = $query2->row['iso_code_2'];
                                $numbers[] = $customer_info['telephone'] . ',' . $isoc;
                            }
                        }
                    }
                    break;
                case 'affiliate_all':
                    $affiliate_data = array();
                    $email_total = $this->model_customer_customer->getTotalAffiliates($affiliate_data);
                    $results = $this->model_customer_customer->getAffiliates($affiliate_data);
                    foreach ($results as $result) {
                        $query = $this->db->query("SELECT `iso_code_2` FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$result['country_id'] . "'");
                        $isoc = $query->row['iso_code_2'];
                        $numbers[] = $result['telephone'] . ',' . $isoc;
                    }
                    break;
                case 'affiliate':
                    if (!empty($this->request->post['affiliates'])) {
                        foreach ($this->request->post['affiliates'] as $affiliate_id) {
                            $affiliate_info = $this->model_customer_customer->getAffiliate($affiliate_id);
                            if ($affiliate_info) {
                                $query = $this->db->query("SELECT `iso_code_2` FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$affiliate_info['country_id'] . "'");
                                $isoc = $query->row['iso_code_2'];
                                $numbers[] = $affiliate_info['telephone'] . ',' . $isoc;
                            }
                        }
                    }
                    break;
                case 'product':                
                    if (isset($this->request->post['products'])) {                        
                        $results = $this->model_sale_order->getNohpByProductsOrdered($this->request->post['products']);
                        foreach ($results as $result) {
                            $query2 = $this->db->query("SELECT `iso_code_2` FROM " . DB_PREFIX . "country WHERE country_id = '" . (int)$result['payment_country_id'] . "'");
                            $isoc = $query2->row['iso_code_2'];
                            $numbers[] = $result['telephone'] . ',' . $isoc;
                        }
                    }
                    break;
            }

            $status = '';

          

            if ($numbers) {
               
                $this->load->model('extension/module/msegatsms');
                $gateway = '';
                foreach ($numbers as $destination) {
                    $break = explode(',', $destination);
                    $destination = $this->model_extension_module_msegatsms->getConvertPhonePrefix($break[0], $break[1]);
                    $getresponse = $this->model_extension_module_msegatsms->send_message($destination, $this->request->post['message'], $gateway);
                }
                $status = $getresponse;
            }

            if ($status == "Success") {
                $this->session->data['success'] = $this->language->get('text_success_sent');
            } else {
                $this->session->data['error'] = $this->language->get('text_error') . '<b><i>' . $status . '</i></b>)';
            }
            $this->response->redirect($this->url->link('extension/module/msegatsendsms', 'user_token=' . $this->session->data['user_token'], 'SSL'));
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];
            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }
        if (isset($this->session->data['error'])) {
            $data['error'] = $this->session->data['error'];
            unset($this->session->data['error']);
        } else {
            $data['error'] = '';
        }
        $data['heading_title'] = $this->language->get('heading_title');
    //    $defaultgateway = $this->config->get('gateway');
        $data['gateway'] = "Msegat.com";

        $data['text_none'] = $this->language->get('text_none');
        $data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
        $data['cancel'] = $this->language->get('cancel');
        $data['text_nohp'] = $this->language->get('text_nohp');
        $data['text_newsletter'] = $this->language->get('text_newsletter');
        $data['text_customer_all'] = $this->language->get('text_customer_all');
        $data['text_customer'] = $this->language->get('text_customer');
        $data['text_customer_group'] = $this->language->get('text_customer_group');
        $data['text_affiliate_all'] = $this->language->get('text_affiliate_all');
        $data['text_affiliate'] = $this->language->get('text_affiliate');
        $data['text_product'] = $this->language->get('text_product');
        $data['entry_nohp'] = $this->language->get('entry_nohp');
		$data['entry_nohp_span'] = $this->language->get('entry_nohp_span');
        $data['entry_message'] = $this->language->get('entry_message');
        $data['entry_or'] = $this->language->get('entry_or');
		$data['entry_or_span'] = $this->language->get('entry_or_span');
        $data['entry_customer_group'] = $this->language->get('entry_customer_group');
        $data['entry_customer'] = $this->language->get('entry_customer');
        $data['entry_affiliate'] = $this->language->get('entry_affiliate');
        $data['entry_product'] = $this->language->get('entry_product');
		$data['entry_product_span'] = $this->language->get('entry_product_span');
        $data['button_send'] = $this->language->get('button_send');
        $data['button_setting'] = $this->language->get('button_setting');
        $data['user_token'] = $this->session->data['user_token'];
        

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }
        if (isset($this->error['nohp'])) {
            $data['error_nohp'] = $this->error['nohp'];
        } else {
            $data['error_nohp'] = '';
        }
        if (isset($this->error['message'])) {
            $data['error_message'] = $this->error['message'];
        } else {
            $data['error_message'] = '';
        }
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array('text' => $this->language->get('text_home'), 'href' => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], 'SSL'), 'separator' => false);
        $data['breadcrumbs'][] = array('text' => $this->language->get('text_module'), 'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'], 'SSL'), 'separator' => ' :: ');
        $data['breadcrumbs'][] = array('text' => $this->language->get('heading_title'), 'href' => $this->url->link('extension/module/msegatsendsms', 'user_token=' . $this->session->data['user_token'], 'SSL'), 'separator' => ' :: ');
		
		$data['cancel']        = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'], 'SSL');
        $data['action'] = $this->url->link('extension/module/msegatsendsms', 'user_token=' . $this->session->data['user_token'], 'SSL');
        $data['setting'] = $this->url->link('extension/module/msegatsms', 'user_token=' . $this->session->data['user_token'], 'SSL');
        
        $this->load->model('design/layout');

        $data['layouts'] = $this->model_design_layout->getLayouts();
        
        $this->load->model('customer/customer_group');
        
        $data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups(0);

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        //$this->response->setOutput($this->render());
        
        $this->response->setOutput($this->load->view('extension/module/msegatsendsms', $data));
    }

    protected function validate()
    {
        if (!$this->user->hasPermission('modify', 'extension/module/msegatsendsms')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        if (!$this->request->post['to']) {
            $this->error['nohp'] = $this->language->get('error_nohp');
        }
        switch ($this->request->post['to']) {
            case 'nohp':
                if (!$this->request->post['nohp']) {
                    $this->error['nohp'] = $this->language->get('error_nohp');
                }
                break;
            case 'customer':
                if (!$this->request->post['customers']) {
                    $this->error['nohp'] = $this->language->get('error_nohp');
                }
                break;
            case 'affiliate':
                if (!$this->request->post['affiliates']) {
                    $this->error['nohp'] = $this->language->get('error_nohp');
                }
                break;
            case 'product':
                if (!$this->request->post['products']) {
                    $this->error['nohp'] = $this->language->get('error_nohp');
                }
                break;
        }
        if (!$this->request->post['message']) {
            $this->error['message'] = $this->language->get('error_message');
        }
        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }
} ?>