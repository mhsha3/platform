<?php
class ControllerExtensionModuleTabby extends Controller {
	private $version = '1.0.8';
	private $error = array();

	public function index() {
		$this->load->language('extension/module/tabby');

		$this->load->model('setting/setting');
		$this->load->model('design/layout');

		$this->document->setTitle($this->language->get('heading_title'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('module_tabby', $this->request->post);

			$this->load->model('setting/event');
			//$this->model_setting_event->deleteEventByCode('amazon_pay');
			//$this->model_setting_event->addEvent('amazon_pay', 'catalog/controller/account/logout/after', 'extension/module/amazon_pay/logout');
			
			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['heading_title'] = $this->language->get('heading_title') . ' ' . $this->version;

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true),
			'separator' => false
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true),
			'separator' => ' :: '
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/tabby', 'user_token=' . $this->session->data['user_token'], true),
			'separator' => ' :: '
		);

		$data['action'] = $this->url->link('extension/module/tabby', 'user_token=' . $this->session->data['user_token'], true);
		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);
		$data['user_token'] = $this->session->data['user_token'];

        // public key
        if (isset($this->request->post['module_tabby_public_key'])) {
            $data['public_key'] = $this->request->post['module_tabby_public_key'];
        } else {
            $data['public_key'] = $this->config->get('module_tabby_public_key');
        }

        // secret key
        if (isset($this->request->post['module_tabby_secret_key'])) {
            $data['secret_key'] = $this->request->post['module_tabby_secret_key'];
        } else {
            $data['secret_key'] = $this->config->get('module_tabby_secret_key');
        }

        // debug
        if (isset($this->request->post['module_tabby_debug'])) {
            $data['debug'] = $this->request->post['module_tabby_debug'];
        } else {
            $data['debug'] = $this->config->get('module_tabby_debug');
        }

        // geo zone id
        if (isset($this->request->post['module_tabby_geo_zone_id'])) {
            $data['geo_zone_id'] = $this->request->post['module_tabby_geo_zone_id'];
        } else {
            $data['geo_zone_id'] = $this->config->get('module_tabby_geo_zone_id');
        }

        $this->load->model('localisation/geo_zone');

        $data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

        // Order Status
        if (isset($this->request->post['module_tabby_status'])) {
            $data['status'] = $this->request->post['module_tabby_status'];
        } else {
            $data['status'] = $this->config->get('module_tabby_status');
        }

        $this->load->model('localisation/order_status');

        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

        if (isset($this->request->post['module_tabby_sort_order'])) {
            $data['sort_order'] = $this->request->post['module_tabby_sort_order'];
        } else {
            $data['sort_order'] = $this->config->get('module_tabby_sort_order');
        }




		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/tabby', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/tabby')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

    public function install() {
        $this->load->model('extension/module/tabby');

        $this->model_extension_module_tabby->createTables();
    }

    public function uninstall() {
        $this->load->model('extension/module/tabby');

        $this->model_extension_module_tabby->dropTables();
    }


}
