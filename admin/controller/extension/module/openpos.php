<?php
class ControllerExtensionModuleOpenpos extends Controller {
	private $error = array();
    public function __construct($registry) {

        parent::__construct($registry);
        $this->document->addScript('view/openpos/js/openpos.js');
        $this->document->addStyle('view/openpos/css/openpos.css');
        $this->rrmdir(DIR_CACHE);
        $this->load->library('openpos');
        $this->load->model('catalog/product');
    }

	public function index() {

        $this->document->addScript('https://www.gstatic.com/charts/loader.js');
        $this->document->addScript('view/openpos/js/jquery.timeago.js');
        $this->load->model('extension/openpos/transaction');
        $this->load->model('extension/openpos/order');

        $this->load->language('extension/openpos/openpos');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/openpos', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['total_sales'] = $this->currency->format($this->model_extension_openpos_order->getTotalSalesPosOrder(), $this->config->get('config_currency'));
        $data['total_orders'] = $this->model_extension_openpos_order->getTotalPosOrder();
        $data['total_cash'] = $this->currency->format($this->model_extension_openpos_transaction->getCashBalance(), $this->config->get('config_currency'));
        $data['last_orders'] = array();

        $orders = $this->model_extension_openpos_order->getOrders(array(
                'limit' => 10,
                'page' => 1,
                'order_by' => 'date_added',
                'sort' => 'desc'
        ));

        foreach($orders as $o)
        {
            $url = '';
            $customer_name = implode(' ',array($o['firstname'],$o['lastname']));
            $data['last_orders'][] = array(
                'order_id' => $o['order_id'],
                'edit' => $this->url->link('sale/order/info', 'user_token=' . $this->session->data['user_token'] . '&order_id=' .$o['order_id'] . $url, true),
                'customer' => $customer_name,
                'total' => $this->currency->format($o['grand_total'], $this->config->get('config_currency')),
                'created_at' => $o['date_added']
            );
        }

        $chart_data = array();
        $date_start = strtotime('-' . date('w') . ' days');
        for ($i = 0; $i < 7; $i++) {
            $date = date('Y-m-d', $date_start + ($i * 86400));
            $chart_data[] = array(
                date('D', strtotime($date)),
                $this->model_extension_openpos_order->getTotalSalePosByDate($date)
            );
        }
        $data['chart_data'] = json_encode($chart_data);
        $catlog_url = $this->config->get('config_secure') ? HTTPS_CATALOG : HTTP_CATALOG;
        $pos_url = rtrim($catlog_url,'/').'/pos/';
        $data['pos_url'] = rtrim($catlog_url,'/').'/pos/redirect.php?url='.urlencode($pos_url);

        $data['print_barcode_link'] = $this->url->link('extension/module/openpos/products', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);
        $data['transaction_link'] = $this->url->link('extension/module/openpos/transactions', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);
        $data['cashdrawer_link'] = $this->url->link('extension/module/openpos/registers', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);
        $data['setting_link'] = $this->url->link('extension/module/openpos/stores', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);
        $data['session_link'] = $this->url->link('extension/module/openpos/sessions', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

        $data['cancel'] = $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'] , true);
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/module/openpos/dashboard', $data));
	}

	public function products(){
        $this->document->addStyle('view/openpos/css/datatables.min.css');
        $this->document->addStyle('view/openpos/css/dataTables.bootstrap.min.css');
        $this->document->addScript('view/openpos/js/datatables.min.js');

        $this->load->language('extension/module/openpos');

        $this->load->language('extension/openpos/product');
        $this->document->setTitle($this->language->get('heading_title'));

        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_pos'),
            'href' => $this->url->link('extension/module/openpos', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/openpos/products', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['ajax_product_link'] = $this->url->link('extension/module/openpos/ajax_products', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);
        $data['ajax_barcode_link'] = $this->url->link('extension/module/openpos/ajax_products_barcode', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);
        $data['ajax_product_print_barcode_link'] = $this->url->link('extension/module/openpos/print_barcode', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);
        $data['cancel'] = $this->url->link('extension/module/openpos', 'user_token=' . $this->session->data['user_token'] , true);


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('extension/module/openpos/products', $data));
    }
    public function ajax_products(){
        $this->load->model('extension/openpos/product');
        $start = $this->request->get['start'];
        $length = $this->request->get['length'];
        $draw = $this->request->get['draw'];
        $columns = $this->request->get['columns'];
        $order = $this->request->get['order'];

        $sort_by = '';
        $sort_direction = '';
        if($order && !empty($order))
        {
            $_order = end($order);
            $sort_column_index = $_order['column'];
            $sort_direction = $_order['dir'];
            $sort_by = $columns[$sort_column_index]['name'];

        }

        $term = isset($this->request->get['search']) ? $this->request->get['search']['value'] : '';

        $args = array(
            'start' => $start,
            'limit' => $length,
        );
        if($term)
        {
            $args['filter_name'] = $term;
        }
        if($sort_by)
        {
            $args['sort'] = $sort_by;
            $args['order'] = strtoupper($sort_direction);
        }

        $result = $this->model_extension_openpos_product->getPosProducts($args);
        $total = $this->model_extension_openpos_product->getTotalProducts($args);

        $data = array();

        $setting = $this->openpos->getAllSettingValues(0);
        $field = $setting['pos_barcode_field'];

        foreach($result as $product)
        {

            $product_barcode_link = $this->url->link('extension/module/openpos/print_barcode', 'user_token=' . $this->session->data['user_token'] . '&type=module&id='.$product['product_id'], true);
            $data[] = array(
                '<label><input type="checkbox" class="barcode-checkbox" data-id="'.$product['product_id'].'" /></label>',
                $product['product_id'],
                $this->model_extension_openpos_product->barcode($product['product_id'],$field),
                $product['name'],
                $product['quantity'],
                '<a target="_blank" href="'.$product_barcode_link.'">Print Barcode</a>'
            );
        }

        $this->response->setOutput(json_encode(array(
            'data' => $data,
            "draw" => $draw,
            "recordsTotal" => $total,
            "recordsFiltered" => $total
        )));
    }
    public function ajax_products_barcode(){
        $ids_str = $this->request->get['ids'];
        $ids = explode(',',$ids_str);
        $html = '';
        $this->load->model('extension/openpos/product');
        foreach($ids as $id)
        {
            $product = $this->model_catalog_product->getProduct($id);
            $qty = 1 * $product['quantity'];
            $html .= '<div class="form-group"><label class="col-sm-10 control-label">'.$product['name'].'</label><div class="col-sm-2"><input type="number" name="product_qty['.$id.']" value="'.$qty.'" class="form-control" placeholder="0"></div></div>';
        }
        
        $this->response->setOutput($html );
    }

    public function print_barcode(){
        $data = array();
        $this->load->model('extension/openpos/setting');
        $this->load->model('extension/openpos/product');
        $product_id = isset($this->request->get['id']) ? $this->request->get['id'] : 0;
        $product_qty = isset($this->request->post['product_qty']) ? $this->request->post['product_qty'] : array();
        $setting = $this->openpos->getAllSettingValues(0);
        $field = $setting['pos_barcode_field'];
        if(!$product_qty)
        {
            $product_qty = array();
        }

        $store_id = 0;

        

        if(!$product_id && empty($product_qty))
        {
            $this->response->redirect($this->url->link('extension/module/openpos', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
        }
        $allSetting = $this->openpos->getAllSettingValues($store_id);
        $data['setting']['unit'] = $allSetting['unit'];
        $data['setting']['sheet_width'] = $allSetting['sheet_width'];
        $data['setting']['sheet_height'] = $allSetting['sheet_height'];
        $data['setting']['vertical_space'] = $allSetting['vertical_space'];
        $data['setting']['horizontal_space'] = $allSetting['horizontal_space'];
        $data['setting']['margin_top'] = $allSetting['margin_top'];
        $data['setting']['margin_right'] = $allSetting['margin_right'];
        $data['setting']['margin_bottom'] = $allSetting['margin_bottom'];
        $data['setting']['margin_left'] = $allSetting['margin_left'];
        $data['setting']['label_width'] = $allSetting['label_width'];
        $data['setting']['label_height'] = $allSetting['label_height'];
        $data['setting']['padding_top'] = $allSetting['padding_top'];
        $data['setting']['padding_right'] = $allSetting['padding_right'];
        $data['setting']['padding_bottom'] = $allSetting['padding_bottom'];
        $data['setting']['padding_left'] = $allSetting['padding_left'];
        $data['setting']['label_template'] = $allSetting['label_template'];
        $data['setting']['barcode_mode'] = $allSetting['barcode_mode'];
        $data['setting']['barcode_width'] = $allSetting['barcode_width'];
        $data['setting']['barcode_height'] = $allSetting['barcode_height'];


        $data['product'] = $this->model_catalog_product->getProduct($product_id);

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $request_data = $this->request->post;
            $unit = isset($request_data['unit']) ? $request_data['unit'] : $allSetting['unit']; //
            $sheet_width = isset($request_data['sheet_width']) ? $request_data['sheet_width'] : $allSetting['sheet_width']; //$this->request->post['sheet_width'];
            $sheet_height = isset($request_data['sheet_height']) ? $request_data['sheet_height'] : $allSetting['sheet_height']; //$this->request->post['sheet_height'];
            $vertical_space = isset($request_data['sheet_vertical_space']) ? $request_data['sheet_vertical_space'] : $allSetting['vertical_space']; //$this->request->post['sheet_vertical_space'];
            $horizontal_space = isset($request_data['sheet_horisontal_space']) ? $request_data['sheet_horisontal_space'] : $allSetting['horizontal_space']; //$this->request->post['sheet_horisontal_space'];
            $sheet_padding_top = isset($request_data['sheet_margin_top']) ? $request_data['sheet_margin_top'] : $allSetting['margin_top']; //$this->request->post['sheet_margin_top'];
            $sheet_padding_right = isset($request_data['sheet_margin_right']) ? $request_data['sheet_margin_right'] : $allSetting['margin_right']; //$this->request->post['sheet_margin_right'];
            $sheet_padding_bottom = isset($request_data['sheet_margin_bottom']) ? $request_data['sheet_margin_bottom'] : $allSetting['margin_bottom']; //$this->request->post['sheet_margin_bottom'];
            $sheet_padding_left = isset($request_data['sheet_margin_left']) ? $request_data['sheet_margin_left'] : $allSetting['margin_left']; //$this->request->post['sheet_margin_left'];
            $label_width = isset($request_data['label_width']) ? $request_data['label_width'] : $allSetting['label_width']; //$this->request->post['label_width'];
            $label_height = isset($request_data['label_height']) ? $request_data['label_height'] : $allSetting['label_height']; //$this->request->post['label_height'];
            //$label_margin_top = isset($request_data['label_margin_top']) ? $request_data['label_margin_top'] : $allSetting['unit']; //$this->request->post['label_margin_top'];
            //$label_margin_right = isset($request_data['label_margin_right']) ? $request_data['label_margin_right'] : $allSetting['unit']; //$this->request->post['label_margin_right'];
            //$label_margin_bottom = isset($request_data['label_margin_bottom']) ? $request_data['label_margin_bottom'] : $allSetting['unit']; //$this->request->post['label_margin_bottom'];
            //$label_margin_left = isset($request_data['label_margin_left']) ? $request_data['label_margin_left'] : $allSetting['unit']; //$this->request->post['label_margin_left'];


            $barcode_width = isset($request_data['barcode_width']) ? $request_data['barcode_width'] : $allSetting['barcode_width']; //$this->request->post['barcode_width'];
            $barcode_height = isset($request_data['barcode_height']) ? $request_data['barcode_height'] : $allSetting['barcode_height']; //$this->request->post['barcode_height'];
            $total = isset($request_data['total']) ? $request_data['total'] : 0; //$this->request->post['total'];
            if(!empty($product_qty))
            {
                $total = array_sum($product_qty);
            }else{
                $product_qty[$product_id] = $total; 
            }

            $sheet_space_width = $sheet_width - $sheet_padding_left - $sheet_padding_right + $horizontal_space ;
            $sheet_space_height = $sheet_height - $sheet_padding_top - $sheet_padding_bottom + $vertical_space ;
            $columns = ceil($sheet_space_width / ($label_width + $horizontal_space));
            $rows = ceil($sheet_space_height / ($label_height + $vertical_space));

            $truth_label_width = ($sheet_space_width  / $columns )  ;
            $truth_label_height = ($sheet_space_height  / $rows );

            $label_per_sheet = $rows * $columns;
            $page = ceil($total / $label_per_sheet);
            $count = 0;

            $templates = array();
            $barcode_mode = 'code_128';
            foreach($product_qty as $product_id => $qty)
            {
                $data['product'] = $this->model_catalog_product->getProduct($product_id);
                $price = $this->currency->format($this->tax->calculate($data['product']['price'], $data['product']['tax_class_id'], $this->config->get('config_tax')), $this->config->get('config_currency'));
                $barcode = $this->model_extension_openpos_product->barcode($data['product']['product_id'],$field);
                $rules = array(
                        '[product_name]' => $data['product']['name'],
                        '[product_barcode_number]' => $barcode,
                        '[product_price]' => $price,
                        '[barcode_image]' => '<img src="data:image/png;base64, '.$this->openpos->generateBarcode($barcode,$barcode_mode).'" style="width: '.$barcode_width.$unit.' ;max-width:'.$barcode_width.$unit.';max-height:'.$barcode_height.$unit.';height:'.$barcode_height.$unit.'">'//$this->openpos->generateBarcode($barcode,$barcode_mode),
                );
                $template = $data['setting']['label_template'];
                foreach($rules as $rkey => $rval)
                {
                    $template = str_replace($rkey,$rval,$template);
                }
                $template = html_entity_decode($template);
                for($qi = 0; $qi< $qty; $qi++)
                {
                    $templates[] = $template;
                }
            }
            
            ob_start();
            ?>
            <body style="background-color: transparent;padding:0;margin:0;">
            <?php $count_index = 0; for($k = 1;$k <= $page;$k++): ?>
                <div style="width: <?php echo $sheet_width.$unit;?>;height:<?php echo $sheet_height.$unit; ?>; display: block; overflow: hidden; background-color: transparent;" class="sheet">
                    <div style="display: block; overflow: hidden;background-color: transparent;">
                        <?php for($i = 0; $i < $rows; $i++): ?>
                            <div class="label-row" style="margin-bottom: <?php echo ($i != ($rows - 1)) ? $vertical_space.$unit:0;?>; display: block;width: 100%;">
                                <?php for($j = 0; $j < $columns; $j++): $count++; ?>
                                    <div class="label"  style=" text-align: center; width: <?php echo $truth_label_width.$unit; ?>;height: <?php echo $truth_label_height.$unit; ?>; display: inline-block;overflow: hidden; <?php echo ($j != ($columns - 1))? 'margin-right:'.$horizontal_space.$unit:'';?> " >
                                        <?php echo $templates[$count_index]; $count_index++; ?>

                                    </div>
                                    <?php if($count == $total){ break; }  endfor; ?>
                            </div>
                            <?php if($count == $total){ break; }  endfor; ?>
                    </div>
                </div>
                <?php if($k != $page): ?>
                    <div class="pagebreak"> </div>
                <?php endif; ?>
                <?php if($count == $total){ break; }  endfor; ?>
            </body>
            <?php
            $out2 = ob_get_contents();

            ob_end_clean();
            $buffer = preg_replace('/\s+/', ' ', $out2);

            $search = array(
                '/\>[^\S ]+/s',
                '/[^\S ]+\</s',
                '/(\s)+/s'
            );
            $replace = array(
                '>',
                '<',
                '\\1'
            );
            if (preg_match("/\<html/i",$buffer) == 1 && preg_match("/\<\/html\>/i",$buffer) == 1) {
                $buffer = preg_replace($search, $replace, $buffer);
            }
            $buffer = str_replace('> <', '><', $buffer);

            $data['buffer'] = $buffer;
            $data['unit'] = $unit;
            $data['sheet_width'] = $sheet_width.$unit;
            $data['sheet_height'] = $sheet_height.$unit;
            $data['sheet_padding_top'] = $sheet_padding_top.$unit;
            $data['sheet_padding_right'] = $sheet_padding_right.$unit;
            $data['sheet_padding_bottom'] = $sheet_padding_bottom.$unit;
            $data['sheet_padding_left'] = $sheet_padding_left.$unit;
            $this->response->setOutput($this->load->view('extension/module/openpos/print_barcode_paper', $data));

        }else{


            $data['title'] = 'Barcode for '.$data['product']['name'];

            $this->response->setOutput($this->load->view('extension/module/openpos/print_barcode', $data));
        }


    }

    public function preview_receipt(){
        $this->load->language('extension/openpos/openpos');
        $setting = $this->openpos->getAllSettingValues(0);


        $receipt_padding_top = $setting['receipt_padding_top'];
        $unit = 'in';
        $receipt_padding_right = $setting['receipt_padding_right'];
        $receipt_padding_bottom = $setting['receipt_padding_bottom'];
        $receipt_padding_left = $setting['receipt_padding_left'];
        $receipt_width = $setting['receipt_width'];
        $receipt_css = $setting['receipt_css'];
        $receipt_template_header = $this->openpos->_formatReceiptTemplate($setting['receipt_template_header']);
        $receipt_template = $this->openpos->receiptBodyTemplate();
        $receipt_template_footer = $this->openpos->_formatReceiptTemplate($setting['receipt_template_footer']);

        $html_header = '<style type="text/css" media="print,screen">';

        $html_header .= '#invoice-POS { ';
        $html_header .= 'padding:  '.$receipt_padding_top.$unit. ' ' . $receipt_padding_right.$unit .' '.$receipt_padding_bottom.$unit.' '.$receipt_padding_left.$unit.';';
        $html_header .= 'margin: 0 auto;';
        $html_header .= 'width: '.$receipt_width.$unit.' ;';
        $html_header .=  '}';

        $html_header .= $receipt_css;
        $html_header .= '</style>';
        $html_header .= '<body>';


        $html = '<div id="invoice-POS">';
        $html .= '<div id="invoce-header">';
        $html .= $receipt_template_header;



        $html .= '</div>';
        $html .= '<div id="bot">';


        $html .= '<div id="table">';
        $html .= $receipt_template;

        $html .= '</div><!--End Table-->';

        $html .= '<div id="invoce-footer">';
        $html .= $receipt_template_footer;
        $html .= '</div>';

        $html .= '</div><!--End InvoiceBot-->';
        $html .= '</div><!--End Invoice-->';


        $html = trim(preg_replace('/\s+/', ' ', $html));
        $data = array(
                'setting' => $setting,
                'html_header' => $html_header,
                'html_body' =>  $html,

        );

        $this->response->setOutput($this->load->view('extension/module/openpos/preview_receipt', $data));
    }

    public function sessions(){
        $this->document->addStyle('view/openpos/css/datatables.min.css');
        $this->document->addStyle('view/openpos/css/dataTables.bootstrap.min.css');
        $this->document->addScript('view/openpos/js/datatables.min.js');

        $this->load->language('extension/openpos/sessions');

        $this->document->setTitle('الجلسات');
        $data['ajax_session_link'] = $this->url->link('extension/module/openpos/ajax_sessions', 'user_token=' . $this->session->data['user_token'] , true);
        $data['cancel'] = $this->url->link('extension/module/openpos', 'user_token=' . $this->session->data['user_token'] , true);
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('extension/module/openpos/sessions', $data));
    }
    public function ajax_sessions(){
        $sessions = $this->openpos->getAllSessions();

        $this->load->model('extension/openpos/transaction');
        $this->load->model('extension/openpos/register');

        $action = isset($this->request->post['action']) ? $this->request->post['action'] : 0;
        if($action && $action == 'delete_session')
        {
            $rows_str = isset($this->request->post['data']) ? $this->request->post['data'] : '';
            $rows = explode(',',$rows_str);

            foreach($rows as $session_id)
            {
                if($session_id)
                {
                    $session_obj = $this->openpos->getPosSession();
                    $session_obj->clean($session_id);
                }
            }

        }

        $draw = isset($this->request->get['draw']) ? $this->request->get['draw'] : 1;
        $total = count($sessions);
        $data = array();
        foreach($sessions as $session_id => $session)
        {
            $register = '';
            $date_added = $session['logged_time'];
            $registers = $session['cash_drawers'];
            $login_cashdrawer_id = isset($session['login_cashdrawer_id']) ? $session['login_cashdrawer_id'] : 0;
            foreach($registers as $re)
            {
                if($re['id'] == $login_cashdrawer_id)
                {
                    $register = $re['name'];
                }
            }
            $user = isset($session['name']) ? $session['name'] : $session['username'];


            $location = 'Unknown';
            $tmp_location = array();
            if($session['location'])
            {
                $tmp_location = json_decode(html_entity_decode($session['location']),true);
            }

            if(!empty($tmp_location))
            {
                $coord = implode('%2C',array($tmp_location['latitude'],$tmp_location['longitude']));
                $url = 'https://maps.google.com/maps?q='.$coord.'&t=&z=13&ie=UTF8&iwloc=';
                $location = '<a href="'.$url.'" target="_blank" data-url="'.$url.'" class="session-location">View</a>';
            }

            $ip = isset($session['ip']) ? $session['ip'] : '0.0.0.0';
            $data[] = array(
                '<input type="checkbox" class="editor-active" data-id="'.$session_id.'">',
                $user,
                $register,
                $date_added,
                $location,
            );
        }


        $this->response->setOutput(json_encode(array(
            'data' => $data,
            "draw" => $draw,
            "recordsTotal" => (int)$total,
            "recordsFiltered" =>  (int)$total,
        )));
    }


    public function transactions(){

        $this->document->addStyle('view/openpos/css/datatables.min.css');
        $this->document->addStyle('view/openpos/css/dataTables.bootstrap.min.css');
        $this->document->addScript('view/openpos/js/datatables.min.js');

        $this->load->language('extension/openpos/transaction');

        $this->document->setTitle($this->language->get('heading_title'));
        $data['ajax_transaction_link'] = $this->url->link('extension/module/openpos/ajax_transactions', 'user_token=' . $this->session->data['user_token'] , true);
        $data['cancel'] = $this->url->link('extension/module/openpos', 'user_token=' . $this->session->data['user_token'] , true);
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('extension/module/openpos/transactions', $data));
    }
    public function ajax_transactions(){
        $this->load->model('extension/openpos/transaction');
        $this->load->model('extension/openpos/register');

        $action = isset($this->request->post['action']) ? $this->request->post['action'] : 0;
        if($action && $action == 'delete_transaction')
        {
            $rows_str = isset($this->request->post['data']) ? $this->request->post['data'] : '';
            $rows = explode(',',$rows_str);
            foreach($rows as $transaction_id)
            {
                if($transaction_id)
                {
                    $this->model_extension_openpos_transaction->deleteTransaction($transaction_id);
                }
            }

        }
        $start = isset($this->request->get['start']) ? $this->request->get['start'] : 0;
        $length = isset($this->request->get['length']) ? $this->request->get['length'] : 10;
        $draw = isset($this->request->get['draw']) ? $this->request->get['draw'] : 1;
        $columns = isset($this->request->get['columns']) ? $this->request->get['columns']:'';
        $order = isset($this->request->get['order']) ? $this->request->get['order'] : 'desc';
        $term = isset($this->request->get['search']) ? $this->request->get['search']['value'] : '';
        if(is_array($order) && !empty($order))
        {
            $tmp = end($order);
            $order = $tmp['dir'];
        }

        $args = array(
            'start' => $start,
            'limit' => $length,
            'order' => $order,
        );
        if($term)
        {
            $args['filter_note'] = $term;
        }

        $result = $this->model_extension_openpos_transaction->transactions($args);
        $total = $this->model_extension_openpos_transaction->getTotalTransactions($args);

        $data = array();

        foreach($result as $transaction)
        {
            $register = 'Unknown';
            if($transaction['register_id'])
            {
                $register_data = $this->model_extension_openpos_register->register($transaction['register_id']);
                if($register_data)
                {
                    $register = $register_data['register_name'];
                }

            }
            $by = implode(' ',array($transaction['firstname'],$transaction['lastname']));
            $data[] = array(
                '<input type="checkbox" class="editor-active" data-id="'.$transaction['transaction_id'].'">',
                $register,
                $transaction['date_added'],
                $by,
                $this->currency->format($transaction['in_amount'], $this->config->get('config_currency')),
                $this->currency->format($transaction['out_amount'], $this->config->get('config_currency')),
                $transaction['comment'],
            );
        }

        $this->response->setOutput(json_encode(array(
            'data' => $data,
            "draw" => $draw,
            "recordsTotal" => (int)$total,
            "recordsFiltered" =>  (int)$total,
        )));
    }

    public function registers(){
        $this->load->model('extension/openpos/register');
        $this->load->model('extension/openpos/transaction');
        $this->load->model('extension/openpos/order');


        $this->load->language('extension/openpos/register');

        $data['cancel'] = $this->url->link('extension/module/openpos', 'user_token=' . $this->session->data['user_token'] , true);

        $data['new_register_link'] = $this->url->link('extension/module/openpos/new_register', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);


        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_pos'),
            'href' => $this->url->link('extension/module/openpos', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/openpos/registers', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['registers'] = array();
         $registers = $this->model_extension_openpos_register->registers();
        foreach($registers as $register)
        {
            $cashiers = $this->model_extension_openpos_register->cashiers($register['register_id']);
            $catlog_url = $this->config->get('config_secure') ? HTTPS_CATALOG : HTTP_CATALOG;
            $register['bill_url'] = rtrim($catlog_url,'/').'/pos/bill/index.php?id='.$register['register_id'];
            $register['cashiers'] = $cashiers;
            $register['total_sales'] = $this->currency->format($this->model_extension_openpos_order->getTotalSalesPosOrder($register['register_id']), $this->config->get('config_currency'));
            $register['cash_balance'] = $this->currency->format($this->model_extension_openpos_transaction->getRegisterCashBalance($register['register_id']), $this->config->get('config_currency'));
            $register['edit_link'] = $this->url->link('extension/module/openpos/edit_register', 'user_token=' . $this->session->data['user_token'].'&id='.$register['register_id'],true);
            $data['registers'][] = $register;
        }
        $data['delete_url'] = $this->url->link('extension/module/openpos/delete_register', 'user_token=' . $this->session->data['user_token'],true);

        $this->document->setTitle($this->language->get('heading_title'));
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('extension/module/openpos/registers', $data));
    }
    public function delete_register(){
        $result = array(
          'status' => 0,
          'message' => ''
        );
        $this->load->model('extension/openpos/register');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $register_id = $this->request->post['id'];
            if($register_id)
            {
                $this->model_extension_openpos_register->delete($register_id);
            }
            $result['status'] = 1;

        }else{
            $result['message'] = 'You do not have permission on openpos';
        }
        echo json_encode($result);exit;
    }
    public function new_register(){
        $this->load->model('setting/store');
        $this->load->model('user/user');
        $this->load->model('extension/openpos/register');
        $this->load->language('extension/openpos/register');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $result = array('status' => 0 , 'message' => '');
            try{
                $register_id = $this->request->post['register_id'];
                if(!isset($this->request->post['register_name']))
                {
                    throw  new Exception('Please enter register name');
                }
                $register_name = $this->request->post['register_name'];
                $register_store_id = $this->request->post['store_id'];
                $register_cashiers = isset($this->request->post['cashiers']) ? $this->request->post['cashiers'] :  array();
                $status = $this->request->post['status'];
                $register_data = array(
                        'register_name' => $register_name,
                        'store_id' => $register_store_id,
                        'cashiers' => $register_cashiers,
                        'status' => $status
                );
                $this->model_extension_openpos_register->save($register_data);
                $result['status'] = 1;
            }catch (Exception $e)
            {
                $result = array('status' => 0 , 'message' => $e->getMessage());
            }
            echo json_encode($result);
            exit;
        }

        $stores = array();
        $stores[] = array(
            'store_id' => 0,
            'name'     => $this->config->get('config_name') . $this->language->get('text_default'),
            'total_products' => 0,
            'total_pos_sales' => 0,
        );
        $addition_stores = $this->model_setting_store->getStores();
        foreach($addition_stores as $addition_store)
        {
            $stores[] = array(
                'store_id' => $addition_store['store_id'],
                'name'     => $addition_store['name'],
                'total_products' => 0,
                'total_pos_sales' => 0,

            );
        }
        $data['register_id'] = 0;
        $data['stores'] = $stores;

        $data['users'] = $this->model_user_user->getUsers();
        $data['save_url'] = $this->url->link('extension/module/openpos/new_register', 'user_token=' . $this->session->data['user_token'],true);

        $data['cancel'] = $this->url->link('extension/module/openpos/registers', 'user_token=' . $this->session->data['user_token'] , true);

        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => 'Open POS',
            'href' => $this->url->link('extension/module/openpos', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => 'New Register',
            'href' => '#'
        );

        $this->document->setTitle('OpenPOS - New Register');
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('extension/module/openpos/new_register', $data));
    }

    public function edit_register(){
        $this->load->model('setting/store');
        $this->load->model('user/user');
        $this->load->model('extension/openpos/register');
        $this->load->language('extension/openpos/register');


        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $result = array('status' => 0 , 'message' => '');
            try{
                $register_id = $this->request->post['register_id'];
                if(!isset($this->request->post['register_name']))
                {
                    throw  new Exception('Please enter register name');
                }
                $register_name = $this->request->post['register_name'];
                $register_store_id = $this->request->post['store_id'];
                $register_cashiers = isset($this->request->post['cashiers']) ? $this->request->post['cashiers'] :  array();
                $status = $this->request->post['status'];
                $register_data = array(
                    'register_id' => $register_id,
                    'register_name' => $register_name,
                    'store_id' => $register_store_id,
                    'cashiers' => $register_cashiers,
                    'status' => $status
                );
                $this->model_extension_openpos_register->save($register_data);
                $result['status'] = 1;
            }catch (Exception $e)
            {
                $result = array('status' => 0 , 'message' => $e->getMessage());
            }
            echo json_encode($result);
            exit;
        }

        $id = isset($this->request->get['id']) ? $this->request->get['id'] : 0;

        if(!$id)
        {
            $this->response->redirect($this->url->link('extension/module/openpos/registers', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
        }
        $cashiers = array();
        $data['register_id'] = $id;
        $register_data =   $this->model_extension_openpos_register->register($id);
        $cashiers_data = $this->model_extension_openpos_register->cashiers($id);
        foreach($cashiers_data as $cd)
        {
            $cashiers[] = $cd['user_id'];
        }
        $data['register'] = array(
                'register_name' => $register_data['register_name'],
                'store_id' => $register_data['store_id'],
                'status' => $register_data['status'],
                'cashiers' => $cashiers
        );
        $stores = array();
        $stores[] = array(
            'store_id' => 0,
            'name'     => $this->config->get('config_name') . $this->language->get('text_default'),
            'total_products' => 0,
            'total_pos_sales' => 0,
        );
        $addition_stores = $this->model_setting_store->getStores();
        foreach($addition_stores as $addition_store)
        {
            $stores[] = array(
                'store_id' => $addition_store['store_id'],
                'name'     => $addition_store['name'],
                'total_products' => 0,
                'total_pos_sales' => 0,

            );
        }

        $data['stores'] = $stores;

        $data['users'] = $this->model_user_user->getUsers();
        $data['save_url'] = $this->url->link('extension/module/openpos/edit_register', 'user_token=' . $this->session->data['user_token'],true);


        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => 'Open POS',
            'href' => $this->url->link('extension/module/openpos', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => 'Edit Register',
            'href' => '#'
        );
        $data['cancel'] = $this->url->link('extension/module/openpos/registers', 'user_token=' . $this->session->data['user_token'] , true);
        $this->document->setTitle('OpenPOS - Edit Register');
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('extension/module/openpos/new_register', $data));
    }

    public function stores(){
        $this->load->model('setting/store');
        $this->load->model('extension/openpos/transaction');
        $this->load->model('extension/openpos/order');


        $this->load->language('extension/openpos/stores');
        $this->document->setTitle($this->language->get('heading_title'));


        $stores = array();
        $stores[] = array(
            'store_id' => 0,
            'name'     => $this->config->get('config_name') . $this->language->get('text_default'),
            'total_cash' =>  $this->currency->format($this->model_extension_openpos_transaction->getCashBalance(0), $this->config->get('config_currency')),
            'total_pos_sales' =>  $this->currency->format($this->model_extension_openpos_order->getTotalSalesPosOrderByStore(0), $this->config->get('config_currency')),
            'setting_url' => $this->url->link('extension/module/openpos/setting', 'user_token=' . $this->session->data['user_token'] . '&type=module&store_id=0', true)
        );
        $addition_stores = $this->model_setting_store->getStores();
        foreach($addition_stores as $addition_store)
        {
            $stores[] = array(
                'store_id' => $addition_store['store_id'],
                'name'     => $addition_store['name'],
                'total_cash' => $this->currency->format($this->model_extension_openpos_transaction->getCashBalance($addition_store['store_id']), $this->config->get('config_currency')),
                'total_pos_sales' => $this->currency->format($this->model_extension_openpos_order->getTotalSalesPosOrderByStore($addition_store['store_id']), $this->config->get('config_currency')),
                'setting_url' => $this->url->link('extension/module/openpos/setting', 'user_token=' . $this->session->data['user_token'] . '&type=module&store_id='.$addition_store['store_id'], true)
            );
        }


        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => 'Open POS',
            'href' => $this->url->link('extension/module/openpos', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => 'Stores',
            'href' => '#'
        );

        $data['cancel'] = $this->url->link('extension/module/openpos', 'user_token=' . $this->session->data['user_token'] , true);
        $data['stores'] = $stores;
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('extension/module/openpos/stores', $data));
    }

    public function setting(){
        $this->load->language('extension/openpos/setting');
        $this->load->model('extension/openpos/setting');
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $setting = $this->request->post['setting'];
            $store_id = $this->request->post['store_id'];
            $this->model_extension_openpos_setting->saveSetting($setting,$store_id);
            $this->session->data['success'] = $this->language->get('msg_success');
            echo json_encode(array('status'=> 1));
            exit;
        }



        $this->document->setTitle( $this->language->get('heading_title'));
        $data['user_token'] = $this->session->data['user_token'];

        $data['success'] = isset($this->session->data['success']) ? $this->session->data['success'] : '';

        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_pos'),
            'href' => $this->url->link('extension/module/openpos', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/openpos/setting', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['store_id'] = isset($this->request->get['store_id']) ? $this->request->get['store_id'] : 0;

        $data['settings'] = $this->openpos->getSettings($data['store_id']);

        $this->load->model('catalog/category');
        $data['categories'] = array();

        $filter_data = array(
            'start' => 0,
            'limit' => 1000
        );

        $results = $this->model_catalog_category->getCategories($filter_data);
        foreach ($results as $result) {
            $data['categories'][$result['category_id']] = $result['name'];
        }
        $data['save_url'] = $this->url->link('extension/module/openpos/setting', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);
        $data['cancel'] = $this->url->link('extension/module/openpos/stores', 'user_token=' . $this->session->data['user_token'] , true);
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->session->data['success'] = "";
        $this->response->setOutput($this->load->view('extension/module/openpos/setting', $data));
    }

    public function order_info(){
        $order_id = $this->request->get['order_id'];

        $this->load->language('extension/openpos/info');

        $this->load->model('extension/openpos/order');
        $this->load->model('extension/openpos/register');
        $this->load->model('user/user');

        $pos_order_data = $this->model_extension_openpos_order->getPosOrder($order_id);
        $register_data = $this->model_extension_openpos_register->register($pos_order_data['register_id']);
        $cashier_data = $this->model_user_user->getUser($pos_order_data['cashier_user_id']);
        $seller_data = $this->model_user_user->getUser($pos_order_data['seller_user_id']);

        $c_firstname = isset($cashier_data['firstname']) ? $cashier_data['firstname'] : '';
        $c_lastname = isset($cashier_data['lastname']) ? $cashier_data['lastname'] : '';
        $s_firstname = isset($seller_data['firstname']) ? $seller_data['firstname'] : '';
        $s_lastname = isset($seller_data['lastname']) ? $seller_data['lastname'] : '';

        $data = array(
            'register_name' => $register_data['register_name'],
            'cashier_name' => implode( ' ',array($c_firstname,$c_lastname)),
            'seller_name' => implode( ' ',array($s_firstname,$s_lastname)),
        );



        $data['payments'] =  $this->model_extension_openpos_order->getPosOrderPayments($order_id);

        return $this->load->view('extension/module/openpos/order_info', $data);
    }

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/openpos')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
    public function rrmdir($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != ".." ) {
                    if (filetype($dir."/".$object) == "dir")
                        $this->rrmdir($dir."/".$object);
                    else unlink   ($dir."/".$object);
                }
            }
            reset($objects);
        }
    }

    public function install() {

        $this->db->query("
				CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "openpos_transaction` (
				      `transaction_id` int(11) NOT NULL AUTO_INCREMENT,
                      `created_by` int(11) NOT NULL DEFAULT '0',
                      `in_amount` decimal(16,2) NOT NULL DEFAULT '0.00',
                      `out_amount` decimal(16,2) NOT NULL DEFAULT '0.00',
                      `register_id` int(11) NOT NULL DEFAULT '0',
                      `store_id` int(11) NOT NULL DEFAULT '0',
                      `comment` text,
                      `local_id` VARCHAR(255),
                      `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      PRIMARY KEY (`transaction_id`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");

        $this->db->query("
				CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "openpos_order` (
				      `pos_order_id` int(11) NOT NULL AUTO_INCREMENT,
                      `cashier_user_id` int(11) NOT NULL DEFAULT '0',
                      `seller_user_id` int(11) NOT NULL DEFAULT '0',
                      `order_id`  int(11) NOT NULL DEFAULT '0.00',
                      `register_id` int(11) NOT NULL DEFAULT '0',
                      `comment` text,
                      `local_id` VARCHAR(255),
                      `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      PRIMARY KEY (`pos_order_id`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");

        $this->db->query("
				CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "openpos_order_payment` (
				      `pos_payment_id` int(11) NOT NULL AUTO_INCREMENT,
                      `payment_code` VARCHAR(255) NOT NULL DEFAULT '',
                      `payment_title` VARCHAR(255) NOT NULL DEFAULT '',
                      `amount` decimal(16,2) NOT NULL DEFAULT '0.00',
                      `order_id` int(11) NOT NULL DEFAULT '0',
                      `comment` text,
                      `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      PRIMARY KEY (`pos_payment_id`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");

        $this->db->query("
				CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "openpos_register` (
				  `register_id` int(11) NOT NULL AUTO_INCREMENT,
				  `register_name` VARCHAR(255) NOT NULL,
				  `store_id` int(11) NOT NULL,
				  `status` int(1) NOT NULL,
				  `date_added` DATETIME NOT NULL,
				  `date_modified` DATETIME NOT NULL,
				  PRIMARY KEY (`register_id`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");

        $this->db->query("
				CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "openpos_register_user` (
				  `register_user_id` int(11) NOT NULL AUTO_INCREMENT,
				  `register_id` int(11) NOT NULL,
				  `user_id` int(11) NOT NULL,
				  `date_added` DATETIME NOT NULL,
				  `date_modified` DATETIME NOT NULL,
				  PRIMARY KEY (`register_user_id`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");

        $this->db->query("
        CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "openpos_product_log` (
            `product_log_id` int(11) NOT NULL AUTO_INCREMENT,
            `product_id` int(11) NOT NULL,
            `version_id` int(11) NOT NULL,
            PRIMARY KEY (`product_log_id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");

        $this->db->query("
        CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "openpos_z_report` (
            `z_id` int(11) NOT NULL AUTO_INCREMENT,
            `session` VARCHAR(255) NOT NULL,
            `report_content` text,
            `date_added` DATETIME NOT NULL,
			`date_modified` DATETIME NOT NULL,
            PRIMARY KEY (`z_id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");

        $this->db->query("
        CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "openpos_receipt_template` (
            `template_id` int(11) NOT NULL AUTO_INCREMENT,
            `template_content` text,
            `template_css` text,
            `date_added` DATETIME NOT NULL,
			`date_modified` DATETIME NOT NULL,
            PRIMARY KEY (`template_id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");
        
        $this->db->query("ALTER TABLE `". DB_PREFIX . "openpos_order` ADD COLUMN  `session` VARCHAR(255) NOT NULL AFTER `local_id`;");
        $this->db->query("ALTER TABLE `". DB_PREFIX . "openpos_order` ADD COLUMN `order_content` text NOT NULL AFTER `register_id`;");
        $this->db->query("ALTER TABLE `". DB_PREFIX . "openpos_transaction` ADD COLUMN `session` VARCHAR(255) NOT NULL AFTER `local_id`;");
        $this->db->query("ALTER TABLE `". DB_PREFIX . "openpos_transaction` ADD COLUMN `transaction_content` text NOT NULL AFTER `local_id`;");
        /*
        $this->db->query("ALTER TABLE `". DB_PREFIX . "openpos_order` ADD COLUMN    IF NOT EXISTS `session` VARCHAR(255) NOT NULL AFTER `local_id`;");
        $this->db->query("ALTER TABLE `". DB_PREFIX . "openpos_order` ADD COLUMN IF NOT EXISTS `order_content` text NOT NULL AFTER `register_id`;");
        $this->db->query("ALTER TABLE `". DB_PREFIX . "openpos_transaction` ADD COLUMN  IF NOT EXISTS  `session` VARCHAR(255) NOT NULL AFTER `local_id`;");
        $this->db->query("ALTER TABLE `". DB_PREFIX . "openpos_transaction` ADD COLUMN  IF NOT EXISTS  `transaction_content` text NOT NULL AFTER `local_id`;");
        */
        
    }

    public function uninstall() {
        $this->db->query("TRUNCATE TABLE `". DB_PREFIX . "openpos_product_log`;");
    }
}