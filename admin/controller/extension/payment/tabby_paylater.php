<?php
class ControllerExtensionPaymentTabbyPaylater extends Controller {
	private $error = array();
    protected $code = 'tabby_paylater';
    const DEFAULT_TITLE = "Pay in 14 days with Tabby. No fees.";
	
	public function index() {
		$this->load->language('extension/payment/' . $this->code);

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		$this->load->model('extension/module/tabby');

        if (!$this->model_extension_module_tabby->getIsConfigured()) {
            $this->error['warning'] = $this->model_extension_module_tabby->getConfigureWarningMessage();
        }
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('payment_' . $this->code, $this->request->post);
														
			$this->session->data['success'] = $this->language->get('success_save');

			$this->response->redirect($this->url->link('common/services/payment', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true));
		}
			
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extensions'),
			'href' => $this->url->link('common/services/payment', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/payment/' . $this->code, 'user_token=' . $this->session->data['user_token'], true)
		);
						
		$data['action'] = $this->url->link('extension/payment/' . $this->code, 'user_token=' . $this->session->data['user_token'], true);
		$data['cancel'] = $this->url->link('common/services/payment', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true);
		
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$data['server'] = HTTPS_SERVER;
			$data['catalog'] = HTTPS_CATALOG;
		} else {
			$data['server'] = HTTP_SERVER;
			$data['catalog'] = HTTP_CATALOG;
		}
		
        $data['module_code'] = $this->code;

        foreach (['status', 'order_status_id', 'geo_zone_id', 'title', 'terms', 'sort_order'] as $field) {
            $fname = 'payment_' . $this->code . '_' . $field;

		    if (isset($this->request->post[$fname])) {
			    $data[$field] = $this->request->post[$fname];
		    } else {
			    $data[$field] = $this->config->get($fname);
		    }
        }
        
        $this->load->model('localisation/geo_zone');

        $data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

        $this->load->model('localisation/order_status');

        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

        if (empty($data['title'])) {
            $data['title'] = static::DEFAULT_TITLE;
        }
        
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
					
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/payment/tabby', $data));
	}
		
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/payment/' . $this->code)) {
			$this->error['warning'] = $this->language->get('error_permission');
        }

		return !$this->error;
	}
    public function order() {

		$this->load->language('extension/module/tabby');

        $data = [];

		$this->load->model('extension/module/tabby');
        $txn = $this->model_extension_module_tabby->getTransaction($this->request->get['order_id']);

        if ($txn && !empty($txn['body'])) {
            $txn = json_decode($txn['body']);
            $data['txn'] = $txn;
        } else {
            $data['text_no_transaction'] = $this->language->get('text_no_transaction');
        }
		return $this->load->view('extension/payment/tabby_order', $data);
    }
	
}
