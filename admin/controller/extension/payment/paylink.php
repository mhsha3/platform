<?php
class ControllerExtensionPaymentPaylink extends Controller {
	private $error = array();
	public function install() {
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "success_orders_payments_tracking` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `transaction_num` varchar(255) NOT NULL,
          `order_id` int(11) NOT NULL,
          `amount` float NOT NULL,
          `payment_company` varchar(255) NOT NULL,
          `response` text NOT NULL,
          `created_at` datetime NOT NULL,
          `lana_payed` int(11) NOT NULL,
		  PRIMARY KEY (`id`)
        )");
	}
	public function index() {
		$this->install();
		$this->load->language('extension/payment/paylink');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('payment_paylink', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('common/services/payment', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('common/services/payment', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/payment/paylink', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/payment/paylink', 'user_token=' . $this->session->data['user_token'], true);

		$data['cancel'] = $this->url->link('common/services/payment', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true);

		if (isset($this->request->post['payment_paylink_apiId'])) {
			$data['payment_paylink_apiId'] = $this->request->post['payment_paylink_apiId'];
		} else {
			$data['payment_paylink_apiId'] = $this->config->get('payment_paylink_apiId');
		}
		
		if (isset($this->request->post['payment_paylink_secretKey'])) {
			$data['payment_paylink_secretKey'] = $this->request->post['payment_paylink_secretKey'];
		} else {
			$data['payment_paylink_secretKey'] = $this->config->get('payment_paylink_secretKey');
		}

		if (isset($this->request->post['payment_paylink_total'])) {
			$data['payment_paylink_total'] = $this->request->post['payment_paylink_total'];
		} else {
			$data['payment_paylink_total'] = $this->config->get('payment_paylink_total');
		}

		if (isset($this->request->post['payment_paylink_order_status_id'])) {
			$data['payment_paylink_order_status_id'] = $this->request->post['payment_paylink_order_status_id'];
		} else {
			$data['payment_paylink_order_status_id'] = $this->config->get('payment_paylink_order_status_id');
		}

		$this->load->model('localisation/order_status');

		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		if (isset($this->request->post['payment_paylink_geo_zone_id'])) {
			$data['payment_paylink_geo_zone_id'] = $this->request->post['payment_paylink_geo_zone_id'];
		} else {
			$data['payment_paylink_geo_zone_id'] = $this->config->get('payment_paylink_geo_zone_id');
		}

		$this->load->model('localisation/geo_zone');

		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

		if (isset($this->request->post['payment_paylink_status'])) {
			$data['payment_paylink_status'] = $this->request->post['payment_paylink_status'];
		} else {
			$data['payment_paylink_status'] = $this->config->get('payment_paylink_status');
		}

		if (isset($this->request->post['payment_paylink_sort_order'])) {
			$data['payment_paylink_sort_order'] = $this->request->post['payment_paylink_sort_order'];
		} else {
			$data['payment_paylink_sort_order'] = $this->config->get('payment_paylink_sort_order');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/payment/paylink', $data));
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/payment/paylink')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}