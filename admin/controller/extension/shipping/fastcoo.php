<?php
class ControllerExtensionShippingFastcoo extends Controller {
	private $error = array();
	public function index() {
		$this->load->language('extension/shipping/fastcoo');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('shipping_fastcoo', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=shipping', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['email_id'])) {
			$data['error_email_id'] = $this->error['email_id'];
		} else {
			$data['error_email_id'] = '';
		}

		if (isset($this->error['password'])) {
			$data['error_password'] = $this->error['password'];
		} else {
			$data['error_password'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=shipping', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/shipping/fastcoo', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/shipping/fastcoo', 'user_token=' . $this->session->data['user_token'], true);
		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=shipping', true);

		if (isset($this->request->post['shipping_fastcoo_email_id'])) {
			$data['shipping_fastcoo_email_id'] = $this->request->post['shipping_fastcoo_email_id'];
		} else {
			$data['shipping_fastcoo_email_id'] = $this->config->get('shipping_fastcoo_email_id');
		}

		if (isset($this->request->post['shipping_fastcoo_password'])) {
			$data['shipping_fastcoo_password'] = $this->request->post['shipping_fastcoo_password'];
		} else {
			$data['shipping_fastcoo_password'] = $this->config->get('shipping_fastcoo_password');
		}

		if (isset($this->request->post['shipping_fastcoo_cost'])) {
			$data['shipping_fastcoo_cost'] = $this->request->post['shipping_fastcoo_cost'];
		} else {
			$data['shipping_fastcoo_cost'] = $this->config->get('shipping_fastcoo_cost');
		}

		if (isset($this->request->post['shipping_fastcoo_status'])) {
			$data['shipping_fastcoo_status'] = $this->request->post['shipping_fastcoo_status'];
		} else {
			$data['shipping_fastcoo_status'] = $this->config->get('shipping_fastcoo_status');
		}

		if (isset($this->request->post['shipping_fastcoo_sort_order'])) {
			$data['shipping_fastcoo_sort_order'] = $this->request->post['shipping_fastcoo_sort_order'];
		} else {
			$data['shipping_fastcoo_sort_order'] = $this->config->get('shipping_fastcoo_sort_order');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/shipping/fastcoo', $data));
	
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/shipping/fastcoo')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->request->post['shipping_fastcoo_email_id']) {
			$this->error['email_id'] = $this->language->get('error_email_id');
		}
		
		if (!$this->request->post['shipping_fastcoo_password']) {
			$this->error['password'] = $this->language->get('error_password');
		}
		
		return !$this->error;
	}
}