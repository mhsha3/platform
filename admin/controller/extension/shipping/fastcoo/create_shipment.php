<?php

class ControllerExtensionShippingFastcooCreateShipment extends Controller {

    private $error = array();
    private $api;

    public function __construct($registry) {
        parent::__construct($registry);
        require_once DIR_SYSTEM . '../admin/controller/extension/shipping/fastcoo/api.php';
        $this->api = new \Fastcoo\Api($this->config->get('shipping_fastcoo_email_id'), $this->config->get('shipping_fastcoo_password'));
    }

    public function index() {

      $this->language->load('extension/shipping/fastcoo/create_shipment');
      $this->getForm();

  }

  public function getForm() {

      $this->load->model('sale/order');
      $this->load->model('setting/setting');
      $this->load->model('localisation/country');
      $this->load->model('extension/shipping/fastcoo/fastcoo');

      $order_id = $this->request->get['order_id'] ?? 0;
      $order_info = $this->model_sale_order->getOrder($order_id);

      if ($order_info) {

            $data['user_token'] = $this->session->data['user_token'];

            ##################### Language Labels #####################

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], 'SSL'),
                'separator' => false
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'], 'SSL'),
                'separator' => ' :: '
            );

            $data['order_id'] = $this->request->get['order_id'];
            $data['ordernumber'] = $order_id;

            ##################### Buttons #####################

            $data['back_to_order'] = $this->url->link('sale/order/info', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $order_id, true);
            $data['create_sipment'] = $this->url->link('extension/shipping/fastcoo/create_shipment', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $order_id, 'SSL');

            $is_shipment = $this->model_extension_shipping_fastcoo_fastcoo->checkAWB($order_id);
            $data['is_shipment'] = $is_shipment;

            if($is_shipment) {
                $AWBNumber = $this->model_extension_shipping_fastcoo_fastcoo->getAWBNumber($order_id);
                $trackingOrderResponse = $this->api->trackingOrder(['awb_no' => $AWBNumber]);
                if(isset($trackingOrderResponse['error'])) {
                    if(is_array($trackingOrderResponse['error']) && count($trackingOrderResponse['error'])) {
                        foreach($trackingOrderResponse['error'] as $an_error) $data["eRRORS"][] = $an_error;
                    } else {
                        $data["eRRORS"][] = $trackingOrderResponse['error'];
                    }
                    $data['can_be_canceled'] = false;
                    $data['print_label_url'] = null;
                } else {
                    $data['print_label_url'] = $this->model_extension_shipping_fastcoo_fastcoo->getAWBPrintURL($order_id);
                    $data['read_shipment'] = $trackingOrderResponse;
                    $data['can_be_canceled'] = true;
                }

            } else {

                ##################### Sender details #####################

                $store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);

                if ($store_info) {
                    $store_name = $store_info['config_name'];
                    $store_email = $store_info['config_email'];
                    $store_telephone = $store_info['config_telephone'];
                    $store_address = $store_info['config_address'];
                    $store_zone_id = $store_info['config_zone_id'];
                    $store_country_id = $store_info['config_country_id'];
                } else {
                    $store_name = $this->config->get('config_name');
                    $store_email = $this->config->get('config_email');
                    $store_telephone = $this->config->get('config_telephone');
                    $store_address = $this->config->get('config_address');
                    $store_zone_id = $this->config->get('config_zone_id');
                    $store_country_id = $this->config->get('config_country_id');
                }

                $this->load->model('localisation/zone');
                $zone_info = $this->model_localisation_zone->getZone($store_zone_id);

                if (isset($this->request->post['sender_name'])) {
                    $data['sender_name'] = $this->request->post['sender_name'];
                } else {
                    $data['sender_name'] = $store_name;
                }

                if (isset($this->request->post['sender_mobile'])) {
                    $data['sender_mobile'] = $this->request->post['sender_mobile'];
                } else {
                    $data['sender_mobile'] = $store_telephone;
                }

                if (isset($this->request->post['sender_address'])) {
                    $data['sender_address'] = $this->request->post['sender_address'];
                } else {
                    $data['sender_address'] = $store_address;
                }

                if (isset($this->request->post['sender_city'])) {
                    $data['sender_city'] = $this->request->post['sender_city'];
                } else {
                    $data['sender_city'] = $zone_info ? $zone_info["name"] : "";
                }

                ##################### Receiver details #####################

                $shipment_receiver_name = "";
                if (isset($order_info['shipping_firstname']) && !empty($order_info['shipping_firstname'])) {
                    $shipment_receiver_name .= $order_info['shipping_firstname'];
                }
                if (isset($order_info['shipping_lastname']) && !empty($order_info['shipping_lastname'])) {
                    $shipment_receiver_name .= " " . $order_info['shipping_lastname'];
                }

                if (isset($this->request->post['receiver_name'])) {
                    $data['receiver_name'] = $this->request->post['receiver_name'];
                } else {
                    $data['receiver_name'] = trim($shipment_receiver_name);
                }

                if (isset($this->request->post['receiver_email'])) {
                    $data['receiver_email'] = $this->request->post['receiver_email'];
                } else {
                    $data['receiver_email'] = ($order_info['email']) ? $order_info['email'] : '';
                }

                if (isset($this->request->post['receiver_address'])) {
                    $data['receiver_address'] = $this->request->post['receiver_address'];
                } else {
                    $data['receiver_address'] = ($order_info['shipping_address_1']) ? $order_info['shipping_address_1'] : '';
                }

                if (isset($this->request->post['receiver_phone'])) {
                    $data['receiver_phone'] = $this->request->post['receiver_phone'];
                } else {
                    $data['receiver_phone'] = ($order_info['telephone']) ? $order_info['telephone'] : "";
                }
    
                if (isset($this->request->post['receiver_city'])) {
                    $data['receiver_city'] = $this->request->post['receiver_city'];
                } else {
                    $data['receiver_city'] = ($order_info['shipping_city']) ? $order_info['shipping_city'] : "";
                }

                ##################### Shipment details #####################

                $data['product_types'] = array();
                $data['product_types'][] = array(
                  'value' => 'KVAIMI',
                  'text'  => $this->language->get('text_product_type_kvaimi')
                );
                $data['product_types'][] = array(
                  'value' => 'PARCEL',
                  'text'  => $this->language->get('text_product_type_parcel')
                );

                if (isset($this->request->post['product_type'])) {
                    $data['product_type'] = $this->request->post['product_type'];
                } else {
                    $data['product_type'] = "";
                }

                $data['delivery_services'] = array();
                $data['delivery_services'][] = array(
                    'value' => 4,
                    'text'  => $this->language->get('text_delivery_service_same_day')
                );
                $data['delivery_services'][] = array(
                    'value' => 3,
                    'text'  => $this->language->get('text_delivery_service_next_day')
                );

                if (isset($this->request->post['delivery_service'])) {
                    $data['delivery_service'] = $this->request->post['delivery_service'];
                } else {
                    $data['delivery_service'] = "";
                }

                $data['booking_modes'] = array();
                $data['booking_modes'][] = array(
                    'value' => 'CC',
                    'text'  => $this->language->get('text_booking_mode_cc')
                );
                $data['booking_modes'][] = array(
                    'value' => 'COD',
                    'text'  => $this->language->get('text_booking_mode_cod')
                );

                if (isset($this->request->post['booking_mode'])) {
                    $data['booking_mode'] = $this->request->post['booking_mode'];
                } else {
                    $data['booking_mode'] = "";
                }

                if (isset($this->request->post['reference'])) {
                    $data['reference'] = $this->request->post['reference'];
                } else {
                    $data['reference'] = $order_id;
                }

                if (isset($this->request->post['shipment_weight'])) {
                    $data['shipment_weight'] = $this->request->post['shipment_weight'];
                } else {
                    $data['shipment_weight'] = "";
                }

                if (isset($this->request->post['shipment_description'])) {
                    $data['shipment_description'] = $this->request->post['shipment_description'];
                } else {
                    $data['shipment_description'] = "";
                }

                if (isset($this->request->post['number_of_packets'])) {
                    $data['number_of_packets'] = $this->request->post['number_of_packets'];
                } else {
                    $data['number_of_packets'] = "";
                }

                if (isset($this->request->post['cod_value'])) {
                    $data['cod_value'] = $this->request->post['cod_value'];
                } else {
                    $data['cod_value'] = "";
                }

                $order_products = $this->model_sale_order->getOrderProducts($order_id);
                $weighttot = 0;
                foreach ($order_products as $order_product) {
                    if (isset($order_product['order_option'])) {
                        $order_option = $order_product['order_option'];
                    } elseif (isset($this->request->get['order_id'])) {
                        $order_option = $this->model_sale_order->getOrderOptions($this->request->get['order_id'], $order_product['order_product_id']);
                        $product_weight_query = $this->db->query("SELECT weight, weight_class_id FROM " . DB_PREFIX . "product WHERE product_id = '" . $order_product['product_id'] . "'");
                        $weight_class_query = $this->db->query("SELECT wcd.unit FROM " . DB_PREFIX . "weight_class wc LEFT JOIN " . DB_PREFIX . "weight_class_description wcd ON (wc.weight_class_id = wcd.weight_class_id) WHERE wcd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND wc.weight_class_id = '" . $product_weight_query->row['weight_class_id'] . "'");
                    } else {
                        $order_option = array();
                    }

                    $config_weight_class_id = $this->config->get('config_weight_class_id');
                    $prodweight = $this->weight->convert($product_weight_query->row['weight'], $product_weight_query->row['weight_class_id'], $config_weight_class_id);
                    $prodweight = ($prodweight * $order_product['quantity']);
                    $weighttot = ($weighttot + $prodweight);
                
                    $order_products_data[] = array(
                        'comment' => $order_product['name'],
                        'quantity' => $order_product['quantity']
                    );
                }

                $data['weight'] = number_format($weighttot, 2);
                $data['total'] = number_format($order_info['total'], 2);
                $data['total'] = $this->currency->format(str_replace(',', '', $data['total']), $order_info['currency_code'], $order_info['currency_value'], false);
                $data['order_products'] = $order_products_data;
                $data['currency_code'] = ($order_info['currency_code']) ? $order_info['currency_code'] : 'SAR';
        
                ################## Create Shipment ##################

                if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

                    try {

                        $shipmentPushingData = array(
                            'sender_name' => $data['sender_name'],
                            'sender_mobile' => $data['sender_mobile'],
                            'sender_address' => $data['sender_address'],
                            'sender_city' => $data['sender_city'],
                            
                            'Receiver_name' => $data['receiver_name'],
                            'Receiver_email' => $data['receiver_email'],
                            'Receiver_address' => $data['receiver_address'],
                            'Receiver_phone' => $data['receiver_phone'],
                            'Reciever_city' => $data['receiver_city'],

                            'refrence_id' => $data['reference'],
                            'productType' => $data['product_type'],
                            'service' => $data['delivery_service'],
                            'Weight' => $data['shipment_weight'],
                            'Description' => $data['shipment_description'],
                            'NumberOfParcel' => $data['number_of_packets'],
                            'BookingMode' => $data['booking_mode'],
                            'codValue' => ($data['booking_mode'] == "COD" ? $data['cod_value'] : ''),
                            'Product_price' => $data['total'],
                        );

                        $response = $this->api->createShipping($shipmentPushingData);
                        if (isset($response['error'])) {
                            if(is_array($response['error']) && count($response['error'])) {
                                foreach($response['error'] as $an_error) $data["eRRORS"][] = $an_error;
                            } else {
                                $data["eRRORS"][] = $response['error'];
                            }
                        } else {    
                            $this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '2', date_modified = NOW() WHERE order_id = '" . (int) $order_id . "'");
                            $is_email = (isset($this->request->post['fastcoo_email_customer']) && $this->request->post['fastcoo_email_customer'] == 'yes') ? 1 : 0;
                            $shipmenthistory = "AWB No. " . $response['awb'] . " [<a href=\"" . $response['awb_print_url'] . "\" target=\"_blank\">" . $response['awb_print_url'] . "</a>]";

                            $message = array(
                                'notify' => $is_email,
                                'comment' => $shipmenthistory
                            );

                            $this->model_extension_shipping_fastcoo_fastcoo->addOrderHistory($order_id, $message);
                            $this->session->data['success'] = $this->language->get('text_success');

                            $this->response->redirect($this->url->link('extension/shipping/fastcoo/create_shipment', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $order_id, 'SSL'));
                        }
                    
                    } catch (Exception $e) {
                        $data['eRRORS'][] = $e->getMessage();
                    }

                }

                if (isset($this->error['sender_name'])) {
                    $data['error_sender_name'] = $this->error['sender_name'];
                } else {
                    $data['error_sender_name'] = '';
                }

                if (isset($this->error['sender_mobile'])) {
                    $data['error_sender_mobile'] = $this->error['sender_mobile'];
                } else {
                    $data['error_sender_mobile'] = '';
                }

                if (isset($this->error['sender_address'])) {
                    $data['error_sender_address'] = $this->error['sender_address'];
                } else {
                    $data['error_sender_address'] = '';
                }

                if (isset($this->error['sender_city'])) {
                    $data['error_sender_city'] = $this->error['sender_city'];
                } else {
                    $data['error_sender_city'] = '';
                }

                if (isset($this->error['receiver_name'])) {
                    $data['error_receiver_name'] = $this->error['receiver_name'];
                } else {
                    $data['error_receiver_name'] = '';
                }

                if (isset($this->error['receiver_email'])) {
                    $data['error_receiver_email'] = $this->error['receiver_email'];
                } else {
                    $data['error_receiver_email'] = '';
                }

                if (isset($this->error['receiver_address'])) {
                    $data['error_receiver_address'] = $this->error['receiver_address'];
                } else {
                    $data['error_receiver_address'] = '';
                }

                if (isset($this->error['receiver_phone'])) {
                    $data['error_receiver_phone'] = $this->error['receiver_phone'];
                } else {
                    $data['error_receiver_phone'] = '';
                }

                if (isset($this->error['receiver_city'])) {
                    $data['error_receiver_city'] = $this->error['receiver_city'];
                } else {
                    $data['error_receiver_city'] = '';
                }

                if (isset($this->error['reference'])) {
                    $data['error_reference'] = $this->error['reference'];
                } else {
                    $data['error_reference'] = '';
                }

                if (isset($this->error['product_type'])) {
                    $data['error_product_type'] = $this->error['product_type'];
                } else {
                    $data['error_product_type'] = '';
                }

                if (isset($this->error['delivery_service'])) {
                    $data['error_delivery_service'] = $this->error['delivery_service'];
                } else {
                    $data['error_delivery_service'] = '';
                }

                if (isset($this->error['shipment_weight'])) {
                    $data['error_shipment_weight'] = $this->error['shipment_weight'];
                } else {
                    $data['error_shipment_weight'] = '';
                }

                if (isset($this->error['shipment_description'])) {
                    $data['error_shipment_description'] = $this->error['shipment_description'];
                } else {
                    $data['error_shipment_description'] = '';
                }

                if (isset($this->error['number_of_packets'])) {
                    $data['error_number_of_packets'] = $this->error['number_of_packets'];
                } else {
                    $data['error_number_of_packets'] = '';
                }

                if (isset($this->error['booking_mode'])) {
                    $data['error_booking_mode'] = $this->error['booking_mode'];
                } else {
                    $data['error_booking_mode'] = '';
                }

                if (isset($this->error['cod_value'])) {
                    $data['error_cod_value'] = $this->error['cod_value'];
                } else {
                    $data['error_cod_value'] = '';
                }                

            }

            if (isset($this->error['warning'])) {
                $data['error_warning'] = $this->error['warning'];
            } else {
                $data['error_warning'] = '';
            }

            if (isset($this->session->data['success'])) {
                $data['success'] = $this->session->data['success'];
    
                unset($this->session->data['success']);
            } else {
                $data['success'] = '';
            }    

            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');

            $this->response->setOutput($this->load->view('extension/shipping/fastcoo/create_shipment', $data));

        } else {
            $this->language->load('error/not_found');

            $data['heading_title'] = $this->language->get('heading_title');

            $data['text_not_found'] = $this->language->get('text_not_found');

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], 'SSL'),
                'separator' => false
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('error/not_found', 'user_token=' . $this->session->data['user_token'], 'SSL'),
                'separator' => ' :: '
            );

            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');

            $this->response->setOutput($this->load->view('error/not_found', $data));
        }
    }

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'extension/shipping/fastcoo/create_shipment')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->request->post['sender_name']) {
            $this->error['sender_name'] = $this->language->get('error_sender_name');
        }

        if (!$this->request->post['sender_mobile']) {
            $this->error['sender_mobile'] = $this->language->get('error_sender_mobile');
        }

        if (!$this->request->post['sender_address']) {
            $this->error['sender_address'] = $this->language->get('error_sender_address');
        }

        if (!$this->request->post['sender_city']) {
            $this->error['sender_city'] = $this->language->get('error_sender_city');
        }
        
        if (!$this->request->post['receiver_name']) {
            $this->error['receiver_name'] = $this->language->get('error_receiver_name');
        }

        if (!$this->request->post['receiver_email']) {
            $this->error['receiver_email'] = $this->language->get('error_receiver_email');
        }

        if (!$this->request->post['receiver_address']) {
            $this->error['receiver_address'] = $this->language->get('error_receiver_address');
        }

        if (!$this->request->post['receiver_phone']) {
            $this->error['receiver_phone'] = $this->language->get('error_receiver_phone');
        }

        if (!$this->request->post['receiver_city']) {
            $this->error['receiver_city'] = $this->language->get('error_receiver_city');
        }

        if (!$this->request->post['product_type']) {
            $this->error['product_type'] = $this->language->get('error_product_type');
        }

        if (!$this->request->post['delivery_service']) {
            $this->error['delivery_service'] = $this->language->get('error_delivery_service');
        }

        if (!$this->request->post['shipment_weight']) {
            $this->error['shipment_weight'] = $this->language->get('error_shipment_weight');
        }

        if (!$this->request->post['shipment_description']) {
            $this->error['shipment_description'] = $this->language->get('error_shipment_description');
        }

        if (!$this->request->post['number_of_packets']) {
            $this->error['number_of_packets'] = $this->language->get('error_number_of_packets');
        }

        if (!$this->request->post['booking_mode']) {
            $this->error['booking_mode'] = $this->language->get('error_booking_mode');
        }

        if ($this->request->post['booking_mode'] == 'COD') {
            if (!$this->request->post['cod_value']) {
                $this->error['cod_value'] = $this->language->get('error_cod_value');
            }    
        }

        if (!$this->request->post['reference']) {
            $this->error['reference'] = $this->language->get('error_reference');
        }

        return !$this->error;
    }
}
