<?php

namespace Fastcoo;

class Api {

	private $email_id;
	private $password;
	
    public function __construct($email_id, $password) {
		$this->email_id = $email_id;
		$this->password = $password;
    }

	private function apiRequest($method, $reqData = []) {
		$arRequest = [];
		foreach ($reqData as $key => $val) {
			$arRequest[] = $key . '=' . urlencode($val);
		}

		$queryParams = implode('&', $arRequest);
		$url = 'https://habex.fastcoo-solutions.com/lm/' . $method . '?' . $queryParams;

		return json_decode(@file_get_contents($url), true);
    }

	public function createShipping($data) {
		$data['sender_email'] = $this->email_id;
		$data['password'] = $this->password;
		$response = $this->apiRequest('shipmentBookingApi_lm.php', $data);
		if (!$response || isset($response['error'])) {
			$error_messages = [];
			if (isset($response['error'])) {
				if(is_numeric($response['error'])) {
					if(count($response) > 1) {
						$error_values = array_values($response);
						$error_messages[] = $error_values[1];	
					}
				} else {
					$error_messages[] = $response['error'];
				}
			}
			return ['error' => count($error_messages) ? $error_messages : null];
		} elseif (isset($response['status']) && is_numeric($response['status'])) {
			if(count($response) > 1) {
				$error_values = array_values($response);
				$error_messages[] = $error_values[1];	
			}
			return ['error' => count($error_messages) ? $error_messages : null];
		}
        return $response;
	}

	public function trackingOrder($data) {
		$data['sender_email'] = $this->email_id;
		$data['password'] = $this->password;
		$response = $this->apiRequest('shipmentBookingApi_lm.php', $data);
		if (!$response || (isset($response['status']) && in_array($response['status'], [136, 138])) ) {
			$error_messages = [];
			if($response) {
				if(isset($response['awb_no'])) {
					$error_messages[] = $response['awb_no'];
				} elseif (isset($response['message'])) {
					$error_messages = $response['message'];
				}
			}
			return ['error' => count($error_messages) ? $error_messages : null];
		}
        return $response;
	}

	public function cancelOrder($data) {
		$data['user_name'] = $this->email_id;
		$data['password'] = $this->password;
		$response = $this->apiRequest('cancelShipmentApi.php', $data);
		if (!$response || (isset($response['code']) && $response['code'] != 1)) {
			return ['error' => isset($response['Message']) ? $response['Message'] : null];
		}
        return $response;
	}

}