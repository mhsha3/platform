<?php

class ControllerExtensionShippingFastcooManage extends Controller {

    private $api;
    
    public function __construct($registry) {
        parent::__construct($registry);
        require_once DIR_SYSTEM . '../admin/controller/extension/shipping/fastcoo/api.php';
        $this->api = new \Fastcoo\Api($this->config->get('shipping_fastcoo_email_id'), $this->config->get('shipping_fastcoo_password'));
    }

    public function cancel() {
        $this->load->model('extension/shipping/fastcoo/fastcoo');
        $this->language->load('extension/shipping/fastcoo/manage');

        $order_id = $this->request->get['order_id'] ?? 0;
        $AWBNumber = $this->model_extension_shipping_fastcoo_fastcoo->getAWBNumber($order_id);

        if($AWBNumber) {
            $response = $this->api->cancelOrder(['awb_no' => $AWBNumber]);
            if(isset($response['error'])) {
                $json = ["error" => $response['error']];
            } else {

                $this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '7', date_modified = NOW() WHERE order_id = '" . (int) $order_id . "'");
                $shipmenthistory = $this->language->get('text_shipment_cancelled');
        
                $message = array(
                    'notify' => 0,
                    'comment' => $shipmenthistory
                );
        
                $this->model_extension_shipping_fastcoo_fastcoo->addOrderHistory($order_id, $message);
                $json = ["response" => 'Success'];
            }                        
        } else {
            $json = ["error" => null];
        }

        $this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
