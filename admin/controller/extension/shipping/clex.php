<?php
class ControllerExtensionShippingClex extends Controller {
	private $error = array();
	public function index() {
		$this->load->language('extension/shipping/clex');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('shipping_clex', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=shipping', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['access_token'])) {
			$data['error_access_token'] = $this->error['access_token'];
		} else {
			$data['error_access_token'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=shipping', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/shipping/clex', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/shipping/clex', 'user_token=' . $this->session->data['user_token'], true);
		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=shipping', true);

		if (isset($this->request->post['shipping_clex_access_token'])) {
			$data['shipping_clex_access_token'] = $this->request->post['shipping_clex_access_token'];
		} else {
			$data['shipping_clex_access_token'] = $this->config->get('shipping_clex_access_token');
		}

		if (isset($this->request->post['shipping_clex_cost'])) {
			$data['shipping_clex_cost'] = $this->request->post['shipping_clex_cost'];
		} else {
			$data['shipping_clex_cost'] = $this->config->get('shipping_clex_cost');
		}
		
		if (isset($this->request->post['shipping_clex_status'])) {
			$data['shipping_clex_status'] = $this->request->post['shipping_clex_status'];
		} else {
			$data['shipping_clex_status'] = $this->config->get('shipping_clex_status');
		}

		if (isset($this->request->post['shipping_clex_sort_order'])) {
			$data['shipping_clex_sort_order'] = $this->request->post['shipping_clex_sort_order'];
		} else {
			$data['shipping_clex_sort_order'] = $this->config->get('shipping_clex_sort_order');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/shipping/clex', $data));
	
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/shipping/clex')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->request->post['shipping_clex_access_token']) {
			$this->error['access_token'] = $this->language->get('error_access_token');
		}
		
		return !$this->error;
	}
}