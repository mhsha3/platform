<?php
class ControllerExtensionShipping525k extends Controller {
	private $error = array();
	public function index() {
		$this->load->language('extension/shipping/525k');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('shipping_525k', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=shipping', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['client_id'])) {
			$data['error_client_id'] = $this->error['client_id'];
		} else {
			$data['error_client_id'] = '';
		}

		if (isset($this->error['client_secret'])) {
			$data['error_client_secret'] = $this->error['client_secret'];
		} else {
			$data['error_client_secret'] = '';
		}

		if (isset($this->error['api_key'])) {
			$data['error_api_key'] = $this->error['api_key'];
		} else {
			$data['error_api_key'] = '';
		}

		if (isset($this->error['shipper_id'])) {
			$data['error_shipper_id'] = $this->error['shipper_id'];
		} else {
			$data['error_shipper_id'] = '';
		}

		if (isset($this->error['origin_phone'])) {
			$data['error_origin_phone'] = $this->error['origin_phone'];
		} else {
			$data['error_origin_phone'] = '';
		}

		if (isset($this->error['origin_email'])) {
			$data['error_origin_email'] = $this->error['origin_email'];
		} else {
			$data['error_origin_email'] = '';
		}

		if (isset($this->error['origin_gps_latitude'])) {
			$data['error_origin_gps_latitude'] = $this->error['origin_gps_latitude'];
		} else {
			$data['error_origin_gps_latitude'] = '';
		}

		if (isset($this->error['origin_gps_longitude'])) {
			$data['error_origin_gps_longitude'] = $this->error['origin_gps_longitude'];
		} else {
			$data['error_origin_gps_longitude'] = '';
		}

		if (isset($this->error['origin_address'])) {
			$data['error_origin_address'] = $this->error['origin_address'];
		} else {
			$data['error_origin_address'] = '';
		}
		
		if (isset($this->error['origin_city'])) {
			$data['error_origin_city'] = $this->error['origin_city'];
		} else {
			$data['error_origin_city'] = '';
		}

		if (isset($this->error['origin_country'])) {
			$data['error_origin_country'] = $this->error['origin_country'];
		} else {
			$data['error_origin_country'] = '';
		}

		if (isset($this->error['pickup_instructions'])) {
			$data['error_pickup_instructions'] = $this->error['pickup_instructions'];
		} else {
			$data['error_pickup_instructions'] = '';
		}

		if (isset($this->error['pickup_arrival_window'])) {
			$data['error_pickup_arrival_window'] = $this->error['pickup_arrival_window'];
		} else {
			$data['error_pickup_arrival_window'] = '';
		}
		
		if (isset($this->error['delivery_arrival_window'])) {
			$data['error_delivery_arrival_window'] = $this->error['delivery_arrival_window'];
		} else {
			$data['error_delivery_arrival_window'] = '';
		}
				
		if (isset($this->error['dimension'])) {
			$data['error_dimension'] = $this->error['dimension'];
		} else {
			$data['error_dimension'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=shipping', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/shipping/525k', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/shipping/525k', 'user_token=' . $this->session->data['user_token'], true);
		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=shipping', true);

		if (isset($this->request->post['shipping_525k_client_id'])) {
			$data['shipping_525k_client_id'] = $this->request->post['shipping_525k_client_id'];
		} else {
			$data['shipping_525k_client_id'] = $this->config->get('shipping_525k_client_id');
		}

		if (isset($this->request->post['shipping_525k_client_secret'])) {
			$data['shipping_525k_client_secret'] = $this->request->post['shipping_525k_client_secret'];
		} else {
			$data['shipping_525k_client_secret'] = $this->config->get('shipping_525k_client_secret');
		}

		if (isset($this->request->post['shipping_525k_api_key'])) {
			$data['shipping_525k_api_key'] = $this->request->post['shipping_525k_api_key'];
		} else {
			$data['shipping_525k_api_key'] = $this->config->get('shipping_525k_api_key');
		}

		if (isset($this->request->post['shipping_525k_shipper_id'])) {
			$data['shipping_525k_shipper_id'] = $this->request->post['shipping_525k_shipper_id'];
		} else {
			$data['shipping_525k_shipper_id'] = $this->config->get('shipping_525k_shipper_id');
		}

		if (isset($this->request->post['shipping_525k_requestedby_id'])) {
			$data['shipping_525k_requestedby_id'] = $this->request->post['shipping_525k_requestedby_id'];
		} else {
			$data['shipping_525k_requestedby_id'] = $this->config->get('shipping_525k_requestedby_id');
		}

		if (isset($this->request->post['shipping_525k_test'])) {
			$data['shipping_525k_test'] = $this->request->post['shipping_525k_test'];
		} else {
			$data['shipping_525k_test'] = $this->config->get('shipping_525k_test');
		}

		if (isset($this->request->post['shipping_525k_origin_name'])) {
			$data['shipping_525k_origin_name'] = $this->request->post['shipping_525k_origin_name'];
		} else {
			$data['shipping_525k_origin_name'] = $this->config->get('shipping_525k_origin_name');
		}

		if (isset($this->request->post['shipping_525k_origin_phone'])) {
			$data['shipping_525k_origin_phone'] = $this->request->post['shipping_525k_origin_phone'];
		} else {
			$data['shipping_525k_origin_phone'] = $this->config->get('shipping_525k_origin_phone');
		}

		if (isset($this->request->post['shipping_525k_origin_email'])) {
			$data['shipping_525k_origin_email'] = $this->request->post['shipping_525k_origin_email'];
		} else {
			$data['shipping_525k_origin_email'] = $this->config->get('shipping_525k_origin_email');
		}

		if (isset($this->request->post['shipping_525k_origin_gps_latitude'])) {
			$data['shipping_525k_origin_gps_latitude'] = $this->request->post['shipping_525k_origin_gps_latitude'];
		} else {
			$data['shipping_525k_origin_gps_latitude'] = $this->config->get('shipping_525k_origin_gps_latitude');
		}

		if (isset($this->request->post['shipping_525k_origin_gps_longitude'])) {
			$data['shipping_525k_origin_gps_longitude'] = $this->request->post['shipping_525k_origin_gps_longitude'];
		} else {
			$data['shipping_525k_origin_gps_longitude'] = $this->config->get('shipping_525k_origin_gps_longitude');
		}

		if (isset($this->request->post['shipping_525k_origin_address'])) {
			$data['shipping_525k_origin_address'] = $this->request->post['shipping_525k_origin_address'];
		} else {
			$data['shipping_525k_origin_address'] = $this->config->get('shipping_525k_origin_address');
		}

		if (isset($this->request->post['shipping_525k_origin_address2'])) {
			$data['shipping_525k_origin_address2'] = $this->request->post['shipping_525k_origin_address2'];
		} else {
			$data['shipping_525k_origin_address2'] = $this->config->get('shipping_525k_origin_address2');
		}

		if (isset($this->request->post['shipping_525k_origin_city'])) {
			$data['shipping_525k_origin_city'] = $this->request->post['shipping_525k_origin_city'];
		} else {
			$data['shipping_525k_origin_city'] = $this->config->get('shipping_525k_origin_city');
		}

		if (isset($this->request->post['shipping_525k_origin_country'])) {
			$data['shipping_525k_origin_country'] = $this->request->post['shipping_525k_origin_country'];
		} else {
			$data['shipping_525k_origin_country'] = $this->config->get('shipping_525k_origin_country');
		}

		if (isset($this->request->post['shipping_525k_pickup_instructions'])) {
			$data['shipping_525k_pickup_instructions'] = $this->request->post['shipping_525k_pickup_instructions'];
		} else {
			$data['shipping_525k_pickup_instructions'] = $this->config->get('shipping_525k_pickup_instructions');
		}

		if (isset($this->request->post['shipping_525k_delivery_instructions'])) {
			$data['shipping_525k_delivery_instructions'] = $this->request->post['shipping_525k_delivery_instructions'];
		} else {
			$data['shipping_525k_delivery_instructions'] = $this->config->get('shipping_525k_delivery_instructions');
		}
		
		if (isset($this->request->post['shipping_525k_pickup_arrival_window_days'])) {
            $data['shipping_525k_pickup_arrival_window_days'] = $this->request->post['shipping_525k_pickup_arrival_window_days'];
        } else {
            $data['shipping_525k_pickup_arrival_window_days'] = $this->config->get('shipping_525k_pickup_arrival_window_days');
		}

		if (isset($this->request->post['shipping_525k_pickup_arrival_window_exclude_begin'])) {
            $data['shipping_525k_pickup_arrival_window_exclude_begin'] = $this->request->post['shipping_525k_pickup_arrival_window_exclude_begin'];
        } else {
            $data['shipping_525k_pickup_arrival_window_exclude_begin'] = $this->config->get('shipping_525k_pickup_arrival_window_exclude_begin');
		}

		if (isset($this->request->post['shipping_525k_pickup_arrival_window_exclude_end'])) {
            $data['shipping_525k_pickup_arrival_window_exclude_end'] = $this->request->post['shipping_525k_pickup_arrival_window_exclude_end'];
        } else {
            $data['shipping_525k_pickup_arrival_window_exclude_end'] = $this->config->get('shipping_525k_pickup_arrival_window_exclude_end');
		}
		
		if (isset($this->request->post['shipping_525k_delivery_arrival_window_days'])) {
            $data['shipping_525k_delivery_arrival_window_days'] = $this->request->post['shipping_525k_delivery_arrival_window_days'];
        } else {
            $data['shipping_525k_delivery_arrival_window_days'] = $this->config->get('shipping_525k_delivery_arrival_window_days');
        }

		if (isset($this->request->post['shipping_525k_delivery_arrival_window_exclude_begin'])) {
            $data['shipping_525k_delivery_arrival_window_exclude_begin'] = $this->request->post['shipping_525k_delivery_arrival_window_exclude_begin'];
        } else {
            $data['shipping_525k_delivery_arrival_window_exclude_begin'] = $this->config->get('shipping_525k_delivery_arrival_window_exclude_begin');
		}

		if (isset($this->request->post['shipping_525k_delivery_arrival_window_exclude_end'])) {
            $data['shipping_525k_delivery_arrival_window_exclude_end'] = $this->request->post['shipping_525k_delivery_arrival_window_exclude_end'];
        } else {
            $data['shipping_525k_delivery_arrival_window_exclude_end'] = $this->config->get('shipping_525k_delivery_arrival_window_exclude_end');
		}

		if (isset($this->request->post['shipping_525k_packaging_type'])) {
			$data['shipping_525k_packaging_type'] = $this->request->post['shipping_525k_packaging_type'];
		} else {
			$data['shipping_525k_packaging_type'] = $this->config->get('shipping_525k_packaging_type');
		}		

		$data['packaging_types'] = array();

		$data['packaging_types'][] = array(
			'value' => 'BOX',
			'text'  => $this->language->get('text_box')
		);

		$data['packaging_types'][] = array(
			'value' => 'PALLET',
			'text'  => $this->language->get('text_pallet')
		);

		$data['packaging_types'][] = array(
			'value' => 'OTHER',
			'text'  => $this->language->get('text_other')
		);		

		if (isset($this->request->post['shipping_525k_stackable'])) {
			$data['shipping_525k_stackable'] = $this->request->post['shipping_525k_stackable'];
		} else {
			$data['shipping_525k_stackable'] = $this->config->get('shipping_525k_stackable');
		}

		if (isset($this->request->post['shipping_525k_length'])) {
			$data['shipping_525k_length'] = $this->request->post['shipping_525k_length'];
		} else {
			$data['shipping_525k_length'] = $this->config->get('shipping_525k_length');
		}

		if (isset($this->request->post['shipping_525k_width'])) {
			$data['shipping_525k_width'] = $this->request->post['shipping_525k_width'];
		} else {
			$data['shipping_525k_width'] = $this->config->get('shipping_525k_width');
		}

		if (isset($this->request->post['shipping_525k_height'])) {
			$data['shipping_525k_height'] = $this->request->post['shipping_525k_height'];
		} else {
			$data['shipping_525k_height'] = $this->config->get('shipping_525k_height');
		}
		
		if (isset($this->request->post['shipping_525k_status'])) {
			$data['shipping_525k_status'] = $this->request->post['shipping_525k_status'];
		} else {
			$data['shipping_525k_status'] = $this->config->get('shipping_525k_status');
		}

		if (isset($this->request->post['shipping_525k_sort_order'])) {
			$data['shipping_525k_sort_order'] = $this->request->post['shipping_525k_sort_order'];
		} else {
			$data['shipping_525k_sort_order'] = $this->config->get('shipping_525k_sort_order');
		}

		if (isset($this->request->post['shipping_525k_debug'])) {
			$data['shipping_525k_debug'] = $this->request->post['shipping_525k_debug'];
		} else {
			$data['shipping_525k_debug'] = $this->config->get('shipping_525k_debug');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/shipping/525k', $data));
	
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/shipping/525k')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->request->post['shipping_525k_client_id']) {
			$this->error['client_id'] = $this->language->get('error_client_id');
		}

		if (!$this->request->post['shipping_525k_client_secret']) {
			$this->error['client_secret'] = $this->language->get('error_client_secret');
		}

		if (!$this->request->post['shipping_525k_api_key']) {
			$this->error['api_key'] = $this->language->get('error_api_key');
		}
		
		if (!$this->request->post['shipping_525k_shipper_id']) {
			$this->error['shipper_id'] = $this->language->get('error_shipper_id');
		}

		if (!$this->request->post['shipping_525k_origin_phone']) {
			$this->error['origin_phone'] = $this->language->get('error_origin_phone');
		}

		if (!$this->request->post['shipping_525k_origin_email']) {
			$this->error['origin_email'] = $this->language->get('error_origin_email');
		}

		if (!$this->request->post['shipping_525k_origin_gps_latitude']) {
			$this->error['origin_gps_latitude'] = $this->language->get('error_origin_gps_latitude');
		}

		if (!$this->request->post['shipping_525k_origin_gps_longitude']) {
			$this->error['origin_gps_longitude'] = $this->language->get('error_origin_gps_longitude');
		}

		if (!$this->request->post['shipping_525k_origin_address']) {
			$this->error['origin_address'] = $this->language->get('error_origin_address');
		}

		if (!$this->request->post['shipping_525k_origin_city']) {
			$this->error['origin_city'] = $this->language->get('error_origin_city');
		}

		if (!$this->request->post['shipping_525k_origin_country']) {
			$this->error['origin_country'] = $this->language->get('error_origin_country');
		}

		if (!$this->request->post['shipping_525k_pickup_instructions']) {
			$this->error['pickup_instructions'] = $this->language->get('error_pickup_instructions');
		}

		if (!$this->request->post['shipping_525k_pickup_arrival_window_days']) {
			$this->error['pickup_arrival_window'] = $this->language->get('error_pickup_arrival_window');
		}

		if (!$this->request->post['shipping_525k_delivery_arrival_window_days']) {
			$this->error['delivery_arrival_window'] = $this->language->get('error_delivery_arrival_window');
		}

		if (!$this->request->post['shipping_525k_length'] || !$this->request->post['shipping_525k_width'] || !$this->request->post['shipping_525k_height']) {
			$this->error['dimension'] = $this->language->get('error_dimension');
		}

		return !$this->error;
	}
	
}