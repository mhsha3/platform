<?php

class ControllerExtensionShippingClexCreateShipment extends Controller {

    private $error = array();
    private $api;

    public function __construct($registry) {
        parent::__construct($registry);
        require_once '/home/lanaapps/public_html/admin/controller/extension/shipping/clex/api.php';
        $this->api = new \Clex\Api($this->config->get('shipping_clex_access_token'));
    }

    public function index() {

      $this->language->load('extension/shipping/clex/create_shipment');
      $this->getForm();

  }

  public function getForm() {

      $this->load->model('sale/order');
      $this->load->model('setting/setting');
      $this->load->model('extension/shipping/clex/clex');

      $order_id = $this->request->get['order_id'] ?? 0;
      $order_info = $this->model_sale_order->getOrder($order_id);

      if ($order_info) {

            $data['user_token'] = $this->session->data['user_token'];

            ##################### Language Labels #####################

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], 'SSL'),
                'separator' => false
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'], 'SSL'),
                'separator' => ' :: '
            );

            $data['order_id'] = $this->request->get['order_id'];
            $data['ordernumber'] = $order_id;

            ##################### Buttons #####################

            $data['back_to_order'] = $this->url->link('sale/order/info', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $order_id, true);
            $data['create_sipment'] = $this->url->link('extension/shipping/clex/create_shipment', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $order_id, 'SSL');

            $is_shipment = $this->model_extension_shipping_clex_clex->checkConsignmentId($order_id);
            $data['is_shipment'] = $is_shipment;

            if($is_shipment) {
                $ConsignmentIdNo = $this->model_extension_shipping_clex_clex->getConsignmentId($order_id);
                $data['consignment_id'] = $ConsignmentIdNo;
                $data['print_label_url'] = $this->api->getPrintLableURL($ConsignmentIdNo);

                $trackStatusResponse = $this->api->trackStatus($ConsignmentIdNo);
                if (isset($trackStatusResponse['code']) && $trackStatusResponse['code'] == 200) {
                    $data['read_shipment'] = $trackStatusResponse['data'][$ConsignmentIdNo];
                }
                
            } else {

                ##################### Consignor details #####################

                $store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);

                if ($store_info) {
                    $store_name = $store_info['config_name'];
                    $store_email = $store_info['config_email'];
                    $store_telephone = $store_info['config_telephone'];
                    $store_address = $store_info['config_address'];
                    $store_zone_id = $store_info['config_zone_id'];
                    $store_country_id = $store_info['config_country_id'];
                } else {
                    $store_name = $this->config->get('config_name');
                    $store_email = $this->config->get('config_email');
                    $store_telephone = $this->config->get('config_telephone');
                    $store_address = $this->config->get('config_address');
                    $store_zone_id = $this->config->get('config_zone_id');
                    $store_country_id = $this->config->get('config_country_id');
                }

                $this->load->model('localisation/zone');
                $zone_info = $this->model_localisation_zone->getZone($store_zone_id);

                if (isset($this->request->post['consignor'])) {
                    $data['consignor'] = $this->request->post['consignor'];
                } else {
                    $data['consignor'] = $store_name;
                }

                if (isset($this->request->post['consignor_email'])) {
                    $data['consignor_email'] = $this->request->post['consignor_email'];
                } else {
                    $data['consignor_email'] = $store_email;
                }                

                if (isset($this->request->post['origin_city'])) {
                    $data['origin_city'] = $this->request->post['origin_city'];
                } else {
                    $data['origin_city'] = $zone_info ? $zone_info["name"] : "";
                }

                if (isset($this->request->post['origin_area_new'])) {
                    $data['origin_area_new'] = $this->request->post['origin_area_new'];
                } else {
                    $data['origin_area_new'] = "";
                }

                if (isset($this->request->post['consignor_street_name'])) {
                    $data['consignor_street_name'] = $this->request->post['consignor_street_name'];
                } else {
                    $data['consignor_street_name'] = $store_address;
                }

                if (isset($this->request->post['consignor_building_name'])) {
                    $data['consignor_building_name'] = $this->request->post['consignor_building_name'];
                } else {
                    $data['consignor_building_name'] = "";
                }

                if (isset($this->request->post['consignor_address_house_appartment'])) {
                    $data['consignor_address_house_appartment'] = $this->request->post['consignor_address_house_appartment'];
                } else {
                    $data['consignor_address_house_appartment'] = "";
                }

                if (isset($this->request->post['consignor_address_landmark'])) {
                    $data['consignor_address_landmark'] = $this->request->post['consignor_address_landmark'];
                } else {
                    $data['consignor_address_landmark'] = "";
                }

                if (isset($this->request->post['consignor_country_code'])) {
                    $data['consignor_country_code'] = $this->request->post['consignor_country_code'];
                } else {
                    $data['consignor_country_code'] = "";
                }

                if (isset($this->request->post['consignor_phone'])) {
                    $data['consignor_phone'] = $this->request->post['consignor_phone'];
                } else {
                    $data['consignor_phone'] = "";
                }

                if (isset($this->request->post['consignor_alternate_country_code'])) {
                    $data['consignor_alternate_country_code'] = $this->request->post['consignor_alternate_country_code'];
                } else {
                    $data['consignor_alternate_country_code'] = "";
                }

                if (isset($this->request->post['consignor_alternate_phone'])) {
                    $data['consignor_alternate_phone'] = $this->request->post['consignor_alternate_phone'];
                } else {
                    $data['consignor_alternate_phone'] = "";
                }

                ##################### Consignee details #####################

                $shipment_receiver_name = "";
                if (isset($order_info['shipping_firstname']) && !empty($order_info['shipping_firstname'])) {
                    $shipment_receiver_name .= $order_info['shipping_firstname'];
                }
                if (isset($order_info['shipping_lastname']) && !empty($order_info['shipping_lastname'])) {
                    $shipment_receiver_name .= " " . $order_info['shipping_lastname'];
                }

                if (isset($this->request->post['consignee'])) {
                    $data['consignee'] = $this->request->post['consignee'];
                } else {
                    $data['consignee'] = $shipment_receiver_name;
                }

                if (isset($this->request->post['consignee_email'])) {
                    $data['consignee_email'] = $this->request->post['consignee_email'];
                } else {
                    $data['consignee_email'] = ($order_info['email']) ? $order_info['email'] : '';
                }                

                if (isset($this->request->post['destination_city'])) {
                    $data['destination_city'] = $this->request->post['destination_city'];
                } else {
                    $data['destination_city'] = ($order_info['shipping_city']) ? $order_info['shipping_city'] : "";
                }

                if (isset($this->request->post['destination_area_new'])) {
                    $data['destination_area_new'] = $this->request->post['destination_area_new'];
                } else {
                    $data['destination_area_new'] = "";
                }

                if (isset($this->request->post['consignee_street_name'])) {
                    $data['consignee_street_name'] = $this->request->post['consignee_street_name'];
                } else {
                    $data['consignee_street_name'] = ($order_info['shipping_address_1']) ? $order_info['shipping_address_1'] : '';
                }

                if (isset($this->request->post['consignee_building_name'])) {
                    $data['consignee_building_name'] = $this->request->post['consignee_building_name'];
                } else {
                    $data['consignee_building_name'] = "";
                }

                if (isset($this->request->post['consignee_address_house_appartment'])) {
                    $data['consignee_address_house_appartment'] = $this->request->post['consignee_address_house_appartment'];
                } else {
                    $data['consignee_address_house_appartment'] = "";
                }

                if (isset($this->request->post['consignee_address_landmark'])) {
                    $data['consignee_address_landmark'] = $this->request->post['consignee_address_landmark'];
                } else {
                    $data['consignee_address_landmark'] = "";
                }

                if (isset($this->request->post['consignee_country_code'])) {
                    $data['consignee_country_code'] = $this->request->post['consignee_country_code'];
                } else {
                    $data['consignee_country_code'] = "";
                }

                if (isset($this->request->post['consignee_phone'])) {
                    $data['consignee_phone'] = $this->request->post['consignee_phone'];
                } else {
                    $data['consignee_phone'] = "";
                }

                if (isset($this->request->post['consignee_alternate_country_code'])) {
                    $data['consignee_alternate_country_code'] = $this->request->post['consignee_alternate_country_code'];
                } else {
                    $data['consignee_alternate_country_code'] = "";
                }

                if (isset($this->request->post['consignee_alternate_phone'])) {
                    $data['consignee_alternate_phone'] = $this->request->post['consignee_alternate_phone'];
                } else {
                    $data['consignee_alternate_phone'] = "";
                }

                ##################### Shipment details #####################

                if (isset($this->request->post['shipment_reference_number'])) {
                    $data['shipment_reference_number'] = $this->request->post['shipment_reference_number'];
                } else {
                    $data['shipment_reference_number'] = $order_id;
                }

                if (isset($this->request->post['shipment_type'])) {
                    $data['shipment_type'] = $this->request->post['shipment_type'];
                } else {
                    $data['shipment_type'] = "";
                }

                if (isset($this->request->post['billing_type'])) {
                    $data['billing_type'] = $this->request->post['billing_type'];
                } else {
                    $data['billing_type'] = "";
                }

                if (isset($this->request->post['collect_amount'])) {
                    $data['collect_amount'] = $this->request->post['collect_amount'];
                } else {
                    $data['collect_amount'] = "";
                }
                
                if (isset($this->request->post['primary_service'])) {
                    $data['primary_service'] = $this->request->post['primary_service'];
                } else {
                    $data['primary_service'] = "";
                }

                if (isset($this->request->post['secondary_service'])) {
                    $data['secondary_service'] = $this->request->post['secondary_service'];
                } else {
                    $data['secondary_service'] = "";
                }

                if (isset($this->request->post['commodity_description'])) {
                    $data['commodity_description'] = $this->request->post['commodity_description'];
                } else {
                    $data['commodity_description'] = "";
                }

                if (isset($this->request->post['shipment_piece'])) {
                    $shipment_pieces = $this->request->post['shipment_piece'];
               } else {
                    $shipment_pieces = array();
                }
        
                $data['shipment_pieces'] = array();        
                foreach ($shipment_pieces as $shipment_piece) {
                    $data['shipment_pieces'][] = array(
                        'weight_actual'          => $shipment_piece['weight_actual'],
                        'volumetric_width'          => $shipment_piece['volumetric_width'],
                        'volumetric_height'          => $shipment_piece['volumetric_height'],
                        'volumetric_depth'          => $shipment_piece['volumetric_depth']
                    );
                }
        
                // Products details ..
                $order_products = $this->model_sale_order->getOrderProducts($order_id);
                $weighttot = 0;
                foreach ($order_products as $order_product) {
                    if (isset($order_product['order_option'])) {
                        $order_option = $order_product['order_option'];
                    } elseif (isset($this->request->get['order_id'])) {
                        $order_option = $this->model_sale_order->getOrderOptions($this->request->get['order_id'], $order_product['order_product_id']);
                        $product_weight_query = $this->db->query("SELECT weight, weight_class_id FROM " . DB_PREFIX . "product WHERE product_id = '" . $order_product['product_id'] . "'");
                        $weight_class_query = $this->db->query("SELECT wcd.unit FROM " . DB_PREFIX . "weight_class wc LEFT JOIN " . DB_PREFIX . "weight_class_description wcd ON (wc.weight_class_id = wcd.weight_class_id) WHERE wcd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND wc.weight_class_id = '" . $product_weight_query->row['weight_class_id'] . "'");
                    } else {
                        $order_option = array();
                    }

                    $config_weight_class_id = $this->config->get('config_weight_class_id');
                    $prodweight = $this->weight->convert($product_weight_query->row['weight'], $product_weight_query->row['weight_class_id'], $config_weight_class_id);
                    $prodweight = ($prodweight * $order_product['quantity']);
                    $weighttot = ($weighttot + $prodweight);
                
                    $order_products_data[] = array(
                        'comment' => $order_product['name'],
                        'quantity' => $order_product['quantity']
                    );
                }

                $data['weight'] = number_format($weighttot, 2);
                $data['total'] = number_format($order_info['total'], 2);
                $data['total'] = $this->currency->format(str_replace(',', '', $data['total']), $order_info['currency_code'], $order_info['currency_value'], false);
                $data['order_products'] = $order_products_data;

                // Predefined data lists ..
                $data['shipment_types'] = array(
                    ['value' => 'delivery', 'text'  => $this->language->get('text_shipment_type_delivery')],
                    ['value' => 'self pickup', 'text'  => $this->language->get('text_shipment_type_self_pickup')]
                );
                $data['billing_types'] = array(
                    ['value' => 'COD', 'text'  => $this->language->get('text_billing_type_cod')],
                    ['value' => 'PRE PAID', 'text'  => $this->language->get('text_billing_type_prepaid')]
                );
                $data['primary_services'] = array(
                    ['value' => 'delivery', 'text'  => $this->language->get('text_primary_service_delivery')]
                );
                $data['secondary_services'] = array(
                    ['value' => 'Insurance', 'text'  => $this->language->get('text_secondary_service_insurance')],
                    ['value' => 'COD', 'text'  => $this->language->get('text_secondary_service_cod')],
                );

                ################## Create Shipment ##################

                if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

                    try {
                
                        $shipmentData = [
                            "shipment_reference_number" =>  $data['shipment_reference_number'],
                            "shipment_type" =>  $data['shipment_type'],
                            "billing_type" =>  $data['billing_type'],
                            "collect_amount" =>  $data['collect_amount'],
                            "primary_service" =>  $data['primary_service'],
                            "secondary_service" =>  implode("-", $data['secondary_service']),
                            "item_value" => (in_array("Insurance", $data['secondary_service']) ? $data['total'] : null),
                            "consignor" =>  $data['consignor'],
                            "consignor_email" =>  $data['consignor_email'],
                            "origin_city" =>  $data['origin_city'],
                            "origin_area_new" =>  $data['origin_area_new'],
                            "consignor_street_name" =>  $data['consignor_street_name'],
                            "consignor_building_name" =>  $data['consignor_building_name'],
                            "consignor_address_house_appartment" =>  $data['consignor_address_house_appartment'],
                            "consignor_address_landmark" =>  $data['consignor_address_landmark'],
                            "consignor_country_code" =>  $data['consignor_country_code'],
                            "consignor_phone" =>  $data['consignor_phone'],
                            "consignor_alternate_country_code" =>  $data['consignor_alternate_country_code'],
                            "consignor_alternate_phone" =>  $data['consignor_alternate_phone'],
                            "consignee" =>  $data['consignee'],
                            "consignee_email" =>  $data['consignee_email'],
                            "destination_city" =>  $data['destination_city'],
                            "destination_area_new" =>  $data['destination_area_new'],
                            "consignee_street_name" =>  $data['consignee_street_name'],
                            "consignee_building_name" =>  $data['consignee_building_name'],
                            "consignee_address_house_appartment" =>  $data['consignee_address_house_appartment'],
                            "consignee_address_landmark" =>  $data['consignee_address_landmark'],
                            "consignee_country_code" =>  $data['consignee_country_code'],
                            "consignee_phone" =>  $data['consignee_phone'],
                            "consignee_alternate_country_code" =>  $data['consignee_alternate_country_code'],
                            "consignee_alternate_phone" =>  $data['consignee_alternate_phone'],
                            "pieces_count" =>  count($data['shipment_pieces']),
                            "order_date" =>  date("d-m-Y", strtotime($order_info['date_added'])),
                            "commodity_description" =>  $data['commodity_description'],
                            "pieces" => $data['shipment_pieces']                            
                        ];

                        $response = $this->api->addShipment($shipmentData);
                        if (isset($response['code']) && $response['code'] == 200) {
                            $output = $response['data'][0]; // get first record
                            
                            $this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '2', date_modified = NOW() WHERE order_id = '" . (int) $order_id . "'");
                            $is_email = (isset($this->request->post['clex_email_customer']) && $this->request->post['clex_email_customer'] == 'yes') ? 1 : 0;
                            $shipmenthistory = "[ConsignmentId: " . $output['cn_id'] . "]";

                            $message = array(
                                'notify' => $is_email,
                                'comment' => $shipmenthistory
                            );

                            $this->model_extension_shipping_clex_clex->addOrderHistory($order_id, $message);
                            $this->session->data['success'] = $this->language->get('text_success');

                            $this->response->redirect($this->url->link('extension/shipping/clex/create_shipment', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $order_id, 'SSL'));

                        } else if (isset($response['error_message'])) {
                            if(is_array($response['error_message'])) {
                                foreach($response['error_message'] as $error_message) {
                                    $data["eRRORS"][] = $error_message[0];
                                }
                            } else {
                                $data["eRRORS"][] = $response['error_message'];
                            }
                        }
                    } catch (Exception $e) {
                        $data['eRRORS'][] = $e->getMessage();
                    }

                }

                if (isset($this->error['consignor'])) {
                    $data['error_consignor'] = $this->error['consignor'];
                } else {
                    $data['error_consignor'] = '';
                }

                if (isset($this->error['consignor_email'])) {
                    $data['error_consignor_email'] = $this->error['consignor_email'];
                } else {
                    $data['error_consignor_email'] = '';
                }

                if (isset($this->error['origin_city'])) {
                    $data['error_origin_city'] = $this->error['origin_city'];
                } else {
                    $data['error_origin_city'] = '';
                }

                if (isset($this->error['consignor_country_code'])) {
                    $data['error_consignor_country_code'] = $this->error['consignor_country_code'];
                } else {
                    $data['error_consignor_country_code'] = '';
                }

                if (isset($this->error['consignor_phone'])) {
                    $data['error_consignor_phone'] = $this->error['consignor_phone'];
                } else {
                    $data['error_consignor_phone'] = '';
                }

                if (isset($this->error['consignee'])) {
                    $data['error_consignee'] = $this->error['consignee'];
                } else {
                    $data['error_consignee'] = '';
                }

                if (isset($this->error['consignee_email'])) {
                    $data['error_consignee_email'] = $this->error['consignee_email'];
                } else {
                    $data['error_consignee_email'] = '';
                }

                if (isset($this->error['destination_city'])) {
                    $data['error_destination_city'] = $this->error['destination_city'];
                } else {
                    $data['error_destination_city'] = '';
                }

                if (isset($this->error['consignee_country_code'])) {
                    $data['error_consignee_country_code'] = $this->error['consignee_country_code'];
                } else {
                    $data['error_consignee_country_code'] = '';
                }

                if (isset($this->error['consignee_phone'])) {
                    $data['error_consignee_phone'] = $this->error['consignee_phone'];
                } else {
                    $data['error_consignee_phone'] = '';
                }

                if (isset($this->error['shipment_reference_number'])) {
                    $data['error_shipment_reference_number'] = $this->error['shipment_reference_number'];
                } else {
                    $data['error_shipment_reference_number'] = '';
                }

                if (isset($this->error['shipment_type'])) {
                    $data['error_shipment_type'] = $this->error['shipment_type'];
                } else {
                    $data['error_shipment_type'] = '';
                }

                if (isset($this->error['billing_type'])) {
                    $data['error_billing_type'] = $this->error['billing_type'];
                } else {
                    $data['error_billing_type'] = '';
                }

                if (isset($this->error['collect_amount'])) {
                    $data['error_collect_amount'] = $this->error['collect_amount'];
                } else {
                    $data['error_collect_amount'] = '';
                }

                if (isset($this->error['primary_service'])) {
                    $data['error_primary_service'] = $this->error['primary_service'];
                } else {
                    $data['error_primary_service'] = '';
                }

                if (isset($this->error['secondary_service'])) {
                    $data['error_secondary_service'] = $this->error['secondary_service'];
                } else {
                    $data['error_secondary_service'] = '';
                }

                if (isset($this->error['commodity_description'])) {
                    $data['error_commodity_description'] = $this->error['commodity_description'];
                } else {
                    $data['error_commodity_description'] = '';
                }

                if (isset($this->error['shipment_piece'])) {
                    $data['error_shipment_piece'] = $this->error['shipment_piece'];
                } else {
                    $data['error_shipment_piece'] = '';
                }

            }

            if (isset($this->error['warning'])) {
                $data['error_warning'] = $this->error['warning'];
            } else {
                $data['error_warning'] = '';
            }

            if (isset($this->session->data['success'])) {
                $data['success'] = $this->session->data['success'];
    
                unset($this->session->data['success']);
            } else {
                $data['success'] = '';
            }    

            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');

            $this->response->setOutput($this->load->view('extension/shipping/clex/create_shipment', $data));

        } else {
            $this->language->load('error/not_found');

            $data['heading_title'] = $this->language->get('heading_title');

            $data['text_not_found'] = $this->language->get('text_not_found');

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], 'SSL'),
                'separator' => false
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('error/not_found', 'user_token=' . $this->session->data['user_token'], 'SSL'),
                'separator' => ' :: '
            );

            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');

            $this->response->setOutput($this->load->view('error/not_found', $data));
        }
    }

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'extension/shipping/clex/create_shipment')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->request->post['consignor']) {
            $this->error['consignor'] = $this->language->get('error_consignor');
        }

        if (!$this->request->post['consignor_email']) {
            $this->error['consignor_email'] = $this->language->get('error_consignor_email');
        }

        if (!$this->request->post['origin_city']) {
            $this->error['origin_city'] = $this->language->get('error_origin_city');
        }

        if (!$this->request->post['consignor_country_code']) {
            $this->error['consignor_country_code'] = $this->language->get('error_consignor_country_code');
        }

        if (!$this->request->post['consignor_phone']) {
            $this->error['consignor_phone'] = $this->language->get('error_consignor_phone');
        }
        
        if (!$this->request->post['consignee']) {
            $this->error['consignee'] = $this->language->get('error_consignee');
        }

        if (!$this->request->post['consignee_email']) {
            $this->error['consignee_email'] = $this->language->get('error_consignee_email');
        }

        if (!$this->request->post['destination_city']) {
            $this->error['destination_city'] = $this->language->get('error_destination_city');
        }

        if (!$this->request->post['consignee_country_code']) {
            $this->error['consignee_country_code'] = $this->language->get('error_consignee_country_code');
        }

        if (!$this->request->post['consignee_phone']) {
            $this->error['consignee_phone'] = $this->language->get('error_consignee_phone');
        }

        if (!$this->request->post['shipment_reference_number']) {
            $this->error['shipment_reference_number'] = $this->language->get('error_shipment_reference_number');
        }

        if (!$this->request->post['shipment_type']) {
            $this->error['shipment_type'] = $this->language->get('error_shipment_type');
        }

        if (!$this->request->post['billing_type']) {
            $this->error['billing_type'] = $this->language->get('error_billing_type');
        } else if ($this->request->post['billing_type'] == 'COD' && !$this->request->post['collect_amount']) {
            $this->error['collect_amount'] = $this->language->get('error_collect_amount');
        }

        if (!$this->request->post['primary_service']) {
            $this->error['primary_service'] = $this->language->get('error_primary_service');
        }

        if (!isset($this->request->post['secondary_service']) || !$this->request->post['secondary_service']) {
            $this->error['secondary_service'] = $this->language->get('error_secondary_service');
        }

        if (!$this->request->post['commodity_description']) {
            $this->error['commodity_description'] = $this->language->get('error_commodity_description');
        }
        if (empty($this->request->post['shipment_piece'])) {
            $this->error['shipment_piece'] = $this->language->get('error_shipment_piece');
        } else {
            foreach ($this->request->post['shipment_piece'] as $shipment_piece) {
                if(empty($shipment_piece['weight_actual']) || empty($shipment_piece['volumetric_width']) || 
                   empty($shipment_piece['volumetric_height']) || empty($shipment_piece['volumetric_depth'])) {
                        $this->error['shipment_piece'] = $this->language->get('error_shipment_piece_invalid');
                        break;
                }
            }
        }

        return !$this->error;
    }
}
