<?php

namespace Clex;

class Api {

	private $access_token;

    public function __construct($access_token) {
		$this->access_token = $access_token;
    }

	protected function clexCall($method, $data) {

		$url = 'https://api.clexsa.com/consignment/'.$method;
		$request = json_encode($data, JSON_UNESCAPED_UNICODE);

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Access-token: '.$this->access_token, 'Content-Type: application/json; charset=utf-8'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		// curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $request);

		$response = json_decode(curl_exec($ch), true);
		curl_close ($ch);

		if (!is_array($response) || !isset($response['error'])) {
			if(is_array($response)) {
				return ['error_message' => $response];
			} else {
				return ['error_message' => 'Server Error'];
			}
		} else if (isset($response['error']) && $response['error']) {
            return ['error_message' => $response['message']];
        }

		return $response;
	}
	
	public function addShipment($data) {
		return $this->clexCall('add', $data);
	}
	
	public function trackStatus($shipmentId) {
		return $this->clexCall('track-status', ['shipment_id' => $shipmentId]);
	}

	public function printLabel($shipmentId) {
		return $this->clexCall('print-label', ['shipment_id' => $shipmentId]);
	}

	public function getPrintLableURL($shipmentId) {
		return "https://cockpit.clexsa.com/pdf/download/" . $shipmentId . "?id=" . $shipmentId . "&type=A4-consignment_clex_label&return_type=pdf";
	}

}