<?php

class ControllerExtensionShippingFastloManage extends Controller {

    private $api;

    public function __construct($registry) {
        parent::__construct($registry);
        require_once __DIR__ . '/api.php';
        $this->api = new \Fastlo\Api($this->config->get('shipping_fastlo_api_key'), $this->config->get('shipping_fastlo_client_secret'), $this->config->get('shipping_fastlo_api_key'), $this->config->get('shipping_fastlo_test'));
    }

    public function print_labels() {
        $trackNumber = $this->request->get['trackNumber'] ?? false;
        if($trackNumber) {
            $pdfFormat = $this->request->get['pdfFormat'] ?? 'base64';
            $labelSize = $this->request->get['labelSize'] ?? '4in_2in';
            $optionalBarcode = $this->request->get['optionalBarcode'] ?? 'none';

            $response = $this->api->fastloGetShipmentLabel($trackNumber, $pdfFormat, $labelSize, $optionalBarcode);
            if ($response['status_code'] == 200) {
                $output = $response['output'];

                header('Content-type: application/pdf');
                echo base64_decode($output['shipment_label']);

                exit();
            }
        }
    }

    public function cancel() {
        $this->load->model('extension/shipping/fastlo/fastlo');
        $this->language->load('extension/shipping/fastlo/manage');

        $order_id = $this->request->get['order_id'] ?? 0;
        $track_number = $this->model_extension_shipping_fastlo_fastlo->getTracknumber($order_id);

        if($track_number) {
            $response = $this->api->fastloCancelShipment($track_number);

            if ($response['status_code'] == 200) {
                $output = $response['output'];

                if($output['canceled_status'] == 1) {
                    $this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '7', date_modified = NOW() WHERE order_id = '" . (int) $order_id . "'");
                    $shipmenthistory = $this->language->get('text_shipment_cancelled');

                    $message = array(
                        'notify' => 0,
                        'comment' => $shipmenthistory
                    );

                    $this->model_extension_shipping_fastlo_fastlo->addOrderHistory($order_id, $message);
                    $json = ["response" => 'Success'];

                }
            } else {
                $json = ["error" => $response['error']];
            }

        } else {
            $json = ["error" => null];
        }

        $this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
