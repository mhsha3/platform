<?php

class ControllerExtensionShippingFastloCreateShipment extends Controller {

    private $error = array();
    private $api;

    public function __construct($registry) {
        parent::__construct($registry);
        require_once __DIR__ . '/api.php';
        $this->api = new \Fastlo\Api($this->config->get('shipping_fastlo_api_key'), $this->config->get('shipping_fastlo_client_secret'), $this->config->get('shipping_fastlo_api_key'), $this->config->get('shipping_fastlo_test'));
    }

    public function index() {

      $this->language->load('extension/shipping/fastlo/create_shipment');
      $this->getForm();

  }

  public function getForm() {

      $this->load->model('sale/order');
      $this->load->model('setting/setting');
      $this->load->model('localisation/country');
      $this->load->model('extension/shipping/fastlo/fastlo');

      $order_id = $this->request->get['order_id'] ?? 0;
      $order_info = $this->model_sale_order->getOrder($order_id);

      if ($order_info) {

            $data['user_token'] = $this->session->data['user_token'];

            ##################### Language Labels #####################

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], 'SSL'),
                'separator' => false
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'], 'SSL'),
                'separator' => ' :: '
            );

            $data['order_id'] = $this->request->get['order_id'];
            $data['ordernumber'] = $order_id;

            ##################### Buttons #####################

            $data['back_to_order'] = $this->url->link('sale/order/info', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $order_id, true);
            $data['create_sipment'] = $this->url->link('extension/shipping/fastlo/create_shipment', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $order_id, 'SSL');

            $is_shipment = $this->model_extension_shipping_fastlo_fastlo->checkWritecode($order_id);
            $data['is_shipment'] = $is_shipment;

            if($is_shipment) {
                $trackNumber = $this->model_extension_shipping_fastlo_fastlo->getTracknumber($order_id);
                $data['print_label_url'] = $this->url->link('extension/shipping/fastlo/manage/print_labels', 'user_token=' . $this->session->data['user_token'] . '&trackNumber=' . $trackNumber, 'SSL');

                $canBeCanceledResponse = $this->api->fastloCanBeCanceled($trackNumber);
                if ($canBeCanceledResponse['status_code'] == 200) {
                    $canBeCanceledOutput = $canBeCanceledResponse['output'];
                    $data['can_be_canceled'] = $canBeCanceledOutput['can_be_canceled'];
                }

                $readShipmentResponse = $this->api->fastloReadShipment($trackNumber);
                if ($readShipmentResponse['status_code'] == 200) {
                    $data['read_shipment'] = $readShipmentResponse['output'];
                }

            } else {

                ##################### Sender details #####################

                $store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);

                if ($store_info) {
                    $store_name = $store_info['config_name'];
                    $store_email = $store_info['config_email'];
                    $store_telephone = $store_info['config_telephone'];
                    $store_address = $store_info['config_address'];
                    $store_zone_id = $store_info['config_zone_id'];
                    $store_country_id = $store_info['config_country_id'];
                } else {
                    $store_name = $this->config->get('config_name');
                    $store_email = $this->config->get('config_email');
                    $store_telephone = $this->config->get('config_telephone');
                    $store_address = $this->config->get('config_address');
                    $store_zone_id = $this->config->get('config_zone_id');
                    $store_country_id = $this->config->get('config_country_id');
                }

                $this->load->model('localisation/country');
                $country_info = $this->model_localisation_country->getCountry($this->config->get('config_country_id'));

                $this->load->model('localisation/zone');
                $zone_info = $this->model_localisation_zone->getZone($store_zone_id);

                if (isset($this->request->post['sender_name'])) {
                    $data['sender_name'] = $this->request->post['sender_name'];
                } else {
                    $data['sender_name'] = $store_name;
                }

                if (isset($this->request->post['sender_mobile1'])) {
                    $data['sender_mobile1'] = $this->request->post['sender_mobile1'];
                } else {
                    $data['sender_mobile1'] = $store_telephone;
                }

                if (isset($this->request->post['sender_mobile2'])) {
                    $data['sender_mobile2'] = $this->request->post['sender_mobile2'];
                } else {
                    $data['sender_mobile2'] = "";
                }

                if (isset($this->request->post['sender_country'])) {
                    $data['sender_country'] = $this->request->post['sender_country'];
                } else {
                    $data['sender_country'] = $country_info ? $country_info["iso_code_2"] : "";
                }

                if (isset($this->request->post['sender_city'])) {
                    $data['sender_city'] = $this->request->post['sender_city'];
                } else {
                    $data['sender_city'] = $zone_info ? $zone_info["name"] : "";
                }

                if (isset($this->request->post['sender_area'])) {
                    $data['sender_area'] = $this->request->post['sender_area'];
                } else {
                    $data['sender_area'] = "";
                }

                if (isset($this->request->post['sender_street'])) {
                    $data['sender_street'] = $this->request->post['sender_street'];
                } else {
                    $data['sender_street'] = $store_address;
                }
                    
                if (isset($this->request->post['sender_additional'])) {
                    $data['sender_additional'] = $this->request->post['sender_additional'];
                } else {
                    $data['sender_additional'] = "";
                }

                if (isset($this->request->post['sender_latitude'])) {
                    $data['sender_latitude'] = $this->request->post['sender_latitude'];
                } else {
                    $data['sender_latitude'] = "";
                }

                if (isset($this->request->post['sender_longitude'])) {
                    $data['sender_longitude'] = $this->request->post['sender_longitude'];
                } else {
                    $data['sender_longitude'] = "";
                }

                ##################### Receiver details #####################

                $shipment_receiver_name = "";
                if (isset($order_info['shipping_firstname']) && !empty($order_info['shipping_firstname'])) {
                    $shipment_receiver_name .= $order_info['shipping_firstname'];
                }
                if (isset($order_info['shipping_lastname']) && !empty($order_info['shipping_lastname'])) {
                    $shipment_receiver_name .= " " . $order_info['shipping_lastname'];
                }

                if (isset($this->request->post['receiver_name'])) {
                    $data['receiver_name'] = $this->request->post['receiver_name'];
                } else {
                    $data['receiver_name'] = trim($shipment_receiver_name);
                }

                if (isset($this->request->post['receiver_mobile1'])) {
                    $data['receiver_mobile1'] = $this->request->post['receiver_mobile1'];
                } else {
                    $data['receiver_mobile1'] = ($order_info['telephone']) ? $order_info['telephone'] : "";
                }

                if (isset($this->request->post['receiver_mobile2'])) {
                    $data['receiver_mobile2'] = $this->request->post['receiver_mobile2'];
                } else {
                    $data['receiver_mobile2'] = "";
                }

                $payment_country = $this->model_localisation_country->getCountry($order_info['payment_country_id']);
                if (isset($this->request->post['receiver_country'])) {
                    $data['receiver_country'] = $this->request->post['receiver_country'];
                } else {
                    $data['receiver_country'] = ($payment_country['iso_code_2']) ? $payment_country['iso_code_2'] : "";
                }

                if (isset($this->request->post['receiver_city'])) {
                    $data['receiver_city'] = $this->request->post['receiver_city'];
                } else {
                    $data['receiver_city'] = ($order_info['shipping_city']) ? $order_info['shipping_city'] : "";
                }

                if (isset($this->request->post['receiver_area'])) {
                    $data['receiver_area'] = $this->request->post['receiver_area'];
                } else {
                    $data['receiver_area'] = "";
                }

                if (isset($this->request->post['receiver_street'])) {
                    $data['receiver_street'] = $this->request->post['receiver_street'];
                } else {
                    $data['receiver_street'] = ($order_info['shipping_address_1']) ? $order_info['shipping_address_1'] : '';
                }

                if (isset($this->request->post['receiver_additional'])) {
                    $data['receiver_additional'] = $this->request->post['receiver_additional'];
                } else {
                    $data['receiver_additional'] = "";
                }

                if (isset($this->request->post['receiver_latitude'])) {
                    $data['receiver_latitude'] = $this->request->post['receiver_latitude'];
                } else {
                    $data['receiver_latitude'] = "";
                }

                if (isset($this->request->post['receiver_longitude'])) {
                    $data['receiver_longitude'] = $this->request->post['receiver_longitude'];
                } else {
                    $data['receiver_longitude'] = "";
                }

                ##################### Shipment details #####################

                if (isset($this->request->post['reference'])) {
                    $data['reference'] = $this->request->post['reference'];
                } else {
                    $data['reference'] = $order_id;
                }

                if (isset($this->request->post['collect_cash_amount'])) {
                    $data['collect_cash_amount'] = $this->request->post['collect_cash_amount'];
                } else {
                    $data['collect_cash_amount'] = "";
                }

                if (isset($this->request->post['number_of_pieces'])) {
                    $data['number_of_pieces'] = $this->request->post['number_of_pieces'];
                } else {
                    $data['number_of_pieces'] = "";
                }

                $order_products = $this->model_sale_order->getOrderProducts($order_id);
                $weighttot = 0;
                foreach ($order_products as $order_product) {
                    if (isset($order_product['order_option'])) {
                        $order_option = $order_product['order_option'];
                    } elseif (isset($this->request->get['order_id'])) {
                        $order_option = $this->model_sale_order->getOrderOptions($this->request->get['order_id'], $order_product['order_product_id']);
                        $product_weight_query = $this->db->query("SELECT weight, weight_class_id FROM " . DB_PREFIX . "product WHERE product_id = '" . $order_product['product_id'] . "'");
                        $weight_class_query = $this->db->query("SELECT wcd.unit FROM " . DB_PREFIX . "weight_class wc LEFT JOIN " . DB_PREFIX . "weight_class_description wcd ON (wc.weight_class_id = wcd.weight_class_id) WHERE wcd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND wc.weight_class_id = '" . $product_weight_query->row['weight_class_id'] . "'");
                    } else {
                        $order_option = array();
                    }

                    $config_weight_class_id = $this->config->get('config_weight_class_id');
                    $prodweight = $this->weight->convert($product_weight_query->row['weight'], $product_weight_query->row['weight_class_id'], $config_weight_class_id);
                    $prodweight = ($prodweight * $order_product['quantity']);
                    $weighttot = ($weighttot + $prodweight);
                
                    $order_products_data[] = array(
                        'comment' => $order_product['name'],
                        'quantity' => $order_product['quantity']
                    );
                }

                $data['weight'] = number_format($weighttot, 2);
                $data['total'] = number_format($order_info['total'], 2);
                $data['total'] = $this->currency->format(str_replace(',', '', $data['total']), $order_info['currency_code'], $order_info['currency_value'], false);
                $data['order_products'] = $order_products_data;

                if (isset($this->request->post['currency_code'])) {
                    $data['currency_code'] = $this->request->post['currency_code'];
                } else {
                    $data['currency_code'] = ($order_info['currency_code']) ? $order_info['currency_code'] : '';
                }

                $this->load->model('localisation/country');
                $data['countries'] = $this->model_localisation_country->getCountries();
        
                ################## Create Shipment ##################

                if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

                    try {

                        $senderAddress = array(
                            'use_default_address' => 0,
                            'sender_name' => $data['sender_name'],
                            'sender_mobile1' => $data['sender_mobile1'],
                            'sender_mobile2' => $data['sender_mobile2'],
                            'sender_country' => $data['sender_country'],
                            'sender_city' => $data['sender_city'],
                            'sender_area' => $data['sender_area'],
                            'sender_street' => $data['sender_street'],
                            'sender_additional' => $data['sender_additional'],
                            'sender_latitude' => $data['sender_latitude'],
                            'sender_longitude' => $data['sender_longitude']
                        );
                        
                        $receiverAddress = array(
                            'receiver_name' => $data['receiver_name'],
                            'receiver_mobile1' => $data['receiver_mobile1'],
                            'receiver_mobile2' => $data['receiver_mobile2'],
                            'receiver_country' => $data['receiver_country'],
                            'receiver_city' => $data['receiver_city'],
                            'receiver_area' => $data['receiver_area'],
                            'receiver_street' => $data['receiver_street'],
                            'receiver_additional' => $data['receiver_additional'],
                            'receiver_latitude' => $data['receiver_latitude'],
                            'receiver_longitude' => $data['receiver_longitude']
                        );
                        
                        $shipmentData = array(
                            'collect_cash_amount' => $data['collect_cash_amount'],
                            'number_of_pieces' => $data['number_of_pieces'],
                            'reference' => $data['reference']
                        );

                        $response = $this->api->fastloAddShipment($senderAddress, $receiverAddress, $shipmentData);
                        if ($response['status_code'] == 200) {
                            $output = $response['output'];
                            
                            $this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '2', date_modified = NOW() WHERE order_id = '" . (int) $order_id . "'");
                            $is_email = (isset($this->request->post['fastlo_email_customer']) && $this->request->post['fastlo_email_customer'] == 'yes') ? 1 : 0;
                            $shipmenthistory = "[Writecode: " . $output['writecode'] . "], [Tracknumber: " . $output['tracknumber'] . "]";

                            $message = array(
                                'notify' => $is_email,
                                'comment' => $shipmenthistory
                            );

                            $this->model_extension_shipping_fastlo_fastlo->addOrderHistory($order_id, $message);
                            $this->session->data['success'] = $this->language->get('text_success');

                            $this->response->redirect($this->url->link('extension/shipping/fastlo/create_shipment', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $order_id, 'SSL'));

                        } else {
                            $data["eRRORS"][] = $response['error'];
                        }
                    
                    } catch (Exception $e) {
                        $data['eRRORS'][] = $e->getMessage();
                    }

                }

                if (isset($this->error['sender_name'])) {
                    $data['error_sender_name'] = $this->error['sender_name'];
                } else {
                    $data['error_sender_name'] = '';
                }

                if (isset($this->error['sender_mobile1'])) {
                    $data['error_sender_mobile1'] = $this->error['sender_mobile1'];
                } else {
                    $data['error_sender_mobile1'] = '';
                }

                if (isset($this->error['sender_country'])) {
                    $data['error_sender_country'] = $this->error['sender_country'];
                } else {
                    $data['error_sender_country'] = '';
                }

                if (isset($this->error['sender_city'])) {
                    $data['error_sender_city'] = $this->error['sender_city'];
                } else {
                    $data['error_sender_city'] = '';
                }

                if (isset($this->error['sender_area'])) {
                    $data['error_sender_area'] = $this->error['sender_area'];
                } else {
                    $data['error_sender_area'] = '';
                }

                if (isset($this->error['receiver_name'])) {
                    $data['error_receiver_name'] = $this->error['receiver_name'];
                } else {
                    $data['error_receiver_name'] = '';
                }

                if (isset($this->error['receiver_mobile1'])) {
                    $data['error_receiver_mobile1'] = $this->error['receiver_mobile1'];
                } else {
                    $data['error_receiver_mobile1'] = '';
                }

                if (isset($this->error['receiver_country'])) {
                    $data['error_receiver_country'] = $this->error['receiver_country'];
                } else {
                    $data['error_receiver_country'] = '';
                }

                if (isset($this->error['receiver_city'])) {
                    $data['error_receiver_city'] = $this->error['receiver_city'];
                } else {
                    $data['error_receiver_city'] = '';
                }

                if (isset($this->error['receiver_area'])) {
                    $data['error_receiver_area'] = $this->error['receiver_area'];
                } else {
                    $data['error_receiver_area'] = '';
                }

                if (isset($this->error['collect_cash_amount'])) {
                    $data['error_collect_cash_amount'] = $this->error['collect_cash_amount'];
                } else {
                    $data['error_collect_cash_amount'] = '';
                }

                if (isset($this->error['number_of_pieces'])) {
                    $data['error_number_of_pieces'] = $this->error['number_of_pieces'];
                } else {
                    $data['error_number_of_pieces'] = '';
                }

                if (isset($this->error['reference'])) {
                    $data['error_reference'] = $this->error['reference'];
                } else {
                    $data['error_reference'] = '';
                }

            }

            if (isset($this->error['warning'])) {
                $data['error_warning'] = $this->error['warning'];
            } else {
                $data['error_warning'] = '';
            }

            if (isset($this->session->data['success'])) {
                $data['success'] = $this->session->data['success'];
    
                unset($this->session->data['success']);
            } else {
                $data['success'] = '';
            }    

            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');

            $this->response->setOutput($this->load->view('extension/shipping/fastlo/create_shipment', $data));

        } else {
            $this->language->load('error/not_found');

            $data['heading_title'] = $this->language->get('heading_title');

            $data['text_not_found'] = $this->language->get('text_not_found');

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], 'SSL'),
                'separator' => false
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('error/not_found', 'user_token=' . $this->session->data['user_token'], 'SSL'),
                'separator' => ' :: '
            );

            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');

            $this->response->setOutput($this->load->view('error/not_found', $data));
        }
    }

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'extension/shipping/fastlo/create_shipment')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->request->post['sender_name']) {
            $this->error['sender_name'] = $this->language->get('error_sender_name');
        }

        if (!$this->request->post['sender_mobile1']) {
            $this->error['sender_mobile1'] = $this->language->get('error_sender_mobile1');
        }

        if (!$this->request->post['sender_country']) {
            $this->error['sender_country'] = $this->language->get('error_sender_country');
        }

        if (!$this->request->post['sender_city']) {
            $this->error['sender_city'] = $this->language->get('error_sender_city');
        }

        if (!$this->request->post['sender_area']) {
            $this->error['sender_area'] = $this->language->get('error_sender_area');
        }
        
        if (!$this->request->post['receiver_name']) {
            $this->error['receiver_name'] = $this->language->get('error_receiver_name');
        }

        if (!$this->request->post['receiver_mobile1']) {
            $this->error['receiver_mobile1'] = $this->language->get('error_receiver_mobile1');
        }

        if (!$this->request->post['receiver_country']) {
            $this->error['receiver_country'] = $this->language->get('error_receiver_country');
        }

        if (!$this->request->post['receiver_city']) {
            $this->error['receiver_city'] = $this->language->get('error_receiver_city');
        }

        if (!$this->request->post['receiver_area']) {
            $this->error['receiver_area'] = $this->language->get('error_receiver_area');
        }

        if (!$this->request->post['collect_cash_amount']) {
            $this->error['collect_cash_amount'] = $this->language->get('error_collect_cash_amount');
        }

        if (!$this->request->post['number_of_pieces']) {
            $this->error['number_of_pieces'] = $this->language->get('error_number_of_pieces');
        }

        if (!$this->request->post['reference']) {
            $this->error['reference'] = $this->language->get('error_reference');
        }

        return !$this->error;
    }
}
