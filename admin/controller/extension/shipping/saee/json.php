<?php

class ControllerExtensionShippingSaeeJson extends Controller {

    private $api;

    public function __construct($registry) {
        parent::__construct($registry);

        require_once DIR_SYSTEM . '../admin/controller/extension/shipping/saee/api.php';
        $this->api = new \Saee\Api($this->config->get('shipping_saee_api_key'), $this->config->get('shipping_saee_test'));

    }

    public function tracking() {

        $prepare_data = [
            'trackingnum' => $this->request->get['waybill']
        ];            
        $json = $this->api->tracking($prepare_data);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

    public function cancel() {

        $this->language->load('extension/shipping/saee');
        $this->load->model('extension/shipping/saee/saee');

        $order_id = $this->request->get['order_id'];
        $waybill = $this->model_extension_shipping_saee_saee->getWaybill($order_id);

        if($waybill) {

            $prepare_data = [
                'waybill' => $waybill
            ];            

            $json = $this->api->cancelOrder($prepare_data);
            if($json['status'] == 1) {
    
                $this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '7', date_modified = NOW() WHERE order_id = '" . (int) $order_id . "'");
                $shipmenthistory = $this->language->get('text_shipment_cancelled');
        
                $message = array(
                    'notify' => 0,
                    'comment' => $shipmenthistory
                );
        
                $this->model_extension_shipping_saee_saee->addOrderHistory($order_id, $message);

            }
    
        } else {
            $json = ["error" => null];
        }

        $this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
