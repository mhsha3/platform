<?php

namespace Saee;
class Api {

    private $api_key;
    private $api_url;

    public function __construct($api_key, $test_mode) {
        $this->api_key    = $api_key;
        $this->api_url    = $test_mode ? "https://k-w-h.com" : "https://legacy.saeex.com";
    }

    private function apiRequest($method, $reqData = [], $method_type = 'POST') {
		
		$queryParams = '';
		$reqBody     = '';

		if ($method_type != 'GET') {
			if(!isset($reqData['secret'])) {
				$reqData['secret'] = $this->api_key;
			}
			$reqBody = json_encode($reqData);
		} else {
			$arRequest = [];
			foreach ($reqData as $key => $val) {
				$arRequest[] = $key . '=' . $val;
			}
			$queryParams = implode('&', $arRequest);
		}

		$arHeaders = [
			'Content-Type: application/json; charset=utf-8',
		];

		$url = $this->api_url . $method;
		if ($queryParams) $url .= '?' . $queryParams;

		return $this->curlRequest($url, $arHeaders, $reqBody, $method_type);
    }

	private function curlRequest($url, $arHeaders, $reqBody, $method_type = 'GET') {

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $arHeaders);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		if ($method_type == 'POST') {
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $reqBody);
		} elseif ($method_type == 'PUT' || $method_type == 'DELETE') {
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method_type);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $reqBody);
		}
	
		$response = json_decode(curl_exec($ch), true);
		curl_close ($ch);

		return $response;
		
	}

    private static function getErrors($response) {
		$error_messages = [];
		if (!$response || !isset($response['error'])) {
            $error_messages[] = $response['error'];
        } else {
			if(isset($response['error_messages']) && is_array($response['error_messages']) && count($response['error_messages'])) {
				$error_messages = $response['error_messages'];
			} else {
				$error_messages[] = $response['error'];
			}
		}
		return count($error_messages) ? ['error' => $error_messages] : null;
    }

	public function lastMileBooking($data) {
		$response = $this->apiRequest('/deliveryrequest/new', $data);
		if (!$response || isset($response['error'])) {
            return self::getErrors($response);
        }
        return $response;
	}

	public function newPickup($data) {
		$response = $this->apiRequest('/deliveryrequest/newpickup', $data);
		if (!$response || isset($response['error'])) {
            return self::getErrors($response);
        }
        return $response;
	}

	public function updateOder($data) {
		$response = $this->apiRequest('/deliveryrequest/update', $data);
		if (!$response || isset($response['error'])) {
            return self::getErrors($response);
        }
        return $response;
	}

	public function tracking($data) {
		$response = $this->apiRequest('/tracking', $data, 'GET');
		if (!$response || isset($response['error'])) {
            return self::getErrors($response);
        }
        return $response;
	}

	public function cancelOrder($data) {
		$response = $this->apiRequest('/deliveryrequest/cancel', $data);
		if ((!$response && $response != 0) || isset($response['error'])) {
            return self::getErrors($response);
		}
        return ['status' => $response];
	}

	public function getPrintLableURL($waybill) {
		return $this->api_url . "/deliveryrequest/printsticker/" . $waybill;
	}
}