<?php

class ControllerExtensionShippingSaeeCreateShipment extends Controller {

    private $api;

    public function __construct($registry) {
    parent::__construct($registry);

        require_once DIR_SYSTEM . '../admin/controller/extension/shipping/saee/api.php';
        $this->api = new \Saee\Api($this->config->get('shipping_saee_api_key'), $this->config->get('shipping_saee_test'));

    }

    private function filterData($myArray, $allowed) {
        $filtered = array_filter(
            $myArray,
            function ($key) use ($allowed) {
                return in_array($key, $allowed);
            },
            ARRAY_FILTER_USE_KEY
        );
        return $filtered;
    }

    public function index() {

        $this->language->load('extension/shipping/saee');
        $this->getForm();

    }

    public function getForm() {

        $this->load->model('sale/order');
		$this->load->model('setting/setting');
        $this->load->model('extension/shipping/saee/saee');

        $order_id = $this->request->get['order_id'] ?? 0;
        $order_info = $this->model_sale_order->getOrder($order_id);

        if ($order_info) {

            $data['user_token'] = $this->session->data['user_token'];

            ##################### Language Labels #####################

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], 'SSL'),
                'separator' => false
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('saee_create_heading_title'),
                'href' => $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'], 'SSL'),
                'separator' => ' :: '
            );

            $data['order_id'] = $this->request->get['order_id'];
            $data['ordernumber'] = $order_id;

            ##################### Buttons #####################

            $data['back_to_order'] = $this->url->link('sale/order/info', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $order_id, true);
            $data['create_sipment'] = $this->url->link('extension/shipping/saee/create_shipment', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $order_id, 'SSL');

            ##################### Sender details #####################

            $store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);

            if ($store_info) {
                $store_name = $store_info['config_name'];
                $store_email = $store_info['config_email'];
                $store_telephone = $store_info['config_telephone'];
                $store_address = $store_info['config_address'];
                $store_zone_id = $store_info['config_zone_id'];
                $store_country_id = $store_info['config_country_id'];
            } else {
                $store_name = $this->config->get('config_name');
                $store_email = $this->config->get('config_email');
                $store_telephone = $this->config->get('config_telephone');
                $store_address = $this->config->get('config_address');
                $store_zone_id = $this->config->get('config_zone_id');
                $store_country_id = $this->config->get('config_country_id');
            }

            $this->load->model('localisation/country');
			$country_info = $this->model_localisation_country->getCountry($this->config->get('config_country_id'));

            $this->load->model('localisation/zone');
            $zone_info = $this->model_localisation_zone->getZone($store_zone_id);

            if (isset($this->request->post['sendername'])) {
                $data['sendername'] = $this->request->post['sendername'];
            } else {
                $data['sendername'] = $store_name;
            }
    
            if (isset($this->request->post['sendermail'])) {
                $data['sendermail'] = $this->request->post['sendermail'];
            } else {
                $data['sendermail'] = $store_email;
            }

            if (isset($this->request->post['senderphone'])) {
                $data['senderphone'] = $this->request->post['senderphone'];
            } else {
                $data['senderphone'] = $store_telephone;
            }

            if (isset($this->request->post['senderaddress'])) {
                $data['senderaddress'] = $this->request->post['senderaddress'];
            } else {
                $data['senderaddress'] = $store_address;
            }

            if (isset($this->request->post['sendercity'])) {
                $data['sendercity'] = $this->request->post['sendercity'];
            } else {
                $data['sendercity'] = $zone_info ? $zone_info["name"] : "";
            }
    
            if (isset($this->request->post['sendercountry'])) {
                $data['sendercountry'] = $this->request->post['sendercountry'];
            } else {
                $data['sendercountry'] = $country_info ? $country_info["name"] : "";
            }
                
            ##################### Pickup details #####################

            $data['cashondelivery'] = $order_info['payment_code'] === "cod" ? 1 : 0;

            if (isset($this->request->post['cashonpickup'])) {
                $data['cashonpickup'] = $this->request->post['cashonpickup'];
            } else {
                $data['cashonpickup'] = $this->config->get('shipping_saee_cashonpickup');
            }

            if (isset($this->request->post['pickup_address_id'])) {
                $data['pickup_address_id'] = $this->request->post['pickup_address_id'];
            } else {
                $data['pickup_address_id'] = $this->config->get('shipping_saee_pickup_address_id');
            }

            if (isset($this->request->post['pickup_address_code'])) {
                $data['pickup_address_code'] = $this->request->post['pickup_address_code'];
            } else {
                $data['pickup_address_code'] = $this->config->get('shipping_saee_pickup_address_code');
            }

            if (isset($this->request->post['p_name'])) {
                $data['p_name'] = $this->request->post['p_name'];
            } else {
                $data['p_name'] = $this->config->get('shipping_saee_origin_name');
            }
    
            // if (isset($this->request->post['p_email'])) {
            //     $data['p_email'] = $this->request->post['p_email'];
            // } else {
            //     $data['p_email'] = $this->config->get('shipping_saee_origin_email');
            // }

            if (isset($this->request->post['p_mobile'])) {
                $data['p_mobile'] = $this->request->post['p_mobile'];
            } else {
                $data['p_mobile'] = $this->config->get('shipping_saee_origin_phone');
            }
    
            if (isset($this->request->post['p_mobile2'])) {
                $data['p_mobile2'] = $this->request->post['p_mobile2'];
            } else {
                $data['p_mobile2'] = $this->config->get('shipping_saee_origin_phone2');
            }

            if (isset($this->request->post['p_streetaddress'])) {
                $data['p_streetaddress'] = $this->request->post['p_streetaddress'];
            } else {
                $data['p_streetaddress'] = $this->config->get('shipping_saee_origin_address');
            }
    
            if (isset($this->request->post['p_streetaddress2'])) {
                $data['p_streetaddress2'] = $this->request->post['p_streetaddress2'];
            } else {
                $data['p_streetaddress2'] = $this->config->get('shipping_saee_origin_address2');
            }
                
            if (isset($this->request->post['p_latitude'])) {
                $data['p_latitude'] = $this->request->post['p_latitude'];
            } else {
                $data['p_latitude'] = $this->config->get('shipping_saee_origin_gps_latitude');
            }
    
            if (isset($this->request->post['p_longitude'])) {
                $data['p_longitude'] = $this->request->post['p_longitude'];
            } else {
                $data['p_longitude'] = $this->config->get('shipping_saee_origin_gps_longitude');
            }
        
            if (isset($this->request->post['p_district'])) {
                $data['p_district'] = $this->request->post['p_district'];
            } else {
                $data['p_district'] = $this->config->get('shipping_saee_origin_district');
            }
    
            if (isset($this->request->post['p_city'])) {
                $data['p_city'] = $this->request->post['p_city'];
            } else {
                $data['p_city'] = $this->config->get('shipping_saee_origin_city');
            }
    
            if (isset($this->request->post['p_state'])) {
                $data['p_state'] = $this->request->post['p_state'];
            } else {
                $data['p_state'] = $this->config->get('shipping_saee_origin_state');
            }

            if (isset($this->request->post['p_zipcode'])) {
                $data['p_zipcode'] = $this->request->post['p_zipcode'];
            } else {
                $data['p_zipcode'] = $this->config->get('shipping_saee_origin_zipcode');
            }

            // if (isset($this->request->post['p_country'])) {
            //     $data['p_country'] = $this->request->post['p_country'];
            // } else {
            //     $data['p_country'] = $this->config->get('shipping_saee_origin_country');
            // }

            if (isset($this->request->post['hs_code'])) {
                $data['hs_code'] = $this->request->post['hs_code'];
            } else {
                $data['hs_code'] = "";
            }

            ##################### Consignee details #####################

            $shipment_receiver_name = '';
            if (isset($order_info['shipping_firstname']) && !empty($order_info['shipping_firstname'])) {
                $shipment_receiver_name .= $order_info['shipping_firstname'];
            }
            if (isset($order_info['shipping_lastname']) && !empty($order_info['shipping_lastname'])) {
                $shipment_receiver_name .= " " . $order_info['shipping_lastname'];
            }

            if (isset($this->request->post['c_name']) && !empty($this->request->post['saee_shipment_receiver_name'])) {
                $data['c_name'] = $this->request->post['c_name'];
            } else {
                $data['c_name'] = trim($shipment_receiver_name);
            }

            if (isset($this->request->post['email'])) {
                $data['email'] = $this->request->post['email'];
            } else {
                $data['email'] = ($order_info['email']) ? $order_info['email'] : '';
            }

            if (isset($this->request->post['c_mobile'])) {
                $data['c_mobile'] = $this->request->post['c_mobile'];
            } else {
                $data['c_mobile'] = ($order_info['telephone']) ? $order_info['telephone'] : '';
            }

            if (isset($this->request->post['c_mobile2'])) {
                $data['c_mobile2'] = $this->request->post['c_mobile2'];
            } else {
                $data['c_mobile2'] = "";
            }

            if (isset($this->request->post['c_streetaddress'])) {
                $data['c_streetaddress'] = $this->request->post['c_streetaddress'];
            } else {
                $data['c_streetaddress'] = ($order_info['shipping_address_1']) ? $order_info['shipping_address_1'] : '';
            }

            if (isset($this->request->post['c_streetaddress2'])) {
                $data['c_streetaddress2'] = $this->request->post['c_streetaddress2'];
            } else {
                $data['c_streetaddress2'] = ($order_info['shipping_address_2']) ? $order_info['shipping_address_2'] : '';
            }

            if (isset($this->request->post['c_district'])) {
                $data['c_district'] = $this->request->post['c_district'];
            } else {
                $data['c_district'] = '';
            }

            if (isset($this->request->post['c_city'])) {
                $data['c_city'] = $this->request->post['c_city'];
            } else {
                $data['c_city'] = ($order_info['shipping_city']) ? $order_info['shipping_city'] : '';
            }

            if (isset($this->request->post['c_state'])) {
                $data['c_state'] = $this->request->post['c_state'];
            } else {
                $data['c_state'] = ($order_info['shipping_zone']) ? $order_info['shipping_zone'] : '';
            }

            if (isset($this->request->post['c_zipcode'])) {
                $data['c_zipcode'] = $this->request->post['c_zipcode'];
            } else {
                $data['c_zipcode'] = ($order_info['shipping_postcode']) ? $order_info['shipping_postcode'] : '';
            }

            if (isset($this->request->post['custom_value'])) {
                $data['custom_value'] = $this->request->post['custom_value'];
            } else {
                $data['custom_value'] = "";
            }

            if (isset($this->request->post['hs_code'])) {
                $data['hs_code'] = $this->request->post['hs_code'];
            } else {
                $data['hs_code'] = "";
            }

            if (isset($this->request->post['category_id'])) {
                $data['category_id'] = $this->request->post['category_id'];
            } else {
                $data['category_id'] = "";
            }

            if (isset($this->request->post['c_latitude'])) {
                $data['c_latitude'] = $this->request->post['c_latitude'];
            } else {
                $data['c_latitude'] = "";
            }

            if (isset($this->request->post['c_longitude'])) {
                $data['c_longitude'] = $this->request->post['c_longitude'];
            } else {
                $data['c_longitude'] = "";
            }

            ##################### Other details #####################

            if (isset($this->request->post['category_id'])) {
                $data['category_id'] = $this->request->post['category_id'];
            } else {
                $data['category_id'] = "";
            }

            if (isset($this->request->post['quantity'])) {
                $data['quantity'] = $this->request->post['quantity'];
            } else {
                $data['quantity'] = "";
            }

            if (isset($this->request->post['order_product'])) {
                $order_products = $this->request->post['order_product'];
            } elseif (isset($this->request->get['order_id'])) {
                $order_products = $this->model_sale_order->getOrderProducts($this->request->get['order_id']);
            } else {
                $order_products = array();
            }
            $weighttot = 0;
            foreach ($order_products as $order_product) {
                if (isset($order_product['order_option'])) {
                    $order_option = $order_product['order_option'];
                } elseif (isset($this->request->get['order_id'])) {
                    $order_option = $this->model_sale_order->getOrderOptions($this->request->get['order_id'], $order_product['order_product_id']);
                    $product_weight_query = $this->db->query("SELECT weight, weight_class_id FROM " . DB_PREFIX . "product WHERE product_id = '" . $order_product['product_id'] . "'");
                    $weight_class_query = $this->db->query("SELECT wcd.unit FROM " . DB_PREFIX . "weight_class wc LEFT JOIN " . DB_PREFIX . "weight_class_description wcd ON (wc.weight_class_id = wcd.weight_class_id) WHERE wcd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND wc.weight_class_id = '" . $product_weight_query->row['weight_class_id'] . "'");
                } else {
                    $order_option = array();
                }

                $config_weight_class_id = $this->config->get('config_weight_class_id');
                $prodweight = $this->weight->convert($product_weight_query->row['weight'], $product_weight_query->row['weight_class_id'], $config_weight_class_id);
                $prodweight = ($prodweight * $order_product['quantity']);
                $weighttot = ($weighttot + $prodweight);
             
                $order_products_data[] = array(
                    'comment' => $order_product['name'],
                    'quantity' => $order_product['quantity']
                );
            }

            if (isset($this->request->post['weight'])) {
                $data['weight'] = $this->request->post['weight'];
            } else {
                $data['weight'] = number_format($weighttot, 2);
            }

            $data['total'] = number_format($order_info['total'], 2);
            $data['total'] = $this->currency->format(str_replace(',', '', $data['total']), $order_info['currency_code'], $order_info['currency_value'], false);

            $data['order_products'] = $order_products_data;
            $data['description'] = json_encode($order_products_data);
            
            if (isset($this->request->post['currency_code'])) {
                $data['currency_code'] = $this->request->post['currency_code'];
            } else {
                $data['currency_code'] = ($order_info['currency_code']) ? $order_info['currency_code'] : '';
            }

            if (isset($this->request->post['cod_value'])) {
                $data['cod_value'] = $this->request->post['cod_value'];
            } else {
                $data['cod_value'] = ($order_info['total']) ? number_format($order_info['total'], 2) : '';
            }            

            $is_shipment = $this->model_extension_shipping_saee_saee->checkWaybill($this->request->get['order_id']);
            $data['is_shipment'] = $is_shipment;

            if(!$is_shipment) {
                if (isset($this->request->post['create_order_type'])) {
                    $data['create_order_type'] = $this->request->post['create_order_type'];
                } else {
                    $data['create_order_type'] = "last_mile_booking";
                }    
            } else {
                $waybill = $this->model_extension_shipping_saee_saee->getWaybill($this->request->get['order_id']);
                $data['waybill'] = $waybill;
                $data['print_label_url'] = $this->api->getPrintLableURL($waybill);
                // $data['tracking_url'] = $this->url->link('extension/shipping/saee/json/tracking', 'user_token=' . $this->session->data['user_token'] . '&waybill=' . $waybill, true);

            }

            ################## Create/Update Shipment ##################

            if ($this->request->post) {
                try {

                    if($is_shipment) {
                        $allowed_fields = ['cashondelivery',  'c_name', 'c_mobile', 
                        'c_mobile2', 'c_streetaddress', 'c_streetaddress2', 'c_district', 
                        'c_city', 'c_state', 'c_zipcode', 'c_latitude', 'c_longitude', 
                        'custom_value', 'hs_code', 'category_id', 'weight', 'quantity', 'description'];

                        $temp_data_fields = $this->filterData($data, $allowed_fields);
                        $prepare_data = [];
                        foreach($temp_data_fields as $key => $temp_data_field) {
                            $modified_key = str_replace("c_", "", $key);
                            $prepare_data[$modified_key] = $temp_data_field;
                        }

                        $prepare_data['waybill'] = $waybill;
                        $response = $this->api->updateOder($prepare_data);

                    } else {
                        if($data['create_order_type'] == "last_mile_booking") {
                            $allowed_fields = ['ordernumber', 'cashondelivery',  'c_name', 'c_mobile', 
                            'c_mobile2', 'c_streetaddress', 'c_streetaddress2', 'c_district', 
                            'c_city', 'c_state', 'c_zipcode', 'c_latitude', 'c_longitude', 
                            'custom_value', 'hs_code', 'category_id', 'weight', 'quantity', 'description', 
                            'email', 'pickup_address_id', 'pickup_address_code', 'sendername', 'sendermail', 
                            'senderphone', 'senderaddress', 'sendercity', 'sendercountry', 'sender_hub'];

                            $temp_data_fields = $this->filterData($data, $allowed_fields);
                            $prepare_data = [];
                            foreach($temp_data_fields as $key => $temp_data_field) {
                                $modified_key = str_replace("c_", "", $key);
                                $prepare_data[$modified_key] = $temp_data_field;
                            }
                            $response = $this->api->lastMileBooking($prepare_data);
                        } else {

                            $allowed_fields = ['ordernumber', 'cashonpickup', 'cashondelivery', 
                            'pickup_address_id', 'pickup_address_code', 'p_name', 'p_mobile', 
                            'p_mobile2', 'p_streetaddress', 'p_streetaddress2', 'p_district', 
                            'p_city', 'p_state', 'p_zipcode', 'p_latitude', 'p_longitude', 
                            'custom_value', 'hs_code', 'category_id', 'c_name', 'c_mobile', 'c_mobile2', 
                            'c_streetaddress', 'c_streetaddress2', 'c_district', 'c_city', 'c_state', 
                            'c_zipcode', 'c_latitude', 'c_longitude', 'weight', 'quantity', 'description',
                            'email', 'sendername', 'sendermail', 'senderphone', 'senderaddress', 
                            'sendercity', 'sendercountry', 'sender_hub'];

                            $prepare_data = $this->filterData($data, $allowed_fields);
                            $response = $this->api->newPickup($prepare_data);
                        }
                    }

                    if(isset($response['success'])) {

                        if(!$is_shipment) {

                            $this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '2', date_modified = NOW() WHERE order_id = '" . (int) $order_id . "'");
                            $is_email = (isset($this->request->post['saee_email_customer']) && $this->request->post['saee_email_customer'] == 'yes') ? 1 : 0;
                            $shipmenthistory = "Waybill No. " . $response['waybill'] . " [<a href=\"" . $response['epayment_url'] . "\" target=\"_blank\">" . $response['epayment_url'] . "</a>]";

                            $message = array(
                                'notify' => $is_email,
                                'comment' => $shipmenthistory
                            );

                            $this->model_extension_shipping_saee_saee->addOrderHistory($order_id, $message);
                            $this->session->data['success_html'] = "Order created successfully";
                        
                        } else {
                            $this->session->data['success_html'] = "Order updated successfully";
                        }
                     
                        $this->response->redirect($this->url->link('extension/shipping/saee/create_shipment', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $order_id, 'SSL'));

                    } elseif(isset($response['error'])) {
                        $data["eRRORS"] = $response['error'];
                    }

                } catch (Exception $e) {
                    $data['eRRORS'][] = $e->getMessage();
                }

            }

            if (isset($this->session->data['success_html'])) {
                $data['success_html'] = $this->session->data['success_html'];
                unset($this->session->data['success_html']);
            } else {
                $data['success_html'] = '';
            }

            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');

            $this->response->setOutput($this->load->view('extension/shipping/saee/create_shipment', $data));
        } else {
            $this->language->load('error/not_found');

            $data['heading_title'] = $this->language->get('heading_title');

            $data['text_not_found'] = $this->language->get('text_not_found');

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], 'SSL'),
                'separator' => false
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('error/not_found', 'user_token=' . $this->session->data['user_token'], 'SSL'),
                'separator' => ' :: '
            );

            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');

            $this->response->setOutput($this->load->view('error/not_found', $data));
        }
    }

}
