<?php

class ControllerExtensionShipping525kCreateShipment extends Controller {

    private $error = array();
    private $api;
    private $shipper_id;

    public function __construct($registry) {
    parent::__construct($registry);

        require_once DIR_SYSTEM . '../admin/controller/extension/shipping/525k/api.php';
        $this->api = new \GS525K\Api($this->config->get('shipping_525k_client_id'), $this->config->get('shipping_525k_client_secret'), $this->config->get('shipping_525k_api_key'), $this->config->get('shipping_525k_test'));
        $this->shipper_id = $this->config->get('shipping_525k_shipper_id');

    }

    public function index() {

      $this->language->load('extension/shipping/525k');
      $this->getForm();

  }

  public function getForm() {

      $this->load->model('sale/order');
      $this->load->model('setting/setting');
      $this->load->model('localisation/country');
      $this->load->model('extension/shipping/525k/525k');

      $order_id = $this->request->get['order_id'] ?? 0;
      $order_info = $this->model_sale_order->getOrder($order_id);

      if ($order_info) {

          $data['user_token'] = $this->session->data['user_token'];

          ##################### Language Labels #####################

          $data['breadcrumbs'] = array();

          $data['breadcrumbs'][] = array(
              'text' => $this->language->get('text_home'),
              'href' => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], 'SSL'),
              'separator' => false
          );

          $data['breadcrumbs'][] = array(
              'text' => $this->language->get('i525k_create_heading_title'),
              'href' => $this->url->link('sale/order', 'user_token=' . $this->session->data['user_token'], 'SSL'),
              'separator' => ' :: '
          );

          $data['order_id'] = $this->request->get['order_id'];
          $data['ordernumber'] = $order_id;

          ##################### Buttons #####################

          $data['back_to_order'] = $this->url->link('sale/order/info', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $order_id, true);
          $data['create_sipment'] = $this->url->link('extension/shipping/525k/create_shipment', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $order_id, 'SSL');

          ##################### Sender details #####################

          $store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);

          if ($store_info) {
              $store_name = $store_info['config_name'];
              $store_email = $store_info['config_email'];
              $store_telephone = $store_info['config_telephone'];
              $store_address = $store_info['config_address'];
              $store_zone_id = $store_info['config_zone_id'];
              $store_country_id = $store_info['config_country_id'];
          } else {
              $store_name = $this->config->get('config_name');
              $store_email = $this->config->get('config_email');
              $store_telephone = $this->config->get('config_telephone');
              $store_address = $this->config->get('config_address');
              $store_zone_id = $this->config->get('config_zone_id');
              $store_country_id = $this->config->get('config_country_id');
          }

          $this->load->model('localisation/country');
          $country_info = $this->model_localisation_country->getCountry($this->config->get('config_country_id'));

          $this->load->model('localisation/zone');
          $zone_info = $this->model_localisation_zone->getZone($store_zone_id);

          if (isset($this->request->post['sendername'])) {
              $data['sendername'] = $this->request->post['sendername'];
          } else {
              $data['sendername'] = $store_name;
          }
  
          if (isset($this->request->post['sendermail'])) {
              $data['sendermail'] = $this->request->post['sendermail'];
          } else {
              $data['sendermail'] = $store_email;
          }

          if (isset($this->request->post['senderphone'])) {
              $data['senderphone'] = $this->request->post['senderphone'];
          } else {
              $data['senderphone'] = $store_telephone;
          }

          if (isset($this->request->post['senderaddress'])) {
              $data['senderaddress'] = $this->request->post['senderaddress'];
          } else {
              $data['senderaddress'] = $store_address;
          }

          if (isset($this->request->post['sendercity'])) {
              $data['sendercity'] = $this->request->post['sendercity'];
          } else {
              $data['sendercity'] = $zone_info ? $zone_info["name"] : "";
          }
  
          if (isset($this->request->post['sendercountry'])) {
              $data['sendercountry'] = $this->request->post['sendercountry'];
          } else {
              $data['sendercountry'] = $country_info ? $country_info["name"] : "";
          }
              
          ##################### Pickup details #####################

          $data['cashondelivery'] = $order_info['payment_code'] === "cod" ? 1 : 0;

          if (isset($this->request->post['p_name'])) {
              $data['p_name'] = $this->request->post['p_name'];
          } else {
              $data['p_name'] = $this->config->get('shipping_525k_origin_name');
          }
  
          if (isset($this->request->post['p_email'])) {
              $data['p_email'] = $this->request->post['p_email'];
          } else {
              $data['p_email'] = $this->config->get('shipping_525k_origin_email');
          }

          if (isset($this->request->post['p_mobile'])) {
              $data['p_mobile'] = $this->request->post['p_mobile'];
          } else {
              $data['p_mobile'] = $this->config->get('shipping_525k_origin_phone');
          }
  
          if (isset($this->request->post['p_mobile2'])) {
              $data['p_mobile2'] = $this->request->post['p_mobile2'];
          } else {
              $data['p_mobile2'] = $this->config->get('shipping_525k_origin_phone2');
          }

          if (isset($this->request->post['p_streetaddress'])) {
              $data['p_streetaddress'] = $this->request->post['p_streetaddress'];
          } else {
              $data['p_streetaddress'] = $this->config->get('shipping_525k_origin_address');
          }
  
          if (isset($this->request->post['p_streetaddress2'])) {
              $data['p_streetaddress2'] = $this->request->post['p_streetaddress2'];
          } else {
              $data['p_streetaddress2'] = $this->config->get('shipping_525k_origin_address2');
          }
        
          if (isset($this->request->post['p_city'])) {
              $data['p_city'] = $this->request->post['p_city'];
          } else {
              $data['p_city'] = $this->config->get('shipping_525k_origin_city');
          }
  
          if (isset($this->request->post['p_country'])) {
              $data['p_country'] = $this->request->post['p_country'];
          } else {
              $data['p_country'] = $this->config->get('shipping_525k_origin_country');
          }

          if (isset($this->request->post['p_latitude'])) {
              $data['p_latitude'] = $this->request->post['p_latitude'];
          } else {
              $data['p_latitude'] = $this->config->get('shipping_525k_origin_gps_latitude');
          }
  
          if (isset($this->request->post['p_longitude'])) {
              $data['p_longitude'] = $this->request->post['p_longitude'];
          } else {
              $data['p_longitude'] = $this->config->get('shipping_525k_origin_gps_longitude');
          }

          if (isset($this->request->post['p_arrival_window_begin'])) {
              $data['p_arrival_window_begin'] = $this->request->post['p_arrival_window_begin'];
          } else {
              $data['p_arrival_window_begin'] = date('Y-m-d\TH:i');
          }

          if (isset($this->request->post['p_arrival_window_end'])) {
              $data['p_arrival_window_end'] = $this->request->post['p_arrival_window_end'];
          } else {
              $data['p_arrival_window_end'] = date('Y-m-d\TH:i', strtotime('+' . $this->config->get('shipping_525k_pickup_arrival_window_days') . ' day'));
          }

          if (isset($this->request->post['p_exclude_arrival_window_begin'])) {
              $data['p_exclude_arrival_window_begin'] = $this->request->post['p_exclude_arrival_window_begin'];
          } else {
              $data['p_exclude_arrival_window_begin'] = $this->config->get('shipping_525k_pickup_arrival_window_exclude_begin');
          }

          if (isset($this->request->post['p_exclude_arrival_window_end'])) {
              $data['p_exclude_arrival_window_end'] = $this->request->post['p_exclude_arrival_window_end'];
          } else {
              $data['p_exclude_arrival_window_end'] = $this->config->get('shipping_525k_pickup_arrival_window_exclude_end');
          }
          
          if (isset($this->request->post['p_instructions'])) {
              $data['p_instructions'] = $this->request->post['p_instructions'];
          } else {
              $data['p_instructions'] = $this->config->get('shipping_525k_pickup_instructions');
          }

          ##################### Consignee details #####################

          $shipment_receiver_name = '';
          if (isset($order_info['shipping_firstname']) && !empty($order_info['shipping_firstname'])) {
              $shipment_receiver_name .= $order_info['shipping_firstname'];
          }
          if (isset($order_info['shipping_lastname']) && !empty($order_info['shipping_lastname'])) {
              $shipment_receiver_name .= " " . $order_info['shipping_lastname'];
          }

          if (isset($this->request->post['c_name']) && !empty($this->request->post['525k_shipment_receiver_name'])) {
              $data['c_name'] = $this->request->post['c_name'];
          } else {
              $data['c_name'] = trim($shipment_receiver_name);
          }

          if (isset($this->request->post['c_email'])) {
              $data['c_email'] = $this->request->post['c_email'];
          } else {
              $data['c_email'] = ($order_info['email']) ? $order_info['email'] : '';
          }

          if (isset($this->request->post['c_mobile'])) {
              $data['c_mobile'] = $this->request->post['c_mobile'];
          } else {
              $data['c_mobile'] = ($order_info['telephone']) ? $order_info['telephone'] : '';
          }

          if (isset($this->request->post['c_mobile2'])) {
              $data['c_mobile2'] = $this->request->post['c_mobile2'];
          } else {
              $data['c_mobile2'] = "";
          }

          if (isset($this->request->post['c_streetaddress'])) {
              $data['c_streetaddress'] = $this->request->post['c_streetaddress'];
          } else {
              $data['c_streetaddress'] = ($order_info['shipping_address_1']) ? $order_info['shipping_address_1'] : '';
          }

          if (isset($this->request->post['c_streetaddress2'])) {
              $data['c_streetaddress2'] = $this->request->post['c_streetaddress2'];
          } else {
              $data['c_streetaddress2'] = ($order_info['shipping_address_2']) ? $order_info['shipping_address_2'] : '';
          }

          if (isset($this->request->post['c_city'])) {
              $data['c_city'] = $this->request->post['c_city'];
          } else {
              $data['c_city'] = ($order_info['shipping_city']) ? $order_info['shipping_city'] : '';
          }

          $payment_country = $this->model_localisation_country->getCountry($order_info['payment_country_id']);
          if (isset($this->request->post['c_country'])) {
              $data['c_country'] = $this->request->post['c_country'];
          } else {
              $data['c_country'] = ($payment_country['name']) ? $payment_country['name'] : '';
          }

          if (isset($this->request->post['c_latitude'])) {
              $data['c_latitude'] = $this->request->post['c_latitude'];
          } else {
              $data['c_latitude'] = "";
          }

          if (isset($this->request->post['c_longitude'])) {
              $data['c_longitude'] = $this->request->post['c_longitude'];
          } else {
              $data['c_longitude'] = "";
          }

          if (isset($this->request->post['c_arrival_window_begin'])) {
              $data['c_arrival_window_begin'] = $this->request->post['c_arrival_window_begin'];
          } else {
              $data['c_arrival_window_begin'] = date('Y-m-d\TH:i', strtotime('+' . $this->config->get('shipping_525k_pickup_arrival_window_days') . ' day'));
          }

          if (isset($this->request->post['c_arrival_window_end'])) {
              $data['c_arrival_window_end'] = $this->request->post['c_arrival_window_end'];
          } else {
              $data['c_arrival_window_end'] = date('Y-m-d\TH:i', strtotime('+' . ($this->config->get('shipping_525k_pickup_arrival_window_days') + $this->config->get('shipping_525k_delivery_arrival_window_days')) . ' day'));
          }

          if (isset($this->request->post['c_exclude_arrival_window_begin'])) {
              $data['c_exclude_arrival_window_begin'] = $this->request->post['c_exclude_arrival_window_begin'];
          } else {
              $data['c_exclude_arrival_window_begin'] = $this->config->get('shipping_525k_delivery_arrival_window_exclude_begin');
          }

          if (isset($this->request->post['c_exclude_arrival_window_end'])) {
              $data['c_exclude_arrival_window_end'] = $this->request->post['c_exclude_arrival_window_end'];
          } else {
              $data['c_exclude_arrival_window_end'] = $this->config->get('shipping_525k_delivery_arrival_window_exclude_end');
          }
          
          if (isset($this->request->post['c_instructions'])) {
              $data['c_instructions'] = $this->request->post['c_instructions'];
          } else {
              $data['c_instructions'] = $this->config->get('shipping_525k_delivery_instructions');
          }

          ##################### Other details #####################

          if (isset($this->request->post['ref_id'])) {
              $data['ref_id'] = $this->request->post['ref_id'];
          } else {
              $data['ref_id'] = "";
          }

          if (isset($this->request->post['load_description'])) {
              $data['load_description'] = $this->request->post['load_description'];
          } else {
              $data['load_description'] = "";
          }

          if (isset($this->request->post['full_truck'])) {
            $data['full_truck'] = $this->request->post['full_truck'];
          } else {
              $data['full_truck'] = "";
          }

          $data['packaging_types'] = array();

          $data['packaging_types'][] = array(
            'value' => 'BOX',
            'text'  => $this->language->get('text_box')
          );
      
          $data['packaging_types'][] = array(
            'value' => 'PALLET',
            'text'  => $this->language->get('text_pallet')
          );
      
          $data['packaging_types'][] = array(
            'value' => 'OTHER',
            'text'  => $this->language->get('text_other')
          );		
      
          if (isset($this->request->post['packaging_type'])) {
              $data['packaging_type'] = $this->request->post['packaging_type'];
          } else {
              $data['packaging_type'] = $this->config->get('shipping_525k_packaging_type');
          }

          if (isset($this->request->post['stackable'])) {
            $data['stackable'] = $this->request->post['stackable'];
          } else {
              $data['stackable'] = $this->config->get('shipping_525k_stackable');
          }

          if (isset($this->request->post['item_length'])) {
            $data['item_length'] = $this->request->post['item_length'];
          } else {
              $data['item_length'] = $this->config->get('shipping_525k_length');
          }

          if (isset($this->request->post['item_width'])) {
            $data['item_width'] = $this->request->post['item_width'];
          } else {
              $data['item_width'] = $this->config->get('shipping_525k_width');
          }
           
          if (isset($this->request->post['item_height'])) {
            $data['item_height'] = $this->request->post['item_height'];
          } else {
              $data['item_height'] = $this->config->get('shipping_525k_height');
          }

          if (isset($this->request->post['item_quantity'])) {
            $data['item_quantity'] = $this->request->post['item_quantity'];
          } else {
              $data['item_quantity'] = "";
          }

          if (isset($this->request->post['item_weight'])) {
            $data['item_weight'] = $this->request->post['item_weight'];
          } else {
              $data['item_weight'] = "";
          }

          if (isset($this->request->post['item_refid'])) {
            $data['item_refid'] = $this->request->post['item_refid'];
          } else {
              $data['item_refid'] = "";
          }

          if (isset($this->request->post['cod_value_amount'])) {
            $data['cod_value_amount'] = $this->request->post['cod_value_amount'];
          } else {
              $data['cod_value_amount'] = "";
          }

          if (isset($this->request->post['cod_value_currency'])) {
            $data['cod_value_currency'] = $this->request->post['cod_value_currency'];
          } else {
              $data['cod_value_currency'] = "";
          }

          if (isset($this->request->post['order_product'])) {
              $order_products = $this->request->post['order_product'];
          } elseif (isset($this->request->get['order_id'])) {
              $order_products = $this->model_sale_order->getOrderProducts($this->request->get['order_id']);
          } else {
              $order_products = array();
          }
          $weighttot = 0;
          foreach ($order_products as $order_product) {
              if (isset($order_product['order_option'])) {
                  $order_option = $order_product['order_option'];
              } elseif (isset($this->request->get['order_id'])) {
                  $order_option = $this->model_sale_order->getOrderOptions($this->request->get['order_id'], $order_product['order_product_id']);
                  $product_weight_query = $this->db->query("SELECT weight, weight_class_id FROM " . DB_PREFIX . "product WHERE product_id = '" . $order_product['product_id'] . "'");
                  $weight_class_query = $this->db->query("SELECT wcd.unit FROM " . DB_PREFIX . "weight_class wc LEFT JOIN " . DB_PREFIX . "weight_class_description wcd ON (wc.weight_class_id = wcd.weight_class_id) WHERE wcd.language_id = '" . (int) $this->config->get('config_language_id') . "' AND wc.weight_class_id = '" . $product_weight_query->row['weight_class_id'] . "'");
              } else {
                  $order_option = array();
              }

              $config_weight_class_id = $this->config->get('config_weight_class_id');
              $prodweight = $this->weight->convert($product_weight_query->row['weight'], $product_weight_query->row['weight_class_id'], $config_weight_class_id);
              $prodweight = ($prodweight * $order_product['quantity']);
              $weighttot = ($weighttot + $prodweight);
           
              $order_products_data[] = array(
                  'comment' => $order_product['name'],
                  'quantity' => $order_product['quantity']
              );
          }

          if (isset($this->request->post['weight'])) {
              $data['weight'] = $this->request->post['weight'];
          } else {
              $data['weight'] = number_format($weighttot, 2);
          }

          $data['total'] = number_format($order_info['total'], 2);
          $data['total'] = $this->currency->format(str_replace(',', '', $data['total']), $order_info['currency_code'], $order_info['currency_value'], false);

          if (isset($this->request->post['shipment_value_amount'])) {
            $data['shipment_value_amount'] = $this->request->post['shipment_value_amount'];
          } else {
              $data['shipment_value_amount'] = $data['total'];
          }

          $data['order_products'] = $order_products_data;
          $data['description'] = json_encode($order_products_data);
          
          if (isset($this->request->post['shipment_value_currency'])) {
              $data['shipment_value_currency'] = $this->request->post['shipment_value_currency'];
          } else {
              $data['shipment_value_currency'] = ($order_info['currency_code']) ? $order_info['currency_code'] : '';
          }

          if (isset($this->request->post['cod_value'])) {
              $data['cod_value'] = $this->request->post['cod_value'];
          } else {
              $data['cod_value'] = ($order_info['total']) ? number_format($order_info['total'], 2) : '';
          }            

          $is_shipment = $this->model_extension_shipping_525k_525k->checkWaybill($this->request->get['order_id']);
          $data['is_shipment'] = $is_shipment;

          if($is_shipment) {
              $waybill = $this->model_extension_shipping_525k_525k->getWaybill($this->request->get['order_id']);
              $data['waybill'] = $waybill;
              $data['tracking_url'] = $this->api->getTrackingURL($waybill);

              $shipment_id = $this->model_extension_shipping_525k_525k->getShipmentId($this->request->get['order_id']);
              $data['print_label_url'] = $this->api->getPrintLableURL($shipment_id);
          }

          ################## Create/Update Shipment ##################

          if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

            try {

                $prepare_data = [
                  'autoAssignDrivers' => false,
                  'requestedBy' => $this->config->get('shipping_525k_requestedby_id'),
                  'shipments' => [
                    0 => [
                      'fullTruck' => $data['full_truck'] == 1,
                      'loadDescription' => $data['load_description'],
                      'codValue' => [
                        'amount' => $data['cod_value_amount'],
                        'currency' => $data['cod_value_currency'],
                      ],
                      'delivery' => [
                        'address' => $data['c_streetaddress'] ,
                        'address2' => $data['c_streetaddress2'],
                        'instructions' => $data['c_instructions'],
                        'arrivalWindow' => [
                          'begin' => $data['c_arrival_window_begin'] . ":00.020Z",
                          'end' => $data['c_arrival_window_end'] . ":00.020Z",
                          'excludeBegin' => $data['c_exclude_arrival_window_begin'] == 1,
                          'excludeEnd' => $data['c_exclude_arrival_window_end'] == 1,
                        ],
                        'city' => $data['c_city'],
                        'coordinate' => [
                          'latitude' => $data['c_latitude'],
                          'longitude' => $data['c_longitude'],
                        ],
                        'country' => $data['c_country'],
                        'email' => $data['c_email'],
                        'name' => $data['c_name'],
                        'phone' => $data['c_mobile'],
                      ],
                      'refId' => $data['ref_id'],
                      'items' => [
                        0 => [
                          'dimensions' => [
                            'height' => $data['item_height'],
                            'length' => $data['item_length'],
                            'width' => $data['item_width'],
                          ],
                          'refId' => $data['item_refid'],
                          'weight' => $data['item_weight'],
                          'quantity' => $data['item_quantity'],
                          'stackable' => $data['stackable'] == 1,
                          'type' => $data['packaging_type'],
                        ],
                      ],
                      'pickup' => [
                        'address' => $data['p_streetaddress'],
                        'address2' => $data['p_streetaddress2'],
                        'arrivalWindow' => [
                          'begin' => $data['p_arrival_window_begin'] . ":00.020Z",
                          'end' => $data['p_arrival_window_end'] . ":00.020Z",
                          'excludeBegin' => $data['p_exclude_arrival_window_begin'] == 1,
                          'excludeEnd' => $data['p_exclude_arrival_window_end'] == 1,
                        ],
                        'coordinate' => [
                          'latitude' => $data['p_latitude'],
                          'longitude' => $data['p_longitude'],
                        ],
                        'city' => $data['p_city'],
                        'country' => $data['p_country'],
                        'email' => $data['p_email'],
                        'name' => $data['p_name'],
                        'phone' => $data['p_mobile'],
                      ],
                      'shipmentValue' => [
                        'amount' => $data['shipment_value_amount'],
                        'currency' => $data['shipment_value_currency'],
                      ],
                    ],
                  ],
                  'shipperId' => $this->config->get('shipping_525k_shipper_id'),
                ];
                
                $response = $this->api->createShipping($prepare_data);
                if(isset($response['shipments'])) {

                    if(!$is_shipment) {

                        $this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '2', date_modified = NOW() WHERE order_id = '" . (int) $order_id . "'");
                        $is_email = (isset($this->request->post['525k_email_customer']) && $this->request->post['525k_email_customer'] == 'yes') ? 1 : 0;
                        $shipmenthistory = "[Waybill No. " . $response['shipments'][0]['waybill'] . "], [Shipment Id: " . $response['shipments'][0]['id'] . "]";

                        $message = array(
                            'notify' => $is_email,
                            'comment' => $shipmenthistory
                        );

                        $this->model_extension_shipping_525k_525k->addOrderHistory($order_id, $message);
                        $this->session->data['success_html'] = "Order created successfully";
                    
                    } else {
                        $this->session->data['success_html'] = "Order updated successfully";
                    }
                  
                    $this->response->redirect($this->url->link('extension/shipping/525k/create_shipment', 'user_token=' . $this->session->data['user_token'] . '&order_id=' . $order_id, 'SSL'));

                } elseif(isset($response['error'])) {
                    $data["eRRORS"] = $response['error'];
                }

              } catch (Exception $e) {
                  $data['eRRORS'][] = $e->getMessage();
              }

          }

          if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
          } else {
            $data['error_warning'] = '';
          }
          
          if (isset($this->error['eRRORS'])) {
            $data['eRRORS'][] = $this->error['eRRORS'];
          }

          if (isset($this->session->data['success_html'])) {
              $data['success_html'] = $this->session->data['success_html'];
              unset($this->session->data['success_html']);
          } else {
              $data['success_html'] = '';
          }

          $data['header'] = $this->load->controller('common/header');
          $data['column_left'] = $this->load->controller('common/column_left');
          $data['footer'] = $this->load->controller('common/footer');

          $this->response->setOutput($this->load->view('extension/shipping/525k/create_shipment', $data));
      } else {
          $this->language->load('error/not_found');

          $data['heading_title'] = $this->language->get('heading_title');

          $data['text_not_found'] = $this->language->get('text_not_found');

          $data['breadcrumbs'] = array();

          $data['breadcrumbs'][] = array(
              'text' => $this->language->get('text_home'),
              'href' => $this->url->link('common/home', 'user_token=' . $this->session->data['user_token'], 'SSL'),
              'separator' => false
          );

          $data['breadcrumbs'][] = array(
              'text' => $this->language->get('heading_title'),
              'href' => $this->url->link('error/not_found', 'user_token=' . $this->session->data['user_token'], 'SSL'),
              'separator' => ' :: '
          );

          $data['header'] = $this->load->controller('common/header');
          $data['column_left'] = $this->load->controller('common/column_left');
          $data['footer'] = $this->load->controller('common/footer');

          $this->response->setOutput($this->load->view('error/not_found', $data));
      }
  }

  protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/shipping/525k/create_shipment')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

    if (!$this->request->post['c_name'] || 
      !$this->request->post['c_email'] || 
      !$this->request->post['c_mobile'] || 
      !$this->request->post['c_streetaddress'] || 
      !$this->request->post['c_country'] || 
      !$this->request->post['c_arrival_window_begin'] || 
      !$this->request->post['c_arrival_window_end'] || 
      !$this->request->post['p_name'] || 
      !$this->request->post['p_email'] || 
      !$this->request->post['p_mobile'] || 
      !$this->request->post['p_streetaddress'] || 
      !$this->request->post['p_city'] || 
      !$this->request->post['p_country'] || 
      !$this->request->post['p_latitude'] || 
      !$this->request->post['p_longitude'] || 
      !$this->request->post['p_arrival_window_begin'] || 
      !$this->request->post['p_arrival_window_end'] || 
      !$this->request->post['ref_id'] || 
      !$this->request->post['packaging_type'] || 
      !$this->request->post['item_length'] || 
      !$this->request->post['item_width'] || 
      !$this->request->post['item_height'] || 
      !$this->request->post['item_quantity'] || 
      !$this->request->post['item_weight'] || 
      !$this->request->post['shipment_value_amount'] || 
      !$this->request->post['shipment_value_currency']
    ) {
			$this->error['eRRORS'] = $this->language->get('error_input_data');
		}			

		return !$this->error;
	}

}
