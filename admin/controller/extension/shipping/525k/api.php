<?php

namespace GS525K;

class Api {

    private $client_id;
    private $client_secret;
	private $api_key;
	
	private $api_url;
	private $token_url;
	
    public function __construct($client_id, $client_secret, $api_key, $test_mode) {
		
        $this->client_id    = $client_id;
		$this->client_secret = $client_secret;
		$this->api_key = $api_key;

		if($test_mode) {
			$this->api_url = "https://api-dev.scdev.io";
			$this->token_url = "https://auth-dev.525k.io/oauth2/token";
		} else {
			$this->api_url = "https://api.525k.io";
			$this->token_url = "https://auth-prod.525k.io/oauth2/token";
		}
    }

	private function apiRequest($method, $reqData = [], $method_type = 'POST') {
		
		$queryParams = '';
		$reqBody     = '';

		if ($method_type != 'GET') {
			$reqBody = json_encode($reqData);
		} else {
			$arRequest = [];
			foreach ($reqData as $key => $val) {
				$arRequest[] = $key . '=' . $val;
			}
			$queryParams = implode('&', $arRequest);
		}

		$arHeaders = [
			'Content-Type: application/json',
			'x-api-key: ' . $this->api_key,
			'Authorization: Bearer ' . $this->getAccessToken(),
		];

		$url = $this->api_url . $method;
		if ($queryParams) $url .= '?' . $queryParams;

		return $this->curlRequest($url, $arHeaders, $reqBody, $method_type);
	}
	
	private function curlRequest($url, $arHeaders, $reqBody, $method_type = 'GET') {

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $arHeaders);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		if ($method_type == 'POST') {
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $reqBody);
		} elseif ($method_type == 'PUT' || $method_type == 'DELETE' || $method_type == 'PATCH') {
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method_type);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $reqBody);
		}
	
		$response = json_decode(curl_exec($ch), true);
		curl_close ($ch);

		return $response;
		
	}

    private function getAccessToken() {
		
		try {
		
			$client_id = $this->client_id;
			$client_secret = $this->client_secret;
			$tokenUrl = $this->token_url;
			$tokenContent = "grant_type=client_credentials";
			$authorization = base64_encode("$client_id:$client_secret");
			$tokenHeaders = array("Authorization: Basic {$authorization}","Content-Type: application/x-www-form-urlencoded");

			$response = $this->curlRequest($tokenUrl, $tokenHeaders, $tokenContent, 'POST');
			if(isset($response['access_token'])) {
				return $response['access_token'];
			} else {
				return null;
			}

		} catch(Exception $e) {
			return null;
		}
	}
	
    private static function getErrors($response) {
		$error_messages = [];
		if (!$response || !isset($response['error'])) {
            $error_messages[] = $response['error'] ?? null;
        } else {
			if(isset($response['error_messages']) && is_array($response['error_messages']) && count($response['error_messages'])) {
				$error_messages = $response['error_messages'];
			} else {
				$error_messages[] = $response['error'];
			}
		}
		return count($error_messages) ? ['error' => $error_messages] : null;
	}
	
	public function createShipping($data) {
		$response = $this->apiRequest('/api/shipments', $data);
		if (!$response || isset($response['error'])) {
            return self::getErrors($response);
        }

        return $response;
	}

	public function cancelOrder($data) {
		$response = $this->apiRequest('/api/shipments/cancel', $data, 'PATCH');
		if (!$response || isset($response['error'])) {
            return self::getErrors($response);
		}
        return $response;
	}

	public function getPrintLableURL($shipment_id) {
		return "https://cf2.525k.io/shipping-labels/mlcgo/" . $shipment_id . ".pdf";
	}

	public function getTrackingURL($waybill) {
		return "https://shipping.mlcgo.com/shipment/track/" . $waybill;
	}

}