<?php

class ControllerExtensionShipping525kJson extends Controller {

    private $error = array();
    private $api;
    private $shipper_id;

    public function __construct($registry) {
    parent::__construct($registry);

        require_once DIR_SYSTEM . '../admin/controller/extension/shipping/525k/api.php';
        $this->api = new \GS525K\Api($this->config->get('shipping_525k_client_id'), $this->config->get('shipping_525k_client_secret'), $this->config->get('shipping_525k_api_key'), $this->config->get('shipping_525k_test'));
        $this->shipper_id = $this->config->get('shipping_525k_shipper_id');

    }

    public function cancel() {

        $this->language->load('extension/shipping/525k');
        $this->load->model('extension/shipping/525k/525k');

        $order_id = $this->request->get['order_id'];
        $waybill = $this->model_extension_shipping_525k_525k->getWaybill($order_id);

        if($waybill) {

            $prepare_data = [
                'profileId' => $this->shipper_id,
                'waybill' => $waybill
            ];

            $json = $this->api->cancelOrder($prepare_data);
            if($json['response'] == 'Success') {
    
                $this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '7', date_modified = NOW() WHERE order_id = '" . (int) $order_id . "'");
                $shipmenthistory = $this->language->get('text_shipment_cancelled');
        
                $message = array(
                    'notify' => 0,
                    'comment' => $shipmenthistory
                );
        
                $this->model_extension_shipping_525k_525k->addOrderHistory($order_id, $message);

            }
    
        } else {
            $json = ["error" => null];
        }

        $this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
