<?php
class ControllerExtensionShippingSaee extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/shipping/saee');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('shipping_saee', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=shipping', true));
		}

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['api_key'])) {
			$data['error_api_key'] = $this->error['api_key'];
		} else {
			$data['error_api_key'] = '';
		}

		if (isset($this->error['origin_name'])) {
			$data['error_origin_name'] = $this->error['origin_name'];
		} else {
			$data['error_origin_name'] = '';
		}

		if (isset($this->error['origin_phone'])) {
			$data['error_origin_phone'] = $this->error['origin_phone'];
		} else {
			$data['error_origin_phone'] = '';
		}

		if (isset($this->error['origin_email'])) {
			$data['error_origin_email'] = $this->error['origin_email'];
		} else {
			$data['error_origin_email'] = '';
		}

		if (isset($this->error['origin_address'])) {
			$data['error_origin_address'] = $this->error['origin_address'];
		} else {
			$data['error_origin_address'] = '';
		}
		
		if (isset($this->error['origin_city'])) {
			$data['error_origin_city'] = $this->error['origin_city'];
		} else {
			$data['error_origin_city'] = '';
		}

		if (isset($this->error['origin_state'])) {
			$data['error_origin_state'] = $this->error['origin_state'];
		} else {
			$data['error_origin_state'] = '';
		}

		if (isset($this->error['origin_country'])) {
			$data['error_origin_country'] = $this->error['origin_country'];
		} else {
			$data['error_origin_country'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=shipping', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/shipping/saee', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['action'] = $this->url->link('extension/shipping/saee', 'user_token=' . $this->session->data['user_token'], true);
		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=shipping', true);

		if (isset($this->request->post['shipping_saee_api_key'])) {
			$data['shipping_saee_api_key'] = $this->request->post['shipping_saee_api_key'];
		} else {
			$data['shipping_saee_api_key'] = $this->config->get('shipping_saee_api_key');
		}

		if (isset($this->request->post['shipping_saee_test'])) {
			$data['shipping_saee_test'] = $this->request->post['shipping_saee_test'];
		} else {
			$data['shipping_saee_test'] = $this->config->get('shipping_saee_test');
		}

		if (isset($this->request->post['shipping_saee_cashonpickup'])) {
			$data['shipping_saee_cashonpickup'] = $this->request->post['shipping_saee_cashonpickup'];
		} else {
			$data['shipping_saee_cashonpickup'] = $this->config->get('shipping_saee_cashonpickup');
		}
		
		if (isset($this->request->post['shipping_saee_pickup_address_id'])) {
			$data['shipping_saee_pickup_address_id'] = $this->request->post['shipping_saee_pickup_address_id'];
		} else {
			$data['shipping_saee_pickup_address_id'] = $this->config->get('shipping_saee_pickup_address_id');
		}

		if (isset($this->request->post['shipping_saee_pickup_address_code'])) {
			$data['shipping_saee_pickup_address_code'] = $this->request->post['shipping_saee_pickup_address_code'];
		} else {
			$data['shipping_saee_pickup_address_code'] = $this->config->get('shipping_saee_pickup_address_code');
		}

		// if (isset($this->request->post['shipping_saee_origin_name'])) {
		// 	$data['shipping_saee_origin_name'] = $this->request->post['shipping_saee_origin_name'];
		// } else {
		// 	$data['shipping_saee_origin_name'] = $this->config->get('shipping_saee_origin_name');
		// }

		// if (isset($this->request->post['shipping_saee_origin_phone'])) {
		// 	$data['shipping_saee_origin_phone'] = $this->request->post['shipping_saee_origin_phone'];
		// } else {
		// 	$data['shipping_saee_origin_phone'] = $this->config->get('shipping_saee_origin_phone');
		// }

		// if (isset($this->request->post['shipping_saee_origin_phone2'])) {
		// 	$data['shipping_saee_origin_phone2'] = $this->request->post['shipping_saee_origin_phone2'];
		// } else {
		// 	$data['shipping_saee_origin_phone2'] = $this->config->get('shipping_saee_origin_phone2');
		// }

		// if (isset($this->request->post['shipping_saee_origin_email'])) {
		// 	$data['shipping_saee_origin_email'] = $this->request->post['shipping_saee_origin_email'];
		// } else {
		// 	$data['shipping_saee_origin_email'] = $this->config->get('shipping_saee_origin_email');
		// }

		if (isset($this->request->post['shipping_saee_origin_gps_latitude'])) {
			$data['shipping_saee_origin_gps_latitude'] = $this->request->post['shipping_saee_origin_gps_latitude'];
		} else {
			$data['shipping_saee_origin_gps_latitude'] = $this->config->get('shipping_saee_origin_gps_latitude');
		}

		if (isset($this->request->post['shipping_saee_origin_gps_longitude'])) {
			$data['shipping_saee_origin_gps_longitude'] = $this->request->post['shipping_saee_origin_gps_longitude'];
		} else {
			$data['shipping_saee_origin_gps_longitude'] = $this->config->get('shipping_saee_origin_gps_longitude');
		}

		if (isset($this->request->post['shipping_saee_origin_address'])) {
			$data['shipping_saee_origin_address'] = $this->request->post['shipping_saee_origin_address'];
		} else {
			$data['shipping_saee_origin_address'] = $this->config->get('shipping_saee_origin_address');
		}

		if (isset($this->request->post['shipping_saee_origin_address2'])) {
			$data['shipping_saee_origin_address2'] = $this->request->post['shipping_saee_origin_address2'];
		} else {
			$data['shipping_saee_origin_address2'] = $this->config->get('shipping_saee_origin_address2');
		}

		if (isset($this->request->post['shipping_saee_origin_district'])) {
			$data['shipping_saee_origin_district'] = $this->request->post['shipping_saee_origin_district'];
		} else {
			$data['shipping_saee_origin_district'] = $this->config->get('shipping_saee_origin_district');
		}

		if (isset($this->request->post['shipping_saee_origin_city'])) {
			$data['shipping_saee_origin_city'] = $this->request->post['shipping_saee_origin_city'];
		} else {
			$data['shipping_saee_origin_city'] = $this->config->get('shipping_saee_origin_city');
		}

		if (isset($this->request->post['shipping_saee_origin_state'])) {
			$data['shipping_saee_origin_state'] = $this->request->post['shipping_saee_origin_state'];
		} else {
			$data['shipping_saee_origin_state'] = $this->config->get('shipping_saee_origin_state');
		}

		if (isset($this->request->post['shipping_saee_origin_zipcode'])) {
			$data['shipping_saee_origin_zipcode'] = $this->request->post['shipping_saee_origin_zipcode'];
		} else {
			$data['shipping_saee_origin_zipcode'] = $this->config->get('shipping_saee_origin_zipcode');
		}

		if (isset($this->request->post['shipping_saee_origin_country'])) {
			$data['shipping_saee_origin_country'] = $this->request->post['shipping_saee_origin_country'];
		} else {
			$data['shipping_saee_origin_country'] = $this->config->get('shipping_saee_origin_country');
		}
		
		if (isset($this->request->post['shipping_saee_status'])) {
			$data['shipping_saee_status'] = $this->request->post['shipping_saee_status'];
		} else {
			$data['shipping_saee_status'] = $this->config->get('shipping_saee_status');
		}

		if (isset($this->request->post['shipping_saee_sort_order'])) {
			$data['shipping_saee_sort_order'] = $this->request->post['shipping_saee_sort_order'];
		} else {
			$data['shipping_saee_sort_order'] = $this->config->get('shipping_saee_sort_order');
		}

		if (isset($this->request->post['shipping_saee_debug'])) {
			$data['shipping_saee_debug'] = $this->request->post['shipping_saee_debug'];
		} else {
			$data['shipping_saee_debug'] = $this->config->get('shipping_saee_debug');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/shipping/saee', $data));
	
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/shipping/saee')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->request->post['shipping_saee_api_key']) {
			$this->error['api_key'] = $this->language->get('error_api_key');
		}

		// if (!$this->request->post['shipping_saee_origin_name']) {
		// 	$this->error['origin_name'] = $this->language->get('error_origin_name');
		// }

		// if (!$this->request->post['shipping_saee_origin_phone']) {
		// 	$this->error['origin_phone'] = $this->language->get('error_origin_phone');
		// }

		// if (!$this->request->post['shipping_saee_origin_email']) {
		// 	$this->error['origin_email'] = $this->language->get('error_origin_email');
		// }

		if (!$this->request->post['shipping_saee_origin_address']) {
			$this->error['origin_address'] = $this->language->get('error_origin_address');
		}

		if (!$this->request->post['shipping_saee_origin_city']) {
			$this->error['origin_city'] = $this->language->get('error_origin_city');
		}

		if (!$this->request->post['shipping_saee_origin_state']) {
			$this->error['origin_state'] = $this->language->get('error_origin_state');
		}

		if (!$this->request->post['shipping_saee_origin_country']) {
			$this->error['origin_country'] = $this->language->get('error_origin_country');
		}

		return !$this->error;
	}

}