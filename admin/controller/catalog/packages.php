<?php
class ControllerCatalogPackages extends Controller {
    public function index() {
        $this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."store_packages_orders` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `package_id` int(11) NOT NULL,
  `amount` float NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `inputs_data` longtext CHARACTER SET utf8,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;");
        $this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."store_packages` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `price` float NOT NULL,
  `inputs_data` text CHARACTER SET utf8,
  `recommended` int(11) NOT NULL,
  `sort_by` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;");

        $this->load->language('common/global');
        $this->document->setTitle($this->language->get('text_packages'));
        $data['user_token'] = $this->session->data['user_token'];
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_packages'),
            'href' => $this->url->link('catalog/packages/packages', 'user_token=' . $this->session->data['user_token'], true)
        );
        $result = $this->db->query("SELECT * FROM `".DB_PREFIX."store_packages` WHERE 1");
        foreach ($result->rows as $package) {
            $data['packages'][] = array(
                'id' => $package['id'],
                'title' => json_decode($package['title'],true)[$this->config->get('config_language_id')] ?? '',
                'description' => json_decode($package['description'],true)[$this->config->get('config_language_id')] ?? '',
                'recommended' => $package['recommended'],
                'status' => $package['status'],
                'edit' => $this->url->link('catalog/packages/edit', 'user_token=' . $this->session->data['user_token'] . '&id=' . $package['id'], true),
                'del' => $this->url->link('catalog/packages/_del', 'user_token=' . $this->session->data['user_token'] . '&id=' . $package['id'], true),
            );
        }

        $pagination = new Pagination();
        $pagination->total = $result->num_rows;
        $pagination->page = $page = $this->request->get['page'] ?? 1;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('catalog/packages', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
        $data['pagination'] = $pagination->render();
        $data['results'] = sprintf($this->language->get('text_pagination'), ($result->num_rows) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($result->num_rows - $this->config->get('config_limit_admin'))) ? $result->num_rows : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $result->num_rows, ceil($result->num_rows / $this->config->get('config_limit_admin')));

        $data['add'] = $this->url->link('catalog/packages/add', 'user_token=' . $this->session->data['user_token'], true);
        $data['orders'] = $this->url->link('catalog/packages/orders', 'user_token=' . $this->session->data['user_token'], true);
        $data['settings'] = $this->url->link('catalog/packages/settings', 'user_token=' . $this->session->data['user_token'], true);

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/packages/packages', $data));
    }

    public function orders() {
        $this->load->language('common/global');
        $this->document->setTitle($this->language->get('text_packages'));
        $data['user_token'] = $this->session->data['user_token'];
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_orders'),
            'href' => $this->url->link('catalog/packages/packages', 'user_token=' . $this->session->data['user_token'], true)
        );
        $result = $this->db->query("SELECT * FROM `".DB_PREFIX."store_packages_orders` WHERE 1");
        foreach ($result->rows as $package) {
            $data['packages'][] = array(
                'id' => $package['id'],
                'title' => json_decode($this->db->query("SELECT * FROM `".DB_PREFIX."store_packages` WHERE `id` = '".$package['package_id']."'")->row['title'],true)[$this->config->get('config_language_id')],
                'amount' => $this->currency->format($package['amount'],$this->config->get('config_currency'),'', true),
                'customer_id' => $package['customer_id'],
                'created_at' => $package['created_at'],
                'status' => $package['status'],
            );
        }


        $pagination = new Pagination();
        $pagination->total = $result->num_rows;
        $pagination->page = $page = $this->request->get['page'] ?? 1;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('catalog/packages', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);
        $data['pagination'] = $pagination->render();
        $data['results'] = sprintf($this->language->get('text_pagination'), ($result->num_rows) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($result->num_rows - $this->config->get('config_limit_admin'))) ? $result->num_rows : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $result->num_rows, ceil($result->num_rows / $this->config->get('config_limit_admin')));

        $data['add'] = $this->url->link('catalog/packages/add', 'user_token=' . $this->session->data['user_token'], true);
        $data['packages_link'] = $this->url->link('catalog/packages', 'user_token=' . $this->session->data['user_token'], true);
        $data['settings'] = $this->url->link('catalog/packages/settings', 'user_token=' . $this->session->data['user_token'], true);

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/packages/packages_orders', $data));
    }

    public function show() {
        if(isset($this->request->get['id']) && $package = $this->db->query("SELECT * FROM `".DB_PREFIX."store_packages_orders` WHERE `id`='".$this->request->get['id']."'")->row) {
            $this->load->language('common/global');
            $this->document->setTitle($this->language->get('text_packages'));
            $data['user_token'] = $this->session->data['user_token'];
            $data['breadcrumbs'] = array();
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
            );
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_orders'),
                'href' => $this->url->link('catalog/packages/packages', 'user_token=' . $this->session->data['user_token'], true)
            );
            $data['package'] = [
                'id' => $package['id'],
                'title' => json_decode($this->db->query("SELECT * FROM `" . DB_PREFIX . "store_packages` WHERE `id` = '" . $package['package_id'] . "'")->row['title'], true)[$this->config->get('config_language_id')],
                'amount' => $this->currency->format($package['amount'], $this->config->get('config_currency'), '', true),
                'customer_id' => $package['customer_id'],
                'inputs' => json_decode($package['inputs_data'],true),
                'created_at' => $package['created_at'],
                'status' => $package['status'],
            ];

            $data['add'] = $this->url->link('catalog/packages/add', 'user_token=' . $this->session->data['user_token'], true);
            $data['packages_link'] = $this->url->link('catalog/packages', 'user_token=' . $this->session->data['user_token'], true);
            $data['settings'] = $this->url->link('catalog/packages/settings', 'user_token=' . $this->session->data['user_token'], true);

            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');

            $this->response->setOutput($this->load->view('catalog/packages/packages_show', $data));
        }else{
            $this->response->redirect($this->url->link('catalog/packages/orders', 'user_token=' . $this->session->data['user_token'], true));
        }
    }

    public function add() {
        $this->load->language('common/global');
        $this->document->setTitle($this->language->get('text_add_package'));
        $data['user_token'] = $this->session->data['user_token'];
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_packages'),
            'href' => $this->url->link('catalog/packages', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_add_package'),
            'href' => $this->url->link('catalog/packages/add', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/packages/add_packages', $data));
    }

    public function settings() {
        $this->load->language('common/global');
        $this->document->setTitle($this->language->get('text_add_package'));
        $data['user_token'] = $this->session->data['user_token'];
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_packages'),
            'href' => $this->url->link('catalog/packages', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_settings'),
            'href' => $this->url->link('catalog/packages/settings', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['package_status'] = $this->config->get('config_packages_status') ?? 0;
        $data['package_title'] = $this->config->get('config_packages_title') ?? '';
        $data['package_description'] = $this->config->get('config_packages_description') ?? '';

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/packages/packages_settings', $data));
    }

    public function _d(){
        if(isset($this->session->data['user_token']) && isset($this->request->get['user_token']) && $this->session->data['user_token'] == $this->request->get['user_token']){
            $this->load->language('common/global');
            $title = array_map(function($v){
                return trim(strip_tags($v));
            }, $this->request->post['title']);
            $description = array_map(function($v){
                return trim(strip_tags($v));
            }, $this->request->post['des']);
            $inputs_data = array_map(function($v){
                return trim(strip_tags($v));
            }, $this->request->post['inputs']) ?? [];
            $recommended = $this->request->post['recommended'] ?? 0;
            $sort_by = $this->request->post['sort_by'] ?? 0;
            $price = $this->request->post['price'] ?? 0.00;
            $status = $this->request->post['status'] ?? 0;

            $data_ar = $this->db->query("INSERT INTO `".DB_PREFIX."store_packages` (`id`, `title`, `description`, `price`, `recommended`, `sort_by`, `status`, `created_at`,`inputs_data`) VALUES (NULL, '". $this->db->escape(json_encode($title))."', '". $this->db->escape(json_encode($description))."', '". (float)$price."', '".(int)$recommended."','".(int)$sort_by."', '".(int)$status."', '".date("Y-m-s H:i:s")."', '".$this->db->escape(json_encode($inputs_data))."');");
            if($data_ar == 1){
                echo '<script>Swal.fire("'.$this->language->get('text_done_add_package').'", "", "success");setTimeout(function(){location.href = "index.php?route=catalog/packages&user_token='.$this->session->data['user_token'].'";},1500)</script>';
            }
        }
    }

    public function _del(){
        if (isset($this->request->get['id']) && isset($this->session->data['user_token']) && isset($this->request->get['user_token']) && $this->session->data['user_token'] == $this->request->get['user_token']) {
            if($this->db->query("DELETE FROM `".DB_PREFIX."store_packages` WHERE `id` = '".(int)$this->request->get['id']."'")){
                $this->response->redirect($this->url->link('catalog/packages', 'user_token=' . $this->session->data['user_token'], true));
            }
        }
    }

    public function _s(){
        if(isset($this->session->data['user_token']) && isset($this->request->get['user_token'])){
            $this->load->language('common/global');
            $status = (int)$this->request->post['status'] ?? 0;
            $title = array_map(function($v){
                return trim(strip_tags($v));
            }, $this->request->post['title']);
            $description = array_map(function($v){
                return trim(strip_tags($v));
            }, $this->request->post['des']);
            $this->db->query("DELETE FROM `".DB_PREFIX."setting` WHERE store_id = '0' AND `code` = 'config_packages'");
            $this->db->query("INSERT INTO `".DB_PREFIX."setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES (NULL, '0', 'config_packages', 'config_packages_status', '".$status."', '0')");
            $this->db->query("INSERT INTO `".DB_PREFIX."setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES (NULL, '0', 'config_packages', 'config_packages_title', '".$this->db->escape(json_encode($title))."', '1')");
            $this->db->query("INSERT INTO `".DB_PREFIX."setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES (NULL, '0', 'config_packages', 'config_packages_description', '".$this->db->escape(json_encode($description))."', '1')");
            echo '<script>Swal.fire("'.$this->language->get('text_done_setting').'", "", "success");</script>';
        }
    }
}
