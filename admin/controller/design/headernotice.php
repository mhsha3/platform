<?php
class ControllerDesignHeadernotice extends Controller{
    public function index(){
        $this->load->language('common/global');
        $this->document->setTitle($this->language->get('text_header_notice'));
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_header_notice'),
            'href' => $this->url->link('design/headernotice', 'user_token=' . $this->session->data['user_token'], true)
        );

        $header_noteice = $this->config->get('theme_header_notice');
        $data['header_status'] = $header_noteice['status'] ?? 1;
        $data['header_arabic'] = $header_noteice['text_arabic'] ?? '';
        $data['header_english'] = $header_noteice['text_english'] ?? '';
        $data['header_link'] = $header_noteice['link'] ?? '';

        $data['user_token'] = $this->session->data['user_token'];
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $this->response->setOutput($this->load->view('design/header_notice', $data));
    }
    public function add(){
        $this->load->language('common/global');
        if( isset($this->request->post['text_arabic']) && isset($this->request->post['text_english']) &&
            isset($this->request->post['link']) && isset($this->request->post['status']) ){
            $header_data = [
                'text_arabic'=>$this->request->post['text_arabic'],
                'text_english'=>$this->request->post['text_english'],
                'link'=>$this->request->post['link'],
                'status'=>$this->request->post['status']
            ];
            if($this->config->has('theme_header_notice')){
                $this->load->model('setting/setting');
                $this->model_setting_setting->editSettingValue('theme_header_notice','theme_header_notice',$header_data);
            }else{
                $this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `code` = 'theme_header_notice', `key` = 'theme_header_notice', `value` = '" . $this->db->escape(json_encode($header_data, true)) . "', serialized = '1'");
            }
            echo '<script>Swal.fire("'.$this->language->get('text_done_add_header_notice').'", "", "success");</script>';
        }else{
            echo '<script>Swal.fire("'.$this->language->get('text_please_enter_all_data').'", "", "error");</script>';
        }
    }
}
