<?php 

class ControllerSaleSmsa extends Controller {
    public $_apiUrl = "http://track.smsaexpress.com/SECOM/SMSAwebService.asmx";
    
    public function index() {
        $this->load->model('sale/order');

        if($this->config->get('smsa_status')){

            $this->load->language('sale/smsa');
            $this->load->model('sale/smsa');

            if (isset($this->request->get['order_id']) && $this->request->get['order_id']) {
                $order_id = $this->request->get['order_id'];
            } else {
                $order_id = 0;
            }

            $results = $this->model_sale_smsa->getConsignment($order_id);
            $data['consignment'] = array();
            
            $data['awb_btn'] = 1;
            $data['awb_number'] = false;
            $track = false;
            if($results){
                $array = current($results);
                if($array['status'] == 'addShip'){
                    $data['awb_btn'] = 0;
                    $data['awb_number'] = $array['awb_number'];
                    $track = $this->url->link('sale/smsa/trackShipment', 'user_token=' . $this->session->data['user_token'] . '&awb_number=' . $data['awb_number'], true);
                }
            }

            if($track){
                $data['column_status'] = '<a class="agree btn-info" href="'.$track.'" data-toggle="tooltip" title="Track live status" class="btn btn-link">&nbsp;<i class="fa fa-hand-o-right"></i> SMSA Live Staus&nbsp;</a>';
            }

            foreach ($results as $result) {
                
                if($result['shipment_label']){
                    $shipment_label = HTTP_CATALOG.'awb/'.$result['shipment_label'];
                } else {
                    $shipment_label = false; 
                }
               $data['consignment'][] = array(
                'awb_number'  => $result['awb_number'],
                'shipment_label'  => $shipment_label,
                'reference_number'  => $result['reference_number'],
                'status'  => $result['status'],
                'pickup_date'  => $result['pickup_date'],
                'date_added'  => $result['date_added'],
                //'track'       => 'http://www.smsaexpress.com/Track.aspx?tracknumbers='. $result['awb_number'],
                'track'       =>$this->url->link('sale/smsa/trackShipment', 'user_token=' . $this->session->data['user_token'] . '&awb_number=' . $result['awb_number'], true),
                );
            }

            $data['user_token'] = $this->session->data['user_token'];
            $data['order_id'] = $order_id;  

            $this->response->setOutput($this->load->view('sale/order_smsa', $data));
        }
    }
    
    public function addShip(){
        $json = array();
        $this->load->language('sale/smsa');

        if (!$this->user->hasPermission('modify', 'sale/smsa')) {
            $json['error'] = $this->language->get('error_permission');
        }

        if(!$this->config->get('smsa_status')){
            $json ['error'] = $this->language->get('error_smsa_status');
        }

        if (!isset($this->request->get['order_id']) || !$this->request->get['order_id']) {
            $json ['error'] = $this->language->get('error_order_id');
        }

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {

            // if (empty($this->request->post['pickup_datetime'])) {
            //     $json['error'] = $this->language->get('error_date');
            // }

            // if(!isset($this->request->post['notify'])){
            //     $json['error'] = $this->language->get('error_notify');
            // }          

        }

        if(!$json) {
            $this->load->model('sale/order');           
            $order_info = $this->model_sale_order->getOrder($this->request->get['order_id']);

            //echo "<pre>";print_r($order_info);
            
            if($order_info){
                
                $order_products = $this->model_sale_order->getOrderProducts($this->request->get['order_id']);
                foreach ($order_products as $product) {
                     $itemDesc[] = $product['name'];
                }
                $codAmt = 0;
                $weight = '.5';
                if(in_array($order_info['payment_code'], $this->config->get('smsa_cod_method'))){
                    $codAmt = $order_info['total'];
                }

                if (!empty($this->request->post['shipping_city'])) {
                    $order_info['shipping_city'] = $this->request->post['shipping_city'];
                }

                $weight = 1;
                if (!empty($this->request->post['weight'])) {
                    $weight = (float)$this->request->post['weight'];
                }

                $refNo = time();
                $args = array(
                    'passKey'       => $this->config->get('smsa_passkey'),
                    'refNo'         => $refNo,
                    'sentDate'      => date('Y/m/d'),
                    'idNo'          => $this->request->get['order_id'],
                    'cName'         => $order_info['shipping_firstname'].' '.$order_info['shipping_lastname'],
                    'cntry'         => 'KSA',
                    'cCity'         => $order_info['shipping_city'],
                    'cZip'          => $order_info['shipping_postcode'],
                    'cPOBox'        => '',
                    'cMobile'       => $order_info['telephone'],
                    'cTel1'         => '',
                    'cTel2'         => '',
                    'cAddr1'        => $order_info['shipping_address_1'],
                    'cAddr2'        => $order_info['shipping_address_2'],
                    'shipType'      => 'DLV',
                    'PCs'           => count($order_products),
                    'cEmail'        => $order_info['email'],
                    'carrValue'     => $order_info['total'],
                    'carrCurr'      => $order_info['currency_code'],
                    'codAmt'        => $codAmt,
                    'weight'        => $weight,
                    'custVal'       => '',
                    'custCurr'      => '',
                    'insrAmt'       => '',
                    'insrCurr'      => '',
                    'itemDesc'      => implode(", ",$itemDesc),
                    'sName'         => $this->config->get('smsa_name'),
                    'sContact'      => $this->config->get('smsa_contact'),
                    'sAddr1'        => $this->config->get('smsa_address_1'),
                    'sAddr2'        => $this->config->get('smsa_address_2'),
                    'sCity'         => $this->config->get('smsa_city'),
                    'sPhone'        => $this->config->get('smsa_telephone'),
                    'sCntry'        => $this->config->get('smsa_country'),
                    'prefDelvDate'  => '',
                    'gpsPoints'     => ''
                );

                $xml = $this->createXml('http://track.smsaexpress.com/secom/addShip', 'addShip', $args);
                $result = $this->send($xml);

                $response = $result['soapBody']['addShipResponse']['addShipResult'];
                if(is_numeric($response)){
                    $param = array(
                        'awbNo'  =>$response,
                        'passKey'=>$this->config->get('smsa_passkey'),
                    );
                    $PDF = $this->createXml('http://track.smsaexpress.com/secom/getPDF', 'getPDF', $param);
                    $resultPDF = $this->send($PDF);

                    $filename = $response.'.pdf';
                    $file_path = str_replace('image', 'awb', DIR_IMAGE).$filename;
                    $content =base64_decode($resultPDF['soapBody']['getPDFResponse']['getPDFResult']);
                    $fp = fopen($file_path,"wb");
                    fwrite($fp,$content);
                    fclose($fp);

                    $save_data = array(
                        'order_id'  => $this->request->get['order_id'],
                        'awb_number'    => $response,
                        'reference_number'  => $refNo,
                        'pickup_date' => '',
                        'shipment_label' => $filename,
                        'status' => 'addShip'
                    );

                    $this->load->model('sale/smsa');
                    $this->model_sale_smsa->addConsignment($save_data);
                    
                    $json['success'] = $this->language->get('txt_success').' - '.$response;
                }else{
                    $json['error'] = $response;
                }
            } else {
                $json['error'] = $this->language->get('error_order');
            }
        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));       

    }
    
    public function cancelShipment(){
        $json = array();
        $this->load->language('sale/smsa');

        if (!$this->user->hasPermission('modify', 'sale/smsa')) {
            $json['error'] = $this->language->get('error_permission');
        }

        if(!$this->config->get('smsa_status')){
            $json ['error'] = $this->language->get('error_smsa_status');
        }

        if (!isset($this->request->get['order_id']) || !$this->request->get['order_id']) {
            $json ['error'] = $this->language->get('error_order_id');
        }

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {

            if (empty($this->request->post['reason'])) {
                $json['error'] = $this->language->get('error_reason');
            }

            if(!isset($this->request->post['awb_number'])){
                $json['error'] = $this->language->get('error_awb_number');
            }          

        }

        if(!$json) {
            $args = array(
                'awbNo'     => $this->request->post['awb_number'],
                'passkey'   => $this->config->get('smsa_passkey'),
                'reas'      => $this->request->post['reason']
            );
        
            $xml = $this->createXml('http://track.smsaexpress.com/secom/cancelShipment', 'cancelShipment', $args);
            $result = $this->send($xml);
            if (strpos($result['soapBody']['cancelShipmentResponse']['cancelShipmentResult'], 'Success') !== false) {
                $save_data = array(
                    'order_id'  => $this->request->get['order_id'],
                    'awb_number'    => $this->request->post['awb_number'],
                    'reference_number'  =>'',
                    'pickup_date' => '',
                    'shipment_label' => '',
                    'status' => 'cancelShipment'
                );
                $this->load->model('sale/smsa');
                $this->model_sale_smsa->addConsignment($save_data);        
                $json['success'] = $result['soapBody']['cancelShipmentResponse']['cancelShipmentResult'];
            } else {
                $json['error'] = $result['soapBody']['cancelShipmentResponse']['cancelShipmentResult'];
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
       
    }

    public function trackShipment(){
        $this->load->language('account/smsa');

        $data['result'] = array();

        if($this->config->get('smsa_status') && (isset($this->request->get['awb_number']) || $this->request->get['awb_number'])){
            $args = array(
                'awbNo'     => $this->request->get['awb_number'],
                'passkey'   => $this->config->get('smsa_passkey'),
            );

            $xml = $this->createXml('http://track.smsaexpress.com/secom/getTracking', 'getTracking', $args);

            $result = $this->send($xml);

            if(isset($result['soapBody']['getTrackingResponse']['getTrackingResult']['diffgrdiffgram']) && $result['soapBody']['getTrackingResponse']['getTrackingResult']['diffgrdiffgram']){
                $response = $result['soapBody']['getTrackingResponse']['getTrackingResult']['diffgrdiffgram'];
                
                if(isset($response['NewDataSet']['Tracking'][0])){
                    $data['awbNo'] = $response['NewDataSet']['Tracking'][0]['awbNo'];
                    $data['status'] = $response['NewDataSet']['Tracking'][0]['Activity'];
                    unset($response['NewDataSet']['Tracking'][0]);
                    $data['result'] = $response['NewDataSet']['Tracking']; 
                } else {
                    $data['awbNo'] = $response['NewDataSet']['Tracking']['awbNo'];
                    $data['status'] = $response['NewDataSet']['Tracking']['Activity'];
                    $data['result'][] = $response['NewDataSet']['Tracking'];
                }
               
            }
        }

        $this->response->setOutput($this->load->view('sale/order_smsa_track', $data));
       
    }
    
    public function createXml($SOAPAction, $method, $variables){
        $xmlcontent = '<?xml version="1.0" encoding="utf-8"?>
            <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
              <soap:Body>
                <'.$method.' xmlns="http://track.smsaexpress.com/secom/">';
                  if(count($variables)){
                    foreach($variables As $key=>$val){
                        $xmlcontent .= '<'.$key.'>'.$val.'</'.$key.'>';
                    }
                  }
        $xmlcontent .= '</'.$method.'>
              </soap:Body>
            </soap:Envelope>';
            
        $headers = array(
            "POST /SECOM/SMSAwebService.asmx HTTP/1.1",
            "Host: track.smsaexpress.com",
            "Content-Type: text/xml; charset=utf-8",
            "Content-Length: ".strlen($xmlcontent),
            "SOAPAction: ".$SOAPAction
        );
        
        return array(
            'xml'       => $xmlcontent,
            'header'    => $headers
        );
    }
    
    public function send($arg=array()){
        $ch = curl_init();
        //curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $arg['header']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_URL, $this->_apiUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $arg['xml']);
        $content=curl_exec($ch);
        
        $response = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $content);
        $xml = new SimpleXMLElement($response);
        $array = json_decode(json_encode((array)$xml), TRUE);
        return $array;
    }
}