<?php
$_['heading_title']       = 'البنرات المتحركة';
$_['text_extension']      = 'الاضافات';
$_['text_success']        = 'تم التعديل!';
$_['text_edit']           = 'تحرير';
$_['entry_name']          = 'اسم الاضافة';
$_['entry_banner']        = 'بنر';
$_['entry_width']         = 'العرض';
$_['entry_height']        = 'الارتفاع';
$_['entry_status']        = 'الحالة';
$_['error_permission']    = 'تحذير: أنت لا تمتلك صلاحيات التعديل !';
$_['error_name']       = 'اسم الاضافة يجب ان يكون بين 3 و 64 حرف !';
$_['error_width']      = 'العرض مطلوب !';
$_['error_height']     = 'الارتفاع مطلوب !';
