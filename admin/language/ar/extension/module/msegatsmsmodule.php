<?php
// Heading
$_['heading_title']    = 'Msegatsms';

// Text
$_['text_module']      = 'الخدمات';
$_['text_success']     = 'تم التعديل بنجاح على خدمة مسجات';
$_['text_edit']        = 'التعديل على مسجات';

// Entry
$_['entry_name']       = 'اسم الخدمة';
$_['entry_status']     = 'الحالة';

// Error
$_['error_permission'] = 'تحذير: ليس لديك إذن بتعديل خدمة Msegatsms!';
$_['error_name']       = 'يجب أن يتراوح اسم الوحدة النمطية بين 3 و 64 حرفًا!';
