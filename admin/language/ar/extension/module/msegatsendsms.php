<?php
// Heading
$_['heading_title']       = 'ارسال رسالة نصية';

// Text
$_['text_module']          = 'الخدمات';
$_['text_success_sent']    = 'نجاح: تم إرسال الرسالة!';
$_['text_error']        	 = 'فشل: لم يتم إرسال الرسالة! (رسالة خطأ: ';
$_['text_none']      			 = '-- حدد وجهتك --';
$_['text_nohp']      			 = 'رقم الادخال';
$_['text_newsletter']      = 'جميع المشتركين في النشرة الإخبارية';
$_['text_customer_all']    = 'كل العملاء';
$_['text_customer_group']  = 'مجموعة العملاء';
$_['text_customer']        = 'العملاء';
$_['text_affiliate_all']   = 'جميع المسوقين بالعمولة';
$_['text_affiliate']       = 'الشركات التابعة';
$_['text_product']         = 'منتجات';

$_['button_save']          = 'أرسل رسالة';
$_['cancel']          = 'الغاء';
// Entry
$_['entry_nohp']    			 = 'رقم الجوال';
$_['entry_message']    		 = 'الرسالة:';
$_['entry_or']    				 = 'الى';
$_['entry_customer_group'] = 'مجموعة العملاء';
$_['entry_customer']       = 'الزبون';
$_['entry_affiliate']      = 'شركة تابعة';
$_['entry_product']        = 'المنتجات';

// Button
$_['button_send']					 = 'أرسل رسالة';
$_['button_setting']			 = 'إعداد Msegat';

// Error
$_['error_permission']     = 'تحذير: ليس لديك إذن بتعديل خدمة مسجات!';
$_['error_nohp']       		 = 'رقم الوجهة مطلوب';
$_['error_message']        = 'مطلوب رسالة نصية';
$_['error_gateway_null']   = 'البوابة غير موجودة! يرجى تعيين البوابة الافتراضية';

?>