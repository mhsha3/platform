<?php
// Heading
$_['heading_title']       		= 'إعدادات مسجات';
$_['intruction_title']    		= 'تعليمات';

// Tab
$_['tab_settings']                  = 'الاعدادات';
$_['tab_order_status_sms']          = 'حالة الطلب SMS';
$_['tab_order_sms']                 = 'أوامر SMS';
$_['tab_accounts_sms']              = 'حسابات الرسائل القصيرة';

// Text
$_['text_module']         		= 'الخدمات';
$_['text_success']        		= 'نجاح: لقد قمت بتعديل خدمة Msegat!';
$_['text_content_top']    		= 'أعلى المحتوى';
$_['text_content_bottom'] 		= 'أسفل المحتوى';
$_['text_column_left']    		= 'العمود الأيسر';
$_['text_column_right']   		= 'العمود الأيمن';
$_['text_yes']      			= 'نعم';
$_['text_no']      			    = 'لا';
$_['text_none']      			= '- اختر بوابة -';
$_['text_status_none']      	= '- اختر الحالة -';
$_['text_limit']      			= 'الرسائل القصيرة / اليوم';
$_['text_enable_verify']        = 'تمكين التحقق من الهاتف المحمول؟';
$_['text_enable_verify_span']   = 'الهاتف المحمول للتحقق عندما يطلب العملاء';

// Gateway
$_['entry_gateway']       		= 'Gateway:<br /><span class=\'help\'>Select gateway provider.<br />For the list of countries supported by gateways, you can visit the each gateway site.</span>';


// msegat
$_['entry_userkey_msegat']   = 'اسم المستخدم';
$_['entry_userkey_msegat_span']   = 'username for the account in <a href=\'https://msegat.com\' target=\'_blank\'>Msegat.com</a>';


$_['entry_passkey_msegat']   = 'API مفتاح';
$_['entry_passkey_msegat_span']   = 'userPassword for the account in <a href=\'https://msegat.com\' target=\'_blank\'>Msegat.com</a>';

$_['entry_function']   = 'وظيفة';
$_['entry_template']   = 'قالب';
$_['entry_enable']   = 'ممكن';

$_['entry_httpapi_msegat']   = 'HTTP API';
$_['entry_senderid_msegat']	 = 'اسم المرسل';
$_['entry_senderid_msegat_span']	 = ' اسم المرسل ، يجب تفعيله من <a href=\'https://msegat.com\' target=\'_blank\'>Msegat.com</a>';

$_['httpapi_example_msegat'] = '<span class=\'help\'><b>Use this for HTTP API-></b> msegat.com/gw/index.php</span>';


// Entry
$_['entry_layout']        		= 'نسق:';
$_['entry_position']      		= 'موضع:';
$_['entry_status']        		= 'الحالة:';
$_['entry_sort_order']    		= 'امر ترتيب:';
$_['entry_nohp']    			= 'رقم الهاتف المحمول:';
$_['entry_message']    			= 'رسالة:';
$_['entry_smslimit']    		= 'حد الرسائل القصيرة';
$_['entry_smslimit_span']    	= 'يمكنك تحديد الرسالة التي يمكنها إرسال الزائر ليوم واحد. اتركه فارغا لتعطيل الحد.';


$_['entry_alert_reg']   			= 'تسجيل تنبيه الرسائل';
$_['entry_alert_reg_span']   		= ' يمكنك إدراج ملف <i>{firstname}</i>, <i>{lastname}</i>, <i>{email}</i>, <i>{password}</i> and <i>{storename}</i> in the message. <b>eg</b>: <i>Thanks for sign up {firstname} {lastname}. Your login email: {email} and password: {password}</i>';

 
 
$_['entry_alert_order']   		= 'وضع تنبيه رسالة الطلب';
$_['entry_alert_order_span']   		= 'يمكنك إدراج ملف<i>{firstname}</i>, <i>{lastname}</i>, <i>{email}</i>, <i>{orderid}</i>, <i>{orderstatus}</i>, <i>{shippingmethod}</i>, <i>{paymentmethod}</i>, <i>{total}</i>, <i>{storename}</i> in the message.';



$_['entry_alert_changestate']      = 'تنبيه الطلب';
$_['entry_alert_changestate_span'] = 'يمكنك إدراج ملف <i>{firstname}</i>, <i>{lastname}</i>, <i>{orderid}</i>, <i>{orderstatus}</i>, <i>{shippingmethod}</i>, <i>{invoiceno}</i>, <i>{total}</i>, <i>{storename}</i> in the message.';

$_['entry_additional_alert']     	= 'رسائل تنبيه SMS إضافية';
$_['entry_additional_alert_span']	= 'أي رقم هاتف محمول إضافي للمسؤول (مع رمز الدولة) تريد تلقي رسائل SMS للتسجيل وتنبيه الطلب. (مفصولة بفواصل). على سبيل المثال: 1803015xxx ، 4475258xxx ، 6285220xxx';

$_['entry_alert_sms']    			= 'رسالة تنبيه بالطلب الجديد';
$_['entry_alert_sms_span']    		= 'أرسل رسالة نصية قصيرة إلى صاحب المتجر عند إنشاء طلب جديد. يمكنك إدراج ملف <i>{firstname}</i>, <i>{lastname}</i>, <i>{email}</i>, <i>{orderid}</i>, <i>{orderstatus}</i> and etc (<strong>Variable accepted</strong>) in the message.';

$_['entry_account_sms']  			= 'الرسائل القصيرة الجديدة لتنبيه الحساب';
$_['entry_account_sms_span']  		= 'إرسال رسالة نصية قصيرة إلى صاحب المتجر عند تسجيل حساب جديد. يمكنك إدراج ملف<i>{firstname}</i>, <i>{lastname}</i>, <i>{email}</i> and <i>{storenamفى الرسالةmessage.';



$_['entry_alert_blank']    		= '<span class=\'help\'>* اتركه فارغا لتعطيل هذا التنبيه</span>';
$_['entry_status_order_alert']= 'تغيير تنبيه طلب الحالة';
$_['entry_status_order_alert_span']= 'حدد حالة الطلب';


$_['entry_verify_code']				= 'رسالة رمز التحقق';
$_['entry_verify_code_span']		= ' Add <i>{code}</i>لمتغير رمز التحقق في الرسالة.<على سبيل المثال: رمز التحقق الخاص بكe is: {code}';

 
 
$_['entry_skip_group']				= 'تخطي التحقق للمجموعات';
$_['entry_skip_group_span']				= 'لن يطلب العميل الموجود في المجموعة المحددة التحقق من رقم الهاتف المحمول.';



$_['entry_skip_group_help']   = '<span class=\'help\'>اضغط باستمرار على مفتاح بديل للاختيار المتعدد.</span>';
$_['entry_code_digit']   			= 'طول رقم رمز التحقق:';
$_['entry_code_digit_span']   			= 'طول رقم رمز التحقق';

$_['entry_max_retry']   			= 'الحد الأقصى لإعادة المحاولة';
$_['entry_max_retry_span']   			= 'الحد الأقصى لعدد العملاء يمكنهم إعادة إرسال رمز التحقق.';



$_['entry_limit_blank']    		= '<span class=\'help\'>Leave blank to disable limit.</span>';
$_['entry_parsing']    				= '<span class=\'help\'><strong>متغير :</strong> {order_date},{products_ordered},{firstname},{lastname},{email},{telephone},{orderid},{orderstatus},{shippingmethod},{shipping_firstname},{shipping_lastname},{shipping_company},{shipping_address},{shipping_city},{shipping_postcode},{shipping_state},{shipping_country},{paymentmethod},{payment_firstname},{payment_lastname},{payment_company},{payment_address},{payment_city},{payment_postcode},{payment_state},{payment_country},{total},{comment},{storename}</span>';
$_['entry_edit']    				= 'تعديل';

// Button
$_['button_save']							= 'حفظ';
$_['button_cancel']						= 'الغاء';
$_['button_add_module']				= 'Add Module';
$_['button_remove']						= 'خذف';

// Error
$_['error_permission']    		= 'تحذير: ليس لديك إذن بتعديل الوحدة النمطية Msegat!';
$_['error_gateway']       		= 'بوابة مطلوبة';
$_['error_userkey']       		= 'هذا الحقل مطلوب';
$_['error_passkey']       		= 'هذا الحقل مطلوب';
$_['error_httpapi']       		= 'هذا الحقل مطلوب';
$_['error_senderid']       		= 'هذا الحقل مطلوب';
$_['error_apiid']       		= 'API مطلوب';
$_['error_nohp']       			= 'رقم الهاتف مطلوب';
$_['error_message']       		= 'مطلوب رسالة نصية';
$_['error_gateway_null']   		= 'البوابة غير موجودة! يرجى تعيين البوابة الافتراضية';

// Anounce
$_['text_instruction']				= '<span class=\'help\'>You can add <b>Msegat OpenCart</b> module to enable your web visitor can Sending Message for free from your web. This can increase visitor traffic of your web.<br />Click <b>Add Module</b> button and set the configuration of module.<br/><br/><b>Layout</b>: On the page where the module will be displayed.<br/><b>Position</b>: Position of module.<br/><b>Status</b>: You can enable or disable the module.<br/><b>Sort Order</b>: Order position of the module.</span>';
?>