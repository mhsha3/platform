<?php
// Heading
$_['heading_title']                    = 'Clex Shipping';

// نص
$_['text_back_to_order'] = 'العودة للطلب';
$_['text_print_label'] = 'طباعة الملصق';
$_['text_track'] = 'تتبع الشحن';
$_['text_cancel_sipment'] = "إلغاء الشحن";
$_['text_error_heading'] = "خطأ" ;
$_['text_shipment_details'] = "تفاصيل الشحن";
$_['text_sender_details'] = "تفاصيل المرسل";
$_['text_receiver_details'] = 'تفاصيل المرسل إليه';
$_['text_shipment_information'] = "تفاصيل الشحن";
$_['text_success'] = 'نجاح: تم إنشاء الشحنة بنجاح!';

$_['text_shipping_submitted_message'] = '<b> تم تقديم معلومات الشحن الخاصة بك. </ b>';
$_['text_cancel_failed'] = 'فشل إلغاء الشحنة';
$_['text_button_create_shipment'] = 'إنشاء شحنة';
$_['text_checkbox_notify_customer'] = 'أبلغ العميل بالبريد الإلكتروني';
$_['text_processing'] = 'جارٍ المعالجة ...';
$_['text_ajax_error_common'] = 'حدث خطأ ، يرجى المحاولة مرة أخرى لاحقًا.';
$_['text_product_item_name'] = "اسم العنصر" ;
$_['text_product_quantity'] = "الكمية" ;
$_['text_no_of_products_shipped'] = 'عدد العناصر التي سيتم شحنها';
$_['text_ok'] = 'موافق';
$_['text_shipment_status'] = 'حالة الشحن:';
$_['text_created_date'] = 'تاريخ الإنشاء:';

$_['text_shipment_type_delivery'] = 'التسليم';
$_['text_shipment_type_self_pickup'] = 'التقاط ذاتي';
$_['text_billing_type_cod'] = "الدفع عند الاستلام";
$_['text_billing_type_prepaid'] = "الدفع المسبق" ;
$_['text_primary_service_delivery'] = 'توصيل';
$_['text_secondary_service_insurance'] = 'تأمين';
$_['text_secondary_service_cod'] = "الدفع عند الاستلام";

$_['text_shipment_pieces'] = 'حزم الشحن';

// دخول
# المرسل (المرسل)
$_['entry_consignor'] = 'اسم المرسل إليه';
$_['entry_consignor_email'] = 'البريد الإلكتروني للمرسل';
$_['entry_origin_city'] = 'Origin City';
$_['entry_origin_area_new'] = "اسم منطقة المنشأ";
$_['entry_consignor_street_name'] = 'اسم شارع المرسل';
$_['entry_consignor_building_name'] = "اسم مبنى المرسل";
$_['entry_consignor_address_house_appartment'] = 'قسم عنوان المرسل للمنزل';
$_['entry_consignor_address_landmark'] = 'علامة عنوان المرسل';
$_['entry_consignor_country_code'] = 'رمز بلد المرسل';
$_['entry_consignor_phone'] = 'هاتف المرسل';
$_['entry_consignor_alternate_country_code'] = 'رمز البلد البديل للمرسل';
$_['entry_consignor_alternate_phone'] = 'الهاتف البديل المرسل';

# المرسل إليه (المتلقي)
$_['entry_consignee'] = "اسم المرسل إليه";
$_['entry_consignee_email'] = 'البريد الإلكتروني المرسل إليه';
$_['entry_destination_city'] = "مدينة الوجهة";
$_['entry_destination_area_new'] = 'اسم منطقة الوجهة';
$_['entry_consignee_street_name'] = 'اسم شارع المرسل إليه';
$_['entry_consignee_building_name'] = "اسم مبنى المرسل إليه";
$_['entry_consignee_address_house_appartment'] = 'قسم عنوان المرسل إليه';
$_['entry_consignee_address_landmark'] = 'علامة عنوان المرسل إليه';
$_['entry_consignee_country_code'] = 'رمز بلد المرسل إليه';
$_['entry_consignee_phone'] = 'هاتف المرسل إليه';
$_['entry_consignee_alternate_country_code'] = 'رمز البلد البديل للمرسل إليه';
$_['entry_consignee_alternate_phone'] = 'الهاتف البديل المرسل إليه';

# شحنة
$_['entry_shipment_reference_number'] = "الرقم المرجعي للشحن";
$_['entry_shipment_type'] = "نوع الشحنة";
$_['entry_billing_type'] = 'نوع الفواتير';
$_['entry_collect_amount'] = 'اجمع المبلغ';
$_['entry_primary_service'] = 'الخدمات الأساسية';
$_['entry_secondary_service'] = "الخدمة الثانوية";
$_['entry_item_value'] = "قيمة العنصر" ;
$_['entry_commodity_description'] = "وصف السلع";

$_['entry_weight_actual'] = "الوزن الفعلي" ;
$_['entry_volumetric_width'] = 'العرض الحجمي';
$_['entry_volumetric_height'] = 'الارتفاع الحجمي';
$_['entry_volumetric_depth'] = 'العمق الحجمي';

// مساعدة
$_['help_country_code'] = 'رمز البلد الأولي المكون من 3 أرقام e-g +966، 966 00966';
$_['help_phone'] = 'رقم مكون من 7 أرقام';


// خطأ
$_['error_permission'] = 'تحذير: ليس لديك إذن لتعديل clex shipping!';

$_['error_consignor'] = 'الاسم مطلوب!';
$_['error_consignor_email'] = 'البريد الإلكتروني مطلوب!';
$_['error_origin_city'] = 'المدينة مطلوبة!';
$_['error_consignor_country_code'] = 'كود الدولة مطلوب!';
$_['error_consignor_phone'] = 'الهاتف مطلوب!';

$_['error_consignee'] = 'الاسم مطلوب!';
$_['error_consignee_email'] = 'البريد الإلكتروني مطلوب!';
$_['error_destination_city'] = 'المدينة مطلوبة!';
$_['error_consignee_country_code'] = 'كود الدولة مطلوب!';
$_['error_consignee_phone'] = 'الهاتف مطلوب!';

$_['error_shipment_reference_number'] = 'الرقم المرجعي للشحن مطلوب!';
$_['error_shipment_type'] = 'نوع الشحنة مطلوب!';
$_['error_billing_type'] = 'نوع الفاتورة مطلوب!';
$_['error_collect_amount'] = 'اجمع المبلغ المطلوب!';

$_['error_primary_service'] = 'الخدمة الأساسية مطلوبة!';
$_['error_secondary_service'] = 'الخدمة الثانوية مطلوبة!';
$_['error_commodity_description'] = 'وصف السلعة مطلوب!';
$_['error_shipment_piece'] = 'حزم الشحن مطلوبة!';
$_['error_shipment_piece_invalid'] = 'حزم الشحن غير صالحة!';

// البريد الإلكتروني
$_['clex_text_link'] = 'لعرض طلبك ، انقر على الرابط أدناه';
$_['clex_text_order_status'] = 'تم تحديث طلبك إلى الحالة التالية';
$_['clex_text_order_id'] = 'معرف الطلب';
$_['clex_text_date_added'] = 'تاريخ الإضافة';
$_['clex_text_comment'] = 'التعليقات على طلبك هي';
$_['clex_text_subject'] = '٪ s - تم إنشاء الشحنة';