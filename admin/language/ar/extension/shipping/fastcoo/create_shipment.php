<?php
// العنوان
$_['heading_title '] =' Fastcoo Shipping ';

// نص
$_['text_back_to_order'] = 'العودة للطلب';
$_['text_print_label'] = 'طباعة الملصق';
$_['text_track'] = 'تتبع الشحن';
$_['text_cancel_sipment'] = "إلغاء الشحن";
$_['text_error_heading'] = "خطأ" ;
$_['text_shipment_details'] = "تفاصيل الشحن";
$_['text_sender_details'] = "تفاصيل المرسل";
$_['text_receiver_details'] = "تفاصيل جهاز الاستقبال";
$_['text_shipment_information'] = "تفاصيل الشحن";
$_['text_shipment_travel_history'] = 'سجل السفر';
$_['text_success'] = 'نجاح: تم إنشاء الشحنة بنجاح!';

$_['text_shipping_submitted_message'] = '<b> تم تقديم معلومات الشحن الخاصة بك. </ b>';
$_['text_cancel_failed'] = 'فشل إلغاء الشحنة';
$_['text_shipment_cancelled'] = "تم إلغاء الشحنة";
$_['text_button_create_shipment'] = 'إنشاء شحنة';
$_['text_checkbox_notify_customer'] = 'أبلغ العميل بالبريد الإلكتروني';
$_['text_button_resubmit_shipment'] = 'أعد الإرسال للتحديث';
$_['text_cancel_shipment'] = 'إلغاء الشحنة';
$_['text_cancel_confirm_shipment'] = 'هل أنت متأكد أنك تريد إلغاء الشحن؟';
$_['text_shipment_tracking'] = "تتبع الشحنة";
$_['text_processing'] = 'جارٍ المعالجة ...';
$_['text_ajax_error_common'] = 'حدث خطأ ، يرجى المحاولة مرة أخرى لاحقًا.';
$_['text_product_item_name'] = "اسم العنصر" ;
$_['text_product_quantity'] = "الكمية" ;
$_['text_no_of_products_shipped'] = 'عدد العناصر التي سيتم شحنها';
$_['text_ok'] = 'موافق';
$_['text_shipment_status'] = 'حالة الشحن:';
$_['text_created_date'] = 'تاريخ الإنشاء:';

$_['text_product_type_kvaimi'] = 'KVAIMI';
$_['text_product_type_parcel'] = 'Parcel';
$_['text_delivery_service_same_day'] = 'توصيل في نفس اليوم';
$_['text_delivery_service_next_day'] = 'التوصيل في اليوم التالي';
$_['text_booking_mode_cc'] = 'النقدية المجمعة';
$_['text_booking_mode_cod'] = "الدفع عند الاستلام";


// دخول
# مرسل
$_['entry_sender_name'] = "الاسم" ;
$_['entry_sender_mobile'] = 'Mobile';
$_['entry_sender_address'] = "العنوان";
$_['entry_sender_city'] = "المدينة" ;

# المتلقي
$_['entry_receiver_name'] = "الاسم" ;
$_['entry_receiver_email'] = "البريد الإلكتروني" ;
$_['entry_receiver_address'] = "العنوان" ;
$_['entry_receiver_phone'] = "الهاتف" ;
$_['entry_receiver_city'] = "المدينة" ;

# شحنة
$_['entry_product_type'] = "نوع المنتج";
$_['entry_delivery_service'] = 'خدمة التوصيل';
$_['entry_shipment_weight'] = "وزن الشحنة (كجم)" ;
$_['entry_shipment_description'] = 'تفاصيل الشحنة';
$_['entry_number_of_packets'] = "عدد الحزم" ;
$_['entry_booking_mode'] = 'وضع الحجز';
$_['entry_cod_value'] = "قيمة COD" ;
$_['entry_reference'] = 'مرجع';

$_['entry_shipment_items_price'] = "سعر العناصر";
$_['entry_shipment_quantity'] = "كمية العناصر" ;
$_['entry_shipment_items_weight'] = "وزن العناصر" ;
$_['entry_shipment_currency'] = 'العملة' ;
$_['entry_awb_no'] = "AWB No";
$_['entry_status_code'] = "كود الحالة" ;
$_['entry_origin'] = "الأصل" ;
$_['entry_destination'] = 'الوجهة';

$_['entry_travel_history_new_location'] = 'موقع جديد';
$_['entry_travel_history_new_status'] = "حالة جديدة" ;
$_['entry_travel_history_activites'] = 'الأنشطة';
$_['entry_travel_history_code'] = 'Code';
$_['entry_travel_history_comment'] = 'تعليق';
$_['entry_travel_history_entry_date'] = 'تاريخ الدخول';

// خطأ
$_['error_permission'] = 'تحذير: ليس لديك إذن لتعديل Fastcoo shipping!';

$_['error_sender_name'] = 'الاسم مطلوب!';
$_['error_sender_address'] = 'العنوان مطلوب!';
$_['error_sender_mobile'] = 'الجوال مطلوب!';
$_['error_sender_city'] = 'المدينة مطلوبة!';

$_['error_receiver_name'] = 'الاسم مطلوب!';
$_['error_receiver_email'] = 'البريد الإلكتروني مطلوب!';
$_['error_receiver_address'] = 'العنوان مطلوب!';
$_['error_receiver_phone'] = 'الهاتف مطلوب!';
$_['error_receiver_city'] = 'المدينة مطلوبة!';

$_['error_product_type'] = 'نوع المنتج مطلوب!';
$_['error_delivery_service'] = 'خدمة التوصيل مطلوبة!';
$_['error_shipment_weight'] = 'وزن الشحنة مطلوب!';
$_['error_shipment_description'] = 'تفاصيل الشحنة مطلوبة!';
$_['error_number_of_packets'] = 'عدد الحزم المطلوبة!';
$_['error_booking_mode'] = 'وضع الحجز مطلوب!';
$_['error_cod_value'] = 'قيمة COD مطلوبة!';
$_['error_reference'] = 'المرجع مطلوب!';

// البريد الإلكتروني
$_['fastcoo_text_link'] = 'لعرض طلبك ، انقر على الرابط أدناه';
$_['fastcoo_text_order_status'] = 'تم تحديث طلبك إلى الحالة التالية';
$_['fastcoo_text_order_id'] = 'معرف الطلب';
$_['fastcoo_text_date_added'] = 'تاريخ الإضافة';
$_['fastcoo_text_comment'] = 'التعليقات على طلبك هي';
$_['fastcoo_text_subject'] = '٪ s - تم إنشاء الشحنة';