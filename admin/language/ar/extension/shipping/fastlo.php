<?php
// العنوان
$_['heading_title '] = 'Fastlo';

// نص
$_['text_extension'] = 'الامتدادات' ;
$_['text_success'] = 'نجاح: لقد قمت بتعديل Fastlo shipping!';

// دخول
$_['entry_api_key'] = "مفتاح API" ;
$_['entry_test'] = "وضع الاختبار" ;
$_['entry_cost'] = "التكلفة" ;
$_['entry_status'] = 'الحالة' ;
$_['entry_sort_order'] = "ترتيب الفرز" ;

// مساعدة
$_['help_test'] = 'استخدم هذه الوحدة في الاختبار (نعم) أو وضع الإنتاج (لا)؟';

// خطأ
$_['error_permission'] = 'تحذير: ليس لديك إذن بتعديل 525 ألف شحن!';
$_['error_api_key'] = 'مفتاح API مطلوب!';