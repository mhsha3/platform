<?php
// العنوان
$_['head_title '] = 'SAEE';

// نص
$_['text_extension'] = 'الإضافات';
$_['text_success'] = 'نجاح: لقد قمت بتعديل Saee shipping!';
$_['text_edit'] = 'تحرير Saee Shipping';
$_['text_cancel_failed']           = 'فشل إلغاء الشحنة';
$_['text_shipment_cancelled']       = 'تم إلغاء الشحنة';
$_['text_shipping_submitted_message']       = '<b> تم إرسال معلومات الشحن الخاصة بك. </b><br> إذا كنت تريد تغيير شيء ما!';
$_['text_shipping_resubmit_button']       = 'إعادة إرسال التفاصيل';

// دخول
$_['entry_api_key'] = 'مفتاح API';
$_['entry_test'] = "وضع الاختبار";
$_['entry_cashonpickup'] = "النقد عند الاستلام";
$_['entry_cashondelivery'] = "الدفع نقدًا عند الاستلام" ;


$_['entry_pickup_address_id'] = "معرّف عنوان الالتقاط";
$_['entry_pickup_address_code'] = "رمز عنوان الاستلام";
$_['entry_origin_name'] = "اسم الالتقاط" ;
$_['entry_origin_name'] = "اسم الالتقاط" ;
$_['entry_origin_phone'] = "التقاط الهاتف" ;
$_['entry_origin_phone2'] = 'هاتف بيك أب 2';
$_['entry_origin_email'] = "استلام البريد الإلكتروني" ;
$_['entry_origin_gps_latitude'] = "نقطة عرض GPS" ;
$_['entry_origin_gps_longitude'] = 'نقطة خط طول GPS';
$_['entry_origin_address'] = "عنوان الاستلام";
$_['entry_origin_address2'] = 'عنوان الاستلام 2';
$_['entry_origin_district'] = 'منطقة الالتقاء';
$_['entry_origin_city'] = 'Pickup City';
$_['entry_origin_state'] = "ولاية / مقاطعة الالتقاط";
$_['entry_origin_zipcode'] = 'لاقط الرمز البريدي';
$_['entry_origin_country'] = "بلد الالتقاء";
$_['entry_status'] = "الحالة" ;
$_['entry_sort_order'] = 'ترتيب الفرز';
$_['entry_debug'] = "وضع التصحيح";

// مساعدة
$_['help_api_key'] = 'أدخل مفتاح Saee API الخاص بك.';
$_['help_test'] = 'استخدم هذه الوحدة في الاختبار (نعم) أو وضع الإنتاج (لا)؟';
$_['help_cashonpickup'] = 'نقدًا عند الاستلام.';
$_['help_cashonpickup2'] = "القيمة 0 تشير إلى عدم وجود نقود عند الاستلام";
$_['help_cashondelivery'] = "القيمة 0 لا تدل على COD";
$_['help_pickup_address_id'] = 'معرّف عنوان الالتقاط سيتم استخدام العناوين المخزنة بالفعل لمعرف عنوان الالتقاط المقدم بدلاً من الحقول التالية (عنوان الاستلام ، عنوان الالتقاط 2 ، منطقة الالتقاط ، مدينة الالتقاط ، حالة الالتقاط ، الرمز البريدي لاقط ، لاقط Latitude ، خط الطول لاقط) ';
$_['help_pickup_address_code'] = 'للحصول على قائمة برموز عناوين الالتقاط المسجلة للشركة ، استخدم رقم API 17 قائمة عناوين الالتقاط. (يستخدم هذا إذا كان لديك الرمز الخاص بك ، يمكنك تمريره بدلاً من معرّف عنوان الالتقاط) ';
$_['help_origin_name'] = 'أدخل اسمك الأصلي.';
$_['help_origin_phone'] = 'أدخل رقم هاتفك الأصلي.';
$_['help_origin_email'] = 'أدخل بريدك الإلكتروني الأصلي.';
$_['help_origin_gps_latitude'] = 'نقطة خط عرض GPS.';
$_['help_origin_gps_longitude'] = 'نقطة خط طول GPS.';
$_['help_origin_address'] = 'وصف عنوان الاستلام.';
$_['help_origin_address2'] = 'التقط وصفًا إضافيًا للعنوان.';
$_['help_origin_district'] = 'أدخل اسم المنطقة الأصلية.';
$_['help_origin_city'] = 'أدخل اسم المدينة الأصلية.';
$_['help_origin_state'] = 'أدخل الولاية / المقاطعة الأصلية.';
$_['help_origin_zipcode'] = 'أدخل الرمز البريدي الأصلي.';
$_['help_origin_country'] = 'أدخل بلدك الأصلي.';
$_['help_debug'] = 'حفظ إرسال / استقبال البيانات في سجل النظام';
$_['help_shipment_quantity'] = 'أرقام حزمة بوليصة الشحن';
$_['help_weight'] = 'وزن العبوة بالكيلو جرام';


// خطأ
$_['error_permission'] = 'تحذير: ليس لديك إذن بتعديل شحن Saee!';
$_['error_api_key'] = 'معرف العميل مطلوب!';
$_['error_origin_name'] = 'اسم الالتقاط مطلوب!';
$_['error_origin_phone'] = 'يجب التقاط الهاتف!';
$_['error_origin_email'] = 'استلام البريد الإلكتروني مطلوب!';
$_['error_origin_gps_latitude'] = 'لاقط خط عرض GPS مطلوب!';
$_['error_origin_gps_longitude'] = 'لاقط خط طول GPS مطلوب!';
$_['error_origin_address'] = 'عنوان الاستلام مطلوب!';
$_['error_origin_city'] = 'مدينة النقل مطلوبة!';
$_['error_origin_state'] = 'ولاية / مقاطعة الالتقاط مطلوبة!';
$_['error_origin_country'] = 'بلد الالتقاط مطلوب!';

// إنشاء شحنة
$_['saee_create_heading_title'] = 'ساي للشحن';
$_['text_back_to_order'] = 'العودة للطلب';
$_['text_print_label'] = 'طباعة الملصق';
$_['text_track'] = 'تتبع الشحن';
$_['text_cancel_sipment'] = "إلغاء الشحن";
$_['text_error_heading'] = "خطأ" ;
$_['text_shipment_details'] = "تفاصيل الشحن";
$_['text_last_mile_booking'] = 'حجز الميل الأخير';
$_['text_pickup_shipments'] = "شحنات الاستلام" ;

$_['text_sender_details'] = "تفاصيل المرسل";
$_['text_sender_name'] = "الاسم" ;
$_['text_sender_email'] = "البريد الإلكتروني" ;
$_['text_sender_phone'] = "الهاتف" ;
$_['text_sender_address'] = "عنوان الشارع";
$_['text_sender_city'] = "المدينة" ;
$_['text_sender_country'] = "البلد" ;

$_['text_receiver_details'] = "تفاصيل جهاز الاستقبال";
$_['text_receiver_name'] = "الاسم" ;
$_['text_receiver_email'] = "البريد الإلكتروني" ;
$_['text_receiver_mobile'] = 'موبايل';
$_['text_receiver_mobile2'] = 'موبايل 2';
$_['text_receiver_address'] = "عنوان الشارع";
$_['text_receiver_address2'] = 'عنوان الشارع 2';
$_['text_receiver_district'] = 'المنطقة';
$_['text_receiver_city'] = "المدينة" ;
$_['text_receiver_state'] = 'State';
$_['text_receiver_zipcode'] = "الرمز البريدي" ;
$_['text_receiver_latitude'] = 'Latitude';
$_['text_receiver_longitude'] = 'خط الطول';

$_['text_custom_value'] = "قيمة مخصصة" ;
$_['text_hs_code'] = "رمز النظام المنسق" ;
$_['text_category_id'] = 'معرف الفئة';

$_['text_pickup_details'] = "تفاصيل الاستلام";

$_['text_shipment_information'] = "معلومات الشحن";
$_['text_shipment_quantity'] = "الكمية" ;
$_['text_shipment_weight'] = "الوزن" ;
$_['text_shipment_currency'] = 'العملة';
$_['text_shipment_items_price'] = "سعر العناصر";
$_['text_product_item_name'] = "اسم العنصر" ;
$_['text_product_quantity'] = "الكمية" ;
$_['text_no_of_products_shipped'] = 'عدد العناصر التي سيتم شحنها';
$_['text_button_create_shipment'] = 'إنشاء شحنة';
$_['text_checkbox_notify_customer'] = 'أبلغ العميل بالبريد الإلكتروني';
$_['text_button_resubmit_shipment'] = 'أعد الإرسال للتحديث';
$_['text_cancel_shipment'] = 'إلغاء الشحنة';
$_['text_cancel_confirm_shipment'] = 'هل أنت متأكد أنك تريد إلغاء الشحن؟';
$_['text_shipment_tracking'] = "تتبع الشحنة";
$_['text_processing'] = 'جارٍ المعالجة ...';
$_['text_tracking_city'] = "المدينة" ;
$_['text_tracking_status'] = "الحالة" ;
$_['text_tracking_date'] = 'التاريخ';
$_['text_ajax_error_common'] = 'حدث خطأ ، يرجى المحاولة مرة أخرى لاحقًا.';
$_['text_ok'] = 'موافق';

// البريد الإلكتروني
$_['saee_text_link'] = 'لعرض طلبك ، انقر على الرابط أدناه';
$_['saee_text_order_status'] = 'تم تحديث طلبك إلى الحالة التالية';
$_['saee_text_order_id'] = 'معرف الطلب';
$_['saee_text_date_added'] = 'تاريخ الإضافة';
$_['saee_text_comment'] = 'التعليقات على طلبك هي';
$_['saee_text_subject'] = '٪ s - تم إنشاء الشحنة';