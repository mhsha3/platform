<?php
// العنوان
$_['heading_title '] = 'Clex';

// نص
$_['text_extension'] = 'الامتدادات' ;
$_['text_success'] = 'نجاح: لقد قمت بتعديل Clex shipping!';
$_['text_edit']        = 'تحرير Clex Shipping';

// دخول
$_['entry_access_token'] = "Access Token" ;
$_['entry_cost'] = "التكلفة" ;
$_['entry_status'] = 'الحالة' ;
$_['entry_sort_order'] = "ترتيب الفرز" ;

// مساعدة
$_['help_test'] = 'استخدم هذه الوحدة في الاختبار (نعم) أو وضع الإنتاج (لا)؟';

// خطأ
$_['error_permission'] = 'تحذير: ليس لديك الإذن بتعديل Clex shipping!';
$_['error_access_token'] = 'Access Token مطلوب!';