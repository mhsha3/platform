<?php
// العنوان
$_['heading_title '] = '525 ألفًا';

// نص
$_['text_extension'] = 'الإضافات';
$_['text_success'] = 'نجاح: لقد قمت بتعديل 525 ألف شحن!';
$_['text_edit'] = 'تحرير 525 ألف شحن';
$_['text_box'] = 'Box';
$_['text_pallet'] = 'منصة نقالة' ;
$_['text_other'] = 'أخرى';

// دخول
$_['entry_client_id'] = 'معرف العميل' ;
$_['entry_client_secret'] = 'سر العميل';
$_['entry_shipper_id'] = 'معرّف الشاحن' ;
$_['entry_requestedby_id'] = 'RequestedBy Id';
$_['entry_api_key'] = 'API Key';
$_['entry_test'] = 'وضع الاختبار';

$_['entry_origin_name'] = 'اسم الالتقاط' ;
$_['entry_origin_phone'] = 'التقاط الهاتف' ;
$_['entry_origin_email'] = 'استلام البريد الإلكتروني' ;
$_['entry_origin_gps_latitude'] = 'نقطة عرض GPS' ;
$_['entry_origin_gps_longitude'] = 'نقطة خط طول GPS';
$_['entry_origin_address'] = 'عنوان الاستلام';
$_['entry_origin_address2'] = 'عنوان الاستلام 2';
$_['entry_origin_city'] = 'Pickup City';
$_['entry_origin_state'] = 'ولاية / مقاطعة الالتقاط';
$_['entry_origin_country'] = 'بلد الالتقاء';
$_['entry_pickup_instructions'] = 'تعليمات الالتقاط';
$_['entry_delivery_instructions'] = 'تعليمات التسليم';

$_['text_pickup_arrival_window_begin'] = 'بدء نافذة الوصول';
$_['text_pickup_arrival_window_end'] = 'نهاية نافذة الوصول';

$_['entry_pickup_arrival_window'] = 'نافذة وصول صغيرة';
$_['entry_pickup_arrival_window_days'] = 'أيام';
$_['entry_pickup_arrival_window_exclude_begin'] = 'استبعاد البدء';
$_['entry_pickup_arrival_window_exclude_end'] = 'استبعاد النهاية';

$_['entry_delivery_arrival_window'] = 'نافذة وصول التسليم';
$_['entry_delivery_arrival_window_days'] = 'أيام';
$_['entry_delivery_arrival_window_exclude_begin'] = 'استبعاد البدء';
$_['entry_delivery_arrival_window_exclude_end'] = 'استبعاد النهاية';

$_['entry_dimension'] = 'الأبعاد (الطول × العرض × الارتفاع) في سم' ;
$_['entry_length'] = 'الطول' ;
$_['entry_height'] = 'الارتفاع' ;
$_['entry_width'] = 'العرض' ;
$_['entry_packaging_type'] = 'نوع التغليف';
$_['entry_stackable'] = 'قابل للتكديس';

$_['entry_status'] = 'الحالة' ;
$_['entry_sort_order'] = 'ترتيب الفرز';
$_['entry_debug'] = 'وضع التصحيح';

$_['entry_ref_id'] = 'RefId';
$_['entry_load_description'] = 'تحميل الوصف';

$_['entry_shipment_value_amount'] = 'مبلغ السعر';
$_['entry_shipment_value_currency'] = 'عملة السعر';

$_['entry_truck_type'] = 'نوع الشاحنة' ;
$_['entry_full_truck'] = 'شاحنة كاملة' ;

$_['entry_cod_value_amount'] = 'COD Amount';
$_['entry_cod_value_currency'] = 'عملة COD';

// مساعدة
$_['help_client_id'] = 'أدخل معرف العميل 525 ألف.';
$_['help_client_secret'] = 'أدخل سر العميل البالغ 525 ألفًا.';
$_['help_shipper_id'] = 'أدخل معرف الشاحن المقدم من 525 ألف.';
$_['help_test'] = 'استخدم هذه الوحدة في الاختبار (نعم) أو وضع الإنتاج (لا)؟';
$_['help_origin_name'] = 'أدخل اسمك الأصلي.';
$_['help_origin_phone'] = 'أدخل رقم هاتفك الأصلي.';
$_['help_origin_email'] = 'أدخل بريدك الإلكتروني الأصلي.';
$_['help_origin_gps_latitude'] = 'نقطة خط عرض GPS.';
$_['help_origin_gps_longitude'] = 'نقطة خط طول GPS.';
$_['help_origin_address'] = 'وصف عنوان الاستلام.';
$_['help_origin_address2'] = 'التقط وصفًا إضافيًا للعنوان.';
$_['help_origin_city'] = 'أدخل اسم المدينة الأصلية.';
$_['help_origin_state'] = 'أدخل الولاية / المقاطعة الأصلية.';
$_['help_origin_country'] = 'أدخل بلدك الأصلي.';
$_['help_pickup_instructions'] = 'تعليمات الاستلام الخاصة.';
$_['help_delivery_instructions'] = 'تعليمات التسليم الخاصة.';
$_['help_pickup_arrival_window'] = 'المدة الزمنية لاختيار الشحنة.';
$_['help_delivery_arrival_window'] = 'المدة الزمنية لتسليم الشحنة.';

$_['help_packaging_type'] = 'نوع تغليف العنصر.';
$_['help_stackable'] = 'عنصر الشحن قابل للتكديس.';
$_['help_dimension'] = 'هذا من المفترض أن يكون متوسط ​​حجم صندوق التعبئة لديك. أبعاد العنصر الفردية غير مدعومة في الوقت الحالي ، لذا يجب إدخال أبعاد متوسطة مثل 5 × 5 × 5. ';
$_['help_debug'] = 'حفظ إرسال / استقبال البيانات في سجل النظام';

$_['help_ref_id'] = 'معرّف مرجع الشحنة';
$_['help_load_description'] = 'وصف تحميل الشحنة';

$_['help_shipment_value_amount'] = 'مبلغ سعر العنصر المرسل ، لن يتم استخدام هذا الحقل لتحديد مبلغ الدفع النقدي عند التسليم. ';
$_['help_shipment_value_currency'] = 'عملة السعر للعنصر المرسل لن يتم استخدام هذا الحقل لتحديد المبلغ النقدي عند التسليم.';

$_['help_truck_type'] = 'نوع الشاحنة المطلوبة. <u class="text-danger"> يجب استخدامها فقط في حالة طلب نوع شاحنة محدد لكل اتفاقية مع 525k. </u> ';
$_['help_full_truck'] = 'مطلوب شاحنة كاملة لكل شحنة. <u class="text-danger"> لا يجب ملؤه ما لم يتم الاتفاق مع 525k لتمكينه. </ u> (افتراضي = false) ';

$_['help_cod_value_amount'] = 'النقد الذي سيتم تحصيله من العملاء';
$_['help_cod_value_currency'] = 'العملة النقدية التي سيتم تحصيلها من العميل';

$_['help_item_quantity'] = 'كمية عنصر الشحن';
$_['help_item_weight'] = 'وزن سلعة الشحن بالكيلو جرام';
$_['help_item_refid'] = 'مرجع عنصر الشحنة';

// خطأ
$_['error_permission'] = 'تحذير: ليس لديك إذن بتعديل 525 ألف شحن!';
$_['error_client_id'] = 'معرف العميل مطلوب!';
$_['error_client_secret'] = 'سر العميل مطلوب!';
$_['error_shipper_id'] = 'معرّف الشاحن مطلوب!';
$_['error_api_key'] = 'مفتاح API مطلوب!';

$_['error_origin_phone'] = 'يجب التقاط الهاتف!';
$_['error_origin_email'] = 'استلام البريد الإلكتروني مطلوب!';
$_['error_origin_gps_latitude'] = 'لاقط خط عرض GPS مطلوب!';
$_['error_origin_gps_longitude'] = 'لاقط خط طول GPS مطلوب!';
$_['error_origin_address'] = 'عنوان الاستلام مطلوب!';
$_['error_origin_city'] = 'مدينة النقل مطلوبة!';
$_['error_origin_state'] = 'ولاية / مقاطعة الالتقاط مطلوبة!';
$_['error_origin_country'] = 'بلد الالتقاط مطلوب!';

$_['error_pickup_instructions'] = 'تعليمات الالتقاط مطلوبة!';
$_['error_pickup_arrival_window'] = 'نافذة وصول الالتقاط مطلوبة!';
$_['error_delivery_arrival_window'] = 'نافذة وصول التسليم مطلوبة!';
$_['error_dimension'] = 'متوسط ​​الأبعاد مطلوب!';

$_['error_input_data'] = 'بيانات إدخال غير صالحة.';


// إنشاء شحنة
$_['i525k_create_heading_title'] = '525 ألف شحن';
$_['text_back_to_order'] = 'العودة للطلب';
$_['text_print_label'] = 'طباعة الملصق';
$_['text_track'] = 'تتبع الشحن';
$_['text_cancel_sipment'] = 'إلغاء الشحن';
$_['text_error_heading'] = 'خطأ' ;
$_['text_shipment_details'] = 'تفاصيل الشحن';
$_['text_last_mile_booking'] = 'Last Mile Booking';
$_['text_pickup_shipments'] = 'شحنات الاستلام' ;

$_['text_sender_details'] = 'تفاصيل المرسل';
$_['text_sender_name'] = 'الاسم' ;
$_['text_sender_email'] = 'البريد الإلكتروني' ;
$_['text_sender_phone'] = 'الهاتف' ;
$_['text_sender_address'] = 'عنوان الشارع';
$_['text_sender_city'] = 'المدينة' ;
$_['text_sender_country'] = 'البلد' ;

$_['text_receiver_details'] = 'تفاصيل جهاز الاستقبال';
$_['text_receiver_name'] = 'الاسم' ;
$_['text_receiver_email'] = 'البريد الإلكتروني' ;
$_['text_receiver_mobile'] = 'Mobile';
$_['text_receiver_address'] = 'عنوان الشارع';
$_['text_receiver_address2'] = 'عنوان الشارع 2';
$_['text_receiver_city'] = 'المدينة' ;
$_['text_receiver_country'] = 'الدولة';
$_['text_receiver_latitude'] = 'Latitude';
$_['text_receiver_longitude'] = 'خط الطول';
$_['text_receiver_arrival_window_begin'] = 'بدء نافذة الوصول';
$_['text_receiver_arrival_window_end'] = 'نهاية نافذة الوصول';

$_['text_datetime_include'] = 'تضمين';
$_['text_datetime_exclude'] = 'استبعاد';

$_['text_pickup_details'] = 'تفاصيل الاستلام';

$_['text_shipping_submitted_message'] = '<b> تم تقديم معلومات الشحن الخاصة بك. </ b>';
$_['text_cancel_failed'] = 'فشل إلغاء الشحنة';
$_['text_shipment_cancelled'] = 'تم إلغاء الشحنة';

$_['text_shipment_information'] = 'معلومات الشحن';
$_['text_shipment_quantity'] = 'الكمية' ;
$_['text_shipment_weight'] = 'الوزن' ;
$_['text_shipment_item_refid'] = 'Item RefId';
$_['text_shipment_currency'] = 'العملة';
$_['text_shipment_items_price'] = 'سعر العناصر';
$_['text_product_item_name'] = 'اسم العنصر' ;
$_['text_product_quantity'] = 'الكمية' ;
$_['text_no_of_products_shipped'] = 'عدد العناصر التي سيتم شحنها';
$_['text_button_create_shipment'] = 'إنشاء شحنة';
$_['text_checkbox_notify_customer'] = 'أبلغ العميل بالبريد الإلكتروني';
$_['text_button_resubmit_shipment'] = 'أعد الإرسال للتحديث';
$_['text_cancel_shipment'] = 'إلغاء الشحنة';
$_['text_cancel_confirm_shipment'] = 'هل أنت متأكد أنك تريد إلغاء الشحن؟';
$_['text_shipment_tracking'] = 'تتبع الشحنة';
$_['text_processing'] = 'جارٍ المعالجة ...';
$_['text_tracking_city'] = 'المدينة' ;
$_['text_tracking_status'] = 'الحالة' ;
$_['text_tracking_date'] = 'التاريخ';
$_['text_ajax_error_common'] = 'حدث خطأ ، يرجى المحاولة مرة أخرى لاحقًا.';
$_['text_ok'] = 'موافق';
$_['text_'] = 'موافق' ;

// البريد الإلكتروني
$_['i525k_text_link'] = 'لعرض طلبك ، انقر على الرابط أدناه';
$_['i525k_text_order_status'] = 'تم تحديث طلبك إلى الحالة التالية';
$_['i525k_text_order_id'] = 'معرف الطلب';
$_['i525k_text_date_added'] = 'تاريخ الإضافة';
$_['i525k_text_comment'] = 'التعليقات على طلبك هي';
$_['i525k_text_subject'] = '٪ s - تم إنشاء الشحنة';