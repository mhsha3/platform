<?php
// العنوان
$_['heading_title '] = 'Fastlo Shipping';

// نص
$_['text_back_to_order'] = 'العودة للطلب';
$_['text_print_label'] = 'طباعة الملصق';
$_['text_track'] = 'تتبع الشحن';
$_['text_cancel_sipment'] = "إلغاء الشحن";
$_['text_error_heading'] = "خطأ" ;
$_['text_shipment_details'] = "تفاصيل الشحن";
$_['text_sender_details'] = "تفاصيل المرسل";
$_['text_receiver_details'] = "تفاصيل جهاز الاستقبال";
$_['text_shipment_information'] = "تفاصيل الشحن";
$_['text_success'] = 'نجاح: تم إنشاء الشحنة بنجاح!';

$_['text_shipping_submitted_message'] = '<b> تم تقديم معلومات الشحن الخاصة بك. </ b>';
$_['text_cancel_failed'] = 'فشل إلغاء الشحنة';
$_['text_shipment_cancelled'] = "تم إلغاء الشحنة";
$_['text_button_create_shipment'] = 'إنشاء شحنة';
$_['text_checkbox_notify_customer'] = 'أبلغ العميل بالبريد الإلكتروني';
$_['text_button_resubmit_shipment'] = 'أعد الإرسال للتحديث';
$_['text_cancel_shipment'] = 'إلغاء الشحنة';
$_['text_cancel_confirm_shipment'] = 'هل أنت متأكد أنك تريد إلغاء الشحن؟';
$_['text_shipment_tracking'] = "تتبع الشحنة";
$_['text_processing'] = 'جارٍ المعالجة ...';
$_['text_ajax_error_common'] = 'حدث خطأ ، يرجى المحاولة مرة أخرى لاحقًا.';
$_['text_product_item_name'] = "اسم العنصر" ;
$_['text_product_quantity'] = "الكمية" ;
$_['text_no_of_products_shipped'] = 'عدد العناصر التي سيتم شحنها';
$_['text_ok'] = 'موافق';
$_['text_shipment_status'] = 'حالة الشحن:';
$_['text_created_date'] = 'تاريخ الإنشاء:';

// دخول
# مرسل
$_['entry_sender_name'] = "الاسم" ;
$_['entry_sender_mobile1'] = 'Mobile';
$_['entry_sender_mobile2'] = 'Mobile2';
$_['entry_sender_country'] = "الدولة" ;
$_['entry_sender_city'] = "المدينة" ;
$_['entry_sender_area'] = 'المنطقة';
$_['entry_sender_street'] = 'Street';
$_['entry_sender_additional'] = 'إضافي' ;
$_['entry_sender_latitude'] = 'Latitude';
$_['entry_sender_longitude'] = "خط الطول" ;

# المتلقي
$_['entry_receiver_name'] = "الاسم" ;
$_['entry_receiver_mobile1'] = 'Mobile';
$_['entry_receiver_mobile2'] = 'Mobile2';
$_['entry_receiver_country'] = "الدولة" ;
$_['entry_receiver_city'] = "المدينة" ;
$_['entry_receiver_area'] = "المنطقة" ;
$_['entry_receiver_street'] = 'Street';
$_['entry_receiver_additional'] = 'إضافية';
$_['entry_receiver_latitude'] = 'Latitude';
$_['entry_receiver_longitude'] = "خط الطول" ;

# شحنة
$_['entry_collect_cash_amount'] = 'جمع المبلغ النقدي';
$_['entry_number_of_pieces'] = "عدد القطع" ;
$_['entry_reference'] = 'مرجع';

$_['entry_shipment_items_price'] = "سعر العناصر";
$_['entry_shipment_quantity'] = "كمية العناصر" ;
$_['entry_shipment_weight'] = "وزن الأصناف" ;
$_['entry_shipment_currency'] = 'العملة' ;

// خطأ
$_['error_permission'] = 'تحذير: ليس لديك الإذن بتعديل fastlo shipping!';

$_['error_sender_name'] = 'الاسم مطلوب!';
$_['error_sender_mobile1'] = 'الجوال مطلوب!';
$_['error_sender_country'] = 'البلد مطلوب!';
$_['error_sender_city'] = 'المدينة مطلوبة!';
$_['error_sender_area'] = 'المنطقة المطلوبة!';

$_['error_receiver_name'] = 'الاسم مطلوب!';
$_['error_receiver_mobile1'] = 'الجوال مطلوب!';
$_['error_receiver_country'] = 'البلد مطلوب!';
$_['error_receiver_city'] = 'المدينة مطلوبة!';
$_['error_receiver_area'] = 'المنطقة المطلوبة!';

$_['error_collect_cash_amount'] = 'اجمع المبلغ النقدي المطلوب!';
$_['error_number_of_pieces'] = 'عدد القطع المطلوبة!';
$_['error_reference'] = 'المرجع مطلوب!';

// البريد الإلكتروني
$_['fastlo_text_link'] = 'لعرض طلبك ، انقر فوق الرابط أدناه';
$_['fastlo_text_order_status'] = 'تم تحديث طلبك إلى الحالة التالية';
$_['fastlo_text_order_id'] = 'معرف الطلب';
$_['fastlo_text_date_added'] = 'تاريخ الإضافة';
$_['fastlo_text_comment'] = 'التعليقات على طلبك هي';
$_['fastlo_text_subject'] = '٪ s - تم إنشاء الشحنة';