<?php
$_['heading_title']    = 'المنتجات';
$_['heading_pos']    = 'نقاط البيع';

$_['col_id']    = 'رقم المنتج';
$_['col_barcode']    = 'الباركود';
$_['col_product_name']    = 'المنتج';
$_['col_qty']    = 'الكمية';
$_['col_action']    = 'طباعة الباركود';