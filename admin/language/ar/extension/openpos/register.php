<?php
$_['heading_title']    = 'الصلاحيات والفروع';
$_['heading_title_new']    = 'تسجيل جديد';
$_['heading_title_edit']    = 'تعديل';
$_['heading_pos']    = 'نقاط البيع';

$_['col_name']    = 'الاسم';
$_['col_cashiers']    = 'الصرافين';
$_['col_total_sales']    = 'مجموع المبيعات';
$_['col_total_cash']    = 'الرصيد النقدي';
$_['col_action']    = 'التحكم';
$_['msg_delete_confirm']    = 'هل أنت متأكد؟';

$_['label_register_name']    = 'الاسم';
$_['label_store']    = 'المتجر';
$_['label_cashier']    = 'الصرافين';
$_['label_status']    = 'الحالة';
$_['label_enable']    = 'تمكين';
$_['label_disable']    = 'تعطيل';