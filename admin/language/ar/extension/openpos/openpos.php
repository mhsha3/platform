<?php
// Heading
$_['heading_title']    = 'نقاط البيع';

// Text
$_['text_total_order']   = 'مجموع الطلبات';
$_['text_total_sales']     = 'مجموع المبيعات';
$_['text_total_cash']     = 'الرصيد النقدي';


$_['col_customer']     = 'العميل';
$_['col_total']     = 'المجموع';
$_['col_created']     = 'تم الإنشاء في';

$_['chart_date']     = 'التاريخ';
$_['chart_sale']     = 'المبيعات';


$_['menu_products']     = 'طباعة الباركود/المننتجات';
$_['menu_cash']     = 'إدارة النقد';
$_['menu_registers']     = 'صناديق النقد';
$_['menu_setting']     = 'الإعدادات';
$_['menu_session']     = 'الصلاحيات والفروع';


$_['link_goto_pos']    = 'الذهاب لنقطة البيع';

$_['text_date']   = 'التاريخ';



// Entry

// Text
$_['text_op_payment_method']    = 'مدفوع عن طريق نقطة البيع';
$_['text_op_discount']    = 'تخفيض نقطة البيع';
$_['text_op_shipping']    = 'طلب شحن من نقطة االبيع';

$_['text_op_receipt_table_item']    = 'فطعة';
$_['text_op_receipt_table_price']    = 'السعر';
$_['text_op_receipt_table_qty']    = 'الكمية';
$_['text_op_receipt_table_discount']    = 'التخفيض';
$_['text_op_receipt_table_total']    = 'المجموع';

$_['text_op_receipt_total_subtotal']    = 'المجموع الكلي';
$_['text_op_receipt_total_shipping_total']    = 'الشحن';
$_['text_op_receipt_total_discount_total']    = 'الخصم';
$_['text_op_receipt_total_tax_total']    = 'الضريبة';
$_['text_op_receipt_total_grand_total']    = 'المجموع النهائي';