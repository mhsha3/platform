<?php
$_['heading_title']    = 'الإعدادات';
$_['heading_pos']    = 'نقاط البيع';

$_['msg_success']    = 'تم حفظ الإعدادات';

$_['col_store_name'] = 'اسم المتجر';
$_['col_store_sales'] = 'مجموع مبيعات نقاط البيع';
$_['col_store_cash'] = 'الرصيد النقدي';
$_['col_store_action'] = 'اعدادات نقاط البيع';