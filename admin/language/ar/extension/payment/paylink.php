<?php
// Heading
$_['heading_title']      = 'باي لينك';

// Text
$_['text_extension']     = 'الخدمات';
$_['text_success']       = 'نجاح: لقد قمت بتعديل وحدة دفع باى لينك';
$_['text_edit']          = 'تعديل اعدادات باي لينك';
$_['text_paylink']	     = '<a href="#"><img src="view/image/payment/paylink.png" alt="paylink" title="paylink" width="35%" /></a>';
// Entry
$_['entry_total']        = 'الاجمالي المطلوب لتفعيل طريقة الدفع';
$_['entry_order_status'] = 'حالة الطلب';
$_['entry_geo_zone']     = 'المنطقة الجغرافية';
$_['entry_status']       = 'الحالة';
$_['entry_sort_order']   = 'ترتيب الفرز';

// Help
$_['help_total']         = 'يجب أن تكون سلة الشراء تحتوي على هذا المبلغ لكي تظهر طريقة الدفع في صفحة إنهاء الطلب';

// Error
$_['error_permission']   = 'تحذير : أنت لا تمتلك صلاحيات';