<?php
// Heading
$_['heading_title']					= 'تاب';

// Text
$_['text_payment']					= 'طرق الدفع';
$_['text_success']					= 'نجاح: لقد قمت بتعديل تفاصيل حساب تاب!';
$_['text_edit']                     = 'تعديل اعداادت تاب';
$_['text_tap']						= '<img src="view/image/payment/tap.png" alt="Tap" title="Tap" style="border: 1px solid #EEEEEE;" />';

// Entry
$_['entry_merchantid']				= 'Tap Merchant ID';
$_['entry_username']				= 'Tap Username';
$_['entry_password']				= 'Password';
$_['entry_apikey']					= 'Apikey';

$_['entry_total']        = 'الاجمالي المطلوب لتفعيل طريقة الدفع';
$_['entry_order_status'] = 'حالة الطلب';
$_['entry_geo_zone']     = 'المنطقة الجغرافية';
$_['entry_status']       = 'الحالة';
$_['entry_sort_order']   = 'ترتيب الفرز';

// Help
$_['help_password']					= 'فقط استخدم بعض كلمات المرور العشوائية. سيتم استخدام هذا للتأكد من عدم التدخل في معلومات الدفع بعد إرسالها إلى بوابة الدفع.';
$_['help_total']					= 'يجب أن يصل إجمالي الدفع الذي يجب أن يصل إليه الطلب قبل أن تصبح طريقة الدفع هذه نشطة.';

// Error
$_['error_permission']				= 'تحذير: ليس لديك إذن لتعديل الدفع اضغط!';
$_['error_username']				= 'اسم المستخدم مطلوب!';
$_['error_password']				= 'كلمة المرور مطلوبة!';
$_['error_merchantid']				= 'معرف التاجر مطلوب!';