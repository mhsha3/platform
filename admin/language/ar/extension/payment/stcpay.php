<?php
// Heading
$_['heading_title']      = 'اس تي سي باى';

// Text
$_['text_extension']     = 'الخدمات';
$_['text_success']       = 'نجاح: لقد قمت بتعديل اس تي سي باى للدفع!';
$_['text_edit']          = 'تعديل اعدادات اس تي سي باى';
$_['text_stcpay']	     = '<a href="#"><img src="view/image/stcpay.png" alt="StcPay" title="StcPay" width="35%" /></a>';
// Entry
$_['entry_total']        = 'الاجمالي المطلوب لتفعيل طريقة الدفع';
$_['entry_order_status'] = 'حالة الطلب';
$_['entry_geo_zone']     = 'المنطقة الجغرافية';
$_['entry_status']       = 'الحالة';
$_['entry_sort_order']   = 'ترتيب الفرز';

// Help
$_['help_total']         = 'يجب أن تكون سلة الشراء تحتوي على هذا المبلغ لكي تظهر طريقة الدفع في صفحة إنهاء الطلب';

// Error
$_['error_permission']   = 'تحذير : أنت لا تمتلك صلاحيات التعديل !';
