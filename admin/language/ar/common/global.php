<?php
$_['text_subscription']  = 'الاشتراكات';
$_['text_sar'] = 'ريال';
$_['text_month_package'] = 'شهرى';
$_['text_year_package'] = 'سنوى';
$_['text_info_package'] = 'رعاية شاملة تبدأ من العناية بـإطلاق متجرك الالكتروني مع فريق لنا و التأكد من ربطك مع خدمات شركائنا يساعدونك في تحقيق خطة النمو و انتهاءا بالجلسات الاستشارية الشهرية لمتابعة اداء متجرك
';
$_['text_about_package'] = 'باقات من لنا تناسب احتيجاتكم';
$_['text_subscribe'] = 'اشترك';
$_['text_invalid_phone_number'] = 'الرقم الجوال الذى ادخلتة خطأ';
$_['text_invalid_code_activation'] = 'كود التاكيد خطأ';
$_['text_done_paid'] = 'تهانينا تمت عملية الدفع بنجاح';
$_['text_invalid_paid'] = 'لم تتم عملية الدفع بنجاح';
$_['text_enter_4_code'] = 'برجاء كتابة الكود المرسل لك لاتمام عملية الدفع';
$_['text_pay_by_stc'] = 'الدفع عن طريق STCPay';
$_['text_send_code'] = 'ارسال كود التاكيد';
$_['text_pay'] = 'أدفع الان';
$_['text_platform_updates'] = 'تحديثات المنصة';
$_['text_like'] = 'أعجبنى';
$_['text_dislike'] = 'لم يعجبنى';
$_['text_what_opnion'] = 'ما رائيك ؟';
$_['text_header_notice'] = 'البنر العلوى';
$_['text_link'] = 'الرابط';
$_['text_add_text_arabic'] = 'أدخل النص باللغة العربية';
$_['text_add_text_english'] = 'أدخل النص بال English';
$_['text_add'] = 'أضافة';
$_['text_please_enter_all_data'] = 'من فضلك ادخل كافة البيانات المطلوبة';
$_['text_status'] = 'الحالة';
$_['text_avaliable'] = 'متاح';
$_['text_unavaliable'] = 'غير متاح';
$_['text_done_add_header_notice'] = 'تم اضافة البنر العلوى بنحاج';
$_['text_store_type'] = 'فثة المتجر';
$_['text_doc_single'] = 'فردى';
$_['text_doc_company'] = 'منشأة';
$_['text_add_docs'] = 'يرجاء ارقاق الوثائق هنا ';
$_['text_documents'] = 'الوثائق';
$_['text_done_upload_doc'] = 'تم رفع الوثائق بنجاح و هي الان قيد المراجعة';

$_['text_price'] = 'السعر';
$_['text_done_add_package'] = 'تم اضافة الباقة بنحاج';
$_['text_sort_by'] = 'الترتيب';
$_['text_badge'] = 'الوسام';
$_['text_add_new_input'] = 'أضافة حقل جديد';
$_['text_add_input_name'] = 'ادخل اسم الحقل';
$_['text_add_package'] = 'أضافة باقة جديدة';
$_['text_add_description_arabic'] = 'أدخل الوصف باللغة العربية';
$_['text_add_description_english'] = 'أدخل الوصف بال English';
$_['text_packages'] = 'الباقات';
$_['text_action'] = 'الخيارات';
$_['text_package_title'] = 'أسم الباقة';
$_['text_recommended'] = 'يوصى بها';
$_['text_settings'] = 'الاعدادات';
$_['text_orders'] = 'الطلبات';
$_['text_done_setting'] = 'تم حفظ التعديلات بنجاح';
$_['text_date'] = 'التاريخ';

$_['text_notifications_center'] = 'مركز الاشعارات';
$_['text_not_found_notifications'] = 'لا توجد اشعارات';
$_['text_new'] = 'جديدة';

$_['text_faq'] = 'الاسئلة العشوائية';
$_['text_help_center'] = 'مركز المساعدة';

?>
