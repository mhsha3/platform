<?php
$_['services']    = 'الخدمات';
$_['payment_methods']    = 'طرق الدفع';
$_['shipping_methods']    = 'طرق الشحن';
$_['text_service_add'] = 'اضافة خدمة';
$_['text_service_domain'] = 'طلب دومين جديد';
$_['text_info_domain'] = 'يمكنك من هنا شراء دومين جديد لمتجرك';


$_['text_service_design'] = 'طلب تصميم بنرات';
$_['text_info_design'] = 'من هذه الخانة تستطيع طلب تصميم بنرات';


$_['text_service_help'] = 'طلب استشارة';
$_['text_info_help'] = 'اطلب المساعدة من مستشار خبير التجارة ';


$_['text_service_dropshipping'] = ' طلب منتجات دروب شيبنج ';
$_['text_info_dropshipping'] = 'اطلب المنتجات وراح نزودك فيها';


$_['text_service_pos'] = ' طلب نقاط بيع';
$_['text_info_pos'] = 'عندك محل لنا تعطيك نقاط بيع';


$_['text_service_photosession'] = 'طلب تصوير منتجات';
$_['text_info_photosession'] = 'نجيب لك مصور لمنتجاتك ';


$_['text_service_socailmedia'] = 'طلب ادارة وسائل التواصل';
$_['text_info_socailmedia'] = 'لنا تقدم لك ادارة لحسابك في السوشل ميديا ';


$_['text_service_appilcation'] = 'طلب تطبيق جوال';
$_['text_info_application'] = ' اطلب تطبيق لمتجرك واستلم خلال 14 يوم ';


$_['text_service_design'] = 'طلب تصميم بنرات';
$_['text_info_design'] = 'من هذه الخانة تستطيع طلب تصميم بنرات';


$_['text_settings'] = "الاعدادات";

