<?php
// Heading
$_['heading_title']    = 'لوحة التحكم';

// Error
$_['error_install']    = 'تحذير: لم يتم حذف مجلد التنصيب - ويجب حذفه لأمان المتجر!';
$_['Make_Store_Settings']    = 'ابداء تجهيز متجرك الجديد';
$_['Store_Settings']    = 'اعداد المتجر';
$_['Add_Product']    = 'اضافة المنتجات';
$_['Shipping_Methods']    = 'طرق الشحن';
$_['Payment_Methods']    = 'طريقة الدفع';
$_['lanamass']    = 'اهلا بكم في عالم لنا';
$_['lanamass1']    = 'نسعى لتميز خدماتنا من أجلكم';
$_['text_renew_package_msg'] = 'الرجاء تجديد الاشتراك حتي لا تتوقف الخدمة لديكم للتجديد <a href="subscription_url">  &nbsp; أضعط هنا  &nbsp;</a> ';
$_['text_refuse_documents_msg'] = ' الرجاء اعادة رفع الوثائق لانها لم تستوفى الشروط المطلوبة <a href="documents_url">   &nbsp;أضعط هنا   &nbsp;</a>';
$_['text_upload_documents_msg'] = ' الرجاء رفع الوثائق المطلوبة <a href="documents_url">  &nbsp; أضعط هنا  &nbsp; </a>';
$_['text_online_now'] = 'المتواجدون الآن';
$_['text_total_customer'] = 'عدد العملاء';
$_['text_total_orders'] = 'أجمالى الطلبات';
$_['text_total_sales'] = 'أجمالى المبيعات';
$_['text_quick_statics'] = 'تقارير سريعة';
$_['text_currency'] = 'ريال';
$_['text_recent_orders'] = 'احدث الطلبات';
$_['text_order'] = 'طلب';
$_['text_not_found_orders'] = 'لا توجد طلبات لديك !';
$_['text_total'] = 'الاجمالى';
$_['text_customers_reviews'] = 'تقيمات العملاء';
$_['text_customers_activity'] = 'نشاطات العملاء';
$_['text_loss_product'] = 'منتجات نفذت كمياتها';
$_['text_not_found_loss_product'] = 'لا توجد منتجات نفذت كمياتها لديك ';
$_['text_not_found_customer_activity'] = 'لا توجد نشاطات عملاء لديك';

