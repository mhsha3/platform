<?php
// Heading
$_['heading_title']    					= ' سوق متعدد البائعين';

// Text
$_['text_edit']							= 'إعدادات البائعين المتعددة';
$_['text_success']           			= 'نجاح: لقد قمت بتعديل إعدادات البائعين المتعددين!';
$_['text_store']						= 'متجر';
$_['text_show']							= 'تبين';
$_['text_hide']							= 'إخفاء';

// Tabs
$_['tab_general'] 						= 'الإعدادات';
$_['tab_itemsperpage'] 					= 'مواد لكل صفحة';
$_['tab_image_size'] 					= 'أبعاد الصورة';
$_['tab_info'] 							= 'معلومات البائع';
$_['tab_modulepoints'] 					= 'الدعم';

// Button
$_['button_savechanges'] 	 			= 'حفظ التغييرات';

// Entry
$_['entry_status']						= 'حالة التمديد';
$_['entry_applyseller_page']			= 'زر البائع?';
$_['entry_received_commission_status']  = 'عمولة على النظام';
$_['entry_commission_rate']  			= 'معدل عمولة البائع';
$_['entry_autoapproved_seller']  		= 'لتصبح الموافقة على الحساب البائع تلقائيا؟';
$_['entry_restrict_orderstatus']  		= 'منع البائع من تغيير حالة الطلب. عندما يقوم المشرف بتغيير حالة الطلب إلى؟';
$_['entry_limit_seller']  				= 'العناصر لكل صفحة لصفحات البائع';
$_['entry_limit_store']  				= 'قائمة المتجر لكل صفحة';
$_['entry_limit_store_product']  		= 'قائمة منتجات المتجر لكل صفحة';
$_['entry_limit_store_review']  		= 'قائمة مراجعات المتجر لكل صفحة';
$_['entry_main_banner_size']  			= 'حجم البانر <br> (W x H)';
$_['entry_width']                      	= 'عرض';
$_['entry_height']                     	= 'ارتفاع';
$_['entry_store_logo_size']            	= 'حجم شعار المتجر <br> (W x H)';
$_['entry_profile_image_size']          = 'حجم صورة الملف الشخصي <br> (W x H)';
$_['entry_main_banner_size_listing']	= 'حجم البانر <br> (W x H)';
$_['entry_profile_image_size_listing']	= 'حجم صورة الملف الشخصي <br> (W x H)';
$_['entry_seller_name']					= 'إظهار اسم البائع';
$_['entry_seller_email']				= 'إظهار البريد الإلكتروني للبائع';
$_['entry_seller_telephone']			= 'أظهر رقم هاتف البائع';
$_['entry_seller_address']				= 'إظهار عنوان البائع';
$_['entry_seller_image']				= 'عرض صورة الملف الشخصي للبائع';
$_['entry_seller_changereview']			= 'هل تريد السماح للبائع بتغيير حالة المراجعة على متجره؟';

// Legend
$_['legend_seller_info']          		= 'صفحة معلومات البائع';
$_['legend_seller_listing']          	= 'صفحة قائمة البائعين';

// Help
$_['help_restrict_orderstatus']			= 'قم بتقييد البائعين لتغيير حالة طلباتهم بمجرد اكتمال تحديث حالة الطلب من قبل المسؤول. يمكنك تحديد تماثيل ترتيب متعددة. عندما يقوم المشرف بتغيير حالة الطلب وحالة الطلب الجديدة في حالة الطلب المحددة هذه. بعد ذلك لا يمكن للبائعين تغيير حالة الطلب.';
$_['help_received_commission_status']	= 'حدد حالة الطلب الذي يتلقى فيه البائع عمولة لبيع منتجاتهم.';
$_['help_commission_rate']				= 'تحديد سعر عمولة البائع لبيع منتجات البائع. يمكنك الحد الأقصى أو الحد الأدنى الذي تريده.';

// Error
$_['error_permission'] 				  	= 'تحذير: ليس لديك إذن بتعديل الوحدة النمطية متعددة البائعين!';
$_['error_warning']          			= 'تحذير: يرجى التحقق من النموذج بعناية للأخطاء!';
$_['error_commission_rate']          	= 'سعر العمولة مطلوب!';
$_['error_restrict_orderstatus']        = 'حالة الطلب مطلوبة!';
$_['error_limit_seller']          		= 'مطلوب حد صفحة البائع!';
$_['error_limit_store']          		= 'حد صفحة المتجر مطلوب!';
$_['error_limit_store_product'] 		= 'مطلوب حد صفحة منتجات المتجر!';
$_['error_limit_store_review'] 			= 'مطلوب حد صفحة مراجعات المتجر!';
$_['error_main_banner_size'] 			= 'مطلوب حجم البانر الرئيسي!';
$_['error_store_logo_size'] 			= 'حجم شعار المتجر مطلوب!';
$_['error_profile_image_size'] 			= 'حجم صورة الملف الشخصي مطلوب!';
