<?php
// Heading
$_['heading_title']          = 'استفسارات العملاء البائعين';
$_['enquiry_title']          = 'استفسار بواسطة %s';

// Button
$_['button_cancel']			 = 'عودة';

// Text
$_['text_success']           = 'نجاح: لقد قمت بتعديل استفسارات بائع العملاء!';
$_['text_list']              = 'قائمة الاستفسارات';
$_['text_list_info']         = 'معلومات الاستفسار';
$_['text_default']           = 'إفتراضي';
$_['text_customer_type']     = 'الزبون';
$_['text_seller_type']       = 'تاجر';

$_['text_contact_details']	 = 'بيانات المتصل';
$_['text_date_added']		 = 'مكون';
$_['text_date_modified']	 = 'النشاط الاخير';
$_['text_enquiry_details']	 = 'استعلام:';
$_['text_seller_details']	 = 'تفاصيل البائع';

// Column
$_['column_store_owner']  	 = 'تاجر';
$_['column_customer_name']   = 'الزبون';
$_['column_customer_email']  = 'البريد الإلكتروني للعميل';
$_['column_date_added']		 = 'مكون';
$_['column_date_modified']	 = 'النشاط الاخير';
$_['column_action']          = 'عمل';

// Entry
$_['entry_title']            	= 'عنوان الاستفسار';
$_['entry_store_owner']			= 'تاجر';
$_['entry_customer_name']		= 'الزبون';
$_['entry_customer_email']		= 'البريد الإلكتروني للعميل';
$_['entry_name']   				= 'اسم';
$_['entry_email']  				= 'البريد الإلكتروني';
$_['entry_message']				= 'رسالة';
$_['entry_date_added']			= 'تم إضافة التاريخ';
$_['entry_seller_name']			= 'اسم البائع';
$_['entry_store_name']			= 'اسم المتجر';
$_['entry_seller_email']		= 'البريد الإلكتروني للبائع';



// Error
$_['error_warning']          = 'تحذير: يرجى التحقق من النموذج بعناية للأخطاء!';
$_['error_permission']       = 'تحذير: ليس لديك إذن بتعديل استفسارات بائع العملاء!';
