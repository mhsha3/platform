<?php
// Heading
$_['heading_title']          = 'عمولة المنتج';

// Text
$_['text_success']           = 'تم تعديل عمولة المنتج بنجاح';
$_['text_list']              = 'قائمة عمولات المنتجات';
$_['text_default']           = 'الإفتراضي';
$_['text_final_commission']           = 'مجموع العمولات';

// Column
$_['column_store_owner']  	 = 'اسم التجار';
$_['column_store_name']  	 = 'اسم المتجر';
$_['column_date_added']		 = 'تاريخ الإضافة';
$_['column_action']          = 'خيارات';
$_['column_order_id']		 = 'رقم الطلب';
$_['column_product_name']	 = 'اسم المنتج';
$_['column_quantity']		 = 'الكمية';
$_['column_price']			 = 'السعر';
$_['column_amount']		 	 = 'العمولة';
$_['column_total']			 = 'الاجمالى';

// Entry
$_['entry_store_owner']			= 'البائع';
$_['entry_date_start']			= 'التاريخ من';
$_['entry_date_end']			= 'التاريخ الى';


// Error
$_['error_warning']          = 'تحذير: يرجى التحقق من النموذج بعناية لمعرفة الأخطاء!';
$_['error_permission']       = 'تحذير: ليس لديك إذن لتعديل عمولة المنتج!';
