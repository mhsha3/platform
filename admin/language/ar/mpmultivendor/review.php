<?php
// Heading
$_['heading_title']     = 'مراجعات البائع';

// Text
$_['text_success']      = 'نجاح: لقد قمت بتعديل مراجعات البائع!';
$_['text_list']         = 'قائمة مراجعة البائع';
$_['text_add']          = 'أضف مراجعة البائع';
$_['text_edit']         = 'تعديل مراجعة البائع';

// Column
$_['column_store_owner']= 'تاجر';
$_['column_author']     = 'مؤلف';
$_['column_title']      = 'عنوان';
$_['column_rating']     = 'تقييم';
$_['column_status']     = 'الحالة';
$_['column_date_added'] = 'تم إضافة التاريخ';
$_['column_action']     = 'عمل';

// Entry
$_['entry_store_owner'] = 'تاجر';
$_['entry_title']       = 'عنوان';
$_['entry_description'] = 'وصف';
$_['entry_author']      = 'مؤلف';
$_['entry_rating']      = 'تقييم';
$_['entry_status']      = 'الحالة';
$_['entry_date_added']  = 'تم إضافة التاريخ';

// Help
$_['help_store_owner']	= '(الإكمال التلقائي)';

// Error
$_['error_permission']  = 'تحذير: ليس لديك إذن بتعديل مراجعات البائع!';
$_['error_store_owner'] = 'مطلوب بائع!';
$_['error_title'] 		= 'يجب أن يكون العنوان بين 3 و 255 حرفاً!';
$_['error_author']      = 'يجب أن يكون المؤلف بين 3 و 64 حرفاً!';
$_['error_text']        = 'يجب أن يكون وصف المراجعة حرفًا واحدًا على الأقل!';
$_['error_rating']      = 'مطلوب مراجعة التقييم!';
