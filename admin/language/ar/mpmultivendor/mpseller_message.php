<?php
// Heading
$_['heading_title']          = 'استفسارات البائع';

// Text
$_['text_success']           = 'نجاح: لقد قمت بتعديل استفسارات البائع!';
$_['text_list']              = 'قائمة استفسار البائع';
$_['text_form']              = 'أرسل رسالة إلى %s <b>(%s)</b>';
$_['text_default']           = 'إفتراضي';
$_['text_administrator']     = 'مدير';
$_['text_success_sent']    	 = 'تم إرسال رسالتك بنجاح.';

// Buttons
$_['button_send']			 = 'أرسل رسالة';
$_['button_cancel']			 = 'عودة';
$_['button_filter']			 = 'منقي';

// Column
$_['column_seller_name']     = 'تاجر';
$_['column_store_name']	     = 'اسم المتجر';
$_['column_message']	     = 'اخر رسالة';
$_['column_date_added']	     = 'النشاط الاخير';
$_['column_action']          = 'عمل';

// Entry
$_['entry_message']          = 'رسالة';
$_['entry_store_owner']      = 'اسم البائع';
$_['entry_store_name']       = 'اسم المتجر';

// Error
$_['error_message'] 	   		= 'تحذير: يجب ألا يقل نص الرسالة عن حرفين!';
