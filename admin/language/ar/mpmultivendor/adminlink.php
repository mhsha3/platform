<?php
// Menu
$_['text_mpmultivendor']    = 'سوق التجار المتعددين';
$_['text_mpsetting']    	= 'الإعدادات';
$_['text_mpseller']    	 	= 'البائعين';
$_['text_seller_orders'] 	= 'الطلبات';
$_['text_seller_commissions']= 'الارباح والعمولات';
$_['text_seller_payments']  = 'التحويل للتجار';
$_['text_seller_products']  = 'المنتجات';
$_['text_seller_enquiries'] = 'استفسارات العملاء';
$_['text_seller_message'] 	= 'استفسارات التجار';
$_['text_seller_review'] 	= 'التقيمات';

$_['column_store_owner'] 	= 'اسم التاجر';

// Total
$_['total_unreads_messages'] 	= ' &nbsp; <label class="label label-danger" style="font-size: 11px;">%s</label>';