<?php
// Heading
$_['heading_title']          = 'منتجات';

// Text
$_['text_success']           = 'نجاح: لقد قمت بتعديل المنتجات!';
$_['text_list']              = 'قائمة المنتجات';
$_['text_add']               = 'أضف منتج';
$_['text_edit']              = 'تحرير المنتج';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'إفتراضي';
$_['text_option']            = 'اختيار';
$_['text_option_value']      = 'قيمة الخيار';
$_['text_percent']           = 'النسبة المئوية';
$_['text_amount']            = 'مبلغ ثابت';
$_['text_keyword']           = 'لا تستخدم المسافات ، وبدلاً من ذلك استبدل المسافات بـ - وتأكد من أن عنوان URL لتحسين محركات البحث فريد عالميًا.';

// Column
$_['column_name']            = 'اسم المنتج';
$_['column_store_owner']     = 'مالك المحل';
$_['column_model']           = 'نموذج';
$_['column_image']           = 'صورة';
$_['column_price']           = 'السعر';
$_['column_quantity']        = 'كمية';
$_['column_status']          = 'الحالة';
$_['column_action']          = 'عمل';

// Entry
$_['entry_name']             = 'اسم المنتج';
$_['entry_description']      = 'وصف';
$_['entry_meta_title'] 	     = 'عنوان علامة التعريف';
$_['entry_meta_keyword'] 	 = 'الكلمات المفتاحية لعلامة التعريف';
$_['entry_meta_description'] = 'وصف علامة التعريف';
$_['entry_keyword']          = 'SEO URL';
$_['entry_model']            = 'النوع';
$_['entry_sku']              = 'SKU';
$_['entry_upc']              = 'UPC';
$_['entry_ean']              = 'EAN';
$_['entry_jan']              = 'JAN';
$_['entry_isbn']             = 'ISBN';
$_['entry_mpn']              = 'MPN';
$_['entry_location']         = 'موقعك';
$_['entry_shipping']         = 'يتطلب شحن';
$_['entry_manufacturer']     = 'الصانع';
$_['entry_store']            = 'المتاجر';
$_['entry_date_available']   = 'التاريخ المتوفر';
$_['entry_quantity']         = 'كمية';
$_['entry_minimum']          = 'اقل كمية';
$_['entry_stock_status']     = 'نفذت الكمية';
$_['entry_price']            = 'السعر';
$_['entry_tax_class']        = 'فئة الضريبة';
$_['entry_points']           = 'نقاط';
$_['entry_option_points']    = 'نقاط';
$_['entry_subtract']         = 'طرح الأوراق المالية';
$_['entry_weight_class']     = 'فئة الوزن';
$_['entry_weight']           = 'وزن';
$_['entry_dimension']        = 'الأبعاد (L x W x H)';
$_['entry_length_class']     = 'فئة الطول';
$_['entry_length']           = 'الطول';
$_['entry_width']            = 'عرض';
$_['entry_height']           = 'ارتفاع';
$_['entry_image']            = 'صورة';
$_['entry_additional_image'] = 'صور إضافية';
$_['entry_customer_group']   = 'مجموعة العملاء';
$_['entry_date_start']       = 'تاريخ البدء';
$_['entry_date_end']         = 'نهاية التاريخ';
$_['entry_priority']         = 'أفضلية';
$_['entry_attribute']        = 'ينسب';
$_['entry_attribute_group']  = 'مجموعة السمات';
$_['entry_text']             = 'نص';
$_['entry_option']           = 'اختيار';
$_['entry_option_value']     = 'قيمة الخيار';
$_['entry_required']         = 'مطلوب';
$_['entry_status']           = 'الحالة';
$_['entry_sort_order']       = 'امر ترتيب';
$_['entry_category']         = 'التصنيفات';
$_['entry_filter']           = 'مرشحات';
$_['entry_download']         = 'التحميلات';
$_['entry_related']          = 'منتجات ذات صله';
$_['entry_tag']          	 = 'علامات المنتج';
$_['entry_reward']           = 'نقاط مكافأة';
$_['entry_layout']           = 'تجاوز التخطيط';
$_['entry_recurring']        = 'الملف الشخصي المتكرر';
$_['entry_mpseller']		 = 'تاجر';
$_['entry_store_owner']		 = 'تاجر';

// Help
$_['help_keyword']           = 'لا تستخدم المسافات ، وبدلاً من ذلك استبدل المسافات بـ - وتأكد من أن عنوان URL لتحسين محركات البحث فريد عالميًا.';
$_['help_sku']               = 'وحدة حفظ الأوراق المالية';
$_['help_upc']               = 'رمز المنتج العالمي';
$_['help_ean']               = 'رقم المادة الأوروبية';
$_['help_jan']               = 'رقم المقالة اليابانية';
$_['help_isbn']              = 'الرقم العالمي الموحد للكتاب';
$_['help_mpn']               = 'رقم جزء الشركة المصنعة';
$_['help_manufacturer']      = '(الإكمال التلقائي)';
$_['help_minimum']           = 'فرض الحد الأدنى من المبلغ المطلوب';
$_['help_stock_status']      = 'تظهر الحالة عندما يكون المنتج غير متوفر';
$_['help_points']            = 'عدد النقاط اللازمة لشراء هذا البند. إذا كنت لا تريد شراء هذا المنتج بنقاط ، اتركه على أنه 0.';
$_['help_category']          = '(إكمال تلقائي)';
$_['help_filter']            = '(إكمال تلقائي)';
$_['help_download']          = '(إكمال تلقائي)';
$_['help_related']           = '(إكمال تلقائي)';
$_['help_tag']               = 'مفصولة بفواصل';

// Error
$_['error_warning']          = 'تحذير: يرجى التحقق من النموذج بعناية للأخطاء!';
$_['error_permission']       = 'تحذير: ليس لديك إذن بتعديل المنتجات!';
$_['error_name']             = 'يجب أن يكون اسم المنتج اكثر من 3 أحرف وأقل من 255 حرفاً!';
$_['error_meta_title']       = 'يجب أن يكون العنوان التعريفي اكثر من 3 أحرف وأقل من 255 حرفًا!';
$_['error_model']            = 'يجب أن يكون نموذج المنتج اكثر من 1 وأقل من 64 حرفا!';
$_['error_keyword']          = 'URL SEO قيد الاستخدام بالفعل!';
$_['error_unique']           = 'يجب أن يكون SEO URL فريدًا!';
