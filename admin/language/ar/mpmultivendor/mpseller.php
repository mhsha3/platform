<?php
// Heading
$_['heading_title']         = 'الباعة';

// Text
$_['text_success']          = 'نجاح: لقد قمت بتعديل البائعين!';
$_['text_list']             = 'قائمة البائع';
$_['text_add']              = 'أضف بائع';
$_['text_edit']             = 'تحرير البائع';
$_['text_default']          = 'إفتراضي';
$_['text_order_wise']    	= 'اطلب Wise';
$_['text_product_wise']  	= 'المنتج الحكيم';
$_['text_paypal']        	= 'باي بال';
$_['text_bank_transfer'] 	= 'التحويل المصرفي';
$_['text_cheque']        	= 'التحقق من';
$_['text_paid']        		= 'المبلغ المدفوع';
$_['text_total_commission'] = 'مجموع العمولة';
$_['text_total_paid'] 		= 'مجموع المبالغ المدفوعة';
$_['text_total_balance'] 	= 'الرصيد المتوفر';
$_['text_default']       = 'إفتراضي';
$_['text_keyword']       = 'لا تستخدم المسافات ، وبدلاً من ذلك استبدل المسافات بـ - وتأكد من أن عنوان URL لتحسين محركات البحث فريد عالميًا.';

// Buttons
$_['button_products']		= 'منتجات';
$_['button_enquiries']		= 'استفسارات العملاء';
$_['button_reviews']		= 'مراجعات الملف الشخصي';
$_['button_orders']			= 'الطلب';
$_['button_commission']		= 'عمولة';
$_['button_transaction']	= 'المعاملات';
$_['button_transaction']	= 'المعاملات';
$_['button_login']          = 'تسجيل الدخول إلى المتجر';
$_['button_message']		= 'أرسل رسالة';

// Tab
$_['tab_general']			= 'جنرال لواء';
$_['tab_store']				= 'متجر';
$_['tab_local']				= 'محلي';
$_['tab_image']				= 'صورة';
$_['tab_social_profiles']	= 'لمحات اجتماعية';
$_['tab_shipping']			= 'اعدادات الشحن';
$_['tab_payment']			= 'إعداد الدفع';
$_['tab_commission']		= 'تعيين العمولة';
$_['tab_transaction']		= 'المعاملات';
$_['tab_seo']		= 'SEO URL';

// Column
$_['column_store_owner']    = 'اسم البائع';
$_['column_store_name']    	= 'اسم المتجر';
$_['column_products']    	= 'منتجات';
$_['column_email']          = 'البريد الإلكتروني';
$_['column_status']         = 'الحالة';
$_['column_date_added']     = 'تم إضافة التاريخ';
$_['column_action']         = 'عمل';
$_['column_amount']     	= 'كمية';

// Entry
$_['entry_description']     = 'وصف';
$_['entry_meta_description']= 'ميتا الوصف';
$_['entry_meta_keyword']    = 'كلمة تعريف ميتا';
$_['entry_store_owner'] 	= 'اسم البائع';
$_['entry_store_name']		= 'اسم المتجر';
$_['entry_email']           = 'البريد الإلكتروني';
$_['entry_telephone']       = 'هاتف';
$_['entry_address']         = 'عنوان';
$_['entry_image'] 			= 'الصوره الشخصيه';
$_['entry_logo'] 			= 'شعار';
$_['entry_banner'] 			= 'بانر';
$_['entry_city']    		= 'مدينة';
$_['entry_country']    		= 'دولة';
$_['entry_zone']    		= 'منطقة';
$_['entry_facebook']		= 'موقع التواصل الاجتماعي الفيسبوك';
$_['entry_google_plus']		= 'جوجل بلس';
$_['entry_twitter']			= 'تويتر';
$_['entry_pinterest']		= 'بينتيريست';
$_['entry_linkedin']		= 'لينكد إن';
$_['entry_youtube']			= 'Youtube';
$_['entry_instagram']		= 'انستغرام';
$_['entry_flickr']			= 'فليكر';
$_['entry_status']          = 'الحالة';
$_['entry_approved']        = 'وافق';
$_['entry_date_added']      = 'تم إضافة التاريخ';
$_['entry_shipping_type']   = 'نوع الشحن';
$_['entry_shipping_amount'] = 'كمية الشحن';
$_['entry_payment_type']    = 'نوع الدفع';
$_['entry_paypal_email']    = 'بريد باي بال';
$_['entry_bank_details']    = 'التفاصيل المصرفية';
$_['entry_cheque_payee']    = 'تحقق من اسم المدفوع لأمره';
$_['entry_fax']    			= 'فاكس';
$_['entry_seo_keyword']		= 'Seo Keyword لصفحة معلومات البائع';
$_['entry_review_seo_keyword']	= 'Seo Keyword لصفحة المراجعة';
$_['entry_commission_rate'] = 'نسبة العمولة';
$_['entry_amount']     		= 'كمية';
$_['entry_store']     		= 'متجر';

// Error
$_['error_warning']         = 'تحذير: يرجى التحقق من النموذج بعناية للأخطاء!';
$_['error_permission']      = 'تحذير: ليس لديك إذن بتعديل البائعين!';

$_['error_status_disabled'] = 'تحذير: تم تعليق حساب البائع الخاص بك!';
$_['error_store_owner']     = 'يجب أن يكون اسم البائع بين 3 و 64 حرفًا!';
$_['error_email']          	= 'لا يبدو أن عنوان البريد الإلكتروني صالح!';
$_['error_telephone']      	= 'يجب أن يكون الهاتف بين 3 و 32 حرفًا!';
$_['error_address']        	= 'يجب أن يكون العنوان بين 3 و 128 حرفاً!';
$_['error_city']           	= 'يجب أن تكون المدينة بين 2 و 128 حرفاً!';
$_['error_country']        	= 'رجاء قم بإختيار دوله!';
$_['error_zone']           	= 'يرجى تحديد منطقة / ومدينة!';
$_['error_description']    	= 'الوصف مطلوب!';
$_['error_meta_description'] = 'الوصف التعريفي مطلوب!';
$_['error_meta_title']     	= 'يجب أن يكون عنوان Meta بين 3 و 32 حرفًا!';
$_['error_store_name']   	= 'يجب أن يكون اسم المتجر بين 3 و 255 حرفاً!';
$_['error_meta_keyword']   	= 'مطلوب كلمة تعريف ميتا!';
$_['error_commission_rate'] = 'سعر العمولة مطلوب!';
$_['error_seo_keyword']     = 'URL SEO قيد الاستخدام بالفعل!';
$_['error_review_seo_keyword']     = 'URL SEO قيد الاستخدام بالفعل!';
$_['error_keyword']          = 'URL SEO قيد الاستخدام بالفعل!';
$_['error_unique']           = 'يجب أن يكون SEO URL فريدًا!';
