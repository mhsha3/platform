<?php
// Heading
$_['heading_title']          = 'المعاملات';

// Text
$_['text_success']           = 'نجاح: لقد قمت بتعديل المعاملات!';
$_['text_list']              = 'قائمة المعاملات';
$_['text_default']           = 'الافتراضى';
$_['text_final_paid']  		 = 'اجمالى المدفوع';

// Column
$_['column_store_owner']  	 = 'اسم البائع';
$_['column_store_name']  	 = 'اسم المتجر';
$_['column_date_added']		 = 'تاريخ الاضافة';
$_['column_action']          = 'خيارات';
$_['column_amount']		 	 = 'المبلغ';

// Entry
$_['entry_store_owner']			= 'البائع';
$_['entry_date_start']			= 'التاريخ من';
$_['entry_date_end']			= 'التاريخ الى';


// Error
$_['error_warning']          = 'تحذير: يرجى التحقق من النموذج بعناية لمعرفة الأخطاء!';
$_['error_permission']       = 'تحذير: ليس لديك إذن لتعديل المعاملات!';
