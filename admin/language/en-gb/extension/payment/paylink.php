<?php
// Heading
$_['heading_title']      = 'OnlinePay';

// Text
$_['text_extension']     = 'Extensions';
$_['text_success']       = 'Success: You have modified OnlinePay payment module!';
$_['text_edit']          = 'Edit OnlinePay';
$_['text_paylink']	     = '<a href="#"><img src="view/image/payment/paylink.png" alt="OnlinePayment" title="OnlinePayment" width="35%" /></a>';
// Entry
$_['entry_total']        = 'Total';
$_['entry_order_status'] = 'Order Status';
$_['entry_geo_zone']     = 'Geo Zone';
$_['entry_status']       = 'Status';
$_['entry_sort_order']   = 'Sort Order';

// Help
$_['help_total']         = 'The checkout total the order must reach before this payment method becomes active.';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify payment OnlinePay!';