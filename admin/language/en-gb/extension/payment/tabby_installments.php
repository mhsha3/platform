<?php
// Heading
$_['heading_title']		 				= 'Tabby Installments';

// Text
$_['text_tabby_installments']			= '<a target="_BLANK" href="https://www.tabby.ai/"><img src="view/image/payment/tabby.png" alt="Tabby Pay Later" title="Tabby" style="max-width: 90px;" /></a>';
$_['text_extensions']     				= 'Extensions';
$_['text_edit']          				= 'Edit Tabby Installments';

$_['text_main']          				= 'Tabby Installments';
// Entry
$_['entry_status']		 				= 'Status';
$_['entry_order_status']                = 'Order Status';
$_['entry_geo_zone']                    = 'Geo Zone';
$_['entry_sort_order']	 				= 'Sort Order';
$_['entry_title']	 				    = 'Method Title';
$_['entry_terms']	 				    = 'Method Terms';

// Success
$_['success_save']		 				= 'Success: You have modified Tabby Installments configuration!';

// Error
$_['error_permission']	 				= 'Warning: You do not have permission to modify payment Tabby Installments!';
