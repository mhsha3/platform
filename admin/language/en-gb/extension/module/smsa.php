<?php
// Heading
$_['heading_title']    		= 'SMSA Shipping';

// Text
$_['text_module']      		= 'Modules';
$_['text_extension']      	= 'Extension';
$_['text_success']     		= 'Success: You have modified SMSA Shipping module!';
$_['text_edit']        		= 'Edit SMSA Shipping Module';
$_['text_live']        		= 'Live Mode';
$_['text_test']        		= 'Test Mode';

// Entry
$_['entry_smsa_cod']     	= 'COD Payment Methods';
$_['entry_passkey']      	= 'Passkey';
$_['entry_password']      	= 'Password';
$_['entry_rate']      	    = 'Rate';
$_['entry_mode']    		= 'API Environment';
$_['entry_username']     	= 'Username';
$_['entry_name']     	    = 'Name';
$_['entry_contact']     	= 'Contact Person';
$_['entry_postcode']        = 'Pincode';
$_['entry_address_1']     	= 'Address1';
$_['entry_address_2']     	= 'Address2';
$_['entry_zone']     	    = 'Area Code';
$_['entry_city']     	    = 'City';
$_['entry_country']     	= 'Country';
$_['entry_telephone']     	= 'Telephone';
$_['entry_status']     		= 'Status';

$_['help_country']        	= 'Enter KSA for Saudi Arabia';
// Error
$_['error_permission']  = 'Warning: You do not have permission to modify SMSA Shipping module!';
$_['error_passkey']     = 'Passkey does not appear to be valid!';
$_['error_password']    = 'Password does not appear to be valid!';
$_['error_name']     	= 'Name must be between 3 and 32 characters!';
$_['error_username']    = 'Username must be between 3 and 32 characters!';
$_['error_telephone']   = 'Telephone must be between 3 and 32 characters!';
$_['error_address_1']   = 'Address1 must be between 3 and 32 characters!';
$_['error_address_2']   = 'Address2 must be between 3 and 32 characters!';
$_['error_postcode']    = 'Postcode does not appear to be valid!';
$_['error_city']        = 'City must be between 3 and 32 characters!';
$_['error_zone']        = 'Zone must be between 3 and 32 characters!';
$_['error_rate']        = 'Rate must be between 3 and 32 characters!';
$_['error_contact']     = 'Contact must be between 3 and 32 characters!';