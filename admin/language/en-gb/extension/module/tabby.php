<?php

// Heading
$_['heading_title']    = 'Tabby Module';


// Text
$_['text_tabby']		 				= '<a target="_BLANK" href="https://www.tabby.ai/"><img src="view/image/payment/tabby.png" alt="Tabby" title="Tabby" style="max-width: 90px;" /></a>';
$_['text_extensions']     				= 'Extensions';
$_['text_edit']          				= 'Edit Tabby';

$_['text_api']				 	 		= 'Tabby API';
$_['text_checkout']				 		= 'Checkout';
$_['text_installments']					= 'Installments';

$_['text_no_transaction']			    = 'No transaction information available';

// Entry
$_['entry_status']		 				= 'Status';
$_['entry_sort_order']	 				= 'Sort Order';
$_['entry_public_key']					= 'Public Key';
$_['entry_secret_key']					= 'Secret Key';
$_['entry_checkout_card_status']		= 'Advanced Card';
$_['entry_debug']				 		= 'Debug Logging';
$_['entry_transaction_id']              = 'Transaction #:';
$_['entry_transaction_date']            = 'Date:';
$_['entry_transaction_status']          = 'Status:';
$_['entry_transaction_amount']          = 'Amount:';
$_['entry_transaction_currency']        = 'Currency:';
$_['entry_transaction_reference_id']    = 'Reference #:';
$_['entry_captures']                    = 'Captures';
$_['entry_refunds']                     = 'Refunds';
$_['entry_parent_id']                   = 'Parent #';

// Help

// Buttons
$_['text_button_capture']               = 'Capture payment';
$_['text_button_void']                  = 'Void payment';
$_['text_button_refund']                = 'Refund';

// Success
$_['success_save']		 				= 'Success: You have modified Tabby configuration!';

// Error
$_['error_permission']	 				= 'Warning: You do not have permission to modify Tabby Module!';

$_['warning_not_configured']            = 'Please configure Tabby Module in order to use payment methods <a href="%s">here</a>.';
