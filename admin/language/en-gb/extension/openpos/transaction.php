<?php

// Heading
$_['heading_title']     = 'Transactions';

// Column
$_['column_register']   = 'Register';
$_['column_date']       = 'Date';
$_['column_by']         = 'By';
$_['column_in']         = 'In';
$_['column_out']        = 'Out';
$_['column_ref']        = 'Ref';
