<?php
$_['heading_title']    = 'Registers';
$_['heading_title_new']    = 'New Register';
$_['heading_title_edit']    = 'Edit Register';
$_['heading_pos']    = 'POS';

$_['col_name']    = 'Register Name';
$_['col_cashiers']    = 'Cashiers';
$_['col_total_sales']    = 'Total Sales';
$_['col_total_cash']    = 'Cash Balance';
$_['col_action']    = 'Action';
$_['msg_delete_confirm']    = 'Are you sure ?';

$_['label_register_name']    = 'Name';
$_['label_store']    = 'Store';
$_['label_cashier']    = 'Cashiers';
$_['label_status']    = 'Status';
$_['label_enable']    = 'Enable';
$_['label_disable']    = 'Disable';