<?php
$_['heading_title']    = 'Products';
$_['heading_pos']    = 'POS';

$_['col_id']    = 'ID';
$_['col_barcode']    = 'Barcode';
$_['col_product_name']    = 'Product';
$_['col_qty']    = 'QTY';
$_['col_action']    = 'Action';