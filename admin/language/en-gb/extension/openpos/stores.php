<?php
$_['heading_title']    = 'Setting';
$_['heading_pos']    = 'POS';

$_['msg_success']    = 'Your setting have been saved';

$_['col_store_name'] = 'Store Name';
$_['col_store_sales'] = 'Total POS Sales';
$_['col_store_cash'] = 'Cash Balance';
$_['col_store_action'] = 'POS Setting';