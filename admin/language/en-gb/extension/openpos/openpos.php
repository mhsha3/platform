<?php
// Heading
$_['heading_title']    = 'POS';

// Text
$_['text_total_order']   = 'Total Order';
$_['text_total_sales']     = 'Total Sales';
$_['text_total_cash']     = 'Cash Balance';


$_['col_customer']     = 'Customer';
$_['col_total']     = 'Total';
$_['col_created']     = 'Created At';

$_['chart_date']     = 'Date';
$_['chart_sale']     = 'Sales';


$_['menu_products']     = 'Print Barcode / Products';
$_['menu_cash']     = 'Cash Management';
$_['menu_registers']     = 'Cash Drawers';
$_['menu_setting']     = 'Setting';
$_['menu_session']     = 'Login Sessions';


$_['link_goto_pos']    = 'Goto POS';


// Entry

// Text
$_['text_op_payment_method']    = 'Paid From POS';
$_['text_op_discount']    = 'POS Discount';
$_['text_op_shipping']    = 'POS Shipping';

$_['text_op_receipt_table_item']    = 'Item';
$_['text_op_receipt_table_price']    = 'Price';
$_['text_op_receipt_table_qty']    = 'Qty';
$_['text_op_receipt_table_discount']    = 'Discount';
$_['text_op_receipt_table_total']    = 'Total';

$_['text_op_receipt_total_subtotal']    = 'Sub Total';
$_['text_op_receipt_total_shipping_total']    = 'Shipping';
$_['text_op_receipt_total_discount_total']    = 'Discount';
$_['text_op_receipt_total_tax_total']    = 'Tax';
$_['text_op_receipt_total_grand_total']    = 'Grand Total';