<?php
// Heading
$_['heading_title']                = 'Clex';

// Text
$_['text_extension']               = 'Extensions';
$_['text_success']                 = 'Success: You have modified Clex shipping!';
$_['text_edit']                    = 'Edit Clex Shipping';

// Entry
$_['entry_access_token']           = 'Access Token' ;
$_['entry_cost']                   = 'Cost';
$_['entry_status']                 = 'Status';
$_['entry_sort_order']             = 'Sort Order';

// Help
$_['help_test']                    = 'Use this module in Test (YES) or Production mode (NO)?';

// Error
$_['error_permission']             = 'Warning: You do not have permission to modify Clex shipping!';
$_['error_access_token']           = 'Access Token Required!';
