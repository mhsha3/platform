<?php
// Heading
$_['heading_title']                    = 'Clex Shipping';

// Text
$_['text_back_to_order']               = 'Back to Order';
$_['text_print_label']                 = 'Print Label';
$_['text_track']                       = 'Track Shipping';
$_['text_cancel_sipment']              = 'Cancel Shipping';
$_['text_error_heading']               = 'Error';
$_['text_shipment_details']            = 'Shipment Details';
$_['text_sender_details']              = 'Consignor Details';
$_['text_receiver_details']            = 'Consignee Details';
$_['text_shipment_information']        = 'Shipment Details';
$_['text_success']                     = 'Success: Shipment created successfully!';

$_['text_shipping_submitted_message']  = '<b>Your shipping information has been submitted.</b>';
$_['text_cancel_failed']               = 'Shipment failed to cancel';
$_['text_button_create_shipment']      = 'Create Shipment';
$_['text_checkbox_notify_customer']    = 'Notify customer by email';
$_['text_processing']                  = 'Processing...';
$_['text_ajax_error_common']           = 'An error occurred, please try again later.';
$_['text_product_item_name']           = 'Item Name';
$_['text_product_quantity']            = 'Quantity';
$_['text_no_of_products_shipped']      = 'Number of items to be shipped';
$_['text_ok']                          = 'OK';
$_['text_shipment_status']             = 'Shipment Status:';
$_['text_created_date']                = 'Created Date:';

$_['text_shipment_type_delivery']      = 'Delivery';
$_['text_shipment_type_self_pickup']   = 'Self Pickup';
$_['text_billing_type_cod']            = 'Cash On Delivery';
$_['text_billing_type_prepaid']        = 'Prepaid';
$_['text_primary_service_delivery']    = 'Delivery';
$_['text_secondary_service_insurance'] = 'Insurance';
$_['text_secondary_service_cod']       = 'Cash On Delivery';

$_['text_shipment_pieces']             = 'Shipping Packages';

// Entry
# Consignor (Sender)
$_['entry_consignor']                  = 'Consignor Name';
$_['entry_consignor_email']            = 'Consignor Email';
$_['entry_origin_city']                = 'Origin City';
$_['entry_origin_area_new']            = 'Origin Area Name';
$_['entry_consignor_street_name']      = 'Consignor Street Name';
$_['entry_consignor_building_name']    = 'Consignor Building Name';
$_['entry_consignor_address_house_appartment'] = 'Consignor Address House Appartment';
$_['entry_consignor_address_landmark']         = 'Consignor Address Landmark';
$_['entry_consignor_country_code']             = 'Consignor Country Code';
$_['entry_consignor_phone']                    = 'Consignor Phone';
$_['entry_consignor_alternate_country_code']   = 'Consignor Alternate Country Code';
$_['entry_consignor_alternate_phone']          = 'Consignor Alternate Phone';

# Consignee (Receiver)
$_['entry_consignee']                  = 'Consignee Name';
$_['entry_consignee_email']            = 'Consignee Email';
$_['entry_destination_city']           = 'Destination City';
$_['entry_destination_area_new']       = 'Destination Area Name';
$_['entry_consignee_street_name']      = 'Consignee Street Name';
$_['entry_consignee_building_name']    = 'Consignee Building Name';
$_['entry_consignee_address_house_appartment'] = 'Consignee Address House Appartment';
$_['entry_consignee_address_landmark']         = 'Consignee Address Landmark';
$_['entry_consignee_country_code']             = 'Consignee Country Code';
$_['entry_consignee_phone']                    = 'Consignee Phone';
$_['entry_consignee_alternate_country_code']   = 'Consignee Alternate Country Code';
$_['entry_consignee_alternate_phone']          = 'Consignee Alternate Phone';

# Shipment
$_['entry_shipment_reference_number']          = 'Shipment Reference Number';
$_['entry_shipment_type']                      = 'Shipment Type';
$_['entry_billing_type']                       = 'Billing Type';
$_['entry_collect_amount']                     = 'Collect Amount';
$_['entry_primary_service']                    = 'Primary Service';
$_['entry_secondary_service']                  = 'Secondary Service';
$_['entry_item_value']                         = 'Item Value';
$_['entry_commodity_description']              = 'Commodity Description';

$_['entry_weight_actual']                      = 'Weight Actual';
$_['entry_volumetric_width']                   = 'Volumetric Width';
$_['entry_volumetric_height']                  = 'Volumetric Height';
$_['entry_volumetric_depth']                   = 'Volumetric Depth';

// Help
$_['help_country_code']              = 'Initial 3 digit country code e-g +966 ,966 00966';
$_['help_phone']                     = '7 digit number';


// Error
$_['error_permission']                 = 'Warning: You do not have permission to modify clex shipping!';

$_['error_consignor']                  = 'Name Required!';
$_['error_consignor_email']            = 'Email Required!';
$_['error_origin_city']                = 'City Required!';
$_['error_consignor_country_code']     = 'Country Code Required!';
$_['error_consignor_phone']            = 'Phone Required!';

$_['error_consignee']                  = 'Name Required!';
$_['error_consignee_email']            = 'Email Required!';
$_['error_destination_city']           = 'City Required!';
$_['error_consignee_country_code']     = 'Country Code Required!';
$_['error_consignee_phone']            = 'Phone Required!';

$_['error_shipment_reference_number']  = 'Shipment Reference Number Required!';
$_['error_shipment_type']              = 'Shipment Type Required!';
$_['error_billing_type']               = 'Billing Type Required!';
$_['error_collect_amount']             = 'Collect Amount Required!';

$_['error_primary_service']            = 'Primary Service Required!';
$_['error_secondary_service']          = 'Secondary Service Required!';
$_['error_commodity_description']      = 'Commodity Description Required!';
$_['error_shipment_piece']             = 'Shipping Packages Required!';
$_['error_shipment_piece_invalid']     = 'Shipping Packages Invalid!';

// Email
$_['clex_text_link']                   = 'To view your order click on the link below';
$_['clex_text_order_status']           = 'Your order has been updated to the following status';
$_['clex_text_order_id']               = 'Order ID';
$_['clex_text_date_added']             = 'Date Added';
$_['clex_text_comment']                = 'The comments for your order are';
$_['clex_text_subject']                = '%s - Shipment created';
