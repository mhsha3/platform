<?php
// Heading
$_['heading_title']                = 'Fastcoo';

// Text
$_['text_extension']               = 'Extensions';
$_['text_success']                 = 'Success: You have modified Fastcoo shipping!';
$_['text_edit']                    = 'Edit Fastcoo Shipping';

// Entry
$_['entry_email_id']               = 'Email ID' ;
$_['entry_password']               = 'Password' ;
$_['entry_cost']                   = 'Cost';
$_['entry_status']                 = 'Status';
$_['entry_sort_order']             = 'Sort Order';

// Help
$_['help_test']                    = 'Use this module in Test (YES) or Production mode (NO)?';

// Error
$_['error_permission']             = 'Warning: You do not have permission to modify Fastcoo shipping!';
$_['error_email_id']               = 'Email ID Required!';
$_['error_password']               = 'Password Required!';
