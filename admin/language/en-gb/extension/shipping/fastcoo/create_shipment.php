<?php
// Heading
$_['heading_title']                    = 'Fastcoo Shipping';

// Text
$_['text_back_to_order']               = 'Back to Order';
$_['text_print_label']                 = 'Print Label';
$_['text_track']                       = 'Track Shipping';
$_['text_cancel_sipment']              = 'Cancel Shipping';
$_['text_error_heading']               = 'Error';
$_['text_shipment_details']            = 'Shipment Details';
$_['text_sender_details']              = 'Sender Details';
$_['text_receiver_details']            = 'Receiver Details';
$_['text_shipment_information']        = 'Shipment Details';
$_['text_shipment_travel_history']     = 'Travel History';
$_['text_success']                     = 'Success: Shipment created successfully!';

$_['text_shipping_submitted_message']  = '<b>Your shipping information has been submitted.</b>';
$_['text_cancel_failed']               = 'Shipment failed to cancel';
$_['text_shipment_cancelled']          = 'Shipment cancelled';
$_['text_button_create_shipment']      = 'Create Shipment';
$_['text_checkbox_notify_customer']    = 'Notify customer by email';
$_['text_button_resubmit_shipment']    = 'Resubmit to update';
$_['text_cancel_shipment']             = 'Cancel Shipment';
$_['text_cancel_confirm_shipment']     = 'Are you sure you want to cancel shipment?';
$_['text_shipment_tracking']           = 'Shipment Tracking';
$_['text_processing']                  = 'Processing...';
$_['text_ajax_error_common']           = 'An error occurred, please try again later.';
$_['text_product_item_name']           = 'Item Name';
$_['text_product_quantity']            = 'Quantity';
$_['text_no_of_products_shipped']      = 'Number of items to be shipped';
$_['text_ok']                          = 'OK';
$_['text_shipment_status']             = 'Shipment Status:';
$_['text_created_date']                = 'Created Date:';

$_['text_product_type_kvaimi']         = 'KVAIMI';
$_['text_product_type_parcel']         = 'Parcel';
$_['text_delivery_service_same_day']   = 'Same Day Delivery';
$_['text_delivery_service_next_day']   = 'Next Day Delivery';
$_['text_booking_mode_cc']             = 'Cash Collected';
$_['text_booking_mode_cod']            = 'Cash On Delivery';


// Entry
# Sender
$_['entry_sender_name']                = 'Name';
$_['entry_sender_mobile']              = 'Mobile';
$_['entry_sender_address']             = 'Address';
$_['entry_sender_city']                = 'City';

# Receiver
$_['entry_receiver_name']              = 'Name';
$_['entry_receiver_email']             = 'Email';
$_['entry_receiver_address']           = 'Address';
$_['entry_receiver_phone']             = 'Phone';
$_['entry_receiver_city']              = 'City';

# Shipment
$_['entry_product_type']               = 'Product Type';
$_['entry_delivery_service']           = 'Delivery Service';
$_['entry_shipment_weight']            = 'Shipment Weight (Kg)';
$_['entry_shipment_description']       = 'Detail of Shipment';
$_['entry_number_of_packets']          = 'Number of Packets';
$_['entry_booking_mode']               = 'Booking Mode';
$_['entry_cod_value']                  = 'COD value';
$_['entry_reference']                  = 'Reference';

$_['entry_shipment_items_price']       = 'Items Price';
$_['entry_shipment_quantity']          = 'Items Quantity';
$_['entry_shipment_items_weight']      = 'Items Weight';
$_['entry_shipment_currency']          = 'Currency';
$_['entry_awb_no']                     = 'AWB No';
$_['entry_status_code']                = 'Status Code';
$_['entry_origin']                     = 'Origin';
$_['entry_destination']                = 'Destination';

$_['entry_travel_history_new_location']  = 'New Location';
$_['entry_travel_history_new_status']    = 'New Status';
$_['entry_travel_history_activites']     = 'Activites';
$_['entry_travel_history_code']          = 'Code';
$_['entry_travel_history_comment']       = 'Comment';
$_['entry_travel_history_entry_date']    = 'Entry Date';

// Error
$_['error_permission']                 = 'Warning: You do not have permission to modify Fastcoo shipping!';

$_['error_sender_name']                = 'Name Required!';
$_['error_sender_address']             = 'Address Required!';
$_['error_sender_mobile']              = 'Mobile Required!';
$_['error_sender_city']                = 'City Required!';

$_['error_receiver_name']              = 'Name Required!';
$_['error_receiver_email']             = 'Email Required!';
$_['error_receiver_address']           = 'Address Required!';
$_['error_receiver_phone']             = 'Phone Required!';
$_['error_receiver_city']              = 'City Required!';

$_['error_product_type']               = 'Product Type Required!';
$_['error_delivery_service']           = 'Delivery Service Required!';
$_['error_shipment_weight']            = 'Shipment Weight Required!';
$_['error_shipment_description']       = 'Detail of Shipment Required!';
$_['error_number_of_packets']          = 'Number of Packets Required!';
$_['error_booking_mode']               = 'Booking Mode Required!';
$_['error_cod_value']                  = 'COD value Required!';
$_['error_reference']                  = 'Reference Required!';

// Email
$_['fastcoo_text_link']                   = 'To view your order click on the link below';
$_['fastcoo_text_order_status']           = 'Your order has been updated to the following status';
$_['fastcoo_text_order_id']               = 'Order ID';
$_['fastcoo_text_date_added']             = 'Date Added';
$_['fastcoo_text_comment']                = 'The comments for your order are';
$_['fastcoo_text_subject']                = '%s - Shipment created';
