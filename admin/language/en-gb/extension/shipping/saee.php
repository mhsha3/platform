<?php
// Heading
$_['heading_title']                = 'SAEE';

// Text
$_['text_extension']               = 'Extensions';
$_['text_success']                 = 'Success: You have modified Saee shipping!';
$_['text_edit']                    = 'Edit Saee Shipping';
$_['text_cancel_failed']           = 'Shipment failed to cancel';
$_['text_shipment_cancelled']       = 'Shipment cancelled';
$_['text_shipping_submitted_message']       = '<b>Your shipping information has been submitted.</b><br>If you want to change something!';
$_['text_shipping_resubmit_button']       = 'Resubmit details';


// Entry
$_['entry_api_key']                = 'API Key';
$_['entry_test']                   = 'Test Mode';
$_['entry_cashonpickup']           = 'Cash on Pickup';
$_['entry_cashondelivery']         = 'Cash on Delivery';


$_['entry_pickup_address_id']      = 'Pickup Address Id';
$_['entry_pickup_address_code']    = 'Pickup Address Code';
$_['entry_origin_name']            = 'Pickup Name';
$_['entry_origin_name']            = 'Pickup Name';
$_['entry_origin_phone']           = 'Pickup Phone';
$_['entry_origin_phone2']          = 'Pickup Phone2';
$_['entry_origin_email']           = 'Pickup Email';
$_['entry_origin_gps_latitude']    = 'Pickup GPS latitude point';
$_['entry_origin_gps_longitude']   = 'Pickup GPS longitude point';
$_['entry_origin_address']         = 'Pickup Address';
$_['entry_origin_address2']        = 'Pickup Address2';
$_['entry_origin_district']        = 'Pickup District';
$_['entry_origin_city']            = 'Pickup City';
$_['entry_origin_state']           = 'Pickup State/Province';
$_['entry_origin_zipcode']         = 'Pickup Zipcode';
$_['entry_origin_country']         = 'Pickup Country';
$_['entry_status']                 = 'Status';
$_['entry_sort_order']             = 'Sort Order';
$_['entry_debug']                  = 'Debug Mode';

// Help
$_['help_api_key']                 = 'Enter your Saee API Key.';
$_['help_test']                    = 'Use this module in Test (YES) or Production mode (NO)?';
$_['help_cashonpickup']            = 'Cash on Pickup.';
$_['help_cashonpickup2']           = 'Value 0 denotes no cash on pickup';
$_['help_cashondelivery']          = 'Value 0 denotes no COD';
$_['help_pickup_address_id']       = 'Pickup Address Id submitted addresss already stored for provided Pickup Address Id will be used instead of following fields (Pickup Address, Pickup Address2, Pickup District, Pickup City, Pickup State, Pickup Zipcode, Pickup Latitude, Pickup Longitude)';
$_['help_pickup_address_code']     = 'To get list of company registered Pickup Address Codes use API Number 17 List of Pickup Addresses. (This is used if you have your own code, you can pass it instead of Pickup Address Id)';
$_['help_origin_name']             = 'Enter your origin name.';
$_['help_origin_phone']            = 'Enter your origin phone number.';
$_['help_origin_email']            = 'Enter your origin email.';
$_['help_origin_gps_latitude']     = 'Pickup GPS latitude point.';
$_['help_origin_gps_longitude']    = 'Pickup GPS longitude point.';
$_['help_origin_address']          = 'Pickup address description.';
$_['help_origin_address2']         = 'Pickup additional address description.';
$_['help_origin_district']         = 'Enter the name of the origin district.';
$_['help_origin_city']             = 'Enter the name of the origin city.';
$_['help_origin_state']            = 'Enter your origin state/province.';
$_['help_origin_zipcode']          = 'Enter your origin zipcode.';
$_['help_origin_country']          = 'Enter your origin country.';
$_['help_debug']                   = 'Saves send/recv data to the system log';
$_['help_shipment_quantity']       = 'Numbers of package of waybill';
$_['help_weight']                  = 'Weight of package in KG';


// Error
$_['error_permission']             = 'Warning: You do not have permission to modify Saee shipping!';
$_['error_api_key']                = 'Client Id Required!';
$_['error_origin_name']            = 'Pickup Name Required!';
$_['error_origin_phone']           = 'Pickup Phone Required!';
$_['error_origin_email']           = 'Pickup Email Required!';
$_['error_origin_gps_latitude']    = 'Pickup GPS Latitude Required!';
$_['error_origin_gps_longitude']   = 'Pickup GPS Longitude Required!';
$_['error_origin_address']         = 'Pickup Address Required!';
$_['error_origin_city']            = 'Pickup City Required!';
$_['error_origin_state']           = 'Pickup State/Province Required!';
$_['error_origin_country']         = 'Pickup Country Required!';

// Create Shipment
$_['saee_create_heading_title']        = 'SAEE Shipping';
$_['text_back_to_order']               = 'Back to Order';
$_['text_print_label']                 = 'Print Label';
$_['text_track']                       = 'Track Shipping';
$_['text_cancel_sipment']              = 'Cancel Shipping';
$_['text_error_heading']               = 'Error';
$_['text_shipment_details']            = 'Shipment Details';
$_['text_last_mile_booking']           = 'Last Mile Booking';
$_['text_pickup_shipments']            = 'Pickup Shipments';

$_['text_sender_details']              = 'Sender Details';
$_['text_sender_name']                 = 'Name';
$_['text_sender_email']                = 'Email';
$_['text_sender_phone']                = 'Phone';
$_['text_sender_address']              = 'Street Address';
$_['text_sender_city']                 = 'City';
$_['text_sender_country']              = 'Country';

$_['text_receiver_details']            = 'Receiver Details';
$_['text_receiver_name']               = 'Name';
$_['text_receiver_email']              = 'Email';
$_['text_receiver_mobile']             = 'Mobile';
$_['text_receiver_mobile2']            = 'Mobile2';
$_['text_receiver_address']            = 'Street Address';
$_['text_receiver_address2']           = 'Street Address2';
$_['text_receiver_district']           = 'District';
$_['text_receiver_city']               = 'City';
$_['text_receiver_state']              = 'State';
$_['text_receiver_zipcode']            = 'Zipcode';
$_['text_receiver_latitude']           = 'Latitude';
$_['text_receiver_longitude']          = 'Longitude';

$_['text_custom_value']                = 'Custom Value';
$_['text_hs_code']                     = 'HS Code';
$_['text_category_id']                 = 'Category Id';

$_['text_pickup_details']              = 'Pickup Details';

$_['text_shipment_information']        = 'Shipment Information';
$_['text_shipment_quantity']           = 'Quantity';
$_['text_shipment_weight']             = 'Weight';
$_['text_shipment_currency']           = 'Currency';
$_['text_shipment_items_price']        = 'Items Price';
$_['text_product_item_name']           = 'Item Name';
$_['text_product_quantity']            = 'Quantity';
$_['text_no_of_products_shipped']      = 'Number of items to be shipped';
$_['text_button_create_shipment']      = 'Create Shipment';
$_['text_checkbox_notify_customer']    = 'Notify customer by email';
$_['text_button_resubmit_shipment']    = 'Resubmit to update';
$_['text_cancel_shipment']             = 'Cancel Shipment';
$_['text_cancel_confirm_shipment']     = 'Are you sure you want to cancel shipment?';
$_['text_shipment_tracking']           = 'Shipment Tracking';
$_['text_processing']                  = 'Processing...';
$_['text_tracking_city']               = 'City';
$_['text_tracking_status']             = 'Status';
$_['text_tracking_date']               = 'Date';
$_['text_ajax_error_common']           = 'An error occurred, please try again later.';
$_['text_ok']                          = 'OK';

// Email
$_['saee_text_link']                   = 'To view your order click on the link below';
$_['saee_text_order_status']           = 'Your order has been updated to the following status';
$_['saee_text_order_id']               = 'Order ID';
$_['saee_text_date_added']             = 'Date Added';
$_['saee_text_comment']                = 'The comments for your order are';
$_['saee_text_subject']                = '%s - Shipment created';
