<?php
// Heading
$_['heading_title']                    = 'Fastlo Shipping';

// Text
$_['text_back_to_order']               = 'Back to Order';
$_['text_print_label']                 = 'Print Label';
$_['text_track']                       = 'Track Shipping';
$_['text_cancel_sipment']              = 'Cancel Shipping';
$_['text_error_heading']               = 'Error';
$_['text_shipment_details']            = 'Shipment Details';
$_['text_sender_details']              = 'Sender Details';
$_['text_receiver_details']            = 'Receiver Details';
$_['text_shipment_information']        = 'Shipment Details';
$_['text_success']                     = 'Success: Shipment created successfully!';

$_['text_shipping_submitted_message']  = '<b>Your shipping information has been submitted.</b>';
$_['text_cancel_failed']               = 'Shipment failed to cancel';
$_['text_shipment_cancelled']          = 'Shipment cancelled';
$_['text_button_create_shipment']      = 'Create Shipment';
$_['text_checkbox_notify_customer']    = 'Notify customer by email';
$_['text_button_resubmit_shipment']    = 'Resubmit to update';
$_['text_cancel_shipment']             = 'Cancel Shipment';
$_['text_cancel_confirm_shipment']     = 'Are you sure you want to cancel shipment?';
$_['text_shipment_tracking']           = 'Shipment Tracking';
$_['text_processing']                  = 'Processing...';
$_['text_ajax_error_common']           = 'An error occurred, please try again later.';
$_['text_product_item_name']           = 'Item Name';
$_['text_product_quantity']            = 'Quantity';
$_['text_no_of_products_shipped']      = 'Number of items to be shipped';
$_['text_ok']                          = 'OK';
$_['text_shipment_status']             = 'Shipment Status:';
$_['text_created_date']                = 'Created Date:';

// Entry
# Sender
$_['entry_sender_name']                = 'Name';
$_['entry_sender_mobile1']             = 'Mobile';
$_['entry_sender_mobile2']             = 'Mobile2';
$_['entry_sender_country']             = 'Country';
$_['entry_sender_city']                = 'City';
$_['entry_sender_area']                = 'Area';
$_['entry_sender_street']              = 'Street';
$_['entry_sender_additional']          = 'Additional';
$_['entry_sender_latitude']            = 'Latitude';
$_['entry_sender_longitude']           = 'Longitude';

# Receiver
$_['entry_receiver_name']              = 'Name';
$_['entry_receiver_mobile1']           = 'Mobile';
$_['entry_receiver_mobile2']           = 'Mobile2';
$_['entry_receiver_country']           = 'Country';
$_['entry_receiver_city']              = 'City';
$_['entry_receiver_area']              = 'Area';
$_['entry_receiver_street']            = 'Street';
$_['entry_receiver_additional']        = 'Additional';
$_['entry_receiver_latitude']          = 'Latitude';
$_['entry_receiver_longitude']         = 'Longitude';

# Shipment
$_['entry_collect_cash_amount']        = 'Collect Cash Amount';
$_['entry_number_of_pieces']           = 'Number of Pieces';
$_['entry_reference']                  = 'Reference';

$_['entry_shipment_items_price']       = 'Items Price';
$_['entry_shipment_quantity']          = 'Items Quantity';
$_['entry_shipment_weight']            = 'Items Weight';
$_['entry_shipment_currency']          = 'Currency';

// Error
$_['error_permission']                 = 'Warning: You do not have permission to modify fastlo shipping!';

$_['error_sender_name']                = 'Name Required!';
$_['error_sender_mobile1']             = 'Mobile Required!';
$_['error_sender_country']             = 'Country Required!';
$_['error_sender_city']                = 'City Required!';
$_['error_sender_area']                = 'Area Required!';

$_['error_receiver_name']              = 'Name Required!';
$_['error_receiver_mobile1']           = 'Mobile Required!';
$_['error_receiver_country']           = 'Country Required!';
$_['error_receiver_city']              = 'City Required!';
$_['error_receiver_area']              = 'Area Required!';

$_['error_collect_cash_amount']        = 'Collect Cash Amount Required!';
$_['error_number_of_pieces']           = 'Number of Pieces Required!';
$_['error_reference']                  = 'Reference Required!';

// Email
$_['fastlo_text_link']                   = 'To view your order click on the link below';
$_['fastlo_text_order_status']           = 'Your order has been updated to the following status';
$_['fastlo_text_order_id']               = 'Order ID';
$_['fastlo_text_date_added']             = 'Date Added';
$_['fastlo_text_comment']                = 'The comments for your order are';
$_['fastlo_text_subject']                = '%s - Shipment created';
