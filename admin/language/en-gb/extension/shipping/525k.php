<?php
// Heading
$_['heading_title']                = '525K';

// Text
$_['text_extension']               = 'Extensions';
$_['text_success']                 = 'Success: You have modified 525K shipping!';
$_['text_edit']                    = 'Edit 525K Shipping';
$_['text_box']                     = 'Box';
$_['text_pallet']                  = 'Pallet';
$_['text_other']                   = 'Other';

// Entry
$_['entry_client_id']              = 'Client Id';
$_['entry_client_secret']          = 'Client Secret';
$_['entry_shipper_id']             = 'Shipper Id';
$_['entry_requestedby_id']         = 'RequestedBy Id';
$_['entry_api_key']                = 'API Key' ;
$_['entry_test']                   = 'Test Mode';

$_['entry_origin_name']            = 'Pickup Name';
$_['entry_origin_phone']           = 'Pickup Phone';
$_['entry_origin_email']           = 'Pickup Email';
$_['entry_origin_gps_latitude']    = 'Pickup GPS latitude point';
$_['entry_origin_gps_longitude']   = 'Pickup GPS longitude point';
$_['entry_origin_address']         = 'Pickup Address';
$_['entry_origin_address2']        = 'Pickup Address2';
$_['entry_origin_city']            = 'Pickup City';
$_['entry_origin_state']           = 'Pickup State/Province';
$_['entry_origin_country']         = 'Pickup Country';
$_['entry_pickup_instructions']    = 'Pickup Instructions';
$_['entry_delivery_instructions']  = 'Delivery Instructions';

$_['text_pickup_arrival_window_begin'] = 'Arrival Window Begin';
$_['text_pickup_arrival_window_end']   = 'Arrival Window End';

$_['entry_pickup_arrival_window']                = 'Pickup Arrival Window';
$_['entry_pickup_arrival_window_days']           = 'Days';
$_['entry_pickup_arrival_window_exclude_begin']  = 'Exclude Begin';
$_['entry_pickup_arrival_window_exclude_end']    = 'Exclude End';

$_['entry_delivery_arrival_window']                = 'Delivery Arrival Window';
$_['entry_delivery_arrival_window_days']           = 'Days';
$_['entry_delivery_arrival_window_exclude_begin']  = 'Exclude Begin';
$_['entry_delivery_arrival_window_exclude_end']    = 'Exclude End';

$_['entry_dimension']              = 'Dimensions (L x W x H) in CM';
$_['entry_length']                 = 'Length';
$_['entry_height']                 = 'Height';
$_['entry_width']                  = 'Width';
$_['entry_packaging_type']         = 'Packaging Type';
$_['entry_stackable']              = 'Stackable';

$_['entry_status']                 = 'Status';
$_['entry_sort_order']             = 'Sort Order';
$_['entry_debug']                  = 'Debug Mode';

$_['entry_ref_id']                 = 'RefId';
$_['entry_load_description']       = 'Load Description';

$_['entry_shipment_value_amount']    = 'Price Amount';
$_['entry_shipment_value_currency']  = 'Price Currency';

$_['entry_truck_type']              = 'Truck Type';
$_['entry_full_truck']              = 'Full Truck';

$_['entry_cod_value_amount']        = 'COD Amount';
$_['entry_cod_value_currency']      = 'COD Currency';

// Help
$_['help_client_id']               = 'Enter your 525K Client Id.';
$_['help_client_secret']           = 'Enter your 525K Client Secret.';
$_['help_shipper_id']              = 'Enter Shipper Id provided by 525k.';
$_['help_test']                    = 'Use this module in Test (YES) or Production mode (NO)?';
$_['help_origin_name']             = 'Enter your origin name.';
$_['help_origin_phone']            = 'Enter your origin phone number.';
$_['help_origin_email']            = 'Enter your origin email.';
$_['help_origin_gps_latitude']     = 'Pickup GPS latitude point.';
$_['help_origin_gps_longitude']    = 'Pickup GPS longitude point.';
$_['help_origin_address']          = 'Pickup address description.';
$_['help_origin_address2']         = 'Pickup additional address description.';
$_['help_origin_city']             = 'Enter the name of the origin city.';
$_['help_origin_state']            = 'Enter your origin state/province.';
$_['help_origin_country']          = 'Enter your origin country.';
$_['help_pickup_instructions']     = 'Special pickup instructions.';
$_['help_delivery_instructions']   = 'Special delivery instructions.';
$_['help_pickup_arrival_window']   = 'Time duration to pick shipment.';
$_['help_delivery_arrival_window'] = 'Time duration to deliver shipment.';

$_['help_packaging_type']          = 'Item packaging type.';
$_['help_stackable']               = 'Shipment item is stackable.';
$_['help_dimension']               = 'This is assumed to be your average packing box size. Individual item dimensions are not supported at this time so you must enter average dimensions like 5x5x5.';
$_['help_debug']                   = 'Saves send/recv data to the system log';

$_['help_ref_id']                  = 'Shipment reference Id';
$_['help_load_description']        = 'Shipment load description';

$_['help_shipment_value_amount']   = 'Price amount of sent item.This field will not be used to determine cash on delivery amount.';
$_['help_shipment_value_currency'] = 'Price currency of sent item.This field will not be used to determine cash on delivery amount.';

$_['help_truck_type']              = 'Shipment Requested Truck Type. <u class="text-danger">Only should be used in case of requesting specific truck type per agreement with 525k.</u>';
$_['help_full_truck']              = 'A full truck is requested for each shipment. <u class="text-danger">It shouldn’t be filled unless agreed with 525k to enable it.</u> (default = false)';

$_['help_cod_value_amount']        = 'Cash to be collected from customers';
$_['help_cod_value_currency']      = 'Currency of cash to be collected from customer';

$_['help_item_quantity']           = 'Shipment Item quantity';
$_['help_item_weight']             = 'Shipment Item Weight in KG';
$_['help_item_refid']              = 'Shipment Item RefId';

// Error
$_['error_permission']             = 'Warning: You do not have permission to modify 525K shipping!';
$_['error_client_id']              = 'Client Id Required!';
$_['error_client_secret']          = 'Client Secret Required!';
$_['error_shipper_id']             = 'Shipper Id Required!';
$_['error_api_key']                = 'API Key Required!';

$_['error_origin_phone']           = 'Pickup Phone Required!';
$_['error_origin_email']           = 'Pickup Email Required!';
$_['error_origin_gps_latitude']    = 'Pickup GPS Latitude Required!';
$_['error_origin_gps_longitude']   = 'Pickup GPS Longitude Required!';
$_['error_origin_address']         = 'Pickup Address Required!';
$_['error_origin_city']            = 'Pickup City Required!';
$_['error_origin_state']           = 'Pickup State/Province Required!';
$_['error_origin_country']         = 'Pickup Country Required!';

$_['error_pickup_instructions']    = 'Pickup Instructions Required!';
$_['error_pickup_arrival_window']  = 'Pickup Arrival Window Required!';
$_['error_delivery_arrival_window']  = 'Delivery Arrival Window Required!';
$_['error_dimension']                = 'Average Dimensions Required!';

$_['error_input_data']               = 'Invalid input data.';


// Create Shipment
$_['i525k_create_heading_title']        = '525K Shipping';
$_['text_back_to_order']               = 'Back to Order';
$_['text_print_label']                 = 'Print Label';
$_['text_track']                       = 'Track Shipping';
$_['text_cancel_sipment']              = 'Cancel Shipping';
$_['text_error_heading']               = 'Error';
$_['text_shipment_details']            = 'Shipment Details';
$_['text_last_mile_booking']           = 'Last Mile Booking';
$_['text_pickup_shipments']            = 'Pickup Shipments';

$_['text_sender_details']              = 'Sender Details';
$_['text_sender_name']                 = 'Name';
$_['text_sender_email']                = 'Email';
$_['text_sender_phone']                = 'Phone';
$_['text_sender_address']              = 'Street Address';
$_['text_sender_city']                 = 'City';
$_['text_sender_country']              = 'Country';

$_['text_receiver_details']            = 'Receiver Details';
$_['text_receiver_name']               = 'Name';
$_['text_receiver_email']              = 'Email';
$_['text_receiver_mobile']             = 'Mobile';
$_['text_receiver_address']            = 'Street Address';
$_['text_receiver_address2']           = 'Street Address2';
$_['text_receiver_city']               = 'City';
$_['text_receiver_country']            = 'Country';
$_['text_receiver_latitude']           = 'Latitude';
$_['text_receiver_longitude']          = 'Longitude';
$_['text_receiver_arrival_window_begin'] = 'Arrival Window Begin';
$_['text_receiver_arrival_window_end']   = 'Arrival Window End';

$_['text_datetime_include']            = 'Include';
$_['text_datetime_exclude']            = 'Exclude';

$_['text_pickup_details']              = 'Pickup Details';

$_['text_shipping_submitted_message']  = '<b>Your shipping information has been submitted.</b>';
$_['text_cancel_failed']               = 'Shipment failed to cancel';
$_['text_shipment_cancelled']          = 'Shipment cancelled';

$_['text_shipment_information']        = 'Shipment Information';
$_['text_shipment_quantity']           = 'Quantity';
$_['text_shipment_weight']             = 'Weight';
$_['text_shipment_item_refid']         = 'Item RefId';
$_['text_shipment_currency']           = 'Currency';
$_['text_shipment_items_price']        = 'Items Price';
$_['text_product_item_name']           = 'Item Name';
$_['text_product_quantity']            = 'Quantity';
$_['text_no_of_products_shipped']      = 'Number of items to be shipped';
$_['text_button_create_shipment']      = 'Create Shipment';
$_['text_checkbox_notify_customer']    = 'Notify customer by email';
$_['text_button_resubmit_shipment']    = 'Resubmit to update';
$_['text_cancel_shipment']             = 'Cancel Shipment';
$_['text_cancel_confirm_shipment']     = 'Are you sure you want to cancel shipment?';
$_['text_shipment_tracking']           = 'Shipment Tracking';
$_['text_processing']                  = 'Processing...';
$_['text_tracking_city']               = 'City';
$_['text_tracking_status']             = 'Status';
$_['text_tracking_date']               = 'Date';
$_['text_ajax_error_common']           = 'An error occurred, please try again later.';
$_['text_ok']                          = 'OK';
$_['text_']                          = 'OK';

// Email
$_['i525k_text_link']                   = 'To view your order click on the link below';
$_['i525k_text_order_status']           = 'Your order has been updated to the following status';
$_['i525k_text_order_id']               = 'Order ID';
$_['i525k_text_date_added']             = 'Date Added';
$_['i525k_text_comment']                = 'The comments for your order are';
$_['i525k_text_subject']                = '%s - Shipment created';
