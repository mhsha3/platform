<?php
// Heading
$_['heading_title']                = 'Fastlo';

// Text
$_['text_extension']               = 'Extensions';
$_['text_success']                 = 'Success: You have modified Fastlo shipping!';

// Entry
$_['entry_api_key']                = 'API Key' ;
$_['entry_test']                   = 'Test Mode';
$_['entry_cost']                   = 'Cost';
$_['entry_status']                 = 'Status';
$_['entry_sort_order']             = 'Sort Order';

// Help
$_['help_test']                    = 'Use this module in Test (YES) or Production mode (NO)?';

// Error
$_['error_permission']             = 'Warning: You do not have permission to modify 525K shipping!';
$_['error_api_key']                = 'API Key Required!';
