<?php
// Heading
$_['tab_pickup']        = 'SMSA PickUp Request';
$_['tab_awb']           = 'SMSA AWB Request';
$_['tab_waybill']       = 'SMSA Shipment Details';

//
$_['button_awb']        = 'SMSA AWB';
$_['button_pickUp']     = 'Register PickUp';
// Text
$_['text_success']      = 'Success: You have modified voucher themes!';
$_['text_list']         = 'Voucher Theme List';
$_['text_add']          = 'Add Voucher Theme';
$_['text_edit']         = 'Edit Voucher Theme';

// Column
$_['column_date_added']       = 'Request Datetime';
$_['column_reference_number']     = 'Reference Number';
$_['column_awb_number']     = 'AWB Number';
$_['column_shipment_label']     = 'Label';
$_['column_status']     = 'Status';
$_['column_action']     = 'Action';

// Entry
$_['entry_pickup_datetime'] = 'Pickup Date & Time';
$_['entry_remark']      = 'Special Instruction / Remarks';
$_['entry_weight']      = 'Weight(kg)';
$_['entry_dimension']   = 'Dimension(WxHxL)cm';


// Error
$_['error_permission']  = 'Warning: You do not have permission to modify SMSA API!';
$_['error_date']        = 'Please select date and time!';
$_['error_reason']      = 'Reason must be under 60 characters!';
$_['error_awb_number']  = 'AWb Number missing!';
$_['error_module_status']= 'Warning: This module is disabled!';