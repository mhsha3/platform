<?php
$_['text_subscription']  ='Subscriptions';
$_['text_sar'] = 'SAR';
$_['text_month_package'] ='Monthly';
$_['text_year_package'] ='Annual';
$_['text_info_package'] ='Comprehensive care starts with taking care of launching your online store with a team for us and making sure that you are connected with the services of our partners who help you achieve the growth plan and ending with monthly consulting sessions to follow up the performance of your store';
$_['text_about_package'] ='Packages from LNA fit your needs';
$_['text_subscribe'] ='Subscribe';
$_['text_invalid_phone_number'] ='The mobile number you entered is wrong';
$_['text_invalid_code_activation'] ='The confirmation code is wrong';
$_['text_done_paid'] ='Congratulations. Your payment was successful';
$_['text_invalid_paid'] ='The payment did not complete successfully';
$_['text_enter_4_code'] ='Please write the code sent to you to complete the payment process';
$_['text_pay_by_stc'] ='Pay by STC Pay';
$_['text_send_code'] ='Send confirmation code';
$_['text_pay'] ='Pay Now';
$_['text_platform_updates'] ='Platform Updates';
$_['text_like'] ='Like';
$_['text_dislike'] ='Dislike';
$_['text_what_opnion'] ='What is your opinion ?';
$_['text_header_notice'] ='Top Banner';
$_['text_link'] ='Link';
$_['text_add_text_arabic'] ='Enter text in Arabic';
$_['text_add_text_english'] ='Enter text in English';
$_['text_add'] ='add';
$_['text_please_enter_all_data'] ='Please enter all required information';
$_['text_status'] ='Case';
$_['text_avaliable'] ='Available';
$_['text_unavaliable'] ='Unavailable';
$_['text_done_add_header_notice'] ='Top banner added successfully';
$_['text_store_type'] ='Category Store';
$_['text_doc_single'] ='Individually';
$_['text_doc_company'] ='Facility';
$_['text_add_docs'] ='Please attach documents here';
$_['text_documents'] ='Documents';
$_['text_done_upload_doc'] ='The documents have been successfully uploaded and are now under review';

$_['text_notifications_center'] ='Notification Center';
$_['text_not_found_notifications'] ='Not Found any notifications';
$_['text_new'] ='New';

?>
