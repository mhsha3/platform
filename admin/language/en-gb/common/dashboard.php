<?php
// Heading
$_['heading_title']    = 'Dashboard';

// Error
$_['error_install']    ='Warning: The installation folder has not been deleted - it should be deleted for Store Security!';
$_['Make_Store_Settings']    ='Start preparing your new store';
$_['Store_Settings']    ='Store settings';
$_['Add_Product']    ='Add products';
$_['Shipping_Methods']    = 'Shipping Methods';
$_['Payment_Methods']    = 'Payment method';
$_['lanamass']    = 'Welcome to LNA world';
$_['lanamass1']    = 'We strive to distinguish our services for you';
$_['text_renew_package_msg'] = 'Please renew the subscription so your service does not stop for renewal <a href="subscription_url"> &nbsp; Click here &nbsp; </a> ';
$_['text_refuse_documents_msg'] ='Please re-upload the documents because they do not fulfill the required conditions <a href="documents_url"> &nbsp; click here &nbsp; </a>';
$_['text_upload_documents_msg'] = 'Please upload the required documents <a href="documents_url"> &nbsp; Click here &nbsp; </a> ';
$_['text_online_now'] ='Online Now ';
$_['text_total_customer'] ='Number of clients';
$_['text_total_orders'] ='Total Orders';
$_['text_total_sales'] = 'Total Sales';
$_['text_quick_statics'] = 'Quick Statics';
$_['text_currency'] = 'SAR';
$_['text_recent_orders'] = 'Recent Orders';
$_['recent_orders'] = 'Recent Orders';
$_['text_order'] = 'Order';
$_['text_not_found_orders'] = 'Not found orders!';
$_['text_total'] = 'Total';
$_['text_customers_reviews'] = 'Customers Reviews';
$_['text_customers_activity'] = 'Customers Activity';
$_['text_loss_product'] = 'Out of stock';
$_['text_not_found_loss_product'] ='You are not out of stock';
$_['text_not_found_customer_activity'] ='You have no customer activities';

