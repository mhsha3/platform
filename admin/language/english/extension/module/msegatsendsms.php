<?php
// Heading
$_['heading_title']       = 'Msegat OpenCart Send SMS';

// Text
$_['text_module']          = 'Modules';
$_['text_success_sent']    = 'Success: Message has been sent!';
$_['text_error']        	 = 'Failed: Message not sent! (error message: ';
$_['text_none']      			 = '-- Select Destination --';
$_['text_nohp']      			 = 'Input Number';
$_['text_newsletter']      = 'All Newsletter Subscribers';
$_['text_customer_all']    = 'All Customers';
$_['text_customer_group']  = 'Customer Group';
$_['text_customer']        = 'Customers';
$_['text_affiliate_all']   = 'All Affiliates';
$_['text_affiliate']       = 'Affiliates';
$_['text_product']         = 'Products';

$_['button_save']          = 'Send Message';
$_['cancel']          = 'Cancel';
// Entry
$_['entry_nohp']    			 = 'Mobile Number';
$_['entry_message']    		 = 'Message:';
$_['entry_or']    				 = 'To';
$_['entry_customer_group'] = 'Customer Group';
$_['entry_customer']       = 'Customer';
$_['entry_affiliate']      = 'Affiliate';
$_['entry_product']        = 'Products';

// Button
$_['button_send']					 = 'Send Message';
$_['button_setting']			 = 'Msegat OpenCart Setting';

// Error
$_['error_permission']     = 'Warning: You do not have permission to modify module Msegat OpenCart!';
$_['error_nohp']       		 = 'Destination number required';
$_['error_message']        = 'Text message required';
$_['error_gateway_null']   = 'Gateway not found! Please set the default gateway';

?>