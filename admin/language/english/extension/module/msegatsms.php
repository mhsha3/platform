<?php
// Heading
$_['heading_title']       		= 'Msegat OpenCart Setting';
$_['intruction_title']    		= 'Instruction';

// Tab
$_['tab_settings']                  = 'Settings';
$_['tab_order_status_sms']                   = 'Order Status SMS';
$_['tab_order_sms']                 = 'Orders SMS';
$_['tab_accounts_sms']                 = 'Accounts SMS';

// Text
$_['text_module']         		= 'Modules';
$_['text_success']        		= 'Success: You have modified module Msegat OpenCart!';
$_['text_content_top']    		= 'Content Top';
$_['text_content_bottom'] 		= 'Content Bottom';
$_['text_column_left']    		= 'Column Left';
$_['text_column_right']   		= 'Column Right';
$_['text_yes']      					= 'Yes';
$_['text_no']      						= 'No';
$_['text_none']      					= '-- Select Gateway --';
$_['text_status_none']      	= '-- Select Status --';
$_['text_limit']      				= 'SMS/day';
$_['text_enable_verify']      = 'Enable Mobile Phone Verification?';
$_['text_enable_verify_span']      = 'Verification mobile phone when customers do order';

// Gateway
$_['entry_gateway']       		= 'Gateway:<br /><span class=\'help\'>Select gateway provider.<br />For the list of countries supported by gateways, you can visit the each gateway site.</span>';


// msegat
$_['entry_userkey_msegat']   = 'Username';
$_['entry_userkey_msegat_span']   = 'username for the account in <a href=\'https://msegat.com\' target=\'_blank\'>Msegat.com</a>';


$_['entry_passkey_msegat']   = 'API Key';
$_['entry_passkey_msegat_span']   = 'userPassword for the account in <a href=\'https://msegat.com\' target=\'_blank\'>Msegat.com</a>';

$_['entry_function']   = 'Function';
$_['entry_template']   = 'Template';
$_['entry_enable']   = 'Enable';

$_['entry_httpapi_msegat']   = 'HTTP API';
$_['entry_senderid_msegat']	 = 'Sender Name';
$_['entry_senderid_msegat_span']	 = ' sender name , should be activated from <a href=\'https://msegat.com\' target=\'_blank\'>Msegat.com</a>';

 
 
$_['httpapi_example_msegat'] = '<span class=\'help\'><b>Use this for HTTP API-></b> msegat.com/gw/index.php</span>';




// Entry
$_['entry_layout']        		= 'Layout:';
$_['entry_position']      		= 'Position:';
$_['entry_status']        		= 'Status:';
$_['entry_sort_order']    		= 'Sort Order:';
$_['entry_nohp']    					= 'Mobile Number:';
$_['entry_message']    				= 'Message:';
$_['entry_smslimit']    			= 'SMS Limit';
$_['entry_smslimit_span']    			= 'You can limit the message who can sending of visitor for a day. Leave blank to disable limit.';



$_['entry_alert_reg']   			= 'Register Message Alert';
$_['entry_alert_reg_span']   			= ' You can insert a <i>{firstname}</i>, <i>{lastname}</i>, <i>{email}</i>, <i>{password}</i> and <i>{storename}</i> in the message. <b>eg</b>: <i>Thanks for sign up {firstname} {lastname}. Your login email: {email} and password: {password}</i>';

 
 
$_['entry_alert_order']   		= 'Place Order Message Alert';
$_['entry_alert_order_span']   		= 'You can insert a <i>{firstname}</i>, <i>{lastname}</i>, <i>{email}</i>, <i>{orderid}</i>, <i>{orderstatus}</i>, <i>{shippingmethod}</i>, <i>{paymentmethod}</i>, <i>{total}</i>, <i>{storename}</i> in the message.';



$_['entry_alert_changestate'] = 'Order Alert';
$_['entry_alert_changestate_span'] = 'You can insert a <i>{firstname}</i>, <i>{lastname}</i>, <i>{orderid}</i>, <i>{orderstatus}</i>, <i>{shippingmethod}</i>, <i>{invoiceno}</i>, <i>{total}</i>, <i>{storename}</i> in the message.';

$_['entry_additional_alert']	= 'Additional Alert SMS';
$_['entry_additional_alert_span']	= 'Any additional admin mobile number (with Country Code) you want to receive the register and order alert sms. (comma separated). e.g: 1803015xxx,4475258xxx,6285220xxx';

$_['entry_alert_sms']    			= 'New Order Alert SMS';
$_['entry_alert_sms_span']    			= 'Send a SMS to the store owner when a new order is created. You can insert a <i>{firstname}</i>, <i>{lastname}</i>, <i>{email}</i>, <i>{orderid}</i>, <i>{orderstatus}</i> and etc (<strong>Variable accepted</strong>) in the message.';

$_['entry_account_sms']  			= 'New Account Alert SMS';
$_['entry_account_sms_span']  			= 'Send a SMS to the store owner when a new account is registered. You can insert a <i>{firstname}</i>, <i>{lastname}</i>, <i>{email}</i> and <i>{storename}</i> in the message.';



$_['entry_alert_blank']    		= '<span class=\'help\'>* Leave blank to disable this alert</span>';
$_['entry_status_order_alert']= 'Change Status Order Alert';
$_['entry_status_order_alert_span']= 'Select order status';


$_['entry_verify_code']				= 'Verification code message';
$_['entry_verify_code_span']				= ' Add <i>{code}</i> for verification code variable in message.<br />e.g: Your verification code is: {code}';

 
 
$_['entry_skip_group']				= 'Skip verification for Groups';
$_['entry_skip_group_span']				= 'Customer who is in group selected will not required mobile phone verification.';



$_['entry_skip_group_help']   = '<span class=\'help\'>Hold alt key for multiple select.</span>';
$_['entry_code_digit']   			= 'Length of Verification Code digit:';
$_['entry_code_digit_span']   			= 'Length of Verification Code digit';

$_['entry_max_retry']   			= 'Maximum retry';
$_['entry_max_retry_span']   			= 'The maximum customers can resend validation code.';



$_['entry_limit_blank']    		= '<span class=\'help\'>Leave blank to disable limit.</span>';
$_['entry_parsing']    				= '<span class=\'help\'><strong>Variable accepted:</strong> {order_date},{products_ordered},{firstname},{lastname},{email},{telephone},{orderid},{orderstatus},{shippingmethod},{shipping_firstname},{shipping_lastname},{shipping_company},{shipping_address},{shipping_city},{shipping_postcode},{shipping_state},{shipping_country},{paymentmethod},{payment_firstname},{payment_lastname},{payment_company},{payment_address},{payment_city},{payment_postcode},{payment_state},{payment_country},{total},{comment},{storename}</span>';
$_['entry_edit']    				= 'Edit';

// Button
$_['button_save']							= 'Save';
$_['button_cancel']						= 'Cancel';
$_['button_add_module']				= 'Add Module';
$_['button_remove']						= 'Remove';

// Error
$_['error_permission']    		= 'Warning: You do not have permission to modify module Msegat OpenCart!';
$_['error_gateway']       		= 'Gateway required';
$_['error_userkey']       		= 'This field required';
$_['error_passkey']       		= 'This field required';
$_['error_httpapi']       		= 'This field required';
$_['error_senderid']       		= 'Sender ID required';
$_['error_apiid']       			= 'API ID required';
$_['error_nohp']       				= 'Phone number required';
$_['error_message']       		= 'Text message required';
$_['error_gateway_null']   		= 'Gateway not found! Please set the default gateway';

// Anounce
$_['text_instruction']				= '<span class=\'help\'>You can add <b>Msegat OpenCart</b> module to enable your web visitor can Sending Message for free from your web. This can increase visitor traffic of your web.<br />Click <b>Add Module</b> button and set the configuration of module.<br/><br/><b>Layout</b>: On the page where the module will be displayed.<br/><b>Position</b>: Position of module.<br/><b>Status</b>: You can enable or disable the module.<br/><b>Sort Order</b>: Order position of the module.</span>';
?>