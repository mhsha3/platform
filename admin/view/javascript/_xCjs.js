if(localStorage._xTh == 'dark'){$('#_xdmbtn').attr("class","fa fa-sun text-white");$('#_xdm').attr("href", "view/stylesheet/_xdm.css");}
$('#_xdmbtn').click(()=>{
    if(localStorage._xTh == 'dark'){
        $('#_xdmbtn').attr("class","fa fa-moon text-dark");
        $('#_xdm').attr("href", "");
        localStorage._xTh = 'light';
    }else{
        $('#_xdmbtn').attr("class","fa fa-sun text-white");
        $('#_xdm').attr("href", "view/stylesheet/_xdm.css");
        localStorage._xTh = 'dark';
    }
});
$(document).bind('keydown', 'alt+p',()=>{location.href="index.php?route=catalog/product&user_token="+_token});
$(document).bind('keydown', 'alt+c',()=>{location.href="index.php?route=catalog/category&user_token="+_token});
$(document).bind('keydown', 'alt+h',()=>{location.href="index.php?route=common/dashboard&user_token="+_token});
(function(w,d,v3){
w.chaportConfig = {
appId : '5ff65a89f59ebb5a18f4ca97'
};
if(w.chaport)return;v3=w.chaport={};v3._q=[];v3._l={};v3.q=function(){v3._q.push(arguments)};v3.on=function(e,fn){if(!v3._l[e])v3._l[e]=[];v3._l[e].push(fn)};var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://app.chaport.com/javascripts/insert.js';var ss=d.getElementsByTagName('script')[0];ss.parentNode.insertBefore(s,ss)})(window, document);
