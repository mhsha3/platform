<?php
class ModelExtensionShippingFastcooFastcoo extends Model {
	
	public function getOrderStatusId($order_id) {
		$query = $this->db->query("SELECT order_status_id FROM " . DB_PREFIX . "order WHERE order_id =".(int)$order_id);
		return $query->row['order_status_id'];
	}
    
	public function addOrderHistory($order_id, $data) {
           
		$this->load->model('sale/order');
		$order_status_id = $this->getOrderStatusId($order_id);
		
		
		$this->db->query("UPDATE `" . DB_PREFIX . "order` SET order_status_id = '" . (int)$order_status_id . "', date_modified = NOW() WHERE order_id = '" . (int)$order_id . "'");
		$this->db->query("INSERT INTO " . DB_PREFIX . "order_history SET order_id = '" . (int)$order_id . "', order_status_id = '" . (int)$order_status_id . "', notify = '" . (isset($data['notify']) ? (int)$data['notify'] : 0) . "', comment = '" . $this->db->escape($data['comment']) . "', date_added = NOW()");

		$order_info = $this->model_sale_order->getOrder($order_id);

		// Send out any gift voucher mails
		if ($this->config->get('config_complete_status_id') == $order_status_id) {
			$this->load->model('sale/voucher');

			$results = $this->getOrderVouchers($order_id);

			foreach ($results as $result) {
				$this->model_sale_voucher->sendVoucher($result['voucher_id']);
			}
		}

		if ($data['notify']) {

            $this->load->language('sale/order');
			$this->language->load('mail/order');

            $subject = sprintf($this->language->get('fastcoo_text_subject'), $order_info['store_name'], $order_id);

            $data_send = array();                        
            if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$base = HTTPS_CATALOG;
            } else {
                $base = HTTP_CATALOG;
            }
            
            $data_send['logo'] = $base . 'image/' . $this->config->get('config_logo');        
            $data_send['store_url'] =  $order_info['store_url']; 
            $data_send['store_name'] =  $order_info['store_name']; 
            $data_send['fastcoo_text_order_id'] =  $this->language->get('fastcoo_text_order_id') . ': ' . $order_id . "\n"; 
            $data_send['fastcoo_text_date_added'] =  $this->language->get('fastcoo_text_date_added') . ': ' . date($this->language->get('date_format_short'), strtotime($order_info['date_added']));
                         
			$order_status_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "order_status WHERE order_status_id = '" . (int)$order_status_id . "' AND language_id = '" . (int)$order_info['language_id'] . "'");

			if ($order_status_query->num_rows) {
				$data_send['fastcoo_text_order_status'] = $this->language->get('fastcoo_text_order_status') . ": </br>";
				$data_send['fastcoo_text_order_status'] .= $order_status_query->row['name'] . "</br>";
			}
                        
			if ($order_info['customer_id']) {
				$data_send['fastcoo_text_link'] = $this->language->get('fastcoo_text_link') . ": </br>";
				$data_send['fastcoo_text_link'] .= html_entity_decode($order_info['store_url'] . 'index.php?route=account/order/info&order_id=' . $order_id, ENT_QUOTES, 'UTF-8') . "</br>";
			}
                      
			if ($data['comment']) {
				$data_send['comment'] = $this->language->get('fastcoo_text_comment') . ": </br>";
				$data_send['comment'] .= html_entity_decode($data['comment'], ENT_QUOTES, 'UTF-8') . "</br>";
			}
                        
			$adminemail = $this->config->get('config_email');

            // CODE HERE IF HIGHER
            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->hostname = $this->config->get('config_smtp_host');
            $mail->username = $this->config->get('config_smtp_username');
            $mail->password = $this->config->get('config_smtp_password');
            $mail->port = $this->config->get('config_smtp_port');
            $mail->timeout = $this->config->get('config_smtp_timeout');
            $mail->setTo($order_info['email'] . ',' . $adminemail);
            $mail->setFrom($this->config->get('config_email'));
            $mail->setSender($order_info['store_name']);
            $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
            $mail->setHtml($this->load->view('extension/shipping/fastcoo/mail_order', $data_send));
            $mail->send();

		}
	
	}
    	
	public function checkAWB($order_id)  {
        $query = $this->db->query("SELECT oh.date_added, os.name AS status, oh.comment, oh.notify FROM " . DB_PREFIX . "order_history oh LEFT JOIN " . DB_PREFIX . "order_status os ON oh.order_status_id = os.order_status_id WHERE oh.comment LIKE '%AWB No%' AND oh.order_id = '" . (int)$order_id . "' AND os.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY oh.date_added ASC");
        $shipments = $query->rows;
        if($shipments) {
            return 1;
        }else{
            return 0;
        }
	}
	
	protected function getStringBetween($string, $start, $end){
		$string = ' ' . $string;
		$ini = strpos($string, $start);
		if ($ini == 0) return '';

		$ini += strlen($start);
		$len = strpos($string, $end, $ini) - $ini;
		return substr($string, $ini, $len);
	}

	public function getAWBNumber($order_id) {
        $query = $this->db->query("SELECT oh.date_added, os.name AS status, oh.comment, oh.notify FROM " . DB_PREFIX . "order_history oh LEFT JOIN " . DB_PREFIX . "order_status os ON oh.order_status_id = os.order_status_id WHERE oh.comment LIKE '%AWB No%' AND oh.order_id = '" . (int)$order_id . "' AND os.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY oh.date_added ASC");
        $shipments = $query->rows;
        if($shipments) {
            foreach($shipments as $key=>$comment) {
                $cmnt_txt = ($comment['comment'])?$comment['comment']:'';
				$awb_no = $this->getStringBetween($cmnt_txt, 'AWB No. ', ' [');
                break;
            }
                
            return $awb_no;
        }else{
            return 0;
        }
	}
		
	public function getAWBPrintURL($order_id) {
        $query = $this->db->query("SELECT oh.date_added, os.name AS status, oh.comment, oh.notify FROM " . DB_PREFIX . "order_history oh LEFT JOIN " . DB_PREFIX . "order_status os ON oh.order_status_id = os.order_status_id WHERE oh.comment LIKE '%AWB No%' AND oh.order_id = '" . (int)$order_id . "' AND os.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY oh.date_added ASC");
        $shipments = $query->rows;
        if($shipments) {
            foreach($shipments as $key => $comment) {
                $cmnt_txt = ($comment['comment'])?$comment['comment']:'';
				$awb_print_url = $this->getStringBetween($cmnt_txt, '<a href="', '"');
                break;
            }
            return $awb_print_url;
        }else{
            return false;
        }
    }

}
