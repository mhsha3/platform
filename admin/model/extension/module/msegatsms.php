<?php

class ModelExtensionModuleMsegatsms extends Model {

    public function send_message($destination, $message, $gateway) {
        $this->language->load('extension/module/msegatsendsms');
        mb_internal_encoding("UTF-8");
        mb_http_output("UTF-8");
        
        $msegatsms = $this->config->get('msegatsms');
        $userkey = '';
        if (isset($msegatsms['userkey_msegat']) && !empty($msegatsms['userkey_msegat'])) {
            $userkey = $msegatsms['userkey_msegat'];
        }
        $passkey = '';
        if (isset($msegatsms['passkey_msegat']) && !empty($msegatsms['passkey_msegat'])) {
            $passkey = $msegatsms['passkey_msegat'];
        }
        $from = '';
        if (isset($msegatsms['senderid_msegat']) && !empty($msegatsms['senderid_msegat'])) {
            $from = $msegatsms['senderid_msegat'];
        }
        $url = '';
        if (isset($msegatsms['httpapi_msegat']) && !empty($msegatsms['httpapi_msegat'])) {
            $url = $msegatsms['httpapi_msegat'];
        }
        $text = urlencode($message);
        $content = 'userName=' . urlencode($userkey) . '&apiKey=' . urlencode($passkey) . '&userSender=' . urlencode($from) . '&numbers=' . urlencode($destination) . '&msg=' . $text . '&By=OpenCart&msgEncoding=UTF8';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        curl_close($ch);
        switch ($output) {
            case 1:
                $status = 'Success';
                break;
            case 1010:
                $status = 'Variables missing';
                break;
            case 1020:
                $status = 'Invalid login info';
                break;
            case 1050:
                $status = 'MSG body is empty';
                break;
            case 1060:
                $status = 'Balance is not enough';
                break;
            case 1061:
                $status = 'MSG duplicated';
                break;
            case 1110:
                $status = 'Sender name is missing or incorrect';
                break;
            case 1120:
                $status = 'Mobile numbers is not correct';
                break;
            case 1140:
                $status = 'MSG length is too long';
                break;
            default:
                $status = $output;
        }
        return $status;
    }

    public function getConvertPhonePrefix($phone, $iso_code) {
        return $phone;
    }

    public function hex_chars($data) {
        $mb_hex = '';
        for ($i = 0; $i < mb_strlen($data, 'UTF-8'); $i++) {
            $c = mb_substr($data, $i, 1, 'UTF-8');
            $o = unpack('N', mb_convert_encoding($c, 'UCS-4BE', 'UTF-8'));
            $mb_hex .= sprintf('%04X', $o[1]);
        }
        return $mb_hex;
    }

    public function createMseTables() {
        $sql = "CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "msegatsms_history` (
                `voodoo_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                `d` varchar(255) NOT NULL DEFAULT '',
                `fromorder_i` varchar(255) NOT NULL DEFAULT '',
                `to` varchar(255) NOT NULL DEFAULT '',
                `sms_message` text NOT NULL,
                `status` varchar(255) NOT NULL DEFAULT '',
                `status_message` varchar(255) NOT NULL DEFAULT '',
                `created_time` datetime DEFAULT NULL,
                PRIMARY KEY (`voodoo_id`)
             )";
        $query = $this->db->query($sql);
        
    }

    public function deleteMseTables() {
        $query = $this->db->query("DROP TABLE IF EXISTS " . DB_PREFIX . "msegatsms_history");
    }

}

?>