<?php
class ModelExtensionModuleTabby extends Model {

    public function getIsConfigured() {
        $this->load->model('setting/setting');
        
        return (
            $this->config->get('module_tabby_status') == 1 &&
            !empty($this->config->get('module_tabby_public_key')) &&
            !empty($this->config->get('module_tabby_secret_key'))
        );
    }
    public function getConfigureWarningMessage() {
        $this->load->language('extension/module/tabby');

        return sprintf(
            $this->language->get('warning_not_configured'), 
            $this->url->link('extension/module/tabby', 'user_token=' . $this->session->data['user_token'], true)
        );
    }
    public function getTransaction($order_id) {
        return $this->db->query("SELECT * FROM `" . DB_PREFIX . "tabby_transaction` WHERE order_id='" . (int)$order_id . "'")->row;
    }
    public function getTransactions($order_id) {
        $txn = $this->getTransaction($order_id);

        $transactions = [];
        if ($txn && !empty($txn['body'])) {
            $txn = json_decode($txn['body']);
            $transactions[] = [
                "id"        => $txn->id,
                "order_id"  => $order_id,
                "type"      => 'AUTH',
                "amount"    => $txn->amount,
                "currency"  => $txn->currency,
                "date"      => $txn->created_at
            ];
            foreach ($txn->captures as $capture) {
                $transactions[] = [
                    "id"        => $capture->id,
                    "payment_id"=> $txn->id,
                    "order_id"  => $order_id,
                    "type"      => 'CAPTURE',
                    "amount"    => $capture->amount,
                    "currency"  => $txn->currency,
                    "date"      => $capture->created_at
                ];
            }
            foreach ($txn->refunds as $refund) {
                $transactions[] = [
                    "id"        => $refund->id,
                    "payment_id"=> $txn->id,
                    "capture_id"=> $refund->capture_id,
                    "order_id"  => $order_id,
                    "type"      => 'REFUND',
                    "amount"    => $refund->amount,
                    "currency"  => $txn->currency,
                    "date"      => $refund->created_at
                ];
            }
        }

        return $transactions;
    }
    public function addTransaction($data) {
        $this->db->query("INSERT INTO `" . DB_PREFIX . "tabby_transaction` 
            (`order_id`, `transaction_id`, `body`) 
            VALUES (
                '".$this->db->escape($data['order_id'])."',
                '".$this->db->escape($data['transaction_id'])."',
                '".$this->db->escape($data['body'])."'
            )");
    }
    public function createTables() {
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "tabby_transaction` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `transaction_id` varchar(64) NOT NULL,
            `body` TEXT,
            `order_id` int(11) NOT NULL,

            PRIMARY KEY (`id`),
            KEY (order_id),
            KEY (transaction_id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8");
    }

    public function dropTables() {
        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "tabby_transaction`");
    }

}
