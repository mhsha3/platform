<?php
/**
 * Created by PhpStorm.
 * User: anhvnit
 * Date: 10/29/18
 * Time: 09:54
 */
class ModelExtensionOpenposTransaction extends Model{

    public function getTotalTransactions($data){
        $sql = "SELECT COUNT(DISTINCT r.transaction_id) AS total  FROM " . DB_PREFIX . "openpos_transaction r ";

        if (!empty($data['filter_note'])) {
            $sql .= " WHERE r.comment LIKE '%" . $this->db->escape($data['filter_note']) . "%'";
        }

        $query = $this->db->query($sql);
        return $query->row['total'];
    }

    public function transactions($data)
    {
        $sql = "SELECT r.*,u.firstname,u.lastname,u.username FROM " . DB_PREFIX . "openpos_transaction r LEFT JOIN " . DB_PREFIX . "user u ON (r.created_by = u.user_id)";

        if (!empty($data['filter_note'])) {
            $sql .= " WHERE r.comment LIKE '%" . $this->db->escape($data['filter_note']) . "%'";
        }

        $sort_data = array(
            'r.in_amount',
            'r.out_amount',
            'r.created_at'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY r.date_added";
        }

        if (isset($data['order']) && ( strtoupper($data['order']) == 'ASC')) {
            $sql .= " ASC";
        } else {
            $sql .= " DESC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);
        return $query->rows;
    }
    public function getCashBalance($store_id = -1)
    {
        $sql = "SELECT SUM(r.in_amount) as total_in_amount , SUM(r.out_amount) as total_out_amount  FROM " . DB_PREFIX . "openpos_transaction r ";
        if($store_id >=0)
        {
            $sql .= " WHERE r.store_id=".(int)$store_id;
        }
        $query = $this->db->query($sql);
        $row = $query->row;
        return ($row['total_in_amount'] - $row['total_out_amount']);
    }

    public function getRegisterCashBalance($register_id = -1)
    {
        $sql = "SELECT SUM(r.in_amount) as total_in_amount , SUM(r.out_amount) as total_out_amount  FROM " . DB_PREFIX . "openpos_transaction r ";
        if($register_id >=0)
        {
            $sql .= " WHERE r.register_id=".(int)$register_id;
        }
        $query = $this->db->query($sql);
        $row = $query->row;
        return ($row['total_in_amount'] - $row['total_out_amount']);
    }
    public function deleteTransaction($transaction_id){
        $sql = "DELETE  FROM " . DB_PREFIX . "openpos_transaction WHERE transaction_id='".(int)$transaction_id."'";
        $this->db->query($sql);
    }

}