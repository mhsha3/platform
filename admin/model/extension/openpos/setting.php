<?php
/**
 * Created by PhpStorm.
 * User: anhvnit
 * Date: 10/29/18
 * Time: 09:56
 */
class ModelExtensionOpenposSetting extends Model{
    public $setting = array();
    public function __construct($registry)
    {
        parent::__construct($registry);

    }


    public function saveSetting($data,$store_id){
        $code = 'openpos';
        $this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `code` = '" . $this->db->escape($code) . "'");
        foreach($data as $key => $session_data)
        {
            $this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape(json_encode($session_data, true)) . "', serialized = '1'");
        }
    }
}