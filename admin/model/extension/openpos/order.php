<?php
/**
 * Created by PhpStorm.
 * User: anhvnit
 * Date: 10/29/18
 * Time: 09:54
 */
class ModelExtensionOpenposOrder extends Model{

    public function getTotalPosOrder($register_id = 0){
        $sql = "SELECT COUNT(DISTINCT r.order_id) AS total  FROM " . DB_PREFIX . "openpos_order r ";

        if ($register_id) {
            $sql .= " WHERE r.register_id = ".(int)$register_id;
        }

        $query = $this->db->query($sql);
        return $query->row['total'];
    }
    public function getTotalSalesPosOrder($register_id = 0){
        $sql = "SELECT SUM(t.value) AS total_sales  FROM " . DB_PREFIX . "openpos_order r  LEFT JOIN " . DB_PREFIX . "order_total t ON r.order_id = t.order_id WHERE t.code = 'total'";

        if ($register_id) {
            $sql .= " AND r.register_id = ".(int)$register_id;
        }

        $query = $this->db->query($sql);
        return $query->row['total_sales'];
    }

    public function getTotalSalesPosOrderByStore($store_id){
        $sql = "SELECT SUM(t.value) AS total_sales  FROM " . DB_PREFIX . "openpos_order r LEFT JOIN " . DB_PREFIX . "order o ON r.order_id = o.order_id   LEFT JOIN " . DB_PREFIX . "order_total t ON r.order_id = t.order_id WHERE t.code = 'total'";


        $sql .= " AND o.store_id = ".(int)$store_id;


        $query = $this->db->query($sql);
        return $query->row['total_sales'];
    }

    public function getOrders($args=array()){
        $sql = "SELECT r.*,t.value as grand_total, o.*  FROM " . DB_PREFIX . "openpos_order r   LEFT JOIN " . DB_PREFIX . "order_total t ON (r.order_id = t.order_id AND t.code = 'total') LEFT JOIN " . DB_PREFIX . "order o ON o.order_id = r.order_id";


        $sql .= " ORDER BY r.date_added DESC";
        $sql .= " LIMIT 0,10";

        $query = $this->db->query($sql);
        return $query->rows;
    }
    public function getTotalSalePosByDate($date)
    {
        $implode = array();

        foreach ($this->config->get('config_complete_status') as $order_status_id) {
            $implode[] = "'" . (int)$order_status_id . "'";
        }


        $next_date = $date.' 23:59:59';
        $sql = "SELECT SUM(t.value) as grand_total  FROM " . DB_PREFIX . "openpos_order r   LEFT JOIN " . DB_PREFIX . "order_total t ON (r.order_id = t.order_id AND t.code = 'total') LEFT JOIN " . DB_PREFIX . "order o ON o.order_id = r.order_id";
        $sql .= " WHERE o.order_status_id IN(" . implode(",", $implode) . ") AND DATE(o.date_added) >= DATE('" . $this->db->escape($date) . "') AND DATE(o.date_added) <= DATE('" . $this->db->escape($next_date) . "')";

        $query = $this->db->query($sql);
        $row = $query->row;

        return 1* $row['grand_total'];
    }
    public function getPosOrder($order_id)
    {
        $sql = "SELECT *  FROM " . DB_PREFIX . "openpos_order WHERE order_id= '".(int)$order_id."' ";
        $query = $this->db->query($sql);
        return $query->row;
    }
    public function getPosOrderPayments($order_id)
    {
        $sql = "SELECT *  FROM " . DB_PREFIX . "openpos_order_payment WHERE order_id= '".(int)$order_id."' ";
        $query = $this->db->query($sql);
        return $query->rows;
    }
}