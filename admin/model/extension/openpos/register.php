<?php
/**
 * Created by PhpStorm.
 * User: anhvnit
 * Date: 10/29/18
 * Time: 09:56
 */
class ModelExtensionOpenposRegister extends Model{
    public function save($data)
    {
        $is_new = false;
        if(!isset($data['register_id']) || !$data['register_id'])
        {
            $is_new = true;
        }else{
            $register_id = (int)$data['register_id'];
        }

        if($is_new)
        {
            $this->db->query("INSERT INTO " . DB_PREFIX . "openpos_register SET register_name = '" . $this->db->escape($data['register_name']) . "', store_id = '" . $this->db->escape($data['store_id']) . "', status = '" . $this->db->escape($data['status']) . "', date_added = NOW(), date_modified = NOW()");
            $register_id = $this->db->getLastId();

        }else{
            $this->db->query("UPDATE " . DB_PREFIX . "openpos_register SET register_name = '" . $this->db->escape($data['register_name']) . "', store_id = '" . $this->db->escape($data['store_id']) . "', status = '" . $this->db->escape($data['status']) . "', date_modified = NOW() WHERE register_id = ".(int)$register_id);
            $this->db->query("DELETE FROM " . DB_PREFIX . "openpos_register_user WHERE register_id = '" . (int)$register_id . "'");
        }
        if(isset($data['cashiers']) && !empty($data['cashiers'])){
            foreach($data['cashiers'] as $cashier)
            {
                if($cashier)
                {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "openpos_register_user SET register_id = '" . $this->db->escape($register_id) . "', user_id = '" . $this->db->escape($cashier) . "', date_added = NOW(), date_modified = NOW()");
                }
            }
        }
    }
    public function delete($register_id)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "openpos_register_user WHERE register_id = '" . (int)$register_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "openpos_register WHERE register_id = '" . (int)$register_id . "'");
    }
    public function registers(){
        $sql = "SELECT * FROM " . DB_PREFIX . "openpos_register";
        $query = $this->db->query($sql);
        return $query->rows;
    }
    public function register($register_id){
        $sql = "SELECT * FROM " . DB_PREFIX . "openpos_register WHERE register_id=".(int)$register_id;
        $query = $this->db->query($sql);
        return $query->row;
    }
    public function cashiers($register_id)
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "openpos_register_user r LEFT JOIN " . DB_PREFIX . "user u ON (r.user_id = u.user_id) WHERE r.register_id=".$register_id;
        $query = $this->db->query($sql);
        return $query->rows;
    }

}