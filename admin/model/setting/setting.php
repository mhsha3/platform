<?php
class ModelSettingSetting extends Model {
    public function msegateditSetting($code, $data, $store_id = 0) { foreach ($data as $key => $value) { if (!is_array($value)) { $this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'"); } else { $this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape(serialize($value)) . "', serialized = '1'"); } } }
    
	public function getSetting($code, $store_id = 0) {
		$setting_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `code` = '" . $this->db->escape($code) . "'");

		foreach ($query->rows as $result) {
			if (!$result['serialized']) {
				$setting_data[$result['key']] = $result['value'];
			} else {
				$setting_data[$result['key']] = json_decode($result['value'], true);
			}
		}

		return $setting_data;
	}

	public function editSetting($code, $data, $store_id = 0) {
		 $this->db->query("DELETE FROM `" . DB_PREFIX . "setting` WHERE store_id = '" . (int)$store_id . "' AND `code` = '" . $this->db->escape($code) . "'");

		foreach ($data as $key => $value) {
			if (substr($key, 0, strlen($code)) == $code) {

				if (!is_array($value)) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");


				} else {
					$this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '" . (int)$store_id . "', `code` = '" . $this->db->escape($code) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape(json_encode($value, true)) . "', serialized = '1'");

				}
			}

		}
//        if(isset($this->request->files['config_image']) && $this->request->files['config_image']['error'] == UPLOAD_ERR_OK) {
//            $data['config_image'] = $this->uploadImage($store_id,'config_image');
//        } else if (isset($data['image_remove']) && $data['image_remove'] == 1) {
//            if($data['config_image'] != 'no_image.png' && file_exists(DIR_IMAGE . $data['config_image'])) {
//                @unlink(DIR_IMAGE . $data['config_image']);
//                $data['config_image'] = 'no_image.png';
//            }
//        }
//
//        if (isset($data['config_image'])) {
//            $this->db->query("UPDATE " . DB_PREFIX . "setting SET `value` = '" . $this->db->escape($data['config_image']) . "' WHERE `key`='config_image' and `code`='config' and store_id = '" . (int)$store_id . "'");
//        }

        $this->updateImageFile($this->request,'config_image','image_remove',$store_id,$data);
        $this->updateImageFile($this->request,'config_icon','icon_remove',$store_id,$data);
        $this->updateImageFile($this->request,'config_logo','logo_remove',$store_id,$data);


	}



	public function deleteSetting($code, $store_id = 0) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `code` = '" . $this->db->escape($code) . "'");
	}
	
	public function getSettingValue($key, $store_id = 0) {
		$query = $this->db->query("SELECT value FROM " . DB_PREFIX . "setting WHERE store_id = '" . (int)$store_id . "' AND `key` = '" . $this->db->escape($key) . "'");

		if ($query->num_rows) {
			return $query->row['value'];
		} else {
			return null;	
		}
	}
	
	public function editSettingValue($code = '', $key = '', $value = '', $store_id = 0)
    {

        if (!is_array($value)) {
            $this->db->query("UPDATE " . DB_PREFIX . "setting SET `value` = '" . $this->db->escape($value) . "', serialized = '0'  WHERE `code` = '" . $this->db->escape($code) . "' AND `key` = '" . $this->db->escape($key) . "' AND store_id = '" . (int)$store_id . "'");
        } else {
            $this->db->query("UPDATE " . DB_PREFIX . "setting SET `value` = '" . $this->db->escape(json_encode($value)) . "', serialized = '1' WHERE `code` = '" . $this->db->escape($code) . "' AND `key` = '" . $this->db->escape($key) . "' AND store_id = '" . (int)$store_id . "'");
        }
        $store_id = (int)$store_id;
        //  updateImage($this->request,'config_image','image_remove',$store_id);
       /* if ($key == 'config_image' || $key == 'image_remove') {
            if (isset($this->request->files[$key]) && $this->request->files[$key]['error'] == UPLOAD_ERR_OK) {
                $value = $this->uploadImage($store_id, $key);
            } else if ($key == 'image_remove') {
                if ($value != 'no_image.png' && file_exists(DIR_IMAGE . $value)) {
                    @unlink(DIR_IMAGE . $value);
                    $value = 'no_image.png';
                }
            }
            if (isset($value)) {
                $this->db->query("UPDATE " . DB_PREFIX . "setting SET `value` = '" . $this->db->escape($value) . "' WHERE `key`=" . $key . " AND `code`='config' AND store_id = '" . (int)$store_id . "'");
            }


        }*/

        $this->editUploadSetting($this->request,$store_id,'config_image',$value);
        $this->editUploadSetting($this->request,$store_id,'config_icon',$value);
        $this->editUploadSetting($this->request,$store_id,'config_logo',$value);
        $this->editUploadSetting($this->request,$store_id,'image_remove',$value);
        $this->editUploadSetting($this->request,$store_id,'icon_remove',$value);
        $this->editUploadSetting($this->request,$store_id,'logo_remove',$value);


    }

    public function editUploadSetting($request,$store_id,$key,$value){
        if (isset($request->files[$key]) && $request->files[$key]['error'] == UPLOAD_ERR_OK) {
            $value = $this->uploadImage($store_id, $key);
        } else if ($key == 'image_remove') {
            if ($value != 'no_image.png' && file_exists(DIR_IMAGE . $value)) {
                @unlink(DIR_IMAGE . $value);
                $value = 'no_image.png';
            }
        }
        if (isset($value)) {
            $this->db->query("UPDATE " . DB_PREFIX . "setting SET `value` = '" . $this->db->escape($value) . "' WHERE `key`=" . $key . " AND `code`='config' AND store_id = '" . (int)$store_id . "'");
        }
    }


    protected function uploadImage($user_id,$filename) {

        $catalog_users_pathrelative_path = 'catalog/settings';
        $catalog_users_directory = DIR_IMAGE . $catalog_users_pathrelative_path;

        if (!is_dir($catalog_users_directory)) {
            mkdir($catalog_users_directory, 0777);
            chmod($catalog_users_directory, 0777);

            @touch($catalog_users_directory . '/' . 'index.html');
        }

        $ext = pathinfo($this->request->files[$filename]['name'], PATHINFO_EXTENSION);
        $image = $catalog_users_pathrelative_path . '/' . md5($user_id.$filename) . '.' . $ext;
        move_uploaded_file($this->request->files[$filename]['tmp_name'], DIR_IMAGE . $image);

        return $image;

    }

    public function updateImageFile($request,$file_name,$remove_name,$store_id,$data){
        if(isset($request->files[$file_name]) && $request->files[$file_name]['error'] == UPLOAD_ERR_OK) {
            $data[$file_name] = $this->uploadImage($store_id,$file_name);
        } else if (isset($data[$remove_name]) && $data[$remove_name] == 1) {
            if($data[$file_name] != 'no_image.png' && file_exists(DIR_IMAGE . $data[$file_name])) {
                @unlink(DIR_IMAGE . $data[$file_name]);
                $data[$file_name] = 'no_image.png';
            }
        }

        if (isset($data[$file_name])) {
            $flag=$this->db->query("UPDATE " . DB_PREFIX . "setting SET `value` = '" . $this->db->escape($data[$file_name]) . "' WHERE `key`='".$file_name."' and `code`='config' and store_id = '" . (int)$store_id . "'");
        }
        return $flag;
    }

}
