<?php
class ModelLocalisationLocation extends Model {
	public function addLocation($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "location SET name = '" . $this->db->escape($data['name']) . "', address = '" . $this->db->escape($data['address']) . "', geocode = '" . $this->db->escape($data['geocode']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', open = '" . $this->db->escape($data['open']) . "', comment = '" . $this->db->escape($data['comment']) . "'");

		$location_id = $this->db->getLastId();

		if(isset($this->request->files['image']) && $this->request->files['image']['error'] == UPLOAD_ERR_OK) {
			$data['image'] = $this->uploadImage($location_id);
		}

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "location SET image = '" . $this->db->escape($data['image']) . "' WHERE location_id = '" . (int)$location_id . "'");
		}

		return $location_id;
	}

	protected function uploadImage($location_id) {

		$catalog_locations_pathrelative_path = 'catalog/locations';
		$catalog_locations_directory = DIR_IMAGE . $catalog_locations_pathrelative_path;

		if (!is_dir($catalog_locations_directory)) {
			mkdir($catalog_locations_directory, 0777);
			chmod($catalog_locations_directory, 0777);

			@touch($catalog_locations_directory . '/' . 'index.html');
		}

		$ext = pathinfo($this->request->files['image']['name'], PATHINFO_EXTENSION);
		$image = $catalog_locations_pathrelative_path . '/' . md5($location_id) . '.' . $ext;
		move_uploaded_file($this->request->files['image']['tmp_name'], DIR_IMAGE . $image);

		return $image;	
	}

	public function editLocation($location_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "location SET name = '" . $this->db->escape($data['name']) . "', address = '" . $this->db->escape($data['address']) . "', geocode = '" . $this->db->escape($data['geocode']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', open = '" . $this->db->escape($data['open']) . "', comment = '" . $this->db->escape($data['comment']) . "' WHERE location_id = '" . (int)$location_id . "'");

		if(isset($this->request->files['image']) && $this->request->files['image']['error'] == UPLOAD_ERR_OK) {
			$data['image'] = $this->uploadImage($location_id);
		} else if (isset($data['image_remove']) && $data['image_remove'] == 1) {
			if($data['image'] != 'no_image.png' && file_exists(DIR_IMAGE . $data['image'])) {
				@unlink(DIR_IMAGE . $data['image']);
				$data['image'] = 'no_image.png';
			}
		}

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "location SET image = '" . $this->db->escape($data['image']) . "' WHERE location_id = '" . (int)$location_id . "'");
		}
	}

	public function getLocationImage($location_id) {
		$query = $this->db->query("SELECT image FROM " . DB_PREFIX . "location WHERE location_id = '" . (int)$location_id . "'");
		return ($query->num_rows) ? $query->row['image'] : null;
	}

	public function deleteLocation($location_id) {
		$location_image = $this->getLocationImage($location_id);
		if(isset($location_image) && $location_image != 'no_image.png') {
			@unlink(DIR_IMAGE . $location_image);
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "location WHERE location_id = " . (int)$location_id);
	}

	public function getLocation($location_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "location WHERE location_id = '" . (int)$location_id . "'");

		return $query->row;
	}

	public function getLocations($data = array()) {
		$sql = "SELECT location_id, name, address FROM " . DB_PREFIX . "location";

		$sort_data = array(
			'name',
			'address',
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalLocations() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "location");

		return $query->row['total'];
	}
}
