<?php
class ModelDesignBanner extends Model {
    public function addBanner($data) {
        $this->db->query("INSERT INTO " . DB_PREFIX . "banner SET name = '" . $this->db->escape($data['name']) . "', status = '" . (int)$data['status'] . "'");

        $banner_id = $this->db->getLastId();

        if (isset($data['banner_image'])) {
            foreach ($data['banner_image'] as $language_id => $value) {
                foreach ($value as $image_index_key => $banner_image) {
                    if(isset($this->request->files['banner_image']['name'][$language_id][$image_index_key]['image']) && $this->request->files['banner_image']['error'][$language_id][$image_index_key]['image'] == UPLOAD_ERR_OK) {
                        $banner_image['image'] = $this->uploadImage($banner_id, $language_id, $image_index_key);
                    }
                    $this->db->query("INSERT INTO " . DB_PREFIX . "banner_image SET banner_id = '" . (int)$banner_id . "', language_id = '" . (int)$language_id . "', title = '" .  $this->db->escape($banner_image['title']) . "', link = '" .  $this->db->escape($banner_image['link']) . "', image = '" .  $this->db->escape($banner_image['image']) . "', sort_order = '" .  (int)$banner_image['sort_order'] . "'");
                }
            }
        }

        return $banner_id;
    }

    protected function uploadImage($banner_id, $language_id, $image_index_key) {

        $design_banners_pathrelative_path = 'catalog/banners';
        $design_banners_directory = DIR_IMAGE . $design_banners_pathrelative_path;

        if (!is_dir($design_banners_directory)) {
            mkdir($design_banners_directory, 0777);
            chmod($design_banners_directory, 0777);

            @touch($design_banners_directory . '/' . 'index.html');
        }

        $new_image_relative_path = $design_banners_pathrelative_path . '/' . $banner_id;
        $banner_directory = DIR_IMAGE . $new_image_relative_path;

        if (!is_dir($banner_directory)) {
            mkdir($banner_directory, 0777);
            chmod($banner_directory, 0777);

            @touch($banner_directory . '/' . 'index.html');
        }


        $ext = pathinfo($this->request->files['banner_image']['name'][$language_id][$image_index_key]['image'], PATHINFO_EXTENSION);
        $image = $new_image_relative_path . '/' . md5(uniqid(rand(), true)) . '.' . $ext;
        move_uploaded_file($this->request->files['banner_image']['tmp_name'][$language_id][$image_index_key]['image'], DIR_IMAGE . $image);

        return $image;

    }

    protected function deleteBannerUnsedImages($banner_id, $exclude_files = []) {

        $dirPath = DIR_IMAGE . 'catalog/banners/' . $banner_id;
        if (is_dir($dirPath)) {

            if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
                $dirPath .= '/';
            }

            $files = glob($dirPath . '*', GLOB_MARK);
            foreach ($files as $file) {
                if(is_file($file) && !in_array($file, $exclude_files) && $file != $dirPath . 'index.html') {
                    unlink($file);
                }
            }
        }
    }

    protected function deleteBannerFolder($banner_id) {
        $dirPath = DIR_IMAGE . 'catalog/banners/' . $banner_id;
        if (is_dir($dirPath)) {

            if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
                $dirPath .= '/';
            }

            $files = glob($dirPath . '*', GLOB_MARK);
            foreach ($files as $file) {
                if (is_dir($file)) {
                    self::deleteBannerFolder($file);
                } else {
                    unlink($file);
                }
            }
            rmdir($dirPath);
        }
    }

    public function editBanner($banner_id, $data) {

        $this->db->query("UPDATE " . DB_PREFIX . "banner SET name = '" . $this->db->escape($data['name']) . "', status = '" . (int)$data['status'] . "' WHERE banner_id = '" . (int)$banner_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "banner_image WHERE banner_id = '" . (int)$banner_id . "'");

        $exclude_files = [];
        if (isset($data['banner_image'])) {
            foreach ($data['banner_image'] as $language_id => $value) {
                foreach ($value as $image_index_key => $banner_image) {
                    if(isset($this->request->files['banner_image']['name'][$language_id][$image_index_key]['image']) && $this->request->files['banner_image']['error'][$language_id][$image_index_key]['image'] == UPLOAD_ERR_OK) {
                        $banner_image['image'] = $this->uploadImage($banner_id, $language_id, $image_index_key);
                    } else if (isset($banner_image['image_remove']) && $banner_image['image_remove'] == 1) {
                        if($banner_image['image'] != 'no_image.png' && file_exists(DIR_IMAGE . $banner_image['image'])) {
                            @unlink(DIR_IMAGE . $banner_image['image']);
                            $banner_image['image'] = 'no_image.png';
                        }
                    }
                    $exclude_files[] = DIR_IMAGE . $banner_image['image'];
                    $this->db->query("INSERT INTO " . DB_PREFIX . "banner_image SET banner_id = '" . (int)$banner_id . "', language_id = '" . (int)$language_id . "', title = '" .  $this->db->escape($banner_image['title']) . "', link = '" .  $this->db->escape($banner_image['link']) . "', image = '" .  $this->db->escape($banner_image['image']) . "', sort_order = '" . (int)$banner_image['sort_order'] . "'");
                }
            }
        }
        $this->deleteBannerUnsedImages($banner_id, $exclude_files);
    }

    public function deleteBanner($banner_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "banner WHERE banner_id = '" . (int)$banner_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "banner_image WHERE banner_id = '" . (int)$banner_id . "'");
        $this->deleteBannerFolder($banner_id);
    }

    public function getBanner($banner_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "banner WHERE banner_id = '" . (int)$banner_id . "'");

        return $query->row;
    }

    public function getBanners($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "banner";

        $sort_data = array(
            'name',
            'status'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getBannerImages($banner_id) {
        $banner_image_data = array();

        $banner_image_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "banner_image WHERE banner_id = '" . (int)$banner_id . "' ORDER BY sort_order ASC");

        foreach ($banner_image_query->rows as $banner_image) {
            $banner_image_data[$banner_image['language_id']][] = array(
                'title'      => $banner_image['title'],
                'link'       => $banner_image['link'],
                'image'      => $banner_image['image'],
                'sort_order' => $banner_image['sort_order']
            );
        }

        return $banner_image_data;
    }

    public function getTotalBanners() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "banner");

        return $query->row['total'];
    }
}
