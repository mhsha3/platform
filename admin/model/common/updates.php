<?php
class ModelCommonUpdates extends Model {
	public function add(){
        // 4-2-2021 Notifications 
        //$this->db->query("DROP TABLE IF EXISTS oc_store_notifications");
        $this->db->query("CREATE TABLE IF NOT EXISTS `oc_store_notifications` ( 
            `notification_id` INT NOT NULL AUTO_INCREMENT , 
            `title` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , 
            `tag` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , 
            `content` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , 
            `added_at` DATETIME NOT NULL , 
            `viewed_at` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL, 
            PRIMARY KEY (`notification_id`)) ENGINE = MyISAM;");
       
        // 4-2-2021 Activity login 
        $this->db->query("CREATE TABLE IF NOT EXISTS `oc_users_activity_login` ( 
                `login_id` INT NOT NULL AUTO_INCREMENT , 
                `user_id` INT NOT NULL , 
                `user_agent` LONGTEXT NOT NULL , 
                `ip` VARCHAR(255) NOT NULL , 
                `login_at` DATETIME NOT NULL , 
            PRIMARY KEY (`login_id`)) ENGINE = MyISAM;");
    }
}