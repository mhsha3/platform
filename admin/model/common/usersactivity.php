<?php
class ModelCommonUsersactivity extends Model {
    protected function ip(){
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
    
	public function setLogin(){
        $this->db->query("INSERT INTO `oc_users_activity_login` (`login_id`, `user_id`, `user_agent`, `ip`, `login_at`) 
        VALUES (NULL, '". $this->user->getId() ."', '". $_SERVER['HTTP_USER_AGENT'] ."', '" . $this->ip() . "', '".date("Y-m-d H:i:s")."')");
        return 1;
    }
}