<?php
class ModelCommonLanaapp extends Model{
    private $connection;
    protected function query($sql){
		$this->connection = new \mysqli(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, 'lanaapp_system', '3306');
		if ($this->connection->connect_error) {
			throw new \Exception('Error: ' . $this->connection->error . '<br />Error No: ' . $this->connection->errno);
		}
		$this->connection->set_charset("utf8");
        $this->connection->query("SET SQL_MODE = ''");
        $query = $this->connection->query($sql);
		if (!$this->connection->errno) {
			if ($query instanceof \mysqli_result) {
				$data = array();
				while ($row = $query->fetch_assoc()) {
					$data[] = $row;
				}
				$result = new \stdClass();
				$result->num_rows = $query->num_rows;
				$result->row = isset($data[0]) ? $data[0] : array();
				$result->rows = $data;
				$query->close();
				return $result;
			} else {
				return true;
			}
		} else {
			throw new \Exception('Error: ' . $this->connection->error  . '<br />Error No: ' . $this->connection->errno . '<br />' . $sql);
		}
    }
    protected function getLastId() {return $this->connection->insert_id;}
    // Functions -->
    public function add_services($type = '',$data=[]){
        $x = $this->query("INSERT INTO `services` (`id`, `type`, `store_id`, `data`, `employee_id`, `start_at`, `finished_at`, `note`, `created_at`, `status`)
      VALUES (NULL, '".(string)$type."', '".$this->config->get('config_lana_store_id')."', '".json_encode($data)."', '', '', '', '', '".date("Y-m-d H:i:s")."', '0')");
        return $this->getLastId();
    }
    public function action($sql = ''){
        return $this->query($sql);
    }
    // Eng.Mohab Functions -->
    public function PlatformUpdates(){
        $query = $this->query('SELECT * FROM `platform_updates` where 1 ORDER BY id DESC');
        return $query->rows;
    }
    public function getFAQ(){
        $query = $this->query('SELECT * FROM `faq_platform` where `status` = 1 ORDER BY sort_by ASC');
        return $query->rows;
    }

    public function checkDocuments(){
        // $query = $this->query("SELECT `status` FROM `documents` where store_id = '{$this->config->get('config_lana_store_id')}' ORDER BY id DESC limit 1");
        // return $query->row['status'] ?? null;
        return 1;
    }

    // LNA Dropshipping Functions -->
    public function getLnaProducts($data=[]){
        $query = $this->query('SELECT * FROM `dropshipping_products` where `status` = 1 ORDER BY product_id DESC');
        return $query->rows;
    }
    public function getLnaGetProduct($product_id = 0){
        $query = $this->query("SELECT * FROM `dropshipping_products` where `status` = 1 AND product_id = '{$product_id}' ");
        return $query->row;
    }
    public function getLnaCategories(){
        $query = $this->query("SELECT * FROM `dropshipping_categories` where `status` = 1 AND `parent_id` = 0 ORDER BY category_id DESC");
        return $query->rows;
    }
    public function getLnaSubCategories($category_id = 0 ){
        $query = $this->query("SELECT * FROM `dropshipping_categories` where `status` = 1 AND `parent_id` = '{$category_id}' ORDER BY category_id DESC");
        return $query->rows;
    }
}


