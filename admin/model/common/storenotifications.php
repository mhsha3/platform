<?php
class ModelCommonStorenotifications extends Model {
	public function getTotalNotifications(){
        $data = $this->db->query("SELECT count(*) as total FROM `oc_store_notifications` WHERE `viewed_at` = ''");
        return $data->row['total'];
    }
    
    public function getNotifications(){
        $data = $this->db->query("SELECT * FROM `oc_store_notifications` ORDER BY `notification_id` DESC");
        return $data->rows;
    }
    public function getNotificationById($id = 0){
        $data = $this->db->query("SELECT * FROM `oc_store_notifications` WHERE notification_id = '".(int)$id."' LIMIT 1");
        return $data->row;
    }
    public function viewedNotificationById($id = 0){
        $data = $this->db->query("UPDATE `oc_store_notifications` SET `viewed_at` = '".date("Y-m-d H:i:s")."' WHERE `notification_id` = '".(int)$id."'");
        return 1;
    }
    
}