<?php
class ModelCommonWallet extends Model {
	public function TotalBalance(){
        $total = $this->db->query("SELECT (SUM(amount) - (SUM(fees) + SUM(supplier_cost))) as total FROM `".DB_PREFIX."wallet_transaction` WHERE 1");
        return $total->row['total'];
    }
    public function GetTransactions(){
       $data = $this->db->query("SELECT * FROM `".DB_PREFIX."wallet_transaction` WHERE 1 ORDER BY transaction_id DESC");
       return $data->rows;
    }
    public function TotalTransactionsByMonth($date = 1){
        $total = $this->db->query("SELECT SUM(amount) as total FROM `".DB_PREFIX."wallet_transaction` WHERE MONTH(created_at) = '$date'");
        return $total->row['total'];
    }
}


