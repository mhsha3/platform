<?php
class ModelUserUser extends Model {
	public function addUser($data) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "user` SET username = '" . $this->db->escape($data['username']) . "', user_group_id = '" . (int)$data['user_group_id'] . "', salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', status = '" . (int)$data['status'] . "', date_added = NOW()");

		$user_id = $this->db->getLastId();

		if(isset($this->request->files['image']) && $this->request->files['image']['error'] == UPLOAD_ERR_OK) {
			$data['image'] = $this->uploadImage($user_id);
		}

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "user SET image = '" . $this->db->escape($data['image']) . "' WHERE user_id = '" . (int)$user_id . "'");
		}

		return $user_id;
	}

	protected function uploadImage($user_id) {

		$catalog_users_pathrelative_path = 'catalog/users';
		$catalog_users_directory = DIR_IMAGE . $catalog_users_pathrelative_path;

		if (!is_dir($catalog_users_directory)) {
			mkdir($catalog_users_directory, 0777);
			chmod($catalog_users_directory, 0777);

			@touch($catalog_users_directory . '/' . 'index.html');
		}

		$ext = pathinfo($this->request->files['image']['name'], PATHINFO_EXTENSION);
		$image = $catalog_users_pathrelative_path . '/' . md5($user_id) . '.' . $ext;
		move_uploaded_file($this->request->files['image']['tmp_name'], DIR_IMAGE . $image);

		return $image;

	}

	public function editUser($user_id, $data) {
		$this->db->query("UPDATE `" . DB_PREFIX . "user` SET username = '" . $this->db->escape($data['username']) . "', user_group_id = '" . (int)$data['user_group_id'] . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', status = '" . (int)$data['status'] . "' WHERE user_id = '" . (int)$user_id . "'");

		if(isset($this->request->files['image']) && $this->request->files['image']['error'] == UPLOAD_ERR_OK) {
			$data['image'] = $this->uploadImage($user_id);
		} else if (isset($data['image_remove']) && $data['image_remove'] == 1) {
			if($data['image'] != 'no_image.png' && file_exists(DIR_IMAGE . $data['image'])) {
				@unlink(DIR_IMAGE . $data['image']);
				$data['image'] = 'no_image.png';
			}
		}

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "user SET image = '" . $this->db->escape($data['image']) . "' WHERE user_id = '" . (int)$user_id . "'");
		}

		if ($data['password']) {
			$this->db->query("UPDATE `" . DB_PREFIX . "user` SET salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "' WHERE user_id = '" . (int)$user_id . "'");
		}
	}

	public function editPassword($user_id, $password) {
		$this->db->query("UPDATE `" . DB_PREFIX . "user` SET salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($password)))) . "', code = '' WHERE user_id = '" . (int)$user_id . "'");
	}

	public function editCode($email, $code) {
		$this->db->query("UPDATE `" . DB_PREFIX . "user` SET code = '" . $this->db->escape($code) . "' WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}

	public function getUserImage($user_id) {
		$query = $this->db->query("SELECT image FROM " . DB_PREFIX . "user WHERE user_id = '" . (int)$user_id . "'");
		return ($query->num_rows) ? $query->row['image'] : null;
	}

	public function deleteUser($user_id) {
		$user_image = $this->getUserImage($user_id);
		if(isset($user_image) && $user_image != 'no_image.png') {
			@unlink(DIR_IMAGE . $user_image);
		}

		$this->db->query("DELETE FROM `" . DB_PREFIX . "user` WHERE user_id = '" . (int)$user_id . "'");
	}

	public function getUser($user_id) {
		$query = $this->db->query("SELECT *, (SELECT ug.name FROM `" . DB_PREFIX . "user_group` ug WHERE ug.user_group_id = u.user_group_id) AS user_group FROM `" . DB_PREFIX . "user` u WHERE u.user_id = '" . (int)$user_id . "'");

		return $query->row;
	}

	public function getUserByUsername($username) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "user` WHERE username = '" . $this->db->escape($username) . "'");

		return $query->row;
	}

	public function getUserByEmail($email) {
		$query = $this->db->query("SELECT DISTINCT * FROM `" . DB_PREFIX . "user` WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}

	public function getUserByCode($code) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "user` WHERE code = '" . $this->db->escape($code) . "' AND code != ''");

		return $query->row;
	}

	public function getUsers($data = array()) {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "user` WHERE `username` = 'LANA_SUPPORT' AND email = 'support@lanaapp.sa'");
		$sql = "SELECT * FROM `" . DB_PREFIX . "user` WHERE `username` != 'LANA_SUPPORT' AND email != 'support@lanaapp.sa'";
		$sort_data = array(
			'username',
			'status',
			'date_added'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY username";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);



		return $query->rows;
	}

	public function getTotalUsers() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "user`");

		return $query->row['total'];
	}

	public function getTotalUsersByGroupId($user_group_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "user` WHERE user_group_id = '" . (int)$user_group_id . "'");

		return $query->row['total'];
	}

	public function getTotalUsersByEmail($email) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "user` WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row['total'];
	}
}
